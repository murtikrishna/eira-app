/**
 * 
 */

/* PIE CHARTS */

function eventsChartsAgainstViewType(siteCodes, siteCounts) {
//	Highcharts.chart('DataChart', {
//		chart : {
//			type : 'pie',
//			options3d : {
//				enabled : true,
//				alpha : 15
//			}
//		},
//		title : {
//			text : ''
//		},
//		subtitle : {
//			text : ''
//		},
//		tooltip : {
//			pointFormat : '{point.y}'
//		},
//		legend : {
//			align : 'right',
//			layout : 'vertical'
//		},
//		plotOptions : {
//			pie : {
//				innerSize : 120,
//				depth : 45,
//
//				dataLabels : {
//					enabled : false,
//					format : '{point.name}'
//				}
//			}
//
//		},
//		credits : {
//			enabled : false
//		},
//		exporting : {
//			menuItemDefinitions : {
//				// Custom definition
//				label : {
//					onclick : function() {
//						this.renderer
//								.label('You just clicked a custom menu item',
//										100, 100).attr({
//									fill : '#a4edba',
//									r : 5,
//									padding : 10,
//									zIndex : 10
//								}).css({
//									fontSize : '1.5em'
//								}).add();
//					},
//
//				}
//			},
//			buttons : {
//				contextButton : {
//					menuItems : [ 'downloadPNG', 'downloadJPEG', 'downloadPDF',
//							'downloadSVG', 'downloadCSV', 'downloadXLS' ]
//				}
//			}
//		},
//		series : [ {
//			name : ' ',
//			data : ticketsdata
//		} ]
//	});

	Highcharts
			.chart(
					'top5EventSites',
					{
						chart : {
							type : 'column'
						},
						xAxis : {
							categories : siteCodes

						},
						yAxis : {
							min : 0,
							title : {
								text : 'Events Count'
							}
						},
						title : {
							text : ''
						},
						subtitle : {
							text : ''
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{categories.name} </td>'
									+ '<td style="padding:0"><b>{point.y}</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.4,
								borderWidth : 0,
								events : {
									legendItemClick : function() {
										return false;
									}
								}
							},
							series : {

								cursor : 'pointer',
								point : {
									events : {

										click : function() {

											ajax_request3 = $
													.ajax({
														type : 'GET',
														url : './siteEvents?siteRedIf='
																+ this.category,
														success : function(msg) {

															$('#example tbody')
																	.empty();
															$('#example')
																	.DataTable()
																	.clear()
																	.draw();
															$('#example')
																	.DataTable()
																	.destroy();
															var eventdetaillist = msg.eventsgriddata;
															// var
															// eventdetaillist =
															// eval("[" +
															// dataTableData +
															// "]");
															for (var i = 0; i < eventdetaillist.length; i++) {

																var prioritytext = "Low"
																var priority = eventdetaillist[i].priority;

																if (priority == 1) {
																	prioritytext = "Low";
																} else if (priority == 2) {
																	prioritytext = "Medium";
																} else if (priority == 3) {
																	prioritytext = "High";
																}

																var action = '<div class="dropdown"><button  data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu dropevent" role="menu" aria-labelledby="dLabel" > <li  class="evtCol" data-toggle="modal" data-target="#evntCreation" data-backdrop="static" data-keyboard="false"><a href="#"><span id="evt">  Ticket</span></a></li> <li><a class="btnDeleteRecord" onclick="supresseventdetails('
																		+ eventdetaillist[i].transactionId
																		+ ')">Suppress</a></li></ul></div>';

																$('#example')
																		.DataTable().row
																		.add(
																				[
																						eventdetaillist[i].errorCode,
																						eventdetaillist[i].errorMessage,
																						eventdetaillist[i].eventTimestampText,
																						eventdetaillist[i].lastEventTimestampText,
																						eventdetaillist[i].eventOccurrence,
																						eventdetaillist[i].siteName,
																						eventdetaillist[i].equipmentName,
																						eventdetaillist[i].equipmentType,
																						prioritytext,
																						eventdetaillist[i].siteID,
																						eventdetaillist[i].priority,
																						eventdetaillist[i].equipmentId,
																						eventdetaillist[i].transactionId,
																						action ])
																		.draw(
																				false);

																// style="width:7%;display:none;"
															}

															$('#example')
																	.DataTable()
																	.columns(
																			[
																					9,
																					10,
																					11,
																					12 ])
																	.visible(
																			false);

														}
													});

										}
									}
								}

							}
						},
						series : [ {
							name : 'SiteId',
							data : siteCounts

						} ]
					});

}
