/** Current Day ***/
		$('#daily').click(function() {
			Daily();
			$('#hdnSelectedDataRange').val('daily');
			ViewReport();
		})

		/** Current Week **/
		$('#weekly').click(function() {
			Weekly();
			$('#hdnSelectedDataRange').val('weekly');
			ViewReport();
		})

		/** Current Month ***/
		$('#monthly').click(function() {
			Monthly();
			$('#hdnSelectedDataRange').val('monthly');
			ViewReport();
		})

		/** Current Year ***/
		$('#yearly').click(function() {
			Yearly();
			$('#hdnSelectedDataRange').val('yearly');
			ViewReport();
		})



		/** Custom ***/
		$('#custom').click(function() {
			Custom();
			$('#hdnSelectedDataRange').val('custom');

		})


		$('#CurrentVal').click(function() {
			ViewReport();
		})
		
		$('#btnApply').click(function() {
			ViewReport();
		})


		$('#fromDate').on('focusout', function() {
			try {
				var fromdate = $.datepicker.parseDate('mm/dd/yy', $('#fromDate').val());
				if ($('#fromDate').val() == '') {
					$('#fromDate').val($('#toDate').val());
				}
			} catch (err) {
				$('#fromDate').val($('#toDate').val());
			}
			$('#custom').click();

		})



		$('#toDate').on('focusout', function() {
			try {
				var todate = $.datepicker.parseDate('mm/dd/yy', $('#toDate').val());
				if ($('#toDate').val() == '') {
					$('#toDate').val($('#fromDate').val());
				}

			} catch (err) {
				$('#toDate').val($('#fromDate').val());
			}
			$('#custom').click();
		})

		
