function LoadChartOnDashboard() {
	var totalsites = $('#totalsites').text();
	var RooftopCount = $('#rooftop').text();
	var UtilityCount = $('#utilitycount').text();

	var active = $('#ActiveCount').text();
	var warning = $('#WarningCount').text();
	var offline = $('#OfflineCount').text();
	var down = $('#DownCount').text();

	var varRooftopCount = eval($('#rooftop').text());
	var varUtilityCount = eval($('#utilitycount').text());



	Highcharts
			.chart(
					'chart',
					{
						chart : {
							plotBackgroundColor : null,
							plotBorderWidth : null,
							plotShadow : false,
							type : 'pie',

						},

						title : {
							text : ''
						},
						tooltip : {

							pointFormat : '<b>{point.y}</b>'

						},
						credits : {
							enabled : false
						},
						plotOptions : {
							pie : {
								innerSize : 50,
								depth : 15,
								allowPointSelect : true,
								cursor : 'pointer',
								dataLabels : {
									enabled : false,
									distance : 0,
									format : '<b>{point.name}</b>: {point.percentage:.1f} %',
									style : {
										color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
												|| 'black'
									}
								}
							}
						},
						series : [ {
							name : '',
							colorByPoint : true,

							data : [ {
								name : 'Rooftop',
								y : varRooftopCount,
								color : '#6a95a5',

								sliced : true,
								selected : true
							}, {
								name : 'Utility',

								y : varUtilityCount,
								color : '#164725'

							} ]
						} ]
					});

}

function handlenullable(param) {
	if (param == null || param == 'null') {
		return '';
	} else {
		return param;
	}

}

function initializeStatusChart(){
	var varActiveCount = eval($('#ActiveCount').text());
	var varWarningCount = eval($('#WarningCount').text());
	var varDownCount = eval($('#DownCount').text());
	var varOfflineCount = eval($('#OfflineCount').text());

	Highcharts.chart('statuschart', {
		chart : {
			plotBackgroundColor : null,
			plotBorderWidth : null,
			plotShadow : false,
			type : 'pie',

		},

		title : {
			text : ''
		},
		tooltip : {

			pointFormat : '<b>{point.y}</b>'

		},
		credits : {
			enabled : false
		},
		plotOptions : {
			pie : {
				allowPointSelect : true,
				cursor : 'pointer',
				dataLabels : {
					enabled : false,
					distance : 1,
				/* format: '<b>{point.name}</b>: {point.percentage:.1f} %', */
				/*
				 * style: { color: (Highcharts.theme &&
				 * Highcharts.theme.contrastTextColor) || 'black' }
				 */
				}
			}
		},
		series : [ {
			name : '',
			colorByPoint : true,

			data : [ {
				name : 'Active',
				y : varActiveCount,
				color : '#5bc102',
				sliced : true,
				selected : true
			}, {
				name : 'Warning',
				y : varWarningCount,
				color : '#ffd600'
			}, {
				name : 'Down',
				y : varDownCount,
				color : '#f03c3d'
			}, {
				name : 'Offline',
				y : varOfflineCount,
				color : '#6a6c6f'
			} ]
		} ]
	});
}
function AjaxCallForDashBoardDetails() {

	$
			.ajax({
				type : 'GET',
				url : './dashboardsitedetail',
				success : function(msg) {
					debugger;

					var dashboardSiteList = msg.dashboardSiteList;

					$('#example').DataTable().clear().draw();
					$('#example').DataTable().destroy();

					var ticketrows = "";

					var activeCount = 0;

					var warningCount = 0;

					var downCount = 0;

					var offlineCount = 0;

					for (var i = 0; i < dashboardSiteList.length; i++) {

						/*
						 * var sitecode='<a href="./siteview' +
						 * handlenullable(dashboardSiteList[i].siteID) + '"
						 * class="newlink">' +
						 * handlenullable(dashboardSiteList[i].siteCode) + '</a>';
						 */var siteref = '<a href="./siteview'
								+ handlenullable(dashboardSiteList[i].siteID)
								+ '" class="newlink">'
								+ handlenullable(dashboardSiteList[i].siteReference)
								+ '</a>';
						var sitename = '<a href="./siteview'
								+ handlenullable(dashboardSiteList[i].siteID)
								+ '" class="newlink">'
								+ handlenullable(dashboardSiteList[i].siteName)
								+ '</a>';

						var sitestatus = '<div class="off"></div>&nbsp;Offline';

						if (dashboardSiteList[i].networkStatus == 'Offline') {
							sitestatus = '<div class="offline"></div>&nbsp;Offline';
							offlineCount++;
						} else if (dashboardSiteList[i].networkStatus == 'Active') {
							sitestatus = '<div class="act"></div>&nbsp;Active';
							activeCount++;
						} else if (dashboardSiteList[i].networkStatus == 'Down') {
							sitestatus = '<div class="erro"></div>&nbsp;Down';
							downCount++;
						} else if (dashboardSiteList[i].networkStatus == 'Warning') {
							sitestatus = '<div class="warn"></div>&nbsp;Warning';
							warningCount++;
						}

						var todayenergy = handlenullable(dashboardSiteList[i].todayEnergy);
						var totalenergy = handlenullable(dashboardSiteList[i].totalEnergy);
						var performanceratio = handlenullable(dashboardSiteList[i].performanceRatio);
						var inverters = handlenullable(dashboardSiteList[i].inverters);
						var meters = handlenullable(dashboardSiteList[i].ems);

						var lastupdate = handlenullable(dashboardSiteList[i].lastUpdate);
						var sitetype = handlenullable(dashboardSiteList[i].siteTypeID);
						var capacity = handlenullable(dashboardSiteList[i].installationCapacity);
						
						if(lastupdate){
							var preSortDate=lastupdate.split(" ");
							var sortDate=preSortDate[0].split('-').reverse().join('')+preSortDate[1].replace(':','');
						}

						if (i == 0) {

							ticketrows = "<tr><td>" + siteref + "</td><td>"
									+ sitetype + "</td><td>" + sitename
									+ "</td><td>" + sitestatus + "</td><td>"
									+ todayenergy + "</td><td>" + totalenergy
									+ "</td><td>" + performanceratio
									+ "</td><td>" + inverters + "</td><td>"
									+ meters + "</td><td>" + capacity
									+ "</td><td><span>"+sortDate+"</span>"+ lastupdate + "</td></tr>";
						} else {

							ticketrows = ticketrows + "<tr><td>" + siteref
									+ "</td><td>" + sitetype + "</td><td>"
									+ sitename + "</td><td>" + sitestatus
									+ "</td><td>" + todayenergy + "</td><td>"
									+ totalenergy + "</td><td>"
									+ performanceratio + "</td><td>"
									+ inverters + "</td><td>" + meters
									+ "</td><td>" + capacity + "</td><td><span>"+sortDate+"</span>"
									+ lastupdate + "</td></tr>";

						}

					}

					
					$('#DownCount').text(downCount);
					
					$('#WarningCount').text(warningCount);
					
					$('#OfflineCount').text(offlineCount);
					
					$('#ActiveCount').text(activeCount);
					
					initializeStatusChart();
					
					$('#example').append(ticketrows);

					$('#example').DataTable(
							{
								"order" : [ [ 3, "asc" ], [ 9, 'asc' ] ],
								"lengthMenu" : [ [ 5, 10, 25, 50, -1 ],
										[ 5, 10, 25, 50, "All" ] ],
								"bLengthChange" : true,
							});

				},
				error : function(msg) {

					// alert(0);
					// alert(msg);
				}
			});

}
