

function GetDateFromString(dateString)
{
    var dateParts = dateString.split("/");
	var day = dateParts[0];
	var month = dateParts[1] - 1;
	
	day = day.toString().padStart(2,"0");
	month = month.toString().padStart(2,"0");
	 
    return new Date(dateParts[2],month, day); // month is 0-based
  
}


function CheckParameter(idParameterCheckBox)
{		    	
	if ($('#' + idParameterCheckBox).prop('checked')) {		
		$('#' + idParameterCheckBox).prop('checked', false); // Checks it			
	} 
	else {
		$('#' + idParameterCheckBox).prop('checked', true); // Unchecks it
	}
	
	
	var idParameter = idParameterCheckBox;
	
	var chktext = $('#lbl' + idParameter).text();
	chktext = $.trim(chktext.replace(/[\t\n]+/g, ' '));

	var filterid = "filterparam" + idParameter;
	var filterclodeid = "filterparamclose" + idParameter;
	
	
	
	if ($('#' + idParameter).prop('checked')) {
		
		if (window[filterid]) {
			//no need of adding new node
		} else {
			var newNode = '<div id="' + filterid + '" class="alert alert-info info-window parameterfilterdivclose" role="alert" title="' + chktext + '"><button id="' + filterclodeid + '" class="close info-close-window parameterfilterclose" data-dismiss="alert"></button>' + chktext + '</div>';
			$('.ParameterFilterList').append(newNode);
			$('.Sele').show();
			$('.closeAll').show();
		}
		
		
	} else {
		$('#' + filterid).remove();
		
		if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 0) {
			$('.Sele').hide();
			$('.closeAll').hide();
		}

	}
	
	
	
	
	if ($('#ddlGraphType').val() == 2) {
		if ($('.ParameterFilterList').children().length == 0)
		{
			
		$('#btnView').attr("disabled",true);
		}
		else
		{
			
		$('#btnView').attr("disabled",false);
		}
	}

	
}



function GetStringFromDate(GivenDate)
{
	day = GivenDate.getDate().toString().padStart(2,"0");
	month = (GivenDate.getMonth()+1).toString().padStart(2,"0");
	   	
	return  day  + '/' +   month + '/' +  GivenDate.getFullYear();
	
}

function myFunctionPlus() {

	if($('#hdnSelectedDataRange').val() == 'daily')
	{
		var dateString = document.getElementById("CurrentVal").innerHTML; // Oct 23
	    var d = GetDateFromString(dateString);
	    
	    d.setDate(d.getDate() + 1);	    
	    var todayDate = new Date();
		   
	    if(d==todayDate)
	    {
	    	$('.btn-rhs').prop('disabled',true);
	    }
	    
	    if( d <= todayDate)
	    {
	    	var n = GetStringFromDate(d);
	 	    document.getElementById("CurrentVal").innerHTML = n;
	 	    $('#fromDate').val(n);
	 	    $('#toDate').val(n);
	    }
	    
	}
	else if($('#hdnSelectedDataRange').val() == 'weekly')
	{
		var fromDateString = $('#fromDate').val();
		var fromDate = GetDateFromString(fromDateString);
		var toDate = GetDateFromString(fromDateString);
	    fromDate.setDate(fromDate.getDate() + 7);
	    toDate.setDate(toDate.getDate() + 13);
	  
	    var week = GetWeekOfYear(fromDate);
	    var year = GetYear(fromDate);
	    	    
	    var todayDate = new Date();
	    if(fromDate >= todayDate)
	    {
	    	$('.btn-rhs').prop('disabled',true);
	    }
	    
	    if( fromDate <= todayDate)
	    {
	    	 var n =  year + ', Week-' +  week;
			 var fd = GetStringFromDate(fromDate);
			 var td = GetStringFromDate(toDate);
			 document.getElementById("CurrentVal").innerHTML = n;
			 $('#fromDate').val(fd);
			
			 if(toDate > todayDate)
			    {
				 td = GetStringFromDate(todayDate);
			    }
			
			 $('#toDate').val(td);
	    }
	    
		
	}
	else if($('#hdnSelectedDataRange').val() == 'monthly')
	{
		var fromDateString = $('#fromDate').val();
		var fromDate = GetDateFromString(fromDateString);
		fromDate.setMonth(fromDate.getMonth() + 1);
		 
		var n = GetMonth(fromDate);
		
		var firstDay = new Date(fromDate.getFullYear(), fromDate.getMonth(), 1);
		var lastDay = new Date(fromDate.getFullYear(), fromDate.getMonth() + 1, 0);
		
		var fd = GetStringFromDate(firstDay);
		var td = GetStringFromDate(lastDay);

		var todayDate = new Date();
		    
		if(firstDay >= todayDate)
		{
			$('.btn-rhs').prop('disabled',true);
		}
		    
		if( firstDay <= todayDate)
		{
			document.getElementById("CurrentVal").innerHTML = n;
			$('#fromDate').val(fd);

			if(lastDay > todayDate)
			{
				td = GetStringFromDate(todayDate);
			}

			$('#toDate').val(td);
		}
		
	}
	else if($('#hdnSelectedDataRange').val() == 'yearly')
	{
		var dateString = document.getElementById("CurrentVal").innerHTML; // Oct 23
		dateString = dateString - 1 + 2;
		  
		var todayDate = new Date();
		if(dateString >= todayDate.getFullYear())
		{
			$('.btn-rhs').prop('disabled',true);
		}

		if( dateString <= todayDate.getFullYear())
		{
			document.getElementById("CurrentVal").innerHTML = dateString; 
			var firstDay = new Date(dateString, 0, 1);
			var fd = GetStringFromDate(firstDay);
			$('#fromDate').val(fd);	

			var lastDay = new Date(dateString - 1 + 2 , 0, 0);			

			if( dateString == todayDate.getFullYear())
			{
				lastDay = todayDate;
			}

			var td = GetStringFromDate(lastDay);
			$('#toDate').val(td);


		}
		    
		    
		    
	}
	else if($('#hdnSelectedDataRange').val() == 'custom')
	{
		var fromDateString = $('#fromDate').val(); 
		var toDateString = $('#toDate').val(); 
		var fd = GetDateFromString(fromDateString);
		var td = GetDateFromString(toDateString);
		
		var days =  GetNumberOfDays(fd, td);
		
		var firstDay = fd.setDate(fd.getDate() -1 + 1 + days);
		var lastDay = td.setDate(td.getDate() -1 + 1 + days);
		 
		 $('#fromDate').val(GetStringFromDate(firstDay));	
		 $('#toDate').val(GetStringFromDate(lastDay));
	}
	
}

function myFunctionMins() {
	
	$('.btn-rhs').prop('disabled',false);
	
	if($('#hdnSelectedDataRange').val() == 'daily')
	{
	
		var dateString = document.getElementById("CurrentVal").innerHTML; // Oct 23
		var d = GetDateFromString(dateString);
		d.setDate(d.getDate() - 1);

		day = d.getDate().toString().padStart(2,"0");
		month = (d.getMonth()+1).toString().padStart(2,"0");

		var n =  day + '/' +  month  + '/' +  d.getFullYear();

		document.getElementById("CurrentVal").innerHTML = n;
		$('#fromDate').val(n);
		$('#toDate').val(n);

	}
	else if($('#hdnSelectedDataRange').val() == 'weekly')
	{
		var fromDateString = $('#fromDate').val();
		var fromDate = GetDateFromString(fromDateString);
		var toDate = GetDateFromString(fromDateString);
	    fromDate.setDate(fromDate.getDate() - 7);
	    toDate.setDate(toDate.getDate() - 1);
	  
	    var week = GetWeekOfYear(fromDate);
	    var year = GetYear(fromDate);
	    
	    
		 var n =  year + ', Week-' +  week;
		 var fd = GetStringFromDate(fromDate);
		 var td = GetStringFromDate(toDate);
		 document.getElementById("CurrentVal").innerHTML = n;
		 $('#fromDate').val(fd);
		 $('#toDate').val(td);
	}
	else if($('#hdnSelectedDataRange').val() == 'monthly')
	{
		var fromDateString = $('#fromDate').val();
		var fromDate = GetDateFromString(fromDateString);
		 fromDate.setMonth(fromDate.getMonth() - 1);
		 
		var n = GetMonth(fromDate);
		document.getElementById("CurrentVal").innerHTML = n;
		
		var firstDay = new Date(fromDate.getFullYear(), fromDate.getMonth(), 1);
		var lastDay = new Date(fromDate.getFullYear(), fromDate.getMonth() + 1, 0);
		
		 var fd = GetStringFromDate(firstDay);
		 var td = GetStringFromDate(lastDay);
		$('#fromDate').val(fd);
		$('#toDate').val(td);
	}
	else if($('#hdnSelectedDataRange').val() == 'yearly')
	{
		var dateString = document.getElementById("CurrentVal").innerHTML; // Oct 23
		dateString = dateString - 1;
		document.getElementById("CurrentVal").innerHTML = dateString; 
		
		var firstDay = new Date(dateString, 0, 1);
		var lastDay = new Date(dateString - 1 + 2 , 0, 0);

		 var fd = GetStringFromDate(firstDay);
		 var td = GetStringFromDate(lastDay);
		 
		 $('#fromDate').val(fd);	
		 $('#toDate').val(td);
	}
	else if($('#hdnSelectedDataRange').val() == 'custom')
	{
		var fromDateString = $('#fromDate').val(); 
		var toDateString = $('#toDate').val(); 
		

		 var fd = GetDateFromString(fromDateString);
		 var td = GetDateFromString(toDateString);
		
		var days =  GetNumberOfDays(fd, td);
		
		
		
		var firstDay = fd.setDate(fd.getDate() - days);
		var lastDay = td.setDate(td.getDate() - days);

		 
		 $('#fromDate').val(GetStringFromDate(firstDay));	
		 $('#toDate').val(GetStringFromDate(lastDay));
	}
}
     


function GetNumberOfDays(date1, date2 )
{
		var one_day=1000*60*60*24;    // Convert both dates to milliseconds
		var date1_ms = date1.getTime();   
		var date2_ms = date2.getTime();    // Calculate the difference in milliseconds  
		 var difference_ms = date2_ms - date1_ms;        // Convert back to days and return   
		return Math.round(difference_ms/one_day); 	
}

function getMondayOfCurrentWeek(d)
{
    var day = d.getDay();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate() + (day == 0?-6:1)-day );
}


function getSundayOfCurrentWeek(d)
{
    var day = d.getDay();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate() + (day == 0?0:7)-day );
}


function Daily()
{
	var date = new Date();
 	var localdate= date.getDate().toString().padStart(2,"0") + '/' + (date.getMonth()+1).toString().padStart(2,"0") + '/' +  date.getFullYear();

 	 $('#CurrentVal').html(localdate); 	
 	 $('#fromDate').val(localdate); 
 	 $('#toDate').val(localdate); 	
}



Date.prototype.getWeek = function (dowOffset) {
/*getWeek() was developed by Nick Baicoianu at MeanFreePath: http://www.meanfreepath.com */

    dowOffset = typeof(dowOffset) == 'int' ? dowOffset : 0; //default dowOffset to zero
    var newYear = new Date(this.getFullYear(),0,1);
    var day = newYear.getDay() - dowOffset; //the day of week the year begins on
    day = (day >= 0 ? day : day + 7);
    var daynum = Math.floor((this.getTime() - newYear.getTime() - 
    (this.getTimezoneOffset()-newYear.getTimezoneOffset())*60000)/86400000) + 1;
    var weeknum;
    //if the year starts before the middle of a week
    if(day < 4) {
        weeknum = Math.floor((daynum+day-1)/7) + 1;
        if(weeknum > 52) {
            nYear = new Date(this.getFullYear() + 1,0,1);
            nday = nYear.getDay() - dowOffset;
            nday = nday >= 0 ? nday : nday + 7;
            /*if the next year starts before the middle of
              the week, it is week #1 of that year*/
            weeknum = nday < 4 ? 1 : 53;
        }
    }
    else {
        weeknum = Math.floor((daynum+day-1)/7);
    }
    return weeknum;
};


function GetWeekOfYear(GivenDate)
{
	return GivenDate.getWeek();
}


function Weekly()
{
	    var today = new Date();
	    var weekno = today.getFullYear() +  ', Week-' + GetWeekOfYear(today);
	    $('#CurrentVal').html(weekno); 
	    
	    var date = new Date();
	    var fromdate =  getMondayOfCurrentWeek(date);
	    var startdate= fromdate.getDate().toString().padStart(2,"0") + '/' + (fromdate.getMonth()+1).toString().padStart(2,"0") + '/' +  fromdate.getFullYear();
		var todaydate= date.getDate().toString().padStart(2,"0") + '/' + (date.getMonth()+1).toString().padStart(2,"0") + '/' +  date.getFullYear();

	    $('#fromDate').val(startdate); 
	 	$('#toDate').val(todaydate); 
	 	 
}


function Monthly()
{
	var objDate = new Date();
	var n = GetMonth(objDate);
	$('#CurrentVal').html(n);
	
		
	var firstDay = new Date(objDate.getFullYear(), objDate.getMonth(), 1);
	var fd = GetStringFromDate(firstDay);
	var td = GetStringFromDate(objDate);
		 
	$('#fromDate').val(fd);	
	$('#toDate').val(td);
		 
 	$('.btn-rhs').prop('disabled', true);
 	 	
}

function Yearly()
{
	var CurrentYear = new Date().getFullYear();
	$('#CurrentVal').html(CurrentYear);

	var firstDay = new Date(CurrentYear, 0, 1);	 
	var todaydate = new Date();

	var fd = GetStringFromDate(firstDay);
	var td = GetStringFromDate(todaydate);

	$('#fromDate').val(fd);	
	$('#toDate').val(td);
	$('.btn-rhs').prop('disabled', true);	 
		 
}

function Custom()
{	
	$('#CurrentVal').html('Custom Range');
	
	var fd = GetDateFromString($('#fromDate').val());
	var td = GetDateFromString($('#toDate').val());
	 
 	$('.btn-rhs').prop('disabled', true);
}

function GetMonth(GivenDate)
{
	var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]; 
	var year = GivenDate.getFullYear(); 
	var month = monthNames[GivenDate.getMonth()]; 	
	return year + ", " + month;
}

function GetYear(GivenDate)
{
	 return GivenDate.getFullYear();
}





function CustomerChange()
{
	$('#chkSites').prop('checked', false); // Unchecks it                                

	var $inputsOK = $('#ddfSite').find('input');
	var $inputsNotOK = $('#ddfSite').find('input');
	var $inputsInv = $('#ddfInverter').find('input');


	for(var i=0;i< $inputsNotOK.length; i++) {
		var chk =  $inputsNotOK[i].id;
		$('#div' + chk).css('display','none');
		$('#' + chk).prop('checked', false); // Unchecks it
	}

	for(var i=0;i< $inputsOK.length; i++) {
		var chk =  $inputsOK[i].id;
		$('#div' + chk).css('display','block');                                        
	}



	for(var i=0;i< $inputsInv.length; i++) {
		var chk =  $inputsInv[i].id;
		$('#div' + chk).css('display','none');

	}

	var isInvAvailable = false;
	for(var i=0;i< $inputsInv.length; i++) {
		var chk =  $inputsInv[i].id;
		if($('#div' + chk).css('display') == 'block') {
			isInvAvailable = true;
		}                                           
	}




	var $inputsSite = $('#ddfSite').find('input');
	var defaultCheck = false;
	for(var i=0;i< $inputsSite.length; i++)	{
		var chk =  $inputsSite[i].id;
		if(defaultCheck == false && $('#div' + chk).css('display') == 'block')	{ 	
			$('#' + chk).prop('checked', true); // Checks it
			SiteCheckedChange(chk);
			defaultCheck = true;
		}
	}

	if(isInvAvailable == false)  
		$('#divNoDataFoundInv').show();
	else
		$('#divNoDataFoundInv').hide();



}






function SiteCheckedChange(idSite)
{
	debugger;
	var SiteId = $('#' + idSite).attr('data-value');                                
	/* $('#chkInverters').prop('checked', true); // Checks it
			$('#chkSensors').prop('checked', true); // Checks it
	 */
	var $inputsInvOK = $('#ddfInverter').find('input').filter('[data-site="' + SiteId + '"]');
	var $inputsInvNotOK = $('#ddfInverter').find('input').filter('[data-site!="' + SiteId + '"]');


	var $inputsInv = $('#ddfInverter').find('input');


	if( $('#' + idSite).prop('checked')) {
		for(var i=0;i< $inputsInvOK.length; i++) {
			var chk =  $inputsInvOK[i].id;
			$('#div' + chk).css('display','block');
			$('#chkInverters').prop('checked', false);
			//$('#' + chk).prop('checked', true); // Checks it
		}

	}
	else
	{
		for(var i=0;i< $inputsInvOK.length; i++) {
			var chk =  $inputsInvOK[i].id;
			$('#div' + chk).css('display','none');
			$('#' + chk).prop('checked', false); // Unchecks it
			$('#chkInverters').prop('checked', false);
			$('#filterequ' + chk).remove();
			
			
		}
	}


	var isInvAvailable = false;
	for(var i=0;i< $inputsInv.length; i++) {
		var chk =  $inputsInv[i].id;
		if($('#div' + chk).css('display') == 'block') {
			isInvAvailable = true;
		}
	}


	if(isInvAvailable == false)  
		$('#divNoDataFoundInv').show();
	else
		$('#divNoDataFoundInv').hide();



}




function ViewReport() {
	 
	 debugger;
	//var CustomerId = $('#ddfCustomer').val();
	var $inputsSite = $('#ddfSite').find('input').filter(':checked');
		var $inputsInverter = $('#ddfInverter').find('input').filter(':checked');
		var $inputsParameter = $('#ddfParameter').find('input').filter(':checked');
		
		
		var varSiteList = '';
		for(var i=0;i< $inputsSite.length; i++) {
		var chk =  $inputsSite[i].id;
       var DataVal= $('#' + chk).attr('data-value');
       if(i==0) {
       	varSiteList = DataVal; 
       }
       else {
       	varSiteList = varSiteList + ',' + DataVal;
        }
    }


var varInverterList = '';
for(var i=0;i< $inputsInverter.length; i++) {
	var chk =  $inputsInverter[i].id;
   var DataVal= $('#' + chk).attr('data-value');
   if(i==0) {
  		varInverterList = DataVal; 
   }
   else {
   	varInverterList = varInverterList + ',' + DataVal;
   }
}


var varParameterList = '';
for(var i=0;i< $inputsParameter.length; i++) {
	var chk =  $inputsParameter[i].id;
	 var DataVal= $('#' + chk).attr('data-value');
   if(i==0) {
   	varParameterList = DataVal; 
   }
   else {
   	varParameterList = varParameterList + ',' + DataVal;
   }
}



// var radioValue = $("input[name='GraphType']:checked").val();
var radioValue = $('#ddlGraphType').val();
debugger;
/*
if(radioValue=='Energy Performance' && varInverterList == '')
{
  radioValue ='Daily Energy Generation';
   $('#lirdoEnergyPerformance').removeClass('tf-selected');
   $('#lirdoParameterComparison').removeClass('tf-selected');
   $('#lirdoDailyEnergy').addClass('tf-selected');                                                     
		$('#hdnGraphType').val(radioValue);
		document.getElementById('lblReportName').innerHTML = radioValue;
}
else if(radioValue=='Energy Performance' &&  varInverterList != '' &&  varParameterList != '')
{
		radioValue ='Parameter Comparison';
   $('#lirdoDailyEnergy').removeClass('tf-selected');
  	$('#lirdoEnergyPerformance').removeClass('tf-selected');  
   $('#lirdoParameterComparison').addClass('tf-selected');                                   
		$('#hdnGraphType').val(radioValue);  
		document.getElementById('lblReportName').innerHTML = radioValue;                   
}
else if(radioValue=='Daily Energy Generation' &&  varInverterList != '' &&  varParameterList != '')
{
		radioValue ='Parameter Comparison';
   $('#lirdoDailyEnergy').removeClass('tf-selected');
  	$('#lirdoEnergyPerformance').removeClass('tf-selected');  
   $('#lirdoParameterComparison').addClass('tf-selected');                                   
		$('#hdnGraphType').val(radioValue);  
		document.getElementById('lblReportName').innerHTML = radioValue;                   
}
else if(radioValue=='Daily Energy Generation' &&  varInverterList != '')
{
		radioValue ='Energy Performance';
   $('#lirdoDailyEnergy').removeClass('tf-selected');
   $('#lirdoParameterComparison').removeClass('tf-selected');
  	$('#lirdoEnergyPerformance').addClass('tf-selected');                                     
		$('#hdnGraphType').val(radioValue);  
		document.getElementById('lblReportName').innerHTML = radioValue;                   
}
else if(radioValue=='Parameter Comparison' &&  varInverterList != '' && varParameterList == '')
{
  radioValue ='Energy Performance';
  $('#lirdoDailyEnergy').removeClass('tf-selected');
  $('#lirdoParameterComparison').removeClass('tf-selected');
 	$('#lirdoEnergyPerformance').addClass('tf-selected');                                     
		$('#hdnGraphType').val(radioValue);  
		document.getElementById('lblReportName').innerHTML = radioValue;   
		
		         
}
else if(radioValue=='Parameter Comparison' &&  varInverterList == '' )
{
  radioValue ='Daily Energy Generation';
  $('#lirdoEnergyPerformance').removeClass('tf-selected');
  $('#lirdoParameterComparison').removeClass('tf-selected');
  $('#lirdoDailyEnergy').addClass('tf-selected');                                                     
		$('#hdnGraphType').val(radioValue);
		document.getElementById('lblReportName').innerHTML = radioValue;
		
		         
}*/
		
	



			AjaxCall(varSiteList,varInverterList,'','',$('#hdnSelectedDataRange').val(),$('#fromDate').val(),$('#toDate').val(),radioValue);

}


/*
function AjaxCallForEquipmentBySiteID(SiteID)
{
	
	debugger;
	if(varInverterList != '') {		
		 $.ajax({
             type: 'POST',
             url: './filterequipments',
             data: {                            
                 'SiteID': SiteID  
             },
             success: function(msg){
            	
            	 debugger;
            	 
            	//$('#ddlEquipment').empty();	
            	$('#dropdown').empty();	
            	
     			//$('.EquipmentFilterList').empty();	
     			
     		    
     		    if ($('#ddlGraphType').val() == 2) {
     				$('#btnView').attr("disabled",true);
     			}
     			
            	
           	  var equipments = msg.equipments;
           	  for(var i=0;i < equipments.length; i++)
           	  {
           		  var equipments = equipments[i];
           		  var equipmentvalue = equipments[1];
           		  var equipmentdesc =equipments[2];
             	  var filterid = "divchkparam" + equipmentvalue;
             	
             	  
             	 var NewNode = '<option id="' + filterid + '" value="' + equipmentvalue + '">' + equipmentdesc + '</option>';
             	
             	 
             	 
             	  //var NewNode = '<div onclick="CheckParameter('+ parameterqoute +')" id="' + filterid + '"	class="ui item checkbox divchkparameter" data-value="' + parametervalue +'">  <input type="checkbox" id="chkparam' + parametervalue + '"  class="chkparameter" data-value="' + parametervalue + '">  <label id="lblchkparam' + parametervalue + '">' + parameterdesc + '</label> </div>';
           		 
             	
           		
				
				if (window[filterid]) {
					//no need of adding new node
				} else {
					
					 var categCheck  = $('#dropdown').multiselect({
		        		    includeSelectAllOption: true,
		        		     enableFiltering : true
		        		}).multiselectfilter();
		        			 $('#dropdown').append(NewNode);
		        			categCheck.multiselect('refresh');
		        	
		        			
		        	var categCheck1  = $('#dropdown1').multiselect({
		        		includeSelectAllOption: true,
		        		includeSelectAllText: true,
		        		enableFiltering : true
		        	}).multiselectfilter();
		        		$('#dropdown1').append('<optgroup label="Group Three"><option value="option8">Option 8</option><option value="option9">Option 9</option><option value="option10">Option 10</option><option value="option11">Option 11</option></optgroup>');
					categCheck1.multiselect('refresh');
					
					
				
				}
				
				
				
           		
           	  
           	  }
             }, 
             error: function(msg){
         
            	 debugger;
             }
		 });
	}
	else
		{
		 
    		//$('#ddlEquipment').empty();	
    		$('#dropdown').empty();	    	
			//$('.EquipmentFilterList').empty();	
			
			if ($('#ddlGraphType').val() == 2) {
 				$('#btnView').attr("disabled",true);
 			}
		}
	
   
}

*/




function AjaxCallForStandardParameters()
{
	debugger;
	var varInverterList = '';
	
	var $inputsInverter = $('#ddfInverter').find('input').filter(':checked');
	
	for(var i=0;i< $inputsInverter.length; i++) {
		var chk =  $inputsInverter[i].id;
	   var DataVal= $('#' + chk).attr('data-value');
	   if(i==0) {
	  		varInverterList = DataVal; 
	   }
	   else {
	   	varInverterList = varInverterList + ',' + DataVal;
	   }
	}

	debugger;
	if(varInverterList != '') {		
		 $.ajax({
             type: 'POST',
             url: './filterstandardparameters',
             data: {                            
                 'EquipmentIDList': varInverterList  
             },
             success: function(msg){
            	 //alert(msg);
            	 debugger;
            	 
            	$('#ddfParameter').empty();	
     			$('.ParameterFilterList').empty();	
     			
     			if ($('#ddlGraphType').val() == 2) {
     				$('#btnView').attr("disabled",true);
     			}
            	
           	  var parameters = msg.parameters;
           	  for(var i=0;i < parameters.length; i++)
           	  {
           		  var parameter = parameters[i];
           		  var parametervalue = parameter[1];
           		  var parameterqoute="'chkparam" + parametervalue + "'";
           		  var parameterdesc =parameter[2];
             	  var filterid = "divchkparam" + parametervalue;
             	 //var NewNode = '<div id="' + filterid + '"	class="ui item checkbox divchkparameter" data-value="' + parametervalue +'">  <input type="checkbox" id="chkparam' + parametervalue + '"  class="chkparameter" data-value="' + parametervalue + '">  <label id="lblchkparam' + parametervalue + '">' + parameterdesc + '</label> </div>';
             	 var NewNode = '<div onclick="CheckParameter('+ parameterqoute +')" id="' + filterid + '"	class="ui item checkbox divchkparameter" data-value="' + parametervalue +'">  <input type="checkbox" id="chkparam' + parametervalue + '"  class="chkparameter" data-value="' + parametervalue + '">  <label id="lblchkparam' + parametervalue + '">' + parameterdesc + '</label> </div>';
           		 
             	 //var NewNode = '<div onclick="chkparamevalue('+ parameterqoute +')" id="' + filterid + '"	class="ui item checkbox divchkparameter" data-value="' + parametervalue +'">  <input type="checkbox" id="chkparam' + parametervalue + '"  class="chkparameter" data-value="' + parametervalue + '">  <label id="lblchkparam' + parametervalue + '">' + parameterdesc + '</label> </div>';
           		 // var NewNode0 = '<option id="' + filterid + '" value="' + parametervalue + '">' + parameterdesc + '</option>';
           		  //var NewNode1 = '<div class="sol-option"> <label class="sol-label"> <input id="' + filterid + '" type="checkbox" class="sol-checkbox" name="character" value="' + parametervalue +'"> <div class="sol-label-text">' + parameterdesc + '</div> </label> </div>';
           		
           		
				//$('<option>').val(parametervalue).text(parameterdesc).appendTo('#ddlParameter');
				//$('#ddlParameter').append(new Option(parameterdesc, parametervalue));
           		
				
				if (window[filterid]) {
					//no need of adding new node
				} else {
					
					$('#ddfParameter').append(NewNode);
				///	$('#ddfParameter').trigger();
					 //$('#ddlParameter').append(NewNode0);
					 //$('#ddlParameterList').append(NewNode1);
					//addOption('ddlParameter',parametervalue);
				}
				
           		
           	  
           	  }
             }, 
             error: function(msg){
         
            	 debugger;
             }
		 });
	}
	else
		{
			$('#ddfParameter').empty();	
			$('.ParameterFilterList').empty();	
			
			if ($('#ddlGraphType').val() == 2) {
 				$('#btnView').attr("disabled",true);
 			}
		}
	
   
}



function getOption(ddl){
    var select = document.getElementById(ddl);
    if(select.options.length > 0) {
        var option = select.options[select.selectedIndex];
        alert("Text: " + option.text + "\nValue: " + option.value);
    } else {
        window.alert("Select box is empty");
    }
}

function addOption(ddl,data){
    var select = document.getElementById(ddl);
    select.options[select.options.length] = new  Option(data, '0', false, false);
}

function removeOption(ddl){
    var select = document.getElementById(ddl);
    select.options[select.selectedIndex] = null;
}

function removeAllOptions(ddl){
    var select = document.getElementById(ddl);
    select.options.length = 0;
}





function AjaxCall(varSiteIDList,varEquipmentIDList,varParameter1,varParameter2,varRange, varFromDate,varToDate,varGraphType) {                
     
	  debugger;
	  var ThirdGraph = '';
	  var chartheight = $('#hdnGraphType').css('height').replace(/px/,'');	  
      var chartwidth = $('#hdnGraphType').css('width').replace(/px/,'');
      
      if(chartwidth >= 1200)
      {
    	  	chartwidth = $(window).width() - 150;    	  
      }
      else if(chartwidth >= 700)
	  {
  	  	chartwidth = $(window).width() - 200;    	  
  	  }
                
                
                  document.getElementById('ndf').style.visibility="hidden";
                  document.getElementById('load').style.visibility="visible";
                  document.getElementById('container').style.visibility="hidden";
                  document.getElementById('lblReportName').style.visibility="visible"; 
              
                var DataInfo = '';
                var ChartType = 'column';
                var ChartSeriesColor = 'rgb(98, 165, 96)';
                var ChartRangeSelector = 0;
                var ChartXaxisName= 'Date';
                var ChartButtons = [ {
         type: 'month',
         count: 1,
         text: 'Month'
       }];
                
                 if(varGraphType == 0) { varGraphType ="Daily Energy Generation";}
                 else  if(varGraphType == 1) { varGraphType = "Energy Performance"; }
                 else  if(varGraphType == 2) {varGraphType = "Parameter Comparison";}
                 else {varGraphType = "";}
                 
               
                if(varGraphType=="Daily Energy Generation" || varGraphType == 0)
                {
                	document.getElementById('lblReportName').innerHTML = "Daily Energy Generation";
                	ChartType = 'column';
                	ChartSeriesColor = 'rgba(36, 95, 68, 0.87)';
                	ChartRangeSelector = 0;
                	ChartXaxisName= 'Date';
                	
                	if( varSiteIDList == "" ){

                		document.getElementById('load').style.visibility="hidden";
                		document.getElementById('ndf').style.visibility="visible";
                		document.getElementById('container').style.visibility="hidden";
                		document.getElementById('lblReportName').style.visibility="visible";
                		return;
                	}


                }
                else if(varGraphType == "Energy Performance" || varGraphType == 1)
                {
                	document.getElementById('lblReportName').innerHTML = "Energy Performance";
                	ChartType = 'areaspline';
                	ChartSeriesColor = 'rgb(98, 165, 96)';
                	ChartRangeSelector = 0;
                	ChartXaxisName= 'Time';
                	
                	if( varEquipmentIDList == ""){

                		document.getElementById('load').style.visibility="hidden";
                		document.getElementById('ndf').style.visibility="visible";
                		document.getElementById('container').style.visibility="hidden";
                		document.getElementById('lblReportName').style.visibility="visible";
                		return;
                	}


                }
                else if(varGraphType == "Parameter Comparison" || varGraphType == 2)
                {
                	document.getElementById('lblReportName').innerHTML = "Parameter Comparison";
                	var $inputsParameter = $('#ddfParameter').find('input').filter(':checked'); 
                	
                	if($inputsParameter.length == 0)
                	{
                		varParameter1 = '' ;
                		varParameter2 =  '';
                		
                		document.getElementById('load').style.visibility="hidden";
                		document.getElementById('ndf').style.visibility="visible";
                		document.getElementById('container').style.visibility="hidden";
                		document.getElementById('lblReportName').style.visibility="visible";
                		return;
                	}
                	else if($inputsParameter.length >= 1)
                	{
                		for(var i=0;i<  $inputsParameter.length;i++)
                		{
                			if(i==0)
                			{
                				varParameter1 = $('#' + $inputsParameter[i].id).attr('data-value') ;
                			}
                			else
                			{
                				varParameter1 = varParameter1 + ',' + $('#' + $inputsParameter[i].id).attr('data-value') ;
                			}
                		}

                		varParameter2 =  '';
                	}

                	if( varEquipmentIDList == "" || varParameter1 == ""){

                		document.getElementById('load').style.visibility="hidden";
                		document.getElementById('ndf').style.visibility="visible";
                		document.getElementById('container').style.visibility="hidden";
                		document.getElementById('lblReportName').style.visibility="visible";
                		return;
                	}

                }
                else
                {  
                	document.getElementById('lblReportName').innerHTML = "";
                	document.getElementById('load').style.visibility="hidden";
                	document.getElementById('ndf').style.visibility="visible";
                	document.getElementById('container').style.visibility="hidden";
                	document.getElementById('lblReportName').style.visibility="visible";
                	return;
                }
                
                           
              
            /*   var ThirdGraph = '';
              var GraphVal = 'A1512040280 (KART1003) - Current'  + '||'  + C1;
              GraphVal = GraphVal + '&&' + 'A1512040332 (KART1003) - Current' + '||'  + C2;
              GraphVal = GraphVal + '&&' + 'A1511122762 (KART1003) - Current' + '||' + C3;
                 
              ThirdGraph = '0$$Current$$' + GraphVal;
              
              GraphVal =  'A1512040280 (KART1003) - Voltage' + '||'  + C4;
              GraphVal = GraphVal + '&&' + 'A1512040332 (KART1003) - Voltage' + '||' + C5;
              GraphVal = GraphVal + '&&' + 'A1511122762 (KART1003) - Voltage' + '||' + C6;
                 
              
              
              ThirdGraph = ThirdGraph  + '##' + '1$$Voltage$$' + GraphVal;
               */
              
                  $.ajax({
                                    type: 'POST',
                                    url: './chartrequest',
                                    data: { 
                                         
                                        'SiteIDList': varSiteIDList , 
                                        'EquipmentIDList': varEquipmentIDList , 
                                        'Parameter1': varParameter1 , 
                                        'Parameter2': varParameter2 , 
                                        'Range': varRange , 
                                        'FromDate': varFromDate , 
                                        'ToDate': varToDate , 
                                        'GraphType': varGraphType 
                                    },
                                    success: function(msg){
                                
                                                
                                                var ChartValue = msg;
                                               
                                                console.log(ChartValue);
                                                
                                                if( ChartValue == ""){

                                                   	document.getElementById('load').style.visibility="hidden";
                                              		document.getElementById('ndf').style.visibility="visible";
                                                   	document.getElementById('container').style.visibility="hidden";
                                                    document.getElementById('lblReportName').style.visibility="visible";
                                              		return;
                              					}
                                 
                                                               
                                               if(varGraphType=="Daily Energy Generation" || varGraphType == "Energy Performance" ||  varGraphType == 0 ||  varGraphType == 1)
                                               {
                                            	   var b=ChartValue.split('||');
                                                   
                                                            	   if( b[1] == "[]" ||  b[1] == "" || msg == ""){

                                                                       	document.getElementById('load').style.visibility="hidden";
                                                                  		document.getElementById('ndf').style.visibility="visible";
                                                                       	document.getElementById('container').style.visibility="hidden";
                                                                        document.getElementById('lblReportName').style.visibility="visible";
                                                                  		return;
                                                  					}
                                                  					else {

                                                                  		document.getElementById('ndf').style.visibility="hidden";
                                                                       	document.getElementById('container').style.visibility="visible";
                                                                        document.getElementById('lblReportName').style.visibility="visible";
                                                                 	}
                                                  
                                                               		ChartValue = eval('(' + b[1] + ')');
                                                                 	DataInfo =  b[0];
                                          
                                                                 	
                                                                 
                                                                 	if(chartheight==350)
                                                                 		{
                                                                 			chartheight = 330;
                                                                 		}
                                                                 	
                                                                 
                                                            	   Highcharts.stockChart('container', {
                                                                       chart : {
                                                                          height : chartheight,
                                                                          width : chartwidth 
                                                                         
                                                                       },
                                                                       rangeSelector: {
                                                                            enabled: false
                                                                          },
                                                                          scrollbar: {
                                                                              enabled: false
                                                                            },
                                                                            navigator: {
                                                                                enabled: false
                                                                            },
                                                                          legend: {
                                                                            enabled: true
                                                                          },
                                                                      /*  rangeSelector: {
                                                                               buttons: ChartButtons,
                                                                              buttonTheme: {
                                                                                width: 60
                                                                              },
                                                                                selected: ChartRangeSelector,
                                                                                        inputDateFormat: '%Y-%m-%d' 
                                                                                        
                                                                              }, */
                                                                             /*  title: {
                                                                      	        text: varGraphType
                                                                      	    }, */
                                                                          yAxis: {
                                                                              gridLineColor: 'white',
                                                                              opposite: false,
                                                                              min: 0,
                                                                              lineColor: '#5B835B',
                                                                              lineWidth: 1,
                                                                              title :{
                                                                                        text: 'Energy (Wh)',
                                                                                          style: {
                                                                                        color: 'black',
                                                                                        fontWeight: 'bold',
                                                                                        fontSize: '12px',
                                                                                        fontFamily: 'Lucida Sans Unicode", Arial, Helvetica, sans-serif'
                                                                                       /* fontFamily:'Helvetica Neue", Helvetica, Arial, sans-serif'*/
                                                                                    }
                                                                              }
                                                                             /*  labels: {
                                                                                  format: ' Wh'
                                                                              } */
                                                                            },
                                                                         xAxis: {
                                                                              minRange: 1000,     
                                                                              
                                                                                        lineColor: '#5B835B ',
                                                                              lineWidth: 1 ,
                                                                              title :{
                                                                                        text: ChartXaxisName,
                                                                                        style: {
                                                                                        color: 'black',
                                                                                        fontWeight: 'bold',
                                                                                        fontSize: '12px',
                                                                                        fontFamily: 'Lucida Sans Unicode", Arial, Helvetica, sans-serif'
                                                                                            
                                                                                    }
                                                                              }
                                                                            },
                                                                           /* tooltip: {
                                                                                valueDecimals: 0,
                                                                                valueSuffix: ' Wh'
                                                                            },*/
                                                                            tooltip: {
                                                                                valueDecimals: 0,
                                                                                formatter: function() 
                                                                                {   var result = '';
                                                                                     var val = this.y;
                                                                                     if(val >= 1000000) 
                                                                                     { 
                                                                                            val = val / 1000000;
                                                                                            result =  Number(Math.round(val+'e2')+'e-2') + " MWh";
                                                                                     } 
                                                                                     else if(val >= 1000) 
                                                                                     {
                                                                                            val = val / 1000;
                                                                                            result =  Number(Math.round(val+'e2')+'e-2') + " kWh";
                                                                                     } 
                                                                                     else 
                                                                                     {
                                                                                            result =  Number(Math.round(val+'e2')+'e-2') + " Wh";
                                                                                     }
                                                                                     return 'Energy : ' + result; 
                                                                                }
                                                                            },

                                                                       series: [{
                                                                           name: DataInfo,
                                                                           data: ChartValue,
                                                                           type: ChartType,
                                                                           color: ChartSeriesColor,
                                                                           tooltip: {
                                                                               valueDecimals: 2
                                                                           }
                                                                       }]
                                                               
                                                                   }, function (chart) {
                                                                   Highcharts.each(chart.legend.allItems, function(i) {
                                                                           /*  $(i.checkbox).change(function() {
                                                                              if (this.checked) {
                                                                                i.show();
                                                                              } else {
                                                                                i.hide();
                                                                              }
                                                                            }) */
                                                                          }
                                                                   )
                                                               
                                                                       // apply the date pickers
                                                                       setTimeout(function () {
                                                                           $('input.highcharts-range-selector', $(chart.container).parent())
                                                                               .datepicker();
                                                                       }, 0);
                                                                   });
                                                              //});
                                                               
                                                               
                                                               // Set the datepicker's date format
                                                               $.datepicker.setDefaults({
                                                                   dateFormat: 'yy-mm-dd',
                                                                   onSelect: function () {
                                                                       this.onchange();
                                                                       this.onblur();
                                                                   }
                                                               });
                                                               
                                  	}
                                     else if(varGraphType == "Parameter Comparison" ||  varGraphType == 2)
                                    {
                                    	
                                    	 console.log(ChartValue);
                                    	
                                    	//ChartValue = ThirdGraph;
                                    	
                                    	  var YAxis0 ='';
                                    	  var YAxis1 ='';
                                    	  var ChartSeries = '';
                                    	  var ChartDataByYAxis = ChartValue.split('##');
                                          for(var i=0; i< ChartDataByYAxis.length; i++)
                                        	  {
                                        	  		var ChartDataBySingleAxis = ChartDataByYAxis[i];
                                        	  		var ChartAxisData = ChartDataBySingleAxis.split('$$');
                                        	  		var AxisSide =  ChartAxisData[0];
                                        	  		var AxisSideValue =  ChartAxisData[1];
                                        	  		var ChartDataValues =  ChartAxisData[2];
                                        	  		
                                        	  		if(i==0)
                                        	  			{
                                        	  				YAxis0 = AxisSideValue;
                                        	  			}
                                        	  		else
                                        	  			{
                                        	  				YAxis1 = AxisSideValue; 
                                        	  			}
                                        	  		
                                        	  		
                                        	  		var ChartGroupData = ChartDataValues.split('&&');
                                        	  		
                                        	  		for(var j=0; j< ChartGroupData.length; j++)
                                               	    {
                                               	  		var ChartLineData = ChartGroupData[j];
                                               	  	    var MyChartData = ChartLineData.split('||');
                                        	  		    
                                               	  	    var chartGraphName = MyChartData[0];
                                               	  	    var chartGraphAxisValue = MyChartData[1];
                                               	  		
                                               	  	    var ChartSeriesLine = "{type:'spline',name:'" + chartGraphName  + "',data: " + chartGraphAxisValue + ",yAxis: " + AxisSide + "}";
                                               	  	    
                                               	  	    
                                               	  	    debugger;
                                               	  	    if(i==0 && j==0)
                                               	  	    	{
                                               	  	   			ChartSeries = ChartSeriesLine; 
                                               	  	    	}
                                               	  	    else
                                               	  	    	{
                                               	  	   			ChartSeries = ChartSeries + ',' + ChartSeriesLine;
                                               	  	    	}
                                               	  	
                                               	  	    
                                               	     }
                                                         
                                        	  		
                                        	  }
                                                
                                          debugger;
                                           ChartSeries = "[" + ChartSeries + "]";
                                          var ChartSeriesData = eval('(' + ChartSeries +')');                                          
                                          console.log(ChartSeriesData); 
                                          
                                             
                                       	 
                                          Highcharts.chart('container', {

                                        	    chart: {
                                        	        type: 'column',
                                        	        marginTop: 5,                                          	       
                                        	        width: 1220,
                                        	        height:380,
                                        	    },
                                        	    
                                        	   title: {
                                        	        text: '',
                                        	        style: {
                                                        color: 'black',
                                                        fontWeight: 'bold',
                                                        fontSize: '12px',
                                                        fontFamily: 'Lucida Sans Unicode", Arial, Helvetica, sans-serif'
                                                            
                                                    }
                                        	    }, 
                                        	     legend: {
                                        	    	/*borderWidth:3,*/
                                        	    	itemWidth: 360,
                                        	    	width: 1200,
                                        	    	height:100,
                                        	    	x:50
                                        	     },
                                        	    yAxis: [{
                                        	    	gridLineColor: '#FFF',
                                        	    	lineColor: '#5B835B',   
                                        	    	min:0,
                                        	        lineWidth: 1,
                                        	        className: 'highcharts-color-0',
                                        	        title: {
                                        	            text: YAxis0,
                                        	            style: {
                                                            color: 'black',
                                                            fontWeight: 'bold',
                                                            fontSize: '12px',
                                                            fontFamily: 'Lucida Sans Unicode", Arial, Helvetica, sans-serif'

                                                        }
                                        	        }
                                        	    }, {
                                        	        className: 'highcharts-color-1',
                                        	        gridLineColor: '#FFF',
                                        	        lineColor: '#5B835B',
                                        	        lineWidth: 1,
                                        	        opposite: true,
                                        	        title: {
                                        	            text: YAxis1,
                                        	            style: {
                                                            color: 'black',
                                                            fontWeight: 'bold',
                                                            fontSize: '12px',
                                                            fontFamily: 'Lucida Sans Unicode", Arial, Helvetica, sans-serif'
                                                                

                                                        }
                                        	        }
                                        	    }],
                                        	    xAxis: [{
                                        	    	lineColor: '#5B835B',
                                        	    	gridLineColor: '#FFF',
                                        	        lineWidth: 1,
                                        	    	 minRange: 1000,                                                      
                                                    
                                        	    	type: 'datetime'
                                        	    }],

                                        	    plotOptions: {
                                        	        column: {
                                        	            borderRadius: 5
                                        	        }
                                        	    },

                                        	    series: ChartSeriesData
                                        	   /*  series: [{type:'spline',data: [[1519754400000,0.00],[1519755300000,1000.00],[1519756204000,0.00],[1519757102000,0.00],[1519758003000,0.00],[1519804805000,0.00],[1519805705000,2000.00],[1519806609000,4000.00],[1519807506000,5000.00],[1519808408000,5000.00],[1519809300000,5000.00],[1519810209000,6000.00],[1519811110000,7000.00],[1519812009000,7000.00],[1519812907000,7000.00],[1519813811000,9000.00],[1519814701000,8000.00],[1519815629000,9000.00],[1519816502000,9000.00],[1519817401000,9000.00],[1519818301000,9000.00],[1519819208000,10000.00],[1519820106000,10000.00],[1519821009000,10000.00],[1519821906000,8000.00],[1519822806000,10000.00],[1519823703000,10000.00],[1519824609000,10000.00],[1519825502000,9000.00],[1519826400000,9000.00],[1519827309000,8000.00],[1519828203000,9000.00],[1519829108000,8000.00],[1519830006000,7000.00],[1519830907000,6000.00],[1519831804000,8000.00],[1519832709000,6000.00],[1519833609000,5000.00],[1519834505000,5000.00],[1519835406000,5000.00],[1519836303000,3000.00],[1519837208000,3000.00],[1519838108000,3000.00],[1519839006000,1000.00],[1519839903000,2000.00],[1519840807000,0.00],[1519841703000,0.00],[1519842611000,1000.00],[1519843522000,0.00],[1519844423000,0.00],[1519887606000,0.00],[1519888505000,0.00]],yAxis: 0},{type:'spline',data: [[1519754400000,0.00],[1519755300000,0.00],[1519756204000,1000.00],[1519757102000,0.00],[1519758002000,0.00],[1519804022000,0.00],[1519804804000,1000.00],[1519805705000,2000.00],[1519806606000,3000.00],[1519807506000,3000.00],[1519808408000,4000.00],[1519809300000,5000.00],[1519810209000,5000.00],[1519811109000,5000.00],[1519812009000,5000.00],[1519812907000,7000.00],[1519813811000,6000.00],[1519814700000,8000.00],[1519815610000,7000.00],[1519816502000,8000.00],[1519817401000,7000.00],[1519818301000,8000.00],[1519819208000,7000.00],[1519820106000,8000.00],[1519821009000,9000.00],[1519821906000,7000.00],[1519822805000,8000.00],[1519823701000,8000.00],[1519824608000,9000.00],[1519825501000,7000.00],[1519826400000,8000.00],[1519827309000,7000.00],[1519828202000,7000.00],[1519829108000,6000.00],[1519830005000,7000.00],[1519830907000,5000.00],[1519831800000,6000.00],[1519832709000,5000.00],[1519833609000,4000.00],[1519834504000,4000.00],[1519835405000,3000.00],[1519836301000,3000.00],[1519837207000,2000.00],[1519838108000,1000.00],[1519839005000,2000.00],[1519839902000,0.00],[1519840807000,1000.00],[1519841702000,0.00],[1519842611000,0.00],[1519843522000,0.00],[1519844423000,0.00],[1519887606000,0.00],[1519888504000,0.00]],yAxis: 0},{type:'spline',data: [[1519754405000,0.00],[1519755300000,0.00],[1519756204000,0.00],[1519757102000,0.00],[1519804805000,0.00],[1519805705000,3000.00],[1519806609000,4000.00],[1519807506000,5000.00],[1519808408000,5000.00],[1519809300000,5000.00],[1519810209000,6000.00],[1519811110000,7000.00],[1519812009000,7000.00],[1519812907000,8000.00],[1519813811000,8000.00],[1519814701000,8000.00],[1519815630000,10000.00],[1519816502000,9000.00],[1519817401000,8000.00],[1519818301000,10000.00],[1519819209000,9000.00],[1519820106000,10000.00],[1519821011000,10000.00],[1519821906000,8000.00],[1519822806000,10000.00],[1519823703000,10000.00],[1519824609000,10000.00],[1519826400000,18000.00],[1519827309000,8000.00],[1519828203000,8000.00],[1519829108000,8000.00],[1519830006000,8000.00],[1519830907000,7000.00],[1519831804000,7000.00],[1519832709000,6000.00],[1519833609000,5000.00],[1519834506000,5000.00],[1519835406000,5000.00],[1519836303000,3000.00],[1519837208000,3000.00],[1519838108000,3000.00],[1519839006000,1000.00],[1519839903000,1000.00],[1519840808000,1000.00],[1519841703000,0.00],[1519842611000,0.00],[1519843522000,0.00],[1519844423000,0.00],[1519887606000,0.00],[1519888505000,0.00]],yAxis: 0},{type:'spline',data: [[1519754400000,0.00],[1519755300000,10.00],[1519756204000,0.00],[1519757102000,0.00],[1519758003000,0.00],[1519804805000,0.00],[1519805705000,20.00],[1519806609000,40.00],[1519807506000,50.00],[1519808408000,50.00],[1519809300000,50.00],[1519810209000,60.00],[1519811110000,70.00],[1519812009000,70.00],[1519812907000,70.00],[1519813811000,90.00],[1519814701000,80.00],[1519815629000,90.00],[1519816502000,90.00],[1519817401000,90.00],[1519818301000,90.00],[1519819208000,100.00],[1519820106000,100.00],[1519821009000,100.00],[1519821906000,80.00],[1519822806000,100.00],[1519823703000,100.00],[1519824609000,100.00],[1519825502000,90.00],[1519826400000,90.00],[1519827309000,80.00],[1519828203000,90.00],[1519829108000,80.00],[1519830006000,70.00],[1519830907000,60.00],[1519831804000,80.00],[1519832709000,60.00],[1519833609000,50.00],[1519834505000,50.00],[1519835406000,50.00],[1519836303000,30.00],[1519837208000,30.00],[1519838108000,30.00],[1519839006000,10.00],[1519839903000,20.00],[1519840807000,0.00],[1519841703000,0.00],[1519842611000,10.00],[1519843522000,0.00],[1519844423000,0.00],[1519887606000,0.00],[1519888505000,0.00]],yAxis: 1},{type:'spline',data: [[1519754400000,0.00],[1519755300000,0.00],[1519756204000,10.00],[1519757102000,0.00],[1519758002000,0.00],[1519804022000,0.00],[1519804804000,10.00],[1519805705000,20.00],[1519806606000,30.00],[1519807506000,30.00],[1519808408000,40.00],[1519809300000,50.00],[1519810209000,50.00],[1519811109000,50.00],[1519812009000,50.00],[1519812907000,70.00],[1519813811000,60.00],[1519814700000,80.00],[1519815610000,70.00],[1519816502000,80.00],[1519817401000,70.00],[1519818301000,80.00],[1519819208000,70.00],[1519820106000,80.00],[1519821009000,90.00],[1519821906000,70.00],[1519822805000,80.00],[1519823701000,80.00],[1519824608000,90.00],[1519825501000,70.00],[1519826400000,80.00],[1519827309000,70.00],[1519828202000,70.00],[1519829108000,60.00],[1519830005000,70.00],[1519830907000,50.00],[1519831800000,60.00],[1519832709000,50.00],[1519833609000,40.00],[1519834504000,40.00],[1519835405000,30.00],[1519836301000,30.00],[1519837207000,20.00],[1519838108000,10.00],[1519839005000,20.00],[1519839902000,0.00],[1519840807000,10.00],[1519841702000,0.00],[1519842611000,0.00],[1519843522000,0.00],[1519844423000,0.00],[1519887606000,0.00],[1519888504000,0.00]],yAxis: 1},{type:'spline',data: [[1519754405000,0.00],[1519755300000,0.00],[1519756204000,0.00],[1519757102000,0.00],[1519804805000,0.00],[1519805705000,30.00],[1519806609000,40.00],[1519807506000,50.00],[1519808408000,50.00],[1519809300000,50.00],[1519810209000,60.00],[1519811110000,70.00],[1519812009000,70.00],[1519812907000,80.00],[1519813811000,80.00],[1519814701000,80.00],[1519815630000,100.00],[1519816502000,90.00],[1519817401000,80.00],[1519818301000,100.00],[1519819209000,90.00],[1519820106000,100.00],[1519821011000,100.00],[1519821906000,80.00],[1519822806000,100.00],[1519823703000,100.00],[1519824609000,100.00],[1519826400000,180.00],[1519827309000,80.00],[1519828203000,80.00],[1519829108000,80.00],[1519830006000,80.00],[1519830907000,70.00],[1519831804000,70.00],[1519832709000,60.00],[1519833609000,50.00],[1519834506000,50.00],[1519835406000,50.00],[1519836303000,30.00],[1519837208000,30.00],[1519838108000,30.00],[1519839006000,10.00],[1519839903000,10.00],[1519840808000,10.00],[1519841703000,0.00],[1519842611000,0.00],[1519843522000,0.00],[1519844423000,0.00],[1519887606000,0.00],[1519888505000,0.00]],yAxis: 1}]
                                        	    */
                                        	 
                                        	});               
                                                            	   
                                    }
                                             
                                       
                                        
                                     
                                     
                                     document.getElementById('load').style.visibility="hidden";
                                     document.getElementById('container').style.visibility="visible";
                                                 
                                     
                                     
                                    }
                                
                                                               
                  });
  }
  














/*
var LastCheckedParam = '';

$('.chkparameter').on('change', function() {
		var select = $(this);
    var idParameter = select.attr('id');                
    var currentStatus = $('#' + idParameter).prop('checked');
    
    
    //var checkcnt =  $('.chkparameter').find('input[type="checkbox"]:checked').length;
     var $inputsParameter = $('#ddfParameter').find('input').filter(':checked'); 

     if($inputsParameter.length > 2)
    	 {
    	 	var forUnchk1 =  $inputsParameter[0].id;
    	 	var forUnchk2 =  $inputsParameter[1].id;
    	 	var forUnchk3 =  $inputsParameter[2].id;
    	 	
    	 	if(LastCheckedParam != forUnchk1 && idParameter != forUnchk1)
    	 		{
    	 			$('#' + forUnchk1).prop('checked', false); 
    	 		}
    	 	else if(LastCheckedParam != forUnchk2 && idParameter != forUnchk2)
	 		{
	 			$('#' + forUnchk2).prop('checked', false); 
	 		}
    	 	else if(LastCheckedParam != forUnchk3 && idParameter != forUnchk3)
	 		{
	 			$('#' + forUnchk3).prop('checked', false); 
	 		}
    	 }
 
    
		if(currentStatus==true) {
         $('#' + idParameter).prop('checked', true);
         LastCheckedParam = idParameter;
		}
    	
		
}); 
*/
  