function data(){
	
	$('.chkboxFilter').hide();
	$('.Sele').hide();
	$('.closeAll').hide();
	$('#btnApply').hide();
	
	
	
	$('#Equipdropdown').css("visibility", "hidden");
	
	
	
	$(document).on('click','.tagclose', function()
	{
		var equipmentid = $(this).attr('id').replace(/btnclose/,'');
				
		$('#EquipmentTree').fancytree("getRootNode").visit(function(node){
			if (node.selected) {
				if(node.data.id == equipmentid){  node.setSelected(false); }			           
			}
		});
	});
	

	$(document).on('click','.tagparaclose', function()
	{
		var equipmentid = $(this).attr('id').replace(/btnparaclose/,'');
				
		$('#ParameterTree').fancytree("getRootNode").visit(function(node){
			if (node.selected) {
				if(node.data.id == equipmentid){  node.setSelected(false); }			           
			}
		});
	});
	
	
	 $(document).on('change',"#ddlSite", function(){
		 SiteDropdownChange();
     }); 
	
	/*$('#ddlSite').change(function(){
		
		
	});*/
	
	
	
	$('.btn-display').click(function(){
		DataPopulateAjaxCall();
		$('.btn-display').hide();
	});
	
	
	$('#ddlGraphType').change(function(){
		//////;
		var GraphType = $('#ddlGraphType option:selected').text();
		var GraphId =  $('#ddlGraphType option:selected').val();
		var SiteName = $('#ddlSite option:selected').text();
		 
		
		$('.sname').html(GraphType);
	})
	
	
	
	 
    	  
	
	
	 $('#GraphTypeSelect li').click(function() {
		 //////;
		    //alert($(this).attr("id"));
		    var GraphType =  $(this).text();
		    //alert(GraphType);
		    $('.txtGraphType').html(GraphType)
		    $('#hdnGraphType').val(GraphType);
		    
		    //var GraphType = $(this).text();
		    //Daily Energy Generation
		    if (GraphType == 'Daily Energy Generation') {
		    	//////;
		    	debugger;
		    	$('.filter-labels').empty();
		    	$("#EquipmentTree").fancytree("getTree").visit(function(node){
    		        node.setSelected(false);
		    	});
    		 
		    	$("#ParameterTree").fancytree("getTree").visit(function(node){
		    		node.setSelected(false);
		    	});
    		 
		    	 $('#chartData1').css({"display": "block","position":"absolute"});
				 $('#chartData3').css({"display": "none","position":"absolute"});
				 $('#chartData2').css({"display": "none","position":"absolute"});				 
				 $('.GraphPart').css("display", "block");
				 //$('.filter-labels').remove();
				 $('.filter-labels').empty();
				
				 $('#equipment-tab').addClass('disabled');
				 $(".data-toggle-equipment-add").removeAttr("data-toggle",'tab');
				 $('#parameter-tab').addClass('disabled');
				 $(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
				 $('#parameter-tab a').addClass('disabled');
				 $('#tab-fillup3').removeClass('disabled');
				
				 DataPopulateAjaxCall();
		    }
		    else if (GraphType == 'Energy Performance') {		    	
		    	//////;
		    	$('.filter-labels').empty();
		    	
		    	$("#EquipmentTree").fancytree("getTree").visit(function(node){
    		        node.setSelected(false);
		    	});
    		 
		    	$("#ParameterTree").fancytree("getTree").visit(function(node){
		    		node.setSelected(false);
		    	});
		    	//$('#success').click(function () {
	                 //toastr.info("Select 'Equipment' to view graph", {positionClass:'toast-top-left',closeButton:true, progressBar:true, showDuration:300,hideDuration:5000 ,timeOut: 7000, extendedTimeOut:1000});
	                 
		    	toastr.info("Select 'Equipment' to view graph");
		    	
	             //});
				   $('#chartData1').css({"display": "none","position":"absolute"});
				   $('#chartData3').css({"display": "none","position":"absolute"});
				   $('#chartData2').css({"display": "block","position":"absolute"});
				   $('.GraphPart').css("display", "block");
				  
				   $('#equipment-tab a').addClass('active');
				   $('#graph-tab a').removeClass('active');
				   $('#tab-fillup1').removeClass('active');
				   $('#tab-fillup2').addClass('active');		   

				   $('#equipment-tab').removeClass('disabled');
	               $(".data-toggle-equipment-add").attr("data-toggle",'tab');
	               $('#parameter-tab').addClass('disabled');
				   $(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');		    		 
				   //DataPopulateAjaxCall();
				  	  
			   }
		    else if (GraphType == 'Parameter Comparison') {
		    	debugger;
		    	$('.filter-labels').empty();
		    	 toastr.info("Select 'Equipment' then 'Parameter'");
		    	 $("#EquipmentTree").fancytree("getTree").visit(function(node){
	    		        node.setSelected(false);
			    	});
	    		 
			    	$("#ParameterTree").fancytree("getTree").visit(function(node){
			    		node.setSelected(false);
			    	});
		    	$('.GraphPart').css("display", "block");
				   $('#chartData1').css({"display": "none","position":"absolute"});
				   $('#chartData2').css({"display": "none","position":"absolute"});
				   $('#chartData3').css({"display": "block","position":"absolute"});
				 
				   $('#equipment-tab').removeClass('disabled');				  
	               $(".data-toggle-equipment-add").attr("data-toggle",'tab');
	               
	               $('#equipment-tab a').addClass('active');
				   $('#graph-tab a').removeClass('active');
				   $('#tab-fillup1').removeClass('active');
				   $('#tab-fillup2').addClass('active');
	              					   
				   var Equipmentids = SelectedFancyTreeElement("EquipmentTree");
				   
				 

	            	if(Equipmentids.toString() == "")
	            	{
	            		
	            	   $('#parameter-tab').addClass('disabled');
	  				   $(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
	  				   
	            	}
	            	else
	            	{

	        			
	            		$('#parameter-tab').removeClass('disabled');
	 				    $(".data-toggle-parameter-add").attr("data-toggle",'tab');
	            	
	 				  
	            	}
	               
	            	 
	            	  	
				  // DataPopulateAjaxCall();
				 
		    }
		    
	 })
	
	
	 
	
	
	//**** Site Select *****//
		
	
	
	$('.closeAll').click(function(){
		$('.Sele').hide();
		$('.closeAll').hide();
		$('#btnApply').hide();
	})
	
	
	$("#my_styles .btn").click(function() {
		jQuery("#my_styles .btn").removeClass('active');
		jQuery(this).toggleClass('active');
	});
	
	$("#my_btn-styles .btn").click(function() {
		jQuery("#my_btn-styles .btn").removeClass('active');
		jQuery(this).toggleClass('active');
	});
	
	$('#btnView').click(function(){		
		DataPopulateAjaxCall();
	});

	
	
	/*$("#dropdown").multiselect({
		
		   selectedText: function(numChecked, numTotal, checkedItems){
		      return numChecked + ' of ' + numTotal + ' checked';
		   }
	
		});*/
	

		/** Default - Current Day ***/
		Daily();


		/** Current Day ***/
		$('#daily').click(function() {
			Daily();
			$('#hdnSelectedDataRange').val('daily');
			ViewReport();
		})

		/** Current Week **/
		$('#weekly').click(function() {
			Weekly();
			$('#hdnSelectedDataRange').val('weekly');
			ViewReport();
		})

		/** Current Month ***/
		$('#monthly').click(function() {
			Monthly();
			$('#hdnSelectedDataRange').val('monthly');
			ViewReport();
		})

		/** Current Year ***/
		$('#yearly').click(function() {
			Yearly();
			$('#hdnSelectedDataRange').val('yearly');
			ViewReport();
		})



		/** Custom ***/
		$('#custom').click(function() {
			Custom();
			$('#hdnSelectedDataRange').val('custom');

		})


		$('#CurrentVal').click(function() {
			DataPopulateAjaxCall();
			//ViewReport();
		})
		
		$('#btnApply').click(function() {
			ViewReport();
		})


		$('#fromDate').on('focusout', function() {
			try {
				var fromdate = $.datepicker.parseDate('mm/dd/yy', $('#fromDate').val());
				if ($('#fromDate').val() == '') {
					$('#fromDate').val($('#toDate').val());
				}
			} catch (err) {
				$('#fromDate').val($('#toDate').val());
			}
			$('#custom').click();

		})



		$('#toDate').on('focusout', function() {
			try {
				var todate = $.datepicker.parseDate('mm/dd/yy', $('#toDate').val());
				if ($('#toDate').val() == '') {
					$('#toDate').val($('#fromDate').val());
				}

			} catch (err) {
				$('#toDate').val($('#fromDate').val());
			}
			$('#custom').click();
		})

		$("#fromDate").datepicker({
			dateFormat : 'dd/mm/yy',
			maxDate : new Date(),
			maxDate : "0",
			onSelect : function(selected) {
				$("#toDate").datepicker("option", "minDate", selected);
				//$('#response').text('day clicked '+cont);
				$('#custom').click();
			}
		});


		$("#toDate").datepicker({
			dateFormat : 'dd/mm/yy',
			maxDate : new Date(),
			maxDate : "0",
			onSelect : function(selected) {
				$("#fromDate").datepicker("option", "maxDate", selected);
				// $('#response').text('day clicked '+cont); 
				$('#custom').click();
			}
		});
	
		
		
		
		
	
	
	$('.SiteFilterList').on('click', '.sitefilterdivclose').on('click', '.sitefilterclose', function() {
		
		//////;
		
	    // Do something on an existent or future .dynamicElement
	    var select = $(this);
		var idSelect = select.attr('id');	
		
		idSelect = idSelect.replace(/lbl/,'').replace(/filterclose/,'');	
		
		$('#' + idSelect).prop('checked', false);  
		SiteCheckedChange(idSelect);
		
		if ($('.SiteFilterList').children().length == 1 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 0) {
			$('.Sele').hide();
			$('.closeAll').hide();
			$('#btnApply').hide();
			
			if ($('#ddlGraphType').val() == 0) {
				$('#btnView').attr("disabled",true);
			}
			
		}
		else {
			$('#btnApply').show();
		}
		
		
		if ($('#ddlGraphType').val() == 1) {
			if ($('.EquipmentFilterList').children().length == 0)
			{
			$('#btnView').attr("disabled",true);
			$('#btnApply').hide();
			$('#ddfParameter').empty();	
			$('.ParameterFilterList').empty();
			}			
		}
		else if ($('#ddlGraphType').val() == 2) {						
							
			if ($('.EquipmentFilterList').children().length == 0)
			{
				$('#ddfParameter').empty();	
				$('.ParameterFilterList').empty();
				$('#btnView').attr("disabled",true);
				$('#btnApply').hide();
			}
			else
			{
				AjaxCallForStandardParameters();
				
				
				
				if ($('.ParameterFilterList').children().length == 0)
				{
					$('#ddfParameter').empty();	
					$('.ParameterFilterList').empty();
					$('#btnView').attr("disabled",true);
					$('#btnApply').hide();
				}
				else
				{
					$('#btnView').attr("disabled",false);
					$('#btnApply').show();
				}
			}
			
		}
		
		
		
		
	});

		
		
		
		$('.ParameterFilterList').on('click', '.parameterfilterdivclose').on('click', '.parameterfilterclose', function() {
			
			//////;
		    // Do something on an existent or future .dynamicElement
		    var select = $(this);
			var idSelect = select.attr('id');	
			
			idSelect = idSelect.replace(/lbl/,'').replace(/filterparamclose/,'');	
			
			$('#' + idSelect).prop('checked', false);  
			SiteCheckedChange(idSelect);
			
			if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 1) {
				$('.Sele').hide();
				$('.closeAll').hide();
				$('#btnApply').hide();
				
				if ($('#ddlGraphType').val() == 2) {
					$('#btnView').attr("disabled",true);
				}
			}
			else {
				$('#btnApply').show();
			}
			
			
			
			
			if ($('#ddlGraphType').val() == 1) {
				if ($('.EquipmentFilterList').children().length == 0)
				{
				$('#btnView').attr("disabled",true);
				$('#btnApply').hide();
				$('#ddfParameter').empty();	
				$('.ParameterFilterList').empty();
				}			
			}
			else if ($('#ddlGraphType').val() == 2) {						
								
				if ($('.EquipmentFilterList').children().length == 0)
				{
					$('#ddfParameter').empty();	
					$('.ParameterFilterList').empty();
					$('#btnView').attr("disabled",true);
					$('#btnApply').hide();
				}
				else
				{
					
					
					
					if ($('.ParameterFilterList').children().length == 1)
					{
						$('#ddfParameter').empty();	
						$('.ParameterFilterList').empty();
						$('#btnView').attr("disabled",true);
						$('#btnApply').hide();
					}
					else
					{
						$('#btnView').attr("disabled",false);
						$('#btnApply').show();
					}
				}
				
			}
			
			
			
			
			
		});
		
		
		
		
		
 $('.EquipmentFilterList').on('click', '.equipmentfilterdivclose').on('click', '.equipmentfilterclose', function() {
		
		//////;
	    // Do something on an existent or future .dynamicElement
	    var select = $(this);
		var idSelect = select.attr('id');	
		
		idSelect = idSelect.replace(/lbl/,'').replace(/filterequclose/,'');	
		
		$('#' + idSelect).prop('checked', false); 		
		$('#chkInverters').prop('checked', false);
		
		if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 1 && $('.ParameterFilterList').children().length == 0) {
			$('.Sele').hide();
			$('.closeAll').hide();
			$('#btnApply').hide();
			
			
		}
		else {
			$('#btnApply').show();
		}
		
		
		if ($('#ddlGraphType').val() == 1) {
			if($('.EquipmentFilterList').children().length <= 1)
			{
				$('#btnView').attr("disabled",true);
				$('#btnApply').hide();
			}
			
		}
		
	});
	
		
		
		 $('#chkSites').on('change', function() {
			//var CustomerId = $('#ddfCustomer').val();                             
			//var $inputs = $('#ddfSite').find('input').filter('[data-customer="' + CustomerId + '"]');    

			var $inputs = $('#ddfSite').find('input');

			//////;
			if (this.checked) {
				for (var i = 0; i < $inputs.length; i++) {
					var chk = $inputs[i].id;
					var chktext = $('#lbl' + chk).text();
					
					
					chktext = $.trim(chktext.replace(/[\t\n]+/g, ' '));

					
					$('#' + chk).prop('checked', true); // Checks it      

					var filterid = "filter" + chk;
					var filterclodeid = "filterclose" + chk;

					if (window[filterid]) {
						//no need of adding new node
					} else {
						var newNode = '<div id="' + filterid + '" class="alert alert-success info-window sitefilterdivclose" role="alert" title="' + chktext + '"><button id="' + filterclodeid + '"  class="close info-close-window sitefilterclose" data-dismiss="alert"></button>' + chktext + '</div>';
						$('.SiteFilterList').append(newNode);
						$('.Sele').show();
						$('.closeAll').show();
					}


					SiteCheckedChange(chk);
				}
				
				if ($('#ddlGraphType').val() == 0) {
					$('#btnView').attr("disabled",false);
				}
				else if ($('#ddlGraphType').val() == 1) {
					if ($('.EquipmentFilterList').children().length == 0)
					{$('#btnView').attr("disabled",true);}
					else
					{$('#btnView').attr("disabled",false);}
				}
				
				
			} else {
				for (var i = 0; i < $inputs.length; i++) {
					var chk = $inputs[i].id;
					$('#' + chk).prop('checked', false); // Unchecks it                        
					
					$('.SiteFilterList').empty();
					$('.EquipmentFilterList').empty();
					$('.ParameterFilterList').empty();
				
					$('#chkInverters').prop('checked', false);
					
					
					
					if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 0) {
						$('.Sele').hide();
						$('.closeAll').hide();
					}
					SiteCheckedChange(chk);
				}
				
				if ($('#ddlGraphType').val() == 0) {
					$('#btnView').attr("disabled",true);
				}
				else if ($('#ddlGraphType').val() == 1) {
					if ($('.EquipmentFilterList').children().length == 0)
					{$('#btnView').attr("disabled",true);}
					else
					{$('#btnView').attr("disabled",false);}
				}
				else if ($('#ddlGraphType').val() == 2) {						
					
					
					if ($('.EquipmentFilterList').children().length == 0)
					{
						$('#ddfParameter').empty();	
						$('.ParameterFilterList').empty();
						$('#btnView').attr("disabled",true);
					}
					else
					{
						AjaxCallForStandardParameters();
						
						
							
							
						
						if ($('.ParameterFilterList').children().length == 0)
						{
							$('#ddfParameter').empty();	
							$('.ParameterFilterList').empty();
							$('#btnView').attr("disabled",true);
						}
						else
						{
							$('#btnView').attr("disabled",false);
						}
					}
					
				}
			}
		});
          
         
			
			 
	    $('.chksite').on('change', function() {
			var select = $(this);
			var idSite = select.attr('id');

			//////;

			var chktext = $('#lbl' + idSite).text();
			chktext = $.trim(chktext.replace(/[\t\n]+/g, ' '));


			var filterid = "filter" + idSite;
			var filterclodeid = "filterclose" + idSite;
			
			
			if ($('#' + idSite).prop('checked')) {
				if (window[filterid]) {

				} else {
					var newNode = '<div id="' + filterid + '" class="alert alert-success info-window sitefilterdivclose " role="alert" title="' + chktext + '"><button id="' + filterclodeid + '"  class="close info-close-window sitefilterclose" data-dismiss="alert"></button>' + chktext + '</div>';
					$('.SiteFilterList').append(newNode);
					$('.Sele').show();
					$('.closeAll').show();
				}
			} else {
				
				$('#' + filterid).remove();
				$('#chkInverters').prop('checked', false);
				if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 0) {
					$('.Sele').hide();
					$('.closeAll').hide();
				}
				
				
			}


			
				
			
			SiteCheckedChange(idSite);
			
			if ($('#ddlGraphType').val() == 0) {
				if ($('.SiteFilterList').children().length == 0)
				{$('#btnView').attr("disabled",true);}
				else
				{$('#btnView').attr("disabled",false);}
			}
			else if ($('#ddlGraphType').val() == 1) {
				if ($('.EquipmentFilterList').children().length == 0)
				{$('#btnView').attr("disabled",true);}
				else
				{$('#btnView').attr("disabled",false);}
			}
			else if ($('#ddlGraphType').val() == 2) {						
								
				if ($('.EquipmentFilterList').children().length == 0)
				{
					$('#ddfParameter').empty();	
					$('.ParameterFilterList').empty();
					$('#btnView').attr("disabled",true);
				}
				else
				{
					AjaxCallForStandardParameters();
					
					if ($('.ParameterFilterList').children().length == 0)
					{
						$('#ddfParameter').empty();	
						$('.ParameterFilterList').empty();
						$('#btnView').attr("disabled",true);
					}
					else
					{
						$('#btnView').attr("disabled",false);
					}
				}
				
			}
			
			
		});
	          
	    
	          	          

		$('#chkInverters').on('change', function() {
			//////;

			var $inputsInv = $('#ddfInverter').find('input');
			if (this.checked) {
				for (var i = 0; i < $inputsInv.length; i++) {
					var chk = $inputsInv[i].id;
					
					
					var chktext = $('#lbl' + chk).text();
					chktext = $.trim(chktext.replace(/[\t\n]+/g, ' '));

					if($('#div' + chk).css('display') == 'block')
					{
						$('#' + chk).prop('checked', true); // Checks it           				
						
						var filterid = "filterequ" + chk;
						var filterclodeid = "filterequclose" + chk;
						
						
						if (window[filterid]) {
							//no need of adding new node
						} else {
							var newNode = '<div id="' + filterid + '" class="alert alert-danger info-window equipmentfilterdivclose" role="alert" title="' + chktext + '"><button id="' + filterclodeid + '" class="close info-close-window equipmentfilterclose" data-dismiss="alert"></button>' + chktext + '</div>';
							$('.EquipmentFilterList').append(newNode);
							$('.Sele').show();
							$('.closeAll').show();
						}

						
						
						SiteCheckedChange(chk);
						
						if ($('#ddlGraphType').val() == 1) {
							$('#btnView').attr("disabled",false);
						}
						
						
						
					}
					
					
				}
				
				
				
			} 
			else {
				for (var i = 0; i < $inputsInv.length; i++) {
					var chk = $inputsInv[i].id;
						$('#' + chk).prop('checked', false); // Unchecks it
						
						
					}
				
				
				$('.EquipmentFilterList').empty();	
				$('.ParameterFilterList').empty();	
				
				if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 0) {
					$('.Sele').hide();
					$('.closeAll').hide();
				}
				
				if ($('#ddlGraphType').val() == 1) {
					$('#btnView').attr("disabled",true);
				}

			}
			
			
			
			
			
			AjaxCallForStandardParameters();
			
			if ($('#ddlGraphType').val() == 1) {
				if ($('.EquipmentFilterList').children().length == 0)
				{$('#btnView').attr("disabled",true);}
				else
				{$('#btnView').attr("disabled",false);}
			}
			else if ($('#ddlGraphType').val() == 2) {
				if ($('.ParameterFilterList').children().length == 0)
				{$('#btnView').attr("disabled",true);}
				else
				{$('#btnView').attr("disabled",false);}
			}
			
		});
	          
	          
	          
				
		
		 
	    $('.chkinverter').on('change', function() {
	    	//////;
			var select = $(this);
			var idEquipment = select.attr('id');

			//////;

			var chktext = $('#lbl' + idEquipment).text();
			chktext = $.trim(chktext.replace(/[\t\n]+/g, ' '));

			var filterid = "filterequ" + idEquipment;
			var filterclodeid = "filterequclose" + idEquipment;
			
			
			
			if ($('#' + idEquipment).prop('checked')) {
				
				if (window[filterid]) {
					//no need of adding new node
				} else {
					var newNode = '<div id="' + filterid + '" class="alert alert-danger info-window equipmentfilterdivclose" role="alert" title="' + chktext + '"><button id="' + filterclodeid + '" class="close info-close-window equipmentfilterclose" data-dismiss="alert"></button>' + chktext + '</div>';
					$('.EquipmentFilterList').append(newNode);
					$('.Sele').show();
					
					$('.closeAll').show();
				}
				
				
			} else {
				$('#' + filterid).remove();
				
				if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 0) {
					$('.Sele').hide();
					$('.closeAll').hide();
					
				}

			}

			

			

			AjaxCallForStandardParameters();
			
			
			if ($('#ddlGraphType').val() == 1) {
				if ($('.EquipmentFilterList').children().length == 0)
				{$('#btnView').attr("disabled",true);}
				else
				{$('#btnView').attr("disabled",false);}
			}
			else if ($('#ddlGraphType').val() == 2) {
				if ($('.ParameterFilterList').children().length == 0)
				{$('#btnView').attr("disabled",true);}
				else
				{$('#btnView').attr("disabled",false);}
			}
			
			
		});
	          
	         
	   
        

$('#chkParameters').on('change', function() {
	
	//////;

	var $inputs = $('#ddfParameter').find('input');
	if (this.checked) {
		for (var i = 0; i < $inputs.length; i++) {
			var chk = $inputs[i].id;
			
			
			var chktext = $('#lbl' + chk).text();
			
			
			
			chktext = $.trim(chktext.replace(/[\t\n]+/g, ' '));

			if($('#div' + chk).css('display') == 'block')
			{
				$('#' + chk).prop('checked', true); // Checks it           				
				
				var filterid = "filterparam" + chk;
				var filtercloseid = "filterparamclose" + chk;
				
				
				if (window[filterid]) {
					//no need of adding new node
				} else {
					var newNode = '<div id="' + filterid + '" class="alert alert-info info-window equipmentfilterdivclose" role="alert" title="' + chktext + '"><button id="' + filtercloseid + '" class="close info-close-window equipmentfilterclose" data-dismiss="alert"></button>' + chktext + '</div>';
					$('.ParameterFilterList').append(newNode);
					$('.Sele').show();
					$('.closeAll').show();
				}

				if ($('#ddlGraphType').val() == 2) {
					$('#btnView').attr("disabled",false);
				}
				
			}
			
			
		}
	} 
	else {
		for (var i = 0; i < $inputs.length; i++) {
			var chk = $inputs[i].id;
				$('#' + chk).prop('checked', false); // Unchecks it
				
				
			}
		
		
		$('.ParameterFilterList').empty();			
		
		if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 0) {
			$('.Sele').hide();
			$('.closeAll').hide();
		}
		
		if ($('#ddlGraphType').val() == 2) {
			$('#btnView').attr("disabled",true);
		}

	}

});
    
    
    
    

       
        
		 
	    $('.chkparameter').on('change', function() {
	    	
	    	
	    	//////;
			var select = $(this);
			var idParameter = select.attr('id');


			var chktext = $('#lbl' + idParameter).text();
			chktext = $.trim(chktext.replace(/[\t\n]+/g, ' '));

			var filterid = "filterparam" + idParameter;
			var filterclodeid = "filterparamclose" + idParameter;
			
			
			
			if ($('#' + idParameter).prop('checked')) {
				
				if (window[filterid]) {
					//no need of adding new node
				} else {
					var newNode = '<div id="' + filterid + '" class="alert alert-info info-window parameterfilterdivclose" role="alert" title="' + chktext + '"><button id="' + filterclodeid + '" class="close info-close-window parameterfilterclose" data-dismiss="alert"></button>' + chktext + '</div>';
					$('.ParameterFilterList').append(newNode);
					$('.Sele').show();
					$('.closeAll').show();
					
				}
				
				
			} else {
				$('#' + filterid).remove();
				
				if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 0) {
					$('.Sele').hide();
					$('.closeAll').hide();
				}

			}
			
			
			if ($('#ddlGraphType').val() == 2) {
				if ($('.ParameterFilterList').children().length == 0)
				{
					
				$('#btnView').attr("disabled",true);
				}
				else
				{
					
				$('#btnView').attr("disabled",false);
				}
			}



		});
	          
	         
	    

	    
				
        var LastCheckedParam = '';
          $('#btnClrFilter').on('click', function() {

			
			var $inputs = $('#ddfSite').find('input');
			var $inputsInverter = $('#ddfInverter').find('input');
			var $inputsParameter = $('#ddfParameter').find('input').filter(':checked');


			for (var i = 0; i < $inputs.length; i++) {
				var chk = $inputs[i].id;
				$('#' + chk).prop('checked', false); // Unchecks it
				SiteCheckedChange(chk);
			}
			for (var i = 0; i < $inputsInverter.length; i++) {
				var chk = $inputsInverter[i].id;
				$('#' + chk).prop('checked', false);
			}
			for (var i = 0; i < $inputsParameter.length; i++) {
				var chk = $inputsParameter[i].id;
				$('#' + chk).prop('checked', false);
			}
			LastCheckedParam = '';
			$('#ddfParameter').empty();
			
			
			$('#chkSites').prop('checked', false);
			$('#chkInverters').prop('checked', false);
			$('#chkParameters').prop('checked', false);
			
			$('.SiteFilterList').empty();
			$('.EquipmentFilterList').empty();
			$('.ParameterFilterList').empty();
			
			$('.Sele').hide();
			$('.closeAll').hide();
		});
          
          
          
          
          $('#btnClearAll').on('click', function() {        	  
        	  $('#btnClrFilter').click();
          });
         
          
         
          
       
         /* $('#btnView').on('click', function() {     
          	//////;
             ViewReport();
             //$('.Sele').show();
             $('.filterbox').hide();
             $('.closeAll').show();
             //$('#filter').hide();
             //$('#btnfliterHide').click();
                                          
          });*/
          
          
          $('.GraphTypeClick1').on('click', function() {
             
          	//////;
             var HiddenValue=  document.getElementById($(this).attr('id')).innerHTML;                          
             $('#hdnGraphType').val(HiddenValue);  
             document.getElementById('lblReportName').innerHTML = HiddenValue;
             
             //var CustomerId = $('#ddfCustomer').val();             
             var $inputsSite = $('#ddfSite').find('input').filter(':checked');
             var $inputsInverter = $('#ddfInverter').find('input').filter(':checked');               
             var $inputsAllInverter = $('#ddfInverter').find('input');


             var varSiteList = '';
             for(var i=0;i< $inputsSite.length; i++) {
					var chk =  $inputsSite[i].id;
                  var DataVal= $('#' + chk).attr('data-value');
                  if(i==0) {
                  	varSiteList = DataVal; 
                  }
                  else {
                  	varSiteList = varSiteList + ',' + DataVal;
                  }
               }
             
             
             var bolInvChecked = false;
             var varInverterList = '';
             for(var i=0;i< $inputsInverter.length; i++) {
					var chk =  $inputsInverter[i].id;
                  var DataVal= $('#' + chk).attr('data-value');
                 if(i==0) {
                   varInverterList = DataVal; 
                 }
                 else {
                		varInverterList = varInverterList + ',' + DataVal;
                  }
             }
             
             
             
             
             var bolCheckDefaultInverter = false;
             // var radioValue = $("input[name='GraphType']:checked").val(); 
             var radioValue = $('#hdnGraphType').val();
             
             if(radioValue=='Energy Performance' && varInverterList == '')
             {
                             for(var i=0;i< $inputsAllInverter.length; i++)
                                             {
                                             var chk =  $inputsAllInverter[i].id;
                                                             if($('#div' + chk).css('display') == 'block' && bolCheckDefaultInverter==false)
                                                             {
                                                                             varInverterList = $('#' + chk).attr('data-value');
                                                                             $('#' + chk).prop('checked', true);
                                                                             bolCheckDefaultInverter = true;                                                                                
                                                             }
                                                             
                                             }
                             
             }
             else if(radioValue=='Parameter Comparison' && varInverterList == '')
                 {
                                 for(var i=0;i< $inputsAllInverter.length; i++)
                                                 {
                                                 var chk =  $inputsAllInverter[i].id;
                                                                 if($('#div' + chk).css('display') == 'block' && bolCheckDefaultInverter==false)
                                                                 {
                                                                                 varInverterList = $('#' + chk).attr('data-value');
                                                                                 $('#' + chk).prop('checked', true);
                                                                                 bolCheckDefaultInverter = true;                                                                                
                                                                 }
                                                                 
                                                 }
                                 
                 }
             
             
             
                             AjaxCall(varSiteList,varInverterList,'','',$('#hdnSelectedDataRange').val(),$('#fromDate').val(),$('#toDate').val(),radioValue);
                             
                             
          });
          
}




var ajax_request;

function EquipmentAjaxOld(SiteId) {
	  var ddlList = $('#Equipdropdown');
	  
	  
	  //////;
	  ddlList.empty();

	  //////;
	  $.ajax({
        type: 'POST',
        url: './filterequipments',
        data: {                   
            'SiteID': SiteId                
        },
        success: function(data){ 
      	  //////;
			if(data != null) {
				var equipments = data.equipments;
				 if(equipments != null) {
					 var currentgroup = '';
					 var optgroup= '';
					 var option ='';
					 for (var i=0; i<equipments.length;i++) {
						var equipment =  equipments[i];
						 	
						if(equipment != null && equipment.length >= 5 ) {								
							 var EquipementId = equipments[i][1];
							 var GroupName = equipments[i][3];
							 var EquipmentName = equipments[i][4];
							
							 if(i==0)
							 {
								 option =  '<option type="checkbox" class="equipment-opt" id="equipmentid' + EquipementId + '" value="' + EquipementId + '">'+  EquipmentName +'</option>';
								 currentgroup = GroupName;
							 }
							 else if(currentgroup == GroupName)
							 {
								 option = option + '<option type="checkbox" class="equipment-opt" id="equipmentid' + EquipementId + '"  value="' + EquipementId + '">'+  EquipmentName +'</option>';
								 
								 if(i == equipments.length-1)
								 {
									var EquipemetAppend = '<optgroup label=" ' + currentgroup + '">' + option + '</optgroup>';
									
									ddlList.append(EquipemetAppend);						 
									//categCheck1.multiselect('refresh');
										
									option='';
									currentgroup = GroupName;										
								 }
								
							 }
							 else
							 {
								    var EquipemetAppend = '<optgroup label=" ' + currentgroup + '">' + option + '</optgroup>';
								
								    ddlList.append(EquipemetAppend);						 
									//categCheck1.multiselect('refresh');
									
									option = '<option class="equipment-opt" id="equipmentid' + EquipementId + '" value="' + EquipementId + '">'+  EquipmentName +'</option>';;
									currentgroup = GroupName;
									
									if(i == equipments.length-1)
									 {
										
										var EquipemetAppend = '<optgroup label=" ' + currentgroup + '">' + option + '</optgroup>';
										
										ddlList.append(EquipemetAppend);						 
										//categCheck1.multiselect('refresh');
											
										option='';
										currentgroup = GroupName;											
									 }										
							 		}
						 		} 
					 		}
					 }
				}  
        },
        error: function(data){ 
      	  //////;
       	 //alert(data); 
       	console.log(data);
         }
        });
	}


/*function ParameterAjaxOld(SiteId) {
	  var ddlParameterList = $('#Paradropdown');
	  ddlParameterList.empty();
	  //////;
	  $.ajax({
      type: 'POST',
      url: './filterequipmentparameters',
      data: {                   
          'SiteID': SiteId                
      },
      success: function(data){ 
    	  //////;
			if(data != null) {
				var equipments = data.equipmentparameters;
				 if(equipments != null) {
					 var currentgroup = '';
					 var optgroup= '';
					 var option ='';
					 for (var i=0; i<equipments.length;i++) {
						var equipment =  equipments[i];
						 	
						if(equipment != null && equipment.length >= 5 ) {								
							 var EquipementId = equipments[i][1];
							 var GroupName = equipments[i][3];
							 var ParameterName = equipments[i][6];
							 var ParameterId = equipments[i][5];
							
							 if(i==0)
							 {
								 option =  '<option type="checkbox" class="parameter-opt" id="parameterid' + EquipementId + '-' + ParameterId + '"  value="' + EquipementId + '|' + ParameterId + '">'+  ParameterName +'</option>';
								 currentgroup = GroupName;
							 }
							 else if(currentgroup == GroupName)
							 {
								 option = option + '<option type="checkbox" class="parameter-opt" id="parameterid' + EquipementId + '-' + ParameterId + '" value="'  + EquipementId + '|' + ParameterId + '">'+  ParameterName +'</option>';
								 
								 if(i == equipments.length-1)
								 {
									var EquipemetAppend = '<optgroup label=" ' + currentgroup + '">' + option + '</optgroup>';
									
									ddlParameterList.append(EquipemetAppend);						 
									//categCheck2.multiselect('refresh');
										
									option='';
									currentgroup = GroupName;										
								 }
								
							 }
							 else
							 {
								    var EquipemetAppend = '<optgroup label=" ' + currentgroup + '">' + option + '</optgroup>';
								
								    ddlParameterList.append(EquipemetAppend);						 
									//categCheck2.multiselect('refresh');
									
									option = '<option type="checkbox" class="parameter-opt" id="parameterid' + EquipementId + '-' + ParameterId + '" value="'  + EquipementId + '|' + ParameterId + '">'+  ParameterName +'</option>';;
									currentgroup = GroupName;
									
									if(i == equipments.length-1)
									 {
										
										var EquipemetAppend = '<optgroup label=" ' + currentgroup + '">' + option + '</optgroup>';
										
										ddlParameterList.append(EquipemetAppend);						 
										//categCheck2.multiselect('refresh');
											
										option='';
										currentgroup = GroupName;											
									 }										
							 		}
						 		} 
					 		}
					 }
				}  
      },
      error: function(data){ 
    	  //////;
     	 //alert(data); 
     	console.log(data);
       }
      });
	}
*/




function EquipmentAjax(SiteId) {
	//////;
	 $('#EquipmentJSTree').jstree('destroy');
	//////;
	
	 var jsondata = '';
	 
	 // alert(1);
	  //////;
	  $.ajax({
        type: 'POST',
        url: './filterequipments' + SiteId,
       
        success: function(data){ 
      	  //////;
			if(data != null) {
				var equipments = data.equipments;
				 if(equipments != null) {
					 
					
					 
					 var currentgroup = '';
					 var optgroup= '';
					 var option ='';
					 
					 
					 for (var i=0; i<equipments.length;i++) {
						var equipment =  equipments[i];
						 	
						if(equipment != null && equipment.length >= 5 ) {								
							 var EquipementId = equipments[i][1];
							 var GroupName = equipments[i][3];
							 var EquipmentName = equipments[i][4];
							
							 if(i==0)
							 {
								 option = '{ "id": "' + EquipementId + '", "parent": "' + GroupName + '", "text": "'+  EquipmentName +'", "icon" : "" }';
								 
								 currentgroup = GroupName;
							 }
							 else if(currentgroup == GroupName)
							 {
								 option = option +',{ "id": "' + EquipementId + '", "parent": "' + GroupName + '", "text": "'+  EquipmentName +'" , "icon" : ""  }';
								 
								 if(i == equipments.length-1)
								 {
									var EquipemetAppend = '{ "id": "' + currentgroup + '", "parent": "#", "text": "'+  currentgroup +'" },' + option;
									 
									if(jsondata=='')
									{
										jsondata = EquipemetAppend;
									}
									else
									{
										jsondata = jsondata + ',' + EquipemetAppend;
									}
										
									option='';
									currentgroup = GroupName;										
								 }
								
							 }
							 else
							 {
								    var EquipemetAppend = '{ "id": "' + currentgroup + '", "parent": "#", "text": "'+  currentgroup +'" },' + option;
									

									if(jsondata=='')
									{
										jsondata = EquipemetAppend;
									}
									else
									{
										jsondata = jsondata + ',' + EquipemetAppend;
									}
									
									 option = '{ "id": "' + EquipementId + '", "parent": "' + GroupName + '", "text": "'+  EquipmentName +'" , "icon" : "" }';
									currentgroup = GroupName;
									
									if(i == equipments.length-1)
									 {
										
										var EquipemetAppend = '{ "id": "' + currentgroup + '", "parent": "#", "text": "'+  currentgroup +'" },' + option;
										
										if(jsondata=='')
										{
											jsondata = EquipemetAppend;
										}
										else
										{
											jsondata = jsondata + ',' + EquipemetAppend;
										}
											
										option='';
										currentgroup = GroupName;											
									 }										
							 		}
						 		} 
					 		}
					 
					 
					 
					 
					 //////;
					  jsondata = eval("[" + jsondata + "]");
					  createEquipementJSTree(jsondata);
					 }
				}  
        },
        error: function(data){ 
      	  //////;
       	 //alert(data); 
       	console.log(data);
         }
        });
	  
	
	 
	  
	  
	}






function createEquipementJSTree(jsondata) {
    $('#EquipmentJSTree').jstree({
        "core": {                    
            'data': jsondata
        },
        "types" : {
        	
          },
        "plugins": ["search","checkbox","types"],
        "search": {
            "case_sensitive": false,
            "show_only_matches": true
        }
    });
}



/*
function ParameterAjax(SiteId) {
	//////;
	
	 $('#ParameterJSTree').jstree('destroy');
	//////;
	
	 var jsondata = '';
	 
	 // alert(1);
	  //////;
	  $.ajax({
        type: 'POST',
        url: './filterequipmentparameters' + SiteId,
       
        success: function(data){ 
      	  //////;
			if(data != null) {
				var equipmentparameters = data.equipmentparameters;
				 if(equipmentparameters != null) {
					 
					
					 
					
					 var option ='';
					 
					 
					 for (var i=0; i<equipmentparameters.length;i++) {
						var equipment =  equipmentparameters[i];
						 	
						if(equipment != null) {								
							 var ParameterRef = equipment[1];
							 var ParameterName = equipment[2];
							
							 if(i==0)
							 {
								 option = '{ "id": "' + ParameterRef + '", "parent": "#", "text": "'+  ParameterName +'", "icon" : ""  }';
							 }
							 else 
							 {
								 option = option + ',{ "id": "' + ParameterRef + '", "parent": "#", "text": "'+  ParameterName +'", "icon" : ""  }';
							 }
							
							 
						 } 
					 }
					 
					 
					 
					 
					 //////;
					  jsondata = eval("[" + option + "]");
					  createParameterJSTree(jsondata);
					 }
				}  
        },
        error: function(data){ 
      	  //////;
       	 //alert(data); 
       	console.log(data);
         }
        });
	  
	  
	  
	  
	}*/


function createParameterJSTree(jsondata) {
    $('#ParameterJSTree').jstree({
        "core": {                    
            'data': jsondata
        },
        "types" : {
        	
          },
        "plugins": ["search","checkbox","types"],
        "search": {
            "case_sensitive": false,
            "show_only_matches": true
        }
    });
}




function EquipmentAjaxCall(SiteId) {
	
	
	//$("#EquipmentTree").empty();
	//$(":ui-fancytree").fancytree("destroy");	
	//$("#ParameterTree").fancytree("destroy");
	
	/* var treeid = $("#EquipmentTree").fancytree();
	 if(typeof treeid != 'undefined' && treeid !== undefined) {
		// $(":ui-fancytree").fancytree("destroy");
		 $("#EquipmentTree:ui-fancytree").fancytree("destroy");
	 }*/
	 //alert('Destroy');
	 $("#EquipmentTree:ui-fancytree").fancytree("destroy");
	 
	 
	//////;
	
	 var jsondata = '';
	 
	 // alert(1);
	  //////;
	  $.ajax({
        type: 'POST',
        url: './filterequipments' + SiteId,
       
        success: function(data){ 
      	  //////;
			if(data != null) {
				var equipments = data.equipments;
				 if(equipments != null) {
					 
					
					 
					 var currentgroup = '';
					 var optgroup= '';
					 var option ='';
					 var cnt = 0;
					 var isfirsttreecreated = false;
					 
					 
					 for (var i=0; i<equipments.length;i++) {
						var equipment =  equipments[i];
						 	
						if(equipment != null && equipments.length > 1 ) {								
							 var EquipementId = equipments[i][1];
							 var GroupName = equipments[i][3];
							 var EquipmentName = equipments[i][4];
							
							 if(i==0)
							 {
								 option = '{"title": "'+  EquipmentName +'", "type": "' + GroupName + '", "id": "' + EquipementId + '"}';
								 cnt = cnt + 1;
								 currentgroup = GroupName;
								 
							 }
							 else if(currentgroup == GroupName)
							 {
								 option = option + ',{"title": "'+  EquipmentName +'", "type": "' + GroupName + '", "id": "' + EquipementId + '"}';
								 cnt = cnt + 1;
								 
								 if(i == equipments.length-1)
								 {
									
									var EquipmentAppend = '{"title": "' + currentgroup + ' (' + cnt + ')'  + '", "expanded": true, "folder": true, "id": "' + currentgroup + '", "children": [' + option + ']}';
										
									if(isfirsttreecreated)
									{
										EquipmentAppend = '{"title": "' + currentgroup + ' (' + cnt + ')'  + '", "expanded": false, "folder": true, "id": "' + currentgroup + '", "children": [' + option + ']}';
									}
									
									
									isfirsttreecreated = true;
									cnt = 0;
									option='';
									
									if(jsondata=='')
									{
										jsondata = EquipmentAppend;
									}
									else
									{
										jsondata = jsondata + ',' + EquipmentAppend;
									}
										
									
									
									currentgroup = GroupName;										
								 }
								
							 }
							 else
							 {
								    
								    var EquipmentAppend = '{"title": "' + currentgroup + ' (' + cnt + ')'  + '", "expanded": true, "folder": true, "id": "' + currentgroup + '", "children": [' + option + ']}';
									
									if(isfirsttreecreated)
									{
										EquipmentAppend = '{"title": "' + currentgroup + ' (' + cnt + ')'  + '", "expanded": false, "folder": true, "id": "' + currentgroup + '", "children": [' + option + ']}';
									}
																		
									isfirsttreecreated = true;									
								    cnt = 0;

									if(jsondata=='')
									{
										jsondata = EquipmentAppend;
									}
									else
									{
										jsondata = jsondata + ',' + EquipmentAppend;
									}
									
									option = '{"title": "'+  EquipmentName +'", "type": "' + GroupName + '", "id": "' + EquipementId + '"}';
									cnt = cnt + 1;
									currentgroup = GroupName;
									
									if(i == equipments.length-1)
									 {
										
										var EquipmentAppend = '{"title": "' + currentgroup + ' (' + cnt + ')'  + '", "expanded": false, "folder": true, "id": "' + currentgroup + '", "children": [' + option + ']}';
										cnt = 0;
										option='';
										
										if(jsondata=='')
										{
											jsondata = EquipmentAppend;
										}
										else
										{
											jsondata = jsondata + ',' + EquipmentAppend;
										}
											
										
										currentgroup = GroupName;											
									 }										
							 		}
						 		}else if(equipment!=null){
									 var EquipementId = equipments[i][1];
							 var GroupName = equipments[i][3];
							 var EquipmentName = equipments[i][4];
									option = '{"title": "'+  EquipmentName +'", "type": "' + GroupName + '", "id": "' + EquipementId + '"}';
								 cnt = cnt + 1;
								 currentgroup = GroupName;
								 
								 var EquipmentAppend = '{"title": "' + currentgroup + ' (' + cnt + ')'  + '", "expanded": true, "folder": true, "id": "' + currentgroup + '", "children": [' + option + ']}';
								 jsondata = EquipmentAppend;
								}




								
					 		}
					 
					 
					 
					 
					 //////;
					  jsondata = eval("[" + jsondata + "]");
					  createEquipementFancyTree(jsondata);
					 }
				}  
        },
        error: function(data){ 
      	  //////;
       	 //alert(data); 
       	console.log(data);
         }
        });
	  
	
	 
	  
	  
	}


function ParameterAjaxCall(SiteId) {
	//////;
	
	//$("#ParameterTree").empty();
	 var treeid = $("#ParameterTree").fancytree();
	 if(typeof treeid != 'undefined' && treeid !== undefined) {
		 //$(":ui-fancytree").fancytree("destroy");
		 $("#ParameterTree:ui-fancytree").fancytree("destroy");
	 }
	
	//$("#ParameterTree").fancytree("destroy");
	
	
	 $("#ParameterTree:ui-fancytree").fancytree("destroy");
	
	//////;
	
	 var jsondata = '';
	 
	 // alert(1);
	  //////;
	  $.ajax({
        type: 'POST',
        url: './filterequipmentparameters' + SiteId,
       
        success: function(data){ 
      	  //////;
			if(data != null) {
				var equipmentparameters = data.equipmentparameters;
				 if(equipmentparameters != null) {
					 
					
					 
					
					 var option ='';
					 
					 
					 for (var i=0; i<equipmentparameters.length;i++) {
						var equipment =  equipmentparameters[i];
						 	
						if(equipment != null) {								
							 var ParameterRef = equipment[1];
							 var ParameterName = equipment[2];
							
							 if(i==0)
							 {
								 option = '{"title": "'+  ParameterName +'",  "id": "' + ParameterRef + '"}';
									
							 }
							 else 
							 {
								 option = option + ',{"title": "'+  ParameterName +'",  "id": "' + ParameterRef + '"}';
							 }
							
							 
						 } 
					 }
					 
					 
					 
					 
					 //////;
					  jsondata = eval("[" + option + "]");
					  createParameterFancyTree(jsondata);
					 }
				}  
        },
        error: function(data){ 
      	  //////;
       	 //alert(data); 
       	console.log(data);
         }
        });
	  
	  
	  
	  
	}





function ParameterClickAjaxCall(SiteId,EquipmentsIDs) {
	
	
	 var jsondata = '';
	 
	 // alert(1);
	  //////;
	  $.ajax({
        type: 'POST',
        url: './filterstandardparameters',
       data: {'EquipmentIDList': SiteId,'EquipmentsIDs': EquipmentsIDs},                
                   
       
        success: function(data){
        	
      	  //////;
			if(data != null) {
				var equipmentparameters = data.equipmentparameters;
				 if(equipmentparameters != null) {
					 
					
					 
					
					 var option ='';
					 
					 
					 for (var i=0; i<equipmentparameters.length;i++) {
						var equipment =  equipmentparameters[i];
						 	
						if(equipment != null) {								
							 var ParameterRef = equipment[1];
							 var ParameterName = equipment[2];
							
							 if(i==0)
							 {
								 option = '{"title": "'+  ParameterName +'",  "id": "' + ParameterRef + '"}';
									
							 }
							 else 
							 {
								 option = option + ',{"title": "'+  ParameterName +'",  "id": "' + ParameterRef + '"}';
							 }
							
							 
						 } 
					 }
					 
					 
					 
					 
					 //////;
					  jsondata = eval("[" + option + "]");
					  $("#ParameterTree:ui-fancytree").fancytree("destroy");
					  createParameterFancyTree(jsondata);
					 }
				}  
        },
        error: function(data){ 
      	  //////;
       	 //alert(data); 
       	console.log(data);
         }
        });
	  
	  
	  
	  
	}






function createEquipementFancyTree(jsondata) {
	// Initialize Fancytree
			$("#EquipmentTree").fancytree({
				extensions: ["edit", "glyph", "wide"],
				checkbox: true,
				selectMode: 3,
				click: function(event, data) { 
					//////;
					if( data.node.isFolder() ){
						if(data.targetType == 'expander')
						{
							return true;
						}
						else if (data.targetType == 'checkbox') {
							if(data.node.selected) {
						    	//////;	
						    	data.node.setSelected(false);
								for(var i =0; i < data.node.children.length; i++)
								{
									var title = data.node.children[i].title;
									var id = data.node.children[i].data.id;
							       	
							       	$('#btnclose' + id).click();
								}
						       	
						     }
						     else {
						    	 data.node.setSelected(true);
						    	 for(var i =0; i < data.node.children.length; i++)
									{
										var title = data.node.children[i].title;
								       	var id = data.node.children[i].data.id;
								       	
								       	if (window['tagbx' + id]) {
											//no need of adding new node
										} else {
											$('.filter-labels').append('<div id="tagbx' + id + '" class="alert alert-info btn-alert-danger" role="alert" title="' + title + '"><button id="btnclose' + id + '" class="tagclose close" data-dismiss="alert"></button>' + title + '</div>');
										}	
								  }
							       	
						       	
						       	
						     }
							 
							return false;
				        }
						else
						{
							//////;
							 if(data.node.expanded) {
						       	data.node.setExpanded(false);						       
						     }
						     else {
						    	 data.node.setExpanded(true);							       
						     }
							
							
							 return false;
						}
						 
					 }
					 else
					 {
						 if(data.node.selected) {
					       	data.node.setSelected(false);
					    	
					       	var title = data.node.title;
					       	var id = data.node.data.id;
					       	
					       	$('#btnclose' + id).click();
					     }
					     else {
					       	data.node.setSelected(true);
					    	
					    	var title = data.node.title;
					       	var id = data.node.data.id;
					       	
					       	
					       	$('.filter-labels').append('<div id="tagbx' + id + '" class="alert alert-info btn-alert-danger" role="alert" title="' + title + '"><button id="btnclose' + id + '" class="tagclose close" data-dismiss="alert"></button>' + title + '</div>');
					    
					     }
						 return false;
					 }
				 },
				 select: function(event, data) {
					 fancytreecheckchangeeventforequipment();
				      },
				      activate: function(event, data) {
				         return false;
				        },
				dnd: {
					focusOnClick: true,
					dragStart: function(node, data) { return true; },
					dragEnter: function(node, data) { return false; },
					dragDrop: function(node, data) { data.otherNode.copyTo(node, data.hitMode); }
				},
				glyph: glyph_opts,
				source:  jsondata,
				wide: {
					iconWidth: "1em",       // Adjust this if @fancy-icon-width != "16px"
					iconSpacing: "0.5em",   // Adjust this if @fancy-icon-spacing != "3px"
					labelSpacing: "0.1em",  // Adjust this if padding between icon and label != "3px"
					levelOfs: "1.5em"       // Adjust this if ul padding != "16px"
				},

				icon: function(event, data){				
				},
				lazyLoad: function(event, data) {
					data.result =  [ {"title": "Sub item", "lazy": true }, {"title": "Sub folder", "folder": true, "lazy": true } ];
				}
			});
			
			//////;
			
			

}

function createParameterFancyTree(jsondata) {
	// Initialize Fancytree
			$("#ParameterTree").fancytree({
				extensions: ["glyph"],
				checkbox: true,
				selectMode: 3,
				click: function(event, data) { 
					
				/*	if (data.targetType !== 'checkbox') {
			              data.node.toggleSelected();
			            }*/
					
					
					 if( data.node.isFolder() ){
						 return true;
					 }
					 else
					 {
						 
						 
						 if(data.node.selected) {
						       	data.node.setSelected(false);
						    	
						       	var title = data.node.title;
						       	var id = data.node.data.id;
						       	
						       	$('#btnparaclose' + id).click();
						       	
						     }
						     else {
						       	data.node.setSelected(true);
						    	
						    	var title = data.node.title;
						       	var id = data.node.data.id;
						       	
						       	
						       	$('.filter-labels').append('<div id="tagparabx' + id + '" class="alert alert-success btn-alert-danger" role="alert" title="' + title + '"><button id="btnparaclose' + id + '" class="tagparaclose close" data-dismiss="alert"></button>' + title + '</div>');
						    
						     }
							 return false;
							 
							 
					 }
				       
				 },
				
				 select: function(event, data) {
					 fancytreecheckchangeeventforparameter();
				      },
				      activate: function(event, data) {
					         return false;
					        },
				dnd: {
					focusOnClick: true,
					dragStart: function(node, data) { return true; },
					dragEnter: function(node, data) { return false; },
					dragDrop: function(node, data) { data.otherNode.copyTo(node, data.hitMode); }
				},
				glyph: glyph_opts,
				source:  jsondata,
				wide: {
					iconWidth: "1em",       // Adjust this if @fancy-icon-width != "16px"
					iconSpacing: "0.5em",   // Adjust this if @fancy-icon-spacing != "3px"
					labelSpacing: "0.1em",  // Adjust this if padding between icon and label != "3px"
					levelOfs: "1.5em"       // Adjust this if ul padding != "16px"
				},

				icon: function(event, data){				
				},
				lazyLoad: function(event, data) {
					data.result =  [ {"title": "Sub item", "lazy": true }, {"title": "Sub folder", "folder": true, "lazy": true } ];
				}
			});
			
			//////;
			
			
			
			

}





function reloadEquipementFancyTree(jsondata) {
			
	var tree = $("#EquipmentTree").fancytree("getTree");
	tree.reload({  source: jsondata	}).done(function() {     });

}

function reloadParameterFancyTree(jsondata) {
	
	var tree = $("#ParameterTree").fancytree("getTree");
	tree.reload({  source: jsondata	}).done(function() {     });
}




function DailyEnergyGenerationAjax(Range,GraphCount,Name1,Name2,Data1, Data2) {

	var varData1 = eval(Data1);
	var varData2 = eval(Data2);
	var varColumnOrspline = 'column';	
	var varXaxis= 'Time';
	
	var vartickinterval =  48 * 60 * 36000;
	if(Range == 'daily') {
		vartickinterval =  120 * 36000;			
	}
	
	
	   var varyaxisvalue = '';  
	   var varseriesdata = '';
	    
	 var units = $('#IrradiationUnits').val();
	    
	if(Range=='daily') 	{  varColumnOrspline = 'areaspline'; varXaxis= 'Time (In Hours)';}
	else { varXaxis= 'Date (In Days)';	}
	
	
	if(Data1 == ''){
		$('.ndf').show();
	}
	else {
		$('.ndf').hide();
		
		varyaxisvalue = "{min:0,  opposite: false, title: {text: 'Energy Generation (Wh)', style: {fontWeight:'bold',color:'#000' }}}";
		varseriesdata = "{type: '" + varColumnOrspline + "', name: 'Energy Generation', data: " + Data1 + ",  yAxis: 0, color: '#238ca7',lineColor: '#1a88a5' }";
		
		
		
		if(GraphCount==2)
		{	
			varyaxisvalue = "{min:0,  opposite: false, title: {text: 'Energy Generation (Wh)', style: {fontWeight:'bold' }}},{min:0,  opposite: true, title: {text: 'Irradiation ("+ units +")', style: {fontWeight:'bold' }}}";
			varseriesdata = "{type: '" + varColumnOrspline + "', name: 'Energy Generation', data: " + Data1 + ",  yAxis: 0, color: '#238ca7',lineColor: '#1a88a5' },{type: 'spline',dashStyle: 'longdash', name: '" + Name2 + "', data: " + Data2 + ",  yAxis: 1,lineColor: '#7cb5ec' }";
		}
		
		
		varyaxisvalue = eval("[" + varyaxisvalue + "]");
		varseriesdata = eval("[" + varseriesdata + "]");
		
		/*((H) => {
			  H.Tooltip.prototype.defaultFormatter = function(tooltip) {
			    const items = this.points || H.splat(this)

			    // Build the header
			    const header = [tooltip.tooltipFooterHeaderFormatter(items[0])]
			      // build the values
			    const values = tooltip.bodyFormatter(items)
			    console.log(items);
			      // footer
			    const footer = tooltip.tooltipFooterHeaderFormatter(items[0], true)
			      // Add custom text
			    values[0] += 'custom text'
			    return [...header, ...values, ...footer]
			  }
			})(Highcharts)
			*/
		Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
		 Highcharts.chart('chartData1', {
			    chart: {
			        type: 'spline'
			    },
			    title: {
			        text: 'Daily Energy Generation - ' + Name1,			        
			    },
			    credits: {
			        enabled: false
			      },
			    
			    subtitle: {
			    	/*text: 'Daily Energy Generation - ' + Name1*/
			    	
			    },
			    
			    exporting: {
			        menuItemDefinitions: {
			            // Custom definition
			            label: {
			                onclick: function () {
			                    this.renderer.label(
			                        'You just clicked a custom menu item',
			                        100,
			                        100
			                    )
			                    .attr({
			                        fill: '#a4edba',
			                        r: 5,
			                        padding: 10,
			                        zIndex: 10
			                    })
			                    .css({
			                        fontSize: '1.5em'
			                    })
			                    .add();
			                },
			              
			            }
			        },
			        filename: 'Daily Energy Generation - ' + Name1,
			        buttons: {
			            contextButton: {
			                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
			            }
			        }
			    },
				 plotOptions: {
			        series: {
			            allowPointSelect: false,
			            marker: {
			                enabled: false
			            },
			            
			            /*events: {
			                legendItemClick: function () {
			                	if(this.name =='Energy Generation')
			                    {
			                    		return false;
			                    }
			                    else
			                    {
			                    		return true;
			                    }
			                    
			                }
			            }*/
			        }
			    },
			    tooltip: {
			        shared: false,
			        useHTML: true,
			        formatter: function() {
			        	 var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
		            	 var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

			            var text = '';
			            if(this.series.name == 'Energy Generation') {
			            	var ret,numericSymbols = ['kWh', 'MWh', 'GWh', 'TWh', 'PWh', 'EWh'],
	                        i = 6;
		            	 //var d = new Date(this.x);
		            	
		            	var d =  convertDateToUTC(new Date(this.x));
		            	 
		            	 //d.setTime(parseInt(this.x));
		            	
		            	 var s='<b>'+days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+', '+d.getHours()+':'+d.getMinutes()+'</b>';
		            	 
		            	  s += ' <span style="color:' + this.color + '">\u25CF</span> ' + this.series.name + ': ';
	                    if(this.y >=1000) {
	                        while (i-- && ret === undefined) {
	                            multi = Math.pow(1000, i + 1);
	                            if (this.y >= multi && numericSymbols[i] !== null) {
	                                ret = (this.y / multi) + ' '+numericSymbols[i];
	                            }
	                        }
	                    }
	                    return s+(ret ? ret : this.y+' Wh');
			            } else {
			            	 var ret,numericSymbols = ['kWh', 'MWh', 'GWh', 'TWh', 'PWh', 'EWh'],
		                        i = 6;
			            	// var d = new Date(this.x);
			            	 var d =  convertDateToUTC(new Date(this.x));
			            	// d.setTime(parseInt(this.x));
			            	 var s='<b>'+days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+', '+d.getHours()+':'+d.getMinutes()+'</b>';
			            	  s += ' <span style="color:' + this.color + '">\u25CF</span> ' + this.series.name + ': ';
		                    if(this.y >=1000) {
		                        while (i-- && ret === undefined) {
		                            multi = Math.pow(1000, i + 1);
		                            if (this.y >= multi && numericSymbols[i] !== null) {
		                                ret = (this.y / multi) + ' '+'W/m<sup>2</sup>';
		                            }
		                        }
		                    }
		                    return s+(ret ? ret : this.y+' W/m<sup>2</sup>');
			            }
			            return text;
			        }
			    },
			    xAxis: {
			        type: 'datetime',
			        
			        tickInterval:vartickinterval,
			        title :{
	                    text: varXaxis,	                    
	                    style: {
	                    color: 'black',
	                    fontWeight: 'bold',
	                    fontSize: '12px',
	                    fontFamily: 'Lucida Sans Unicode", Arial, Helvetica, sans-serif'
	                    }   

			        }
			    },
			    yAxis: varyaxisvalue,  
			    series: varseriesdata
		 });
		 
		 
		 
		 
		
	}
	
	
}

function EnergyPerformanceAjax(Range,GraphCount,Name1,Name2,Data1, Data2, EnergySummary) {
	
	var varData1 = eval(Data1);
	var varData2 = eval(Data2);
	var varType = 'areaspline';	
	var varXaxis= 'Time';
	
	var varyaxisvalue = '';  
	var varseriesdata = '';
	   
	if(Range=='daily') 	{ varXaxis= 'Time (In Hours)';}
	else { varXaxis= 'Date (In Days)';	}
	
	
	
	 var units = $('#IrradiationUnits').val();
	
	
	if(Data1 == ''){
		$('.ndf').show();
	}
	else {
		$('.ndf').hide();
		
		
		varyaxisvalue = "{min:0,  opposite: false, title: {text: 'Energy Generation (Wh)'}, style: {fontWeight:'bold',color:'#7cb5ec' }}";
		varseriesdata = "{type: '" + varType + "', name: '" + EnergySummary + "', data: " + Data1 + ",  yAxis: 0,lineColor: '#7cb5ec' }";
		
			
		
		if(GraphCount==2)
		{	
			varyaxisvalue = "{min:0,  opposite: false, title: {text: 'Energy Generation (Wh)'}},{min:0,  opposite: true, title: {text: 'Irradiation ("+units+")',color:'#7cb5ec'}}";
			varseriesdata = "{type: '" + varType + "', name: '" + EnergySummary + "', data: " + Data1 + ",  yAxis: 0, lineColor: '#1a88a5' },{type: 'spline',dashStyle: 'longdash', name: '" + Name2 + "', data: " + Data2 + ",  yAxis: 1,lineColor: '#000' }";
		}
		
		
		
		var vartickinterval =  48 * 60 * 36000;
		if(Range == 'daily') {
			vartickinterval =  120 * 36000;			
		}
		
			
		varyaxisvalue = eval("[" + varyaxisvalue + "]");
		varseriesdata = eval("[" + varseriesdata + "]");
			
		 
		
		Highcharts.chart('chartData2', {
			    chart: {
			        type: 'spline'
			    },
			    title: {
			        text: ' ',			        
			    },
			    credits: {
			        enabled: false
			      },
			    subtitle: {
			    	text: 'Energy Performance - ' + Name1,
			    	color:'black'
			    },
			    
			    exporting: {
			        menuItemDefinitions: {
			            // Custom definition
			            label: {
			                onclick: function () {
			                    this.renderer.label(
			                        'You just clicked a custom menu item',
			                        100,
			                        100
			                    )
			                    .attr({
			                        fill: '#a4edba',
			                        r: 5,
			                        padding: 10,
			                        zIndex: 10
			                    })
			                    .css({
			                        fontSize: '1.5em'
			                    })
			                    .add();
			                },
			              
			            }
			        },
			        filename: 'Energy Performance - ' + Name1,
			        buttons: {
			            contextButton: {
			                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
			            }
			        }
			    },
			    /*tooltip: {
			        formatter: function () {
			           var ret,numericSymbols = ['kWh', 'MWh', 'GWh', 'TWh', 'PWh', 'EWh'],
			                        i = 6;
			                    if(this.y >=1000) {
			                        while (i-- && ret === undefined) {
			                            multi = Math.pow(1000, i + 1);
			                            if (this.y >= multi && numericSymbols[i] !== null) {
			                                ret = (this.y / multi) + ' '+numericSymbols[i];
			                            }
			                        }
			                    }
			                    return 'Energy Generation :' + (ret ? ret : this.y);
			        },
			    },*/
				 plotOptions: {
			        series: {
			            allowPointSelect: false,
			            marker: {
			                enabled: false
			            },
			           /* events: {
			                legendItemClick: function () {
			                	if(this.name =='Energy Generation')
			                    {
			                    		return false;
			                    }
			                    else
			                    {
			                    		return true;
			                    }
			                    
			                }
			            }*/
			        }
			    },
			    tooltip: {
			        shared: false,
			        useHTML: true,
			        formatter: function() {
			        	 var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
		            	 var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

			            var text = '';
			            if(this.series.name == EnergySummary) {
			            	var ret,numericSymbols = ['kWh', 'MWh', 'GWh', 'TWh', 'PWh', 'EWh'],
	                        i = 6;
		            	 var d = new Date();
		            	// d.setTime(parseInt(this.x));
		            	 var d =  convertDateToUTC(new Date(this.x));
		            	/* dateString = newDate.toUTCString();*/
		            	 var s='<b>'+days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+', '+d.getHours()+':'+d.getMinutes()+'</b>';
		            	 
		            	  s += ' <span style="color:' + this.color + '">\u25CF</span> ' + this.series.name + ': ';
	                    if(this.y >=1000) {
	                        while (i-- && ret === undefined) {
	                            multi = Math.pow(1000, i + 1);
	                            if (this.y >= multi && numericSymbols[i] !== null) {
	                                ret = (this.y / multi) + ' '+numericSymbols[i];
	                            }
	                        }
	                    }
	                    return s+(ret ? ret : this.y+' Wh');
			            } else {
			            	 var ret,numericSymbols = ['kWh', 'MWh', 'GWh', 'TWh', 'PWh', 'EWh'],
		                        i = 6;
			            	 var d = new Date();
			            	// d.setTime(parseInt(this.x));
			            	 var d =  convertDateToUTC(new Date(this.x));
			            	 var s='<b>'+days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+', '+d.getHours()+':'+d.getMinutes()+'</b>';
			            	  s += ' <span style="color:' + this.color + '">\u25CF</span> ' + this.series.name + ': ';
		                    if(this.y >=1000) {
		                        while (i-- && ret === undefined) {
		                            multi = Math.pow(1000, i + 1);
		                            if (this.y >= multi && numericSymbols[i] !== null) {
		                                ret = (this.y / multi) + ' '+'W/m<sup>2</sup>';
		                            }
		                        }
		                    }
		                    return s+(ret ? ret : this.y+' W/m<sup>2</sup>');
			            }
			            return text;
			        }
			    },
			    xAxis: {
			        type: 'datetime',
			        
			        tickInterval:vartickinterval,
			        title :{
	                    text: varXaxis,
	                    style: {
	                    color: 'black',
	                    fontWeight: 'bold',
	                    fontSize: '12px',
	                    
	                    fontFamily: 'Lucida Sans Unicode", Arial, Helvetica, sans-serif'
	                    }   

			        }
			    },
			    yAxis: varyaxisvalue,  
			    series: varseriesdata
		 });
		 
		 
		
	
	}
	
	
}


function ParameterComparisonAjax(data){
	
	 var firstname = '';
	 var seriesdata = "";
	 var yaxisvalue =""; 
	 var iCnt = 0; 
	 var h = new Object(); 
	 var dataarray = data.split('&&');
	 
	 var arruom=[];
	  if(dataarray.length > 0)
	  {        			  
		  for(var i=0;i < dataarray.length; i++)
		  {
			 
			  var datavalue = dataarray[i];
			  var datavaluearray = datavalue.split('||');
			  if(datavaluearray.length >= 5)
			  {        			  
				  arruom[datavaluearray[1]]=datavaluearray[2];
				   
					  var equipmentname = datavaluearray[0];
					  var fieldname = datavaluearray[1];
					  var axisname = datavaluearray[0] + ' - ' +  datavaluearray[1];
					  var graphname = datavaluearray[3] + ' (' +  datavaluearray[2] + ')';
					  var uom = datavaluearray[2];
					  var axisgroup = datavaluearray[3];
					  var axisdata = datavaluearray[4];
					  

					  var isexist = false;
					  var keyvalue = "0";
					  // show the values stored
					  for (var k in h) {
					      // use hasOwnProperty to filter out keys from the Object.prototype
					      if (h.hasOwnProperty(k)) {
					         
					    	  if(h[k]== axisgroup)
					    	  {
					    		  isexist = true;
					    		  keyvalue = k;
					    	  }
					    	  
					      }
					  }
					  
					  
					  if(isexist == false)
					  {
						  h[iCnt] = axisgroup;
						  keyvalue = iCnt;
						  var opposite  = "true";
						 
						  if(iCnt%2 == 0)
						  {
							  opposite = "false";	
						  }
						  
						  if(iCnt==0)
						  {
							  yaxisvalue = "{className: 'highcharts-color-" + iCnt + "', opposite: " + opposite + ", title: {text: '" + graphname + "'}}";
						  }
						  else
						  {
							  yaxisvalue = yaxisvalue + ",{className: 'highcharts-color-" + iCnt + "', opposite: " + opposite + ", title: {text: '" + graphname + "'}}";
						  }
						  

						  iCnt = iCnt +1;
						  
					  }
					  
					  
					  
					  
					  if(i==0)
					  {
						  firstname = axisname;
						  seriesdata = "{ name: '" + axisname + "', data: " + axisdata + ",  yAxis: " + keyvalue + " }";
					  }
					  else
					  {
						  seriesdata = seriesdata + ", { name: '" + axisname + "', data: " + axisdata + ",  yAxis: " + keyvalue + " }";
					  }
					  
					  
			  }
			
		  }
		  
		  //////;
		  
		  
		  
		  if (seriesdata == '') {
			  $('.ndf').show();
		  }
		  else {
			  yaxisvalue = "[" + yaxisvalue + "]";	  
			  seriesdata = "[" + seriesdata + "]";
			  
			  var varyaxisvalue = eval(yaxisvalue);
			  var varseriesdata = eval(seriesdata);
			  
			  $('.ndf').hide();
			
			 var Range='daily';

			  var vartickinterval =  48 * 60 * 36000;
			  if(Range == 'daily') {
				  vartickinterval =  120 * 36000;			
			  }
			  

			  var varXaxis= 'Time';
			  if(Range=='daily') 	{ varXaxis= 'Time (In Hours)';}
			  else { varXaxis= 'Date (In Days)';	}
	
		
			  Highcharts.chart('chartData3', {
			    chart: {
			        type: 'spline'
			    },

			    title: {
			        text: 'Parameter Comparison - ' + $('#ddlSite option:selected').text()
			    },
			    credits: {
			        enabled: false
			      },
			    
			    exporting: {
			        menuItemDefinitions: {
			            // Custom definition
			            label: {
			                onclick: function () {
			                    this.renderer.label(
			                        'You just clicked a custom menu item',
			                        100,
			                        100
			                    )
			                    .attr({
			                        fill: '#a4edba',
			                        r: 5,
			                        padding: 10,
			                        zIndex: 10
			                    })
			                    .css({
			                        fontSize: '1.5em'
			                    })
			                    .add();
			                },
			              
			            }
			        },
			        filename: 'Parameter Comparison - ' + $('#ddlSite option:selected').text(),
			        buttons: {
			            contextButton: {
			                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
			            }
			        }
			    },
				 plotOptions: {
			        series: {
			            allowPointSelect: false,
			            marker: {
			                enabled: false
			            },
			          /*  events: {
			                legendItemClick: function () {
			                    if(this.name == firstname)
			                    {
			                    		return false;
			                    }
			                    else
			                    {
			                    		return true;
			                    }
			                    
			                }
			            }*/
			        }
			    },
			    tooltip: {
			        shared: false,
			        useHTML: true,
			        formatter: function() {
			        	 var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
		            	 var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

			            var text = '';
			            	var ret,numericSymbols = ['k','M','GWh', 'TWh', 'PWh', 'EWh'],
	                        i = 6;
		            	 //var d = new Date();
			            	var d =  convertDateToUTC(new Date(this.x));
		            	 //d.setTime(parseInt(this.x));
		            	 var s='<b>'+days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+', '+d.getHours()+':'+d.getMinutes()+'</b>';
		            	  s += ' <span style="color:' + this.color + '">\u25CF</span> ' + this.series.name.split('(')[0] + ': ';
	                    if(this.y >=1000) {
	                        while (i-- && ret === undefined) {
	                            multi = Math.pow(1000, i + 1);
	                            if (this.y >= multi && numericSymbols[i] !== null) {
	                                ret = (this.y / multi) + ''+numericSymbols[i];
	                            }
	                        }
	                    }else{
	                    	ret=this.y;
	                    }
	                  /*  var ind="";
	                    if(this.series.name.split('-')[1].trim()){
	                    	ind=this.series.name.split('-')[1].trim();
	                    }else{
	                    	ind=this.series.name.split('-')[2].trim();
	                    }*/
	                    
	                    return s+ret+" "+arruom[((arruom[this.series.name.split('-')[1].trim()])?this.series.name.split('-')[1].trim():this.series.name.split('-')[2].trim())];
			            
			        }
			    },
			 	
			    xAxis: {
			        type: 'datetime',
			        
			        tickInterval:vartickinterval,
			        title :{
	                    text: varXaxis,
	                    style: {
	                    color: 'black',
	                    fontWeight: 'bold',
	                    fontSize: '12px',
	                    fontFamily: 'Lucida Sans Unicode", Arial, Helvetica, sans-serif'
	                    }   

			        }
			    },
			    yAxis: varyaxisvalue,  
			    series: varseriesdata
		 });
			
		  }
		  
	  }
	  else 
		  {
		  $('.ndf').show();
		  }
	
	  
}


function SelectedFancyTreeElement(TreeName)
{
	var Result = '';
	try
	{
		var Node = $('#' + TreeName).fancytree('getTree').getSelectedNodes();
		var ids = '';
		for(var i =0; i < Node.length; i++)
		{
			if(i==0)
				{ids = Node[i].data.id; }
			else
			{
				ids = ids + ',' + Node[i].data.id; 
			}
		}
		
		Result = ids.toString();
		
		
	}
	catch(err)
	{
		
	}
	
	return Result;
	
}


function SelectedEquipmentsForEnergyPerformance(TreeName)
{
	var Result = '';
	try
	{
		var Node = $('#' + TreeName).fancytree('getTree').getSelectedNodes();
		var ids = '';
		var a = 0;
		for(var i =0; i < Node.length; i++)
		{
			if(!Node[i].isFolder()) 
			{
				if(a==0)
				{ids = Node[i].title; }
				else if(a < 5)
				{
					ids = ids + ',' + Node[i].title; 
				}
				a= a + 1;
				
			}
			
		}
		
		Result = ids.toString();
		
	
		
	}
	catch(err)
	{
		
	}
	
	if(Result != "" && Node.length >= 5)
	{
		Result = "Energy Generation Summary for selected Equipments";
	}
	else if(Result != "" && Node.length == 1)
	{
		Result = "Energy Generation (" + Result + ")";
	}
	else if(Result != "" && Node.length > 0 && Node.length < 5)
	{
		Result = "Energy Generation Summary (" + Result + ")";
	}
	else
	{
		Result = "Energy Generation Summary";
	}
	return Result;
	
}


var energyFlag=false;
var irradiationFlag=false;
var energyData="";
var irradiationData="";



function DataPopulateAjaxCall() {
{
	energyFlag=false;
	irradiationFlag=false;
	energyData="";
	irradiationData="";
	
	$('#chartData1').html("");
	$('#chartData2').html("");
	$('#chartData3').html("");

		
	//////;
	var SummaryType = 'Individual';
	var GraphType = $('#hdnGraphType').val();
	var FromDate = $('#hdnFromDate').val();
	var ToDate = $('#hdnToDate').val();
	var Range = $('#hdnSelectedDataRange').val();
	var SiteID =$('#ddlSite').val();
	var EquipmentIDList = '';
	var EquipmentIDWithParameterList ='';
	
	if(GraphType=="Daily Energy Generation")
	{
		SummaryType = "Summary";
		
		if(FromDate==ToDate) 
		{
			Range = 'daily';
		}
		else
		{
			Range = 'custom';
		}
	}
	else if(GraphType=="Energy Performance")
	{
		SummaryType = "Summary";
		if(FromDate==ToDate) 
		{
			Range = 'daily';
		}
		else
		{
			Range = 'custom';
		}
	}
	
	//////;
	

	var EnergySummary = '';
	
	try
	{
		EquipmentIDList = SelectedFancyTreeElement("EquipmentTree");
		
		
		
		EquipmentIDWithParameterList = SelectedFancyTreeElement("ParameterTree");
		EnergySummary = SelectedEquipmentsForEnergyPerformance("EquipmentTree");
		

		if(typeof ajax_request !== 'undefined')
		{
			ajax_request.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	
	
	
        
	
	$('.loading-icon').show();
	$('.ndf').hide();
	//$('.loading-gif').show();	
	
	//////;
	
	
	
	
	if(Range=="daily" && GraphType=="Daily Energy Generation" && SummaryType=="Summary"){
		
	//alert("Entering new block");
		
		ajax_request = $.ajax({
	          type: 'POST',
	          url: './chartdatarequest',
	          data: {                
	              'SiteID': SiteID , 
	              'EquipmentIDList': EquipmentIDList , 
	              'EquipmentIDWithParameterList': EquipmentIDWithParameterList , 
	              'Range': Range , 
	              'FromDate': FromDate , 
	              'ToDate': ToDate , 
	              'GraphType': GraphType , 
	              'SummaryType': SummaryType,
	              'Tag':'energy'
	          },
	          success: function(data){
	        	  
	        	  if(data != "")
	        	  {
                    //  alert("Energy"+data);
                       energyFlag=true;
                       energyData=data;
                       afterDailyMerge();
	        	  }
	        	
	          },          
	          error: function(data){
	        	 
	          }
	          
		  });
		
		
		ajax_request = $.ajax({
	          type: 'POST',
	          url: './chartdatarequest',
	          data: {                
	              'SiteID': SiteID , 
	              'EquipmentIDList': EquipmentIDList , 
	              'EquipmentIDWithParameterList': EquipmentIDWithParameterList , 
	              'Range': Range , 
	              'FromDate': FromDate , 
	              'ToDate': ToDate , 
	              'GraphType': GraphType , 
	              'SummaryType': SummaryType,
	              'Tag':'irradiance'
	          },
	          success: function(data){
	        	  
	        	  if(data != "")
	        	  {

	        		 // alert("Irradiance"+data);
	        		  
	        		   irradiationFlag=true;
	        		   irradiationData=data;
	        		   afterDailyMerge();
	        		  
	        	  }
	          },          
	          error: function(data){
	        	 
	          }
	          
		  });
		
		
	}
	
	
	
if(Range!="daily" || GraphType!="Daily Energy Generation" || SummaryType!="Summary"){
		
	
	ajax_request = $.ajax({
          type: 'POST',
          url: './chartdatarequest',
          data: {                
              'SiteID': SiteID , 
              'EquipmentIDList': EquipmentIDList , 
              'EquipmentIDWithParameterList': EquipmentIDWithParameterList , 
              'Range': Range , 
              'FromDate': FromDate , 
              'ToDate': ToDate , 
              'GraphType': GraphType , 
              'SummaryType': SummaryType,
              'Tag':null
          },
          success: function(data){
        	  //////;
        	  
        	  if(data != "")
        	  {
        		  if(GraphType == "Parameter Comparison")
        		  {
        			  ParameterComparisonAjax(data);
        		  }
        		  else
        		  {
        			  var dataarray = data.split('&&');
        			  if(dataarray.length > 0)
        			  {        			  
        				  if(dataarray.length == 2)
        				  {
        					  var dataresult1 = dataarray[0].split('||');
        					  var dataresult2 = dataarray[1].split('||');

        					  var name1 = dataresult1[0];
        					  var data1 = dataresult1[1];

        					  var name2 = dataresult2[0];
        					  var data2 = dataresult2[1];

        					  if(GraphType == "Daily Energy Generation" || GraphType == "1")
        						  DailyEnergyGenerationAjax(Range,2,name1,name2,data1,data2);
        					  else if(GraphType == "Energy Performance" || GraphType == "2")
        						
        						  EnergyPerformanceAjax(Range,2,name1,name2,data1,data2,EnergySummary);
        					  else 
        						  $('.ndf').show();
        				  }
        				  else
        				  {
        					  var dataresult1 = dataarray[0].split('||');


        					  var name1 = dataresult1[0];
        					  var data1 = dataresult1[1];

        					  if(GraphType == "Daily Energy Generation" || GraphType == "1")
        						  DailyEnergyGenerationAjax(Range,1,name1,"",data1,"");
        					  else if(GraphType == "Energy Performance" || GraphType == "2")
        						  EnergyPerformanceAjax(Range,1,name1,"",data1,"",EnergySummary);
        					  else 
        						  $('.ndf').show();


        				  }
        			  }
        			  else {
        				  $('.loading-icon').hide();
        				  $('.ndf').show();
        			  }


        		  }
        		 

        	  }
        	  else {
        		  	$('.loading-icon').hide();
        			$('.ndf').show();
        	  }
        		

        		$('.loading-icon').hide();
          },          
          error: function(data){
        	  $('.ndf').show();
        	  $('.loading-icon').hide();
          }
          
	  });
}
	 




	//SiteID, EquipmentIDList,EquipmentIDWithParameterList, Range,FromDate, ToDate, GraphType,SummaryType
}

}




function afterDailyMerge(){
	
//	var energyFlag=false;
//	var irradiationFlag=false;
//	var energyData="";
//	var irradiationData="";
	
	if(energyFlag && irradiationFlag){
		
		alert("Actual data --- "+energyData+irradiationData);
		if(energyData && irradiationData){
			
			  var dataresult1 = energyData.split('||');
			  var dataresult2 = irradiationData.split('||');

			  var name1 = dataresult1[0];
			  var data1 = dataresult1[1];

			  var name2 = dataresult2[0];
			  var data2 = dataresult2[1];

			  DailyEnergyGenerationAjax("daily",2,name1,name2,data1,data2);
			  
		}else{
			var data="";
			if(energyData){
				data=energyData;
			}else{
				data=irradiationFlag;
			}
			
			  var dataresult1 = data.split('||');
			  var name1 = dataresult1[0];
			  var data1 = dataresult1[1];
			  DailyEnergyGenerationAjax("daily",1,name1,"",data1,"");
			
		}
		
	}
	
	
}


function fancytreecheckchangeeventforequipment()
{
	 var Equipmentids = SelectedFancyTreeElement("EquipmentTree");
	
	 var SiteId =  $('#ddlSite option:selected').val();
	
	 ParameterClickAjaxCall(SiteId,Equipmentids);
	 
		
	if(Equipmentids.toString() == "")
	{
			/*$('.btn-equipement-reset').addClass('disabled');
		$('.btn-equipement-apply').addClass('disabled');*/
		
		$('.btn-equipement-apply').prop('disabled',true);	
		$('.btn-equipement-reset').prop('disabled',true);	
		
		            		
		if($('#hdnGraphType').val() == 'Parameter Comparison')
		{
			
			
			$('#parameter-tab').addClass('disabled');
			$(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
		}
		else
		{
			
			$('#parameter-tab').addClass('disabled');
			    $(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
		}
	   
	}
	else
    {
           /*$('.btn-equipement-reset').removeClass('disabled');
           $('.btn-equipement-apply').removeClass('disabled');*/
           

           $('.btn-equipement-apply').prop('disabled',false);  
           $('.btn-equipement-reset').prop('disabled',false);         
           $('.btn-equipement-reset').addClass('btnResetActive');
           
           if($('#hdnGraphType').val() == 'Parameter Comparison')
           {
        	   
        	   $('#parameter-tab').removeClass('disabled');
        	   $(".data-toggle-parameter-add").attr("data-toggle",'tab');
        	   $(".data-toggle-parameter-add").removeClass('disabled'); 
                
                  
                  $('.btn-equipement-apply').on('click', function(e) {
                        if($('#hdnGraphType').val() == 'Parameter Comparison')
                               {
                        toastr.info("Select Parameters");
                      toastr.reset();
                            }
                          });
                  
                  
                  $('.btn-equipement-reset').click(function(){
                  
                        
                        $('#parameter-tab a').removeClass('active');
                        
                           $('#graph-tab a').removeClass('active');
                           $('#tab-fillup1').removeClass('active');
                           
                           $('#tab-fillup3').removeClass('active');
                        
                        
                    });
               
                  
                  
                  
           }
           else
           {
                  $('#parameter-tab').addClass('disabled');
                      $(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
           }
           
    }


}



function fancytreecheckchangeeventforparameter()
{
	

	var Parameterids = SelectedFancyTreeElement("ParameterTree");
	
	if(Parameterids.toString() == "")
	{

		/*$('.btn-equipement-apply').prop('disabled',true);	
		$('.btn-equipement-reset').prop('disabled',true);	*/
		$('.btn-parameter-reset').prop('disabled',true);
		$('.btn-parameter-apply').prop('disabled',true);
		
		/*$('.btn-equipement-reset').addClass('disabled');
		$('.btn-equipement-apply').addClass('disabled');
		$('.btn-parameter-reset').addClass('disabled');
		$('.btn-parameter-apply').addClass('disabled');*/
		
		
		if($('#hdnGraphType').val() == 'Parameter Comparison')
		{
			
			
			
			$('#parameter-tab').removeClass('disabled');
			$(".data-toggle-parameter-add").attr("data-toggle",'tab');
		}
		else
		{
			$('#parameter-tab').addClass('disabled');
			    $(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
		}
	   
	}
	else
	{
		

$('.btn-parameter-reset').prop('disabled',false);
$('.btn-parameter-apply').prop('disabled',false);

$('.btn-equipement-apply').prop('disabled',false);	
$('.btn-equipement-reset').prop('disabled',false);	

		/*$('.btn-equipement-reset').removeClass('disabled');
		$('.btn-equipement-apply').removeClass('disabled');
		$('.btn-parameter-reset').removeClass('disabled');
		$('.btn-parameter-apply').removeClass('disabled');*/
		
		if($('#hdnGraphType').val() == 'Parameter Comparison')
		{
			
			$('#parameter-tab').removeClass('disabled');
			$(".data-toggle-parameter-add").attr("data-toggle",'tab');
		}
		else
		{
			$('#parameter-tab').addClass('disabled');
			    $(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
		}
		
	}
}




function SiteDropdownChange()
{
	//////;
	/*alert('Site Selected');*/
	 $("#EquipmentSearch").val('');
  	 $("#ParameterSearch").val('');
  	 
  	 
	var SiteName = $('#ddlSite option:selected').text();
	//var GraphType = $('#ddlGraphType option:selected').text();
	//$('#hdnGraphType').val("Daily Energy Generation");
	//$('.filter-labels').val();
	//$('.filter-labels').remove();
	//filter-labels
	var SiteId =  $('#ddlSite option:selected').val();
	 $(".analysispage").html(SiteName);	
	 $('.filter-labels').empty();
	 
	 $('#equipment-tab').addClass('disabled');
	 $(".data-toggle-equipment-add").removeAttr("data-toggle",'tab');
	 $('#parameter-tab').addClass('disabled');
	 $(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
	 $('.data-toggle-parameter-add').removeClass('active');
	 
	 
	/*if(GraphType=='')
		$('.sname').html(SiteName );
	else 
		$('.sname').html(SiteName + ' - ' + GraphType);*/
	
	$('.graphDetails').show();
	$(".analysispage").addClass('active');
	//$(".analysispage").css({"color": "#7b7d82","font-weight":" 500"});
/*	$('.analysispage').css("color","#000");*/
	
	//$('.GraphPart').css({"display","block"});
	$(".GraphPart").css("display", "block");
	
	$('#hdnGraphType').val("Daily Energy Generation");
	$('.txtGraphType').html('Daily Energy Generation');		
	$('#equipment-tab').removeClass('disabled');
	$(".data-toggle-equipment-add").removeAttr("data-toggle",'tab');
	$('.data-toggle-equipment-add').removeClass('active');
	$('#tab-fillup2').removeClass('active');		
	$('#parameter-tab').removeClass('disabled');
	$(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
	$(".data-toggle-graph-type").attr("data-toggle",'tab');
	$('.data-toggle-graph-type').addClass('active');
	$('#tab-fillup3').removeClass('active');
	$('#tab-fillup1').addClass('active');
	
	
	
	
	
	
	/*var GraphType = $('#ddlGraphType option:selected').text();*/
	/*$('.txtGraphType').html(GraphType);*/
	
	
	EquipmentAjaxCall(SiteId);
	ParameterAjaxCall(SiteId);
	DataPopulateAjaxCall();
	$('#gtDailyEnergyGeneration').click();
	//EquipmentAjax(SiteId);
	//ParameterAjax(SiteId);        			
	
}


var glyph_opts = {
		preset: "bootstrap3",
		map: {
		}
	};

function convertDateToUTC(date) { 
    return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(),date.getUTCSeconds()); 
} 
