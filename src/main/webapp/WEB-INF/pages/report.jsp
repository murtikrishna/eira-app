<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/loader.css" rel="stylesheet" type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet"
	rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/bootstrap.min3.3.37.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min3.css" type="text/css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="resources/css/jquery-ui.css">
<link href="resources/css/formValidation.min.css" rel="stylesheet"
	type="text/css">
<!--  <link href="resources/css/jquery-ui_cal.css" rel="stylesheet" type="text/css"> -->

<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>


<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script type="text/javascript" src="resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="resources/js/formValidation.min.js"></script>
<script type="text/javascript"
	src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.val.js"></script>
<script type="text/javascript" src="resources/js/toastr.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/toastr.css">
<link rel="stylesheet" type="text/css"
	href="http://codeseven.github.com/toastr/toastr.css">

<script type="text/javascript">
	$(document).on('click', '.ajaxbutton', function() {

		//alert($('#ajaxbutton').attr("name"));
		var SerUrl = $(this).attr("name");
		$.ajax({
			url : SerUrl,
			success : function(result) {
				//alert(result);
				var pdfVal = result;
				//window.location.href= result;
				window.open(result, '_blank');
				/* $("#div1").html(result); */
			}
		});

	});

	function handlenullable(param) {
		if (param == null || param == 'null') {
			return '';
		} else {
			return param;
		}

	}

	function LoadDataTable(ticketdetaillist) {
		var ticketrows = "";
		if (ticketdetaillist.length == 0) {
			toastr.warning("No Data Available");
		}
		for (var i = 0; i < ticketdetaillist.length; i++) {
			var priority = ticketdetaillist[i].priority;
			if (priority == 1) {
				priority = "Low";
			} else if (priority == 2) {
				priority = "Medium";
			} else if (priority == 3) {
				priority = "High";
			}

			var state = ticketdetaillist[i].state;
			if (state == 1) {
				state = "Open";
			} else if (state == 2) {
				state = "Closed";
			} else if (state == 3) {
				state = "Hold";
			}

			var ticketStatus = ticketdetaillist[i].ticketStatus;

			if (ticketStatus == 1) {
				ticketStatus = "Created";
			} else if (ticketStatus == 2) {
				ticketStatus = "Assigned";
			} else if (ticketStatus == 3) {
				ticketStatus = "Inprogress";
			}

			if (ticketStatus == 4) {
				ticketStatus = "Unfinished";
			} else if (ticketStatus == 5) {
				ticketStatus = "Finished";
			} else if (ticketStatus == 6) {
				ticketStatus = "Closed";
			} else if (ticketStatus == 7) {
				ticketStatus = "Hold";
			}

			var daycycle = ticketdetaillist[i].dayCycle - 1;

			<c:choose>
			<c:when test="${access.customerListView == 'visible'}">

			var href = '.\\ticketviews' + ticketdetaillist[i].ticketID;
			var download = ' ';
			if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'Modules Cleaning')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadmodulecleaningreport/'+ ticketdetaillist[i].ticketID +'/'+ daycycle +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}

			} else if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'Mechanical PM')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadmechanicalpmreport/'+ ticketdetaillist[i].ticketID +'/'+ daycycle +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}

			} else if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'Preventive Maintenance')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadpmticket/'+ ticketdetaillist[i].ticketID +'/'+ daycycle +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}
			} else if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'JMR Visit')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadjmrreport/'+ ticketdetaillist[i].ticketID +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}
			} else if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'Vegetation')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadvegetationreport/'+ ticketdetaillist[i].ticketID +'/'+ daycycle +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}
			} else if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'Inverter Cleaning')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadinvertercleaningreport/'+ ticketdetaillist[i].ticketID +'/'+ daycycle +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}
			} else if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'DataLogger Cleaning')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloaddlcleaningreport/'+ ticketdetaillist[i].ticketID +'/'+ daycycle +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}
			} else if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'String Current Measurement')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadstringmeasurementreport/'+ ticketdetaillist[i].ticketID +'/'+ daycycle +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}
			} else {

				download = ' ';
			}

			if (i == 0) {

				ticketrows = "<tr><td>"
						+ handlenullable(ticketdetaillist[i].ticketCode)
						+ "</td><td>"
						+ handlenullable(ticketdetaillist[i].siteName)
						+ "</td><td>"
						+ handlenullable(ticketdetaillist[i].ticketType)
						+ "</td><td>"
						+ handlenullable(ticketdetaillist[i].ticketCategory)
						+ "</td><td>" + handlenullable(state) + "</td><td>"
						+ handlenullable(ticketStatus) + "</td><td>" + download
						+ "</td></tr>";
			} else {
				ticketrows = ticketrows + "<tr><td>"
						+ handlenullable(ticketdetaillist[i].ticketCode)
						+ "</td><td>"
						+ handlenullable(ticketdetaillist[i].siteName)
						+ "</td><td>"
						+ handlenullable(ticketdetaillist[i].ticketType)
						+ "</td><td>"
						+ handlenullable(ticketdetaillist[i].ticketCategory)
						+ "</td><td>" + handlenullable(state) + "</td><td>"
						+ handlenullable(ticketStatus) + "</td><td>" + download
						+ "</td></tr>";

			}

			</c:when>

			<c:otherwise>

			var download = ' ';
			if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'Modules Cleaning')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadmodulecleaningreport/'+ ticketdetaillist[i].ticketID +'/'+ daycycle +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}

			} else if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'Mechanical PM')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadmechanicalpmreport/'+ ticketdetaillist[i].ticketID +'/'+ daycycle +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}

			} else if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'Preventive Maintenance')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadpmticket/'+ ticketdetaillist[i].ticketID +'/'+ daycycle +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}
			} else if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'JMR Visit')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadjmrreport/'+ ticketdetaillist[i].ticketID +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}
			} else if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'Vegetation')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadvegetationreport/'+ ticketdetaillist[i].ticketID +'/'+ daycycle +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}
			} else if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'Inverter Cleaning')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadinvertercleaningreport/'+ ticketdetaillist[i].ticketID +'/'+ daycycle +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}
			} else if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'DataLogger Cleaning')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloaddlcleaningreport/'+ ticketdetaillist[i].ticketID +'/'+ daycycle +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}
			} else if ((ticketdetaillist[i].ticketType == 'Maintenance' && ticketdetaillist[i].ticketCategory == 'String Current Measurement')) {
				if ((ticketdetaillist[i].ticketStatus == 4
						|| ticketdetaillist[i].ticketStatus == 5 || ticketdetaillist[i].state == 2)) {

					download = '<a class="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadstringmeasurementreport/'+ ticketdetaillist[i].ticketID +'/'+ daycycle +'/'+ ticketdetaillist[i].timeZone +'"><i></i>Download </a>';

				}
			} else {

				download = ' ';
			}

			if (i == 0) {

				ticketrows = "<tr><td>"
						+ handlenullable(ticketdetaillist[i].siteName)
						+ "</td><td>"
						+ handlenullable(ticketdetaillist[i].ticketType)
						+ "</td><td>"
						+ handlenullable(ticketdetaillist[i].ticketCategory)
						+ "</td><td>" + handlenullable(state) + "</td><td>"
						+ handlenullable(ticketStatus) + "</td><td>" + download
						+ "</td></tr>";
			} else {
				ticketrows = ticketrows + "<tr><td>"
						+ handlenullable(ticketdetaillist[i].siteName)
						+ "</td><td>"
						+ handlenullable(ticketdetaillist[i].ticketType)
						+ "</td><td>"
						+ handlenullable(ticketdetaillist[i].ticketCategory)
						+ "</td><td>" + handlenullable(state) + "</td><td>"
						+ handlenullable(ticketStatus) + "</td><td>" + download
						+ "</td></tr>";

			}

			</c:otherwise>
			</c:choose>

		}

		$('#example').append(ticketrows);

		$('#example').DataTable({
			//"dom": '<"row"<"col-sm-9"l><"col-sm-1"B><"col-sm-2"f>>'+'t<"row"<"col-sm-6"i><"col-sm-6"p>>',
			"order" : [ [ 0, "desc" ] ],
			"lengthMenu" : [ [ 15, 25, 50, -1 ], [ 15, 25, 50, "All" ] ],
			"bLengthChange" : true

		});

	}

	function AjaxCallForTicketDetails() {

		$.ajax({
			type : 'GET',
			url : './reportticketdetail',
			success : function(msg) {
				debugger;
				$("#ddlSiteList").empty();
				//$('#example').DataTable().clear().draw();
				$('#example tbody').empty();

				$('#example').DataTable().clear().draw();
				$('#example').DataTable().destroy();

				var sitelist = msg.siteList;

				var ticketdetaillist = msg.ticketdetaillist;
				$("#ddlSiteList").append("<option value=''>Select</option>");

				$.map(sitelist, function(val, key) {
					$("#ddlSiteList").append(
							"<option value='"+ key +"'>" + val + "</option>");
				});

				LoadDataTable(ticketdetaillist);
				$('.dataTables_filter input[type="search"]').attr(
						'placeholder', 'search');

				$(".theme-loader").hide();

			},
			error : function(msg) {

				//alert(0);
				//alert(msg);
			}
		});

	}

	function AjaxCallForSaveOrUpdate() {
		var TicketFilterData = [ {
			"fromDate" : $("#txtFromDate").val(),
			"toDate" : $('#txtToDate').val(),
			"siteID" : $('#ddlSiteList option:selected').val(),
			"state" : $('#ddlReportType option:selected').val(),
			"ticketCategory" : $('#ddlCateList option:selected').val(),

		} ];

		$.ajax({

			type : 'POST',
			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			url : './reportticketfilterdetail',
			data : JSON.stringify(TicketFilterData),
			success : function(msg) {
				$('#example tbody').empty();
				$('#example').DataTable().clear().draw();
				$('#example').DataTable().destroy();

				var ticketdetaillist = msg.ticketfilterdetaillist;
				debugger;

				var varSiteList = $('#ddlSiteList option:selected').val();
				var varStateList = $("#ddlReportType option:selected").val();
				var varCateList = $('#ddlCateList option:selected').val();
				var varPriorityList = $("#ddlPriorityList option:selected")
						.val();

				LoadDataTable(ticketdetaillist);
				$('.dataTables_filter input[type="search"]').attr(
						'placeholder', 'search');
				$(".theme-loader").hide();

			},
			error : function(msg) {

				$(".theme-loader").hide();

				//alert(0); alert(msg);
			}
		});
	}

	$(document)
			.ready(
					function() {

						var classstate = $('#hdnstate').val();

						$("#reportsid").addClass(classstate);
						$(".theme-loader").animate({
							opacity : "0"
						}, 6000);

						$('#ddlSite').dropdown('clear');
						$('#ddlType').dropdown('clear');
						$('#ddlCategory').dropdown('clear');
						$('#ddlPriority').dropdown('clear');
						$('#txtSubject').val('');
						$('#txtTktdescription').val('');

						$('.ui.fluid.search').change(function() {
							$('.search').val('')
						});

						$('#profileForm').click(function() {

							$('#example tbody').empty();
							$('#example').DataTable().clear().draw();
							$('#example').DataTable().destroy();

							AjaxCallForSaveOrUpdate();

						});

						toastr.options = {
							"debug" : false,
							"positionClass" : "toast-bottom-right",
							"progressBar" : true,
							"onclick" : null,
							"fadeIn" : 500,
							"fadeOut" : 100,
							"timeOut" : 1000,
							"extendedTimeOut" : 2000
						}

						$('#btnClear').click(function() {
							$('#btnCreate').click(function() {
								if ($('#ddlCategory').val == "") {
									alert('Val Empty');
									$('.category ').addClass('error')
								}
							})

							$('#example tbody').empty();
							$('#example').DataTable().clear().draw();
							$('#example').DataTable().destroy();
							/*   $('#ddlSite').dropdown('clear');
							  $('#ddlCate').dropdown('clear');
							  $('#ddlPriority').dropdown('clear');
							  $('#txtFromDate').val("");
							  $('#txtToDate').val(""); */

							$('#ddlSiteList').dropdown('clear');
							$('#ddlCateList').dropdown('clear');
							$('#ddlPriorityList').dropdown('clear');
							$('#txtFromDate').val("");
							$('#txtToDate').val("");
							$('#ddlStateList').dropdown('clear');
							$('#ddlReportType').dropdown('clear');

							AjaxCallForTicketDetails();

						});
						AjaxCallForTicketDetails();

						$('#ddlReportType')
								.on(
										'change',
										function() {

											if ($(this).val() == 'Operation') {

												$('#ddlCateList').dropdown(
														'clear');

												$('#ddlCateList').empty();

												$('#ddlCateList')
														.append(
																'<option data-value="Operation" value="Inverter Down">Inverter Down </option>');
												$('#ddlCateList')
														.append(
																'<option data-value="Operation" value="Plant Down">Plant Down</option>');
												$('#ddlCateList')
														.append(
																'<option data-value="Operation" value="Module Damages">Module Damages</option>');
												$('#ddlCateList')
														.append(
																'<option data-value="Operation" value="Plumbing Damages">Plumbing Damages</option>');
												$('#ddlCateList')
														.append(
																'<option data-value="Operation" value="Equipment Failure">Equipment Failure</option>');
												$('#ddlCateList')
														.append(
																'<option data-value="Operation" value="Equipment Replacement">Equipment Replacement</option>');
												$('#ddlCateList')
														.append(
																'<option data-value="Operation" value="String Down">String Down</option>');
												$('#ddlCateList')
														.append(
																'<option data-value="Operation" value="Communication Issue">Communication Issue</option>');
												$('#ddlCateList')
														.append(
																'<option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</option>');
												$('#ddlCateList')
														.append(
																'<option data-value="Operation" value="Plant Trip">Plant Trip</option>');

											} else if ($(this).val() == 'Maintenance') {
												$('#ddlCateList').dropdown(
														'clear');
												$('#ddlCateList').empty();

												$('#ddlCateList')
														.append(
																'<option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</option>');
												$('#ddlCateList')
														.append(
																'<option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</option>');
												$('#ddlCateList')
														.append(
																'<option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</option>');
												$('#ddlCateList')
														.append(
																'<option data-value="Maintenance" value="String Current Measurement">String Current Measurement</option>');

												$('#ddlCateList')
														.append(
																'<option data-value="Maintenance" value="Preventive Maintenance">Preventive Maintenance</option>');
												$('#ddlCateList')
														.append(
																'<option data-value="Maintenance" value="Mechanical PM">Mechanical PM</option>');
												$('#ddlCateList')
														.append(
																'<option data-value="Maintenance" value="Vegetation">Vegetation</option>');
												$('#ddlCateList')
														.append(
																'<option data-value="Maintenance" value="Visual Inspection">Visual Inspection</option>');

												$('#ddlCateList')
														.append(
																'<option data-value="Maintenance" value="JMR Visit">JMR Visit</option>');

											}

											else {
												$('#ddlCateList').empty();

											}
										});

						/* $("#txtFromDate").datepicker({ dateFormat: "dd/mm/yy"});
						$("#txtToDate").datepicker({ dateFormat: "dd/mm/yy"}); */
						$('.clear').click(function() {
							$('.search').val("");
						});

						$("#txtFromDate").datepicker(
								{
									dateFormat : 'dd/mm/yy',
									minDate : new Date(2018, 01 - 1, 01),
									maxDate : new Date(),
									maxDate : "0",
									onSelect : function(selected) {
										$("#txtToDate").datepicker("option",
												"minDate", selected)
									}
								}).datepicker("setDate", new Date());

						$("#txtFromDate")
								.mouseleave(
										function() {
											var SelectedDate = $('#txtFromDate')
													.val();
											var d = new Date();
											var month = d.getMonth() + 1;
											var day = d.getDate();
											var CurrentDate = (('' + day).length < 2 ? '0'
													: '')
													+ day
													+ '/'
													+ (('' + month).length < 2 ? '0'
															: '')
													+ month
													+ '/'
													+ d.getFullYear();
											/* alert(SelectedDate);
											alert(CurrentDate); */

											if (CurrentDate > SelectedDate) {
												//alert('Future');
												$('#txtFromDate').val('');
											} else {
												// alert('Past');
											}
										});

						$('#txtFromDate')
								.blur(
										function() {
											var SelectedDate = $('#txtFromDate')
													.val();
											var d = new Date();
											var month = d.getMonth() + 1;
											var day = d.getDate();
											var CurrentDate = (('' + day).length < 2 ? '0'
													: '')
													+ day
													+ '/'
													+ (('' + month).length < 2 ? '0'
															: '')
													+ month
													+ '/'
													+ d.getFullYear();
											/* alert(SelectedDate);
											alert(CurrentDate); */
											if (CurrentDate > SelectedDate) {
												//alert('Future');
												$('#txtFromDate').val('');
											} else {
												// alert('Past');
											}
										})

						$("#txtToDate").datepicker(
								{
									dateFormat : 'dd/mm/yy',
									maxDate : new Date(),
									minDate : new Date(2018, 01 - 1, 01),
									onSelect : function(selected) {
										$("#txtFromDate").datepicker("option",
												"maxDate", selected)
									}
								});

						$("#txtToDate")
								.mouseleave(
										function() {
											var SelectedDate = $('#txtToDate')
													.val();
											var d = new Date();

											var month = d.getMonth() + 1;
											var day = d.getDate();
											var CurrentDate = (('' + day).length < 2 ? '0'
													: '')
													+ day
													+ '/'
													+ (('' + month).length < 2 ? '0'
															: '')
													+ month
													+ '/'
													+ d.getFullYear();
											/* alert(SelectedDate);
											alert(CurrentDate); */

											if (CurrentDate > SelectedDate) {
												//alert('Future');
												$('#txtToDate').val('');
											} else {
												// alert('Past');
											}
										});

						$('#txtToDate')
								.blur(
										function() {
											var SelectedDate = $('#txtToDate')
													.val();
											var d = new Date();
											var month = d.getMonth() + 1;
											var day = d.getDate();
											var CurrentDate = (('' + day).length < 2 ? '0'
													: '')
													+ day
													+ '/'
													+ (('' + month).length < 2 ? '0'
															: '')
													+ month
													+ '/'
													+ d.getFullYear();
											/* alert(SelectedDate);
											alert(CurrentDate); */
											if (CurrentDate > SelectedDate) {
												//alert('Future');
												$('#txtToDate').val('');
											} else {
												// alert('Past');
											}
										})

						$.fn.dataTable.moment('DD-MM-YYYY');
						$.fn.dataTable.moment('DD-MM-YYYY HH:mm');

						/*  $('#example').DataTable( {
							 "order": [[ 0, "desc" ]],
							 "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
							 "bLengthChange": true,        		
							 } ); */

						$("#searchSelect").change(
								function() {
									var value = $(
											'#searchSelect option:selected')
											.val();
									var uid = $('#hdneampmuserid').val();
									redirectbysearch(value, uid);
								});
						$('.dataTables_filter input[type="search"]').attr(
								'placeholder', 'search');

						if ($(window).width() < 767) {
							$('.card').removeClass("slide-left");
							$('.card').removeClass("slide-right");
							$('.card').removeClass("slide-top");
							$('.card').removeClass("slide-bottom");
						}

						$('.close').click(function() {
							$('#tktCreation').hide();
							$('.clear').click();
							$('.category').dropdown();
							$('.SiteNames').dropdown();
						});
						$('.clear').click(function() {
							$('.search').val("");
						});

						$('#ddlType').change(function() {
							$('.category').dropdown('clear')
						});

						$('body').click(function() {
							$('#builder').removeClass('open');
						});

					});
</script>




<script type='text/javascript'>
	//<![CDATA[
	$(window)
			.load(
					function() {
						/*  $('.ui.dropdown').dropdown({forceSelection:false});
						$.fn.dropdown.settings.selectOnKeydown = false;
						$.fn.dropdown.settings.forceSelection = false;
						 */

						$('.ui.dropdown').dropdown({
							fullTextSearch : true,
							forceSelection : false,
							selectOnKeydown : true,
							showOnFocus : true,
							on : "click"
						});

						$('.ui.dropdown.oveallsearch').dropdown({
							onChange : function(value, text, $selectedItem) {
								var uid = $('#hdneampmuserid').val();
								redirectbysearchvalue(value, uid);
							},
							fullTextSearch : true,
							forceSelection : false,
							selectOnKeydown : false,
							showOnFocus : true,
							on : "click"
						});

						var $ddlType = $('#ddlType'), $ddlCategory = $('#ddlCategory'), $options = $ddlCategory
								.find('option');
						$ddlType.on(
								'change',
								function() {
									$ddlCategory.html($options
											.filter('[data-value="'
													+ this.value + '"]'));
								}).trigger('change');//]]> 

						var validation = {
							txtSubject : {
								identifier : 'txtSubject',
								rules : [ {
									type : 'empty',
									prompt : 'Please enter a value'
								}, {
									type : 'maxLength[50]',
									prompt : 'Please enter a value'
								} ]
							},
							ddlSite : {
								identifier : 'ddlSite',
								rules : [ {
									type : 'empty',
									prompt : 'Please enter a value'
								} ]
							},
							ddlType : {
								identifier : 'ddlType',
								rules : [ {
									type : 'empty',
									prompt : 'Please enter a value'
								} ]
							},
							ddlCategory : {
								identifier : 'ddlCategory',
								rules : [ {
									type : 'empty',
									prompt : 'Please enter a value'
								} ]
							},
							ddlPriority : {
								identifier : 'ddlPriority',
								rules : [ {
									type : 'empty',
									prompt : 'Please select a dropdown value'
								} ]
							},
							description : {
								identifier : 'description',
								rules : [ {
									type : 'empty',
									prompt : 'Please Enter Description'
								} ]
							}
						};
						var settings = {
							onFailure : function() {
								return false;
							},
							onSuccess : function() {
								$('#btnCreate').hide();
								$('#btnCreateDummy').show()
								//$('#btnReset').attr("disabled", "disabled");          
							}
						};
						$('.ui.form.validation-form')
								.form(validation, settings);

						//DatePicker Validation //
						/*  var validationDate  = {		
								txtFromDate:{
						      identifier:'txtFromDate',
						      rules:[
						        { type:'empty'}
						      ]
						    },
						    txtToDate:  {
						        identifier: 'txtToDate',
						        rules: [
						          {
						            type   : 'empty',
						           
						          }
						        ]
						      }};
						var settings = {
						  onFailure:function(){ 
						  
						      //toastr.warning("Validation Failure"); 
						      return false;
						    }, 
						  onSuccess:function(){    
						   
						   // AjaxCallForSaveOrUpdate();
						    //return false; 
						    }};
						  
						$('.ui.form.date-picker-validation').form(validation,settings); */
					})
</script>
<style type="text/css">
.ajaxbutton {
	cursor: -webkit-grab;
}

.dropdown-menu-right {
	left: -70px !important;
}

.form-control {
	font-size: 12px;
}

.ui.search.dropdown .menu {
	max-height: 12.02857143rem;
}
/* .ui.search.dropdown > input.search {
    background: none transparent !important;
    border: none !important;
    box-shadow: none !important;
    cursor: text;
    top: 0em;
    left: -2px;
    width: 100%;
    outline: none;
    -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
    padding: inherit;
} */
div.dataTables_wrapper div.dataTables_length select {
	width: 50px;
	display: inline-block;
	margin-left: 3px;
	margin-right: 4px;
}

/* .ui.form textarea:not([rows]) {
    height: 4em;
    min-height: 4em;
    max-height: 24em;
} */
.ui.icon.input>i.icon:before, .ui.icon.input>i.icon:after {
	left: 0;
	position: absolute;
	text-align: center;
	top: 36%;
	width: 100%;
}

.input-sm, .form-horizontal .form-group-sm .form-control {
	font-size: 13px;
	min-height: 25px;
	height: 32px;
	padding: 8px 9px;
}

#example td a {
	color: #337ab7;
	font-weight: bold;
}

.dt-button-background {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 0 !important;
}

.tbh-width {
	width: 150px;
	padding-right: 30px !important;
}

.dt-buttons {
	margin-left: 50px;
}

.buttons-collection {
	height: 28px;
	padding: 3px;
	/*  z-index:9999; */
}

.buttons-excel {
	background: rgba(36, 95, 68, 0.87);
	color: #FFF;
}

.buttons-pdf {
	background: rgb(98, 165, 96);
	color: #FFF;
}

.dataTables_filter {
	float: right;
}

.dataTables_length {
	float: left;
}

.ui.selection.dropdown {
	width: 100%;
	margin-top: 0.1em;
	/*  height: 2.3em; */
}

.ui.selection.dropdown .menu {
	width: 100%;
	white-space: normal !important;
}

.ui.selection.dropdown .menu .item:first-child {
	border-top: 1px solid #ddd !important;
	min-height: 2.8em;
}

.ui.selection.dropdown .text {
	text-overflow: ellipsis;
	white-space: nowrap;
	width: 90%;
}

.oveallSearch .text {
	/* color: #eae9e9 !important; */
	color: #a49c9c !important;
}

.ui.selection.dropdown .icon {
	text-align: right;
	margin-left: 7px !important;
}
</style>
<!-- <script type="text/javascript">
$(document).ready(function() {
/*   $("#txtFromDate").datepicker({ dateFormat: "dd/mm/yy"});
$("#txtToDate").datepicker({ dateFormat: "dd/mm/yy"}); */
$("#txtFromDate").datepicker({
   /*  minDate: 0,
    maxDate: "+60D",
    */
    dateFormat: "dd/mm/yy",
    onSelect: function(selected) {
      $("#txtToDate").datepicker("option","minDate", selected)
    }
});
$("#txtToDate").datepicker({ 
    minDate: 0,
   
    dateFormat: "dd/mm/yy",
    onSelect: function(selected) {
       $("#txtFromDate").datepicker("option","maxDate", selected)
    }
});  

    $('#profileForm')
        .bootstrapValidator({
            framework: 'bootstrap',
            fields: {
                fromDate: {
                    validators: {
                        notEmpty: {
                            message: 'Select from date'
                        },
                        date: {
                            format: 'DD/MM/YYYY',
                            message: 'The is not valid date'
                        }
                    }
                },
                 toDate: {
                    validators: {
                        notEmpty: {
                            message: 'Select From Date'
                        },
                        date: {
                            format: 'DD/MM/YYYY',
                            message: 'The is not valid date'
                        }
                    }
                }
            }
        })
        .find('[name="fromDate"]')
            .datepicker({
                onSelect: function(date, inst) {
                    $('#profileForm').formValidation('revalidateField', 'fromDate');
                }
            });


 $('#profileForm').bootstrapValidator({
            framework: 'bootstrap',
        })
            .find('[name="toDate"]')
            .datepicker({
                onSelect: function(date, inst) {
                    $('#profileForm').formValidation('revalidateField', 'toDate');
                }
            });

  });

</script> -->
</head>
<body class="fixed-header">

	<div class="theme-loader">
		<div class="loader-track">
			<div class="loader-bar"></div>
		</div>
	</div>

	<input type="hidden" value="${access.userID}" id="hdneampmuserid">

	<input type="hidden" value="${access.mySiteFilter}"
		id="hdnmysitefilter">

	<input type="hidden" value="${access.mySiteID}" id="hdnmysiteid">

	<input type="hidden" value="${access.myCustomerFilter}"
		id="hdnmycustomerfilter">

	<input type="hidden" value="${access.myCustomerID}"
		id="hdnmycustomerid">


	<input type="hidden" value="${classtate}" id="hdnstate">

	<jsp:include page="slider.jsp" />


	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					<a href="./dashboard"><img src="resources/img/logo01.png"
						alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx tktSearch"
					id="SearcSElect">
					<div class="ui  search selection  dropdown  width oveallsearch">
						<input id="myInput" name="tags" type="text">

						<div class="default text">search</div>

						<div id="myDropdown" class="menu">

							<jsp:include page="searchselect.jsp" />


						</div>
					</div>

				</div>

				<div class="dropdown pull-right hidden-md-down user-log">
					<button class="profile-dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 circular inline"> <img
							src="resources/img/user_01.png" alt
							data-src="resources/img/user_01.png"
							data-src-retina="resources/img/user_01.png" width="32"
							height="32">
						</span>

						<c:if test="${not empty access}">
							<p class="user-login">${access.userName}</p>
						</c:if>
					</button>
					<div class="dropdown-menu dropdown-menu-right profile-dropdown"
						role="menu">
						<a class="dropdown-item disablecolor"><i
							class="pg-settings_small"></i> Settings</a> <a
							class="dropdown-item disablecolor"><i class="pg-outdent"></i>
							Feedback</a> <a class="dropdown-item disablecolor"><i
							class="pg-signals"></i> Help</a> <a href="logout1"
							class="dropdown-item color logout"><i class="pg-power"></i>
							Logout</a>
					</div>
				</div>


				<div class="newticketpopup hidden">
					<span data-toggle="modal" data-target="#tktCreation"
						data-backdrop="static" data-keyboard="false"><p
							class="center m-t-5 tkts">
							<img class="m-t-5" src="resources/img/tkt.png">
						<p class="create-tkts">New Ticket</p>
						</p></span>
				</div>

			</div>
		</div>
		<div class="page-content-wrapper" id="QuickLinkWrapper1">

			<div class="content ">

				<div class="container-fixed-lg">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="./dashboard"><i
								class="fa fa-home mt-7"></i></a></li>

						<li class="breadcrumb-item active">Reports download</li>
						<!-- <li class="breadcrumb-item active">Tickets</li> -->
					</ol>
				</div>



				<div id="newconfiguration" class="">
					<div class="card card-transparent mb-0">
						<!-- Table Start-->
						<div class="padd-3 sortable" id="equipmentdata">
							<!-- 	<div class="row"> -->
							<!-- <div class="card-header">
                                    <div class="card-title tbl-head ml-15">Equipment Curing Data</div>
                                 </div> -->
							<div class="col-lg-12 col-xs-12 col-md-12 padd-0"
								id="datadetails">
								<div class="card card-default bg-default m-b-5"
									data-pages="card">
									<form class="ui form date-picker-validation">
										<c:choose>
											<c:when test="${access.filterFlag == 1}">
												<c:url var="viewAction"
													value="/ticketdetails${access.mySiteID}&ref=${access.userID}"></c:url>
											</c:when>
											<c:otherwise>
												<c:url var="viewAction" value="/ticketdetails"></c:url>
											</c:otherwise>
										</c:choose>
										<div class="row mt-5">
											<div class="col-md-12 padd-0">
												<div class="col-md-1">
													<div class="txtwidth">
														<label class="fields m-t-5">From Date <span
															class="errormess">*</span></label>
													</div>
												</div>
												<div class="col-md-2">
													<div class="controls">
														<div class="input-group">
															<input id="txtFromDate" autocomplete="off"
																name="txtFromDate" type="text" placeholder="From Date"
																class="date-picker form-control" name="fromDate" /> <label
																for="txtFromDate" class="input-group-addon btn"><span
																class="glyphicon glyphicon-calendar"></span> </label>
														</div>
													</div>
												</div>

												<div class="col-lg-1 uitlity ">
													<div class="required  txtwidth">
														<label class="fields m-t-5">To Date <span
															class="errormess">*</span></label>
													</div>
												</div>

												<div class="col-md-2">
													<div class="controls">
														<div class="input-group">
															<input id="txtToDate" autocomplete="off" name="txtToDate"
																type="text" placeholder="To Date"
																class="date-picker form-control" name="toDate" /> <label
																for="txtToDate" class="input-group-addon btn"><span
																class="glyphicon glyphicon-calendar"></span> </label>
														</div>
													</div>
												</div>
												<div class="col-md-1 uitlity">
													<div class="field">
														<label class="fields m-t-5">Site Name</label>
													</div>
												</div>
												<div class="col-md-2">
													<select
														class="ui fluid search selection dropdown width ddLPtype"
														id="ddlSiteList">
														<option value="">Select</option>


													</select>
												</div>
												<div class="col-md-1">
													<div class="field">
														<label class="fields m-t-5">Report Type</label>
													</div>
												</div>
												<div class="col-md-2">
													<select
														class="ui fluid search selection dropdown width ddLPtype"
														name="ddlReportType" id="ddlReportType">
														<option value="">Select</option>
														<option value="Operation">Operation</option>
														<option value="Maintenance">Maintenance</option>



													</select>



												</div>
											</div>
										</div>
										<div class="row  padd-0 m-b-5">
											<div class="col-md-12 padd-0">
												<div class="col-md-1 col-xs-12 m-t-5">
													<div class="txtwidth ">
														<label class="fields m-t-5">Category</label>
													</div>
												</div>
												<div class="col-md-2 col-xs-12 m-t-5">
													<select
														class="ui fluid search selection dropdown width ddLPtype"
														name="ddlCategory" id="ddlCateList">
														<option value="">Select</option>

													</select>
												</div>


												<div class="col-md-1"></div>
												<!-- <div class="col-md-2 col-xs-12 center">
													<div class="col-md-12 col-xs-12 padd-0 center m-t-7">
														<div class="col-md-6 col-xs-6  padd-0">
														<button type="button"  id="profileForm"   class="btn btn-success  submit" style="width:98%;">View</button>
													</div>
													id="profileForm"
													<div class="col-md-6 col-xs-6  padd-0">
													 <button  type="button" class="btn btn-primary btnClear" id="btnClear"style="width:90%;">Clear</button>
														<button  type="button"	class="btn btn-primary  clr" style="float:right;width:98%;">Clear</button>
													</div>
													</div>
													
													</div> -->
											</div>
										</div>


										<div class="row  padd-0 m-b-5">

											<div class="col-md-4"></div>

											<div class="col-md-4 col-xs-12 center">
												<div class="col-md-12 col-xs-12 center">
													<div class="col-md-3 col-xs-3  padd-0"></div>
													<div class="col-md-3 col-xs-3  padd-0">
														<button type="button" id="profileForm"
															class="btn btn-success  submit" style="width: 98%;">View</button>
													</div>
													<!-- id="profileForm" -->
													<div class="col-md-3 col-xs-3  padd-0">
														<button type="button" class="btn btn-primary btnClear"
															id="btnClear" style="width: 90%;">Clear</button>
														<!-- <button  type="button"	class="btn btn-primary  clr" style="float:right;width:98%;">Clear</button> -->
													</div>
												</div>

											</div>
											<div class="col-md-4"></div>
										</div>
									</form>
								</div>
							</div>
						</div>

					</div>


					<!-- Table End-->

					<!-- Table Start-->
					<div class="padd-3 sortable" id="equipmentdata">
						<div class="row">
							<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
								<div class="card card-default bg-default" data-pages="card">
									<!-- <div class="card-header">
											<div class="card-title tbl-head ml-15">
												 <i class="fa fa-list" aria-hidden="true"></i> List of Tickets
											</div>
										</div> -->
									<div class="padd-5 table-responsive">
										<table id="example" class="table table-striped table-bordered"
											width="100%" cellspacing="0">
											<thead>
												<tr>
													<c:if test="${access.customerListView == 'visible'}">
														<th style="width: 10%;">Ticket No</th>
													</c:if>
													<th style="width: 10%;">Site Name</th>
													<th style="width: 10%;">Report Type</th>
													<th style="width: 12%;">Category</th>

													<th style="width: 10%;">State</th>
													<th style="width: 10%;">Status</th>
													<th style="width: 10%;">Action</th>

												</tr>
											</thead>
											<tbody>
												<%--  <c:forEach items="${ticketdetaillist}" var="objticketdetail">
                                                    <tr>
                                                        <td><a
                                                            href=".\ticketviews${objticketdetail.ticketID}">${objticketdetail.ticketCode}</a></td>
                                                        <td>${objticketdetail.siteName}</td>


                                                        <c:choose>
                                                            <c:when test="${objticketdetail.priority == 1}">
                                                                <td>Low</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.priority == 2}">
                                                                <td>Medium</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.priority == 3}">
                                                                <td>High</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td>Low</td>
                                                            </c:otherwise>
                                                        </c:choose>


                                                        <td>${objticketdetail.createdDateText}</td>

                                                        <td>${objticketdetail.ticketCategory}</td>
                                                        <td>${objticketdetail.ticketDetail}</td>

                                                        <td>${objticketdetail.assignedToWhom}</td>
                                                        <td>${objticketdetail.scheduledDateText}</td>



                                                        <c:choose>

                                                            <c:when test="${objticketdetail.state == 1}">
                                                                <td>Open</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.state == 2}">
                                                                <td>Closed</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.state == 3}">
                                                                <td>Hold</td>
                                                            </c:when>

                                                            <c:otherwise>
                                                                <td>-</td>
                                                            </c:otherwise>
                                                        </c:choose>





                                                        <c:choose>
                                                            <c:when test="${objticketdetail.ticketStatus == 1}">
                                                                <td>Created</td>
                                                            </c:when>
                                                            
                                                            <c:when test="${objticketdetail.ticketStatus == 2}">
                                                                <td>Assigned</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.ticketStatus == 3}">
                                                                <td>Inprogress</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.ticketStatus == 4}">
                                                                <td>Unfinished</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.ticketStatus == 5}">
                                                                <td>Finished</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.ticketStatus == 6}">
                                                                <td>Closed</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.ticketStatus == 7}">
                                                                <td>Hold</td>
                                                            </c:when>

                                                            <c:otherwise>
                                                                <td>-</td>
                                                            </c:otherwise>
                                                        </c:choose>


                                                        <!--  <td class="center"><a data-toggle="modal" class="btn btn-primary btn-color" data-target="#Close" data-backdrop="static" data-keyboard="false">Close</a></td> -->
                                                        <!-- <td><i class="fa fa-times m-l-10" data-toggle="modal" data-target="#Close" aria-hidden="true"></i></td> -->
                                                    </tr>
                                                </c:forEach> --%>



											</tbody>
										</table>
									</div>




								</div>
							</div>


						</div>

					</div>


					<!-- Table End-->
				</div>
			</div>


		</div>


		<div class="container-fixed-lg footer mb-0">
			<div class="container-fluid copyright sm-text-center">
				<p class="small no-margin pull-left sm-pull-reset">
					<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
						CLEAN ENERGY</span>. <span class="hint-text">All rights reserved.
					</span>
				</p>
				<p class="small no-margin pull-rhs sm-pull-reset">
					<span class="hint-text">Powered by</span> <span>MESTECH
						SERVICES PVT LTD</span>.
				</p>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	</div>

	<!-- Side Bar Content Start-->




	<div class="quickview-wrapper  builder hidden-sm hidden-xs"
		id="builder">
		<div class="p-l-10 p-r-10 ">
			<a class="builder-close quickview-toggle pg-close"></a> <a
				class="builder-toggle" data-toggle="quickview"
				data-toggle-element="#builder"><img
				src="resources/img/ImageResize_06.png"></a>
			<ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary"
				id="builderTabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#tabLayouts" role="tab"
					aria-controls="home"><span>Quick Link</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active " id="tabLayouts" role="tabcard">
					<div class="scrollable">
						<div class="p-r-50">
							<div class="list">
								<ul>
									<li><a href="#QuickLinkWrapper1">Ticket Filters</a></li>
									<li><a href="#QuickLinkWrapper2">List Of Tickets</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>






	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min3.js"
		type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min3.js"
		type="text/javascript"></script>
	<script type="text/javascript" src="resources/js/moment.min.js"></script>
	<script type="text/javascript" src="resources/js/datetime-moment.js"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>


	<script src="resources/js/dataTables.buttons.min.js"></script>

	<script src="resources/js/buttons.html5.min.js"></script>
	<script src="resources/js/jszip.min.js"></script>








	<div id="gotop"></div>





	<!-- Share Popup End !-->
	<!-- Modal -->
	<div id="tktCreation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header Popup-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="resources/img/close.png">
					</button>
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<p class="modal-title tkt-title">
						<img src="resources/img/tkt.png"><span class="tkt-title">New
							Ticket</span>
					</p>
				</div>
				<div class="modal-body">
					<c:url var="addAction" value="/addnewticket"></c:url>
					<div class="col-md-12">
						<p class="tkt-mandat">
							<span class="errormess">*</span> All fields are mandatory
						</p>
					</div>
					<form:form action="${addAction}" modelAttribute="ticketcreation"
						class="ui form validation-form">

						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width SiteNames"
									id="ddlSite" name="ddlSite" path="siteID"
									onchange="getEquipmentAgainstSite()">
									<form:option value="" tabindex="0">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>

						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Equipment</label>
							</div>

							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width equipments"
									id="ddlEquipment" path="equipmentID" name="ddlEquipment">
									<option value="">Select</option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 m-t-15">
							<div class="col-md-4">
								<label class="fields required m-t-10">Type</label>
							</div>
							<div class="col-md-8">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlType" name="ddlType" path="ticketType">
									<form:option value="">Select</form:option>
									<form:option value="Operation">Operation</form:option>
									<form:option value="Maintenance">Maintenance</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 m-t-15">
							<div class="col-md-4">
								<label class="fields required m-t-10">Category</label>
							</div>
							<div class="col-md-8">
								<form:select
									class="field ui fluid search selection dropdown width category"
									id="ddlCategory" name="ddlCategory" path="ticketCategory">
									<form:option value="">Select </form:option>
									<form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
									<form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
									<form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
									<form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
									<form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
									<form:option data-value="Operation"
										value="Equipment Replacement">Equipment Replacement</form:option>
									<form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
									<form:option data-value="Operation" value="String Down">String Down</form:option>
									<form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
									<form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
									<form:option data-value="Operation" value="">Select</form:option>
									<form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
									<form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="DataLogger Cleaning">DataLogger Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="String Current Measurement">String Current Measurement</form:option>
									<form:option data-value="Maintenance"
										value="Preventive Maintenance">Preventive Maintenance</form:option>

									<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
									<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
									<form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
									<form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
									<form:option data-value="Maintenance" value="">Select</form:option>
								</form:select>
							</div>

						</div>
						<div class="col-md-12 m-t-15">
							<div class="col-md-4">
								<label class="fields required m-t-10">Subject</label>
							</div>
							<div class="col-md-8">
								<div class="field">
									<form:input placeholder="Subject" autocomplete="off"
										name="Subject" type="text" id="txtSubject" path="ticketDetail"
										maxLength="50" />
								</div>
							</div>
						</div>

						<div class="col-md-12 m-t-15">
							<div class="col-md-4">
								<label class="fields required m-t-10">Priority</label>
							</div>
							<div class="col-md-8">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlPriority" name="user" path="priority">
									<form:option value="">Select </form:option>
									<form:option data-value="3" value="3">High</form:option>
									<form:option data-value="2" value="2">Medium</form:option>
									<form:option data-value="1" value="1">Low</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 m-t-15">
							<div class="col-md-4">
								<label class="fields required m-t-10">Description</label>
							</div>
							<div class="col-md-8">
								<div class="field">
									<textarea id="txtTktdescription" autocomplete="off"
										name="description" style="resize: none;"
										placeholder="Ticket Description" maxLength="120"></textarea>
								</div>

							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-15 center">
							<div class="btn btn-success  submit" id="btnCreate">Create</div>
							<button class="btn btn-success" type="button" id="btnCreateDummy"
								tabindex="7" style="display: none;">Create</button>
							<div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
						</div>
						<!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
						<!-- <div class="ui error message"></div> -->
					</form:form>
					<!-- <div class="ui error message"></div> -->

				</div>
			</div>
		</div>
	</div>
	<!-- Address Popup End !-->
	<script>
		$('#gotop').gotop({
			customHtml : '<i class="fa fa-angle-up fa-2x"></i>',
			bottom : '2em',
			right : '2em'
		});
	</script>
	<script>
		$(document).ready(function() {
			$(window).fadeThis({
				speed : 500,
			});
		});
	</script>

</body>
<script type="text/javascript">
	function getEquipmentAgainstSite() {
		var siteId = $('#ddlSite').val();

		$.ajax({
			type : 'GET',
			url : './equipmentsBysites?siteId=' + siteId,
			success : function(msg) {

				var EquipmentList = msg.equipmentlist;

				for (i = 0; i < EquipmentList.length; i++) {
					$('#ddlEquipment').append(
							"<option value="+EquipmentList[i].equipmentId+">"
									+ EquipmentList[i].customerNaming
									+ "</option>");
				}

			},
			error : function(msg) {
				console.log(msg);
			}
		});
	}
</script>




<script type="text/javascript">
	$(document).ready(
			function() {
				debugger;
				var todayDate = new Date();

				var fromMonth;

				if (todayDate.getMonth() >= 10)
					fromMonth = todayDate.getMonth();
				else
					fromMonth = '0' + todayDate.getMonth();

				var fromDate = todayDate.getDate() + '/' + fromMonth + '/'
						+ todayDate.getFullYear();

				console.log(fromDate);
				
				$('#txtFromDate').val(fromDate);
				

				var month;

				if ((todayDate.getMonth() + 1) >= 10)
					month = (todayDate.getMonth() + 1);
				else
					month = '0' + (todayDate.getMonth() + 1);

				var toDate = todayDate.getDate() + '/' + month + '/'
						+ todayDate.getFullYear();

				$('#txtToDate').val(toDate);
				console.log(toDate);
			});
</script>
</html>

