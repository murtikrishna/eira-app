<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To eAMPM ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet" type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet" type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet" rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet" type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet" type="text/css">
<link href="resources/css/bootstrap.min3.3.37.css" type="text/css" rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min3.css" type="text/css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="resources/css/jquery-ui.css">
<link href="resources/css/formValidation.min.css" rel="stylesheet" type="text/css">
<!--  <link href="resources/css/jquery-ui_cal.css" rel="stylesheet" type="text/css"> -->

<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>

<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script type="text/javascript" src="resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="resources/js/formValidation.min.js"></script>
<script type="text/javascript" src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.val.js"></script>




<!-- <script type="text/javascript">                
  function AjaxCallForPageLoad() {                
           
                 
                  $.ajax({
                                    type: 'GET',
                                    url: './site',
                                    success: function(msg){
                                
                                    	debugger;
                                    	var status = msg.status;
                                    	var sitetypeList = msg.sitetypeList;
                                    	var site = [];
                                    	site =  sitetypeList;
                                    	alert(site);
                                                
                                    },
                                    error:function(msg) { alert(0); alert(msg);}
                  });
  }
  
  
  
      </script>
 -->





<script type="text/javascript">
        $(document).ready(function() {
        	
        	
        /* 	 $('#addSite').submit(function(event) {
        	       
        	      var producer = $('#producer').val();
        	      var model = $('#model').val();
        	      var price = $('#price').val();
        	      var json = { "producer" : producer, "model" : model, "price": price};
        	       
        	    $.ajax({
        	        url: $("#newSmartphoneForm").attr( "action"),
        	        data: JSON.stringify(json),
        	        type: "POST",
        	         
        	        beforeSend: function(xhr) {
        	            xhr.setRequestHeader("Accept", "application/json");
        	            xhr.setRequestHeader("Content-Type", "application/json");
        	        },
        	        success: function(smartphone) {
        	            var respContent = "";
        	             
        	            respContent += "<span class='success'>Smartphone was created: [";
        	            respContent += smartphone.producer + " : ";
        	            respContent += smartphone.model + " : " ;
        	            respContent += smartphone.price + "]</span>";
        	             
        	            $("#sPhoneFromResponse").html(respContent);         
        	        }
        	    });
        	      
        	    event.preventDefault();
        	  });
        	  */
        	
        	 $('#example').DataTable( { "order": [[ 0, "desc" ]]} );
        	 $("#txtFromDate").datepicker({ dateFormat: "dd/mm/yy"});
       	     $("#txtToDate").datepicker({ dateFormat: "dd/mm/yy"});
       	  $('.ui.dropdown').dropdown({forceSelection:false}); 
       	   
       	 $("#searchSelect").change(function() {
             var value = $('#searchSelect option:selected').val();
             var uid =  $('#hdneampmuserid').val();
           redirectbysearch(value,uid);
        }); $('.dataTables_filter input[type="search"]').attr('placeholder','search');

        
            if($(window).width() < 767)
				{
				   $('.card').removeClass("slide-left");
				   $('.card').removeClass("slide-right");
				   $('.card').removeClass("slide-top");
				   $('.card').removeClass("slide-bottom");
				}
            
           /*  $('.tkts').click(function(){
            	
            	 $("input").trigger("select");
            }) */
            
            $('.clr').click(function(){
           	 debugger;
           	 $("#txtFromDate").val('');
           	 $("#txtToDate").val('');
           	 $(".text").empty();
           	 $(".text").addClass('default')
           	 $(".text").html("Select");
           	 $('#ddlSite option:selected').removeAttr('selected');
           	 $('#ddlState option:selected').removeAttr('selected');
           	 $('#ddlCate option:selected').removeAttr('selected');
           	 $('#ddlPriority option:selected').removeAttr('selected'); 
           	window.location.href = './eventdetails';
           	//location.reload();
           	   });
            
            
            $('.close').click(function(){
				$('.clear').click();
			});
			$('.clear').click(function(){
			    $('.search').val("");
			});

			$('#ddlType').change(function(){              
                $('.category').dropdown('clear')
               });

			
			
			$('body').click(function(){
            	$('#builder').removeClass('open'); 
         	 });
			
			
			
//			AjaxCallForPageLoad();
			
			
        });
      </script>

      <script type='text/javascript'>//<![CDATA[
         $(window).load(function(){
        	 $('.ui.dropdown').dropdown({forceSelection:false});
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
         
         
         $('.ui.dropdown').dropdown({
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: true, 
             showOnFocus: true,
             on: "click" 
           });

		$('.ui.dropdown.oveallsearch').dropdown({
             onChange: function (value, text, $selectedItem) {
                
                var uid =  $('#hdneampmuserid').val();
                redirectbysearchvalue(value,uid);
             },
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: false, 
             showOnFocus: true,
             on: "click" 
           });
		
		
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );//]]> 



             $('.validation-form')
   .form({
	 
     on: 'blur',
     onFailure: function(formErrors, fields) {
       /* $.each(fields, function(e) {
         var ele = $('#' + e);
         var fail = (ele.val() === false || ele.val().length === 0);
         if (fail) {
           console.log(ele, fail);
           ele.focus();
           $('html,body').animate({
             scrollTop: ele.offset().top - ($(window).height() - ele.outerHeight(true)) / 2
           }, 200);
           return false;
         }
       }); */
       return false;
     },
     fields: {
    	 txtSubject: {
         identifier: 'txtSubject',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
       },
        ddlSite: {
         identifier: 'ddlSite',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
       },
      ddlType: {
            identifier: 'ddlType',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
      },
      ddlCategory: {
         identifier: 'ddlCategory',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
      },
       ddlPriority: {
         identifier: 'ddlPriority',
         rules: [{
           type: 'empty',
           prompt: 'Please select a dropdown value'
         }]
       },
       description: {
         identifier: 'description',
         rules: [{
           type: 'empty',
           prompt: 'Please Enter Description'
         }]
       },
       ddlSiteType : {
    	   identifier: 'ddlSiteType',
           rules: [{
             type: 'empty',
             prompt: 'Please Enter Description'
           }]
       },
       ddlCustometrId : {
    	   identifier: 'ddlCustometrId',
           rules: [{
             type: 'empty',
             prompt: 'Please Enter Description'
           }]
       },
       txtSiteName : {
    	   identifier: 'txtSiteName',
           rules: [{
             type: 'empty',
             prompt: 'Please Enter Description'
           }]
       },
       txtSiteDescription : {
    	   identifier: 'txtSiteDescription',
           rules: [{
             type: 'empty',
             prompt: 'Please Enter Description'
           }]
       },
       
     }
   }); 

         })
         
      </script>
    
    
<style type="text/css">
.dropdown-menu-right {
	left: -70px !important;
}
.ui.search.dropdown > input.search {
    background: none transparent !important;
    border: none !important;
    box-shadow: none !important;
    cursor: text;
    top: 0em;
    left: -2px;
    width: 100%;
    outline: none;
    -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
    padding: inherit;
}
.txtheight {
	    min-height: 28px;
    height: 20px;
}
div.dataTables_wrapper div.dataTables_length select {
			    width: 50px;
			    display: inline-block;
			    margin-left: 3px;
			    margin-right: 4px;
			}
			
			.ui.form textarea:not([rows]) {
    height: 4em;
    min-height: 4em;
    max-height: 24em;
}
.ui.icon.input > i.icon:before, .ui.icon.input > i.icon:after {
    left: 0;
    position: absolute;
    text-align: center;
    top: 36%;
    width: 100%;
}
.evtDropdown {
	min-height: 2.7142em !important; 
font-size:13px !important;
padding: 0.78571429em 2.1em 0.78571429em 1em !important; 
}

.input-sm, .form-horizontal .form-group-sm .form-control {
    font-size: 13px;
    min-height: 25px;
    height: 32px;
    padding: 8px 9px;
}
  
#example td a {
	color: #337ab7;
	font-weight: bold;
}
.dt-button-background {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 0 !important;
}
.dropevent {
	    min-width: 100px;
    margin-left: -35px;
}
.dropevent li a {
	color:#000 !important;
	font-weight:normal !important;
}
.tbh-width {
	width: 150px;
	padding-right: 30px !important;
}

.dt-buttons {
	margin-left: 50px;
}

.buttons-collection {
	height: 28px;
	padding: 3px;
	/*  z-index:9999; */
}


.buttons-excel {
	background: rgba(36, 95, 68, 0.87);
	color: #FFF;
}

.buttons-pdf {
	background: rgb(98, 165, 96);
	color: #FFF;
}

.dataTables_filter {
	float: right;
}

.dataTables_length {
	float: left;
}


.ui.selection.dropdown {
    width: 100%;
    margin-top: 0.1em;
   /*  height: 2.3em; */
}

.ui.selection.dropdown .menu {
    width: 100%;
    white-space: normal !important;
}

.ui.selection.dropdown .menu .item:first-child {
    border-top: 1px solid #ddd !important;
    min-height: 2.8em;
}

.ui.selection.dropdown .text {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    width: 90%;
}

.ui.selection.dropdown .icon {
    text-align: right;
    margin-left: 7px !important;
}

</style>

</head>
<body class="fixed-header">

	<input type="hidden" value="${access.userID}" id="hdneampmuserid">


	<nav class="page-sidebar" data-pages="sidebar">

		<div class="sidebar-header">
			<a href="./dashboard"><img src="resources/img/logo01.png" class="m-t-10"
				alt="logo"
				title="Electronically Assisted Monitoring and Portfolio Management"></a>
			<i class="fa fa-bars bars"></i>
		</div>

		<div class="sidebar-menu">


			<c:if test="${not empty access}">

				<ul class="menu-items">



					<c:if test="${access.dashboard == 'visible'}">

						<li class="" style="margin-top: 2px;"><a
							href="./dashboard" class="detailed"> <span
								class="title">Dashboard</span>
						</a> <span class="icon-thumbnail"><i class="fa fa-tachometer"></i></span>
						</li>
					</c:if>

					<c:if test="${access.overview == 'visible'}">
						<li><a href="#"><span class="title">Overview</span></a> <span
							class="icon-thumbnail"><i class="fa fa-list-alt"></i></span></li>
					</c:if>

					<c:if test="${access.systemMonitoring == 'visible'}">
						<li><a href="#"><span class="title">System
									Monitoring</span></a> <span class="icon-thumbnail"><i
								class="fa fa-desktop"></i></span></li>
					</c:if>

					<c:if test="${access.visualization == 'visible'}">
						<li><a href="javascript:;"><span class="title">Visualization</span>
								<span class=" arrow"></span></a> <span class="icon-thumbnail"> <i class="fa fa-desktop" aria-hidden="true"></i></span>
							<ul class="sub-menu">

								<c:if test="${access.customerListView == 'visible'}">
									<li><a href="./customerlist">Customer View</a>
										<span class="icon-thumbnail"><i class="fa fa-eye"></i></span>
									</li>
								</c:if>



								<li><a href="./sitelist">Site View</a> <span
									class="icon-thumbnail"><i class="fa fa-map-marker"></i></span>
								</li>
								
							</ul></li>

					</c:if>

					<c:if test="${access.analytics == 'visible'}">
						<li><a href="#"> <span class="title">Analytics</span>
						</a> <span class="icon-thumbnail"><i class="fa fa-pie-chart"></i></span>
						</li>
					</c:if>

					<c:if test="${access.portfolioManagement == 'visible'}">
						<li><a href="#"> <span class="title">Portfolio
									Manager</span>
						</a> <span class="icon-thumbnail"><i class="fa fa-briefcase"></i></span>
						</li>
					</c:if>

					<c:if test="${access.ticketing == 'visible'}">
						<li><a href="javascript:;"><span class="title">Operation
									& Maintenance</span> <span class=" arrow"></span></a> <span class="icon-thumbnail" style="padding-top:5px !important;"><img src="resources/img/maintance.png"></span>
							<ul class="sub-menu">

								<c:if test="${access.customerListView == 'visible'}">
									<li ><a href="./ticketdetails">Tickets
											</a> <span class="icon-thumbnail"><i
											class="fa fa-ticket"></i></span></li>
										
								</c:if>
 						<li>
                        	<a href="./eventdetails">Events</a>
                        	<span class="icon-thumbnail"><i class="fa fa-calendar"></i></span>
                     	</li>

							</ul></li>





					</c:if>




					<c:if test="${access.forcasting == 'visible'}">
						<li><a href="#"><span class="title">Forecasting</span></a> <span
							class="icon-thumbnail"><i class="fa fa-desktop"></i></span></li>
					</c:if>



	<c:if test="${access.analysis == 'visible'}">
          		<li>
                  <a href="./analysis"><span class="title">Analysis</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-area-chart"></i></span>
               </li>
           </c:if>
           
           
					
            <c:if test="${access.configuration == 'visible'}">
            
               <li class="active">
                  <a href="javascript:;"><span class="title">Configuration</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="pg-tables"></i></span>
                  <ul class="sub-menu">
                     
                     <li>
                        <a href="./customers">Customer Config.</a>
                        <span class="icon-thumbnail">CU</span>
                     </li>
                     
                     <li class="active">
                        <a href="./sites">Site Config.</a>
                        <span class="icon-thumbnail">SI</span>
                     </li>
                     
                     <li>
                        <a href="./equipments">Equipment Config.</a>
                        <span class="icon-thumbnail">EQ</span>
                     </li>
                     
                   <!--   <li>
                        <a href="#">Data Logger Config.</a>
                        <span class="icon-thumbnail">DL</span>
                     </li>
                     
                     <li>
                        <a href="#">Equ-Attrib Config.</a>
                        <span class="icon-thumbnail">EA</span>
                     </li>
                     
                      <li>
                        <a href="#">Configuration Loader</a>
                        <span class="icon-thumbnail">CL</span>
                     </li>
                     
                     <li>
                        <a href="./activities">Activity Config.</a>
                        <span class="icon-thumbnail">AC</span>
                     </li>
                     
                      <li>
                        <a href="./timezones">Timezone Config.</a>
                        <span class="icon-thumbnail">TZ</span>
                     </li>
                     
                      <li>
                        <a href="./currencies">Currency Config.</a>
                        <span class="icon-thumbnail">CY</span>
                     </li>
                     
                      <li>
                        <a href="./unitofmeasurements">Unit Measurement Config.</a>
                        <span class="icon-thumbnail">UM</span>
                     </li>
                     
                     
                     <li>
                        <a href="./countryregions">Country Region Config.</a>
                        <span class="icon-thumbnail">CR</span>
                     </li>
                     
                     <li>
                        <a href="./countries">Country Config.</a>
                        <span class="icon-thumbnail">CO</span>
                     </li>
                     
                     <li>
                        <a href="./states">State Config.</a>
                        <span class="icon-thumbnail">SE</span>
                     </li>
                     
                     <li>
                        <a href="./customertypes">Customer Type Config.</a>
                        <span class="icon-thumbnail">CT</span>
                     </li>
                     
                      <li>
                        <a href="./sitetypes">Site Type Config.</a>
                        <span class="icon-thumbnail">ST</span>
                     </li>
                     
                      <li>
                        <a href="./equipmentcategories">Equ Category Config.</a>
                        <span class="icon-thumbnail">EC</span>
                     </li>
                     
                      <li>
                        <a href="./equipmenttypes">Equ Type Config.</a>
                        <span class="icon-thumbnail">ET</span>
                     </li>
                     
                      <li>
                        <a href="#">Event Type Config.</a>
                        <span class="icon-thumbnail">EY</span>
                     </li>
                     
                     <li>
                        <a href="#">Event Config.</a>
                        <span class="icon-thumbnail">EV</span>
                     </li>
                     
                      <li>
                        <a href="#">Inspection Config.</a>
                        <span class="icon-thumbnail">IN</span>
                     </li> -->
                     
                      <li>
                        <a href="./userroles">User Role Config.</a>
                        <span class="icon-thumbnail">UR</span>
                     </li> 
                     
                      <li>
                        <a href="./users">User Config.</a>
                        <span class="icon-thumbnail">US</span>
                     </li>
                     
                  </ul>
               </li>
          
           </c:if>










				</ul>


			</c:if>



			<div class="clearfix"></div>
		</div>



	</nav>


	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					 <a href="./dashboard"><img src="resources/img/logo01.png" alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx tktSearch" id="SearcSElect">
                          

 <div class="ui loading search selection  dropdown  width oveallsearch">
                                         <input id="myInput" name="tags" type="text">

                                         <div class="default text">search</div>
                                         <i class="dropdown icon"></i>
                                         <div id="myDropdown" class="menu">

                                                <c:if test="${not empty access}">
                                                       <div class="item">Dashboard</div>

						       							<c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Customer View</div>
                                                       </c:if>
                                                       
                                                       <div class="item">Site View</div>

                                                       <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Tickets</div>
                                                       </c:if>
                                                       
                                                       <div class="item">Events</div>
                                                       
                                                       <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Analysis</div>
                                                       </c:if>
                                                       
                                                       <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Site Configuration</div>
                                                       </c:if>

                                                       <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Equipment Configuration</div>
                                                       </c:if>

                                                       <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Customer Configuration</div>
                                                       </c:if>

                                                       <c:if test="${access.customerListView == 'visible'}">
                                                             <div class="item">User Configuration</div>
                                                       </c:if>


                                                </c:if>


                                         </div>
                                  </div>
                          
                          
                          
                        </div>
				
				 <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user_01.png" width="32" height="32">
                  </span>
                  
                   <c:if test="${not empty access}">         
                              <p class="user-login">${access.userName}</p>
                  </c:if>
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="logout1" class="dropdown-item color logout"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
               
               	<c:if test="${not empty access}">
							<c:if test="${access.customerListView == 'visible'}">
								<div>
								  <span data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><p class="center m-t-5 tkts"><img class="m-t-5" src="resources/img/tkt.png"><p class="create-tkts">New Ticket</p></p></span>
								
               </div>
               
							</c:if>
						</c:if>
						
						
						
              
			
				
			</div>
		</div>
		<div class="page-content-wrapper" id="QuickLinkWrapper1">

			<div class="content ">

				<div class="container-fixed-lg">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="#"><i class="fa fa-home mt-7"></i></a></li>
                     <li class="breadcrumb-item"><a href="#">Configuration</a></li>
                     <li class="breadcrumb-item active">Site Configuration</li>
                  </ol>
               </div>



				<div id="newconfiguration" class="">
					<div class="card card-transparent mb-0">
						<!-- Table Start-->
						<div class="padd-3 sortable" id="equipmentdata">
								<div class="row">
								<!-- <div class="card-header">
                                    <div class="card-title tbl-head ml-15">Equipment Curing Data</div>
                                 </div> -->
                                 
                                 
                                 
								<div class="col-lg-12 col-xs-12 col-md-12"
									id="datadetails">

									<div class="card card-default bg-default" data-pages="card">
										<!-- <div class="card-header">
											<div class="card-title tbl-head ml-15">
												<i class="fa fa-filter" aria-hidden="true"></i> Event
												Filters
											</div>
										</div> -->

										
										
                     <div class="card-header ">
                     
                     
                    <%--  <c:choose>
  							<c:when test="${empty site.siteId}">
    							<div class="card-title">Add New Site</div>
  							</c:when>
  							<c:when test="${site.siteId == 0}">
    							<div class="card-title">Add New Site</div>
  							</c:when>
  							<c:when test="${not empty site.siteId}">
    							<div class="card-title">Edit Site Details</div>
    							<form:hidden path="siteId" />
  							</c:when>
  							<c:otherwise>
    							
  							</c:otherwise>
						</c:choose>
						 --%>
								
                        
                     </div>

									 <c:url var="addAction" value="/addnewsite" ></c:url>                  
                 
    			<%-- <form:form action="${addAction}" modelAttribute="site" class="ui form validation-form"></form:form> --%>
    			 <form:form action="${addAction}" modelAttribute="site" class="ui form noremal">
    			 	<div class="col-md-12 padd-0">
    			 		<div class="col-md-1">
    			 			<div class="field required m-t-6">
    			 			 	<label>Site Type</label>
    			 			</div>
    			 		</div>
    			 		<div class="col-md-2">
    			 			<div class="field">
    			 				<input type="text" name="txtSiteType"  id="txtSiteType" placeholder="Site Type">
    			 			</div>	 			
    			 		</div>
    			 		<div class="col-md-1 ">
    			 			<div class="field required m-t-6">
    			 				  <label>Short Name</label>
    			 			</div>    			 			
    			 		</div>
    			 		<div class="col-md-2">
    			 			<div class="field">
    			 			 <input type="text" name="txtShortName"  id="txtShortName" placeholder="Short Name">
						    </div>
    			 		</div>
    			 		<div class="col-md-1">
    			 			<div class="field required m-t-6">
    			 			 	<label>Description</label>
    			 			</div>
    			 		</div>
    			 		<div class="col-md-2">
    			 			<div class="field">
    			 			<input id="txtSiteDescription" name="txtSiteDescription" placeholder="Site Description" type="text">
    			 			</div>
    			 		</div>
    			 		<div class="col-md-1">
    			 		<div class="field required m-t-6">
    			 			 <label>Type Status</label>
    			 			</div>
    			 		</div>
    			 		<div class="col-md-2">
    			 			<div class="field">
    			 			 	<select class="ui search selection dropdown"  name="ddlTypeStatus" id="ddlTypeStatus">
						        <option value="">Select</option>
						      </select>
    			 			</div>
    			 		</div>
    			 	</div>
 					<div class="col-md-12 center m-t-10">
 							<div class="btn btn-success  submit">Create</div>
				        	<div class="btn btn-primary clear m-l-15">Reset</div>
 					</div> 					
      			</form:form>
									</div>
								
								</div>


							</div>

						</div>


						<!-- Table End-->
						
						<!-- Table Start-->
						<div class="padd-3 sortable" id="equipmentdata">
							<div class="row">								
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails" style="height:260px;">
									<div class="card card-default bg-default" data-pages="card">
										<div class="padd-5 table-responsive">
                                        	<table id="example" class="table table-striped table-bordered"
                                            width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                 <th style="width:10%;">Site Code</th>
                                                 <th style="width:10%;">Site Type</th>
                                                  <th style="width:20%;">Site Name</th>
                                                  <th style="width:15%;">Customer</th>
                                                  <th style="width:10%;">State</th>
                                               	  <th style="width:10%;">Country</th>
                                               	  <th style="width:10%;">Site Status</th>
                                                  <th style="width:7%;">Edit</th>
                                                  <th style="width:8%;">De-activate</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               


                                            </tbody>
                                        </table>


                                    </div>
                                   </div>
								</div>


							</div>

						</div>
						

						<!-- Table End-->
					</div>
				</div>


			</div>

			<div class="container-fixed-lg footer mb-0">
				<div class="container-fluid copyright sm-text-center">
					<p class="small no-margin pull-left sm-pull-reset">
						<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
							CLEAN ENERGY</span>. <span class="hint-text">All rights reserved.
						</span>
					</p>
					<p class="small no-margin pull-rhs sm-pull-reset">
						<span class="hint-text">Powered by</span> <span>MESTECH
							SERVICES PVT LTD</span>.
					</p>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Side Bar Content Start-->

	


	<div class="quickview-wrapper  builder hidden-sm hidden-xs"
		id="builder">
		<div class="p-l-10 p-r-10 ">
			<a class="builder-close quickview-toggle pg-close"></a>
			<a class="builder-toggle" data-toggle="quickview"
				data-toggle-element="#builder"><img
				src="resources/img/ImageResize_06.png"></a>
			<ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary"
				id="builderTabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#tabLayouts" role="tab"
					aria-controls="home"><span>Quick Link</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active " id="tabLayouts" role="tabcard">
					<div class="scrollable">
						<div class="p-r-50">
							<div class="list">
								<ul>
								<!-- 	<li><a href="#QuickLinkWrapper1">Ticket Filters</a></li> -->
									<li><a href="#QuickLinkWrapper2">List Of Events</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>






	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min3.js" type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>









	<div id="gotop"></div>

	<!-- Share Popup End !--> 
<!-- Modal -->
<div id="tktCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog modal-top">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">New Ticket</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"><span class="errormess">*</span> All fields are mandatory</p>
				 </div>            
        		<form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form validation-form"> 
             
              <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Site</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width evtDropdown" id="ddlSite" name="ddlSite" path="siteID">
                        <form:option value="">Select</form:option>
                        <form:options  items="${siteList}" />
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Type</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width evtDropdown" id="ddlType" name="ddlType"  path="ticketType">
                        <form:option value="">Select</form:option>
                        <form:option value="Operation">Operation</form:option>
                        <form:option value="Maintenance">Maintenance</form:option>
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Category</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width category evtDropdown" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                         <form:option data-value="Operation" value="">Select</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                          <form:option data-value="Maintenance" value="">Select</form:option>
                    </form:select> 
                  </div>
                  
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                  <div class="col-md-4 col-xs-12"><label class="fields required m-t-10">Subject</label></div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<form:input  placeholder="Subject"  name="Subject"  type="text"  id="txtSubject" path="ticketDetail" maxLength="50"/>
                  	</div>
                  	</div>
               </div> 
              
              <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Priority</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width evtDropdown" id="ddlPriority" name="user" path="priority">
                        <form:option value="">Select </form:option>
                        <form:option data-value="3" value="3">High</form:option>
                        <form:option data-value="2" value="2">Medium</form:option>
                        <form:option data-value="1" value="1">Low</form:option>
                    </form:select> 
                  </div>
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Description</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<textarea id="txtTktdescription"  name="description" style="resize: none;" placeholder="Ticket Description" maxLength="120"></textarea>
                  	</div>
                    
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
				       <div class="btn btn-success  submit">Create</div>
				        <div class="btn btn-primary clear m-l-15">Reset</div>
				  </div>
  <!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
  <!-- <div class="ui error message"></div> -->
</form:form>
				<!-- <div class="ui error message"></div> -->
				
	  		</div>
	  </div>
    </div>
 </div>
<!-- Address Popup End !--> 
<!-- Modal -->
<div id="evntCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog modal-top">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">Event Ticket Creation</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"> All fields are mandatory</p>
				 </div>            
				 <!-- <span class="errormess">*</span> -->
        		<form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form validation-form"> 
             
              <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                     <div class="col-md-4 col-xs-12">
                     <p class="fields  m-t-10">Site</p>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<p class="fields  m-t-10" id="txtSiteName" path="siteID"></p>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <p class="fields  m-t-10">Type</p>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<p class="fields  m-t-10" id="txtTypeName" path="ticketType">Operation</p>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                     <div class="col-md-4 col-xs-12">
                     <p class="fields  m-t-10">Priority</p>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<p class="fields  m-t-10" id="txtPriority" path="priority"></p>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Category</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width category evtDropdown" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                         <form:option data-value="Operation" value="">Select</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                          <form:option data-value="Maintenance" value="">Select</form:option>
                    </form:select> 
                  </div>
                  
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                  <div class="col-md-4 col-xs-12"><label class="fields required m-t-10">Subject</label></div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<form:input  placeholder="Subject"  name="Subject"  type="text"  id="txtSubject" path="ticketDetail" maxLength="50"/>
                  	</div>
                  	</div>
               </div> 
              
              
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Description</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<textarea id="txtTktdescription"  name="description" style="resize: none;" placeholder="Ticket Description" maxLength="120"></textarea>
                  	</div>
                    
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
				       <div class="btn btn-success  submit">Create</div>
				        <div class="btn btn-primary clear m-l-15">Reset</div>
				  </div>
  <!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
  <!-- <div class="ui error message"></div> -->
</form:form>
				<!-- <div class="ui error message"></div> -->
				
	  		</div>
	  </div>
    </div>
 </div>
<!-- Address Popup End !--> 
	<script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
	<script>
         $(document).ready(function() {
            $(window).fadeThis({
               speed: 500,
            });
         });
      </script>


<script type="text/javascript">
      $(document).ready(function(){
          $('.ui.form.noremal')
  .form({
      inline: true,
      on: "blur",
    fields: {
    	txtSiteType: {
        identifier: 'txtSiteType',
        rules: [
          {
            type   : 'empty',
            prompt : 'select site type'
          }
        ]
      },
      ddlCustomer:  {
          identifier: 'ddlCustomer',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please select customer'
            }
          ]
        },
      txtSitename:  {
          identifier: 'txtSitename',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter site name'
            }
          ]
        },
      txtSiteDescription: {
    	  identifier: 'txtSiteDescription',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter site description'
            }
          ] 
      },
      txtContactperson: {
    	  identifier: 'txtContactperson',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Contact Person'
            }
          ] 
      },
      txtAddress: {
    	  identifier: 'txtAddress',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Address'
            }
          ] 
      },
      ddlCity: {
    	  identifier: 'ddlCity',
          rules: [
            {
              type   : 'empty',
              prompt : 'Select City'
            }
          ] 
      },
      ddlState: {
    	  identifier: 'ddlState',
          rules: [
            {
              type   : 'empty',
              prompt : 'Select State'
            }
          ] 
      },
      
      ddlCountry: {
    	  identifier: 'ddlCountry',
          rules: [
            {
              type   : 'empty',
              prompt : 'Select Country'
            }
          ] 
      },
      txtPostalcode: {
    	  identifier: 'txtPostalcode',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter postal code'
            }
          ] 
      },
      txtLongitude: {
    	  identifier: 'txtLongitude',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter longitude'
            }
          ] 
      },
      txtAltitude: {
    	  identifier: 'txtAltitude',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Altitude'
            }
          ] 
      },
      txtLatitude: {
    	  identifier: 'txtLatitude',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Latitude'
            }
          ] 
      },
      txtSitePONumber: {
    	  identifier: 'txtSitePONumber',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Site PO Number'
            }
          ] 
      },
      txtSiteoperator: {
    	  identifier: 'txtSiteoperator',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Site Operator'
            }
          ] 
      },
      txtManafacturer: {
    	  identifier: 'txtManafacturer',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Manafacturer'
            }
          ] 
      },
      txtModulename: {
    	  identifier: 'txtModulename',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Module name'
            }
          ] 
      },
      txtManufacture: {
    	  identifier: 'txtManufacture',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Manufacture'
            }
          ] 
      },
      txtCommType: {
    	  identifier: 'txtCommType',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Communication Type'
            }
          ] 
      },
      txtCollecType: {
    	  identifier: 'txtCollecType',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Collection Type'
            }
          ] 
      },
      txtFileType: {
    	  identifier: 'txtFileType',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter File Type'
            }
          ] 
      },
      txtIncome: {
    	  identifier: 'txtIncome',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Income'
            }
          ] 
      },
      ddlCurrency: {
    	  identifier: 'ddlCurrency',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Currency'
            }
          ] 
      },
      txtEmailid: {
    	  identifier: 'txtEmailid',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Email Id'
            }
          ] 
      },
      txtMobile: {
    	  identifier: 'txtMobile',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Mobile No'
            }
          ] 
      },
      txtTelephone: {
    	  identifier: 'txtTelephone',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Telephone No'
            }
          ] 
      },
      txtFax: {
    	  identifier: 'txtFax',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter FAX No'
            }
          ] 
      },
      txtLocationMap: {
    	  identifier: 'txtLocationMap',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Location Map'
            }
          ] 
      },
      txtCustomerReference: {
    	  identifier: 'txtCustomerReference',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Customer Reference'
            }
          ] 
      },
      txtCustomerNaming: {
    	  identifier: 'txtCustomerNaming',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Customer Naming'
            }
          ] 
      },
      txtInstallationCapacity: {
    	  identifier: 'txtInstallationCapacity',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Installation Capacity'
            }
          ] 
      },
      txtTimeZone: {
    	  identifier: 'txtTimeZone',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Time Zone'
            }
          ] 
      },
      ddlSiteStatus: {
    	  identifier: 'ddlSiteStatus',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Site Status'
            }
          ] 
      },
      ddlOperationmode: {
    	  identifier: 'ddlOperationmode',
          rules: [
            {
              type   : 'empty',
              prompt : 'Select Operation Mode'
            }
          ] 
      },
      txtLocalFtpDirectory: {
    	  identifier: 'txtLocalFtpDirectory',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Local FTP Directory'
            }
          ] 
      },
      txtLocalFtpDirectoryPath: {
    	  identifier: 'txtLocalFtpDirectoryPath',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Local FTP Directory Path'
            }
          ] 
      },
      txtLocalFtpHomeDirectory: {
    	  identifier: 'txtLocalFtpHomeDirectory',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Local FTP Home Directory'
            }
          ] 
      },
      txtLocalFTPUserName: {
    	  identifier: 'txtLocalFTPUserName',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Local FTP UserName'
            }
          ] 
      },
      txtLocalFtpPassword: {
    	  identifier: 'txtLocalFtpPassword',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Local FTP Password'
            }
          ] 
      },
      txtDataLoggerId: {
    	  identifier: 'txtDataLoggerId',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Data Logger Id'
            }
          ] 
      },
      txtServiceCode: {
    	  identifier: 'txtServiceCode',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Service Code'
            }
          ] 
      },
      txtInstallCapacity: {
    	  identifier: 'txtInstallCapacity',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Install Capacity'
            }
          ] 
      },
      txtAPIUrl: {
    	  identifier: 'txtAPIUrl',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter API URL'
            }
          ] 
      },
      txtAPIKey: {
    	  identifier: 'txtAPIKey',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter API Key'
            }
          ] 
      },
      txtRemoteFtpServer: {
    	  identifier: 'txtRemoteFtpServer',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Remote FTP Server'
            }
          ] 
      },
      txtRemoteFtpUserName: {
    	  identifier: 'txtRemoteFtpUserName',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Remote FTP UserName'
            }
          ] 
      },
      txtRemoteFtpPassword: {
    	  identifier: 'txtRemoteFtpPassword',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Remote FTP Password'
            }
          ] 
      },
      txtRemoteFtpServerPort: {
    	  identifier: 'txtRemoteFtpServerPort',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Remote FTP Server Port'
            }
          ] 
      },
      txtRemoteFtpDirectoryPath: {
    	  identifier: 'txtRemoteFtpDirectoryPath',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Remote FTP Directory Path'
            }
          ] 
      },
      skills: {
        identifier: 'skills',
        rules: [
          {
            type   : 'minCount[1]',
            prompt : 'Please select at least two skills'
          }
        ]
      },
      gender: {
        identifier: 'gender',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please select a gender'
          }
        ]
      },
      username: {
        identifier: 'username',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter a username'
          }
        ]
      },
      password: {
        identifier: 'password',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter a password'
          },
          {
            type   : 'minLength[6]',
            prompt : 'Your password must be at least {ruleValue} characters'
          }
        ]
      },
      terms: {
        identifier: 'terms',
        rules: [
          {
            type   : 'checked',
            prompt : 'You must agree to the terms and conditions'
          }
        ]
      }
    }
  });

      });
    </script>


</body>

</html>


