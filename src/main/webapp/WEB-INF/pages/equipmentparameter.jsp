<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="true" %>


<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
      <meta charset="utf-8"/>
      <title>:: Welcome To EIRA ::</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
      <link rel="icon" type="image/x-icon" href="favicon.ico"/>
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-touch-fullscreen" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="default">
      <meta content name="description"/>
      <meta content name="author"/>

      
      <link href="resources/css/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/select2.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/pages-icons.css" rel="stylesheet" type="text/css">
      <link href="resources/css/animate.css" rel="stylesheet"  />
      <link href="resources/css/pages.css" class="main-stylesheet"  rel="stylesheet" type="text/css"/>
      <link href="resources/css/styles.css" rel="stylesheet" type="text/css">
      <link href="resources/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
      <link href="resources/css/site.css" rel="stylesheet" >
      <link href="resources/css/semantic.min.css" rel="stylesheet" type="text/css">
      <link href="resources/css/calendar.min.css" rel="stylesheet" type="text/css">
      <link href="resources/css/bootstrap.min.3.37.css" type="text/css" rel="stylesheet">
      <link href="resources/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="resources/css/semantic.min.css">
      <script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
      <script src="resources/js/searchselect.js" type="text/javascript"></script>
  	  <script src="resources/js/CustomeScript.js" type="text/javascript"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>
	  
	  
	  
      <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
      <script type="text/javascript" src="http://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mockjax/1.6.2/jquery.mockjax.min.js"></script>
      <script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
      <script type="text/javascript" src="https://mpryvkin.github.io/jquery-datatables-row-reordering/1.2.3/jquery.dataTables.rowReordering.js"></script>
      
      
	<script type="text/javascript">
	 $(window).load(function(){
    	 $('.ui.dropdown').dropdown({forceSelection:false});
    });
	 
	 function rowDelete(obj) {
                   debugger;
                   alert('1');
                   obj.parentNode.parentNode.remove();
               }
        	   
	 
	 
        $(document).ready(function() {  
        	
        	$('#tktCreation').hide();        	
        	$('#ddlSite').dropdown('clear');
        	$('#ddlType').dropdown('clear');
        	$('#ddlCategory').dropdown('clear');
        	$('#ddlPriority').dropdown('clear');
        	$('#txtSubject').val('');
        	$('#txtTktdescription').val('');
        	
        	
        	

        	 $('#example').DataTable({     
   		      "iDisplayLength": 50,
   		      "rowReorder": true,
        	 });
        	
        	$('.dataTables_filter input[type="search"]').attr('placeholder','search');  
        	
        	   $('#addBtn').click(function(){
        	          debugger;
        	        var num = $('#txtbox').val();
        	        var rowCount = $('table#example tr:last').index() + 1;
        	        var TotalCount = parseInt(rowCount) + parseInt(num);
        	        for(var i=rowCount;i< TotalCount;i++){
        	        	$('#example tbody').append('<tr><td><form class="ui form"><div class="field"><input name="Append Parameter" placeholder="Parameter Name"  type="text" class="parametName" style="width:80% !important;"></div></form></td><td><form class="ui form"><div class="field"><input name="Append Parameter" placeholder="UOM"  type="text" class="parametName" style="width:80% !important;"></div></form></td><td><div class="form-group"><select class="ui fluid search selection dropdown width" id="drop"><option value="">Standard Parameter</option><option value="1">5</option><option value="0">10</option></select></div></td><td><form class="ui form"><div class="field"><input name="Append Parameter" placeholder="Description"  type="text" class="parametName" style="width:80% !important;"></div></form></td><td><div class="form-group"><select class="ui fluid search selection dropdown width" id="drop"><option value="">Status</option><option value="1">AActive</option><option value="0">Not Active</option></select></div></td><td style="width: 15%;text-align:center;vertical-align: middle;">'+i+'</td><td class="links" id="del" style="width: 15%;vertical-align:middle;"><a onclick="rowDelete(this);"><i class="fa fa-trash-o center" aria-hidden="true" title="Delete"></i></a></div></td></tr>');          
        	        	 //$('#example tbody').append('<tr><td><form class="ui form"><div class="field"><input name="Append Parameter" placeholder="Parameter Name"  type="text" class="parametName"></div></form></td><td><div class="form-group"><select class="ui fluid search selection dropdown width" id="drop"><option value="">UOM</option><option value="1">5</option><option value="0">10</option></select></div></td><td><div class="form-group"><select class="ui fluid search selection dropdown width" id="drop"><option value="">UOM</option><option value="1">5</option><option value="0">10</option></select></div></td><td class=" middle"><input type="checkbox" name="vehicle" value="Checkbox"></td><td style="width: 15%;text-align:center;vertical-align: middle;">'+i+'</td><td class="links" id="del" style="width: 15%;vertical-align:middle;"><a onclick="rowDelete(this);"><i class="fa fa-trash-o center" aria-hidden="true" title="Delete"></i></a></div></td></tr>');     
        	        	 //$('#example tbody').append('<tr> <td><form class="ui form"><div class="field"><input name="Append Parameter" placeholder="Parameter Name"  type="text" class="parametName" style="width:80%  !important;"></div></form></td> <td><form class="ui form"><div class="field"><input name="Append Parameter" placeholder="Parameter Name"  type="text" class="parametName" style="width:80%  !important;"></div></form></td><td><form class="ui form"><div class="field"><input name="Append Parameter" placeholder="Parameter Name"  type="text" class="parametName" style="width:80%  !important;"></div></form></td> <td><div class="form-group"><select class="ui fluid search selection dropdown width" id="drop"><option value="">UOM</option><option value="1">5</option><option value="0">10</option></select></div></td><td class=" middle"><div class="form-group"><select class="ui fluid search selection dropdown width" id="drop"><option value="">UOM</option><option value="1">5</option><option value="0">10</option></select></div></td><td class="links" id="del" style="width: 15%;vertical-align:middle;"><a onclick="rowDelete(this);"><i class="fa fa-trash-o center" aria-hidden="true" title="Delete"></i></a></div></td></tr>');      
        	        }
        	        });
        	   
        	   
        	   
           
            $('.close').click(function(){
				$('.clear').click();
				$('#tktCreation').hide();
				$('.clear').click();
			    $('.category').dropdown();
			    $('.SiteNames').dropdown();
			});
            
            $('#btnCreate').click(function() {
        		if($('#ddlCategory').val== "") {
        	    	alert('Val Empty');
        	        $('.category ').addClass('error')
        	    }
        	});
            
            
            $('.clear').click(function(){
            	$('.search').val('');
            })
            
            $('#ddlType').change(function(){              
                $('.category').dropdown('clear')
               });
			
			 $("#searchSelect").change(function() {
        	                var value = $('#searchSelect option:selected').val();
        	                var uid =  $('#hdneampmuserid').val();
        	             	redirectbysearch(value,uid);
        	  });
			 
			 var validation  = {
     				 txtSubject: {
     	                  identifier: 'txtSubject',
     	                  rules: [{
     	                    type: 'empty',
     	                    prompt: 'Please enter a value'
     	                  },
     	                  {
     	                      type: 'maxLength[50]',
     	                      prompt: 'Please enter a value'
     	                    }]
     	                },
     	                 ddlSite: {
     	                  identifier: 'ddlSite',
     	                  rules: [{
     	                    type: 'empty',
     	                    prompt: 'Please enter a value'
     	                  }]
     	                },
     	               ddlType: {
     	                     identifier: 'ddlType',
     	                  rules: [{
     	                    type: 'empty',
     	                    prompt: 'Please enter a value'
     	                  }]
     	               },
     	               ddlCategory: {
     	                  identifier: 'ddlCategory',
     	                  rules: [{
     	                    type: 'empty',
     	                    prompt: 'Please enter a value'
     	                  }]
     	               },
     	                ddlPriority: {
     	                  identifier: 'ddlPriority',
     	                  rules: [{
     	                    type: 'empty',
     	                    prompt: 'Please select a dropdown value'
     	                  }]
     	                },
     	                description: {
     	                  identifier: 'description',
     	                  rules: [{
     	                    type: 'empty',
     	                    prompt: 'Please Enter Description'
     	                  }]
     	                }
			};
     		var settings = {
     		  onFailure:function(){
     		      return false;
     		    }, 
     		  onSuccess:function(){    
     		    $('#btnCreate').hide();
     		    $('#btnCreateDummy').show()
     		  	//$('#btnReset').attr("disabled", "disabled");   		   	
     		    }};     		  
     		$('.ui.form.validation-form').form(validation,settings);

             
        });
      </script>
    
     <script type='text/javascript'>//<![CDATA[
         $(window).load(function(){
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
         
         $('.ui.dropdown').dropdown({
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: true, 
             showOnFocus: true,
             on: "click" 
           });

		$('.ui.dropdown.oveallsearch').dropdown({
             onChange: function (value, text, $selectedItem) {                
                var uid =  $('#hdneampmuserid').val();
                redirectbysearchvalue(value,uid);
             },
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: false, 
             showOnFocus: true,
             on: "click" 
           });
           
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );


         })
         
      </script>
       <style type="text/css">
      /* .ui.form textarea:not([rows]) {
		    height: 8em;
		    min-height: 8em;
		    max-height: 24em;
		}

		.ui.form textarea:not([rows]) {
		    height: 4em;
		    min-height: 4em;
		    max-height: 24em;
		}
  
		  .input-sm, .form-horizontal .form-group-sm .form-control {
		    font-size: 13px;
		    min-height: 25px;
		    height: 32px;
		    padding: 8px 9px;
		} */
		</style>
  
       
        
  </head>
  <body  class="fixed-header">
  
  <input type="hidden" value="${access.userID}" id="hdneampmuserid">
  
  
         <nav class="page-sidebar" data-pages="sidebar">
        
         <div class="sidebar-header">
            <a href="./dashboard"><img src="resources/img/logo01.png" class="eira-logo" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management"></a>
            <i class="fa fa-bars bars"></i>
         </div>
         
         <div class="sidebar-menu">
          
          <c:if test="${not empty access}">
          
            <ul class="menu-items">
            
           <c:if test="${access.dashboard == 'visible'}">
          
          		 <li class="" style="margin-top:2px;">
                  <a href="./dashboard" class="detailed">
                  <span class="title">Dashboard</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-tachometer"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.overview == 'visible'}">
          		 <li>
                  <a href="#"><span class="title">Overview</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-list-alt"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.systemMonitoring == 'visible'}">
          		<li>
                  <a href="#"><span class="title">System Monitoring</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-desktop"></i></span>
               </li>
           </c:if>
           
             <c:if test="${access.visualization == 'visible'}">
           <li class="active">
                   <a href="javascript:;"><span class="title">Visualization</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="fa fa-desktop" aria-hidden="true"></i></span>
                  <ul class="sub-menu">
                       
                    <c:if test="${access.customerListView == 'visible'}">
          				<li class="active">
                        	<a href="./customerlist">Customer View</a>
                        	<span class="icon-thumbnail"><i class="fa fa-eye"></i></span>
                     	</li>
                    </c:if>
           
           
           
                     <li>
                        <a href="./sitelist">Site View</a>
                        <span class="icon-thumbnail"><i class="fa fa-map-marker"></i></span>
                     </li>
                   <!--   <li>
                        <a href="./equipmentlist">Equipment View</a>
                        <span class="icon-thumbnail"><i class="fa fa-cog"></i></span>
                     </li> -->
                  </ul>
               </li>
              
           </c:if>

           
           
            <c:if test="${access.analytics == 'visible'}">
           <li>
                  <a href="#">
                  <span class="title">Analytics</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-pie-chart"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.portfolioManagement == 'visible'}">
          <li>
                  <a href="#">
                  <span class="title">Portfolio Manager</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-briefcase"></i></span>
               </li>
           </c:if>
           
          <c:if test="${access.ticketing == 'visible'}">
            <li>
                    <a href="javascript:;"><span class="title">Operation & Maintenance</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail" style="padding-top:5px !important;"><img src="resources/img/maintance.png"></span>
                  <ul class="sub-menu">
                       
                    <c:if test="${access.customerListView == 'visible'}">
          				<li>
                        	<a href="./ticketskpi">Tickets KPI</a>
                        	<span class="icon-thumbnail"><i class="fa fa-ticket"></i></span>
                     	</li>
                     	
                     	
                     	
                     	
                    </c:if>
                    <li>
                        	<a href="./eventdetails">Events</a>
                        	<span class="icon-thumbnail"><i class="fa fa-calendar"></i></span>
                     	</li>
                    
           
                  </ul>
               </li>         
           
           
           
           
           </c:if>
           
           
            <c:if test="${access.forcasting == 'visible'}">
          <li>
                  <a href="#"><span class="title">Forecasting</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-desktop"></i></span>
               </li>
           </c:if>
           
           	<c:if test="${access.analysis == 'visible'}">
          		<li>
                  <a href="./analysis"><span class="title">Analysis</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-area-chart"></i></span>
               </li>
           </c:if>
           
           
            <c:if test="${access.configuration == 'visible'}">
            
               <li>
                  <a href="javascript:;"><span class="title">Configuration</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="pg-tables"></i></span>
                  <ul class="sub-menu">
                     
                     <li>
                        <a href="./customers">Customer Config.</a>
                        <span class="icon-thumbnail">CU</span>
                     </li>
                     
                     <li>
                        <a href="./sites">Site Config.</a>
                        <span class="icon-thumbnail">SI</span>
                     </li>
                     
                      <li>
                        <a href="./equipmenttypes">Equipment Type Config.</a>
                        <span class="icon-thumbnail">ET</span>
                     </li>
                     
                     <li>
                        <a href="./equipments">Equipment Config.</a>
                        <span class="icon-thumbnail">EQ</span>
                     </li>
                     
                      <li class="active">
                        <a href="./equipmentparameters">Equipment Attribute Config.</a>
                        <span class="icon-thumbnail">EA</span>
                     </li>
                     
                     
                      <li>
                        <a href="./customermaps">CustomerMap Config.</a>
                        <span class="icon-thumbnail">CM</span>
                     </li>
                     
                   <!--   <li>
                        <a href="#">Data Logger Config.</a>
                        <span class="icon-thumbnail">DL</span>
                     </li>
                     
                     <li>
                        <a href="#">Equ-Attrib Config.</a>
                        <span class="icon-thumbnail">EA</span>
                     </li>
                     
                      <li>
                        <a href="#">Configuration Loader</a>
                        <span class="icon-thumbnail">CL</span>
                     </li>
                     
                     <li>
                        <a href="./activities">Activity Config.</a>
                        <span class="icon-thumbnail">AC</span>
                     </li>
                     
                      <li>
                        <a href="./timezones">Timezone Config.</a>
                        <span class="icon-thumbnail">TZ</span>
                     </li>
                     
                      <li>
                        <a href="./currencies">Currency Config.</a>
                        <span class="icon-thumbnail">CY</span>
                     </li>
                     
                      <li>
                        <a href="./unitofmeasurements">Unit Measurement Config.</a>
                        <span class="icon-thumbnail">UM</span>
                     </li>
                     
                     
                     <li>
                        <a href="./countryregions">Country Region Config.</a>
                        <span class="icon-thumbnail">CR</span>
                     </li>
                     
                     <li>
                        <a href="./countries">Country Config.</a>
                        <span class="icon-thumbnail">CO</span>
                     </li>
                     
                     <li>
                        <a href="./states">State Config.</a>
                        <span class="icon-thumbnail">SE</span>
                     </li>
                     
                     <li>
                        <a href="./customertypes">Customer Type Config.</a>
                        <span class="icon-thumbnail">CT</span>
                     </li>
                     
                      <li>
                        <a href="./sitetypes">Site Type Config.</a>
                        <span class="icon-thumbnail">ST</span>
                     </li>
                     
                      <li>
                        <a href="./equipmentcategories">Equ Category Config.</a>
                        <span class="icon-thumbnail">EC</span>
                     </li>
                     
                      <li>
                        <a href="./equipmenttypes">Equ Type Config.</a>
                        <span class="icon-thumbnail">ET</span>
                     </li>
                     
                      <li>
                        <a href="#">Event Type Config.</a>
                        <span class="icon-thumbnail">EY</span>
                     </li>
                     
                     <li>
                        <a href="#">Event Config.</a>
                        <span class="icon-thumbnail">EV</span>
                     </li>
                     
                      <li>
                        <a href="#">Inspection Config.</a>
                        <span class="icon-thumbnail">IN</span>
                     </li> -->
                     
                      <li>
                        <a href="./userroles">User Role Config.</a>
                        <span class="icon-thumbnail">UR</span>
                     </li>  
                     
                      <li>
                        <a href="./users">User Config.</a>
                        <span class="icon-thumbnail">US</span>
                     </li>
                     
                     
                       <li>
                        <a href="./masteruploads">Master Upload Config.</a>
                        <span class="icon-thumbnail">MU</span>
                     </li>
                     
                  </ul>
               </li>
          
           </c:if>
          
         </ul>
         
         
          </c:if>
          
          
         
            <div class="clearfix"></div>
         </div>
    
    
    
      </nav>
  
  
     <div class="page-container">
         <div class="header">
            <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="sidebar">
            </a>
            <div>
               <div class="brand inline">
               <a href="./dashboard"><img src="resources/img/logo01.png" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management" class="logos"></a>   
               </div>
            </div>
            <div class="d-flex align-items-center">
                 <div class="form-group required field searchbx" id="SearcSElect">
                           
                           
                            <div class="ui  search selection  dropdown  width oveallsearch">
                                         <input id="myInput" name="tags" type="text">

                                         <div class="default text">search</div>
                               <!--           <i class="dropdown icon"></i> -->
                                         <div id="myDropdown" class="menu">

                                                <c:if test="${not empty access}">
                                                       <div class="item">Dashboard</div>

						       							<%-- <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Customer View</div>
                                                       </c:if> --%>
                                                       
                                                       <div class="item">Site View</div>

                                                       <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Tickets KPI</div>
                                                       </c:if>
                                                       
                                                       <div class="item">Events</div>
                                                       
                                                       <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Analysis</div>
                                                       </c:if>
                                                       
                                                       <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Site Configuration</div>
                                                       </c:if>

                                                       <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Equipment Configuration</div>
                                                       </c:if>

                                                       <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Customer Configuration</div>
                                                       </c:if>
                                                       
                                                       
                                                        <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">CustomerMap Configuration</div>
                                                       </c:if>
                                                       
                                                       

                                                       <c:if test="${access.customerListView == 'visible'}">
                                                             <div class="item">User Configuration</div>
                                                       </c:if>


                                                </c:if>


                                         </div>
                                  </div>
                        </div>
                        <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user_01.png" width="32" height="32">
                  </span>
                  
                   <c:if test="${not empty access}">         
                              <p class="user-login">${access.userName}</p>
                  </c:if>
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="logout1" class="dropdown-item color logout"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
               
               
               
                <c:if test="${access.customerListView == 'visible'}">
                                         <div>
                  <p class="center mb-1 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img src="resources/img/tkt.png"></p>
                  <p class="create-tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false">New Ticket</p>
               </div>
          								</c:if><%-- <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user.png" width="32" height="32">
                  </span>
                  
                  
                   <c:if test="${not empty access}">         
                  		<p class="user-login">${access.userName}</p>
                  </c:if>
                  
                  
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="login" class="dropdown-item color"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
               <span ><p class="center m-t-5 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img class="m-t-5" src="resources/img/tkt.png"><p class="create-tkts">New Ticket</p></p></span>
                --%>
               
                 <!--  <p class="create-tkts">New Ticket</p> -->
               <!-- <a href="#" class="header-icon  btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><i class="fa fa-ticket fa-2x"></i></a> -->
               <!-- <a href="#" class="header-icon pg pg-alt_menu btn-link  sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a> -->
            </div>
         </div>
         <div class="page-content-wrapper ">
            <div class="content" id="QuickLinkWrapper1">               
               <div class="container-fixed-lg">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="./dashboard"><i class="fa fa-home mt-7"></i></a></li>
                   <li class="breadcrumb-item"><a href="#">Configuration</a></li>
                     <li class="breadcrumb-item active">Equipment Attribute Configuration</li>
                  </ol>
               </div>
         <div class="container-fixed-lg padd-3">
                 <div id="sitestatus" data-pages="card">
                          <div class="containerItems">
                   	<div class="sortable" id="customerlist">                		 
                        <div class="col-md-12 padd-0">
                             <div class="card" data-pages="card">    
                             	 <div class="card-header p-l-0 padd-0 m-h-30" >
                          			<div class="card-title pull-left tbl-head m-l-5">Customer List  </div>
                          		</div>
                  <div class="card card-transparent padd-5" style="padding-top:0px !important;">
                   	<div class="row">
                    	<div class="col-lg-12">
                        	<div class="col-lg-4">
                            	<div class="form-group">
                                	<label  class="fields">Data Logger</label> 
                                    	<select class="ui fluid search selection dropdown width" id="drop">
                                        <option value="">Data Logger</option>
                                        <option value="1">Webdyn</option>
                                        <option value="0">Webbox</option>
                                         </select>
                                 </div>
                              </div>
                              <div class="col-lg-4"></div>
                              <div class="col-lg-4">
                                     <form class="ui form">
                                            <div class="field">
                                              <label class="fields">Append Parameter</label>
                                                <input name="Append Parameter" placeholder="Append Parameter" id="txtbox" type="text" class="appendpara">
                                                <input type="button" class="btn btn-primary pull-right" id="addBtn" value="Add">
                                            </div>
                                          </form>
                                    </div>
                                 </div>
                             
                          
                     </div>
                  	</div>
                             </div>    
                  		</div>
               		</div>
                    
         			    <input type='hidden' id='ChartData' value='[{"sites":"day-6","totalenergy":6.02,"color": "#5B835B"},{"sites":"day-5","totalenergy":8.29,"color": "#5B835B"},{"sites":"day-4","totalenergy":10.10,"color": "#5B835B"},{"sites":"day-3","totalenergy":12.03,"color": "#5B835B"},{"sites":"day-2","totalenergy":13.66,"color": "#5B835B"},{"sites":"day-1","totalenergy":0.00,"color": "#5B835B"}]||[{"sites":"day-6","totalenergy":3.17,"color": "#5B835B"},{"sites":"day-5","totalenergy":4.37,"color": "#5B835B"},{"sites":"day-4","totalenergy":5.33,"color": "#5B835B"},{"sites":"day-3","totalenergy":6.34,"color": "#5B835B"},{"sites":"day-2","totalenergy":7.20,"color": "#5B835B"},{"sites":"day-1","totalenergy":0.00,"color": "#5B835B"}]||[{"sites":"day-6","totalenergy":3.00,"color": "#5B835B"},{"sites":"day-5","totalenergy":4.13,"color": "#5B835B"},{"sites":"day-4","totalenergy":5.04,"color": "#5B835B"},{"sites":"day-3","totalenergy":6.00,"color": "#5B835B"},{"sites":"day-2","totalenergy":6.81,"color": "#5B835B"},{"sites":"day-1","totalenergy":0.00,"color": "#5B835B"}]||[{"sites":"day-6","totalenergy":2.87,"color": "#5B835B"},{"sites":"day-5","totalenergy":3.96,"color": "#5B835B"},{"sites":"day-4","totalenergy":4.83,"color": "#5B835B"},{"sites":"day-3","totalenergy":5.75,"color": "#5B835B"},{"sites":"day-2","totalenergy":6.53,"color": "#5B835B"},{"sites":"day-1","totalenergy":0.00,"color": "#5B835B"}]||[{"sites":"day-6","totalenergy":3.72,"color": "#5B835B"},{"sites":"day-5","totalenergy":5.05,"color": "#5B835B"},{"sites":"day-4","totalenergy":6.16,"color": "#5B835B"},{"sites":"day-3","totalenergy":7.31,"color": "#5B835B"},{"sites":"day-2","totalenergy":8.32,"color": "#5B835B"},{"sites":"day-1","totalenergy":0.00,"color": "#5B835B"}]'> 
                   
             
                      </div>
                    </div>
              </div>
              
              
              <div class="container-fixed-lg padd-3">
                 <div id="sitestatus" data-pages="card">
                 	<div class="containerItems">
                   		<div class="sortable" id="customerlist">                		 
                        	<div class="col-md-12 padd-0">
                            	<div class="card" data-pages="card">    
                  					<div class="card card-transparent padd-5" style="padding-top:0px !important;">
                  						<div class="row m-t-15">
				                    		<div class="col-lg-12">
				                        		 <table id="example" class="table table-bordered">
			                                        <thead>
			                                      		<tr>
				                                            <th class="width-20">Parameter Name</th>
				                                            <th class="width-20">UOM</th>
				                                            <th class="width-15">Standared Parametet</th>
				                                            <th class="width-20">Descriptions</th>
				                                            <th class="width-15">Status</th>
				                                            <th class="width-5">Sequence No</th>	
				                                            <th class="width-5">Action</th>		                                          
			                                      		</tr>
			                                    	</thead>
			                                    	<tbody>
                                      					<!-- <tr>
                                      						<td>1</td>
                                      						<td>TEst</td>
                                      						<td>Test</td>
                                      						<td>TEst</td>
                                      						<td>Test</td>
                                      					</tr>
                                      					<tr>
                                      						<td>1</td>
                                      						<td>TEst</td>
                                      						<td>Test</td>
                                      						<td>TEst</td>
                                      						<td>Test</td>
                                      					</tr>
                                      					<tr>
                                      						<td>1</td>
                                      						<td>TEst</td>
                                      						<td>Test</td>
                                      						<td>TEst</td>
                                      						<td>Test</td>
                                      					</tr>
                                      					<tr>
                                      						<td>1</td>
                                      						<td>TEst</td>
                                      						<td>Test</td>
                                      						<td>TEst</td>
                                      						<td>Test</td>
                                      					</tr>
                                      					<tr>
                                      						<td>1</td>
                                      						<td>TEst</td>
                                      						<td>Test</td>
                                      						<td>TEst</td>
                                      						<td>Test</td>
                                      					</tr>  -->
                                    				</tbody>
                                      				</table>
				                            
				                            

				                            
				                        	</div>
				                     	</div>
				                     	                  					
                              		</div>
                  					</div>
                             	</div>    
                  			</div>
               			</div>
                      </div>
                    </div>
              </div>
               </div>
            
            </div>

            <div class="container-fixed-lg footer">
               <div class="container-fluid copyright sm-text-center">
                  <p class="small no-margin pull-left sm-pull-reset">
                     <span class="hint-text">Copyright &copy; 2017.</span>
                     <span>INSPIRE CLEAN ENERGY.</span>
                     <span class="hint-text">All rights reserved. </span>
                  </p>
                  <p class="small no-margin pull-rhs sm-pull-reset">
                     <span class="hint-text">Powered by</span>
                     <span>MESTECH SERVICES PVT LTD</span>.
                  </p>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
 
      <!-- Side Bar Content Start-->

      <!-- Side Bar Content End-->
   <!--   
      <div class="quickview-wrapper  builder hidden-sm hidden-xs" id="builder">
         <div class="p-l-10 p-r-0 ">
            <a class="builder-close quickview-toggle pg-close"  href="#"></a>
            <a class="builder-toggle" data-toggle="quickview" data-toggle-element="#builder"><img src="resources/img/ImageResize_06.png"></a>
            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary" id="builderTabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tabLayouts" role="tab" aria-controls="home"><span>Quick Links</span></a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active " id="tabLayouts" role="tabcard">
                  <div class="scrollable">
                     <div class="p-r-50">
                        <div class="list">
                           <ul>
                              <li><a href="#QuickLinkWrapper1">Customer List dfdfds</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div> -->
      <script src="resources/js/pace.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.gotop.js"></script>
      <script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
      <script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
      <script src="resources/js/tether.min.js" type="text/javascript"></script>
      <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery-easy.js" type="text/javascript"></script>
      <script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.actual.min.js"></script>
      <script src="resources/js/jquery.scrollbar.min.js"></script>
      <script type="text/javascript" src="resources/js/select2.full.min.js"></script>
      <script type="text/javascript" src="resources/js/classie.js"></script>
      <script src="resources/js/switchery.min.js" type="text/javascript"></script>
      <script src="resources/js/pages.min.js"></script>
      <script src="resources/js/card.js" type="text/javascript"></script>
      <script src="resources/js/scripts.js" type="text/javascript"></script>
      <script src="resources/js/demo.js" type="text/javascript"></script>
      <script src="resources/js/jquery.easing.min.js"></script>
      <script src="resources/js/jquery.fadethis.js"></script>

       
      <script src="resources/js/jquery.dataTables.min.js" type="text/javascript" ></script>
      <script src="resources/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
      <script src="resources/js/semantic.min.js" type="text/javascript" ></script>
      <script src="resources/js/calendar.min.js" type="text/javascript" ></script>
      <script src="resources/js/jquery.selectlistactions.js"></script>
    
    
    
    
    
    <div id="gotop"></div>
         <!-- Address Popup Start !-->
     <!-- Modal -->
<div id="tktCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog modal-top">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">New Ticket</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"><span class="errormess">*</span> All fields are mandatory</p>
				 </div>            
        		<form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form validation-form"> 
             
              <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Site Name</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width" id="ddlSite" name="ddlSite" path="siteID">
                        <form:option value="">Select</form:option>
                        <form:options  items="${siteList}" />
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Type</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width" id="ddlType" name="ddlType"  path="ticketType">
                        <form:option value="">Select</form:option>
                        <form:option value="Operation">Operation</form:option>
                        <form:option value="Maintenance">Maintenance</form:option>
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Category</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width category" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                         <form:option data-value="Operation" value="">Select</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Preventive Maintenance">Preventive Maintenance</form:option>
										
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                          <form:option data-value="Maintenance" value="">Select</form:option>
                    </form:select> 
                  </div>
                  
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                  <div class="col-md-4 col-xs-12"><label class="fields required m-t-10">Subject</label></div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<form:input  placeholder="Subject"  name="Subject"  type="text"  autocomplete="off" id="txtSubject" path="ticketDetail" maxLength="50"/>
                  	</div>
                  	</div>
               </div> 
              
              <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Priority</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width" id="ddlPriority" name="user" path="priority">
                        <form:option value="">Select </form:option>
                        <form:option data-value="3" value="3">High</form:option>
                        <form:option data-value="2" value="2">Medium</form:option>
                        <form:option data-value="1" value="1">Low</form:option>
                    </form:select> 
                  </div>
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Description</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<textarea id="txtTktdescription"  autocomplete="off" name="description" style="resize: none;" placeholder="Ticket Description" maxLength="120"></textarea>
                  	</div>
                    
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
				       <div class="btn btn-success  submit" id="btnCreate">Create</div>
				       <button class="btn btn-success" type="button" id="btnCreateDummy" tabindex="7"  style="display:none;">Create</button>
				        <div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
				  </div>
  <!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
  <!-- <div class="ui error message"></div> -->
</form:form>
				<!-- <div class="ui error message"></div> -->
				
	  		</div>
	  </div>
    </div>
 </div>
<!-- Address Popup End !--> 
      <script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
      <script>
         $(document).ready(function() {
            $(window).fadeThis({
               speed: 500,
            });
         });
      </script>


  </body>
</html>

