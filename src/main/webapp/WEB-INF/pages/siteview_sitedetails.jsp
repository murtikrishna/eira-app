	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
				<div class="card card-default bg-default slide-left"
											id="divsitedetails" data-pages="card">
											<div class="h-150">
												<div class="card-header ">
													<div class="card-title pull-left">site details</div>
													<div class="card-title pull-right">
														<i class="fa fa-globe font-size icon-color"
															aria-hidden="true"></i>
													</div>
												</div>
												<div class="card-block">

													<div class="row" style="margin-top: 5px;">
														<div class="col-lg-5 col-xs-5 col-md-5">
															<p>
																<strong>Site Code</strong>
															</p>
														</div>
														<div class="col-lg-1 col-xs-1 col-md-1">
															<p>:</p>
														</div>
														<div class="col-lg-5 col-xs-5 col-md-5">
															<p>${siteview.siteCode}</p>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-5 col-xs-5 col-md-5">
															<p>
																<strong>Capacity</strong>
															</p>
														</div>
														<div class="col-lg-1 col-xs-1 col-md-1">
															<p>:</p>
														</div>
														<div class="col-lg-5 col-xs-5 col-md-5">
															<p>${siteview.capacity}</p>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-5 col-xs-5 col-md-5">
															<p>
																<strong>Inv Capacity</strong>
															</p>
														</div>
														<div class="col-lg-1 col-xs-1 col-md-1">
															<p>:</p>
														</div>
														<div class="col-lg-5 col-xs-5 col-md-5">
															<p>${siteview.equipmentsCapacity}</p>
														</div>

													</div>
													<div class="row">
														<div class="col-lg-5 col-xs-5 col-md-5">
															<p>
																<strong>COD</strong>
															</p>
														</div>
														<div class="col-lg-1 col-xs-1 col-md-1">
															<p>:</p>
														</div>
														<div class="col-lg-5 col-xs-5 col-md-5">
															<p>${siteview.commisionningDate}</p>
														</div>
													</div>

												</div>
											</div>
											<div class="card-footer" id="QuickLinkWrapper2">


												<c:choose>
													<c:when test="${siteview.siteStatus=='0'}">
														<p>
															Status : <span class="siteStatusOffline">Offline</span>
														</p>
													</c:when>

													<c:when test="${siteview.siteStatus=='1'}">
														<p>
															Status : <span class="siteStatusActive">Active</span>
														</p>
													</c:when>

													<c:when test="${siteview.siteStatus=='2'}">
														<p>
															Status : <span class="siteStatusWarning">Warning</span>
														</p>
													</c:when>

													<c:when test="${siteview.siteStatus=='3'}">
														<p>
															Status : <span class="siteStatusDown">Error</span>
														</p>
													</c:when>

													<c:otherwise>
														<p>
															Status : <span class="offstxt">-</span>
														</p>
													</c:otherwise>
												</c:choose>





											</div>
							</div>