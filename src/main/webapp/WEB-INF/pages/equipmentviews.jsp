<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/loader.css" rel="stylesheet" type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet"
	rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/bootstrap.min3.3.37.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min3.css" type="text/css"
	rel="stylesheet">

<link rel="stylesheet" type="text/css"
	href="resources/css/jquery-ui.css">
<link href="resources/css/formValidation.min.css" rel="stylesheet"
	type="text/css">
<!--  <link href="resources/css/jquery-ui_cal.css" rel="stylesheet" type="text/css"> -->

<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>

<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script type="text/javascript" src="resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="resources/js/formValidation.min.js"></script>
<script type="text/javascript"
	src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.val.js"></script>


<script type="text/javascript" src="resources/js/toastr.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/toastr.css">
<link rel="stylesheet" type="text/css"
	href="http://codeseven.github.com/toastr/toastr.css">
	
 <script src="resources/js/chart/highchart.js"></script>
<script src="resources/js/chart/exporting.js"></script>
<script src="resources/js/chart/export-data.js"></script>
<script src="resources/js/chart/series-label.js"></script>
<script src="http://code.highcharts.com/stock/modules/no-data-to-display.js"></script>
 


<!-- <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script> -->
	
 


<!--  High Chart -->
<!-- <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
 -->

<style>
	.title-head {color:#0090d9;}
	.fieldslbl{
		color: #626262;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important;
    font-size: 12px !important;
    letter-spacing: normal !important;
    text-transform: none !important;
    font-weight: normal;
	}
	#example1 td a {
	color: #337ab7;
	font-weight: bold;
}
	
</style>

<script type="text/javascript">






function handlenullable(param){
	if(param == null || param == 'null'){
		return '';
	}
	else{
		return param;
	}
	
}


function LoadDataTable(ticketdetaillist,serialNumber,sitename,dateTime)
{
	 var ticketrows = "";
     for(var i=0; i < ticketdetaillist.length; i++)
		{
			var priority = ticketdetaillist[i].priority;
			if(priority == 1)
			{
				priority = "Low";
			}
			else if(priority == 2)
			{
				priority = "Medium";
			}
			else if(priority == 3)
			{
				priority = "High";
			}
			
			var state = ticketdetaillist[i].state;
			if(state == 1)
			{
				state = "Open";
			}
			else if(state == 2)
			{
				state = "Closed";
			}
			else if(state == 3)
			{
				state = "Hold";
			}
			
			var ticketStatus = ticketdetaillist[i].ticketStatus;
			
			if(ticketStatus == 1)
			{
				ticketStatus = "Created";
			}
			else if(ticketStatus == 2)
			{
				ticketStatus = "Assigned";
			}
			else if(ticketStatus == 3)
			{
				ticketStatus = "Inprogress";
			}
			
			if(ticketStatus == 4)
			{
				ticketStatus = "Unfinished";
			}
			else if(ticketStatus == 5)
			{
				ticketStatus = "Finished";
			}
			else if(ticketStatus == 6)
			{
				ticketStatus = "Closed";
			}
			else if(ticketStatus == 7)
			{
				ticketStatus = "Hold";
			}
			
			    
			
			
			<c:choose>
			<c:when test="${access.customerListView == 'visible'}">
			
			var href = '.\\ticketviews' + ticketdetaillist[i].ticketID;
			
			if(i==0)
			{
				
				ticketrows = "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketdetaillist[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketdetaillist[i].siteName) +"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketdetaillist[i].createdDateText) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketCategory) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketDetail) +"</td><td>" + handlenullable(ticketdetaillist[i].assignedToWhom) +"</td><td>" + handlenullable(ticketdetaillist[i].createdByName) +"</td><td>" + handlenullable(ticketdetaillist[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
			}
			else
			{
				ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketdetaillist[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketdetaillist[i].siteName) +"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketdetaillist[i].createdDateText) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketCategory) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketDetail) +"</td><td>" + handlenullable(ticketdetaillist[i].assignedToWhom) +"</td><td>" + handlenullable(ticketdetaillist[i].createdByName) +"</td><td>" + handlenullable(ticketdetaillist[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
			}
			
			</c:when>
			
			<c:otherwise>
			
			if(i==0)
			{
				
				ticketrows = "<tr><td>" + handlenullable(ticketdetaillist[i].siteName) +"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketdetaillist[i].createdDateText) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketCategory) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketDetail) +"</td><td>" + handlenullable(ticketdetaillist[i].assignedToWhom) +"</td><td>" + handlenullable(ticketdetaillist[i].createdByName) +"</td><td>" + handlenullable(ticketdetaillist[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
			}
			else
			{
				ticketrows = ticketrows + "<tr><td>" + handlenullable(ticketdetaillist[i].siteName) +"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketdetaillist[i].createdDateText) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketCategory) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketDetail) +"</td><td>" + handlenullable(ticketdetaillist[i].assignedToWhom) +"</td><td>" + handlenullable(ticketdetaillist[i].createdByName) +"</td><td>" + handlenullable(ticketdetaillist[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
			}
				
			
			</c:otherwise>
			</c:choose>

			
			
			
			
			
				
		
		} 
     
     $('#example1').append(ticketrows);
     
    
     var table = $('#example1').DataTable( {
    	 //"dom": '<"row"<"col-sm-9"l><"col-sm-1"B><"col-sm-2"f>>'+'t<"row"<"col-sm-6"i><"col-sm-6"p>>',
		 "order": [[ 0, "desc" ]],
 		 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		 "bLengthChange": true, 
		 "buttons": [
			 {
    			 extend:'excelHtml5',
    			 				title: 'Tickets List _'+ dateTime,

    		 }
	        ]
			 }); 
      
      table.buttons().container()
		.appendTo('#example1_wrapper .col-sm-6:eq(0)');
      $('.c-excel-plot-tickets .dt-buttons').append('<span class="c-excel-click-ticket" style="margin-left: 5px;cursor:pointer;"><i class="fa fa-download" aria-hidden="true" style="font-size: 20px;margin-left: 5px;"></i><span style="font-weight: 600;margin-left: 5px;">Excel</span></span>');
	     $(".buttons-excel.buttons-html5").addClass("hidden");
	     $(".dt-buttons").css("margin-top","6px");


    	
     
}
$(document).on('click','.c-excel-click-ticket',function(){
	$('.c-excel-plot-tickets .buttons-excel').trigger('click');
	});
		

function AjaxCallForEquipmentDetails() {  
	
	  $.ajax({
        type: 'GET',
        url: './getEquipmentDetails',
        success: function(msg){
      	  debugger;
      	
      	// $('#example').DataTable().clear().draw();
      	  var equipment = msg.equipment;
      	var sitename = msg.sitename;
            var equipmentHistory=msg.equipmentHistory;
            var eventdetaillist = msg.eventDetail;
            var ticketdetaillist = msg.ticketDetail;
            var equipmentgraphdata = msg.equipmentgraphdata;
            var yaxis = msg.yaxis;
            var chartData = eval("[" + equipmentgraphdata + "]");
           
            var vartickinterval =  48 * 60 * 36000;
            var today = new Date();
    		var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    		var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
    		var dateTime = date + ' ' + time;
            

     	   Highcharts.chart('analyticsChart', {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Equipment Performance',
                    style: {
  	                    color: 'black',
  	                    fontWeight: 'bold',
  	                    fontSize: '18px',
  	                    
  	                    fontFamily: 'Lucida Sans Unicode", Arial, Helvetica, sans-serif'
  	                    }  
                },
               
                credits: {
			        enabled: false
			      },
                xAxis: {
             	   title: {
             		  
                        text: 'Time',
                        style: {
      	                    color: 'black',
      	                    fontWeight: 'bold',
      	                    fontSize: '12px',
      	                    
      	                    fontFamily: 'Lucida Sans Unicode", Arial, Helvetica, sans-serif'
      	                    }  
                        
                    },
                 
             	   type: 'datetime'
             		  
             	  
                },
                lang: {
  		            noData: "No Data Found"
  		        },
  		        noData: {
  		            style: {
  		                fontWeight: 'bold',
  		                fontSize: '25px',
  		                color: '#303030'
  		            }
  		        },
                exporting: {
			        menuItemDefinitions: {
			            // Custom definition
			            label: {
			                onclick: function () {
			                    this.renderer.label(
			                        'You just clicked a custom menu item',
			                        100,
			                        100
			                    )
			                    .attr({
			                        fill: '#a4edba',
			                        r: 5,
			                        padding: 10,
			                        zIndex: 10
			                    })
			                    .css({
			                        fontSize: '1.5em'
			                    })
			                    .add();
			                },
			              
			            }
			        },
			        filename: equipment.serialNumber +'_' + sitename + '_' +  dateTime,
			        buttons: {
			            contextButton: {
			                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
			            }
			        }
			    },
                yAxis: {
                    title: {
                        text: yaxis,
                        style: {
      	                    color: 'black',
      	                    fontWeight: 'bold',
      	                    fontSize: '12px',
      	                    
      	                    fontFamily: 'Lucida Sans Unicode", Arial, Helvetica, sans-serif'
      	                    }  
                    },
                    min:'0',
                    labels: {
                        formatter: function () {
                            return '' +
                            this.axis.defaultLabelFormatter.call(this);
                            
                        }
                    }
                },
                /* tooltip: {
			        formatter: function () {
			           var ret,numericSymbols = ['kWh', 'MWh', 'GWh', 'TWh', 'PWh', 'EWh'],
			                        i = 6;
			                    if(this.y >=1000) {
			                        while (i-- && ret === undefined) {
			                            multi = Math.pow(1000, i + 1);
			                            if (this.y >= multi && numericSymbols[i] !== null) {
			                                ret = (this.y / multi) + ' '+numericSymbols[i];
			                            }
			                        }
			                    }
			                    return 'Energy :' + (ret ? ret : this.y);
			        },
                }, */
                 tooltip: {
    		    
    		    	pointFormat: '<span style="color:{series.color}">'+ yaxis +'</span>: <b>{point.y}</b> <br/>',
    		        shared: true
    		    }, 
                plotOptions: {
                  enabled:false
                 
                },
                series: [{
                	showInLegend: false,  
                	 name: ' ',
                    data: chartData

                }]
            });
            
            
            
           $('#EquipmentCode').append(equipment.equipmentCode);
           $('#CustomerReference').append(equipment.customerRefNumber);
           $('#SerialNumber').append(equipment.serialNumber);
           $('#Capacity').append(equipment.capacity);
           $('#Category').append(equipment.equipmentCategory);
           $('#EquipmentType').append(equipment.equipementType);
           $('#Description').append(equipmentHistory.description);
           $('#Remarks').append(equipmentHistory.remarks);
           $('#Components').append(equipment.components);
           $('#Disconnectrating').append(equipment.disconnectRating);
           $('#Disconnecttype').append(equipment.disconnectType);
           
           $('#InstalledDate').append(equipmentHistory.installationDate);
           $('#WarrantyDate').append(equipmentHistory.warantryExpire);
           $('#Equipmentselection').append(equipmentHistory.isPrimary);
         
           LoadDataTable(ticketdetaillist,equipment.serialNumber,sitename,dateTime);
         
            for(var i=0; i < eventdetaillist.length; i++)
    			{
    			 
            	
            	  var prioritytext="Low"
		        	  var priority = eventdetaillist[i].priority;
            	   
            	
            		if(priority == "1")
		  			{
		  				prioritytext = "Low";
		  			}
		  			else if(priority == "2")
		  			{
		  				prioritytext = "Medium";
		  			}
		  			else if(priority == "3")
		  			{
		  				prioritytext = "High";
		  			}
            		
            		
            	
    				
            		var action= '<div class="dropdown"><button  data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu dropevent" role="menu" aria-labelledby="dLabel" > <li  class="evtCol" data-toggle="modal" data-target="#evntCreation" data-backdrop="static" data-keyboard="false"><a href="#"><span id="evt">  Ticket</span></a></li> <li><a class="btnDeleteRecord" onclick="supresseventdetails(' + eventdetaillist[i].transactionId + ')">Suppress</a></li></ul></div>';
 		  			
 					var toottip= '<span data-toggle="tooltip" title=  "'+ eventdetaillist[i].errorMessage +'" > '+ eventdetaillist[i].errorCode +' </span>';
      				
  				$('#example').DataTable().row.add([toottip,eventdetaillist[i].errorMessage,eventdetaillist[i].eventTimestampText,eventdetaillist[i].lastEventTimestampText,eventdetaillist[i].eventOccurrence,eventdetaillist[i].siteName,eventdetaillist[i].equipmentName,eventdetaillist[i].equipmentType,prioritytext,eventdetaillist[i].siteID,eventdetaillist[i].priority,eventdetaillist[i].equipmentId,eventdetaillist[i].transactionId,eventdetaillist[i].customerNaming,eventdetaillist[i].capacity,action]).draw(false);
  				
						  
    			
    			
    			}
            
           $('#example').DataTable().columns([9,10,11,12]).visible(false);
            $(".theme-loader").hide(); 


            
	 },
        error:function(msg) { 
        	
        	//alert(0);
        	//alert(msg);
        	}
	}); 
	  
}







function supresseventdetails(transactionid)
{
	
	//alert(transactionid);
	
	 $.ajax({
	        type: 'POST',
	        contentType : 'application/json; charset=utf-8',
	        dataType : 'json',
	        url: './supresseventdetails'+ transactionid,
	        data: {'TransactionID' : transactionid }, 
	        success: function(msg){
	        	debugger;
										
	        	if(msg != null)
	        	{
	        		if(msg.status == "true")
	        			{
	        				$('#example').DataTable().row('.selected').remove().draw( false );	        			
	        			}
	        		
	        	}
	        $('#example').DataTable().row.add([eventdetaillist[i].errorCode,eventdetaillist[i].errorMessage,eventdetaillist[i].eventTimestampText,eventdetaillist[i].lastEventTimestampText,eventdetaillist[i].eventOccurrence,eventdetaillist[i].siteName,eventdetaillist[i].equipmentName,eventdetaillist[i].equipmentType,prioritytext,eventdetaillist[i].siteID,eventdetaillist[i].priority,eventdetaillist[i].equipmentId,eventdetaillist[i].transactionId,"Action"]).draw(false);
                
				
	        	
	        
	        },
	        error:function(msg) { 
	        	
	        	//alert(0); alert(msg);
	        	}
		}); 
	
	
	
	
	
	}






        $(document).ready(function() {
        	
var classstate = $('#hdnstate').val();
			
$("#visulaizationid").addClass(classstate);
$("#siteviewid").addClass(classstate);
        	

       	 $(".theme-loader").show();
         	 $(".theme-loader").animate({
                  opacity: "0"
              },10000);
         	 
         	 
     		
   		  AjaxCallForEquipmentDetails();
   		
       	  $('#btnCreate').click(function() {
        		if($('#ddlCategory').val== "") {
        	    	alert('Val Empty');
        	        $('.category ').addClass('error')
        	    }
        	})
        	
        	$('#ddlSite').dropdown('clear');
        	$('#ddlType').dropdown('clear');
        	$('#ddlCategory').dropdown('clear');
        	$('#ddlPriority').dropdown('clear');
        	$('#txtSubject').val('');
        	$('#txtTktdescription').val('');
        	
        	 toastr.options = {
       	           "debug": false,
       	           "positionClass": "toast-bottom-right",
       	           "progressBar":true,
       	           "onclick": null,
       	           "fadeIn": 500,
       	           "fadeOut": 100,
       	           "timeOut": 1000,
       	           "extendedTimeOut": 2000
       	         }
       	   
        	  var today = new Date();
     		var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
     		var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
     		var dateTime = date + ' ' + time;
     		 var table1 =$('#example').DataTable( {  
           		 "order": [[10, "desc" ],[ 14, "desc" ]], 
           		 "columnDefs": [ { orderable: false, targets: [-1] } , {
                        "targets": [ 9 ],
                       /*   "visible": false,  */
                        "searchable": false
                    }, {
                        "targets": [ 10 ],
                      /*  "visible": false,  */
                        "searchable": false
                    }, {
                        "targets": [ 11 ],
                     /*   "visible": false,  */
                        "searchable": false
                    }, {
                        "targets": [ 12 ],
                       /* "visible": false,  */
                        "searchable": false
                    }],
           
           		 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
           		 "bLengthChange": true,        	
           		 //"dom": '<"row"<"col-sm-9"l><"col-sm-1"B><"col-sm-2"f>>'+'t<"row"<"col-sm-6"i><"col-sm-6"p>>',
           		 "buttons": [
           			  {
               			extend:'excelHtml5',
               			 				title:'Events List' + '_'+ dateTime,

               			 exportOptions: {
               			 columns:[0,1,2,3,4,5,6,7,8,13,14],
               		            			 
               			 }
               		 }
            			
       		        ]
           		 
           		 
           		 });
        	 table1.buttons().container()
     		.appendTo('#example_wrapper .col-sm-6:eq(0)');
        	 
        	  $('.c-excel-plot-events .dt-buttons').append('<span class="c-excel-click-event" style="margin-left: 5px;cursor:pointer;"><i class="fa fa-download" aria-hidden="true" style="font-size: 20px;margin-left: 5px;"></i><span style="font-weight: 600;margin-left: 5px;">Excel</span></span>');
      	     $(".buttons-excel.buttons-html5").addClass("hidden");
      	     $(".dt-buttons").css("margin-top","6px");
  
  $(document).on('click','.c-excel-click-event',function(){
  	$('.c-excel-plot-events .buttons-excel').trigger('click');
  });
       	   
  $('#example').on("click", ".btnDeleteRecord", function(){
    console.log($(this).parent());
    $('#example').DataTable().row($(this).parents('tr')).remove().draw(false);
    toastr.info("Successfully Deleted");
  });
        	 
        	 
        	
        	$('#example tbody').on('click', 'tr', function () {
/* 		    	debugger;
 */		       
		         var SiteName = $('#example').DataTable().row(this).data()[5];        		
            		 var SiteId = $('#example').DataTable().row(this).data()[9]; 
            		 var PriorityId = $('#example').DataTable().row(this).data()[10]; 
            		 var Errormessage = $('#example').DataTable().row(this).data()[1]; 
            		 var EquipmentID = $('#example').DataTable().row(this).data()[11]; 
            		 var TransactionID = $('#example').DataTable().row(this).data()[12]; 
            	
            		
            		 var Priority = $('#example').DataTable().row(this).data()[8];
            		 $('#txtSiteName').html(SiteName);
            		 $('#txtPriority').html(Priority);  
            		 $('#txtEventSubject').html(Errormessage);
            		 $('#txtEquipmentID').html(EquipmentID);
            		 $('#txtEventTransaction').html(TransactionID);
            		
            		 $('#hdnSite').val(SiteId);
            		 $('#hdnPriority').val(PriorityId);
            		 $('#hdnSubject').val(Errormessage);
            		 $('#hdnEquipment').val(EquipmentID);
            		 $('#hdnTransaction').val(TransactionID);
            		 
            		 $('#hdnType').val('Operation');
            		 
            		var check=  $('#hdnSite').val();
            		//console.log(check);
            		var check1= $('#hdnPriority').val();
            		
            		var check2= $('#hdnSubject').val();
            		///console.log(check1);
            		console.log(check2);
            		 // alert(SiteId); alert(PriorityId);
            		 // PriorityId = $('#hdnPriority').text();
		 });
        	
        	//var dtTable = $('#example').DataTable()  ;

        	
        	$(document).on('click','.evtCol',function(){
        		debugger;
        		
        	})
        	
        	$(document).on('click','.evtCol111111', function()
        		 	{
        		debugger;
        		     
        		     var SiteName = $(this).closest("tr").find('td:eq(5)').text();        		
            		 var SiteId = $(this).closest("tr").find('td:eq(9)').text();
            		 var PriorityId = $(this).closest("tr").find('td:eq(10)').text();
            		 var Errormessage = $(this).closest("tr").find('td:eq(1)').text();
            		 var EquipmentID = $(this).closest("tr").find('td:eq(11)').text();
            		 var TransactionID = $(this).closest("tr").find('td:eq(12)').text();
            	
            		 //var SiteId = $( "table td:nth-child(10)").html();
            		 //var PriorityId = $( "table td:nth-child(11)").html();
            		
            		 //var PriorityId = $(this).closest("tr").find('td:eq(10)').text();
            		
            		 var Priority = $(this).closest("tr").find('td:eq(8)').text();
            		
            		 $('#txtSiteName').html(SiteName);
            		 $('#txtPriority').html(Priority);  
            		 $('#txtEventSubject').html(Errormessage);
            		 $('#txtEquipmentID').html(EquipmentID);
            		 $('#txtEventTransaction').html(TransactionID);
            		
            		 $('#hdnSite').val(SiteId);
            		 $('#hdnPriority').val(PriorityId);
            		 $('#hdnSubject').val(Errormessage);
            		 $('#hdnEquipment').val(EquipmentID);
            		 $('#hdnTransaction').val(TransactionID);
            		 
            		 
            		var check=  $('#hdnSite').val();
            		//console.log(check);
            		var check1= $('#hdnPriority').val();
            		
            		var check2= $('#hdnSubject').val();
            		///console.log(check1);
            		console.log(check2);
            		 // alert(SiteId); alert(PriorityId);
            		 // PriorityId = $('#hdnPriority').text();
        		 });
        	
        	
        	  $('#ddlEquipmentList').dropdown('clear');
        	
        	
        		 
        			  
         		
         			 
          		
        		 
        	  });
        		  
        	  
        	  $('#btnClear').click(function(){
        		  
        		
        		  $('#ddlSiteList').dropdown('clear');
        		  $('#ddlEquipmentList').dropdown('clear');
        		  $('#txtFromDate').val("");
        		  $('#txtToDate').val("");
        		  
        	
        	  });
        		  
        	
        	
        	
        	
            // $('#ddlEquipmentList option:selected').val($('#hdnequipmentname').val());
          //   $("#ddlEquipmentList option[value=" + $('#hdnequipmentname').val() +"]").attr("selected","selected");
            
        	var sCount = document.getElementById("ddlSiteList").length;
        	if(sCount==2)        		
        		{
        			$("#ddlSiteList").prop("selectedIndex", 1);        			
        		}
        	
        	
        	/*  $('th:nth-child(10)').hide();
   		 	 $('th:nth-child(11)').hide();
        	 $('td:nth-child(10)').hide();
    		 $('td:nth-child(11)').hide();  
    		 $('td:nth-child(12)').hide();
    		 $('td:nth-child(13)').hide();  
    		 $('th:nth-child(12)').hide();
   		 	 $('th:nth-child(13)').hide();  */
    		 $('#hdnType').val('Operation');
    		// alert($('#hdnType').val('Operation'));
    		
        	
        	$('#evt').click(function(){
        		/*  var id = $('#example').closest("tr").find('td:eq(5)').text();
        		 alert(id); */
        		//alert($('#tblSiteName').text());
        		/* var SiteName = $('#tblSiteName').text();
            	var evtSiteName = $('#txtSiteName').text();
            	evtSiteName = SiteName; */
        	})
        	 //$('#example').DataTable();
        	/*  $('#example').DataTable( { "order": [[ 0, "desc" ]]} ); */
        	 $.fn.dataTable.moment('DD-MM-YYYY HH:mm');
        	
        	 //$('#example').DataTable().columns([9]).visible(false);
        	 
        	
        	 
        	 $('#example1').DataTable(); 
        	
        	
        	 $("#txtFromDate").datepicker({                  	
              	dateFormat:'dd/mm/yy',
              	minDate: new Date(2018, 01 -1, 01),
              	maxDate: new Date(),
              	maxDate: "0",
                     onSelect: function(selected) {
                     // $("#txtToDate").datepicker("option","minDate", selected)                 
                     }
                 });
        	 
        	/*  $("#txtFromDate").mouseleave(function(){
        		    var SelectDate=$('#txtFromDate').val();
        		    debugger;
        		    var selectedDate = new Date(SelectDate);
        		   // var mydate =new Date('31-12-2017');
        		    
        		    
        		    selectedDate.getDate();
        		    selectedDate.getDay();
        		    selectedDate.getFullYear();
        		    
        		    /* mydate.getDate();
        		    mydate.getDay();
        		    mydate.getFullYear();
        		     *
        		       if(selectedDate.getFullYear() < parseInt('2018'))
        		       {
        		           //alert("greater");
        		    	   $('#txtFromDate').val('');
        		    	   console.log("inside")
        		       }
        		       else
        		       {
        		    	  
        		           //alert("smaller")
        		       }
        		       
        		      /*  if (CurrentDate > SelectedDate) {
        		    	   //alert('Future');
        		    	   $('#txtFromDate').val('');
        		       }
        		       else{
        		    	  // alert('Past');
        		       } 
        		    }); */
        	 
        	 
        	/*  $('#txtFromDate').blur(function(){
        	  		var SelectedDate=$('#txtFromDate').val();
        	  		 var d = new Date();
        			    var month = d.getMonth()+1;
        			    var day = d.getDate();
        			    var CurrentDate = 
        			        ((''+day).length<2 ? '0' : '')  + day + '/' + ((''+month).length<2 ? '0' : '') + month + '/' +
        			         d.getFullYear();
        			        /* alert(SelectedDate);
        			        alert(CurrentDate); 	       
        			       if (CurrentDate > SelectedDate) {
        			    	  	//alert('Future');
        			    	   $('#txtFromDate').val('');
        			       }
        			       else{
        			    	  // alert('Past');
        			       }
        	      	}) */
        	 
        	 
 		      	
              $("#txtToDate").datepicker({              	
              	dateFormat:'dd/mm/yy',
              	minDate: new Date(2018, 01 -1, 01),
              	maxDate: new Date(),
              	maxDate: "0",
              	onSelect: function(selected) {                                
                  //$("#txtFromDate").datepicker("option","maxDate", selected)      
               }                                
        	 }); 
        	
        	
        	 /* $("#txtToDate").mouseleave(function(){
     		    var SelectedDate=$('#txtToDate').val();
     		    var d = new Date();
     		    var month = d.getMonth()+1;
     		    var day = d.getDate();
     		    var CurrentDate = 
     		        ((''+day).length<2 ? '0' : '')  + day + '/' + ((''+month).length<2 ? '0' : '') + month + '/' +
     		         d.getFullYear();	    
     		       /* alert(SelectedDate);
     		       alert(CurrentDate); 
     		       
     		       if (CurrentDate > SelectedDate) {
     		    	   //alert('Future');
     		    	   $('#txtToDate').val('');
     		       }
     		       else{
     		    	  // alert('Past');
     		       }
     		    }); */
     	 
     	 
     	/*  $('#txtToDate').blur(function(){
     	  		var SelectedDate=$('#txtToDate').val();
     	  		 var d = new Date();
     			    var month = d.getMonth()+1;
     			    var day = d.getDate();
     			    var CurrentDate = 
     			        ((''+day).length<2 ? '0' : '')  + day + '/' + ((''+month).length<2 ? '0' : '') + month + '/' +
     			         d.getFullYear();
     			        /* alert(SelectedDate);
     			        alert(CurrentDate); 	       
     			       if (CurrentDate > SelectedDate) {
     			    	  	//alert('Future');
     			    	   $('#txtToDate').val('');
     			       }
     			       else{
     			    	  // alert('Past');
     			       }
     	      	}) */
       	   
       	 $("#searchSelect").change(function() {
             var value = $('#searchSelect option:selected').val();
             var uid =  $('#hdneampmuserid').val();
           redirectbysearch(value,uid);
        }); $('.dataTables_filter input[type="search"]').attr('placeholder','search');

        
        
        

            if($(window).width() < 767)
				{
				   $('.card').removeClass("slide-left");
				   $('.card').removeClass("slide-right");
				   $('.card').removeClass("slide-top");
				   $('.card').removeClass("slide-bottom");
				}
            
          
            $('.close').click(function(){
                $('#tktCreation').hide();
                $('.clear').click();
                $('.category').dropdown();
                $('.SiteNames').dropdown();
            });
            
            $('#evt_close').click(function(){
                $('#evntCreation').hide();
               
                $('.evtddlPriority').dropdown();
                $('.txtEvtdescription').val();
            });
            
            
			$('.clear').click(function(){
			    $('.search').val("");
			});

			$('#ddlType').change(function(){              
                $('.category').dropdown('clear')
               });

			
			
			$('body').click(function(){
            	$('#builder').removeClass('open'); 
         	 });
			
			

      </script>



<script type='text/javascript'>//<![CDATA[
         $(window).load(function(){
        	 $('.ui.dropdown').dropdown({forceSelection:false});
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
         
         
         $('.ui.dropdown').dropdown({
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: true, 
             showOnFocus: true,
             on: "click" 
           });

		$('.ui.dropdown.oveallsearch').dropdown({
             onChange: function (value, text, $selectedItem) {
                
                var uid =  $('#hdneampmuserid').val();
                redirectbysearchvalue(value,uid);
             },
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: false, 
             showOnFocus: true,
             on: "click" 
           });
		
		
		
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );//]]> 

      //Tkt Creation Validation //

            var validation  = {
                    txtSubject: {
                               identifier: 'txtSubject',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               },
                               {
                                   type: 'maxLength[50]',
                                   prompt: 'Please enter a value'
                                 }]
                             },
                              ddlSite: {
                               identifier: 'ddlSite',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                             },
                            ddlType: {
                                  identifier: 'ddlType',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                            ddlCategory: {
                               identifier: 'ddlCategory',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                             ddlPriority: {
                               identifier: 'ddlPriority',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please select a dropdown value'
                               }]
                             },
                             description: {
                               identifier: 'description',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please Enter Description'
                               }]
                             }
             };
               var settings = {
                 onFailure:function(){
                	 //alert('Tkt Fail');
                     return false;
                   }, 
                 onSuccess:function(){   
                	 //alert('Tkt Sucess');
                   $('#btnCreate').hide();
                   $('#btnCreateDummy').show()
                  
                   //$('#btnReset').attr("disabled", "disabled");          
                   }};           
               $('.ui.form.tkt-validation-form').form(validation,settings);
               
   // Event Tkt Validations //
               var validation  = {
            		   evtddlPriority: {
                                  identifier: 'evtddlPriority',
                                  rules: [{
                                    type: 'empty',
                                    prompt: 'Please enter a value'
                                  }]
                                },
                                txtEvtdescription: {
                                  identifier: 'txtEvtdescription',
                                  rules: [{
                                    type: 'empty',
                                    prompt: 'Please enter a value'
                                  }]
                                },
                               
                };
                  var settings = {
                    onFailure:function(){
                    	
                        return false;
                      }, 
                    onSuccess:function(){    
                    	
                    	 $('#btnEvtCreate').hide();
                         $('#btnEvtDummy').show()
                      
                     
                      //$('#btnReset').attr("disabled", "disabled");          
                      }};           
                  $('.ui.form.validation-form').form(validation,settings);
               
         })
         
      </script>
<style type="text/css">
.dropdown-menu-right {
	left: -70px !important;
}

.table-responsive {
	overflow-y: hidden;
}

.evttxtColor {
	color: #705b88;
}

.btnDeleteRecord {
	cursor: pointer;
}

.ui.search.dropdown .menu {
	max-height: 12.02857143rem;
}

#example_info, #example_paginate {
	padding-top: 20px;
}

div.dataTables_wrapper div.dataTables_length select {
	width: 50px;
	display: inline-block;
	margin-left: 3px;
	margin-right: 4px;
}

.ui.form textarea:not ([rows] ) {
	height: 4em;
	min-height: 4em;
	max-height: 24em;
}

.ui.icon.input>i.icon:before, .ui.icon.input>i.icon:after {
	left: 0;
	position: absolute;
	text-align: center;
	top: 36%;
	width: 100%;
}

.evtDropdown {
	/* min-height: 2.7142em !important; 
font-size:13px !important;
padding: 0.78571429em 2.1em 0.78571429em 1em !important;  */
	
}

.input-sm, .form-horizontal .form-group-sm .form-control {
	font-size: 13px;
	min-height: 25px;
	height: 32px;
	padding: 8px 9px;
}

#example td a {
	color: #337ab7;
	font-weight: bold;
}

.dt-button-background {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 0 !important;
}

.form-control {
	font-size: 12px;
}

.dropevent {
	min-width: 100px;
	margin-left: -35px;
}

.dropevent li a {
	color: #000 !important;
	font-weight: normal !important;
}

.tbh-width {
	width: 150px;
	padding-right: 30px !important;
}

.dt-buttons {
	margin-left: 50px;
}

.buttons-collection {
	height: 28px;
	padding: 3px;
	/*  z-index:9999; */
}

.buttons-excel {
	background: rgba(36, 95, 68, 0.87);
	color: #FFF;
}

.buttons-pdf {
	background: rgb(98, 165, 96);
	color: #FFF;
}

.dataTables_filter {
	float: right;
}

.dataTables_length {
	float: left;
}

.ui.selection.dropdown {
	width: 100%;
	margin-top: 0.1em;
	/*  height: 2.3em; */
}

.ui.selection.dropdown .menu {
	width: 100%;
	white-space: normal !important;
}

.ui.selection.dropdown .menu .item:first-child {
	border-top: 1px solid #ddd !important;
	min-height: 2.8em;
}

.ui.selection.dropdown .text {
	text-overflow: ellipsis;
	white-space: nowrap;
	width: 90%;
}

.oveallSearch .text {
	/* color: #eae9e9 !important; */
	color: #a49c9c !important;
}

.ui.selection.dropdown .icon {
	text-align: right;
	margin-left: 7px !important;
}
</style>

</head>
<body class="fixed-header">

	<div class="theme-loader">
		<div class="loader-track">
			<div class="loader-bar"></div>
		</div>
	</div>

	<input type="hidden" value="${access.userID}" id="hdneampmuserid">

	<input type="hidden" value="${access.mySiteFilter}"
		id="hdnmysitefilter">

	<input type="hidden" value="${access.mySiteID}" id="hdnmysiteid">

	<input type="hidden" value="${access.myCustomerFilter}"
		id="hdnmycustomerfilter">

	<input type="hidden" value="${access.myCustomerID}"
		id="hdnmycustomerid">

	<input type="hidden" value="${classtate}" id="hdnstate">

	<jsp:include page="slider.jsp"/>


	


	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					<a href="./dashboard"><img src="resources/img/logo01.png"
						alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx tktSearch"
					id="SearcSElect">
					<div class="ui  search selection  dropdown  width oveallsearch">
						<input id="myInput" name="tags" type="text">

						<div class="default text">search</div>

						<div id="myDropdown" class="menu">

							<jsp:include page="searchselect.jsp" />	


						</div>
					</div>



				</div>

				<div class="dropdown pull-right hidden-md-down user-log">
					<button class="profile-dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 circular inline"> <img
							src="resources/img/user_01.png" alt
							data-src="resources/img/user_01.png"
							data-src-retina="resources/img/user_01.png" width="32"
							height="32">
						</span>

						<c:if test="${not empty access}">
							<p class="user-login">${access.userName}</p>
						</c:if>
					</button>
					<div class="dropdown-menu dropdown-menu-right profile-dropdown"
						role="menu">
						<a class="dropdown-item disablecolor"><i
							class="pg-settings_small"></i> Settings</a> <a
							class="dropdown-item disablecolor"><i class="pg-outdent"></i>
							Feedback</a> <a class="dropdown-item disablecolor"><i
							class="pg-signals"></i> Help</a> <a href="logout1"
							class="dropdown-item color logout"><i class="pg-power"></i>
							Logout</a>
					</div>
				</div>

				
					<div class="newticketpopup hidden">
						<span data-toggle="modal" data-target="#tktCreation"
							data-backdrop="static" data-keyboard="false"><p
								class="center m-t-5 tkts">
								<img class="m-t-5" src="resources/img/tkt.png">
							<p class="create-tkts">New Ticket</p>
							</p></span>

					</div>
				







			</div>
		</div>
		<div class="page-content-wrapper" id="QuickLinkWrapper1">

			<div class="content ">

				<div class="container-fixed-lg">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="./dashboard"><i
									class="fa fa-home mt-7"></i></a></li>
							<li class="breadcrumb-item"><a>Visualization</a></li>


							<c:if test="${not empty access}">
								<c:choose>
									<c:when test="${access.customerListView == 'visible'}">
										<li class="breadcrumb-item"><a
											href="./customerview${access.myCustomerID}">${customername}</a></li>
									</c:when>
									<c:otherwise>
										<li class="breadcrumb-item"><a href="./dashboard">${customername}</a></li>
									</c:otherwise>
								</c:choose>

							</c:if>




		<li class="breadcrumb-item"><a	href="./siteview${access.mySiteID}">${sitename}</a></li>

							<li class="breadcrumb-item active">${equipmentname}</li>
						</ol>
					</div>



				<div id="newconfiguration" class="">
					<div class="card card-transparent mb-0">
						<!-- Table Start-->
						<div class="padd-3 sortable" id="equipmentdata">
							<%-- <div class="row">
								<div class="col-md-12">
									<div class="card card-default bg-default" data-pages="card">
										<div class="col-md-1">
											<div class="input-group">
															<form:input id="txtDummyFromDate" type="text"
																placeholder="From Date" class="date-picker form-control" autocomplete="off"
																name="fromDate" path="fromDate" required="required" />
															<label for="txtFromDate" class="input-group-addon btn"><span
																class="glyphicon glyphicon-calendar"></span> </label>
														</div>
										</div>
									</div>
								</div>
							</div> --%>
							<div class="row">
								<!-- <div class="card-header">
                                    <div class="card-title tbl-head ml-15">Equipment Curing Data</div>
                                 </div> -->
								<!-- <div class="col-md-12 col-lg-12">
                                 		<div class="card card-default bg-default" data-pages="card">
                                 			<div class="col-md-1">
                                 				<p>Hello</p>
                                 			</div>
                                 		</div>
                                 	</div> -->
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
									<div class="card card-default bg-default" data-pages="card" style="margin-bottom: 3px;">
										<!-- <div class="card-header">
											<div class="card-title tbl-head ml-15">
												<i class="fa fa-filter" aria-hidden="true"></i> Event
												Filters
											</div>
										</div> -->
										<c:choose>
											<c:when test="${access.filterFlag == 1}">
												<c:url var="viewAction"
													value="/siteeventdetails${access.mySiteID}&ref=${access.userID}"></c:url>
											</c:when>

											<c:otherwise>
												<c:url var="viewAction" value="/eventdetails"></c:url>
											</c:otherwise>
										</c:choose>
											<p class="m-t-10 m-l-5 title-head"><b>Equipment Details:</b></p>
										<div class="row">
										<div class="col-md-12  padd-5" style="margin-bottom:0px;">
											<div class="col-md-2 col-xs-6">
												<label class="fieldslbl m-t-5">Equipment Code</label>
											</div>
											<div class="col-md-2 col-xs-6">
												<span class="fieldslbl">:</span><label class="fieldslbl m-t-5 m-l-10" id="EquipmentCode"></label>
											</div>
											<div class="col-md-2 col-xs-6">
												<label class="fieldslbl m-t-5">Customer Reference</label>
											</div>
											<div class="col-md-2 col-xs-6">
												<span class="fieldslbl">:</span><label class="fieldslbl m-t-5 m-l-10" id="CustomerReference"></label>
											</div>
											<div class="col-md-2 col-xs-6">
												<label class="fieldslbl m-t-5" >Serial No</label>
											</div>
											<div class="col-md-2 col-xs-6">
												<span class="fields">:</span>
														<label class="fieldslbl m-t-5 m-l-10" id="SerialNumber"></label>
											</div>
										</div>
										</div>
										
										
										<div class="row">
										<div class="col-md-12  padd-5" style="margin-bottom:0px;">
											<div class="col-md-2 col-xs-6">
												<label class="fieldslbl m-t-5">Capacity (kW)</label>
											</div>
											<div class="col-md-2 col-xs-6">
												<span class="fieldslbl">:</span><label class="fieldslbl m-t-5 m-l-10" id="Capacity"></label>
											</div>
											<div class="col-md-2 col-xs-6">
												<label class="fieldslbl m-t-5">Equipment Category</label>
											</div>
											<div class="col-md-2 col-xs-6">
												<span class="fieldslbl">:</span><label class="fieldslbl m-t-5 m-l-10" id="Category"></label>
											</div>
											<div class="col-md-2 col-xs-6">
												<label class="fieldslbl m-t-5">Equipment Type</label>
											</div>
											<div class="col-md-2 col-xs-6">
											<p style="margin-bottom: 0px;
    float: left;"><span class="fieldslbl" >:</span>
											
												<p class=" p-l-15">
															
															
																	<span class="fieldslbl m-t-5" id="EquipmentType"></span>
														</p>
												
													
											</div>
										</div>
										</div>
										<div class="row">
										<div class="col-md-12 padd-5" style="margin-bottom:0px;">
											<div class="col-md-2 col-xs-6">
												<label class="fieldslbl">No. Of Components</label>
											</div>
											<div class="col-md-2 col-xs-6">
												<span class="fieldslbl">:</span><label class="fieldslbl m-t-5 m-l-10" id="Components"></label>
											</div>
											<div class="col-md-2 col-xs-6">
												<label class="fieldslbl">Disconnect Type</label>
											</div>
											<div class="col-md-2 col-xs-6">
												<span class="fieldslbl">:</span><label class="fieldslbl m-t-5 m-l-10" id="Disconnecttype"></label>
											</div>
											<div class="col-md-2 col-xs-6">
												<label class="fieldslbl">Disconnect Rating</label>
											</div>
											<div class="col-md-2 col-xs-6">
												<span class="fieldslbl">:</span>
														<label class="fieldslbl m-t-5 m-l-10" id="Disconnectrating"></label>
											</div>
										</div>
										
										</div>
										
										
									</div>


								</div>


							</div>
							
							<!-- Analysis Chart Start -->
							<div class="sortable" id="equipmentdata">
							<div class="row">
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails"
									style="">
									<div class="card card-default bg-default" data-pages="card" style="margin-bottom: 3px;">										
										<div class="padd-5 ">
											<div class="col-md-12 padd-0">
												<div class="col-md-6 padd-0">
												<p class=" title-head"><b>Analytics</b></p>
													<div id="analyticsChart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
												</div>
												<div class="col-md-6 padd-0">
												
											<p class="title-head"><b>Equipment History:</b></p>
										
													<div class="row">
														<div class="col-md-4 col-xs-6">															
															<label class="fieldslbl m-t-5">Primary Equipment</label>
														</div>
														<div class="col-md-6 col-xs-6">															
															<span class="fieldslbl">:</span>
															<label class="fieldslbl m-t-5 m-l-10" id="Equipmentselection"></label>															
														</div>
													</div>
													<div class="row">
														<div class="col-md-4 col-xs-6">
															<label class="fieldslbl m-t-5">Installation Date</label>
														</div>
														<div class="col-md-6 col-xs-6">														
															<p><span class="fieldslbl">:</span>
															<span class="fieldslbl m-t-5 m-l-10" id="InstalledDate"></span>	</p>														
														</div>
													</div>
													<div class="row">
														<div class="col-md-4 col-xs-6">														
																<label class="fieldslbl m-t-5">Warranty Expiry Date</label>
														</div>
														<div class="col-md-6 col-xs-6">
														<p>
															
															<span class="fieldslbl">:</span>
																<span class="fieldslbl m-t-5 m-l-10" id="WarrantyDate"></span>
														</p>
														</div>
													</div>
													<div class="row">
														<div class="col-md-4 col-xs-6">
																<label class="fieldslbl m-t-5">Description</label>
														</div>
														<div class="col-md-6 col-xs-6">
															<p>
																
															<span class="fieldslbl">:</span>
																<span class="fieldslbl  m-l-10" id="Description"></span>
															</p>
														
														</div>
													</div>
													<div class="row">
														<div class="col-md-4 col-xs-6">
																<label class="fieldslbl m-t-5">Remarks</label>
														</div>
														<div class="col-md-6 col-xs-6">
														<p>
															
															<span class="fieldslbl">:</span>
																<span class="fieldslbl  m-l-10" id="Remarks"></span>
														</p>
														</div>
													</div>
													<div class="row">
														<div class="col-md-4 col-xs-6">
															
																<label class="fieldslbl m-t-5">Replacement Count</label>
															</div>
														
														<div class="col-md-6 col-xs-6">
														<p>
															
															<span class="fieldslbl">:</span>
																<span class="fieldslbl  m-l-10"></span>
														</p>
														</div>
													</div>
													<div class="row">
														<div class="col-md-4 col-xs-6">
																<label class="fieldslbl m-t-5">Last Replacement Done on</label>
														</div>
														<div class="col-md-6 col-xs-6">
														<p>
															
															<span class="fieldslbl">:</span>
															<span class="fieldslbl  m-l-10"></span>
														</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--  Analysis Chart End -->
						<!--  Table Start  -->
						<!-- Table Start-->
						<div class="sortable" id="equipmentdata">						
							<div class="row c-excel-plot-events">						
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
									<div class="card card-default bg-default" data-pages="card" style="margin-bottom: 3px;">	
										<div class="row ">
										<div class="col-md-12">
											<div class="col-md-6 col-xs-6">
											<a class="m-l-5 title-head" style="text-decoration:none !importatnt;float:left;margin-right:0px;"  href="./siteeventskpi${access.mySiteID}"><b>Events</b></a>	
			
												<!-- <p class="m-l-5 title-head"><b>Events</b></p> -->	
											</div>
											<div class="col-md-6 col-xs-6">
												 <%-- <a class="m-l-5 title-head" style="text-decoration:none !importatnt;float:right;margin-right:10px;"  href="./siteeventskpi${access.mySiteID}"><b>Events</b></a>	
											 --%></div> 
											</div>
										</div>								
										<div class="padd-5 table-responsive">
											<table id="example"
												class="table table-striped table-bordered" width="100%"
												cellspacing="0">
												<thead>
													<tr>
														<th style="width: 9%;">Error Code</th>
														<th style="width: 9%;">Error Message</th>
														<th style="width: 9%;">Event Time</th>
														<th style="width: 9%;">Last Updated</th>
														<th style="width: 9%;">Occurrence</th>
														<th style="width: 9%;">Site Name</th>
														<th style="width: 9%;">Equipment Name</th>
														<th style="width: 9%;">Equipment Type</th>
														<th style="width: 9%;">Priority</th>
														<th style="width: 9%; display: none;">Site ID</th>
														<th style="width: 9%; display: none;">Priority ID</th>
														<th style="width: 9%; display: none;">Equipment ID</th>
														<th style="width: 9%; display: none;">Transaction ID</th>
														<th style="width: 9%;">Customer Naming</th>
														<th style="width: 9%;">Capacity (kW)</th>
														<c:if test="${access.customerListView == 'visible' }">
															<th style="width: 9%;">Action</th>
														</c:if>
													</tr>
												</thead>
												<tbody>											
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Table End-->
						<!--  Table Start -->
<div class="sortable" id="equipmentdata">
							<div class="row c-excel-plot-tickets">
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails"
									style="height: 220px;">
									<div class="card card-default bg-default" data-pages="card">	
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6 col-xs-6">
											<a class="m-l-5 title-head" style="text-decoration:none !importatnt;float:left;margin-right:0px;"  href="./siteticketskpi${access.mySiteID}"><b>Tickets</b></a>
											
												<!-- <p class="m-l-5 title-head"><b>Tickets</b></p>	 -->
											</div>
											 <div class="col-md-6 col-xs-6">
											<%-- <a class="m-l-5 title-head" style="text-decoration:none !importatnt;float:right;margin-right:10px;"  href="./siteticketskpi${access.mySiteID}"><b>Tickets</b></a>
											 --%>	<!-- <p class="m-l-5 title-head" style="text-decoration:none !importatnt;float:right;margin-right:10px;"><b>Tickets</b></p>	 -->
											</div> 
											</div>
										</div>									
										<div class="padd-5 table-responsive">
											<table id="example1"
												class="table table-striped table-bordered" width="100%"
												cellspacing="0">
												<thead>
													<tr>
                                                 	<c:if test="${access.customerListView == 'visible'}">					
                                                    <th style="width:10%;">Ticket No</th>
                                                   </c:if>
                                                    <th style="width:10%;">Site Name</th>
                                                    <th style="width:7%;">Priority</th>
                                                    <th style="width:16%;">Created Time</th>
                                                    <th style="width:12%;">Category</th>
                                                    <th style="width:4%;">Subject</th>
                                                    <th style="width:13%;">Assign To</th>
                                                     <th style="width:15%;">Created By</th>
                                                 
                                                    <th style="width:13%;">Scheduled On</th>
                                                    <th style="width:10%;">State</th>
                                                    <th style="width:10%;">Status</th>
                                                </tr>
												</thead>
												<tbody>
													

												</tbody>
											</table>


										</div>
									</div>
								</div>


							</div>

						</div>

						<!-- Table End-->
						<!--  Table End -->
						<!-- Table End -->
						</div>


						<!-- Table End-->
						
						
						


						<!-- Table End-->
					</div>
				</div>


			</div>

			<div class="container-fixed-lg footer mb-0">
				<div class="container-fluid copyright sm-text-center">
					<p class="small no-margin pull-left sm-pull-reset">
						<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
							CLEAN ENERGY</span>. <span class="hint-text">All rights reserved.
						</span>
					</p>
					<p class="small no-margin pull-rhs sm-pull-reset">
						<span class="hint-text">Powered by</span> <span>MESTECH
							SERVICES PVT LTD</span>.
					</p>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Side Bar Content Start-->




	<div class="quickview-wrapper  builder hidden-sm hidden-xs"
		id="builder">
		<div class="p-l-10 p-r-10 ">
			<a class="builder-close quickview-toggle pg-close"></a> <a
				class="builder-toggle" data-toggle="quickview"
				data-toggle-element="#builder"><img
				src="resources/img/ImageResize_06.png"></a>
			<ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary"
				id="builderTabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#tabLayouts" role="tab"
					aria-controls="home"><span>Quick Link</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active " id="tabLayouts" role="tabcard">
					<div class="scrollable">
						<div class="p-r-50">
							<div class="list">
								<ul>
									<!-- 	<li><a href="#QuickLinkWrapper1">Ticket Filters</a></li> -->
									<li><a href="#QuickLinkWrapper2">List Of Events</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>






	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min3.js"
		type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>


	<script type="text/javascript" src="resources/js/moment.min.js"></script>
	<script type="text/javascript" src="resources/js/datetime-moment.js"></script>

<script src="resources/js/dataTables.buttons.min.js"></script>

<script src="resources/js/buttons.html5.min.js"></script>
<script src="resources/js/jszip.min.js"></script>



	<div id="gotop"></div>

	<!-- Share Popup End !-->
	<!-- Modal -->
	<div id="tktCreation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header Popup-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="resources/img/close.png">
					</button>
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<p class="modal-title tkt-title">
						<img src="resources/img/tkt.png"><span class="tkt-title">New
							Ticket</span>
					</p>
				</div>
				<div class="modal-body">
					<c:url var="addAction" value="/addnewticket"></c:url>
					<div class="col-md-12">
						<p class="tkt-mandat">
							<span class="errormess">*</span> All fields are mandatory
						</p>
					</div>
					<form:form action="${addAction}" modelAttribute="ticketcreation"
						class="ui form tkt-validation-form">

						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width SiteNames"
									id="ddlSite" name="ddlSite" path="siteID" onchange="getEquipmentAgainstSite()">
									<form:option value="" tabindex="0">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>
						
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                                                <div class="col-md-4 col-xs-12">
                                                       <label class="fields required m-t-10">Equipment</label>
                                                </div>

                                              <div class="col-md-8 col-xs-12">
                                                       <form:select
                                                              class="field ui fluid search selection dropdown width equipments"
                                                              id="ddlEquipment" path="equipmentID" name="ddlEquipment">
                                                              <option value="">Select</option>
                                                       </form:select>
                                                </div>
                          </div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Type</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width evtDropdown"
									id="ddlType" name="ddlType" path="ticketType">
									<form:option value="">Select</form:option>
									<form:option value="Operation">Operation</form:option>
									<form:option value="Maintenance">Maintenance</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Category</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width category evtDropdown"
									id="ddlCategory" name="ddlCategory" path="ticketCategory">
									<form:option value="">Select </form:option>
									<form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
									<form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
									<form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
									<form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
									<form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
									<form:option data-value="Operation"
										value="Equipment Replacement">Equipment Replacement</form:option>
									<form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
									<form:option data-value="Operation" value="String Down">String Down</form:option>
									<form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
									<form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
									<form:option data-value="Operation" value="">Select</form:option>
									<form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
									<form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="DataLogger Cleaning">DataLogger Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="String Current Measurement">String Current Measurement</form:option>
									<form:option data-value="Maintenance"
										value="Preventive Maintenance">Preventive Maintenance</form:option>
									<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
									<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
									<form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
									<form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
									<form:option data-value="Maintenance" value="">Select</form:option>
								</form:select>
							</div>

						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Subject</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<form:input placeholder="Subject" autocomplete="off"
										name="Subject" type="text" id="txtSubject" path="ticketDetail"
										maxLength="50" />
								</div>
							</div>
						</div>

						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Priority</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width evtDropdown"
									id="ddlPriority" name="user" path="priority">
									<form:option value="">Select </form:option>
									<form:option data-value="3" value="3">High</form:option>
									<form:option data-value="2" value="2">Medium</form:option>
									<form:option data-value="1" value="1">Low</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Description</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<textarea id="txtTktdescription" autocomplete="off"
										name="description" style="resize: none;"
										placeholder="Ticket Description" maxLength="120"></textarea>
								</div>

							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
							<div class="btn btn-success  submit" id="btnCreate">Create</div>
							<button class="btn btn-success" type="button" id="btnCreateDummy"
								tabindex="7" style="display: none;">Create</button>
							<div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
						</div>
						<!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
						<!-- <div class="ui error message"></div> -->
					</form:form>
					<!-- <div class="ui error message"></div> -->

				</div>
			</div>
		</div>
	</div>
	<!-- Address Popup End !-->
	<!-- Modal -->
	<div id="evntCreation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header Popup-header">
					<button type="button" class="close" id="evt_close"
						data-dismiss="modal">
						<img src="resources/img/close.png">
					</button>
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<p class="modal-title tkt-title">
						<img src="resources/img/tkt.png"><span class="tkt-title">Event
							Ticket</span>
					</p>
				</div>
				<div class="modal-body">
					<c:url var="addAction" value="/addnewticket"></c:url>
					<div class="col-md-12">
						<p class="tkt-mandat">
							<span class="errormess">*</span> All fields are mandatory
						</p>
					</div>
					<form:form action="${addAction}" modelAttribute="ticketcreation"
						class="ui form validation-form">

						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-4">
								<p class="fields  m-t-10">Site Name</p>
								<form:input type="hidden" id="hdnSite" path="siteID" />
							</div>
							<div class="col-md-8 col-xs-8">
								<p class="fields  m-t-10 evttxtColor" id="txtSiteName"></p>
							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-4">
								<p class="fields  m-t-10 ">Type</p>
								<form:input type="hidden" id="hdnType" value="Operation"
									path="ticketType" />
							</div>
							<div class="col-md-8 col-xs-8">

								<p class="fields  m-t-10 evttxtColor" id="txtTypeName">Operation</p>
							</div>
						</div>
						<%-- <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                     <div class="col-md-4 col-xs-4">
                     <p class="fields  m-t-10">Priority</p><form:input type="hidden" id="hdnPriority" value="Low" path="priority"/>
                  </div>
                  <div class="col-md-8 col-xs-8">
                  	<p class="fields  m-t-10 evttxtColor" id="txtPriority" ></p>
                  </div>
               </div>  --%>
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-4">
								<p class="fields  m-t-10">Subject</p>
								<form:input type="hidden" id="hdnSubject" path="ticketDetail" />
							</div>
							<div class="col-md-8 col-xs-8">
								<p class="fields  m-t-10 evttxtColor" id="txtEventSubject"></p>
							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15"
							style="display: none;">
							<div class="col-md-4 col-xs-12">
								<p class="fields  m-t-10">Equipment Name</p>
								<form:input type="hidden" id="hdnEquipment" path="equipmentID" />
							</div>
							<div class="col-md-8 col-xs-12">
								<p class="fields  m-t-10 evttxtColor" id="txtEquipmentID"></p>
							</div>
						</div>

						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Priority</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width evtDropdown"
									id="evtddlPriority" name="user" path="priority">
									<form:option value="">Select </form:option>
									<form:option data-value="3" value="3">High</form:option>
									<form:option data-value="2" value="2">Medium</form:option>
									<form:option data-value="1" value="1">Low</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Description</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<textarea id="txtEvtdescription" autocomplete="off"
										name="description" style="resize: none;"
										placeholder="Ticket Description" maxLength="120"></textarea>
								</div>

							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
							<div class="btn btn-success  submit" id="btnEvtCreate">Create</div>
							<button class="btn btn-success" type="button" id="btnEvtDummy"
								tabindex="7" style="display: none;">Create</button>
							<div class="btn btn-primary clear m-l-15">Reset</div>
						</div>
						<!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
						<!-- <div class="ui error message"></div> -->
					</form:form>
					<!-- <div class="ui error message"></div> -->

				</div>
			</div>
		</div>
	</div>
	<!-- Address Popup End !-->
	<script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
	<script>
         $(document).ready(function() {
            $(window).fadeThis({
               speed: 500,
            });
            

         
         });
      </script>

	<script type="text/javascript">
      $(document).ready(function(){
var validation  = {
	
		ddlEquipmentList: {
          identifier: 'ddlEquipmentList',
            rules: [
              {
                type   : 'empty',
                prompt : 'Enter Equipment List'
              }
            ] 
        }, 
        ddlSiteList: {
                identifier: 'ddlSiteList',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Enter Site'
                    }
                  ] 
              },
             
                };
var settings = {
  onFailure:function(){ 
   // alert('fail');
      return false;
    }, 
  onSuccess:function(){    
    //alert('Success');
    
    $( "#ddlSiteList" ).val();
var selectedid = $( "#ddlSiteList option:selected" ).val();
//alert(selectedid);

    return false; 
    }};
  
$('.ui.form.noremal').form(validation,settings);
      });
    </script>



</body>

<script type="text/javascript">
function getEquipmentAgainstSite() {
    var siteId = $('#ddlSite').val();
   
    $.ajax({
          type : 'GET',
          url : './equipmentsBysites?siteId=' + siteId,
          success : function(msg) {
       	   
       	   var EquipmentList = msg.equipmentlist;
                 
                        for (i = 0; i < EquipmentList.length; i++) {
                               $('#ddlEquipment').append(
                                             "<option value="+EquipmentList[i].equipmentId+">" + EquipmentList[i].customerNaming
                                                          + "</option>");
                        }

          },
          error : function(msg) {
                 console.log(msg);
          }
    });
}

       </script>
</html>

