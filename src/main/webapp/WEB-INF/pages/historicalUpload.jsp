<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet"
	rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/bootstrap.min3.3.37.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min3.css" type="text/css"
	rel="stylesheet">

<link rel="stylesheet" type="text/css"
	href="resources/css/jquery-ui.css">
<link href="resources/css/formValidation.min.css" rel="stylesheet"
	type="text/css">
<!--  <link href="resources/css/jquery-ui_cal.css" rel="stylesheet" type="text/css"> -->

<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>

<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script type="text/javascript" src="resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="resources/js/formValidation.min.js"></script>
<script type="text/javascript"
	src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.val.js"></script>


<script type="text/javascript" src="resources/js/toastr.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/toastr.css">
<link rel="stylesheet" type="text/css"
	href="http://codeseven.github.com/toastr/toastr.css">



<script type="text/javascript">
	$(document)
			.ready(
					function() {

						 var classstate = $('#hdnstate').val();

							$("#configurationid").addClass(classstate);
							$("#historicuploadconfigid").addClass(classstate);
						

						$('#ddlUserRoleStatus').dropdown('clear');

						$('#txtRoleName').val('');
						$('#txtRoleDescription').val('');

						toastr.options = {
							"debug" : false,
							"positionClass" : "toast-bottom-right",
							"progressBar" : true,
							"onclick" : null,
							"fadeIn" : 500,
							"fadeOut" : 100,
							"timeOut" : 1000,
							"extendedTimeOut" : 2000
						}

						$('#btnClear').click(function() {

							$('#hdnRoleId').val('Auto-Generation');

							$('#btnSubmit').text('Submit');
						});

						$('#example')
								.DataTable(
										{
											"oLanguage" : {
												"sEmptyTable" : "Select Site to view the Documents"
											},
											"lengthMenu" : [
													[ 15, 25, 50, -1 ],
													[ 15, 25, 50, "All" ] ]
										});

						$('#btnedit').click(function() {
							$('#btnSubmit').text('None');
						});

						/* 	 $('#addSite').submit(function(event) {
							       
							      var producer = $('#producer').val();
							      var model = $('#model').val();
							      var price = $('#price').val();
							      var json = { "producer" : producer, "model" : model, "price": price};
							       
							    $.ajax({
							        url: $("#newSmartphoneForm").attr( "action"),
							        data: JSON.stringify(json),
							        type: "POST",
							         
							        beforeSend: function(xhr) {
							            xhr.setRequestHeader("Accept", "application/json");
							            xhr.setRequestHeader("Content-Type", "application/json");
							        },
							        success: function(smartphone) {
							            var respContent = "";
							             
							            respContent += "<span class='success'>Smartphone was created: [";
							            respContent += smartphone.producer + " : ";
							            respContent += smartphone.model + " : " ;
							            respContent += smartphone.price + "]</span>";
							             
							            $("#sPhoneFromResponse").html(respContent);         
							        }
							    });
							      
							    event.preventDefault();
							  });
						 */

						var url = './userdetailslist';
						/*  $('#example').DataTable({ 
						 	"order": [[ 0, "desc" ]],
							 'processing': true,
						     'serverSide': true,
						     'ajax': {
						       type: 'POST',
						       'url': url                   
						     },
						 "columns": [
						     { "data": "siteCode" },
						     { "data": "siteTypeName" },
						     { "data": "SiteName" },
						     { "data": "customerName" },
						     { "data": "stateName" },
						     { "data": "countryName" },
						     {"data":"activeFlag"},
						     {"data":"activeFlag"},
						     {"data":"activeFlag"}
						 ]
						 }); */

						$("#txtFromDate").datepicker({
							dateFormat : "dd/mm/yy"
						});
						$("#txtToDate").datepicker({
							dateFormat : "dd/mm/yy"
						});
						$('.ui.dropdown').dropdown({
							forceSelection : false
						});

						$("#searchSelect").change(
								function() {
									var value = $(
											'#searchSelect option:selected')
											.val();
									var uid = $('#hdneampmuserid').val();
									redirectbysearch(value, uid);
								});
						$('.dataTables_filter input[type="search"]').attr(
								'placeholder', 'search');

						if ($(window).width() < 767) {
							$('.card').removeClass("slide-left");
							$('.card').removeClass("slide-right");
							$('.card').removeClass("slide-top");
							$('.card').removeClass("slide-bottom");
						}

						/*  $('.tkts').click(function(){
						 	
						 	 $("input").trigger("select");
						 }) */

						$('.clr').click(
								function() {

									$("#txtFromDate").val('');
									$("#txtToDate").val('');
									$(".text").empty();
									$(".text").addClass('default')
									$(".text").html("Select");
									$('#ddlSite option:selected').removeAttr(
											'selected');
									$('#ddlState option:selected').removeAttr(
											'selected');
									$('#ddlCate option:selected').removeAttr(
											'selected');
									$('#ddlPriority option:selected')
											.removeAttr('selected');
									window.location.href = './eventdetails';
									//location.reload();
								});

						$('#btnCreate').click(function() {
							if ($('#ddlCategory').val == "") {
								alert('Val Empty');
								$('.category ').addClass('error')
							}
						})

						$('.close').click(function() {
							$('#tktCreation').hide();
							$('.clear').click();
							$('.category').dropdown();
							$('.SiteNames').dropdown();
						});

						$('.clear').click(function() {
							$('.search').val("");
						});

						$('#ddlType').change(function() {
							$('.category').dropdown('clear')
						});

						$('body').click(function() {
							$('#builder').removeClass('open');
						});

					});
</script>

<script type='text/javascript'>
	//<![CDATA[
	$(window)
			.load(
					function() {
						$('.ui.dropdown').dropdown({
							forceSelection : false
						});
						$.fn.dropdown.settings.selectOnKeydown = false;
						$.fn.dropdown.settings.forceSelection = false;

						$('.ui.dropdown').dropdown({
							fullTextSearch : true,
							forceSelection : false,
							selectOnKeydown : true,
							showOnFocus : true,
							on : "click"
						});

						$('.ui.dropdown.oveallsearch').dropdown({
							onChange : function(value, text, $selectedItem) {

								var uid = $('#hdneampmuserid').val();
								redirectbysearchvalue(value, uid);
							},
							fullTextSearch : true,
							forceSelection : false,
							selectOnKeydown : false,
							showOnFocus : true,
							on : "click"
						});

						var $ddlType = $('#ddlType'), $ddlCategory = $('#ddlCategory'), $options = $ddlCategory
								.find('option');
						$ddlType.on(
								'change',
								function() {
									$ddlCategory.html($options
											.filter('[data-value="'
													+ this.value + '"]'));
								}).trigger('change');//]]> 

						var validation = {
							txtSubject : {
								identifier : 'txtSubject',
								rules : [ {
									type : 'empty',
									prompt : 'Please enter a value'
								}, {
									type : 'maxLength[50]',
									prompt : 'Please enter a value'
								} ]
							},
							ddlSite : {
								identifier : 'ddlSite',
								rules : [ {
									type : 'empty',
									prompt : 'Please enter a value'
								} ]
							},
							ddlType : {
								identifier : 'ddlType',
								rules : [ {
									type : 'empty',
									prompt : 'Please enter a value'
								} ]
							},
							ddlCategory : {
								identifier : 'ddlCategory',
								rules : [ {
									type : 'empty',
									prompt : 'Please enter a value'
								} ]
							},
							ddlPriority : {
								identifier : 'ddlPriority',
								rules : [ {
									type : 'empty',
									prompt : 'Please select a dropdown value'
								} ]
							},
							description : {
								identifier : 'description',
								rules : [ {
									type : 'empty',
									prompt : 'Please Enter Description'
								} ]
							}
						};
						var settings = {
							onFailure : function() {
								return false;
							},
							onSuccess : function() {
								$('#btnCreate').hide();
								$('#btnCreateDummy').show()
								//$('#btnReset').attr("disabled", "disabled");          
							}
						};
						$('.ui.form.validation-form')
								.form(validation, settings);
					})
</script>


<style type="text/css">
.dropdown-menu-right {
	left: -70px !important;
}
/* .ui.search.dropdown .menu {
    max-height: 12.02857143rem !important;
} */
.ui.search.dropdown>input.search {
	background: none transparent !important;
	border: none !important;
	box-shadow: none !important;
	cursor: text;
	top: 0em;
	left: -2px;
	width: 100%;
	outline: none;
	-webkit-tap-highlight-color: rgba(255, 255, 255, 0);
	padding: inherit;
}

div.dataTables_wrapper div.dataTables_length select {
	width: 50px;
	display: inline-block;
	margin-left: 3px;
	margin-right: 4px;
}

.ui




























.form














 














textarea




























:not














 














(
[
rows
]














 














)
{
height




























:














 














4
em




























;
min-height




























:














 














4
em




























;
max-height




























:














 














24
em




























;
}
.ui.icon.input>i.icon:before, .ui.icon.input>i.icon:after {
	left: 0;
	position: absolute;
	text-align: center;
	top: 36%;
	width: 100%;
}

.evtDropdown {
	/* min-height: 2.7142em !important; 
font-size:13px !important;
padding: 0.78571429em 2.1em 0.78571429em 1em !important;  */
	
}

.input-sm, .form-horizontal .form-group-sm .form-control {
	font-size: 13px;
	min-height: 25px;
	height: 32px;
	padding: 8px 9px;
}

.editbtn {
	cursor: pointer;
}

#example td a {
	color: #337ab7;
	font-weight: bold;
}

.dt-button-background {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 0 !important;
}

.dropevent {
	min-width: 100px;
	margin-left: -35px;
}

.dropevent li a {
	color: #000 !important;
	font-weight: normal !important;
}

.tbh-width {
	width: 150px;
	padding-right: 30px !important;
}

.dt-buttons {
	margin-left: 50px;
}

.buttons-collection {
	height: 28px;
	padding: 3px;
	/*  z-index:9999; */
}

.buttons-excel {
	background: rgba(36, 95, 68, 0.87);
	color: #FFF;
}

.buttons-pdf {
	background: rgb(98, 165, 96);
	color: #FFF;
}

.dataTables_filter {
	float: right;
}

.dataTables_length {
	float: left;
}

.ui.selection.dropdown {
	width: 100%;
	margin-top: 0.1em;
	/*  height: 2.3em; */
}

.ui.selection.dropdown .menu {
	width: 100%;
	white-space: normal !important;
}

.ui.selection.dropdown .menu .item:first-child {
	border-top: 1px solid #ddd !important;
	min-height: 2.8em;
}

.ui.selection.dropdown .icon {
	text-align: right;
	margin-left: 7px !important;
}
</style>

</head>
<body class="fixed-header">

	<input type="hidden" value="${access.userID}" id="hdneampmuserid">

	<input type="hidden" value="${classtate}" id="hdnstate">

	<jsp:include page="slider.jsp" />



	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					<a href="./dashboard"><img src="resources/img/logo01.png"
						alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx tktSearch"
					id="SearcSElect">
					<div class="ui  search selection  dropdown  width oveallsearch">
						<input id="myInput" name="tags" type="text">

						<div class="default text">search</div>

						<div id="myDropdown" class="menu">

							<jsp:include page="searchselect.jsp" />


						</div>
					</div>

				</div>

				<div class="dropdown pull-right hidden-md-down user-log">
					<button class="profile-dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 circular inline"> <img
							src="resources/img/user_01.png" alt
							data-src="resources/img/user_01.png"
							data-src-retina="resources/img/user_01.png" width="32"
							height="32">
						</span>

						<c:if test="${not empty access}">
							<p class="user-login">${access.userName}</p>
						</c:if>
					</button>
					<div class="dropdown-menu dropdown-menu-right profile-dropdown"
						role="menu">
						<a class="dropdown-item disablecolor"><i
							class="pg-settings_small"></i> Settings</a> <a
							class="dropdown-item disablecolor"><i class="pg-outdent"></i>
							Feedback</a> <a class="dropdown-item disablecolor"><i
							class="pg-signals"></i> Help</a> <a href="logout1"
							class="dropdown-item color logout"><i class="pg-power"></i>
							Logout</a>
					</div>
				</div>


				<div class="newticketpopup hidden">
					<span data-toggle="modal" data-target="#tktCreation"
						data-backdrop="static" data-keyboard="false"><p
							class="center m-t-5 tkts">
							<img class="m-t-5" src="resources/img/tkt.png">
						<p class="create-tkts">New Ticket</p>
						</p></span>

				</div>


			</div>
		</div>
		<div class="page-content-wrapper" id="QuickLinkWrapper1">

			<div class="content ">

				<div class="container-fixed-lg">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="./dashboard"><i
								class="fa fa-home mt-7"></i></a></li>
						<li class="breadcrumb-item "><a>Configuration</a></li>
						<li class="breadcrumb-item active"><a>Historic Upload</a></li>

					</ol>
				</div>



				<div id="newconfiguration" class="">
					<div class="card card-transparent mb-0">
						<!-- Table Start-->
						<div class="padd-3 sortable">


							<form method="post" class="ui form noremal"
								style="margin-bottom: 0px;">
								<div class="row">
									<div class="col-lg-12 col-xs-12 col-md-12 padd-0"
										id="datadetails">
										<div class="card bg-default" data-pages="card">
											<div class="card card-transparent " style="padding: 5px 5px;">
												<ul class="nav nav-tabs nav-tabs-fillup"
													data-init-reponsive-tabs="dropdownfx">
													<li class="nav-item"><a href="#" class="active"
														data-toggle="tab" data-target="#slide1"><span>Historic
																Uploads</span></a></li>
												</ul>
												<div class="tab-content" style="overflow: visible;">
													<div class="tab-pane  active" id="slide1">
														<div class="col-md-12   m-t-5 padd-0">

															<div class="col-md-3">
																<div class="field required m-t-6">
																	<label>Site Name</label>
																</div>
																<div class="field">
																	<select class="ui search selection dropdown"
																		name="ddlSiteName" id="ddlSiteName">
																		<option value="">Select</option>
																	</select>
																</div>
															</div>


															<div class="col-md-3">
																<div class="field required m-t-6">
																	<label>Directory</label>
																</div>
																<div class="field">
																	<select
																		class="field ui fluid search selection dropdown width documentCategory"
																		id="documentCategory" name="documentCategory" onchange="loadGrid()">
																		<option value="">Select</option>
																		<option value="MODBUS">ModBus</option>
																		<option value="INV">Inverter</option>
																	</select>
																</div>
															</div>




															<div class="col-md-3">
																<div class="field required m-t-6">
																	<label style="margin-left: 13px;">Files</label>
																</div>



																<div class="col-md-8">
																	<input type="file" class="" data-icon="false" id="docs"
																		name="file" multiple="multiple" style="border: none;">
																</div>
																<div class="col-md-2">
																	<!-- <a href="#" data-toggle="tooltip"
																			data-placement="bottom" title=""
																			data-original-title="Image format should be in jpg / png / svg. Image size should be less than 5 MB."
																			class="red-tooltip"><i class="fa fa-info-circle"
																			style="font-size: 32px; color: #000;"></i></a> -->
																	<button type="button" class="btn btn-success"
																		id="uploadbtn">
																		<i class="fa fa-upload" aria-hidden="true"
																			style="font-size: 15px; margin-left: 5px;"></i>
																		Upload
																	</button>

																</div>
															</div>






														</div>





													</div>

												</div>
											</div>

										</div>
									</div>


								</div>
						</div>
					</div>




					</form>


				</div>


				<!-- Table End-->

				<!-- Table Start-->

				<div class="padd-3 sortable" id="equipmentdata">
					<div class="row" id="QuickLinkWrapper2">
						<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails"
							style="height: 260px;">
							<div class="card card-default bg-default" data-pages="card">
								<!-- <div class="card-header">
											<div class="card-title tbl-head ml-15" style="text-transform:none !important;">
												 <i class="fa fa-list" aria-hidden="true"></i> List of Events
											</div>
										</div> -->
								<!-- <div class="col-md-12" style="margin-top:10px;height:220px !important;">
											<div class="col-md-4">
											<div id="barcontainer" style="min-width: 310px; height: 200px; margin: 0 auto"></div>
											</div>
											<div class="col-md-4">
												<div id="donutcontainer" style="height: 250px"></div>
											</div>
											<div class="col-md-4 padd-0">
												<div id="activitycontainer"></div>
											</div>
										</div> -->
								<div class="padd-5 table-responsive">
									<table id="example" class="table table-striped table-bordered"
										width="100%" cellspacing="0">
										<thead>
											<tr>
												<th style="width: 15%;">Site Code</th>
												<th style="width: 25%;">Site Name</th>
												<th style="width: 10%;">Directory</th>
												<th style="width: 20%;">Uploaded Date</th>
												<th style="width: 30%;">FileName</th>
											</tr>
										</thead>
										<tbody>



										</tbody>
									</table>


								</div>
							</div>
						</div>


					</div>

				</div>

				<!-- Table End-->
			</div>
		</div>


	</div>

	<div class="container-fixed-lg footer mb-0">
		<div class="container-fluid copyright sm-text-center">
			<p class="small no-margin pull-left sm-pull-reset">
				<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
					CLEAN ENERGY</span>. <span class="hint-text">All rights reserved. </span>
			</p>
			<p class="small no-margin pull-rhs sm-pull-reset">
				<span class="hint-text">Powered by</span> <span>MESTECH
					SERVICES PVT LTD</span>.
			</p>
			<div class="clearfix"></div>
		</div>
	</div>
	</div>
	</div>

	<!-- Side Bar Content Start-->




	<div class="quickview-wrapper  builder hidden-sm hidden-xs"
		id="builder">
		<div class="p-l-10 p-r-10 ">
			<a class="builder-close quickview-toggle pg-close"></a> <a
				class="builder-toggle" data-toggle="quickview"
				data-toggle-element="#builder"><img
				src="resources/img/ImageResize_06.png"></a>
			<ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary"
				id="builderTabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#tabLayouts" role="tab"
					aria-controls="home"><span>Quick Link</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active " id="tabLayouts" role="tabcard">
					<div class="scrollable">
						<div class="p-r-50">
							<div class="list">
								<ul>
									<!-- 	<li><a href="#QuickLinkWrapper1">Ticket Filters</a></li> -->
									<li><a href="#QuickLinkWrapper2">List Of Documents</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>






	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min3.js"
		type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>









	<div id="gotop"></div>

	<!-- Share Popup End !-->
	<!-- Modal -->
	<div id="tktCreation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header Popup-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="resources/img/close.png">
					</button>
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<p class="modal-title tkt-title">
						<img src="resources/img/tkt.png"><span class="tkt-title">New
							Ticket</span>
					</p>
				</div>
				<div class="modal-body">
					<c:url var="addAction" value="/addnewticket"></c:url>
					<div class="col-md-12">
						<p class="tkt-mandat">
							<span class="errormess">*</span> All fields are mandatory
						</p>
					</div>
					<form:form action="${addAction}" modelAttribute="ticketcreation"
						class="ui form validation-form">

						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width SiteNames"
									id="ddlSite" name="ddlSite" path="siteID"
									onchange="getEquipmentAgainstSite()">
									<form:option value="" tabindex="0">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>

						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Equipment</label>
							</div>

							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width equipments"
									id="ddlEquipment" path="equipmentID" name="ddlEquipment">
									<option value="">Select</option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Type</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width evtDropdown"
									id="ddlType" name="ddlType" path="ticketType">
									<form:option value="">Select</form:option>
									<form:option value="Operation">Operation</form:option>
									<form:option value="Maintenance">Maintenance</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Category</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width category evtDropdown"
									id="ddlCategory" name="ddlCategory" path="ticketCategory">
									<form:option value="">Select </form:option>
									<form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
									<form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
									<form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
									<form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
									<form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
									<form:option data-value="Operation"
										value="Equipment Replacement">Equipment Replacement</form:option>
									<form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
									<form:option data-value="Operation" value="String Down">String Down</form:option>
									<form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
									<form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
									<form:option data-value="Operation" value="">Select</form:option>
									<form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
									<form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="DataLogger Cleaning">DataLogger Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="String Current Measurement">String Current Measurement</form:option>
									<form:option data-value="Maintenance"
										value="Preventive Maintenance">Preventive Maintenance</form:option>
									<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
									<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
									<form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
									<form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
									<form:option data-value="Maintenance" value="">Select</form:option>
								</form:select>
							</div>

						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Subject</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<form:input placeholder="Subject" autocomplete="off"
										name="Subject" type="text" id="txtSubject" path="ticketDetail"
										maxLength="50" />
								</div>
							</div>
						</div>

						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Priority</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width evtDropdown"
									id="ddlPriority" name="user" path="priority">
									<form:option value="">Select </form:option>
									<form:option data-value="3" value="3">High</form:option>
									<form:option data-value="2" value="2">Medium</form:option>
									<form:option data-value="1" value="1">Low</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Description</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<textarea id="txtTktdescription" autocomplete="off"
										name="description" style="resize: none;"
										placeholder="Ticket Description" maxLength="120"></textarea>
								</div>

							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
							<div class="btn btn-success  submit" id="btnCreate">Create</div>
							<button class="btn btn-success" type="button" id="btnCreateDummy"
								tabindex="7" style="display: none;">Create</button>
							<div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
						</div>
						<!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
						<!-- <div class="ui error message"></div> -->
					</form:form>
					<!-- <div class="ui error message"></div> -->

				</div>
			</div>
		</div>
	</div>
	<!-- Address Popup End !-->
	<!-- Modal -->
	<div id="evntCreation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header Popup-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="resources/img/close.png">
					</button>
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<p class="modal-title tkt-title">
						<img src="resources/img/tkt.png"><span class="tkt-title">Event
							Ticket Creation</span>
					</p>
				</div>
				<div class="modal-body">
					<c:url var="addAction" value="/addnewticket"></c:url>
					<div class="col-md-12">
						<p class="tkt-mandat">All fields are mandatory</p>
					</div>
					<!-- <span class="errormess">*</span> -->
					<form:form action="${addAction}" modelAttribute="ticketcreation"
						class="ui form validation-form">

						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<p class="fields  m-t-10">Site</p>
							</div>
							<div class="col-md-8 col-xs-12">
								<p class="fields  m-t-10" id="txtSiteName" path="siteID"></p>
							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<p class="fields  m-t-10">Type</p>
							</div>
							<div class="col-md-8 col-xs-12">
								<p class="fields  m-t-10" id="txtTypeName" path="ticketType">Operation</p>
							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<p class="fields  m-t-10">Priority</p>
							</div>
							<div class="col-md-8 col-xs-12">
								<p class="fields  m-t-10" id="txtPriority" path="priority"></p>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Category</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width category evtDropdown"
									id="ddlCategory" name="ddlCategory" path="ticketCategory">
									<form:option value="">Select </form:option>
									<form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
									<form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
									<form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
									<form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
									<form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
									<form:option data-value="Operation"
										value="Equipment Replacement">Equipment Replacement</form:option>
									<form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
									<form:option data-value="Operation" value="String Down">String Down</form:option>
									<form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
									<form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
									<form:option data-value="Operation" value="">Select</form:option>
									<form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
									<form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="DataLogger Cleaning">DataLogger Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="String Current Measurement">String Current Measurement</form:option>
									<form:option data-value="Maintenance"
										value="Preventive Maintenance">Preventive Maintenance</form:option>
									<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
									<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
									<form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
									<form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
									<form:option data-value="Maintenance" value="">Select</form:option>
								</form:select>
							</div>

						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Subject</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<form:input placeholder="Subject" autocomplete="off"
										name="Subject" type="text" id="txtSubject" path="ticketDetail"
										maxLength="50" />
								</div>
							</div>
						</div>


						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Description</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<textarea id="txtTktdescription" autocomplete="off"
										name="description" style="resize: none;"
										placeholder="Ticket Description" maxLength="120"></textarea>
								</div>

							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
							<div class="btn btn-success  submit">Create</div>
							<div class="btn btn-primary clear m-l-15">Reset</div>
						</div>
						<!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
						<!-- <div class="ui error message"></div> -->
					</form:form>
					<!-- <div class="ui error message"></div> -->

				</div>
			</div>
		</div>
	</div>
	<!-- Address Popup End !-->
	<script>
		$('#gotop').gotop({
			customHtml : '<i class="fa fa-angle-up fa-2x"></i>',
			bottom : '2em',
			right : '2em'
		});
	</script>
	<script>
		$(document).ready(function() {
			$(window).fadeThis({
				speed : 500,
			});
		});
	</script>


	<!-- <script type="text/javascript">
      $(document).ready(function(){
          $('.ui.form.noremal')
  .form({
  /*     inline: true, */
      on: "blur",
    fields: {
    	txtDesignation: {
        identifier: 'txtDesignation',
        rules: [
          {
            type   : 'empty',
            prompt : 'Select Designation'
          }
        ]
      },
  txtDepartment:  {
          identifier: 'txtDepartment',
          rules: [
            {
              type   : 'empty',
              prompt : 'Select Department'
            }
          ]
        },
        ddlUserRole: {
      	  identifier: 'ddlUserRole',
            rules: [
              {
                type   : 'empty',
                prompt : 'Enter Role'
              }
            ] 
        }, 
       /*  txtBloodGroup: {
    	  identifier: 'txtBloodGroup',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Blood Group'
            }
          ] 
      }, */
        
        txtUserName:  {
          identifier: 'txtUserName',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter UserName'
            }
          ]
        },
       /*  txtDateOfBirth: {
    	  identifier: 'txtDateOfBirth',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Date Of Birth'
            }
          ] 
      }, */
      txtMobileNumber: {
    	  identifier: 'txtMobileNumber',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Mobile Number'
            }
          ] 
      },
      txtEmailID: {
    	  identifier: 'txtEmailID',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Email'
            }
          ] 
      },
      txtPassword: {
    	  identifier: 'txtPassword',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Password'
            }
          ] 
      },
      ddlUserStatus: {
    	  identifier: 'ddlUserStatus',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter User Status'
            }
          ] 
      }
    }
  });

      });
    </script> -->

	<script type="text/javascript">
		$(document).ready(function() {
			var validation = {
				/* txtDesignation: {
				    identifier: 'txtDesignation',
				    rules: [
				      {
				        type   : 'empty',
				        prompt : 'Eneter Designation'
				      }
				    ]
				  },
				   txtDepartment:  {
				      identifier: 'txtDepartment',
				      rules: [
				        {
				          type   : 'empty',
				          prompt : 'Enter Department'
				        }
				      ]
				    }, */
				ddlUserRoleStatus : {
					identifier : 'ddlUserRoleStatus',
					rules : [ {
						type : 'empty',
						prompt : 'Enter Role'
					} ]
				},
				txtRoleDescription : {
					identifier : 'txtRoleDescription',
					rules : [ {
						type : 'empty',
						prompt : 'Enter Short Name'
					}, {
						type : 'regExp[/^[ A-Za-z]*$/]',
						prompt : 'Enter Numbers Only'
					}, {
						type : 'minLength[1]',
						prompt : 'Short Name Must Be at Least 1 Characters'
					}, {
						type : 'maxLength[30]',
						prompt : 'Short Name Must Be at Least 10 Characters'
					} ]
				},

				txtRoleName : {
					identifier : 'txtRoleName',
					rules : [ {
						type : 'empty',
						prompt : 'Enter UserName'
					}, {
						type : 'regExp[/^[ A-Za-z0-9()]*$/]',
						prompt : 'Enter Numbers Only'
					}, {
						type : 'minLength[1]',
						prompt : 'UserName Must Be at Least 1 Characters'
					}, {
						type : 'maxLength[100]',
						prompt : 'UserName Must Be at Least 100 Characters'
					} ]
				},

			};
			var settings = {
				onFailure : function() {
					toastr.warning("Validation Failure");
					// alert('fail');
					return false;
				},
				onSuccess : function() {
					//alert('Success');
					AjaxCallForSaveOrUpdate();
					return false;

				}
			};

			$('.ui.form.noremal').form(validation, settings);
		});
	</script>



	<script type="text/javascript">
		var siteGrid = new Map();
		$(document).ready(
				function getSites() {
					var userId = $('#hdneampmuserid').val();
					$('#example').DataTable();
					$.ajax({
						type : 'GET',
						url : './getSites?customerId=' + userId,
						success : function(msg) {
							if (msg) {

								for (i = 0; i < msg.length; i++) {
									$('#ddlSiteName').append(
											"<option value="+msg[i].localFtpHomeDirectory+">"
													+ msg[i].siteName
													+ "</option>");
								}

							}

						},
						error : function(msg) {
							console.log(msg);
						}
					});
				});
	</script>

	<script type="text/javascript">
		function loadGrid() {

			var siteCode = $('#ddlSiteName').val();
			var docType = $('#documentCategory').val();
			$('#example').DataTable().clear().draw();
			if(siteCode!=null && siteCode!=''&& docType!=null && docType!=''){
				$.ajax({
					url : './getHistory?directory=' + docType + "&siteCode="
							+ siteCode,
					type : 'GET',
					success : function(data) {
						var responseStatus = data.status;
						if (responseStatus == "success") {
						var t=$('#example').DataTable();
							var historys = data.data;
							if(historys){
								for(i=0;i<historys.length;i++){
									var history=historys[i];
									t.row
									.add([
												history.siteCode,
												history.siteName,
												history.directory,
												history.creationDateText,
												history.fileName
													
											 ])
									.draw(false);	
								}
							}
						}
					},
					error : function(err) {
						$('#iframeloading').hide();
					}
				});
			}else{
				toastr
				.error("Select siteName and directory");
			}


		}
	</script>

	<script type="text/javascript">
		$("#uploadbtn")
				.click(
						function() {
							$('#iframeloading').show();
							var siteId = $('#ddlSiteName').val();
							var siteName= $('#ddlSiteName option:selected').text();
							var docType = $('#documentCategory').val();
							if(siteId!=null && siteId!=''&& docType!=null && docType!='' && siteName!=null && siteName!=''){
								var formData = new FormData();
								formData.append('directory', docType);
								formData.append('siteCode', siteId);
								formData.append('siteName',siteName);
								var count = $('input[type=file]')[0].files.length;
								if (count == 0) {
									toastr.error("Upload file");
									return;
								}
								for (i = 0; i < count; i++) {
									var file = $('input[type=file]')[0].files[i];
									var filename = file.name;
									/* var filetype = filename.split("\\.(?=[^\\.]+$)"); */
									if(filename.endsWith(".csv.gz") || filename.endsWith(".gz") || filename.endsWith(".zip"))
										{
										
									if (file.size > 5242880) {
										toastr
												.error("File Should be less than 5MB");
										return;
									} else {
										formData.append('files',
												$('input[type=file]')[0].files[i]);
									}
								}
									else
										{
										toastr.error("File must be in .zip / .gz format");
										return;
												
										}
								}
								$
										.ajax({
											url : './uploadHistorical',
											data : formData,
											type : 'POST',
											enctype : 'multipart/form-data',
											processData : false,
											contentType : false,
											success : function(data) {
												$('#ddlSiteName').val('');
												$('#documentCategory').val('');
												$('input[type=file]').val('');
												var responseStatus = data.status;
												if (responseStatus == "Success") {
													var t = $('#example')
															.DataTable();
													var siteDocuments = data.data;
													if (siteDocuments) {
														for (i = 0; i < siteDocuments.length; i++) {
															var fileName = siteDocuments[i].fileName
																	.concat(".")
																	.concat(
																			siteDocuments[i].fileType);
															var viewPath = siteDocuments[i].prefixpath
																	.concat(
																			siteDocuments[i].directoryPath)
																	.concat(
																			siteDocuments[i].serverFileName);
															var siteId = siteDocuments[i].siteId;
															t.row
																	.add(
																			[
																					siteDocuments[i].siteCode,
																					siteDocuments[i].siteName,
																					siteDocuments[i].docCategory,
																					fileName,
																					siteDocuments[i].dateCreated,
																					/* 	"<a href='"+viewPath+"' >View</a> &nbsp &nbsp <a href='' class='btnDelete'>Delete</a>", */

																					"<span class='btn btn-danger btn-sm btnDelete'>Delete</span>" ])
																	.draw(false);
														}

													}
													$('#iframeloading').hide();
													toastr
															.info("Successfully uploaded");
													location.reload();
												} else {
													$('#iframeloading').hide();
													toastr.error("No Ftp directory for this site");
													$('#ddlSiteName').dropdown('clear');
													$('#documentCategory').dropdown('clear');
												}
											},
											error : function(err) {
												$('#iframeloading').hide();
												toastr
														.info("Upload Failed");
											}
										});
							}else{
								toastr
								.error("Select Site name and directory");
							}


						});
	</script>



</body>


</html>


