
<div class="header">
	<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
		data-toggle="sidebar"> </a>
	<div>
		<div class="brand inline">
			<a href="./dashboard"><img src="resources/img/logo01.png"
				alt="logo"
				title="Electronically Assisted Monitoring and Portfolio Management"
				class="logos"></a>
		</div>
	</div>
	<div class="d-flex align-items-center">
		<div class="form-group required field searchbx tktSearch"
			id="SearcSElect">
			<div class="ui  search selection  dropdown  width oveallsearch">
				<input id="myInput" name="tags" type="text">

				<div class="default text">search</div>

				<div id="myDropdown" class="menu">

					<c:if test="${not empty access}">
						<div class="item">Dashboard</div>

						<c:if test="${access.customerListView == 'visible'}">
							<div class="item">Customer View</div>
						</c:if>

						<div class="item">Site View</div>





						<div class="item">Tickets KPI</div>






						<div class="item">Analysis</div>


						<c:if test="${access.customerListView == 'visible'}">
							<div class="item">Site Configuration</div>
						</c:if>

						<c:if test="${access.customerListView == 'visible'}">
							<div class="item">Equipment Category Configuration</div>
						</c:if>

						<c:if test="${access.customerListView == 'visible'}">
							<div class="item">Equipment Type Configuration</div>
						</c:if>

						<c:if test="${access.customerListView == 'visible'}">
							<div class="item">Equipment Configuration</div>
						</c:if>

						<c:if test="${access.customerListView == 'visible'}">
							<div class="item">Customer Configuration</div>
						</c:if>

						<c:if test="${access.customerListView == 'visible'}">
							<div class="item">CustomerMap Configuration</div>
						</c:if>

						<c:if test="${access.customerListView == 'visible'}">
							<div class="item">User Role Configuration</div>
						</c:if>

						<c:if test="${access.customerListView == 'visible'}">
							<div class="item">User Configuration</div>
						</c:if>




					</c:if>


				</div>
			</div>



		</div>

		<div class="dropdown pull-right hidden-md-down user-log">
			<button class="profile-dropdown-toggle" type="button"
				data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="thumbnail-wrapper d32 circular inline"> <img
					src="resources/img/user_01.png" alt
					data-src="resources/img/user_01.png"
					data-src-retina="resources/img/user_01.png" width="32" height="32">
				</span>

				<c:if test="${not empty access}">
					<p class="user-login">${access.userName}</p>
				</c:if>
			</button>
			<div class="dropdown-menu dropdown-menu-right profile-dropdown"
				role="menu">
				<a class="dropdown-item disablecolor"><i
					class="pg-settings_small"></i> Settings</a> <a
					class="dropdown-item disablecolor"><i class="pg-outdent"></i>
					Feedback</a> <a class="dropdown-item disablecolor"><i
					class="pg-signals"></i> Help</a> <a href="logout1"
					class="dropdown-item color logout"><i class="pg-power"></i>
					Logout</a>
			</div>
		</div>

		<c:if test="${access.customerListView == 'visible'}">
			<div>
				<span data-toggle="modal" data-target="#tktCreation"
					data-backdrop="static" data-keyboard="false"><p
						class="center m-t-5 tkts">
						<img class="m-t-5" src="resources/img/tkt.png">
					<p class="create-tkts">New Ticket</p>
					</p></span>

			</div>
		</c:if>







	</div>
</div>