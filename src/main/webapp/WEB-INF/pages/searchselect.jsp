<div class="item dashboard-page hidden" db-id="dashboardvisible" id="searchcustomerviewid">Dashboard</div>
<div class="item customerview-page hidden"  id="searchcustomerviewid">Customer View</div>
<div class="item siteview-page hidden" id="searchsiteviewid">Site View</div>
<div class="item tickets-page hidden" id="searchticketsid">Tickets</div>
<div class="item events-page hidden" id="searcheventsid">Events</div>
<div class="item analysis-page hidden" id="searchanalyticsid">Analytics</div>
<div class="item documents-upload hidden" id="searchdocumentsuploadid">Documents Upload</div>
<div class="item documents-download hidden" id="searchdocumentsdownloadid">Documents Download</div>
<div class="item reports-page hidden" id="searchreportsid">Reports</div>

<div class="item customer-page hidden" id="searchcustomerid">Customer Configuration</div>
<div class="item site-page hidden" id="searchuserid">Site Configuration</div>
<div class="item equipmentcategory-page hidden" id="searchequipmentcategoryid">Equipment Category Configuration</div>
<div class="item equipmenttype-page hidden" id="searchequipmenttypeid">Equipment Type Configuration</div>
<div class="item equipment-page hidden" id="searchequipmentid">Equipment Configuration</div>
<div class="item parameter-page hidden" id="searchparameterid">Standard Parameter Configuration</div>
<div class="item errorcode-page hidden" id="searcherrorcodeid">Error Code Configuration</div>
<div class="item status-page hidden" id="searchstatusid">Status Configuration</div>
<div class="item dataloggermaster-page hidden" id="searchdataloggerid">Datalogger Configuration</div>
<div class="item master-upload hidden" id="searchmasteruploadid">Master Upload</div>
<div class="item historicupload-page hidden" id="searchhistoricuploadid">Historic Upload</div>
<div class="item datacollection-page hidden" id="searchdatacollectionid">Data Collection</div>
<div class="item sop-push hidden" id="searchsoppushid">SOP Configuration</div>
<div class="item user-page hidden" id="searchuserid">User Configuration</div>
<div class="item roleaccess-page hidden" id="searchuserid">RoleAccess Configuration</div>