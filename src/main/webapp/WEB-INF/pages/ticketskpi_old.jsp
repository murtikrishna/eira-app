<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet" type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet"	type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet" rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/kpiStyles.css" type="text/css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet" type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet" type="text/css">
<link href="resources/css/bootstrap.min3.3.37.css" type="text/css" rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min3.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css"	href="resources/css/jquery-ui.css">

<link href="resources/css/formValidation.min.css" rel="stylesheet" type="text/css">
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!--  <link href="resources/css/jquery-ui_cal.css" rel="stylesheet" type="text/css"> -->

<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>

 

<!-- 
<script src="resources/js/highcharts.js"></script>
      <script src="resources/js/highcharts-3d.js"></script>
      <script src="resources/js/exporting.js"></script>
      <script src="resources/js/export-data.js"></script> -->
     <script src="https://code.highcharts.com/highcharts.js"></script>
      <script src="resources/js/highcharts-3d.js"></script>
      <script src="resources/js/exporting.js"></script>
      <script src="resources/js/export-data.js"></script>
     <!--  <script src="https://code.highcharts.com/highcharts-3d.js"></script>
      <script src="https://code.highcharts.com/modules/exporting.js"></script>
      <script src="https://code.highcharts.com/modules/export-data.js"></script> -->

<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script type="text/javascript" src="resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="resources/js/formValidation.min.js"></script>
<script type="text/javascript" src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.val.js"></script>

<style type="text/css">
.content-bg {
	    background: #FFF;
    height: 45px;
    /* margin: 5px; */
    padding: 5px;
	
}
.ticketdetails {
	
color:#000; 
    padding-top: 8px;
    position: absolute;

}
.ticketdetails a {
	font-weight:bold;
	color:#4b51a7; 
}
.selectSite .text {
	color:#000 !important;
}
.selectSite .search {
    color: #000 !important;
}
.highcharts-title {
    fill: #434348;
    font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif !important;  
    font-size: 12px;
}
.highcharts-axis-title {
	fill: #434348;
    font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif !important;  
    font-size: 12px;
}
.highcharts-axis-labels {
	fill: #434348;
    font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif !important;  
    font-size: 18px;
}
.highcharts-legend-item{
	font-size: 10px !important;
}
.form-control {
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 5px;
    font-size: 12px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
.carousel-indicators {
    bottom: 0px;
    margin-bottom: 4px;
}
	.carousel-control {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    width: 2%;
    font-size: 20px;
    color: #fff;
    text-align: center;
    text-shadow: 0 1px 2px rgba(0, 0, 0, .6);
    background-color: rgba(0, 0, 0, 0);
    filter: alpha(opacity=50);
    opacity: .5;
}
.glyphicon-chevron-right:before {
    content: "\e080";
    color: #818181;
}
.glyphicon-chevron-left:before {
    content: "\e079";
    color: #818181;
}
.carousel-indicators li {
    display: inline-block;
    width: 10px;
    height: 10px;
    margin: 1px;
    text-indent: -999px;
    cursor: pointer;
    background-color: #000\9;
    background-color: #AAAAAA;
    border: 1px solid #fff;
    border-radius: 10px;
}
.carousel-control.right,.carousel-control.left{
	background-image: none !important;
}
.carousel-indicators .active {
    width: 12px;
    height: 12px;
    margin: 0;
    background-color: #CCCCCC;
}
.carousel-caption {
    position: absolute;
    right: 15%;
    bottom: 20px;
    left: 15%;
    top:0px;
    z-index: 10;
    padding-top: 0px;
    padding-bottom: 0px;
    color: #fff;
    text-align: center;
    text-shadow: none;
}
.carousel-caption {
    right: 0% !important;
    left: 1% !important;
    padding-bottom: 0px;
}
.paragrapth{
font-size: 11px;
color:black;
font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
}


/*.carousel-caption {
    right: 10%;
    left: 10%;
    padding-bottom: 30px;
}*/
</style>
<script type="text/javascript">

var ajax_request1;
var ajax_request2;
var ajax_request3;
var ajax_request4;
var ajax_request5;

function handlenullable(param){
	if(param == null || param == 'null'){
		return '';
	}
	else{
		return param;
	}
	
}

function AjaxCallForOpenStateTicketDetails() {  
	debugger;
	
	
	try
	{
		if(typeof ajax_request3 !== 'undefined')
		{
			ajax_request3.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request3 =  $.ajax({
		type: 'GET',
		url: './stateopentickets',
		success: function(msg){
	
	// $('#example').DataTable().clear().draw(); 
		  $('#example tbody').empty();
		  $('#example').DataTable().clear().draw();
   		  $('#example').DataTable().destroy(); 
	
	var ticketdetaillist = msg.ticketdetaillist;
	  
	 var categories = msg.categories;
	 var categoryAxis = "";
	 
	 for (i=0;i<categories.length;i++) {
		 
		 if (i== 0) {
			 categoryAxis = "'"  + categories[i] + "'";
		 }
		 else {
			 categoryAxis = categoryAxis + ',' +  "'"  + categories[i] + "'";
		 }
	 }
	 
	 categoryAxis = eval("[" + categoryAxis + "]");
	
	 var seriesData = ticketdetaillist;
 
	
	 var ticketgriddata = msg.ticketgriddata;
	 var siteList = msg.siteList;
	 
	 $( "#ddlSiteList" ).append("<option value='0'>All Sites</option>");
	 $.map(siteList, function(val, key) {                                    		 
   		 $( "#ddlSiteList" ).append("<option value="+ key +">" + val +"</option>");
   		});
	 
	 debugger;
	 
	 Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
Highcharts.chart('DataChart', {
             chart: {
                 type: 'bar'
             },
             title: {
                 text: 'Ticket Summary - Days Lapsed Since  Creation'
             },
           
             xAxis: {
            	 categories:  categoryAxis
                // categories: ['Open-Created','Open-Assigned','Open-Inprogress','Open-Unfinished','Open-Finished']
             },
             yAxis: {
                 min: 0,
                 allowDecimals: false,
                 title: {
                     text: 'Total Tickets '
                 }
             },
             legend: {
            	itemStyle: {
                	color: 'black',
                	fontWeight: 'bold',
                	fontSize: '10px'
           		 }
        	},
        	   exporting: {
			        menuItemDefinitions: {
			            // Custom definition
			            label: {
			                onclick: function () {
			                    this.renderer.label(
			                        'You just clicked a custom menu item',
			                        100,
			                        100
			                    )
			                    .attr({
			                        fill: '#a4edba',
			                        r: 5,
			                        padding: 10,
			                        zIndex: 10
			                    })
			                    .css({
			                        fontSize: '1.5em'
			                    })
			                    .add();
			                },
			              
			            }
			        },
			        buttons: {
			            contextButton: {
			                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
			            }
			        }
			    },
            legend: {
                 reversed: true
             },
             credits: {
            	    enabled: false
            	  },
             plotOptions: {
                 series: {
                     stacking: 'normal',
                     cursor: 'pointer',
                     pointWidth: 15,
                     point: {
                         events: {
 					click: function () {
 					
 					var category= this.category;
 					
 					var res = category.split("-");
 					
 					var state=res[0];
 					var ticketstatus=res[1];
 					
 				
 					
 					var status= this.series.name;
 					var splitdays = status.replace("days","");
 					
 					var DaysCount = splitdays.split("-");
 					
 					var fromday=DaysCount[0];
 					var today=DaysCount[1];
 					
 					
 					
 					
 					
 					 if(state == "Open")
            			{
 						state = 1;
            			}
            			else if(state == "Closed")
            			{
            				state = 2;
            			}
            			else if(state == "Hold")
            			{
            				state = 3;
            			}
 					 
 					 if(ticketstatus == "Created")
 						{
 						ticketstatus = "1";
 						}
 						else if(ticketstatus == "Assigned")
 						{
 							ticketstatus = "2";
 						}
 						else if(ticketstatus == "Inprogress")
 						{
 							ticketstatus = "3";
 						}
 						
 						if(ticketstatus == "Unfinished")
 						{
 							ticketstatus = "4";
 						}
 						else if(ticketstatus == "Finished")
 						{
 							ticketstatus = "5";
 						}
 						else if(ticketstatus == "Closed")
 						{
 							ticketstatus = "6";
 						}
 						else if(ticketstatus == "Hold")
 						{
 							ticketstatus = "7";
 						}
 						
 						  $('#example tbody').empty();
 						 $('#example').DataTable().clear().draw();
 				   		  $('#example').DataTable().destroy(); 
 						AjaxCallForDaylapsedsincecreationTicketfileterdetails(state,ticketstatus,fromday,today); 
 						
 }
 }
 }

                 }
             },
            
             series: seriesData
         });



		

		 var arraylength=10;
		if(ticketgriddata.length < 10)
		 {
		 arraylength = ticketgriddata.length;
	 
	 	} 
		var ticketrows = "";
	for(var i=0; i < arraylength; i++)
	{
		var priority = ticketgriddata[i].priority;
		if(priority == 1)
		{
			priority = "Low";
		}
		else if(priority == 2)
		{
			priority = "Medium";
		}
		else if(priority == 3)
		{
			priority = "High";
		}
		
		var state = ticketgriddata[i].state;
		if(state == 1)
		{
			state = "Open";
		}
		else if(state == 2)
		{
			state = "Closed";
		}
		else if(state == 3)
		{
			state = "Hold";
		}
		
		var ticketStatus = ticketgriddata[i].ticketStatus;
		
		if(ticketStatus == 1)
		{
			ticketStatus = "Created";
		}
		else if(ticketStatus == 2)
		{
			ticketStatus = "Assigned";
		}
		else if(ticketStatus == 3)
		{
			ticketStatus = "Inprogress";
		}
		
		if(ticketStatus == 4)
		{
			ticketStatus = "Unfinished";
		}
		else if(ticketStatus == 5)
		{
			ticketStatus = "Finished";
		}
		else if(ticketStatus == 6)
		{
			ticketStatus = "Closed";
		}
		else if(ticketStatus == 7)
		{
			ticketStatus = "Hold";
		}
		
		
		
		
		
		<c:choose>
		<c:when test="${access.customerListView == 'visible'}">
		
		var href = '.\\ticketviews' + ticketgriddata[i].ticketID;
		
		if(i==0)
		{
			
			ticketrows = "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
			
		}
		else
		{
			ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
			
		}
		
		</c:when>
		
		<c:otherwise>
		

		
		if(i==0)
		{
			
			ticketrows = "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
			
		}
		else
		{
			ticketrows = ticketrows + "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
			
		}
			
		
		</c:otherwise>
		</c:choose>
		         
		
		//$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketdetaillist[i].ticketCode+'</a>' ,ticketdetaillist[i].siteName,priority,ticketdetaillist[i].createdDateText,ticketdetaillist[i].ticketCategory,ticketdetaillist[i].ticketDetail,ticketdetaillist[i].assignedToWhom,ticketdetaillist[i].scheduledDateText,state,ticketStatus]).draw(false);
		
		
	
	} 
	

$('#example').append(ticketrows);
$('#example').DataTable( {
	 "order": [[ 0, "desc" ]],
	 "lengthMenu": [[10], [10]],
	 "bLengthChange": true,        		
	 } ); 
$('.dataTables_filter input[type="search"]').attr('placeholder','search');


	 
     
 /*   for(var i=0; i < arraylength; i++)
		{
			var priority = ticketgriddata[i].priority;
			if(priority == 1)
			{
				priority = "Low";
			}
			else if(priority == 2)
			{
				priority = "Medium";
			}
			else if(priority == 3)
			{
				priority = "High";
			}
			
			var state = ticketgriddata[i].state;
			if(state == 1)
			{
				state = "Open";
			}
			else if(state == 2)
			{
				state = "Closed";
			}
			else if(state == 3)
			{
				state = "Hold";
			}
			
			var ticketStatus = ticketgriddata[i].ticketStatus;
			
			if(ticketStatus == 1)
			{
				ticketStatus = "Created";
			}
			else if(ticketStatus == 2)
			{
				ticketStatus = "Assigned";
			}
			else if(ticketStatus == 3)
			{
				ticketStatus = "Inprogress";
			}
			
			if(ticketStatus == 4)
			{
				ticketStatus = "Unfinished";
			}
			else if(ticketStatus == 5)
			{
				ticketStatus = "Finished";
			}
			else if(ticketStatus == 6)
			{
				ticketStatus = "Closed";
			}
			else if(ticketStatus == 7)
			{
				ticketStatus = "Hold";
			}
			
			
			
		//	var action= '<div class="dropdown"><button  data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu dropevent" role="menu" aria-labelledby="dLabel" > <li  class="evtCol" data-toggle="modal" data-target="#evntCreation" data-backdrop="static" data-keyboard="false"><a href="#"><span id="evt">Ticket</span></a></li> <li><a onclick="supresseventdetails(' + eventdetaillist[i].transactionId + ')">Suppress</a></li></ul></div>';
			
			//supresseventdetails          
			var href = '.\\ticketviews' + ticketgriddata[i].ticketID;
			
			$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketgriddata[i].ticketCode+'</a>' ,ticketgriddata[i].siteName,priority,ticketgriddata[i].createdDateText,ticketgriddata[i].ticketCategory,ticketgriddata[i].ticketDetail,ticketgriddata[i].assignedToWhom,ticketgriddata[i].scheduledDateText,state,ticketStatus]).draw(false);
 				
			
			//style="width:7%;display:none;"
 
		}  */
    
    
    
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForAllStateTicketDetails() {  
	
	try
	{
		if(typeof ajax_request2 !== 'undefined')
		{
			ajax_request2.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request2 = $.ajax({
type: 'GET',
url: './Allstateopentickets',
success: function(msg){
	
	  
	  var ticketdetaillist = msg.ticketdetaillist;
	  
	 var categories = msg.categories;
	 var categoryAxis = "";
	 
	 for (i=0;i<categories.length;i++) {
		 
		 if (i== 0) {
			 categoryAxis = "'"  + categories[i] + "'";
		 }
		 else {
			 categoryAxis = categoryAxis + ',' +  "'"  + categories[i] + "'";
		 }
	 }
	 
	 categoryAxis = eval("[" + categoryAxis + "]");
	
	 var seriesData = ticketdetaillist;

	 
	
	 Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
	 Highcharts.chart('BarChart', {
		    chart: {
		        type: 'column'
		    },
		    title: {
		        text: 'Ticket State Summary'
		    },
		    legend: {
		            itemStyle: {
		                color: 'black',
		                fontWeight: 'bold',
		                fontSize: '10px'
		            }
		        },
		    xAxis: {
		        categories: categoryAxis,
		     
		        labels: {
		            style: {
		                color: '#000',
		                fontWeight:'bold'
		            }
		        }
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Tickets Count',
		            style: {
		                color: '#000',
		                fontWeight:'bold'
		            }
		        },
		        stackLabels: {
		            enabled: false,
		            style: {
		                fontWeight: 'bold',
		                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
		            }
		        }
		        
		    },
		    exporting: {
		        menuItemDefinitions: {
		            // Custom definition
		            label: {
		                onclick: function () {
		                    this.renderer.label(
		                        'You just clicked a custom menu item',
		                        100,
		                        100
		                    )
		                    .attr({
		                        fill: '#a4edba',
		                        r: 5,
		                        padding: 10,
		                        zIndex: 10
		                    })
		                    .css({
		                        fontSize: '1.5em'
		                    })
		                    .add();
		                },
		              
		            }
		        },
		        buttons: {
		            contextButton: {
		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		            }
		        }
		    },
		    tooltip: {
		    	headerFormat: '<tabel><tr><td><span style="font-size:10px;fontWeight:600;align:center;"><b>{point.x}</b></span></td></tr></tabel><br/>',
		    	pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> <br/>',
		        shared: true
		    },
		    credits: {
		        enabled: false
		      },
		    plotOptions: {
                series: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    },
                    cursor:'pointer',
                    point: {
                        events: {
					click: function () {
					
					var category= this.category;
					var status= this.series.name;
					/* alert(category);
					alert(status); */
					
					 if(category == "Open")
           			{
						 category = 1;
           			}
           			else if(category == "Closed")
           			{
           				category = 2;
           			}
           			else if(category == "Hold")
           			{
           				category = 3;
           			}
					 
					 if(status == "Created")
						{
						 status = "1";
						}
						else if(status == "Assigned")
						{
							status = "2";
						}
						else if(status == "Inprogress")
						{
							status = "3";
						}
						
						if(status == "Unfinished")
						{
							status = "4";
						}
						else if(status == "Finished")
						{
							status = "5";
						}
						else if(status == "Closed")
						{
							status = "6";
						}
						else if(status == "Hold")
						{
							status = "7";
						}
						
						  $('#example tbody').empty();
						  $('#example').DataTable().clear().draw();
				   		  $('#example').DataTable().destroy(); 
						AjaxCallForStateandStatusTicketfileterdetails(category,status);
   
}
}
}

                   
                }
		   
            },
		    series: seriesData
		    
		});

	
	 
	
	 


	 
   
  
  
  
  
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForStateTicketDetails() {  
	
	try
	{
		if(typeof ajax_request4 !== 'undefined')
		{
			ajax_request4.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request4 =  $.ajax({
type: 'GET',
url: './stateopenticketsByAssignedByUser',
success: function(msg){
	
	  
	  var ticketdetaillist = msg.ticketdetaillist;
	  
	  var categories = msg.categories;
	  
	
	 var categoryAxis = "";
	 
	 for (i=0;i<categories.length;i++) {
		 
		 if (i== 0) {
			 categoryAxis = "'"  + categories[i] + "'";
		 }
		 else {
			 categoryAxis = categoryAxis + ',' +  "'"  + categories[i] + "'";
		 }
	 }
	 
	 categoryAxis = eval("[" + categoryAxis + "]");
	
	 var seriesData = ticketdetaillist;

	 
	
	 
	
	 
	 
	 debugger;
	 Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
	 Highcharts.chart('ColumnChart', {
		    chart: {
		        type: 'bar'
		    },
		    title: {
		        text: 'Ticket Summary - Days Lapsed Since  Creation'
		    },
		    xAxis: {
		    	 categories:  categoryAxis
		        /* categories: ['Hold - Hold', 'Closed - Unfinished', 'Closed - Finished', 'Closed - Closed', 'Open - Unfinsihed','Open - Finsihed','Open - Inprogress','Open - Assigned'] */
		    },
		    yAxis: {
		        min: 0,
		        allowDecimals: false,
		        title: {
		            text: 'Total Tickets '
		        }
		    },
		    legend: {
		            	itemStyle: {
		                	color: 'black',
		                	fontWeight: 'bold',
		                	fontSize: '10px'
		           		 }
		        	},
		        	credits: {
		        	    enabled: false
		        	  },
		    /*legend: {
		        reversed: true
		    },*/
		    plotOptions: {
                series: {
                    stacking: 'normal',
                    cursor: 'pointer',
                    pointWidth: 20
                }

            },
		    
		    series: seriesData
		  
		});


	 
   
  
  
  
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForStateTicketDetailsforpiechart() {  
	
	try
	{
		if(typeof ajax_request1 !== 'undefined')
		{
			ajax_request1.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request1 =  $.ajax({
type: 'GET',
url: './stateopenticketforpiechart',
success: function(msg){
	
	  
	  var ticketdetaillist = msg.ticketdetaillist;
	
	  
	
	  var seriesData = ticketdetaillist;
	  

	  var ticketsdata = eval("[" + seriesData + "]");
	  
	  Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
	  Highcharts.chart('Chartcontainer', {
          chart: {
              type: 'pie',
              options3d: {
                  enabled: true,
                  alpha:75,
                  beta: 0
              }
          },
          title: {
              text: 'Ticket Summary'
          },
          legend: {
         itemStyle: {
             color: 'black',
             fontWeight: 'bold',
             fontSize: '20px'
         }
     },
          tooltip: {
              pointFormat: '{point.y}'
          },
          credits: {
        	    enabled: false
        	  },
          plotOptions: {
              pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  depth: 35,
                  dataLabels: {
                      enabled: true,
                      format: '{point.name}'
                  }
              }
             
          
          },
          exporting: {
		        menuItemDefinitions: {
		            // Custom definition
		            label: {
		                onclick: function () {
		                    this.renderer.label(
		                        'You just clicked a custom menu item',
		                        100,
		                        100
		                    )
		                    .attr({
		                        fill: '#a4edba',
		                        r: 5,
		                        padding: 10,
		                        zIndex: 10
		                    })
		                    .css({
		                        fontSize: '1.5em'
		                    })
		                    .add();
		                },
		              
		            }
		        },
		        buttons: {
		            contextButton: {
		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		            }
		        }
		    },
          series: [{
              type: 'pie',
              name: 'Ticket Summary',
               data: ticketsdata,
              
              point: {
                  events: {
                      click: function () {
                          //alert('Category: ' + this.name + ', value: ' + this.y);
                          var Dtaa = (this.name);
                          
                          if(Dtaa == "Open")
              			{
                        	  Dtaa = 1;
              			}
              			else if(Dtaa == "Closed")
              			{
              				Dtaa = 2;
              			}
              			else if(Dtaa == "Hold")
              			{
              				Dtaa = 3;
              			}
                         /*  alert(Dtaa); */
                          $('#example tbody').empty();
                          $('#example').DataTable().clear().draw();
   		 				 $('#example').DataTable().destroy(); 
                          AjaxCallForOpenTicketfileterdetails(Dtaa);

                      }
                  }
              }
          }]
      });
	 
	  

	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}




function AjaxCallForOpenStateTicketDetailsBySiteByClick(siteid) {  
	
	try
	{
		if(typeof ajax_request3 !== 'undefined')
		{
			ajax_request3.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request3 =  $.ajax({
type: 'GET',
url: './clickstateopenticketsBysite',
data: {'Siteid' :siteid },
success: function(msg){
	
	 $('#example tbody').empty();
	 $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	  var ticketdetaillist = msg.ticketdetaillist;
	  
	 var categories = msg.categories;
	 var categoryAxis = "";
	 
	 for (i=0;i<categories.length;i++) {
		 
		 if (i== 0) {
			 categoryAxis = "'"  + categories[i] + "'";
		 }
		 else {
			 categoryAxis = categoryAxis + ',' +  "'"  + categories[i] + "'";
		 }
	 }
	 
	 categoryAxis = eval("[" + categoryAxis + "]");
	
	 var seriesData = ticketdetaillist;
/*  for (i=0;i<ticketdetaillist.length;i++) {
		 
		 if (i== 0) {
			 seriesname = "'"  + ticketdetaillist[i] + "'";
		 }
		 else {
			
		 }
	 }  */
		
	/* 
[{
  name: '30+ days',
  data: [5, 3, 4, 7, 2],
  color:''
}, {
  name: '16-30 days',
  data: [2, 2, 3, 2, 1]
}, {
  name: '6-15 days',
  data: [3, 4, 4, 2, 5]
},
{
  name: '0-5 days',
  data: [3, 4, 4, 2, 5]
}] */
	 
	
	 
	 var siteticketgriddata = msg.ticketgriddata;
	 var siteList = msg.siteList;
	/*  $.map(siteList, function(val, key) {                                    		 
		 $( "#ddlSiteList" ).append("<option value="+ key +">" + val +"</option>");
		}); */
	
	 
	 debugger;
	 
		Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
Highcharts.chart('DataChart', {
         chart: {
             type: 'bar'
         },
         title: {
             text: 'Ticket Summary - Days Lapsed Since  Creation'
         },
       
         xAxis: {
        	 categories:  categoryAxis
            // categories: ['Open-Created','Open-Assigned','Open-Inprogress','Open-Unfinished','Open-Finished']
         },
         plotOptions: {
             series: {
                 pointWidth: 200
             }
         },
         credits: {
        	    enabled: false
        	  },

         yAxis: {
             min: 0,
             allowDecimals: false,
             title: {
                 text: 'Total Tickets '
             }
         },
         legend: {
        	itemStyle: {
            	color: 'black',
            	fontWeight: 'bold',
            	fontSize: '10px'
       		 }
    	},
    	   exporting: {
		        menuItemDefinitions: {
		            // Custom definition
		            label: {
		                onclick: function () {
		                    this.renderer.label(
		                        'You just clicked a custom menu item',
		                        100,
		                        100
		                    )
		                    .attr({
		                        fill: '#a4edba',
		                        r: 5,
		                        padding: 10,
		                        zIndex: 10
		                    })
		                    .css({
		                        fontSize: '1.5em'
		                    })
		                    .add();
		                },
		              
		            }
		        },
		        buttons: {
		            contextButton: {
		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		            }
		        }
		    },
        legend: {
             reversed: true
         },
         plotOptions: {
             series: {
                 stacking: 'normal',
                 cursor: 'pointer',
                 pointWidth: 15,
                 point: {
                     events: {
					click: function () {
					
					var category= this.category;
					
					var res = category.split("-");
					
					var state=res[0];
					var ticketstatus=res[1];
					
				
					
					var status= this.series.name;
					var splitdays = status.replace("days","");
					
					var DaysCount = splitdays.split("-");
					
					var fromday=DaysCount[0];
					var today=DaysCount[1];
					
					
					
					
					
					 if(state == "Open")
        			{
						state = 1;
        			}
        			else if(state == "Closed")
        			{
        				state = 2;
        			}
        			else if(state == "Hold")
        			{
        				state = 3;
        			}
					 
					 if(ticketstatus == "Created")
						{
						ticketstatus = "1";
						}
						else if(ticketstatus == "Assigned")
						{
							ticketstatus = "2";
						}
						else if(ticketstatus == "Inprogress")
						{
							ticketstatus = "3";
						}
						
						if(ticketstatus == "Unfinished")
						{
							ticketstatus = "4";
						}
						else if(ticketstatus == "Finished")
						{
							ticketstatus = "5";
						}
						else if(ticketstatus == "Closed")
						{
							ticketstatus = "6";
						}
						else if(ticketstatus == "Hold")
						{
							ticketstatus = "7";
						}
						
						  $('#example tbody').empty();
						  $('#example').DataTable().clear().draw();
				   		  $('#example').DataTable().destroy(); 
						AjaxCallForSiteDaylapsedsincecreationTicketfileterdetails(state,ticketstatus,fromday,today,siteid); 
						
}
}
}

             }
         },
        
         series: seriesData
     });

	 
	 
var arraylength=10;
if(siteticketgriddata.length < 10)
 {
 arraylength = siteticketgriddata.length;

	} 
var ticketrows = "";
for(var i=0; i < arraylength; i++)
{
var priority = siteticketgriddata[i].priority;
if(priority == 1)
{
	priority = "Low";
}
else if(priority == 2)
{
	priority = "Medium";
}
else if(priority == 3)
{
	priority = "High";
}

var state = siteticketgriddata[i].state;
if(state == 1)
{
	state = "Open";
}
else if(state == 2)
{
	state = "Closed";
}
else if(state == 3)
{
	state = "Hold";
}

var ticketStatus = siteticketgriddata[i].ticketStatus;

if(ticketStatus == 1)
{
	ticketStatus = "Created";
}
else if(ticketStatus == 2)
{
	ticketStatus = "Assigned";
}
else if(ticketStatus == 3)
{
	ticketStatus = "Inprogress";
}

if(ticketStatus == 4)
{
	ticketStatus = "Unfinished";
}
else if(ticketStatus == 5)
{
	ticketStatus = "Finished";
}
else if(ticketStatus == 6)
{
	ticketStatus = "Closed";
}
else if(ticketStatus == 7)
{
	ticketStatus = "Hold";
}





<c:choose>
<c:when test="${access.customerListView == 'visible'} ">


var href = '.\\ticketviews' + siteticketgriddata[i].ticketID;

if(i==0)
{
	
	ticketrows = "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(siteticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}
else
{
	ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(siteticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}

</c:when>

<c:otherwise>



if(i==0)
{
	
	ticketrows = "<tr><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}
else
{
	ticketrows = ticketrows + "<tr><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}
	

</c:otherwise>
</c:choose>
         

//$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketdetaillist[i].ticketCode+'</a>' ,ticketdetaillist[i].siteName,priority,ticketdetaillist[i].createdDateText,ticketdetaillist[i].ticketCategory,ticketdetaillist[i].ticketDetail,ticketdetaillist[i].assignedToWhom,ticketdetaillist[i].scheduledDateText,state,ticketStatus]).draw(false);



} 

$('#example').append(ticketrows);
$('#example').DataTable( {
"order": [[ 0, "desc" ]],
"lengthMenu": [[10,15, 25, 50, -1], [10,15, 25, 50, "All"]],
"bLengthChange": true,        		
} ); 
$('.dataTables_filter input[type="search"]').attr('placeholder','search');
 
 


	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}




function AjaxCallForOpenStateTicketDetailsBySiteByClickCustomerpage(siteid,customerid) {  
	
	try
	{
		if(typeof ajax_request3 !== 'undefined')
		{
			ajax_request3.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request3 =  $.ajax({
type: 'GET',
url: './customerclickstateopenticketsBysite',
data: {'Siteid' : siteid,'Customerid' :customerid },
success: function(msg){
	
	 $('#example tbody').empty();
	 $('#example').DataTable().clear().draw();
    $('#example').DataTable().destroy(); 
	  var ticketdetaillist = msg.ticketdetaillist;
	  
	 var categories = msg.categories;
	 var categoryAxis = "";
	 
	 for (i=0;i<categories.length;i++) {
		 
		 if (i== 0) {
			 categoryAxis = "'"  + categories[i] + "'";
		 }
		 else {
			 categoryAxis = categoryAxis + ',' +  "'"  + categories[i] + "'";
		 }
	 }
	 
	 categoryAxis = eval("[" + categoryAxis + "]");
	
	 var seriesData = ticketdetaillist;
/*  for (i=0;i<ticketdetaillist.length;i++) {
		 
		 if (i== 0) {
			 seriesname = "'"  + ticketdetaillist[i] + "'";
		 }
		 else {
			
		 }
	 }  */
		
	/* 
[{
name: '30+ days',
data: [5, 3, 4, 7, 2],
color:''
}, {
name: '16-30 days',
data: [2, 2, 3, 2, 1]
}, {
name: '6-15 days',
data: [3, 4, 4, 2, 5]
},
{
name: '0-5 days',
data: [3, 4, 4, 2, 5]
}] */
	 
	
	 
	 var siteticketgriddata = msg.ticketgriddata;
	 var siteList = msg.siteList;
	 /* 
	  $("#ddlSiteList").empty(); 
	 $.map(siteList, function(val, key) {                                    		 
		 $( "#ddlSiteList" ).append("<option value="+ key +">" + val +"</option>");
		});
	  */
	 debugger;
	 
	  Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
Highcharts.chart('DataChart', {
     chart: {
         type: 'bar'
     },
     title: {
         text: 'Ticket Summary - Days Lapsed Since  Creation'
     },
     credits: {
    	    enabled: false
    	  },
   
     xAxis: {
    	 categories:  categoryAxis
        // categories: ['Open-Created','Open-Assigned','Open-Inprogress','Open-Unfinished','Open-Finished']
     },
     plotOptions: {
         series: {
             pointWidth: 200
         }
     },

     yAxis: {
         min: 0,
         allowDecimals: false,
         title: {
             text: 'Total Tickets '
         }
     },
     legend: {
    	itemStyle: {
        	color: 'black',
        	fontWeight: 'bold',
        	fontSize: '10px'
   		 }
	},
	   exporting: {
		        menuItemDefinitions: {
		            // Custom definition
		            label: {
		                onclick: function () {
		                    this.renderer.label(
		                        'You just clicked a custom menu item',
		                        100,
		                        100
		                    )
		                    .attr({
		                        fill: '#a4edba',
		                        r: 5,
		                        padding: 10,
		                        zIndex: 10
		                    })
		                    .css({
		                        fontSize: '1.5em'
		                    })
		                    .add();
		                },
		              
		            }
		        },
		        buttons: {
		            contextButton: {
		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		            }
		        }
		    },
    legend: {
         reversed: true
     },
     plotOptions: {
         series: {
             stacking: 'normal',
             cursor: 'pointer',
             pointWidth: 15,
             point: {
                 events: {
					click: function () {
					
					var category= this.category;
					
					var res = category.split("-");
					
					var state=res[0];
					var ticketstatus=res[1];
					
				
					
					var status= this.series.name;
					var splitdays = status.replace("days","");
					
					var DaysCount = splitdays.split("-");
					
					var fromday=DaysCount[0];
					var today=DaysCount[1];
					
					
					
					
					
					 if(state == "Open")
    			{
						state = 1;
    			}
    			else if(state == "Closed")
    			{
    				state = 2;
    			}
    			else if(state == "Hold")
    			{
    				state = 3;
    			}
					 
					 if(ticketstatus == "Created")
						{
						ticketstatus = "1";
						}
						else if(ticketstatus == "Assigned")
						{
							ticketstatus = "2";
						}
						else if(ticketstatus == "Inprogress")
						{
							ticketstatus = "3";
						}
						
						if(ticketstatus == "Unfinished")
						{
							ticketstatus = "4";
						}
						else if(ticketstatus == "Finished")
						{
							ticketstatus = "5";
						}
						else if(ticketstatus == "Closed")
						{
							ticketstatus = "6";
						}
						else if(ticketstatus == "Hold")
						{
							ticketstatus = "7";
						}
						
						  $('#example tbody').empty();
						  $('#example').DataTable().clear().draw();
				   		  $('#example').DataTable().destroy(); 
						AjaxCallForSiteDaylapsedsincecreationTicketfileterdetails(state,ticketstatus,fromday,today,siteid); 
						
}
}
}

         }
     },
    
     series: seriesData
 });

var arraylength=10;
if(siteticketgriddata.length < 10)
 {
 arraylength = siteticketgriddata.length;

	} 
var ticketrows = "";
for(var i=0; i < arraylength; i++)
{
var priority = siteticketgriddata[i].priority;
if(priority == 1)
{
	priority = "Low";
}
else if(priority == 2)
{
	priority = "Medium";
}
else if(priority == 3)
{
	priority = "High";
}

var state = siteticketgriddata[i].state;
if(state == 1)
{
	state = "Open";
}
else if(state == 2)
{
	state = "Closed";
}
else if(state == 3)
{
	state = "Hold";
}

var ticketStatus = siteticketgriddata[i].ticketStatus;

if(ticketStatus == 1)
{
	ticketStatus = "Created";
}
else if(ticketStatus == 2)
{
	ticketStatus = "Assigned";
}
else if(ticketStatus == 3)
{
	ticketStatus = "Inprogress";
}

if(ticketStatus == 4)
{
	ticketStatus = "Unfinished";
}
else if(ticketStatus == 5)
{
	ticketStatus = "Finished";
}
else if(ticketStatus == 6)
{
	ticketStatus = "Closed";
}
else if(ticketStatus == 7)
{
	ticketStatus = "Hold";
}



<c:choose>
<c:when test="${access.customerListView == 'visible'}">


var href = '.\\ticketviews' + siteticketgriddata[i].ticketID;

if(i==0)
{
	
	ticketrows = "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(siteticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}
else
{
	ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(siteticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}

</c:when>

<c:otherwise>



if(i==0)
{
	
	ticketrows = "<tr><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}
else
{
	ticketrows = ticketrows + "<tr><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}
	

</c:otherwise>
</c:choose>

         

//$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketdetaillist[i].ticketCode+'</a>' ,ticketdetaillist[i].siteName,priority,ticketdetaillist[i].createdDateText,ticketdetaillist[i].ticketCategory,ticketdetaillist[i].ticketDetail,ticketdetaillist[i].assignedToWhom,ticketdetaillist[i].scheduledDateText,state,ticketStatus]).draw(false);



} 

$('#example').append(ticketrows);
$('#example').DataTable( {
"order": [[ 0, "desc" ]],
"lengthMenu": [[10,15, 25, 50, -1], [10,15, 25, 50, "All"]],
"bLengthChange": true,        		
} );  
$('.dataTables_filter input[type="search"]').attr('placeholder','search');




	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}

function AjaxCallForOpenStateTicketDetailsBySiteByClickSitepage(siteid) {  
	
	try
	{
		if(typeof ajax_request3 !== 'undefined')
		{
			ajax_request3.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request3 = $.ajax({
type: 'GET',
url: './siteclickstateopenticketsBysite',
data: {'Siteid' :siteid },
success: function(msg){
	
	 $('#example tbody').empty();
	 $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	  var ticketdetaillist = msg.ticketdetaillist;
	  
	 var categories = msg.categories;
	 var categoryAxis = "";
	 
	 for (i=0;i<categories.length;i++) {
		 
		 if (i== 0) {
			 categoryAxis = "'"  + categories[i] + "'";
		 }
		 else {
			 categoryAxis = categoryAxis + ',' +  "'"  + categories[i] + "'";
		 }
	 }
	 
	 categoryAxis = eval("[" + categoryAxis + "]");
	
	 var seriesData = ticketdetaillist;
/*  for (i=0;i<ticketdetaillist.length;i++) {
		 
		 if (i== 0) {
			 seriesname = "'"  + ticketdetaillist[i] + "'";
		 }
		 else {
			
		 }
	 }  */
		
	/* 
[{
name: '30+ days',
data: [5, 3, 4, 7, 2],
color:''
}, {
name: '16-30 days',
data: [2, 2, 3, 2, 1]
}, {
name: '6-15 days',
data: [3, 4, 4, 2, 5]
},
{
name: '0-5 days',
data: [3, 4, 4, 2, 5]
}] */
	 
	
	 var siteticketgriddata = msg.ticketgriddata;
	 var siteList = msg.siteList;
	 
	 /*  $("#ddlSiteList").empty(); 
	 $.map(siteList, function(val, key) {                                    		 
		 $( "#ddlSiteList" ).append("<option value="+ key +">" + val +"</option>");
		}); */
	 
	 debugger;
	 
		Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
Highcharts.chart('DataChart', {
       chart: {
           type: 'bar'
       },
       title: {
           text: 'Ticket Summary - Days Lapsed Since  Creation'
       },
     
       xAxis: {
      	 categories:  categoryAxis
          // categories: ['Open-Created','Open-Assigned','Open-Inprogress','Open-Unfinished','Open-Finished']
       },
       plotOptions: {
           series: {
               pointWidth: 200
           }
       },
       credits: {
    	    enabled: false
    	  },
       yAxis: {
           min: 0,
           allowDecimals: false,
           title: {
               text: 'Total Tickets '
           }
       },
       legend: {
      	itemStyle: {
          	color: 'black',
          	fontWeight: 'bold',
          	fontSize: '10px'
     		 }
  	},
  	   exporting: {
		        menuItemDefinitions: {
		            // Custom definition
		            label: {
		                onclick: function () {
		                    this.renderer.label(
		                        'You just clicked a custom menu item',
		                        100,
		                        100
		                    )
		                    .attr({
		                        fill: '#a4edba',
		                        r: 5,
		                        padding: 10,
		                        zIndex: 10
		                    })
		                    .css({
		                        fontSize: '1.5em'
		                    })
		                    .add();
		                },
		              
		            }
		        },
		        buttons: {
		            contextButton: {
		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		            }
		        }
		    },
      legend: {
           reversed: true
       },
       plotOptions: {
           series: {
               stacking: 'normal',
               cursor: 'pointer',
               pointWidth: 15,
               point: {
                   events: {
					click: function () {
					
					var category= this.category;
					
					var res = category.split("-");
					
					var state=res[0];
					var ticketstatus=res[1];
					
				
					
					var status= this.series.name;
					var splitdays = status.replace("days","");
					
					var DaysCount = splitdays.split("-");
					
					var fromday=DaysCount[0];
					var today=DaysCount[1];
					
					
					
					
					
					 if(state == "Open")
      			{
						state = 1;
      			}
      			else if(state == "Closed")
      			{
      				state = 2;
      			}
      			else if(state == "Hold")
      			{
      				state = 3;
      			}
					 
					 if(ticketstatus == "Created")
						{
						ticketstatus = "1";
						}
						else if(ticketstatus == "Assigned")
						{
							ticketstatus = "2";
						}
						else if(ticketstatus == "Inprogress")
						{
							ticketstatus = "3";
						}
						
						if(ticketstatus == "Unfinished")
						{
							ticketstatus = "4";
						}
						else if(ticketstatus == "Finished")
						{
							ticketstatus = "5";
						}
						else if(ticketstatus == "Closed")
						{
							ticketstatus = "6";
						}
						else if(ticketstatus == "Hold")
						{
							ticketstatus = "7";
						}
						
						  $('#example tbody').empty();
						  $('#example').DataTable().clear().draw();
				   		  $('#example').DataTable().destroy(); 
						AjaxCallForSiteDaylapsedsincecreationTicketfileterdetails(state,ticketstatus,fromday,today,siteid); 
						
}
}
}

           }
       },
      
       series: seriesData
   });

var arraylength=10;
if(siteticketgriddata.length < 10)
 {
 arraylength = siteticketgriddata.length;

	} 
var ticketrows = "";
for(var i=0; i < arraylength; i++)
{
var priority = siteticketgriddata[i].priority;
if(priority == 1)
{
	priority = "Low";
}
else if(priority == 2)
{
	priority = "Medium";
}
else if(priority == 3)
{
	priority = "High";
}

var state = siteticketgriddata[i].state;
if(state == 1)
{
	state = "Open";
}
else if(state == 2)
{
	state = "Closed";
}
else if(state == 3)
{
	state = "Hold";
}

var ticketStatus = siteticketgriddata[i].ticketStatus;

if(ticketStatus == 1)
{
	ticketStatus = "Created";
}
else if(ticketStatus == 2)
{
	ticketStatus = "Assigned";
}
else if(ticketStatus == 3)
{
	ticketStatus = "Inprogress";
}

if(ticketStatus == 4)
{
	ticketStatus = "Unfinished";
}
else if(ticketStatus == 5)
{
	ticketStatus = "Finished";
}
else if(ticketStatus == 6)
{
	ticketStatus = "Closed";
}
else if(ticketStatus == 7)
{
	ticketStatus = "Hold";
}

    



<c:choose>
<c:when test="${access.customerListView == 'visible'}">

var href = '.\\ticketviews' + siteticketgriddata[i].ticketID;

if(i==0)
{
	
	ticketrows = "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(siteticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}
else
{
	ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(siteticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}

</c:when>

<c:otherwise>



if(i==0)
{
	
	ticketrows = "<tr><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}
else
{
	ticketrows = ticketrows + "<tr><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}
	

</c:otherwise>
</c:choose>

//$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketdetaillist[i].ticketCode+'</a>' ,ticketdetaillist[i].siteName,priority,ticketdetaillist[i].createdDateText,ticketdetaillist[i].ticketCategory,ticketdetaillist[i].ticketDetail,ticketdetaillist[i].assignedToWhom,ticketdetaillist[i].scheduledDateText,state,ticketStatus]).draw(false);



} 

$('#example').append(ticketrows);
$('#example').DataTable( {
"order": [[ 0, "desc" ]],
"lengthMenu": [[10,15, 25, 50, -1], [10,15, 25, 50, "All"]],
"bLengthChange": true,        		
} ); 	 

$('.dataTables_filter input[type="search"]').attr('placeholder','search');



	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}

function AjaxCallForOpenStateTicketDetailsBySite(siteid) {  
	
	try
	{
		if(typeof ajax_request3 !== 'undefined')
		{
			ajax_request3.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request3 = $.ajax({
type: 'GET',
url: './stateopenticketsBysite',
data: {'Siteid' :siteid },
success: function(msg){
	
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	  var ticketdetaillist = msg.ticketdetaillist;
	  
	 var categories = msg.categories;
	 var categoryAxis = "";
	 
	 for (i=0;i<categories.length;i++) {
		 
		 if (i== 0) {
			 categoryAxis = "'"  + categories[i] + "'";
		 }
		 else {
			 categoryAxis = categoryAxis + ',' +  "'"  + categories[i] + "'";
		 }
	 }
	 
	 categoryAxis = eval("[" + categoryAxis + "]");
	
	 var seriesData = ticketdetaillist;
/*  for (i=0;i<ticketdetaillist.length;i++) {
		 
		 if (i== 0) {
			 seriesname = "'"  + ticketdetaillist[i] + "'";
		 }
		 else {
			
		 }
	 }  */
		
	/* 
[{
    name: '30+ days',
    data: [5, 3, 4, 7, 2]
}, {
    name: '16-30 days',
    data: [2, 2, 3, 2, 1]
}, {
    name: '6-15 days',
    data: [3, 4, 4, 2, 5]
},
{
    name: '0-5 days',
    data: [3, 4, 4, 2, 5]
}] */
	 
	
	 
	 var siteticketgriddata = msg.ticketgriddata;
	 var siteList = msg.siteList;
	 
	 $.map(siteList, function(val, key) {                                    		 
 		 $( "#ddlSiteList" ).append("<option value="+ key +">" + val +"</option>");
 		 var sitevalue = $("#ddlSiteList option:selected").val();
 		});
	 
	
	 Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
Highcharts.chart('DataChart', {
           chart: {
               type: 'bar'
           },
           title: {
               text: 'Ticket Summary - Days Lapsed Since  Creation'
           },
         
           xAxis: {
          	 categories:  categoryAxis
              // categories: ['Open-Created','Open-Assigned','Open-Inprogress','Open-Unfinished','Open-Finished']
           },
           plotOptions: {
               series: {
                   pointWidth: 200
               }
           },
           credits: {
        	    enabled: false
        	  },
           yAxis: {
               min: 0,
               allowDecimals: false,
               title: {
                   text: 'Total Tickets '
               }
           },
           legend: {
          	itemStyle: {
              	color: 'black',
              	fontWeight: 'bold',
              	fontSize: '10px'
         		 }
      	},
        exporting: {
	        menuItemDefinitions: {
	            // Custom definition
	            label: {
	                onclick: function () {
	                    this.renderer.label(
	                        'You just clicked a custom menu item',
	                        100,
	                        100
	                    )
	                    .attr({
	                        fill: '#a4edba',
	                        r: 5,
	                        padding: 10,
	                        zIndex: 10
	                    })
	                    .css({
	                        fontSize: '1.5em'
	                    })
	                    .add();
	                },
	              
	            }
	        },
	        buttons: {
	            contextButton: {
	                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
	            }
	        }
	    },
          legend: {
               reversed: true
           },
           plotOptions: {
               series: {
                   stacking: 'normal',
                   cursor: 'pointer',
                   pointWidth: 15,
                   point: {
                       events: {
					click: function () {
					
					var category= this.category;
					
					var res = category.split("-");
					
					var state=res[0];
					var ticketstatus=res[1];
					
				
					
					var status= this.series.name;
					var splitdays = status.replace("days","");
					
					var DaysCount = splitdays.split("-");
					
					var fromday=DaysCount[0];
					var today=DaysCount[1];
					
					
					
					
					
					 if(state == "Open")
          			{
						state = 1;
          			}
          			else if(state == "Closed")
          			{
          				state = 2;
          			}
          			else if(state == "Hold")
          			{
          				state = 3;
          			}
					 
					 if(ticketstatus == "Created")
						{
						ticketstatus = "1";
						}
						else if(ticketstatus == "Assigned")
						{
							ticketstatus = "2";
						}
						else if(ticketstatus == "Inprogress")
						{
							ticketstatus = "3";
						}
						
						if(ticketstatus == "Unfinished")
						{
							ticketstatus = "4";
						}
						else if(ticketstatus == "Finished")
						{
							ticketstatus = "5";
						}
						else if(ticketstatus == "Closed")
						{
							ticketstatus = "6";
						}
						else if(ticketstatus == "Hold")
						{
							ticketstatus = "7";
						}
						
						  $('#example tbody').empty();
						  $('#example').DataTable().clear().draw();
				   		  $('#example').DataTable().destroy(); 
						AjaxCallForSiteDaylapsedsincecreationTicketfileterdetails(state,ticketstatus,fromday,today,siteid); 
						
}
}
}

               }
           },
          
           series: seriesData
       });

	 
	 
var arraylength=10;
if(siteticketgriddata.length < 10)
 {
 arraylength = siteticketgriddata.length;

	} 
var ticketrows = "";
for(var i=0; i < arraylength; i++)
{
var priority = siteticketgriddata[i].priority;
if(priority == 1)
{
	priority = "Low";
}
else if(priority == 2)
{
	priority = "Medium";
}
else if(priority == 3)
{
	priority = "High";
}

var state = siteticketgriddata[i].state;
if(state == 1)
{
	state = "Open";
}
else if(state == 2)
{
	state = "Closed";
}
else if(state == 3)
{
	state = "Hold";
}

var ticketStatus = siteticketgriddata[i].ticketStatus;

if(ticketStatus == 1)
{
	ticketStatus = "Created";
}
else if(ticketStatus == 2)
{
	ticketStatus = "Assigned";
}
else if(ticketStatus == 3)
{
	ticketStatus = "Inprogress";
}

if(ticketStatus == 4)
{
	ticketStatus = "Unfinished";
}
else if(ticketStatus == 5)
{
	ticketStatus = "Finished";
}
else if(ticketStatus == 6)
{
	ticketStatus = "Closed";
}
else if(ticketStatus == 7)
{
	ticketStatus = "Hold";
}


<c:choose>
<c:when test="${access.customerListView == 'visible'}">


var href = '.\\ticketviews' + siteticketgriddata[i].ticketID;

if(i==0)
{
	
	ticketrows = "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(siteticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}
else
{
	ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(siteticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}

</c:when>

<c:otherwise>



if(i==0)
{
	
	ticketrows = "<tr><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}
else
{
	ticketrows = ticketrows + "<tr><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}
	

</c:otherwise>
</c:choose>
         

//$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketdetaillist[i].ticketCode+'</a>' ,ticketdetaillist[i].siteName,priority,ticketdetaillist[i].createdDateText,ticketdetaillist[i].ticketCategory,ticketdetaillist[i].ticketDetail,ticketdetaillist[i].assignedToWhom,ticketdetaillist[i].scheduledDateText,state,ticketStatus]).draw(false);



} 

$('#example').append(ticketrows);
$('#btnCreate').click(function() {
	if($('#ddlCategory').val== "") {
    	alert('Val Empty');
        $('.category ').addClass('error')
    }
})
$('#example').DataTable( {
"order": [[ 0, "desc" ]],
"lengthMenu": [[10,15, 25, 50, -1], [10,15, 25, 50, "All"]],
"bLengthChange": true,        		
} ); 
$('.dataTables_filter input[type="search"]').attr('placeholder','search'); 
  
if($('#hdnmysitefilter').val() == "true") 
{
$("#ddlSiteList").val($('#ddlSiteList option').eq(1).val());
	//$("#ddlSiteList").text($('#ddlSiteList option:selected').text());
}
debugger;

  
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForStateTicketDetailsBySite(siteid) {  
	
	try
	{
		if(typeof ajax_request4 !== 'undefined')
		{
			ajax_request4.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request4 =  $.ajax({
type: 'GET',
url: './stateopenticketsByAssignedBySite',
data: {'Siteid' :siteid },
success: function(msg){
	
	  
	  var ticketdetaillist = msg.ticketdetaillist;
	  
	  var categories = msg.categories;
	  
	
	 var categoryAxis = "";
	 
	 for (i=0;i<categories.length;i++) {
		 
		 if (i== 0) {
			 categoryAxis = "'"  + categories[i] + "'";
		 }
		 else {
			 categoryAxis = categoryAxis + ',' +  "'"  + categories[i] + "'";
		 }
	 }
	 
	 categoryAxis = eval("[" + categoryAxis + "]");
	
	 var seriesData = ticketdetaillist;

	 
	
	 
	
	 
	 
	 debugger;
	 Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
	 Highcharts.chart('ColumnChart', {
		    chart: {
		        type: 'bar'
		    },
		    title: {
		        text: 'Ticket Summary - Days Lapsed Since  Creation'
		    },
		    credits: {
		        enabled: false
		      },
		    xAxis: {
		    	 categories:  categoryAxis
		        /* categories: ['Hold - Hold', 'Closed - Unfinished', 'Closed - Finished', 'Closed - Closed', 'Open - Unfinsihed','Open - Finsihed','Open - Inprogress','Open - Assigned'] */
		    },
		    yAxis: {
		        min: 0,
		        allowDecimals: false,
		        title: {
		            text: 'Total Tickets '
		        }
		    },
		    legend: {
		            	itemStyle: {
		                	color: 'black',
		                	fontWeight: 'bold',
		                	fontSize: '10px'
		           		 }
		        	},
		    /*legend: {
		        reversed: true
		    },*/
		    plotOptions: {
		        series: {
		            stacking: 'normal',
		            cursor: 'pointer'
		        }
		    },
		    
		    series: seriesData
		  
		});


	 
 



	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForStateTicketDetailsforpiechartBySite(siteid) {  
	
	try
	{
		if(typeof ajax_request1 !== 'undefined')
		{
			ajax_request1.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request1 = $.ajax({
type: 'GET',
url: './stateopenticketforpiechartBySite',
data: {'Siteid' :siteid },
success: function(msg){
	
	  
	  var ticketdetaillist = msg.ticketdetaillist;
	
	  
	
	  var seriesData = ticketdetaillist;
	  
	  var ticketsdata = eval("[" + seriesData + "]");
	  Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
	 
	  Highcharts.chart('Chartcontainer', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha:75,
                beta: 0
            }
        },
        title: {
            text: 'Ticket Summary'
        },
        legend: {
       itemStyle: {
           color: 'black',
           fontWeight: 'bold',
           fontSize: '20px'
       }
   },
        tooltip: {
            pointFormat: '{point.y}'
        },
        credits: {
            enabled: false
          },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        exporting: {
	        menuItemDefinitions: {
	            // Custom definition
	            label: {
	                onclick: function () {
	                    this.renderer.label(
	                        'You just clicked a custom menu item',
	                        100,
	                        100
	                    )
	                    .attr({
	                        fill: '#a4edba',
	                        r: 5,
	                        padding: 10,
	                        zIndex: 10
	                    })
	                    .css({
	                        fontSize: '1.5em'
	                    })
	                    .add();
	                },
	              
	            }
	        },
	        buttons: {
	            contextButton: {
	                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
	            }
	        }
	    },
        series: [{
            type: 'pie',
            name: 'Ticket Summary',
            data: ticketsdata,
            point: {
                events: {
                    click: function () {
                        //alert('Category: ' + this.name + ', value: ' + this.y);
                        var Dtaa = (this.name);
                        
                        if(Dtaa == "Open")
            			{
                      	  Dtaa = 1;
            			}
            			else if(Dtaa == "Closed")
            			{
            				Dtaa = 2;
            			}
            			else if(Dtaa == "Hold")
            			{
            				Dtaa = 3;
            			}
                       /*  alert(Dtaa); */
                        $('#example tbody').empty();
                        $('#example').DataTable().clear().draw();
   		  				$('#example').DataTable().destroy(); 
                        AjaxCallForSiteStateTicketfileterdetails(Dtaa,siteid);

                    }
                }
            }
        }]
    });
	 
	  

	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForAllStateTicketDetailsBySite(siteid) {  
	
	try
	{
		if(typeof ajax_request2 !== 'undefined')
		{
			ajax_request2.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request2 =  $.ajax({
type: 'GET',
url: './AllstateopenticketsBySite',
data: {'Siteid' :siteid },
success: function(msg){
	
	  
	  var ticketdetaillist = msg.ticketdetaillist;
	  
	 var categories = msg.categories;
	 var categoryAxis = "";
	 
	 for (i=0;i<categories.length;i++) {
		 
		 if (i== 0) {
			 categoryAxis = "'"  + categories[i] + "'";
		 }
		 else {
			 categoryAxis = categoryAxis + ',' +  "'"  + categories[i] + "'";
		 }
	 }
	 
	 categoryAxis = eval("[" + categoryAxis + "]");
	
	 var seriesData = ticketdetaillist;

	 
	 Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
	 
	 Highcharts.chart('BarChart', {
		    chart: {
		        type: 'column'
		    },
		    title: {
		        text: 'Ticket State Summary'
		    },
		    credits: {
		        enabled: false
		      },
		    legend: {
		            itemStyle: {
		                color: 'black',
		                fontWeight: 'bold',
		                fontSize: '10px'
		            }
		        },
		    xAxis: {
		        categories: categoryAxis,
		        labels: {
		            style: {
		                color: '#000',
		                fontWeight:'bold'
		            }
		        }
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Tickets Count',
		            style: {
		                color: '#000',
		                fontWeight:'bold'
		            }
		        },
		        stackLabels: {
		            enabled: false,
		            style: {
		                fontWeight: 'bold',
		                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
		            }
		        }
		   
		    },
		    exporting: {
		        menuItemDefinitions: {
		            // Custom definition
		            label: {
		                onclick: function () {
		                    this.renderer.label(
		                        'You just clicked a custom menu item',
		                        100,
		                        100
		                    )
		                    .attr({
		                        fill: '#a4edba',
		                        r: 5,
		                        padding: 10,
		                        zIndex: 10
		                    })
		                    .css({
		                        fontSize: '1.5em'
		                    })
		                    .add();
		                },
		              
		            }
		        },
		        buttons: {
		            contextButton: {
		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		            }
		        }
		    },
		    tooltip: {
		    	headerFormat: '<tabel><tr><td><span style="font-size:10px;fontWeight:600;text-align:center;"><b>{point.x}</b></span></td></tr></tabel><br/>',
		    	pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> <br/>',
		        shared: true
		    },
		    plotOptions: {
                series: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    },
                    cursor:'pointer',
                    point: {
                        events: {
					click: function () {
					
					var category= this.category;
					var status= this.series.name;
					//alert(category);
					//alert(status);
					
					 if(category == "Open")
           			{
						 category = 1;
           			}
           			else if(category == "Closed")
           			{
           				category = 2;
           			}
           			else if(category == "Hold")
           			{
           				category = 3;
           			}
					 
					 if(status == "Created")
						{
						 status = "1";
						}
						else if(status == "Assigned")
						{
							status = "2";
						}
						else if(status == "Inprogress")
						{
							status = "3";
						}
						
						if(status == "Unfinished")
						{
							status = "4";
						}
						else if(status == "Finished")
						{
							status = "5";
						}
						else if(status == "Closed")
						{
							status = "6";
						}
						else if(status == "Hold")
						{
							status = "7";
						}
						
						  $('#example tbody').empty();
						  $('#example').DataTable().clear().draw();
				   		  $('#example').DataTable().destroy(); 
						 AjaxCallForSiteStateandStatusTicketfileterdetails(category,status,siteid);
}
}
}

                   
                }
                
            },
		    
		   
		    series: seriesData
		    	/* [{
		        name: 'Created',
		        data: [5, null, null]
		    }, {
		        name: 'Assigned',
		        data: [2, null, null]
		    }, {
		        name: 'Inprogress',
		        data: [3, null, null]
		    },{
		        name: 'Hold',
		        data: [null, 2, null]
		    },{
		        name: 'Finished',
		        data: [3, null, 5]
		    },{
		        name: 'Unfinished',
		        data: [3, null, 2]
		    }] */
		});

	
	 
	
	 


	 
 




	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}

function AjaxCallForOpenStateTicketDetailsByCustomerSites(customerid) {  
	
	try
	{
		if(typeof ajax_request3 !== 'undefined')
		{
			ajax_request3.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request3 = $.ajax({
type: 'GET',
url: './stateopenticketsBycustomer',
data: {'Customerid' :customerid },
success: function(msg){
	
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	  var ticketdetaillist = msg.ticketdetaillist;
	  
	 var categories = msg.categories;
	 var categoryAxis = "";
	 
	 for (i=0;i<categories.length;i++) {
		 
		 if (i== 0) {
			 categoryAxis = "'"  + categories[i] + "'";
		 }
		 else {
			 categoryAxis = categoryAxis + ',' +  "'"  + categories[i] + "'";
		 }
	 }
	 
	 categoryAxis = eval("[" + categoryAxis + "]");
	
	 var seriesData = ticketdetaillist;
/*  for (i=0;i<ticketdetaillist.length;i++) {
		 
		 if (i== 0) {
			 seriesname = "'"  + ticketdetaillist[i] + "'";
		 }
		 else {
			
		 }
	 }  */
		
	/* 
[{
  name: '30+ days',
  data: [5, 3, 4, 7, 2]
}, {
  name: '16-30 days',
  data: [2, 2, 3, 2, 1]
}, {
  name: '6-15 days',
  data: [3, 4, 4, 2, 5]
},
{
  name: '0-5 days',
  data: [3, 4, 4, 2, 5]
}] */
	 
	
	 
	 var siteticketgriddata = msg.ticketgriddata;
	 var siteList = msg.siteList;
	/*  $( "#ddlSiteList" ).append("<option value='0'>All</option>"); */
	 $.map(siteList, function(val, key) {                                    		 
		 $( "#ddlSiteList" ).append("<option value="+ key +">" + val +"</option>");
		});
	 
	 debugger;
	 
	 Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
Highcharts.chart('DataChart', {
         chart: {
             type: 'bar'
         },
         title: {
             text: 'Ticket Summary - Days Lapsed since  creation'
         },
         credits: {
        	    enabled: false
        	  },
       
         xAxis: {
        	 categories:  categoryAxis,
        	 label: {
        		 style: {
		                color: '#000',
		                fontWeight:'bold'
		            }
        	 }
            // categories: ['Open-Created','Open-Assigned','Open-Inprogress','Open-Unfinished','Open-Finished']
         },
         yAxis: {
             min: 0,
             allowDecimals: false,
             title: {
                 text: 'Total Tickets '
             }
         },
         exporting: {
		        menuItemDefinitions: {
		            // Custom definition
		            label: {
		                onclick: function () {
		                    this.renderer.label(
		                        'You just clicked a custom menu item',
		                        100,
		                        100
		                    )
		                    .attr({
		                        fill: '#a4edba',
		                        r: 5,
		                        padding: 10,
		                        zIndex: 10
		                    })
		                    .css({
		                        fontSize: '1.5em'
		                    })
		                    .add();
		                },
		              
		            }
		        },
		        buttons: {
		            contextButton: {
		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		            }
		        }
		    },
         legend: {
        	itemStyle: {
            	color: 'black',
            	fontWeight: 'bold',
            	fontSize: '10px'
       		 }
    	},
        legend: {
             reversed: true
         },
         plotOptions: {
             series: {
                 stacking: 'normal',
                 cursor: 'pointer',
                 pointWidth: 15,
                 point: {
                     events: {
					click: function () {
					
					var category= this.category;
					
					var res = category.split("-");
					
					var state=res[0];
					var ticketstatus=res[1];
					
				
					
					var status= this.series.name;
					var splitdays = status.replace("days","");
					
					var DaysCount = splitdays.split("-");
					
					var fromday=DaysCount[0];
					var today=DaysCount[1];
					
					
					
					
					
					 if(state == "Open")
        			{
						state = 1;
        			}
        			else if(state == "Closed")
        			{
        				state = 2;
        			}
        			else if(state == "Hold")
        			{
        				state = 3;
        			}
					 
					 if(ticketstatus == "Created")
						{
						ticketstatus = "1";
						}
						else if(ticketstatus == "Assigned")
						{
							ticketstatus = "2";
						}
						else if(ticketstatus == "Inprogress")
						{
							ticketstatus = "3";
						}
						
						if(ticketstatus == "Unfinished")
						{
							ticketstatus = "4";
						}
						else if(ticketstatus == "Finished")
						{
							ticketstatus = "5";
						}
						else if(ticketstatus == "Closed")
						{
							ticketstatus = "6";
						}
						else if(ticketstatus == "Hold")
						{
							ticketstatus = "7";
						}
						
						  $('#example tbody').empty();
						  $('#example').DataTable().clear().draw();
				   		  $('#example').DataTable().destroy(); 
						AjaxCallForCustomerDaylapsedsincecreationTicketfileterdetails(state,ticketstatus,fromday,today,customerid); 
						
}
}
}

             }
         },
        
         series: seriesData
     });

	 
var arraylength=10;
if(siteticketgriddata.length < 10)
 {
 arraylength = siteticketgriddata.length;

	} 
var ticketrows = "";
for(var i=0; i < arraylength; i++)
{
var priority = siteticketgriddata[i].priority;
if(priority == 1)
{
	priority = "Low";
}
else if(priority == 2)
{
	priority = "Medium";
}
else if(priority == 3)
{
	priority = "High";
}

var state = siteticketgriddata[i].state;
if(state == 1)
{
	state = "Open";
}
else if(state == 2)
{
	state = "Closed";
}
else if(state == 3)
{
	state = "Hold";
}

var ticketStatus = siteticketgriddata[i].ticketStatus;

if(ticketStatus == 1)
{
	ticketStatus = "Created";
}
else if(ticketStatus == 2)
{
	ticketStatus = "Assigned";
}
else if(ticketStatus == 3)
{
	ticketStatus = "Inprogress";
}

if(ticketStatus == 4)
{
	ticketStatus = "Unfinished";
}
else if(ticketStatus == 5)
{
	ticketStatus = "Finished";
}
else if(ticketStatus == 6)
{
	ticketStatus = "Closed";
}
else if(ticketStatus == 7)
{
	ticketStatus = "Hold";
}

       

<c:choose>
<c:when test="${access.customerListView == 'visible'}">


var href = '.\\ticketviews' + siteticketgriddata[i].ticketID;

if(i==0)
{
	
	ticketrows = "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(siteticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}
else
{
	ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(siteticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}

</c:when>

<c:otherwise>



if(i==0)
{
	
	ticketrows = "<tr><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}
else
{
	ticketrows = ticketrows + "<tr><td>" + handlenullable(siteticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(siteticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(siteticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(siteticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(siteticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
	
}
	

</c:otherwise>
</c:choose>

//$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketdetaillist[i].ticketCode+'</a>' ,ticketdetaillist[i].siteName,priority,ticketdetaillist[i].createdDateText,ticketdetaillist[i].ticketCategory,ticketdetaillist[i].ticketDetail,ticketdetaillist[i].assignedToWhom,ticketdetaillist[i].scheduledDateText,state,ticketStatus]).draw(false);



} 

$('#example').append(ticketrows);
$('#example').DataTable( {
"order": [[ 0, "desc" ]],
"lengthMenu": [[10,15, 25, 50, -1], [10,15, 25, 50, "All"]],
"bLengthChange": true,        		
} ); 
$('.dataTables_filter input[type="search"]').attr('placeholder','search');



	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}

function AjaxCallForAllStateTicketDetailsByCustomerSites(customerid) {  
	
	try
	{
		if(typeof ajax_request2 !== 'undefined')
		{
			ajax_request2.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request2 =  $.ajax({
type: 'GET',
url: './AllstateopenticketsByCustomer',
data: {'Customerid' :customerid },
success: function(msg){
	
	  
	  var ticketdetaillist = msg.ticketdetaillist;
	  
	 var categories = msg.categories;
	 var categoryAxis = "";
	 
	 for (i=0;i<categories.length;i++) {
		 
		 if (i== 0) {
			 categoryAxis = "'"  + categories[i] + "'";
		 }
		 else {
			 categoryAxis = categoryAxis + ',' +  "'"  + categories[i] + "'";
		 }
	 }
	 
	 categoryAxis = eval("[" + categoryAxis + "]");
	
	 var seriesData = ticketdetaillist;

	 Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
	  
	 Highcharts.chart('BarChart', {
		    chart: {
		        type: 'column'
		    },
		    title: {
		        text: 'Ticket State Summary'
		    },
		    legend: {
		            itemStyle: {
		                color: 'black',
		                fontWeight: 'bold',
		                fontSize: '10px'
		            }
		        },
		    xAxis: {
		        categories: categoryAxis,
		        labels: {
		            style: {
		                color: '#000',
		                fontWeight:'bold'
		            }
		        }
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Tickets Count',
		            style: {
		                color: '#000',
		                fontWeight:'bold'
		            }
		        },
		        stackLabels: {
		            enabled: false,
		            style: {
		                fontWeight: 'bold',
		                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
		            }
		        }
		        
		    },
		    credits: {
		        enabled: false
		      },
		    exporting: {
		        menuItemDefinitions: {
		            // Custom definition
		            label: {
		                onclick: function () {
		                    this.renderer.label(
		                        'You just clicked a custom menu item',
		                        100,
		                        100
		                    )
		                    .attr({
		                        fill: '#a4edba',
		                        r: 5,
		                        padding: 10,
		                        zIndex: 10
		                    })
		                    .css({
		                        fontSize: '1.5em'
		                    })
		                    .add();
		                },
		              
		            }
		        },
		        buttons: {
		            contextButton: {
		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		            }
		        }
		    },
		    tooltip: {
		    	headerFormat: '<tabel><tr><td><span style="font-size:10px;fontWeight:600;align:center;"><b>{point.x}</b></span></td></tr></tabel><br/>',
		    	pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> <br/>',
		        shared: true
		    },
		    plotOptions: {
                series: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    },
                    cursor:'pointer',
                    point: {
                        events: {
					click: function () {
					
					var category= this.category;
					var status= this.series.name;
				/* 	alert(category);
					alert(status); */
					
					 if(category == "Open")
           			{
						 category = 1;
           			}
           			else if(category == "Closed")
           			{
           				category = 2;
           			}
           			else if(category == "Hold")
           			{
           				category = 3;
           			}
					 
					 if(status == "Created")
						{
						 status = "1";
						}
						else if(status == "Assigned")
						{
							status = "2";
						}
						else if(status == "Inprogress")
						{
							status = "3";
						}
						
						if(status == "Unfinished")
						{
							status = "4";
						}
						else if(status == "Finished")
						{
							status = "5";
						}
						else if(status == "Closed")
						{
							status = "6";
						}
						else if(status == "Hold")
						{
							status = "7";
						}
						
						  $('#example tbody').empty();
						  $('#example').DataTable().clear().draw();
				   		  $('#example').DataTable().destroy(); 
						 AjaxCallForCustomerStateandStatusTicketfileterdetails(category,status,customerid);
}
}
}

                   
                }
                
            },
		    series: seriesData
		    	/* [{
		        name: 'Created',
		        data: [5, null, null]
		    }, {
		        name: 'Assigned',
		        data: [2, null, null]
		    }, {
		        name: 'Inprogress',
		        data: [3, null, null]
		    },{
		        name: 'Hold',
		        data: [null, 2, null]
		    },{
		        name: 'Finished',
		        data: [3, null, 5]
		    },{
		        name: 'Unfinished',
		        data: [3, null, 2]
		    }] */
		});

	
	 
	
	 


	 
 




	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForCustomerclickAllStateTicketDetailsByCustomerSites(siteid,customerid) {  
	
	try
	{
		if(typeof ajax_request2 !== 'undefined')
		{
			ajax_request2.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request2 = $.ajax({
type: 'GET',
url: './CustomerclickAllstateopenticketsByCustomer',
data: {'Siteid' : siteid,'Customerid' :customerid },
success: function(msg){
	
	  
	  var ticketdetaillist = msg.ticketdetaillist;
	  
	 var categories = msg.categories;
	 var categoryAxis = "";
	 
	 for (i=0;i<categories.length;i++) {
		 
		 if (i== 0) {
			 categoryAxis = "'"  + categories[i] + "'";
		 }
		 else {
			 categoryAxis = categoryAxis + ',' +  "'"  + categories[i] + "'";
		 }
	 }
	 
	 categoryAxis = eval("[" + categoryAxis + "]");
	
	 var seriesData = ticketdetaillist;

	 
	
	 Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
	 Highcharts.chart('BarChart', {
		    chart: {
		        type: 'column'
		    },
		    title: {
		        text: 'Ticket State Summary'
		    },
		    credits: {
		        enabled: false
		      },
		    legend: {
		            itemStyle: {
		                color: 'black',
		                fontWeight: 'bold',
		                fontSize: '10px'
		            }
		        },
		    xAxis: {
		        categories: categoryAxis,
		        labels: {
		            style: {
		                color: '#000',
		                fontWeight:'bold'
		            }
		        }
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Tickets Count',
		            style: {
		                color: '#000',
		                fontWeight:'bold'
		            }
		        },
		        stackLabels: {
		            enabled: false,
		            style: {
		                fontWeight: 'bold',
		                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
		            }
		        }
		        
		    },
		    exporting: {
		        menuItemDefinitions: {
		            // Custom definition
		            label: {
		                onclick: function () {
		                    this.renderer.label(
		                        'You just clicked a custom menu item',
		                        100,
		                        100
		                    )
		                    .attr({
		                        fill: '#a4edba',
		                        r: 5,
		                        padding: 10,
		                        zIndex: 10
		                    })
		                    .css({
		                        fontSize: '1.5em'
		                    })
		                    .add();
		                },
		              
		            }
		        },
		        buttons: {
		            contextButton: {
		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		            }
		        }
		    },
		    tooltip: {
		    	headerFormat: '<tabel><tr><td><span style="font-size:10px;fontWeight:600;align:center;"><b>{point.x}</b></span></td></tr></tabel><br/>',
		    	pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> <br/>',
		        shared: true
		    },
		    plotOptions: {
              series: {
                  stacking: 'normal',
                  dataLabels: {
                      enabled: true,
                      color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                  },
                  cursor:'pointer',
                  point: {
                      events: {
					click: function () {
					
					var category= this.category;
					var status= this.series.name;
				/* 	alert(category);
					alert(status); */
					
					 if(category == "Open")
         			{
						 category = 1;
         			}
         			else if(category == "Closed")
         			{
         				category = 2;
         			}
         			else if(category == "Hold")
         			{
         				category = 3;
         			}
					 
					 if(status == "Created")
						{
						 status = "1";
						}
						else if(status == "Assigned")
						{
							status = "2";
						}
						else if(status == "Inprogress")
						{
							status = "3";
						}
						
						if(status == "Unfinished")
						{
							status = "4";
						}
						else if(status == "Finished")
						{
							status = "5";
						}
						else if(status == "Closed")
						{
							status = "6";
						}
						else if(status == "Hold")
						{
							status = "7";
						}
						
						  $('#example tbody').empty();
						  $('#example').DataTable().clear().draw();
				   		  $('#example').DataTable().destroy(); 
						/*  AjaxCallForCustomerStateandStatusTicketfileterdetails(category,status,customerid); */
				   		AjaxCallForSiteStateandStatusTicketfileterdetails(category,status,siteid);
}
}
}

                 
              }
              
          },
		    series: seriesData
		    	/* [{
		        name: 'Created',
		        data: [5, null, null]
		    }, {
		        name: 'Assigned',
		        data: [2, null, null]
		    }, {
		        name: 'Inprogress',
		        data: [3, null, null]
		    },{
		        name: 'Hold',
		        data: [null, 2, null]
		    },{
		        name: 'Finished',
		        data: [3, null, 5]
		    },{
		        name: 'Unfinished',
		        data: [3, null, 2]
		    }] */
		});

	
	 
	
	 


	 





	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForStateTicketDetailsByCustomerSites(customerid) {  
	
	try
	{
		if(typeof ajax_request4 !== 'undefined')
		{
			ajax_request4.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request4 = $.ajax({
type: 'GET',
url: './stateopenticketsByAssignedByCustomer',
data: {'Customerid' :customerid },
success: function(msg){
	
	  
	  var ticketdetaillist = msg.ticketdetaillist;
	  
	  var categories = msg.categories;
	  
	
	 var categoryAxis = "";
	 
	 for (i=0;i<categories.length;i++) {
		 
		 if (i== 0) {
			 categoryAxis = "'"  + categories[i] + "'";
		 }
		 else {
			 categoryAxis = categoryAxis + ',' +  "'"  + categories[i] + "'";
		 }
	 }
	 
	 categoryAxis = eval("[" + categoryAxis + "]");
	
	 var seriesData = ticketdetaillist;

	 
	
	
	 
	 
	 debugger;
	 Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
	 Highcharts.chart('ColumnChart', {
		    chart: {
		        type: 'bar'
		    },
		    title: {
		        text: 'Ticket Summary - Days Lapsed since  creation'
		    },
		    xAxis: {
		    	 categories:  categoryAxis
		        /* categories: ['Hold - Hold', 'Closed - Unfinished', 'Closed - Finished', 'Closed - Closed', 'Open - Unfinsihed','Open - Finsihed','Open - Inprogress','Open - Assigned'] */
		    },
		    yAxis: {
		        min: 0,
		        allowDecimals: false,
		        title: {
		            text: 'Total Tickets '
		        }
		    },
		    credits: {
		        enabled: false
		      },
		    legend: {
		            	itemStyle: {
		                	color: 'black',
		                	fontWeight: 'bold',
		                	fontSize: '10px'
		           		 }
		        	},
		    /*legend: {
		        reversed: true
		    },*/
		    plotOptions: {
		        series: {
		            stacking: 'normal',
		            cursor: 'pointer'
		        }
		    },
		    
		    series: seriesData
		  
		});


	 




	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForStateTicketDetailsforpiechartByCustomerSites(customerid) {  
	
	try
	{
		if(typeof ajax_request1 !== 'undefined')
		{
			ajax_request1.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request1 = $.ajax({
type: 'GET',
url: './stateopenticketforpiechartByCustomer',
data: {'Customerid' :customerid },
success: function(msg){
	
	  
	  var ticketdetaillist = msg.ticketdetaillist;
	
	  
	
	  var seriesData = ticketdetaillist;
	  
	  var ticketsdata = eval("[" + seriesData + "]");
	  Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
	 
	  Highcharts.chart('Chartcontainer', {
      chart: {
          type: 'pie',
          options3d: {
              enabled: true,
              alpha:75,
              beta: 0
          }
      },
      title: {
          text: 'Ticket Summary'
      },
      legend: {
     itemStyle: {
         color: 'black',
         fontWeight: 'bold',
         fontSize: '20px'
     }
 },
      tooltip: {
          pointFormat: '{point.y}'
      },
      credits: {
    	    enabled: false
    	  },
      exporting: {
	        menuItemDefinitions: {
	            // Custom definition
	            label: {
	                onclick: function () {
	                    this.renderer.label(
	                        'You just clicked a custom menu item',
	                        100,
	                        100
	                    )
	                    .attr({
	                        fill: '#a4edba',
	                        r: 5,
	                        padding: 10,
	                        zIndex: 10
	                    })
	                    .css({
	                        fontSize: '1.5em'
	                    })
	                    .add();
	                },
	              
	            }
	        },
	        buttons: {
	            contextButton: {
	                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
	            }
	        }
	    },
      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              depth: 35,
              dataLabels: {
                  enabled: true,
                  format: '{point.name}'
              }
          }
      },
      series: [{
          type: 'pie',
          name: 'Ticket Summary',
          data: ticketsdata,
          point: {
              events: {
                  click: function () {
                      //alert('Category: ' + this.name + ', value: ' + this.y);
                      var Dtaa = (this.name);
                      
                      if(Dtaa == "Open")
          			{
                    	  Dtaa = 1;
          			}
          			else if(Dtaa == "Closed")
          			{
          				Dtaa = 2;
          			}
          			else if(Dtaa == "Hold")
          			{
          				Dtaa = 3;
          			}
                     /*  alert(Dtaa); */
                      $('#example tbody').empty();
                      $('#example').DataTable().clear().draw();
   		 			 $('#example').DataTable().destroy(); 
   		 			 debugger;
                      AjaxCallForCustomerStateTicketfileterdetails(Dtaa,customerid);

                  }
              }
          }
      }]
  });
	 
	  

	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForCustomerclickStateTicketDetailsforpiechartByCustomerSites(siteid,customerid) {  
	
	try
	{
		if(typeof ajax_request1 !== 'undefined')
		{
			ajax_request1.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request1 = $.ajax({
type: 'GET',
url: './customerclickstateopenticketforpiechartByCustomer',
data: {'Siteid' :siteid,'Customerid' :customerid },
success: function(msg){
	
	  
	  var ticketdetaillist = msg.ticketdetaillist;
	
	  
	
	  var seriesData = ticketdetaillist;
	  
	  var ticketsdata = eval("[" + seriesData + "]");
	  
	  Highcharts.setOptions({
			lang: {
		  	thousandsSep: ""
		  }
		})
	 
	  Highcharts.chart('Chartcontainer', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha:75,
            beta: 0
        }
    },
    title: {
        text: 'Ticket Summary'
    },
    legend: {
   itemStyle: {
       color: 'black',
       fontWeight: 'bold',
       fontSize: '20px'
   }
},
    tooltip: {
        pointFormat: '{point.y}'
    },
    exporting: {
	        menuItemDefinitions: {
	            // Custom definition
	            label: {
	                onclick: function () {
	                    this.renderer.label(
	                        'You just clicked a custom menu item',
	                        100,
	                        100
	                    )
	                    .attr({
	                        fill: '#a4edba',
	                        r: 5,
	                        padding: 10,
	                        zIndex: 10
	                    })
	                    .css({
	                        fontSize: '1.5em'
	                    })
	                    .add();
	                },
	              
	            }
	        },
	        buttons: {
	            contextButton: {
	                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
	            }
	        }
	    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }
    },
    credits: {
        enabled: false
      },
    series: [{
        type: 'pie',
        name: 'Ticket Summary',
        data: ticketsdata,
        point: {
            events: {
                click: function () {
                    //alert('Category: ' + this.name + ', value: ' + this.y);
                    var Dtaa = (this.name);
                    
                    if(Dtaa == "Open")
        			{
                  	  Dtaa = 1;
        			}
        			else if(Dtaa == "Closed")
        			{
        				Dtaa = 2;
        			}
        			else if(Dtaa == "Hold")
        			{
        				Dtaa = 3;
        			}
                   /*  alert(Dtaa); */
                    $('#example tbody').empty();
                    $('#example').DataTable().clear().draw();
 		 			 $('#example').DataTable().destroy();
 		 			 debugger;
 		 			AjaxCallForSiteStateTicketfileterdetails(Dtaa,siteid);
                   /*  AjaxCallForCustomerStateTicketfileterdetails(Dtaa,siteid); */

                }
            }
        }
    }]
});
	 
	  

	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForOpenTicketfileterdetails(state) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 =  $.ajax({
type: 'POST',
url: './openticketfilter',
data: {'State' :state },
success: function(msg){
	
	  
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 
	 var ticketgriddata = msg.ticketgriddata;
	
	
	 
	 debugger;
	 


	
	 var ticketrows = "";
	 for(var i=0; i < ticketgriddata.length; i++)
	 {
	 var priority = ticketgriddata[i].priority;
	 if(priority == 1)
	 {
	 	priority = "Low";
	 }
	 else if(priority == 2)
	 {
	 	priority = "Medium";
	 }
	 else if(priority == 3)
	 {
	 	priority = "High";
	 }

	 var state = ticketgriddata[i].state;
	 if(state == 1)
	 {
	 	state = "Open";
	 }
	 else if(state == 2)
	 {
	 	state = "Closed";
	 }
	 else if(state == 3)
	 {
	 	state = "Hold";
	 }

	 var ticketStatus = ticketgriddata[i].ticketStatus;

	 if(ticketStatus == 1)
	 {
	 	ticketStatus = "Created";
	 }
	 else if(ticketStatus == 2)
	 {
	 	ticketStatus = "Assigned";
	 }
	 else if(ticketStatus == 3)
	 {
	 	ticketStatus = "Inprogress";
	 }

	 if(ticketStatus == 4)
	 {
	 	ticketStatus = "Unfinished";
	 }
	 else if(ticketStatus == 5)
	 {
	 	ticketStatus = "Finished";
	 }
	 else if(ticketStatus == 6)
	 {
	 	ticketStatus = "Closed";
	 }
	 else if(ticketStatus == 7)
	 {
	 	ticketStatus = "Hold";
	 }

	    
	 
	 
	 <c:choose>
	 <c:when test="${access.customerListView == 'visible'}">
		
		 var href = '.\\ticketviews' + ticketgriddata[i].ticketID;

		 if(i==0)
		 {
			 ticketrows =  "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		
		</c:when>
		
		<c:otherwise>
		
		 if(i==0)
		 {
			 ticketrows =  "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
			
		
		</c:otherwise>
		</c:choose>
	
	 //$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketdetaillist[i].ticketCode+'</a>' ,ticketdetaillist[i].siteName,priority,ticketdetaillist[i].createdDateText,ticketdetaillist[i].ticketCategory,ticketdetaillist[i].ticketDetail,ticketdetaillist[i].assignedToWhom,ticketdetaillist[i].scheduledDateText,state,ticketStatus]).draw(false);



	 } 

	 $('#example').append(ticketrows);
	 $('#example').DataTable( {
	 "order": [[ 0, "desc" ]],
	 "lengthMenu": [[10,15, 25, 50, -1], [10,15, 25, 50, "All"]],
	 "bLengthChange": true,        		
	 } ); 
	 $('.dataTables_filter input[type="search"]').attr('placeholder','search'); 
     
     
    
    
    
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}

function AjaxCallForSiteStateTicketfileterdetails(state,siteid) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 = $.ajax({
type: 'POST',
url: './sitestateticketfilter',
data: {'State' :state,'Siteid':siteid},
success: function(msg){
	
	  
	 
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 var ticketgriddata = msg.ticketgriddata;
	
	
	 
	 debugger;
	 


	 var ticketrows = "";
	 for(var i=0; i < ticketgriddata.length; i++)
	 {
	 var priority = ticketgriddata[i].priority;
	 if(priority == 1)
	 {
	 	priority = "Low";
	 }
	 else if(priority == 2)
	 {
	 	priority = "Medium";
	 }
	 else if(priority == 3)
	 {
	 	priority = "High";
	 }

	 var state = ticketgriddata[i].state;
	 if(state == 1)
	 {
	 	state = "Open";
	 }
	 else if(state == 2)
	 {
	 	state = "Closed";
	 }
	 else if(state == 3)
	 {
	 	state = "Hold";
	 }

	 var ticketStatus = ticketgriddata[i].ticketStatus;

	 if(ticketStatus == 1)
	 {
	 	ticketStatus = "Created";
	 }
	 else if(ticketStatus == 2)
	 {
	 	ticketStatus = "Assigned";
	 }
	 else if(ticketStatus == 3)
	 {
	 	ticketStatus = "Inprogress";
	 }

	 if(ticketStatus == 4)
	 {
	 	ticketStatus = "Unfinished";
	 }
	 else if(ticketStatus == 5)
	 {
	 	ticketStatus = "Finished";
	 }
	 else if(ticketStatus == 6)
	 {
	 	ticketStatus = "Closed";
	 }
	 else if(ticketStatus == 7)
	 {
	 	ticketStatus = "Hold";
	 }

	    
	 
	 <c:choose>
	 <c:when test="${access.customerListView == 'visible'}">
		
		 var href = '.\\ticketviews' + ticketgriddata[i].ticketID;

		 if(i==0)
		 {
		 	
			 ticketrows =  "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		
		</c:when>
		
		<c:otherwise>
		
		 if(i==0)
		 {
		 	
			 ticketrows =  "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
			
		
		</c:otherwise>
		</c:choose>
	
	 //$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketdetaillist[i].ticketCode+'</a>' ,ticketdetaillist[i].siteName,priority,ticketdetaillist[i].createdDateText,ticketdetaillist[i].ticketCategory,ticketdetaillist[i].ticketDetail,ticketdetaillist[i].assignedToWhom,ticketdetaillist[i].scheduledDateText,state,ticketStatus]).draw(false);



	 } 

	 $('#example').append(ticketrows);
	 $('#example').DataTable( {
	 "order": [[ 0, "desc" ]],
	 "lengthMenu": [[10,15, 25, 50, -1], [10,15, 25, 50, "All"]],
	 "bLengthChange": true,        		
	 } ); 
	 $('.dataTables_filter input[type="search"]').attr('placeholder','search'); 
  
  
  
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}

function AjaxCallForCustomerStateTicketfileterdetails(state,customerid) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 = $.ajax({
type: 'POST',
url: './customerstateticketfilter',
data: {'State' :state,'Customerid':customerid},
success: function(msg){
	
	  
	 
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 var ticketgriddata = msg.ticketgriddata;
	
	
	 
	 debugger;
	 


	 var ticketrows = "";
	 for(var i=0; i < ticketgriddata.length; i++)
	 {
	 var priority = ticketgriddata[i].priority;
	 if(priority == 1)
	 {
	 	priority = "Low";
	 }
	 else if(priority == 2)
	 {
	 	priority = "Medium";
	 }
	 else if(priority == 3)
	 {
	 	priority = "High";
	 }

	 var state = ticketgriddata[i].state;
	 if(state == 1)
	 {
	 	state = "Open";
	 }
	 else if(state == 2)
	 {
	 	state = "Closed";
	 }
	 else if(state == 3)
	 {
	 	state = "Hold";
	 }

	 var ticketStatus = ticketgriddata[i].ticketStatus;

	 if(ticketStatus == 1)
	 {
	 	ticketStatus = "Created";
	 }
	 else if(ticketStatus == 2)
	 {
	 	ticketStatus = "Assigned";
	 }
	 else if(ticketStatus == 3)
	 {
	 	ticketStatus = "Inprogress";
	 }

	 if(ticketStatus == 4)
	 {
	 	ticketStatus = "Unfinished";
	 }
	 else if(ticketStatus == 5)
	 {
	 	ticketStatus = "Finished";
	 }
	 else if(ticketStatus == 6)
	 {
	 	ticketStatus = "Closed";
	 }
	 else if(ticketStatus == 7)
	 {
	 	ticketStatus = "Hold";
	 }

	    
	 
	 <c:choose>
	 <c:when test="${access.customerListView == 'visible'}">
		 var href = '.\\ticketviews' + ticketgriddata[i].ticketID;

		 if(i==0)
		 {
		 	
			 ticketrows = "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		
		</c:when>
		
		<c:otherwise>
		
		 
		 if(i==0)
		 {
		 	
			 ticketrows = "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
			
		
		</c:otherwise>
		</c:choose>

	
	 //$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketdetaillist[i].ticketCode+'</a>' ,ticketdetaillist[i].siteName,priority,ticketdetaillist[i].createdDateText,ticketdetaillist[i].ticketCategory,ticketdetaillist[i].ticketDetail,ticketdetaillist[i].assignedToWhom,ticketdetaillist[i].scheduledDateText,state,ticketStatus]).draw(false);



	 } 

	 $('#example').append(ticketrows);
	 $('#example').DataTable( {
	 "order": [[ 0, "desc" ]],
	 "lengthMenu": [[10,15, 25, 50, -1], [10,15, 25, 50, "All"]],
	 "bLengthChange": true,        		
	 } ); 
	 $('.dataTables_filter input[type="search"]').attr('placeholder','search'); 



	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}
   



function AjaxCallForStateandStatusTicketfileterdetails(state,status) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 =  $.ajax({
type: 'POST',
url: './stateandstatusticketfilter',
data: {'State' :state,'Status':status},
success: function(msg){
	
	  
	 
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
      $('#example').DataTable().destroy(); 
	 var ticketgriddata = msg.ticketgriddata;
	
	
	 
	 debugger;
	 


	 var ticketrows = "";
	 for(var i=0; i < ticketgriddata.length; i++)
	 {
	 var priority = ticketgriddata[i].priority;
	 if(priority == 1)
	 {
	 	priority = "Low";
	 }
	 else if(priority == 2)
	 {
	 	priority = "Medium";
	 }
	 else if(priority == 3)
	 {
	 	priority = "High";
	 }

	 var state = ticketgriddata[i].state;
	 if(state == 1)
	 {
	 	state = "Open";
	 }
	 else if(state == 2)
	 {
	 	state = "Closed";
	 }
	 else if(state == 3)
	 {
	 	state = "Hold";
	 }

	 var ticketStatus = ticketgriddata[i].ticketStatus;

	 if(ticketStatus == 1)
	 {
	 	ticketStatus = "Created";
	 }
	 else if(ticketStatus == 2)
	 {
	 	ticketStatus = "Assigned";
	 }
	 else if(ticketStatus == 3)
	 {
	 	ticketStatus = "Inprogress";
	 }

	 if(ticketStatus == 4)
	 {
	 	ticketStatus = "Unfinished";
	 }
	 else if(ticketStatus == 5)
	 {
	 	ticketStatus = "Finished";
	 }
	 else if(ticketStatus == 6)
	 {
	 	ticketStatus = "Closed";
	 }
	 else if(ticketStatus == 7)
	 {
	 	ticketStatus = "Hold";
	 }

	     
	 
	 
	 <c:choose>
	 <c:when test="${access.customerListView == 'visible'}">
		
		 var href = '.\\ticketviews' + ticketgriddata[i].ticketID;

		 if(i==0)
		 {
		 	
			 ticketrows = "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		
		</c:when>
		
		<c:otherwise>
		
		 if(i==0)
		 {
		 	
			 ticketrows = "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		
		</c:otherwise>
		</c:choose>
	
	 //$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketdetaillist[i].ticketCode+'</a>' ,ticketdetaillist[i].siteName,priority,ticketdetaillist[i].createdDateText,ticketdetaillist[i].ticketCategory,ticketdetaillist[i].ticketDetail,ticketdetaillist[i].assignedToWhom,ticketdetaillist[i].scheduledDateText,state,ticketStatus]).draw(false);



	 } 

	 $('#example').append(ticketrows);
	 $('#example').DataTable( {
	 "order": [[ 0, "desc" ]],
	 "lengthMenu": [[10,15, 25, 50, -1], [10,15, 25, 50, "All"]],
	 "bLengthChange": true,        		
	 } ); 
	 $('.dataTables_filter input[type="search"]').attr('placeholder','search'); 
  
  
  
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}

function AjaxCallForSiteStateandStatusTicketfileterdetails(state,status,siteid) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 =  $.ajax({
type: 'POST',
url: './sitestateandstatusticketfilter',
data: {'State' :state,'Status':status,'Siteid':siteid},
success: function(msg){
	
	  
	 
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 var ticketgriddata = msg.ticketgriddata;
	
	
	 
	 debugger;
	 


	 var ticketrows = "";
	 for(var i=0; i < ticketgriddata.length; i++)
	 {
	 var priority = ticketgriddata[i].priority;
	 if(priority == 1)
	 {
	 	priority = "Low";
	 }
	 else if(priority == 2)
	 {
	 	priority = "Medium";
	 }
	 else if(priority == 3)
	 {
	 	priority = "High";
	 }

	 var state = ticketgriddata[i].state;
	 if(state == 1)
	 {
	 	state = "Open";
	 }
	 else if(state == 2)
	 {
	 	state = "Closed";
	 }
	 else if(state == 3)
	 {
	 	state = "Hold";
	 }

	 var ticketStatus = ticketgriddata[i].ticketStatus;

	 if(ticketStatus == 1)
	 {
	 	ticketStatus = "Created";
	 }
	 else if(ticketStatus == 2)
	 {
	 	ticketStatus = "Assigned";
	 }
	 else if(ticketStatus == 3)
	 {
	 	ticketStatus = "Inprogress";
	 }

	 if(ticketStatus == 4)
	 {
	 	ticketStatus = "Unfinished";
	 }
	 else if(ticketStatus == 5)
	 {
	 	ticketStatus = "Finished";
	 }
	 else if(ticketStatus == 6)
	 {
	 	ticketStatus = "Closed";
	 }
	 else if(ticketStatus == 7)
	 {
	 	ticketStatus = "Hold";
	 }

	    
	 
	 
		<c:choose>
		<c:when test="${access.customerListView == 'visible'} ">
		
		 var href = '.\\ticketviews' + ticketgriddata[i].ticketID;

		 if(i==0)
		 {
		 	
			 ticketrows = "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		
		</c:when>
		
		<c:otherwise>
		
		
		 if(i==0)
		 {
		 	
			 ticketrows = "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
			
		
		</c:otherwise>
		</c:choose>

	
	 //$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketdetaillist[i].ticketCode+'</a>' ,ticketdetaillist[i].siteName,priority,ticketdetaillist[i].createdDateText,ticketdetaillist[i].ticketCategory,ticketdetaillist[i].ticketDetail,ticketdetaillist[i].assignedToWhom,ticketdetaillist[i].scheduledDateText,state,ticketStatus]).draw(false);



	 } 

	 $('#example').append(ticketrows);
	 $('#example').DataTable( {
	 "order": [[ 0, "desc" ]],
	 "lengthMenu": [[10,15, 25, 50, -1], [10,15, 25, 50, "All"]],
	 "bLengthChange": true,        		
	 } ); 
	 $('.dataTables_filter input[type="search"]').attr('placeholder','search');

	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForCustomerStateandStatusTicketfileterdetails(state,status,customerid) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 = $.ajax({
type: 'POST',
url: './customerstateandstatusticketfilter',
data: {'State' :state,'Status':status,'Customerid':customerid},
success: function(msg){
	
	  
	 
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 var ticketgriddata = msg.ticketgriddata;
	
	
	 
	 debugger;
	 

	 var ticketrows = "";
	 for(var i=0; i < ticketgriddata.length; i++)
	 {
	 var priority = ticketgriddata[i].priority;
	 if(priority == 1)
	 {
	 	priority = "Low";
	 }
	 else if(priority == 2)
	 {
	 	priority = "Medium";
	 }
	 else if(priority == 3)
	 {
	 	priority = "High";
	 }

	 var state = ticketgriddata[i].state;
	 if(state == 1)
	 {
	 	state = "Open";
	 }
	 else if(state == 2)
	 {
	 	state = "Closed";
	 }
	 else if(state == 3)
	 {
	 	state = "Hold";
	 }

	 var ticketStatus = ticketgriddata[i].ticketStatus;

	 if(ticketStatus == 1)
	 {
	 	ticketStatus = "Created";
	 }
	 else if(ticketStatus == 2)
	 {
	 	ticketStatus = "Assigned";
	 }
	 else if(ticketStatus == 3)
	 {
	 	ticketStatus = "Inprogress";
	 }

	 if(ticketStatus == 4)
	 {
	 	ticketStatus = "Unfinished";
	 }
	 else if(ticketStatus == 5)
	 {
	 	ticketStatus = "Finished";
	 }
	 else if(ticketStatus == 6)
	 {
	 	ticketStatus = "Closed";
	 }
	 else if(ticketStatus == 7)
	 {
	 	ticketStatus = "Hold";
	 }

	   
	 
	 
		<c:choose>
		<c:when test="${access.customerListView == 'visible'} ">
		
		 var href = '.\\ticketviews' + ticketgriddata[i].ticketID;

		 if(i==0)
		 {
			 ticketrows =  "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		
		</c:when>
		
		<c:otherwise>
		
		
		 if(i==0)
		 {
			 ticketrows =  "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
			
		
		</c:otherwise>
		</c:choose>


	 //$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketdetaillist[i].ticketCode+'</a>' ,ticketdetaillist[i].siteName,priority,ticketdetaillist[i].createdDateText,ticketdetaillist[i].ticketCategory,ticketdetaillist[i].ticketDetail,ticketdetaillist[i].assignedToWhom,ticketdetaillist[i].scheduledDateText,state,ticketStatus]).draw(false);



	 } 

	 $('#example').append(ticketrows);
	 $('#example').DataTable( {
	 "order": [[ 0, "desc" ]],
	 "lengthMenu": [[10,15, 25, 50, -1], [10,15, 25, 50, "All"]],
	 "bLengthChange": true,        		
	 } ); 
	 $('.dataTables_filter input[type="search"]').attr('placeholder','search');


	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}



function AjaxCallForDaylapsedsincecreationTicketfileterdetails(state,status,fromday,today) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 =  $.ajax({
type: 'POST',
url: './daylapsedsincecreationfilterTickets',
data: {'State' :state,'Status':status,'Fromday':fromday,'Today':today},
success: function(msg){
	
	  
	 
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 var ticketgriddata = msg.ticketgriddata;
	
	
	 
	 debugger;
	 


	 var ticketrows = "";
	 for(var i=0; i < ticketgriddata.length; i++)
	 {
	 var priority = ticketgriddata[i].priority;
	 if(priority == 1)
	 {
	 	priority = "Low";
	 }
	 else if(priority == 2)
	 {
	 	priority = "Medium";
	 }
	 else if(priority == 3)
	 {
	 	priority = "High";
	 }

	 var state = ticketgriddata[i].state;
	 if(state == 1)
	 {
	 	state = "Open";
	 }
	 else if(state == 2)
	 {
	 	state = "Closed";
	 }
	 else if(state == 3)
	 {
	 	state = "Hold";
	 }

	 var ticketStatus = ticketgriddata[i].ticketStatus;

	 if(ticketStatus == 1)
	 {
	 	ticketStatus = "Created";
	 }
	 else if(ticketStatus == 2)
	 {
	 	ticketStatus = "Assigned";
	 }
	 else if(ticketStatus == 3)
	 {
	 	ticketStatus = "Inprogress";
	 }

	 if(ticketStatus == 4)
	 {
	 	ticketStatus = "Unfinished";
	 }
	 else if(ticketStatus == 5)
	 {
	 	ticketStatus = "Finished";
	 }
	 else if(ticketStatus == 6)
	 {
	 	ticketStatus = "Closed";
	 }
	 else if(ticketStatus == 7)
	 {
	 	ticketStatus = "Hold";
	 }

	 
	 
		<c:choose>
		<c:when test="${access.customerListView == 'visible'}">
		
		 var href = '.\\ticketviews' + ticketgriddata[i].ticketID;

		 if(i==0)
		 {
		 	
			 ticketrows = "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		
		</c:when>
		
		<c:otherwise>
		
		
		 if(i==0)
		 {
		 	
			 ticketrows = "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
			
		
		</c:otherwise>
		</c:choose>

	          
	
	 //$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketdetaillist[i].ticketCode+'</a>' ,ticketdetaillist[i].siteName,priority,ticketdetaillist[i].createdDateText,ticketdetaillist[i].ticketCategory,ticketdetaillist[i].ticketDetail,ticketdetaillist[i].assignedToWhom,ticketdetaillist[i].scheduledDateText,state,ticketStatus]).draw(false);



	 } 

	 $('#example').append(ticketrows);
	 $('#example').DataTable( {
	 "order": [[ 0, "desc" ]],
	 "lengthMenu": [[10,15, 25, 50, -1], [10,15, 25, 50, "All"]],
	 "bLengthChange": true,        		
	 } ); 
	 $('.dataTables_filter input[type="search"]').attr('placeholder','search');


	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}
 
 
 
 
function AjaxCallForSiteDaylapsedsincecreationTicketfileterdetails(state,status,fromday,today,siteid) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 =  $.ajax({
type: 'POST',
url: './sitedaylapsedsincecreationfilterTickets',
data: {'State' :state,'Status':status,'Fromday':fromday,'Today':today,'Siteid':siteid},
success: function(msg){
	
	  
	 
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 var ticketgriddata = msg.ticketgriddata;
	
	
	 
	 debugger;
	 


	 var ticketrows = "";
	 for(var i=0; i < ticketgriddata.length; i++)
	 {
	 var priority = ticketgriddata[i].priority;
	 if(priority == 1)
	 {
	 	priority = "Low";
	 }
	 else if(priority == 2)
	 {
	 	priority = "Medium";
	 }
	 else if(priority == 3)
	 {
	 	priority = "High";
	 }

	 var state = ticketgriddata[i].state;
	 if(state == 1)
	 {
	 	state = "Open";
	 }
	 else if(state == 2)
	 {
	 	state = "Closed";
	 }
	 else if(state == 3)
	 {
	 	state = "Hold";
	 }

	 var ticketStatus = ticketgriddata[i].ticketStatus;

	 if(ticketStatus == 1)
	 {
	 	ticketStatus = "Created";
	 }
	 else if(ticketStatus == 2)
	 {
	 	ticketStatus = "Assigned";
	 }
	 else if(ticketStatus == 3)
	 {
	 	ticketStatus = "Inprogress";
	 }

	 if(ticketStatus == 4)
	 {
	 	ticketStatus = "Unfinished";
	 }
	 else if(ticketStatus == 5)
	 {
	 	ticketStatus = "Finished";
	 }
	 else if(ticketStatus == 6)
	 {
	 	ticketStatus = "Closed";
	 }
	 else if(ticketStatus == 7)
	 {
	 	ticketStatus = "Hold";
	 }

	 
	 
		<c:choose>
		<c:when test="${access.customerListView == 'visible'}">
		 var href = '.\\ticketviews' + ticketgriddata[i].ticketID;

		 if(i==0)
		 {
		 	
			 ticketrows = "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		
		</c:when>
		
		<c:otherwise>
		
		 if(i==0)
		 {
		 	
			 ticketrows = "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 }
			
		
		</c:otherwise>
		</c:choose>

	          
	
	 //$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketdetaillist[i].ticketCode+'</a>' ,ticketdetaillist[i].siteName,priority,ticketdetaillist[i].createdDateText,ticketdetaillist[i].ticketCategory,ticketdetaillist[i].ticketDetail,ticketdetaillist[i].assignedToWhom,ticketdetaillist[i].scheduledDateText,state,ticketStatus]).draw(false);



	 } 

	 $('#example').append(ticketrows);
	 $('#example').DataTable( {
	 "order": [[ 0, "desc" ]],
	 "lengthMenu": [[10,15, 25, 50, -1], [10,15, 25, 50, "All"]],
	 "bLengthChange": true,        		
	 } ); 
	 $('.dataTables_filter input[type="search"]').attr('placeholder','search');

	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForCustomerDaylapsedsincecreationTicketfileterdetails(state,status,fromday,today,customerid) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 = $.ajax({
type: 'POST',
url: './customerdaylapsedsincecreationfilterTickets',
data: {'State' :state,'Status':status,'Fromday':fromday,'Today':today,'Customerid':customerid},
success: function(msg){
	
	  
	 
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 var ticketgriddata = msg.ticketgriddata;
	
	
	 
	 debugger;
	 


	 var ticketrows = "";
	 for(var i=0; i < ticketgriddata.length; i++)
	 {
	 var priority = ticketgriddata[i].priority;
	 if(priority == 1)
	 {
	 	priority = "Low";
	 }
	 else if(priority == 2)
	 {
	 	priority = "Medium";
	 }
	 else if(priority == 3)
	 {
	 	priority = "High";
	 }

	 var state = ticketgriddata[i].state;
	 if(state == 1)
	 {
	 	state = "Open";
	 }
	 else if(state == 2)
	 {
	 	state = "Closed";
	 }
	 else if(state == 3)
	 {
	 	state = "Hold";
	 }

	 var ticketStatus = ticketgriddata[i].ticketStatus;

	 if(ticketStatus == 1)
	 {
	 	ticketStatus = "Created";
	 }
	 else if(ticketStatus == 2)
	 {
	 	ticketStatus = "Assigned";
	 }
	 else if(ticketStatus == 3)
	 {
	 	ticketStatus = "Inprogress";
	 }

	 if(ticketStatus == 4)
	 {
	 	ticketStatus = "Unfinished";
	 }
	 else if(ticketStatus == 5)
	 {
	 	ticketStatus = "Finished";
	 }
	 else if(ticketStatus == 6)
	 {
	 	ticketStatus = "Closed";
	 }
	 else if(ticketStatus == 7)
	 {
	 	ticketStatus = "Hold";
	 }

	 
	 
		<c:choose>
		<c:when test="${access.customerListView == 'visible'}">
			 var href = '.\\ticketviews' + ticketgriddata[i].ticketID;

		 if(i==0)
		 {
			 ticketrows = "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
			
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketgriddata[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 	
		 }
		</c:when>
		
		<c:otherwise>
		
	
		 if(i==0)
		 {
			 ticketrows = "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
			
		 }
		 else
		 {
			 ticketrows = ticketrows + "<tr><td>" + handlenullable(ticketgriddata[i].siteName)+"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketgriddata[i].createdDateText) +"</td><td>" + handlenullable(ticketgriddata[i].ticketCategory) +"</td><td>" + handlenullable(ticketgriddata[i].ticketDetail) +"</td><td>" + handlenullable(ticketgriddata[i].assignedToWhom) +"</td><td>" + handlenullable(ticketgriddata[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
		 	
		 }
			
		
		</c:otherwise>
		</c:choose>

	          
	
	 //$('#example').DataTable().row.add(["<a href='"+ href +"'>" + ticketdetaillist[i].ticketCode+'</a>' ,ticketdetaillist[i].siteName,priority,ticketdetaillist[i].createdDateText,ticketdetaillist[i].ticketCategory,ticketdetaillist[i].ticketDetail,ticketdetaillist[i].assignedToWhom,ticketdetaillist[i].scheduledDateText,state,ticketStatus]).draw(false);



	 } 

	 $('#example').append(ticketrows);
	 $('#example').DataTable( {
	 "order": [[ 0, "desc" ]],
	 "lengthMenu": [[10,15, 25, 50, -1], [10,15, 25, 50, "All"]],
	 "bLengthChange": true,        		
	 } ); 
	 $('.dataTables_filter input[type="search"]').attr('placeholder','search');

	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


$(document).ready(function() {
/* 	alert($(window).height());
	alert($(window).width()); */
	 
	
	$('#ddlSite').dropdown('clear');
	$('#ddlType').dropdown('clear');
	$('#ddlCategory').dropdown('clear');
	$('#ddlPriority').dropdown('clear');
	$('#txtSubject').val('');
	$('#txtTktdescription').val('');
	
	
        	
        
        	  if($('#hdnmysitefilter').val() == "true") 
     		 {
        		 /*   $('#example').DataTable().clear().draw(); */
        		   AjaxCallForStateTicketDetailsforpiechartBySite($('#hdnmysiteid').val());
        		   AjaxCallForAllStateTicketDetailsBySite($('#hdnmysiteid').val());
        		  AjaxCallForOpenStateTicketDetailsBySite($('#hdnmysiteid').val());
          	    
                /* 	AjaxCallForStateTicketDetailsBySite($('#hdnmysiteid').val()); */
                	
        			
        		
    			
     		 }
        	  else if($('#hdnmycustomerfilter').val() == "true") 
      		 {
        		/*   $('#example').DataTable().clear().draw(); */
        		  AjaxCallForStateTicketDetailsforpiechartByCustomerSites($('#hdnmycustomerid').val());
        		  AjaxCallForAllStateTicketDetailsByCustomerSites($('#hdnmycustomerid').val());
        			AjaxCallForOpenStateTicketDetailsByCustomerSites($('#hdnmycustomerid').val());
        			
        		/* 	AjaxCallForStateTicketDetailsByCustomerSites($('#hdnmycustomerid').val()); */
        			
      		 }
        	
        	  else{
        			AjaxCallForStateTicketDetailsforpiechart();  
        			AjaxCallForAllStateTicketDetails();
        		 	 AjaxCallForOpenStateTicketDetails();
             /*  	AjaxCallForStateTicketDetails();
 */              
              	
        	  }
        	  
        	  
        	  
        	  
        	
   			
   		
 		 
        	
        	
        	
        	$("#ddlSiteList").change(function(){
        	    var sitevalue = $("#ddlSiteList option:selected").val();
        	    $('#example tbody').empty();
        	    $('.search').val('');
        	    $('#example').DataTable().clear().draw();
         		$('#example').DataTable().destroy(); 
        	 	
        	 	
        	 	if($('#hdnmysitefilter').val() == "true") {
        	 		
             		AjaxCallForStateTicketDetailsforpiechartBySite(sitevalue);
            	 	AjaxCallForAllStateTicketDetailsBySite(sitevalue);
        	 		AjaxCallForOpenStateTicketDetailsBySiteByClickSitepage(sitevalue);
        	 		
        	 		$("#ddlSiteList").val($('#ddlSiteList option').eq(1).val());
        	 		$("#ddlSiteList").text($('#ddlSiteList option:selected').text());
        	 	}
        	 	 else if($('#hdnmycustomerfilter').val() == "true"){
        	 		AjaxCallForOpenStateTicketDetailsBySiteByClickCustomerpage(sitevalue,$('#hdnmycustomerid').val());
        	 		AjaxCallForCustomerclickAllStateTicketDetailsByCustomerSites(sitevalue,$('#hdnmycustomerid').val());
        	 		AjaxCallForCustomerclickStateTicketDetailsforpiechartByCustomerSites(sitevalue,$('#hdnmycustomerid').val());
        	 	}
        	 	 else{
              		AjaxCallForStateTicketDetailsforpiechartBySite(sitevalue);
            	 	AjaxCallForAllStateTicketDetailsBySite(sitevalue);
        	 		AjaxCallForOpenStateTicketDetailsBySiteByClick(sitevalue);
        	 	 }
        	 	
        	 	
         
             
        	    //alert("You have selected the SiteID - " + sitevalue);
        	});
        	
        	
        	
        	
   	     $('.clear').click(function(){
			    $('.search').val("");
			});
   	     
   	     $("#txtFromDate").datepicker({                  	
         	dateFormat:'dd/mm/yy',
         	maxDate: new Date(),
         	maxDate: "0",
                onSelect: function(selected) {
                  $("#txtToDate").datepicker("option","minDate", selected)                 
                }
            });
         $("#txtToDate").datepicker({         	
         	dateFormat:'dd/mm/yy',
         	maxDate: new Date(),
         	maxDate: "0",
         	onSelect: function(selected) {                                
             $("#txtFromDate").datepicker("option","maxDate", selected)      
          }                                
   	 }); 

        
        	
        	 
        	 $.fn.dataTable.moment('DD-MM-YYYY');
        	 $.fn.dataTable.moment('DD-MM-YYYY HH:mm');
        	  
     /*     $('#example').DataTable( {
        		 "order": [[ 0, "desc" ]],
        	 	//  "order": [[ 3, "desc" ]], 
        	//	 "order": [[ 2, "desc" ]], 
         		 "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
        		 "bLengthChange": true,        		
        		 } );  */
        	
       	 $("#searchSelect").change(function() {
             var value = $('#searchSelect option:selected').val();
             var uid =  $('#hdneampmuserid').val();
           redirectbysearch(value,uid);
        }); 
       	 $('.dataTables_filter input[type="search"]').attr('placeholder','search');


            if($(window).width() < 767)
				{
				   $('.card').removeClass("slide-left");
				   $('.card').removeClass("slide-right");
				   $('.card').removeClass("slide-top");
				   $('.card').removeClass("slide-bottom");
				   
				   
				   
				   $('.carousel-inner').append('<div class="item active" style="height: 450px;background: #FFF;"><div class="carousel-caption"><div class="col-md-12 padd-0"><div id="Chartcontainer" style="height: 330px"></div><p class="paragrapth">KPI Accumulated for the Last 60 Days</p></div></div></div>');
				   $('.carousel-inner').append('<div class="item" style="height: 450px;background: #FFF;"><div class="carousel-caption"><div class="col-md-12 padd-0"><div id="BarChart" style="height: 330px; margin: 0 auto"></div><p class="paragrapth">KPI Accumulated for the Last 60 Days</p></div></div></div>');
				   $('.carousel-inner').append('<div class="item" style="height: 450px;"><div class="carousel-caption"><div class="col-md-11"><div id="DataChart" style="height: 400px"></div><p class="paragrapth">KPI Accumulated for Last 60 Days</p></div></div></div>');
				   
				}
            else
            	{
            		$('.carousel-inner').append('<div class="item active" style="height: 450px;background: #FFF;"><div class="carousel-caption"><div class="col-md-12 padd-0"><div class="col-md-5 padd-0"><div id="Chartcontainer" style="height: 330px"></div><p class="paragrapth">KPI Accumulated for the Last 60 Days</p></div><div class="col-md-6 padd-0" ><div id="BarChart" style="height: 330px; margin: 0 auto"></div><p class="paragrapth">KPI Accumulated for the Last 60 Days</p></div></div></div></div>');
				   	$('.carousel-inner').append('<div class="item" style="height: 450px;"><div class="carousel-caption"><div class="col-md-11"><div id="DataChart" style="height: 400px"></div><p class="paragrapth">KPI Accumulated for the Last 60 Days</p></div></div></div>');
				}
           

               $('.close').click(function(){
                   $('#tktCreation').hide();
                   $('.clear').click();
                   $('.category').dropdown();
                   $('.SiteNames').dropdown();
               });
			$('.clear').click(function(){
			    $('.search').val("");
			});
			
			$('#ddlType').change(function(){              
                $('.category').dropdown('clear')
               });


			$('body').click(function(){
            	$('#builder').removeClass('open'); 
         	 });
        });
      </script>
   
   
     
<script type="text/javascript">
         $(window).load(function(){
        	/*  $('.ui.dropdown').dropdown({forceSelection:false});
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
          */
         
         $('.ui.dropdown').dropdown({
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: true, 
             showOnFocus: true,
             on: "click" 
           });

		$('.ui.dropdown.oveallsearch').dropdown({
             onChange: function (value, text, $selectedItem) {                
                var uid =  $('#hdneampmuserid').val();
                redirectbysearchvalue(value,uid);
             },
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: false, 
             showOnFocus: true,
             on: "click" 
           });
		
		
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );//]]> 

            var validation  = {
                    txtSubject: {
                               identifier: 'txtSubject',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               },
                               {
                                   type: 'maxLength[50]',
                                   prompt: 'Please enter a value'
                                 }]
                             },
                              ddlSite: {
                               identifier: 'ddlSite',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                             },
                            ddlType: {
                                  identifier: 'ddlType',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                            ddlCategory: {
                               identifier: 'ddlCategory',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                             ddlPriority: {
                               identifier: 'ddlPriority',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please select a dropdown value'
                               }]
                             },
                             description: {
                               identifier: 'description',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please Enter Description'
                               }]
                             }
             };
               var settings = {
                 onFailure:function(){
                     return false;
                   }, 
                 onSuccess:function(){    
                   $('#btnCreate').hide();
                   $('#btnCreateDummy').show()
                   //$('#btnReset').attr("disabled", "disabled");          
                   }};           
               $('.ui.form.validation-form').form(validation,settings);

         })
         
      </script>
<style type="text/css">
.dropdown-menu-right {
	left: -70px !important;
}
/* .ui.search.dropdown .menu {
    max-height: 12.02857143rem;
}
.ui.search.dropdown > input.search {
    background: none transparent !important;
    border: none !important;
    box-shadow: none !important;
    cursor: text;
    top: 0em;
    left: -2px;
    width: 100%;
    outline: none;
    -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
    padding: inherit;
} */
 	div.dataTables_wrapper div.dataTables_length select {
			    width: 50px;
			    display: inline-block;
			    margin-left: 3px;
			    margin-right: 4px;
			}
			
	/* 		.ui.form textarea:not([rows]) {
    height: 4em;
    min-height: 4em;
    max-height: 24em;
} */
.ui.icon.input > i.icon:before, .ui.icon.input > i.icon:after {
    left: 0;
    position: absolute;
    text-align: center;
    top: 36%;
    width: 100%;
}


.input-sm, .form-horizontal .form-group-sm .form-control {
    font-size: 13px;
    min-height: 25px;
    height: 32px;
    padding: 8px 9px;
}
  
#example td a {
	color: #337ab7;
	font-weight: bold;
}
.dt-button-background {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 0 !important;
}

.tbh-width {
	width: 150px;
	padding-right: 30px !important;
}

.dt-buttons {
	margin-left: 50px;
}

.buttons-collection {
	height: 28px;
	padding: 3px;
	/*  z-index:9999; */
}


.buttons-excel {
	background: rgba(36, 95, 68, 0.87);
	color: #FFF;
}

.buttons-pdf {
	background: rgb(98, 165, 96);
	color: #FFF;
}

.dataTables_filter {
	float: right;
}

.dataTables_length {
	float: left;
}


.ui.selection.dropdown {
    width: 100%;
    margin-top: 0.1em;
   /*  height: 2.3em; */
}

.ui.selection.dropdown .menu {
    width: 100%;
    white-space: normal !important;
}

.ui.selection.dropdown .menu .item:first-child {
    border-top: 1px solid #ddd !important;
    min-height: 2.8em;
}

.ui.selection.dropdown .text {

    text-overflow: ellipsis;
    white-space: nowrap;
    width: 90%;
}
.oveallSearch .text {
    /* color: #eae9e9 !important; */
    color: #a49c9c !important;
}

.ui.selection.dropdown .icon {
    text-align: right;
    margin-left: 7px !important;
}

</style>

</head>
<body class="fixed-header" style="    padding-right: 0px !important;">

	<input type="hidden" value="${access.userID}" id="hdneampmuserid">
	
	<input type="hidden" value="${access.mySiteFilter}" id="hdnmysitefilter">
	
	<input type="hidden" value="${access.mySiteID}" id="hdnmysiteid">
	
	<input type="hidden" value="${access.myCustomerFilter}" id="hdnmycustomerfilter">
	
	<input type="hidden" value="${access.myCustomerID}" id="hdnmycustomerid">


	<nav class="page-sidebar" data-pages="sidebar">

		<div class="sidebar-header">
			<a href="./dashboard"><img src="resources/img/logo01.png" class="eira-logo"
				alt="logo"
				title="Electronically Assisted Monitoring and Portfolio Management"></a>
			<i class="fa fa-bars bars"></i>
		</div>

		<div class="sidebar-menu">


			
<c:if test="${not empty access}">
				<ul class="menu-items">



					<c:if test="${access.dashboard == 'visible'}">

						<li class="" style="margin-top: 2px;"><a
							href="./dashboard" class="detailed"> <span
								class="title">Dashboard</span>
						</a> <span class="icon-thumbnail"><i class="fa fa-tachometer"></i></span>
						</li>
					</c:if>

					<c:if test="${access.overview == 'visible'}">
						<li><a href="#"><span class="title">Overview</span></a> <span
							class="icon-thumbnail"><i class="fa fa-list-alt"></i></span></li>
					</c:if>

					<c:if test="${access.systemMonitoring == 'visible'}">
						<li><a href="#"><span class="title">System
									Monitoring</span></a> <span class="icon-thumbnail"><i
								class="fa fa-desktop"></i></span></li>
					</c:if>

					<c:if test="${access.visualization == 'visible'}">
						<li><a href="javascript:;"><span class="title">Visualization</span>
								<span class=" arrow"></span></a> <span class="icon-thumbnail">  <i class="fa fa-desktop" aria-hidden="true"></i></span>
							<ul class="sub-menu">

								<c:if test="${access.customerListView == 'visible'}">
									<li><a href="./customerlist">Customer View</a>
										<span class="icon-thumbnail"><i class="fa fa-eye"></i></span>
									</li>
								</c:if>



								<li><a href="./sitelist">Site View</a> <span
									class="icon-thumbnail"><i class="fa fa-map-marker"></i></span>
								</li>
							<!-- 	<li><a href="./equipmentlist">Equipment View</a>
									<span class="icon-thumbnail"><i class="fa fa-cog"></i></span></li> -->
							</ul></li>

					</c:if>

					<c:if test="${access.analytics == 'visible'}">
						<li><a href="#"> <span class="title">Analytics</span>
						</a> <span class="icon-thumbnail"><i class="fa fa-pie-chart"></i></span>
						</li>
					</c:if>

					<c:if test="${access.portfolioManagement == 'visible'}">
						<li><a href="#"> <span class="title">Portfolio
									Manager</span>
						</a> <span class="icon-thumbnail"><i class="fa fa-briefcase"></i></span>
						</li>
					</c:if>

				
						<li class="active"><a href="javascript:;"><span class="title">Operation
									& Maintenance</span> <span class=" arrow"></span></a> <span class="icon-thumbnail" style="padding-top:5px !important;"><img src="resources/img/maintance.png"></span>
							<ul class="sub-menu">
 
																	<li class="active"><a href="./ticketskpi">Tickets</a> <span class="icon-thumbnail"><i
											class="fa fa-ticket"></i></span></li>
										
									 					<li>
                        	<a href="./eventskpi">Events</a>
                        	<span class="icon-thumbnail"><i class="fa fa-calendar"></i></span>
                     	</li>
                     	
							</ul></li>





				




					<c:if test="${access.forcasting == 'visible'}">
						<li><a href="#"><span class="title">Forecasting</span></a> <span
							class="icon-thumbnail"><i class="fa fa-desktop"></i></span></li>
					</c:if>
					
						<c:if test="${access.analysis == 'visible'}">
          		<li>
                  <a href="./analysis"><span class="title">Analytics</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-area-chart"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.configuration == 'visible'}">
            
               <li>
                  <a href="javascript:;"><span class="title">Configuration</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="pg-tables"></i></span>
                  <ul class="sub-menu">
                     
                     <li>
                        <a href="./customers">Customer Config.</a>
                        <span class="icon-thumbnail">CU</span>
                     </li>
                     
                     <li>
                        <a href="./sites">Site Config.</a>
                        <span class="icon-thumbnail">SI</span>
                     </li>
                     
                      <li>
                        <a href="./equipmentcategories">Equipment Category Config.</a>
                        <span class="icon-thumbnail">EC</span>
                     </li>
                     
                      <li>
                        <a href="./equipmenttypes">Equipment Type Config.</a>
                        <span class="icon-thumbnail">ET</span>
                     </li>
                     
                     <li>
                        <a href="./equipments">Equipment Config.</a>
                        <span class="icon-thumbnail">EQ</span>
                     </li>
                     
                     
                     <!--  <li>
                        <a href="./parameterstandards">Standard Parameter</a>
                        <span class="icon-thumbnail">SP</span>
                     </li> -->
                     
                      <li>
                        <a href="./customermaps">CustomerMap Config.</a>
                        <span class="icon-thumbnail">CM</span>
                     </li>
                     
                   <!--   <li>
                        <a href="#">Data Logger Config.</a>
                        <span class="icon-thumbnail">DL</span>
                     </li>
                     
                     <li>
                        <a href="#">Equ-Attrib Config.</a>
                        <span class="icon-thumbnail">EA</span>
                     </li>
                     
                      <li>
                        <a href="#">Configuration Loader</a>
                        <span class="icon-thumbnail">CL</span>
                     </li>
                     
                     <li>
                        <a href="./activities">Activity Config.</a>
                        <span class="icon-thumbnail">AC</span>
                     </li>
                     
                      <li>
                        <a href="./timezones">Timezone Config.</a>
                        <span class="icon-thumbnail">TZ</span>
                     </li>
                     
                      <li>
                        <a href="./currencies">Currency Config.</a>
                        <span class="icon-thumbnail">CY</span>
                     </li>
                     
                      <li>
                        <a href="./unitofmeasurements">Unit Measurement Config.</a>
                        <span class="icon-thumbnail">UM</span>
                     </li>
                     
                     
                     <li>
                        <a href="./countryregions">Country Region Config.</a>
                        <span class="icon-thumbnail">CR</span>
                     </li>
                     
                     <li>
                        <a href="./countries">Country Config.</a>
                        <span class="icon-thumbnail">CO</span>
                     </li>
                     
                     <li>
                        <a href="./states">State Config.</a>
                        <span class="icon-thumbnail">SE</span>
                     </li>
                     
                     <li>
                        <a href="./customertypes">Customer Type Config.</a>
                        <span class="icon-thumbnail">CT</span>
                     </li>
                     
                      <li>
                        <a href="./sitetypes">Site Type Config.</a>
                        <span class="icon-thumbnail">ST</span>
                     </li>
                     
                      <li>
                        <a href="./equipmentcategories">Equ Category Config.</a>
                        <span class="icon-thumbnail">EC</span>
                     </li>
                     
                      <li>
                        <a href="./equipmenttypes">Equ Type Config.</a>
                        <span class="icon-thumbnail">ET</span>
                     </li>
                     
                      <li>
                        <a href="#">Event Type Config.</a>
                        <span class="icon-thumbnail">EY</span>
                     </li>
                     
                     <li>
                        <a href="#">Event Config.</a>
                        <span class="icon-thumbnail">EV</span>
                     </li>
                     
                      <li>
                        <a href="#">Inspection Config.</a>
                        <span class="icon-thumbnail">IN</span>
                     </li> -->
                     
                       <li>
                        <a href="./useroles">User Role Config.</a>
                        <span class="icon-thumbnail">UR</span>
                     </li>  
                     
                      <li>
                        <a href="./users">User Config.</a>
                        <span class="icon-thumbnail">US</span>
                     </li>
                     
                     <!--  <li>
                        <a href="./masteruploads">Master Upload Config.</a>
                        <span class="icon-thumbnail">MU</span>
                     </li> -->
                     
                  </ul>
               </li>
          
            </c:if>
				</ul>
				 </c:if>
			
			<div class="clearfix"></div>
		</div>
	</nav>


	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					 <a href="./dashboard"><img src="resources/img/logo01.png" alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx tktSearch" id="SearcSElect">
                          <div class="ui  search selection  dropdown  width oveallsearch">
                                         <input id="myInput" name="tags" type="text">

                                         <div class="default text">search</div>
                                        
                                         <div id="myDropdown" class="menu">

                                                
                                                       <c:if test="${not empty access}">
                                                       <div class="item">Dashboard</div>

						       							<c:if test="${access.customerListView == 'visible' }">
                                                              <div class="item">Customer View</div>
                                                       </c:if>
                                                       
                                                       <div class="item">Site View</div>

                                                       
                                                       
                                                       
                                                       <div class="item">Events</div>
                                                       
                                                    
                                                        <div class="item">Analytics</div>
                                                       
                                                       
                                                       <c:if test="${access.customerListView == 'visible' }">
                                                              <div class="item">Site Configuration</div>
                                                       </c:if>
                                                       
                                                        <c:if test="${access.customerListView == 'visible' }">
                                                              <div class="item">Equipment Category Configuration</div>
                                                       </c:if>
                                                       
                                                       <c:if test="${access.customerListView == 'visible' }">
                                                              <div class="item">Equipment Type Configuration</div>
                                                       </c:if>

                                                       <c:if test="${access.customerListView == 'visible' }">
                                                              <div class="item">Equipment Configuration</div>
                                                       </c:if>

                                                       <c:if test="${access.customerListView == 'visible' }">
                                                              <div class="item">Customer Configuration</div>
                                                       </c:if>
                                                       
                                                         <c:if test="${access.customerListView == 'visible' }">
                                                             <div class="item">CustomerMap Configuration</div>
                                                       </c:if>
                                                       
                                                        <c:if test="${access.customerListView == 'visible' }">
                                                              <div class="item">User Role Configuration</div>
                                                       </c:if>

                                                       <c:if test="${access.customerListView == 'visible' }">
                                                             <div class="item">User Configuration</div>
                                                       </c:if>
                                                       
                                                       


                                                </c:if>
                                                       
                                                       


                                                


                                         </div>
                                  </div>
                          
                          
                          
                          
                          
                          
                        </div>
				
				 <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user_01.png" width="32" height="32">
                  </span>
                  
                            
                             <c:if test="${not empty access}">         
                              <p class="user-login">${access.userName}</p>
                  </c:if>
                  
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="logout1" class="dropdown-item color logout"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
               
                  
					 <c:if test="${access.customerListView == 'visible' }">
					 <div>
								  <span data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><p class="center m-t-5 tkts"><img class="m-t-5" src="resources/img/tkt.png"><p class="create-tkts">New Ticket</p></p></span>
					</div>
						</c:if>		
              
               

						
						
						
              
			
				
			</div>
		</div>
		<div class="page-content-wrapper" id="QuickLinkWrapper1">

			<div class="content ">

				<div class="container-fixed-lg">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="./dashboard"><i
								class="fa fa-home mt-7"></i></a></li>
						<li class="breadcrumb-item hover-idle"><a>Operation &
								Maintenance</a></li>

						<li class="breadcrumb-item active">Tickets KPI</li>
					</ol>
				</div>



				<div id="newconfiguration" class="">
					<div class="card card-transparent mb-0">
						<!-- Table Start-->
						<div class="padd-3 sortable" id="equipmentdata">
							<div class="row">
								<!-- <div class="card-header">
                                    <div class="card-title tbl-head ml-15">Equipment Curing Data</div>
                                 </div> -->
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
									<div class="card-default bg-default content-bg" data-pages="card" style="    margin-bottom: 3px;">
										<div class="col-md-3 padd-0">
												<select class="ui search dropdown forced selectSite" id="ddlSiteList">
      											<option value="">Select Site</option>
     											<!-- <option value="0">All</option> -->
     
   											 </select>
										</div>
										<div class="col-md-6"></div>
										<div class="col-md-3">
											<c:choose>
                                                           <c:when test="${access.mySiteFilter == 'true'}">
                                                                 <%-- <a class="ticketdetails"  href="./siteticketdetails${access.mySiteID}"><span>Click Here</span> to view detailed ticketing page</a> --%>
                                                                   <p class="ticketdetails"><a href="./siteticketdetails${access.mySiteID}">Click Here</a> to view detailed ticketing page</p>
								
                                                            </c:when>
                                                            
                                                            <c:when test="${access.myCustomerFilter == 'true'}">
                                                                <p class="ticketdetails"><a href="./customerticketdetails${access.myCustomerID}">Click Here</a> to view detailed ticketing page</p>
								
                                                            </c:when>
                                                            <c:otherwise>
                                                                 <p class="ticketdetails"><a href="./ticketdetails">Click Here</a> to view detailed ticketing page</p>
														 </c:otherwise>
                                        </c:choose>
										</div>
										
									</div>
								</div>

								
							</div>



						</div>


						<!-- Table End-->

						
						<!-- Chart Part -->
						<div class="padd-3 sortable" id="equipmentdata">
							<div class="row">								
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
									<div class="card card-default bg-default" data-pages="card" style="margin-bottom:3px !important;">
										<!-- <div class="card-header">
											<div class="card-title tbl-head ml-15">
												 <i class="fa fa-list" aria-hidden="true"></i> List of Tickets
											</div>
										</div> -->
										<div class="padd-5 table-responsive">
                                       	  <div id="myCarousel" class="carousel slide" data-ride="carousel">
											    <!-- Indicators -->
											    <ol class="carousel-indicators">
											      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
											      <li data-target="#myCarousel" data-slide-to="1"></li>
											    </ol>
											    <!-- Wrapper for slides -->
											    <div class="carousel-inner">
											     <!--  <div class="item active" style="height: 450px;background: #FFF;">
											       <div class="carousel-caption">
											        	<div class="col-md-12 padd-0">
															<div class="col-md-5 padd-0">
																<div id="Chartcontainer" style="height: 330px"></div>
																<p class="paragrapth">KPI Accumulated for Last 60 Days</p>
															</div>
															<div class="col-md-6 padd-0" >
																<div id="BarChart" style="height: 330px; margin: 0 auto"></div>
															<p class="paragrapth">KPI Accumulated for Last 60 Days</p>
															</div>
														</div>
											        </div>
											      </div> -->

											   <!--   <div class="item" style="height: 450px;">
											        <div class="carousel-caption">
											        <div class="col-md-11">
											        	<div id="DataChart" style="height: 400px"></div>
											        	<p class="paragrapth">KPI Accumulated for Last 60 Days</p>
											        </div>
											       
											        </div>
											      </div> 	 -->
											      
											      
											      
											      
											      				    
											    </div>

											    <!-- Left and right controls -->
											    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
											      <span class="glyphicon glyphicon-chevron-left"></span>
											      <span class="sr-only">Previous</span>
											    </a>
											    <a class="right carousel-control" href="#myCarousel" data-slide="next">
											      <span class="glyphicon glyphicon-chevron-right"></span>
											      <span class="sr-only">Next</span>
											    </a>
											  </div>


                                    	</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Chart End -->
						<!-- Table Start-->
						<div class="padd-3 sortable" id="equipmentdata">
							<div class="row">								
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
									<div class="card card-default bg-default" data-pages="card">
										<!-- <div class="card-header">
											<div class="card-title tbl-head ml-15">
												 <i class="fa fa-list" aria-hidden="true"></i> List of Tickets
											</div>
										</div> -->
										
                                    	 <div class="padd-5 table-responsive">
                                       <table id="example" class="table table-striped table-bordered dataTable no-footer" width="100%" cellspacing="0" role="grid" aria-describedby="example_info" style="width: 100%;">
                                            <thead>
                                                <tr role="row">
                                                <c:if test="${access.customerListView == 'visible'}">
	
                                                <th style="width: 69px;" class="sorting_desc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Ticket No: activate to sort column ascending" aria-sort="descending">Ticket No</th>
                                                </c:if>
                                                <th style="width: 150px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Site Name: activate to sort column ascending">Site Name</th><th style="width: 45px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Priority: activate to sort column ascending">Priority</th><th style="width: 109px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Created Time: activate to sort column ascending">Created Time</th><th style="width: 123px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Category: activate to sort column ascending">Category</th><th style="width: 163px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Subject: activate to sort column ascending">Subject</th><th style="width: 123px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Assign To: activate to sort column ascending">Assign To</th><th style="width: 96px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Scheduled On: activate to sort column ascending">Scheduled On</th><th style="width: 42px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="State: activate to sort column ascending">State</th><th style="width: 43px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Status</th></tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                           </table>
                                    </div>
									</div>
								</div>
							</div>
						</div>			
						<!-- Table End-->
					</div>
				</div>
			</div>
			<div class="container-fixed-lg footer mb-0">
				<div class="container-fluid copyright sm-text-center">
					<p class="small no-margin pull-left sm-pull-reset">
						<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
							CLEAN ENERGY</span>. <span class="hint-text">All rights reserved.
						</span>
					</p>
					<p class="small no-margin pull-rhs sm-pull-reset">
						<span class="hint-text">Powered by</span> <span>MESTECH
							SERVICES PVT LTD</span>.
					</p>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Side Bar Content Start-->
	<!-- <div class="quickview-wrapper  builder hidden-sm hidden-xs"
		id="builder">
		<div class="p-l-10 p-r-10 ">
			<a class="builder-close quickview-toggle pg-close"></a>
			<a class="builder-toggle" data-toggle="quickview"
				data-toggle-element="#builder"><img
				src="resources/img/ImageResize_06.png"></a>
			<ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary"
				id="builderTabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#tabLayouts" role="tab"
					aria-controls="home"><span>Quick Link</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active " id="tabLayouts" role="tabcard">
					<div class="scrollable">
						<div class="p-r-50">
							<div class="list">
								<ul>
									<li><a href="#QuickLinkWrapper1">Ticket Filters</a></li>
									<li><a href="#QuickLinkWrapper2">List Of Tickets</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div> -->
	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min3.js" type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min3.js" type="text/javascript"></script>
	<script type="text/javascript" src="resources/js/moment.min.js"></script>
    <script type="text/javascript" src="resources/js/datetime-moment.js"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>
	<div id="gotop"></div>
	<!-- Share Popup End !--> 
<!-- Modal -->
<div id="tktCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">New Ticket</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"><span class="errormess">*</span> All fields are mandatory</p>
				 </div>            
        		<form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form validation-form"> 
             
              <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Site Name</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width SiteNames" id="ddlSite" name="ddlSite" path="siteID">
                        <form:option value="">Select</form:option>
                        <form:options  items="${siteList}" />
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Type</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width" id="ddlType" name="ddlType"  path="ticketType">
                        <form:option value="">Select</form:option>
                        <form:option value="Operation">Operation</form:option>
                        <form:option value="Maintenance">Maintenance</form:option>
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Category</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width category" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                         <form:option data-value="Operation" value="">Select</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Preventive Maintenance">Preventive Maintenance</form:option>										
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                          <form:option data-value="Maintenance" value="">Select</form:option>
                    </form:select> 
                  </div>
                  
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                  <div class="col-md-4 col-xs-12"><label class="fields required m-t-10">Subject</label></div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<form:input  placeholder="Subject" autocomplete="off"  name="Subject"  type="text"  id="txtSubject" path="ticketDetail" maxLength="50"/>
                  	</div>
                  	</div>
               </div> 
              
              <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Priority</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width" id="ddlPriority" name="user" path="priority">
                        <form:option value="">Select </form:option>
                        <form:option data-value="3" value="3">High</form:option>
                        <form:option data-value="2" value="2">Medium</form:option>
                        <form:option data-value="1" value="1">Low</form:option>
                    </form:select> 
                  </div>
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Description</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<textarea id="txtTktdescription"  autocomplete="off" name="description" style="resize: none;" placeholder="Ticket Description" maxLength="120"></textarea>
                  	</div>
                    
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
				       <div class="btn btn-success  submit" id="btnCreate">Create</div>
				       <button class="btn btn-success" type="button" id="btnCreateDummy" tabindex="7"  style="display:none;">Create</button>
				        <div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
				  </div>
  <!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
  <!-- <div class="ui error message"></div> -->
</form:form>
				<!-- <div class="ui error message"></div> -->
				
	  		</div>
	  </div>
    </div>
 </div>
	<script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
	<script>
         $(document).ready(function() {
            $(window).fadeThis({
               speed: 500,
            });
         });
      </script>

</body>
</html>

