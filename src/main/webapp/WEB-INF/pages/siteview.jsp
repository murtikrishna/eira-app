<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet"
	rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<!--  <link href="resources/css/semantic.min.css" rel="stylesheet" type="text/css"> -->
<link href="resources/css/calendar.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/bootstrap.min.3.37.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="resources/css/semantic.min.css">
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script src="resources/js/searchselect.js" type="text/javascript"></script>

<script src="resources/js/chart/highstock.js"></script>
<script src="resources/js/chart/exporting.js"></script>

<script type="text/javascript"
	src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>

<script type="text/javascript">
        $(document).ready(function() {
        	
          
            $('.ui.dropdown').dropdown({
                fullTextSearch: true,
                forceSelection: false, 
                selectOnKeydown: true, 
                showOnFocus: true,
                on: "click" 
              });

   		$('.ui.dropdown.oveallsearch').dropdown({
                onChange: function (value, text, $selectedItem) {
                   
                   var uid =  $('#hdneampmuserid').val();
                   redirectbysearchvalue(value,uid);
                },
                fullTextSearch: true,
                forceSelection: false, 
                selectOnKeydown: false, 
                showOnFocus: true,
                on: "click" 
              });
   		
   		$('#btnCreate').click(function() {
    		if($('#ddlCategory').val== "") {
    	    	alert('Val Empty');
    	        $('.category ').addClass('error')
    	    }
    	})
        	 
            if($(window).width() < 767)
            {
   			   $('.card').removeClass("slide-left");
   			   $('.card').removeClass("slide-right");
			   $('.card').removeClass("slide-top");
			   $('.card').removeClass("slide-bottom");
			}
            

            $('.close').click(function(){
                $('#tktCreation').hide();
                $('.clear').click();
                $('.category').dropdown();
                $('.SiteNames').dropdown();
            });
            
$('.clear').click(function(){
        	$('.search').val("");
        });
$('#ddlType').change(function(){              
    $('.category').dropdown('clear')
   });


$('body').click(function(){
            $('#builder').removeClass('open'); 
          });

var validation  = {
        txtSubject: {
                   identifier: 'txtSubject',
                   rules: [{
                     type: 'empty',
                     prompt: 'Please enter a value'
                   },
                   {
                       type: 'maxLength[50]',
                       prompt: 'Please enter a value'
                     }]
                 },
                  ddlSite: {
                   identifier: 'ddlSite',
                   rules: [{
                     type: 'empty',
                     prompt: 'Please enter a value'
                   }]
                 },
                ddlType: {
                      identifier: 'ddlType',
                   rules: [{
                     type: 'empty',
                     prompt: 'Please enter a value'
                   }]
                },
                ddlCategory: {
                   identifier: 'ddlCategory',
                   rules: [{
                     type: 'empty',
                     prompt: 'Please enter a value'
                   }]
                },
                 ddlPriority: {
                   identifier: 'ddlPriority',
                   rules: [{
                     type: 'empty',
                     prompt: 'Please select a dropdown value'
                   }]
                 },
                 description: {
                   identifier: 'description',
                   rules: [{
                     type: 'empty',
                     prompt: 'Please Enter Description'
                   }]
                 }
 };
   var settings = {
     onFailure:function(){
         return false;
       }, 
     onSuccess:function(){    
       $('#btnCreate').hide();
       $('#btnCreateDummy').show()
       //$('#btnReset').attr("disabled", "disabled");          
       }};           
   $('.ui.form.validation-form').form(validation,settings);
        });
      </script>


<script type="text/javascript">
         $(document).ready(function() {
        	
             
        	 $("#searchSelect").change(function() {
                 var value = $('#searchSelect option:selected').val();
                 var uid =  $('#hdneampmuserid').val();
              	redirectbysearch(value,uid);
      	    });
        	 
        
        	 $('#dateTime').hide();

        	  $("#ddlTemp").change(function() {
        	            	 var value = $('#ddlTemp option:selected').val();
        	            	 if(value == 0) {
        	            		 $('#dateTime').show();
        	                 }
        	            	 else if(value == 1) {
        	            		 $('#dateTime').show();
        	                 }
        	            })

        	    
        	 
        
          
        	 //var table = $('#example').DataTable();
           
        	 $('#example_filter input[type="search"]').attr('placeholder','search');
        	 $('#example').DataTable({
        		 "lengthMenu": [[5,10, 25, 50, -1], [5, 10,25, 50, "All"]]	
        	 }); 
        	 $('#example1').DataTable({        	
        	     bFilter: false,
        		 searching: false,
        	 }); 
        	 $('#example2').DataTable({        	
        	     bFilter: false,
        		 searching: false,
        	 }); 
        	 $('#example3').DataTable({        	
        	     bFilter: false,
        		 searching: false,
        	 }); 
         	
   } );
        
      </script>

<script type="text/javascript">
        $(document).ready(function() {
        	
        	
        	var classstate = $('#hdnstate').val();
      	  $("#visulaizationid").addClass(classstate);
  			$("#siteviewid").addClass(classstate);
        	
          
        	$('#ddlSite').dropdown('clear');
        	$('#ddlType').dropdown('clear');
        	$('#ddlCategory').dropdown('clear');
        	$('#ddlPriority').dropdown('clear');
        	$('#txtSubject').val('');
        	$('#txtTktdescription').val('');
        	
        
        	 $("#ddlType").change(function() {
        		 $('#ddlCategory').val();
        
                 var value = $('#ddlType option:selected').val();
     
                 if(value == "Operations") {
                	 $('#ddlCategory').val();
                	 $('#category').show();

                 }
                 else{
                	 $('#ddlCategory').val();
                	 $('#category').show();

                    }
                 
              });

        	var $ddlType = $( '#ddlType' ),
    		$ddlCategory = $( '#ddlCategory' ),
        $options = $ddlCategory.find( 'option' );
        
    $ddlType.on( 'change', function() {
    	$ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
    } ).trigger( 'change' );
        });
      </script>


<style type="text/css">
@media ( min-width : 0px) and (max-width: 480px) {
	.dataTables_filter {
		float: left !important;
		margin-left: 30px;
	}
}

.pull-right i {
    margin-right: 11px;
}

.dropdown-menu {
	margin-left: -85px !important;
	left: 0px !important;
}

.highcharts-navigator, .highcharts-navigator-mask-inside,
	.highcharts-scrollbar-thumb, .highcharts-grid,
	.highcharts-scrollbar-track, .highcharts-scrollbar-button,
	.highcharts-scrollbar-arrow {
	display: none;
}

.highcharts-navigator-series, .highcharts-navigator-xaxis,
	.highcharts-scrollbar, .highcharts-legend-item {
	display: none;
}

.ui.form .fields.error .field .ui.dropdown, .ui.form .field.error .ui.dropdown
	{
	border-color: #E0B4B4 !important;
	line-height: 17px !important;
}

.ui.form textarea:not ([rows] ) {
	height: 4em;
	min-height: 4em;
	max-height: 24em;
}

.dt-button-background {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 0 !important;
}

.buttons-collection {
	height: 29px;
	z-index: 9999;
}

.input-sm, .form-horizontal .form-group-sm .form-control {
	font-size: 13px;
	min-height: 25px;
	height: 32px;
	padding: 8px 9px;
}

.buttons-excel {
	background: rgba(36, 95, 68, 0.87);
	color: #FFF;
}

.buttons-pdf {
	background: rgb(98, 165, 96);
	color: #FFF;
}

.dataTables_filter {
	float: right;
}

.dataTables_length {
	float: left;
}

#example th {
	padding-top: 12px;
	padding-bottom: 12px;
	text-align: left;
	background-color: #818181;
}

#chartdiv {
	width: 100%;
	height: 250px;
}

#overviewInfor {
	display: none;
}
</style>

</head>
<body class="fixed-header">

	<input type="hidden" value="${access.userID}" id="hdneampmuserid">
<input type="hidden" value="${classtate}" id="hdnstate">
	<jsp:include page="slider.jsp"/> 


	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					<a href="./dashboard"><img src="resources/img/logo01.png"
						alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx" id="SearcSElect">

					<div class="ui  search selection  dropdown  width oveallsearch">
						<input id="myInput" name="tags" type="text">

						<div class="default text">search</div>

						<div id="myDropdown" class="menu">

								<jsp:include page="searchselect.jsp" />	

						</div>
					</div>





				</div>
				<!-- <div class="pull-left p-t-8 fs-14 font-heading hidden-md-down">
                  <input type="text" placeholder="search..." class="searchbox">
               </div> -->
				<!-- <div class="m-l-15">
               		<p class="center mb-1 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img src="resources/img/tkt.png"></p>
               		<p class="create-tkts">New Ticket</p>
               </div> -->
				<div class="dropdown pull-right hidden-md-down user-log">
					<button class="profile-dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 circular inline"> <img
							src="resources/img/user_01.png" alt
							data-src="resources/img/user_01.png"
							data-src-retina="resources/img/user_01.png" width="32"
							height="32">
						</span>

						<c:if test="${not empty access}">
							<p class="user-login">${access.userName}</p>
						</c:if>
					</button>
					<div class="dropdown-menu dropdown-menu-right profile-dropdown"
						role="menu">
						<a class="dropdown-item disablecolor"><i
							class="pg-settings_small"></i> Settings</a> <a
							class="dropdown-item disablecolor"><i class="pg-outdent"></i>
							Feedback</a> <a class="dropdown-item disablecolor"><i
							class="pg-signals"></i> Help</a> <a href="logout1"
							class="dropdown-item color logout"><i class="pg-power"></i>
							Logout</a>
					</div>
				</div>
				
					<div class="newticketpopup hidden">
						<p class="center mb-1 tkts" data-toggle="modal"
							data-target="#tktCreation" data-backdrop="static"
							data-keyboard="false">
							<img src="resources/img/tkt.png">
						</p>
						<p class="create-tkts" data-toggle="modal"
							data-target="#tktCreation" data-backdrop="static"
							data-keyboard="false">New Ticket</p>
					</div>
				

				<!-- <a href="#" class="header-icon  btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><i class="fa fa-ticket fa-2x"></i></a><br> -->
				<!-- <p class="user-login">Create Ticket</p> -->
				<!--  <a href="#" class="header-icon pg pg-alt_menu btn-link  sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a> -->
			</div>
		</div>

		<div class="page-content-wrapper ">
			<c:if test="${not empty siteview}">
				<div class="sv-cont content" id="QuickLinkWrapper1">

					<div class="container-fixed-lg">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="./dashboard"><i
									class="fa fa-home mt-7"></i></a></li>
							<li class="breadcrumb-item"><a>Visualization</a></li>


							<c:if test="${not empty access}">
								<c:choose>
									<c:when test="${access.customerListView == 'visible'}">
										<li class="breadcrumb-item"><a
											href="./customerview${access.myCustomerID}">${access.myCustomerName}</a></li>
									</c:when>
									<c:otherwise>
										<li class="breadcrumb-item"><a href="./dashboard">${access.myCustomerName}</a></li>
									</c:otherwise>
								</c:choose>

							</c:if>






							<li class="breadcrumb-item active">${siteview.locationName}</li>
						</ol>
					</div>




					<div id="newconfiguration" class="container-fixed-lg">
						<div class="card card-transparent padd-3">
							<div class="card-block sortable padd-0">
								<div class="col-md-12 padd-0 m-t-10">
									<div class="col-lg-3 col-sm-6 col-md-6 col-padd">
									
									
										

								<jsp:include page= "${tile1}.jsp"/>

								<jsp:include page="${tile2}.jsp"/>

								<jsp:include page="${tile3}.jsp"/>
					</div>
									
									
									
									
									<div class="col-lg-3 col-padd">
									
									<jsp:include page="${tile4}.jsp"/>

								<jsp:include page="${tile5}.jsp"/>

								<jsp:include page="${tile6}.jsp"/>
										
									</div>
									<div class="col-lg-3 col-sm-6 col-padd">
										<jsp:include page="${tile7}.jsp"/>

								<jsp:include page="${tile8}.jsp"/>

								<jsp:include page="${tile9}.jsp"/>
									
									</div>
									<div class="col-lg-3 col-sm-6 col-padd">
										
										<jsp:include page="${tile10}.jsp"/>

								<jsp:include page="${tile11}.jsp"/>

								<jsp:include page="${tile12}.jsp"/>
										
									</div>
								</div>
							</div>





							<!-- Table Start-->

							<c:if test="${!empty siteviewEquipmentList}">


								<div class="sortable" id="equipmentDetails"
									style="padding-top: 0px !important; padding-bottom: 20px !important; padding-left: 0px !important;">
									<div class="col-md-12 col-padd" id="QuickViewWrapper5">
										<div class="card card-default bg-default" data-pages="card">
											<div class="card-header min-hei-20">
												<div class="card-title tbl-head">Inverter Details</div>
												
											</div>
											<div class="table-responsive padd-5">
												<table id="example"
													class="table table-striped table-bordered" width="100%"
													cellspacing="0">
													<thead>
														<tr>
															<%-- <c:if test="${access.customerListView == 'visible' }">
																<th>Equipment Code</th>
															</c:if> --%>
															<th>Equipment Name</th>
															<th>Customer Naming</th>
															<th>Equipment Type</th>
															<th>Status</th>
															<th>Today Energy (kWh)</th>
															<th>Total Energy (MWh)</th>
															<th>Specific Yield</th>
															<th>Efficiency</th>
															<c:if test="${access.customerListView == 'visible' }">
																<th>Last Checked</th>
															</c:if>
														</tr>
													</thead>
													<tbody>


														<c:forEach items="${siteviewEquipmentList}"
															var="siteviewlist">


															<tr>




																<%-- <c:if test="${access.customerListView == 'visible' }">
          										<td><a href="./equipmentview${siteviewlist.equipmentID}&ref=${access.userID}" class="newlink">${siteviewlist.equipmentCode}</a></td>
            								</c:if>                 							 
                   							 
            								<td><a href="./equipmentview${siteviewlist.equipmentID}&ref=${access.userID}" class="newlink">${siteviewlist.equipmentName}</a></td>
            								
            								 --%>



																<%-- <c:if test="${access.customerListView == 'visible' }">
																	<td><a href="./equipmentview${siteviewlist.equipmentID}" class="newlink">${siteviewlist.equipmentCode}</a></td>
															
																</c:if> --%>

																<td><a href="./equipmentview${siteviewlist.equipmentID}" class="newlink">${siteviewlist.equipmentCode}</a></td>
																
															<td>${siteviewlist.customerNaming}</a></td>
																



																<td>Inverter</td>

																<c:choose>
																	<c:when test="${siteviewlist.networkStatus=='0'}">
																		<td><div class="offline"></div>&nbsp;Offline</td>
																	</c:when>
																	<c:when test="${siteviewlist.networkStatus=='1'}">
																		<td><div class="act"></div>&nbsp;Active</td>
																	</c:when>
																	<c:when test="${siteviewlist.networkStatus=='3'}">
																		<td><div class="erro"></div>&nbsp;Error</td>
																	</c:when>
																	<c:when test="${siteviewlist.networkStatus=='2'}">
																		<td><div class="warn"></div>Warning</td>
																	</c:when>
																	<c:otherwise>
																		<td><div class="offline"></div>Offline</td>
																	</c:otherwise>
																</c:choose>


																<td>${siteviewlist.todayEnergy}</td>
																<td>${siteviewlist.totalEnergy}</td>
																<td>${siteviewlist.performanceRatio}</td>
																<td>${siteviewlist.inverterEfficiency}</td>


																<c:if test="${access.customerListView == 'visible' }">
																	<td>${siteviewlist.lastUpdate}</td>
																</c:if>

															</tr>
														</c:forEach>





													</tbody>
												</table>
											</div>
										</div>
									</div>

								</div>

							</c:if>


							<!-- Table End-->

							<c:if test="${!empty siteviewEquipmentList1}">


								<div class="sortable" id="equipmentDetails"
									style="padding-top: 0px !important; padding-bottom: 20px !important; padding-left: 0px !important;">
									<div class="col-md-12 col-padd" id="QuickViewWrapper5">
										<div class="card card-default bg-default" data-pages="card">
											<div class="card-header min-hei-20">
												<div class="card-title tbl-head">Energy Meter Details</div>
											</div>
											<div class="table-responsive padd-5">
												<table id="example1"
													class="table table-striped table-bordered" width="100%"
													cellspacing="0">
													<thead>
														<tr>
														<%-- 	<c:if test="${access.customerListView == 'visible' }">
																<th>Equipment Code</th>
															</c:if> --%>
															<th>Equipment Name</th>
															<th>Customer Naming</th>
															<th>Equipment Type</th>
															<th>Status</th>
															<th>Energy Export (MWh)</th>
															<th>Today Energy (kWh)</th>
															<th>Active Power (kW)</th>
															<th>Reactive Power (kVAr)</th>
															<th>Power Factor</th>
															<c:if test="${access.customerListView == 'visible' }">
																<th>Last Checked</th>
															</c:if>
														</tr>
													</thead>
													<tbody>


														<c:forEach items="${siteviewEquipmentList1}"
															var="siteviewlist1">


															<tr>




																<%-- <c:if test="${access.customerListView == 'visible' }">
          										<td><a href="./equipmentview${siteviewlist.equipmentID}&ref=${access.userID}" class="newlink">${siteviewlist.equipmentCode}</a></td>
            								</c:if>                 							 
                   							 
            								<td><a href="./equipmentview${siteviewlist.equipmentID}&ref=${access.userID}" class="newlink">${siteviewlist.equipmentName}</a></td>
            								
            								 --%>



																<%-- <c:if test="${access.customerListView == 'visible' }">
																	<td><a href="./equipmentview${siteviewlist1.equipmentID}" class="newlink">${siteviewlist1.equipmentCode}</a></td>
																</c:if> --%>

																<td><a href="./equipmentview${siteviewlist1.equipmentID}" class="newlink">${siteviewlist1.equipmentCode}</a></td>
																

																<td>${siteviewlist1.customerNaming}</td>


																<td>Energy Meter</td>

																<c:choose>
																	<c:when test="${siteviewlist1.networkStatus=='0'}">
																		<td><div class="offline"></div>&nbsp;Offline</td>
																	</c:when>
																	<c:when test="${siteviewlist1.networkStatus=='1'}">
																		<td><div class="act"></div>&nbsp;Active</td>
																	</c:when>
																	<c:when test="${siteviewlist1.networkStatus=='3'}">
																		<td><div class="erro"></div>&nbsp;Error</td>
																	</c:when>
																	<c:when test="${siteviewlist1.networkStatus=='2'}">
																		<td><div class="warn"></div>Warning</td>
																	</c:when>
																	<c:otherwise>
																		<td><div class="offline"></div>Offline</td>
																	</c:otherwise>
																</c:choose>



																<td>${siteviewlist1.totalEnergy}</td>
																<td>${siteviewlist1.todayEnergy}</td>
																<td>${siteviewlist1.activePower}</td>
																<td>${siteviewlist1.reactivePower}</td>
																<td>${siteviewlist1.powerFactor}</td>


																<c:if test="${access.customerListView == 'visible' }">
																	<td>${siteviewlist1.lastUpdate}</td>
																</c:if>

															</tr>
														</c:forEach>





													</tbody>
												</table>
											</div>
										</div>
									</div>

								</div>

							</c:if>




							<c:if test="${!empty siteviewEquipmentList2}">


								<div class="sortable" id="equipmentDetails"
									style="padding-top: 0px !important; padding-bottom: 20px !important; padding-left: 0px !important;">
									<div class="col-md-12 col-padd" id="QuickViewWrapper5">
										<div class="card card-default bg-default" data-pages="card">
											<div class="card-header min-hei-20">
												<div class="card-title tbl-head">String Combiner
													Details</div>
											</div>
											<div class="table-responsive padd-5">
												<table id="example2"
													class="table table-striped table-bordered" width="100%"
													cellspacing="0">
													<thead>
														<tr>
															<%-- <c:if test="${access.customerListView == 'visible' }">
																<th>Equipment Code</th>
															</c:if> --%>
															<th>Equipment Name</th>
															<th>Customer Naming</th>
															<th>Equipment Type</th>
															<th>Status</th>
															<th>Power (kW)</th>
															<th>Voltage (V)</th>
															<th>Current (A)</th>
															<th>Temp (�C)</th>

															<c:if test="${access.customerListView == 'visible' }">
																<th>Last Checked</th>
															</c:if>
														</tr>
													</thead>
													<tbody>


														<c:forEach items="${siteviewEquipmentList2}"
															var="siteviewlist2">


															<tr>



																<%-- <c:if test="${access.customerListView == 'visible' }">
																	<td><a href="./equipmentview${siteviewlist2.equipmentID}" class="newlink">${siteviewlist2.equipmentCode}</a></td>
																</c:if> --%>

																<td><a href="./equipmentview${siteviewlist2.equipmentID}" class="newlink">${siteviewlist2.equipmentCode}</a></td>
																
																	<td>${siteviewlist2.customerNaming}</td>
																
																<td>SMB</td>

																<c:choose>
																	<c:when test="${siteviewlist2.networkStatus=='0'}">
																		<td><div class="offline"></div>&nbsp;Offline</td>
																	</c:when>
																	<c:when test="${siteviewlist2.networkStatus=='1'}">
																		<td><div class="act"></div>&nbsp;Active</td>
																	</c:when>
																	<c:when test="${siteviewlist2.networkStatus=='3'}">
																		<td><div class="erro"></div>&nbsp;Error</td>
																	</c:when>
																	<c:when test="${siteviewlist2.networkStatus=='2'}">
																		<td><div class="warn"></div>Warning</td>
																	</c:when>
																	<c:otherwise>
																		<td><div class="offline"></div>Offline</td>
																	</c:otherwise>
																</c:choose>



																<td>${siteviewlist2.power}</td>
																<td>${siteviewlist2.voltage}</td>
																<td>${siteviewlist2.current}</td>
																<td>${siteviewlist2.temp}</td>
																<c:if test="${access.customerListView == 'visible' }">
																	<td>${siteviewlist2.lastUpdate}</td>
																</c:if>

															</tr>
														</c:forEach>





													</tbody>
												</table>
											</div>
										</div>
									</div>

								</div>

							</c:if>



							<c:if test="${!empty siteviewEquipmentList3}">


								<div class="sortable" id="equipmentDetails"
									style="padding-top: 0px !important; padding-bottom: 20px !important; padding-left: 0px !important;">
									<div class="col-md-12 col-padd" id="QuickViewWrapper5">
										<div class="card card-default bg-default" data-pages="card">
											<div class="card-header min-hei-20">
												<div class="card-title tbl-head">Tracker Details</div>
											</div>
											<div class="table-responsive padd-5">
												<table id="example3"
													class="table table-striped table-bordered" width="100%"
													cellspacing="0">
													<thead>
														<tr>
															<%-- <c:if test="${access.customerListView == 'visible' }">
																<th>Equipment Code</th>
															</c:if> --%>
															<th>Equipment Name</th>
															<th>Customer Naming</th>
															<th>Equipment Type</th>
															<th>Status</th>
															<th>Commanded Angle</th>
															<th>Tracker Angle</th>
															<th>Wind speed</th>
															<th>Mode</th>


															<c:if test="${access.customerListView == 'visible' }">
																<th>Last Checked</th>
															</c:if>
														</tr>
													</thead>
													<tbody>


														<c:forEach items="${siteviewEquipmentList3}"
															var="siteviewlist3">


															<tr>



																<%-- <c:if test="${access.customerListView == 'visible' }">
																	<td><a href="./equipmentview${siteviewlist3.equipmentID}" class="newlink">${siteviewlist3.equipmentCode}</a></td>
																</c:if> --%>
																
																
																	<td><a href="./equipmentview${siteviewlist3.equipmentID}" class="newlink">${siteviewlist3.equipmentCode}</a></td>
																
															<td>${siteviewlist3.customerNaming}</td>
																
																<td>Tracker</td>

																<c:choose>
																	<c:when test="${siteviewlist3.networkStatus=='0'}">
																		<td><div class="offline"></div>&nbsp;Offline</td>
																	</c:when>
																	<c:when test="${siteviewlist3.networkStatus=='1'}">
																		<td><div class="act"></div>&nbsp;Active</td>
																	</c:when>
																	<c:when test="${siteviewlist3.networkStatus=='3'}">
																		<td><div class="erro"></div>&nbsp;Error</td>
																	</c:when>
																	<c:when test="${siteviewlist3.networkStatus=='2'}">
																		<td><div class="warn"></div>Warning</td>
																	</c:when>
																	<c:otherwise>
																		<td><div class="offline"></div>Offline</td>
																	</c:otherwise>
																</c:choose>



																<td>${siteviewlist3.sunangle}</td>
																<td>${siteviewlist3.trackerangle}</td>
																<td>${siteviewlist3.windspeed}</td>
																<td>-</td>
																<c:if test="${access.customerListView == 'visible' }">
																	<td>${siteviewlist3.lastUpdate}</td>
																</c:if>

															</tr>
														</c:forEach>





													</tbody>
												</table>
											</div>
										</div>
									</div>

								</div>

							</c:if>


							<!--      <div class="card-header">
                                    <div class="card-title  tbl-head" style="margin-left:10px !important;">Daily Energy Generation</div>
                                 </div> -->


							<div class="card-block sortable"
								style="padding-top: 0px !important;">






								<%--  <div class="row chart-mob padding-15" id="dailyEnergy">
                           
                              <div class="card card-default bg-default site-chart-mob" id="sitestatus" data-pages="card">
                                   <div class="card-header">
                                    <div class="card-title  tbl-head" style="margin-left:10px !important;">Daily Energy Generation</div>
                                 </div>
                                 <div class="card-block site-chart-mob">
                                   <!--  <div id="chartdiv"></div> -->
                                     <div id="container"></div>
              
      <input type="hidden" value='${siteview.chartJsonData1}' id="ChartData1">
      <input type="hidden" value='${siteview.chartJsonData1Parameter}' id="Parameter">
      <input type="hidden" value='${siteview.chartJsonData1GraphValue}' id="GraphValue">
                  
                                
                                 </div>
                              </div>
                           </div>
                        --%>



							</div>


						</div>
					</div>


				</div>


			</c:if>
			<div class="container-fixed-lg">
				<div class="container-fluid copyright sm-text-center">
					<p class="small no-margin pull-left sm-pull-reset">
						<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
							CLEAN ENERGY.</span> <span class="hint-text">All rights reserved.
						</span>
					</p>
					<p class="small no-margin pull-rhs sm-pull-reset">
						<span class="hint-text">Powered by</span> <span>MESTECH
							SERVICES PVT LTD</span>.
					</p>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>


	<!-- Side Bar Content Start-->



	<input type="hidden" value="${dashboard.mapJsonData}" id="MapJsonId">

	<!-- Side Bar Content End-->



	<div
		class="quickview-wrapper  builder builder-site hidden-sm hidden-xs"
		id="builder">
		<div class="p-l-10 p-r-10 ">
			<a class="builder-close quickview-toggle pg-close" href="#"></a> <a
				class="builder-toggle" data-toggle="quickview"
				data-toggle-element="#builder"><img
				src="resources/img/ImageResize_06.png"></a>
			<ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary"
				id="builderTabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#tabLayouts" role="tab"
					aria-controls="home"><span>Quick Links</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active " id="tabLayouts" role="tabcard">
					<div class="scrollable">
						<div class="p-r-50">
							<div class="list">
								<ul>
									<li><a href="#QuickLinkWrapper1">Site Details</a></li>
									<li><a href="#QuickLinkWrapper1" id="energy">Energy</a></li>
									<li><a href="#QuickLinkWrapper1" id="productionyield">Production
											Yield</a></li>
									<li><a href="#QuickLinkWrapper1" id="specificyield">Specific
											Yield</a></li>

									<li><a href="#QuickLinkWrapper2" id="maps">Location</a></li>
									<li><a href="#QuickLinkWrapper2" id="sstatus">Weather</a></li>
									<li><a href="#QuickLinkWrapper2" id="tkts">Irradiation</a></li>
									<li><a href="#QuickLinkWrapper2" id="ents">Tickets</a></li>

									<li><a href="#QuickLinkWrapper3" id="maps">Scheduled
											Jobs</a></li>
									<li><a href="#QuickLinkWrapper3" id="sstatus">Events</a></li>
									<li><a href="#QuickLinkWrapper3" id="tkts">Inverter</a></li>
									<li><a href="#QuickLinkWrapper3" id="ents">Co<sub>2</sub>
											Avoided
									</a></li>

									<li><a href="#QuickLinkWrapper4">Equipment Details</a></li>
									<li><a href="#QuickLinkWrapper4">Daily Energy
											Generation</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>

	<script src="resources/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script src="resources/js/chart/amcharts.js"></script>
	<script src="resources/js/chart/serial.js"></script>
	<script src="resources/js/chart/amstock.js"></script>
	<script src="resources/js/chart/export.min.js"></script>





	<div id="gotop"></div>
	<!-- Address Popup Start !-->

	<script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>

	<!-- Modal -->


	<!-- Modal -->
	<div id="tktCreation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header Popup-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="resources/img/close.png">
					</button>
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<p class="modal-title tkt-title">
						<img src="resources/img/tkt.png"><span class="tkt-title">New
							Ticket</span>
					</p>
				</div>
				<div class="modal-body">
					<c:url var="addAction" value="/addnewticket"></c:url>
					<div class="col-md-12">
						<p class="tkt-mandat">
							<span class="errormess">*</span> All fields are mandatory
						</p>
					</div>
					<form:form action="${addAction}" modelAttribute="ticketcreation"
						class="ui form validation-form">

						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width SiteNames"
									id="ddlSite" name="ddlSite" path="siteID" onchange="getEquipmentAgainstSite()">
									<form:option value="" tabindex="0">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>
						
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                                                <div class="col-md-4 col-xs-12">
                                                       <label class="fields required m-t-10">Equipment</label>
                                                </div>

                                                <div class="col-md-8 col-xs-12">
                                                       <form:select
                                                              class="field ui fluid search selection dropdown width equipments"
                                                              id="ddlEquipment" path="equipmentID" name="ddlEquipment">
                                                              <option value="">Select</option>
                                                       </form:select>
                                                </div>
                          </div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Type</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlType" name="ddlType" path="ticketType">
									<form:option value="">Select</form:option>
									<form:option value="Operation">Operation</form:option>
									<form:option value="Maintenance">Maintenance</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Category</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width category"
									id="ddlCategory" name="ddlCategory" path="ticketCategory">
									<form:option value="">Select </form:option>
									<form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
									<form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
									<form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
									<form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
									<form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
									<form:option data-value="Operation"
										value="Equipment Replacement">Equipment Replacement</form:option>
									<form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
									<form:option data-value="Operation" value="String Down">String Down</form:option>
									<form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
									<form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
									<form:option data-value="Operation" value="">Select</form:option>
									<form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
									<form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="DataLogger Cleaning">DataLogger Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="String Current Measurement">String Current Measurement</form:option>
									<form:option data-value="Maintenance"
										value="Preventive Maintenance">Preventive Maintenance</form:option>

									<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
									<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
									<form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
									<form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
									<form:option data-value="Maintenance" value="">Select</form:option>
								</form:select>
							</div>

						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Subject</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<form:input placeholder="Subject" autocomplete="off"
										name="Subject" type="text" id="txtSubject" path="ticketDetail"
										maxLength="50" />
								</div>
							</div>
						</div>

						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Priority</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlPriority" name="user" path="priority">
									<form:option value="">Select </form:option>
									<form:option data-value="3" value="3">High</form:option>
									<form:option data-value="2" value="2">Medium</form:option>
									<form:option data-value="1" value="1">Low</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Description</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<textarea id="txtTktdescription" autocomplete="off"
										name="description" style="resize: none;"
										placeholder="Ticket Description" maxLength="120"></textarea>
								</div>

							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
							<div class="btn btn-success  submit" id="btnCreate">Create</div>
							<button class="btn btn-success" type="button" id="btnCreateDummy"
								tabindex="7" style="display: none;">Create</button>
							<div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
						</div>
						<!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
						<!-- <div class="ui error message"></div> -->
					</form:form>
					<!-- <div class="ui error message"></div> -->

				</div>
			</div>
		</div>
	</div>
	<!-- Address Popup End !-->



</body>
<script type="text/javascript">
function getEquipmentAgainstSite() {
    var siteId = $('#ddlSite').val();
   
    $.ajax({
          type : 'GET',
          url : './equipmentsBysites?siteId=' + siteId,
          success : function(msg) {
       	   
       	   var EquipmentList = msg.equipmentlist;
                 
                        for (i = 0; i < EquipmentList.length; i++) {
                               $('#ddlEquipment').append(
                                             "<option value="+EquipmentList[i].equipmentId+">" + EquipmentList[i].customerNaming
                                                          + "</option>");
                        }

          },
          error : function(msg) {
                 console.log(msg);
          }
    });
}

       </script>
</html>

