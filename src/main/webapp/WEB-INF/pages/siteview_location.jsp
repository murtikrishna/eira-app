<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="card card-default bg-default slide-left"
											id="divsitelocation" data-pages="card">
											<div class="h-150">
												<div class="card-header ">
													<div class="card-title pull-left">location</div>
													<div class="card-title pull-right">
														<i class="fa fa-map-marker font-size icon-color"
															aria-hidden="true"></i>
													</div>
												</div>
												<div class="card-block">
													<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.787859757046!2d80.11167491491186!3d12.921351990888843!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a525f694167708d%3A0x64236ca12eb44292!2sMESTECH+Services+Pvt+Ltd!5e0!3m2!1sen!2sin!4v1527082973373" frameborder="0" style="border:0" allowfullscreen></iframe> -->
													<iframe src="${siteview.locationUrl}" class="sitemap"
														style="border: 0" allowfullscreen frameborder="0"></iframe>
												</div>
											</div>
											<div class="card-footer" id="#QuickLinkWrapper3">
												<p>
													Name : <span class="">${siteview.locationName}</span>
												</p>
											</div>
										</div>