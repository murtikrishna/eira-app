<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet" type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet" type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet" rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet" type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet" type="text/css">
<link href="resources/css/bootstrap.min3.3.37.css" type="text/css" rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min3.css" type="text/css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="resources/css/jquery-ui.css">
<link href="resources/css/formValidation.min.css" rel="stylesheet" type="text/css">
<!--  <link href="resources/css/jquery-ui_cal.css" rel="stylesheet" type="text/css"> -->

<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>

<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script type="text/javascript" src="resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="resources/js/formValidation.min.js"></script>
<script type="text/javascript" src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.val.js"></script>


<script type="text/javascript" src="resources/js/toastr.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/toastr.css">
<link rel="stylesheet" type="text/css" href="http://codeseven.github.com/toastr/toastr.css">


<script type="text/javascript">

var varEquipmentType=0;
function AjaxCallForStateID(TypeId,varEquipmentType) { 
	
	 $('#ddlState').empty();
 	
	  $('#ddlState').dropdown('clear');
	 
	  $('#ddlState').append('<option value="">Select</option>');

		
	  $.ajax({
         type: 'POST',
         url: './getstates',
         data: {'CountryID' : TypeId },
         success: function(msg){
       	  debugger;
       	     	 
       	  
       	  var States = msg.state;        	  
    	  for(var i=0;i< States.length; i++)
    	  {
    		 var StateId = States[i].stateId;
    		 var StateName = States[i].stateName;
    		 
    		 $('#ddlState').append('<option value="' + StateId + '">' + StateName + '</option>');
    	  }  
    	  
    	  if(varEquipmentType!=0){
    		 
    		  setTimeout(function(){  $("#ddlState").val(varEquipmentType).change(); }, 1000);
    		 
    	  }
       	
			 
	 },
         error:function(msg) { 
         	
         	//alert(0);
         	//alert(msg);
         	}
	}); 
}


  function AjaxCallForPageLoad() {   
	  
	  

                  $.ajax({
                                    type: 'GET',
                                    url: './site',
                                    success: function(msg){
                                
                                    	
                                    	var status = msg.status;
                                    	var sitetypeList = msg.sitetypeList;
                                    	var customer=msg.customerList;
                                    	var state=msg.stateList;
                                    	var country=msg.countryList;
                                    	var siteList = msg.siteList;
                                    	var activeStatusList = msg.activeStatusList;
                                    	/* var currencyList = msg.currencyList; */
                                    	
                                    	 $.map(sitetypeList, function(val, key) {                                    		 
                                    		 $( "#ddlSiteType" ).append("<option value="+ key +">" + val +"</option>");
                                    		});
                                    	 $.map(customer, function(val, key) {                                    		 
                                    		 $( "#ddlCustomer" ).append("<option value="+ key +">" + val +"</option>");
                                    		});
                                    	 $.map(state, function(val, key) {                                    		 
                                    		 $( "#ddlState" ).append("<option value="+ key +">" + val +"</option>");
                                    		});
                                    	 
                                    	 $.map(country, function(val, key) {                                    		 
                                    		 $( "#ddlCountry" ).append("<option value="+ key +">" + val +"</option>");
                                    		});
                                    	  
                                    	 $.map(activeStatusList, function(val, key) {                                    		 
                                    		 $( "#ddlSiteStatus" ).append("<option value="+ key +">" + val +"</option>");
                                    		});
                                    	 
                                    	 
                                    	  
                                    	/*  $.map(currencyList, function(val, key) {                                    		 
                                    		 $( "#ddlCurrency" ).append("<option value="+ key +">" + val +"</option>");
                                    		}); */
                                    	 
                                    	 
                                    	for(var i=0; i < siteList.length; i++)
                                    		{
                                    			
                                    			var activeFlag = siteList[i].activeFlag;
                                    			if(activeFlag != null && activeFlag == 1)
                                    				{
                                    				activeFlag = "Active";
                                    				}
                                    			else {activeFlag = "In-active"; }
                                    			
                                    			 //$('#example').DataTable().row.add([siteList[i].siteCode,siteList[i].siteTypeName,siteList[i].siteName,siteList[i].customerName,siteList[i].stateName,siteList[i].countryName,activeFlag,'<a  href="editsite' + siteList[i].siteId + '">Edit</a>','<a href="removesite' + siteList[i].siteId + '">De-activate</a>']).draw(false);
                                    			 $('#example').DataTable().row.add([siteList[i].siteCode,siteList[i].siteTypeName,siteList[i].siteName,siteList[i].customerName,siteList[i].stateName,siteList[i].countryName,activeFlag,'<a href="#QuickLinkWrapper1" class="editbtn btn btn-primary" id="btnedit" onclick="AjaxCallForFetch(' + siteList[i].siteId + ')">Edit</a>']).draw(false);
                                    			  
                                    			 
                                    			
                                    		}
                                    	  $(".theme-loader").hide(); 
                                    	
                                    
                                    },
                                    error:function(msg) { }
                  });
  }
  
  
  
  function AjaxCallForSaveOrUpdate() {                
     // alert(0);
	var varSiteId = null; 
	var varSiteCode = null; 
	if($('#hdnSiteId').val() != null && $('#hdnSiteId').val() != "Auto-Generation" )  
		{
		varSiteId = $('#hdnSiteId').val();
		varSiteCode = $('#lblSiteCode').text();
		}
	
	
      var SiteData = [{
    		    	
    		        "siteId": varSiteId,
    		        "customerID": $("#ddlCustomer option:selected").val(),
    		        "customerName":"",
    		        "siteTypeID": $("#ddlSiteType option:selected").val(),
    		        "siteTypeName":"",
    		        "siteCode":varSiteCode,
    		        "siteName": $('#txtSiteName').val(),
    		        "siteDescription": $('#txtSiteDescription').val(),
    		        "contactPerson": $('#txtContactPerson').val(),
    		        "address": $('#txtAddress').val(),
    		        "city": $('#txtCity').val(),
    		        "stateID":  $("#ddlState option:selected").val(),
    		        "stateName":"",
    		        "countryID":  $("#ddlCountry option:selected").val(),
    		        "countryName":"",
    		        "postalCode": $('#txtPostalCode').val(),
    		        "longitude": $('#txtLongitude').val(),
    		        "latitude": $('#txtLatitude').val(),
    		        "altitude": $('#txtAltitude').val(),
    		        "siteImage": $('#txtLocationMap').val(),
    		        "installationCapacity": $('#txtInstallationCapacity').val(),
    		        "sitePONumber": $('#txtSitePONumber').val(),
    		        "sitePoDateText": $('#txtPoDate').val(),
    		        "codText": $('#txtCOD').val(),
    		        "siteOperator": $('#txtSiteOperator').val(),
    		        "siteManafacturer": $('#txtSiteManufacturer').val(),
    		        "moduleName": $('#txtModuleName').val(),
    		        "communicationType": $('#txtCommunicationType').val(),
    		        "collectionType": $('#txtCollectionType').val(),
    		        "fileType": $('#txtFileType').val(),
    		        "customerReference": $('#txtCustomerReference').val(),
    		        "customerNaming": $('#txtCustomerNaming').val(),
    		        "currencyID": $('#txtCurrency').val(),
    		        "currencyName":"",
    		        "emailID": $('#txtEmailID').val(),
    		        "mobile": $('#txtMobile').val(),
    		        "telephone": $('#txtTelephone').val(),
    		        "fax": $('#txtFax').val(),
    		        "activeFlag":  $("#ddlSiteStatus option:selected").val(),
    		        "timezone": $('#txtTimezone').val(),
    		        "moduleArea": $('#txtPlantarea').val(),
    		        
    		          
    		        "creationDate":null,
    		        "createdBy":null,
    		        "lastUpdatedDate":null,
    		        "lastUpdatedBy":null,
    		        "dataLoggerID":null,
    		        "serviceCode":null,
    		        "localFtpDirectory":null,
    		        "localFtpDirectoryPath":null,
    		        
    		        "sitePONumber":  $('#txtPonumber').val(),
    		        
    		     
    		   
    		      
    		            		        
    		      
    		     
    		            		        
    		     
    		        
    		     
    		        
    		        "typeOfContract":  $('#ddlTypeOfContract option:selected').val(),
    		        "servicePackage":  $('#ddlServicePackage option:selected').val(),
    		        "daf_Service": $('#ddlDaf_Service option:selected').val(),
    		        "daf_Frequency":  $('#ddlDaf_Frequency option:selected').val(),
    		        "tbt_Service":  $('#ddlTbt_Service option:selected').val(),
    		        "tbt_Frequency": $('#ddlTbt_Frequency option:selected').val(),
    		        
    		        "tat_Service":  $('#ddlTat_Service option:selected').val(),
    		        "tat_Frequency":  $('#ddlTat_Frequency option:selected').val(),
    		        "sym_Service": $('#ddlSym_Service option:selected').val(),
    		        "sym_frequency":  $('#ddlSym_Frequency option:selected').val(),
    		        "tls_Service":  $('#ddlTls_Service option:selected').val(),
    		        "tls_Frequency": $('#ddlTls_Frequency option:selected').val(),
    		        
    		    //     "tlm_Service":  $('#ddlTlm_Service option:selected').val(),
    		     //   "tlm_Frequency":  $('#ddlTlm_Frequency option:selected').val(),
    		        
    		        "com_Service": $('#ddlCom_Service option:selected').val(),
    		        "com_Frequency":  $('#ddlCom_Frequency option:selected').val(),
    		        "rt_Service":  $('#ddlRt_Service option:selected').val(),
    		        "rt_Frequency": $('#ddlRt_Frequency option:selected').val(),
    		        
    		        
    		        "secuirty_Service":  $('#ddlSecurity_Service option:selected').val(),
    		        "secuirty_Frequency":  $('#ddlSecurity_Frequency option:selected').val(),
    		        "rowfc_Service": $('#ddlRowfc_Service option:selected').val(),
    		        "rowfc_Frequency":  $('#ddlRowfc_Frequency option:selected').val(),
    		        
    		        
    		        "dti_Service":  $('#ddlDti_Service option:selected').val(),
    		        "dti_Frequency": $('#ddlDti_Frequency option:selected').val(),
    		        
    		        
    		        "di_Service":  $('#ddlDi_Service option:selected').val(),
    		        "di_Frequency":  $('#ddlDi_Frequency option:selected').val(),
    		        
    		        "sact_Service": $('#ddlSact_Service option:selected').val(),
    		        "sact_Frequency":  $('#ddlSact_Frequency option:selected').val(),
    		        
    		        
    		        "eltml_Service":  $('#ddlEltml_Service option:selected').val(),
    		        "eltml_Frequency": $('#ddlEltml_Frequency option:selected').val(),
    		        
    		        "eltms_Service":  $('#ddlEltms_Service option:selected').val(),
    		        "eltms_Frequency":  $('#ddlEltms_Frequency option:selected').val(),
    		        
    		      //  "ir_Service": $('#ddlIr_Service option:selected').val(),
    		      
    		     //   "ir_Frequency":  $('#ddlIr_Frequency option:selected').val(),
    		        
    		        "revampdc_Service":  $('#ddlRevampdc_Service option:selected').val(),
    		      
    		      //  "revampdc_Frequency": $('#ddlRevampdc_Frequency option:selected').val(),
    		        
    		        
    		        "revampaclt_Service":  $('#ddlRevampaclt_Service option:selected').val(),
    		        
    		    //    "revampaclt_Frequency":  $('#ddlRevampaclt_Frequency option:selected').val(),
    		        
    		        "revampacht_Service": $('#ddlRevampacht_Service option:selected').val(),
    		    //    "revampacht_Frequency":  $('#ddlRevampacht_Frequency option:selected').val(),
    		        "revampacht_Service_Civil":  $('#ddlRevampacht_Service_Civil option:selected').val(),
    		        
    		    //    "revampacht_Frequency_Civil": $('#ddlRevampacht_Frequency_Civil option:selected').val(),
    		        
    		        "jmr_Service":  $('#ddlJmr_Service option:selected').val(),
    		        "jmr_Frequency":  $('#ddlJmr_Frequency option:selected').val(),
    		        "sldc_Documentation": $('#ddlSldc_Documentation option:selected').val(),
    		        "sldc_Frequency":  $('#ddlSldc_Frequency option:selected').val(),
    		        "spares_Management":  $('#ddlSpares_Management').val(),
    		        "sm_frequency": $('#ddlSm_Frequency').val(),
    		        
    		        "cleaningCycle":  $('#ddlCleaningcycle option:selected').val(),
    		        "cleaningCycle_Frequency":  $('#ddlCleaningCycle_Frequency option:selected').val(),
    		        "vegetation_Mangement": $('#ddlVegetation_Management option:selected').val(),
    		        "vm_Frequency":  $('#ddlVm_Frequency option:selected').val(),
    		        "plantdown_Issueidentified":  $('#ddlPlantdown_IssueIdentified option:selected').val(),
    		        "plantdown_Assigned": $('#ddlPlantdown_Assigned option:selected').val(),
    		        "plantdown_Resolved": $('#ddlPlantdown_Resolved option:selected').val(),
    		        
    		        "plantwarning_Issueidentified":  $('#ddlPlantwarning_IssueIdentified option:selected').val(),
    		        "plantwarning_Assigned":  $('#ddlPlantwarning_Assigned option:selected').val(),
    		        "plantwarning_Resolved": $('#ddlPlantwarning_Resolved option:selected').val(),
    		        "equgendown_Issueidentified":  $('#ddlEqugendown_Issueidentified option:selected').val(),
    		        "equgendown_Assigned":  $('#ddlEqugendown_Assigned option:selected').val(),
    		        "equgendown_Resolved": $('#ddlEqugendown_Resolved option:selected').val(),
    		        
    		        "equgenwarning_Issueidentified":  $('#ddlEqugenwarning_Issueidentified option:selected').val(),
    		        "equgenwarning_Assigned":  $('#ddlEqugenwarning_Assigned option:selected').val(),
    		        "equgenwarning_Resolved": $('#ddlEqugenwarning_Resolved option:selected').val(),
    		        "equgenoffline_Issueidentified":  $('#ddlEqugenoffline_Issueidentified option:selected').val(),
    		        "equgenoffline_Assigned":  $('#ddlEqugenoffline_Assigned option:selected').val(),
    		        "equgenoffline_Resolved": $('#ddlEqugenoffline_Resolved option:selected').val(),
    		        
    		        "equnongendown_Issueidentified":  $('#ddlEquNongendown_Issueidentified option:selected').val(),
    		        "equnongendown_Assigned":  $('#ddlEquNongendown_Assigned option:selected').val(),
    		        "equnongendown_Resolved": $('#ddlEquNongendown_Resolved option:selected').val(),
    		        "equnongenwarning_Issueidentified":  $('#ddlEquNongenwarning_Issueidentified option:selected').val(),
    		        "equnongenwarning_Assigned":  $('#ddlEquNongenwarning_Assigned option:selected').val(),
    		        "equnongenwarning_Resolved": $('#ddlEquNongenwarning_Resolved option:selected').val(),
    		        
    		        "equnongenoffline_Issueidentified":  $('#ddlEquNongenoffline_Issueidentified option:selected').val(),
    		        "equnongenoffline_Assigned":  $('#ddlEquNongenoffline_Assigned option:selected').val(),
    		        "equnongenoffline_Resolved": $('#ddlEquNongenoffline_Resolved option:selected').val(),
    		        "commercementofworkText":  $('#txtCommercementofwork').val(),
    		        
    		        
    		        
    		        "correctiveVisits_Service":  $('#ddlCorrectiveVisits option:selected').val(),
    		        "correctiveVisits_Frequency":  $('#ddlCorrectiveVisits_Frequency option:selected').val(),
    		        "assethealth_Service": $('#ddlAssethealth option:selected').val(),
    		        "assethealth_Frequency":  $('#ddlAssethealth_Frequency option:selected').val(),
    		        "equipmentRepair":  $('#ddlEquipmentRepair option:selected').val(),
    		        "insurance_Service": $('#ddlInsurance option:selected').val(),
    		        
    		        "insurance_Frequency":  $('#ddlInsurance_Frequency option:selected').val(),
    		        "warranty_Service":  $('#ddlWarranty option:selected').val(),
    		        "warranty_Frequency": $('#ddlWarranty_Frequency option:selected').val(),
    		        "completePreventive_Service":  $('#ddlCompletePreventive option:selected').val(),
    		        "completePreventive_Service":  $('#ddlCompletePreventive_Frequency option:selected').val(),
    		        
    		      
    		   //     "performanceguaranteedamages":  $('#txtPerformanceguaranteedamages').val(),
    		        
    		        "decisionMaker":  $('#txtDecisionMaker').val(),
    		        "dm_Address":  $('#txtDm_Address').val(),
    		        "dm_City": $('#txtDm_City').val(),
    		        "dm_State":  $('#txtDm_State').val(),
    		        "dm_Country":  $('#txtDm_Country').val(),
    		        
    		        "dm_Postalcode":  $('#txtDm_PostalCode').val(),
    		        "dm_Emailid":  $('#txtDm_EmailID').val(),
    		        "dm_Phoneno": $('#txtDm_PhoneNO').val(),
    		        "champion":  $('#txtChampion').val(),
    		        "ch_Address":  $('#txtCh_Address').val(),
    		        
    		        "ch_City":  $('#txtCh_City').val(),
    		        "ch_State":  $('#txtCh_State').val(),
    		        "ch_Country": $('#txtCh_Country').val(),
    		        "ch_Postalcode":  $('#txtCh_PostalCode').val(),
    		        "ch_Emailid":  $('#txtCh_EmailID').val(),
    		        
    		        "ch_Phoneno":  $('#txtCh_PhoneNO').val(),
    		        "roadBlock":  $('#txtRoadBlock').val(),
    		        
    		        "rd_Address":  $('#txtRd_Address').val(),
    		        "rd_City":  $('#txtRd_City').val(),
    		        "rd_State": $('#txtRd_State').val(),
    		        "rd_Country":  $('#txtRd_Country').val(),
    		        "rd_Postalcode":  $('#txtRd_PostalCode').val(),
    		        
    		        "rd_Emailid":  $('#txtRd_EmailID').val(),
    		        "rd_Phoneno":  $('#txtRd_PhoneNO').val(),
    		        "keyDeliverables": $('#txtKeyDeliverables').val(),
    		        "kd_Address":  $('#txtKd_Address').val(),
    		        "kd_City":  $('#txtKd_City').val(),
    		        
    		        "kd_State":  $('#txtKd_State').val(),
    		        "kd_Country":  $('#txtKd_Country').val(),
    		        "kd_Postalcode": $('#txtKd_PostalCode').val(),
    		        "kd_Emailid":  $('#txtKd_EmailID').val(),
    		        "kd_Phoneno":  $('#txtKd_PhoneNO').val(),
    		       
    			
    			
    				 
    			
    				
    			
    				 
    			
    				
    		    
    		}];
      
     
      
      $.ajax({
                        type: 'POST',
                        contentType : 'application/json; charset=utf-8',
                        dataType : 'json',
                        url: './newsite',
                        data: JSON.stringify(SiteData),
                        success: function(msg){
                        	
                        	 var errormessage = msg.Errorlog1;
                        	if(errormessage!=null && errormessage!="")
              				{

                	    		toastr.error(errormessage);        					
              				}
              				else
              					{

                        	if($('#hdnSiteId').val() != null && $('#hdnSiteId').val() != "Auto-Generation" )  
                        	{
                        	 	
                        	}
                        	else
                        		{
                        			
                        		
                        		}
                        	
                        	
                        	
                        	$('#hdnSiteId').val('Auto-Generation');
                	    	$('#lblSiteCode').text('Auto-Generation');
                	    	

                        	var btnText = $('#btnSubmit').text();              	    	
                                	    	
                                	    	if (btnText == 'Save') {
                                	    		toastr.info("Successfully Created");
                                	    	}else {
                                	    		toastr.info("Successfully Updated");	
                                	    	}
            				
                	    	 $('.clear').click();
              					}
                	    	/*
            				$("#ddlCustomer").val(-1).change();
            				$("#ddlSiteType").val(-1).change();
            				$("#ddlState").val(-1).change();
            				$("#ddlCountry").val(-1).change();
            				
            				
            				$('#txtSiteName').val('');
            				$('#txtSiteDescription').val('');
            				$('#txtContactPerson').val('');
            				$('#txtAddress').val('');
            				$('#txtCity').val('');
            				$('#txtPostalCode').val('');
            				$('#txtLongitude').val('');
            				$('#txtLatitude').val('');
            				$('#txtAltitude').val('');
            				$('#txtLocationMap').val('');
            				$('#txtInstallationCapacity').val('');
            				$('#txtSitePONumber').val('');
                    	  
            				$('#txtSiteOperator').val('');
            				$('#txtSiteManufacturer').val('');
            				$('#txtModuleName').val('');
            				$('#txtCommunicationType').val('');
            				$('#txtCollectionType').val('');
            				$('#txtFileType').val('');
            	        	  
            				
            				$('#txtCustomerReference').val('');
            				$('#txtCustomerNaming').val('');
            				$('#txtEmailID').val('');
            				$('#txtMobile').val('');
            				$('#txtTelephone').val('');
            				$('#txtFax').val('');
            	        	  
            				$("#ddlCurrency").val(-1).change();
            				$("#ddlOperationMode").val(0).change();
            				$("#ddlSiteStatus").val(-1).change();
            					        
            		          
            		        $('#txtTimezone').val('');
            				$('#txtRemoteFtpServer').val('');
            				$('#txtRemoteFtpUserName').val('');
            				$('#txtRemoteFtpPassword').val('');
            				$('#txtRemoteFtpServerPort').val('');
            				
            				$('#txtLocalFtpHomeDirectory').val('');
            				$('#txtLocalFtpUserName').val('');
            				$('#txtLocalFtpPassword').val('');
            				$('#txtApiUrl').val('');
            				$('#txtApiKey').val('');
            				
            				$('#txtLocalFtpDirectoryPath_Sensor').val('');
            				$('#txtLocalFtpDirectoryPath_Alarm').val('');
            				$('#txtLocalFtpDirectoryPath_Log').val('');
            				$('#txtLocalFtpDirectoryPath_Modbus').val('');
            				$('#txtLocalFtpDirectoryPath_Inverter').val('');
            				
            				
            				$('#txtLocalFtpDirectoryPath_Other').val('');
            				$('#txtLocalFtpDirectory_Sensor').val('');
            				$('#txtLocalFtpDirectory_Alarm').val('');
            	        	
            				
            				$('#txtLocalFtpDirectory_Log').val('');
            				$('#txtLocalFtpDirectory_Modbus').val('');
            				$('#txtLocalFtpDirectory_Inverter').val('');
            				$('#txtLocalFtpDirectory_Other').val('');
            	        	
            		         
            				$('#txtDataLoggerID_Sensor').val('');
            				$('#txtDataLoggerID_Alarm').val('');
            				$('#txtDataLoggerID_Log').val('');
            				$('#txtDataLoggerID_Modbus').val('');
            				$('#txtDataLoggerID_Inverter').val('');
            				$('#txtDataLoggerID_Other').val('');
            	        	
            		        
            		    	$('#txtServiceCode_Sensor').val('');
            				$('#txtServiceCode_Alarm').val('');
            				$('#txtServiceCode_Log').val('');
            				$('#txtServiceCode_Modbus').val('');
            				$('#txtServiceCode_Inverter').val('');
            				$('#txtServiceCode_Other').val('');
            	        	*/
            				
            				
                        
                        },
                        error:function(msg) { 
                        	toastr.error("There was a some error. Please Contact admin");
                        	}
      }); 
}




  
  function AjaxCallForFetch(siteId) {        
	  
	  $.ajax({
          type: 'POST',
          url: './editsite',
          data: {'SiteId' : siteId },
          success: function(msg){
        	  
        	  var site = msg.site;
        	  
				$('#hdnSiteId').val(site.siteId);
				$('#lblSiteCode').html(site.siteCode);
				
				
				$("#ddlCustomer").val(site.customerID).change();
				$("#ddlSiteType").val(site.siteTypeID).change();
				/* $("#ddlState").val(site.stateID).change(); */
				
				varEquipmentType = site.stateID;
				$("#ddlCountry").val(site.countryID).change();
				
				
				
				$('#txtSiteName').val(site.siteName);
				$('#txtSiteDescription').val(site.siteDescription);
				$('#txtContactPerson').val(site.contactPerson);
				$('#txtAddress').val(site.address);
				$('#txtCity').val(site.city);
				$('#txtPostalCode').val(site.postalCode);
				$('#txtLongitude').val(site.longitude);
				$('#txtLatitude').val(site.latitude);
				$('#txtAltitude').val(site.altitude);
				$('#txtLocationMap').val(site.siteImage);
				$('#txtInstallationCapacity').val(site.installationCapacity);
				$('#txtSitePONumber').val(site.sitePONumber);
				$('#txtPoDate').val(site.sitePoDateText);
				$('#txtCOD').val(site.codText);
				
				$('#txtSiteOperator').val(site.siteOperator);
				$('#txtSiteManufacturer').val(site.siteManafacturer);
				$('#txtModuleName').val(site.moduleName);
				$('#txtCommunicationType').val(site.communicationType);
				$('#txtCollectionType').val(site.collectionType);
				$('#txtFileType').val(site.fileType);
				$('#txtPonumber').val(site.sitePONumber);
	        	
				$('#txtCustomerReference').val(site.customerReference);
				$('#txtCustomerNaming').val(site.customerNaming);
				$('#txtEmailID').val(site.emailID);
				$('#txtMobile').val(site.mobile);
				$('#txtTelephone').val(site.telephone);
				$('#txtFax').val(site.fax);
				$('#txtPlantarea').val(site.moduleArea);
				
				
	        	  
				$("#txtCurrency").val(site.currencyID);
				$("#ddlOperationMode").val(site.operationMode).change();
				$("#ddlSiteStatus").val(site.activeFlag).change();
					        
		          
		        $('#txtTimezone').val(site.timezone);
				$('#txtRemoteFtpServer').val(site.remoteFtpServer);
				$('#txtRemoteFtpUserName').val(site.remoteFtpUserName);
				$('#txtRemoteFtpPassword').val(site.remoteFtpPassword);
				$('#txtRemoteFtpServerPort').val(site.remoteFtpServerPort);
				
				$('#txtLocalFtpHomeDirectory').val(site.localFtpHomeDirectory);
				$('#txtLocalFtpUserName').val(site.localFtpUserName);
				$('#txtLocalFtpPassword').val(site.localFtpPassword);
				$('#txtApiUrl').val(site.apiUrl);
				$('#txtApiKey').val(site.apiKey);
				
				$('#txtLocalFtpDirectoryPath_Sensor').val(site.localFtpDirectoryPath_Sensor);
				$('#txtLocalFtpDirectoryPath_Alarm').val(site.localFtpDirectoryPath_Alarm);
				$('#txtLocalFtpDirectoryPath_Log').val(site.localFtpDirectoryPath_Log);
				$('#txtLocalFtpDirectoryPath_Modbus').val(site.localFtpDirectoryPath_Modbus);
				$('#txtLocalFtpDirectoryPath_Inverter').val(site.localFtpDirectoryPath_Inverter);
				
				
				$('#txtLocalFtpDirectoryPath_Other').val(site.localFtpDirectoryPath_Data);
				$('#txtLocalFtpDirectory_Sensor').val(site.localFtpDirectory_Sensor);
				$('#txtLocalFtpDirectory_Alarm').val(site.localFtpDirectory_Alarm);
	        	
				
				$('#txtLocalFtpDirectory_Log').val(site.localFtpDirectory_Log);
				$('#txtLocalFtpDirectory_Modbus').val(site.localFtpDirectory_Modbus);
				$('#txtLocalFtpDirectory_Inverter').val(site.localFtpDirectory_Inverter);
				$('#txtLocalFtpDirectory_Other').val(site.localFtpDirectory_Data);
	        	
		         
				$('#txtDataLoggerID_Sensor').val(site.dataLoggerID_Sensor);
				$('#txtDataLoggerID_Alarm').val(site.dataLoggerID_Alarm);
				$('#txtDataLoggerID_Log').val(site.dataLoggerID_Log);
				$('#txtDataLoggerID_Modbus').val(site.dataLoggerID_Modbus);
				$('#txtDataLoggerID_Inverter').val(site.dataLoggerID_Inverter);
				$('#txtDataLoggerID_Other').val(site.dataLoggerID_Data);
	        	
		        
		    	$('#txtServiceCode_Sensor').val(site.serviceCode_Sensor);
				$('#txtServiceCode_Alarm').val(site.serviceCode_Alarm);
				$('#txtServiceCode_Log').val(site.serviceCode_Log);
				$('#txtServiceCode_Modbus').val(site.serviceCode_Modbus);
				$('#txtServiceCode_Inverter').val(site.serviceCode_Inverter);
				$('#txtServiceCode_Other').val(site.serviceCode_Data);
	        	
          	
				
				 $('#ddlTypeOfContract').val(site.typeOfContract).change();
   		      $('#ddlServicePackage').val(site.servicePackage).change();
   		       $('#ddlDaf_Service').val(site.daf_Service).change();
   		       $('#ddlDaf_Frequency').val(site.daf_Frequency).change();
   		       $('#ddlTbt_Service').val(site.tbt_Service).change();
   		   $('#ddlTbt_Frequency').val(site.tbt_Frequency).change();
		        
		      $('#ddlTat_Service').val(site.tat_Service).change();
		       $('#ddlTat_Frequency').val(site.tat_Frequency).change();
		      $('#ddlSym_Service').val(site.sym_Service).change();
		       $('#ddlSym_Frequency').val(site.sym_frequency).change();
		        $('#ddlTls_Service').val(site.tls_Service).change();
		       $('#ddlTls_Frequency').val(site.tls_Frequency).change();
		        
		        $('#ddlTlm_Service').val(site.tlm_Service).change();
		        $('#ddlTlm_Frequency').val(site.tlm_Frequency).change();
		        $('#ddlCom_Service').val(site.com_Service).change();
		      $('#ddlCom_Frequency').val(site.com_Frequence).change();
		       $('#ddlRt_Service').val(site.rt_Service).change();
		         $('#ddlRt_Frequency').val(site.rt_Frequency).change();
		        
		        
		      $('#ddlSecurity_Service').val(site.secuirty_Service).change();
		       $('#ddlSecurity_Frequency').val(site.secuirty_Frequency).change();
		      $('#ddlRowfc_Service').val(site.rowfc_Service).change();
		      $('#ddlRowfc_Frequency').val(site.rowfc_Frequency).change();;
		       $('#ddlDti_Service').val(site.dti_Service).change();
		       $('#ddlDti_Frequency').val(site.dti_Frequency).change();
		        
		        
		       $('#ddlDi_Service').val(site.di_Service).change();
		         $('#ddlDi_Frequency').val(site.di_Frequency).change();
		       $('#ddlSact_Service').val(site.sact_Service).change();
		         $('#ddlSact_Frequency').val(site.sact_Frequency).change();;
		        $('#ddlEltml_Service').val(site.eltml_Service).change();
		        $('#ddlEltml_Frequency').val(site.eltml_Frequency).change();
		        
		        $('#ddlEltms_Service').val(site.eltms_Service).change();
		         $('#ddlEltms_Frequency').val(site.eltms_Frequency).change();
		        $('#ddlIr_Service').val(site.ir_Service).change();
		         $('#ddlIr_Frequency').val(site.ir_Frequency).change();
		          $('#ddlRevampdc_Service').val(site.revampdc_Service).change();
		       $('#ddlRevampdc_Frequency').val(site.revampdc_Frequency).change();
		        
		        
		   $('#ddlRevampaclt_Service').val(site.revampaclt_Service).change();
		         $('#ddlRevampaclt_Frequency').val(site.revampaclt_Frequency).change();
		       $('#ddlRevampacht_Service').val(site.revampaclt_Frequency).change();
		        $('#ddlRevampacht_Frequency').val(site.revampacht_Frequency).change();
		        $('#ddlRevampacht_Service_Civil').val(site.revampacht_Service_Civil).change();
		      $('#ddlRevampacht_Frequency_Civil').val(site.revampacht_Frequency_Civil).change();
		        
		     $('#ddlJmr_Service').val(site.jmr_Service).change();
		        $('#ddlJmr_Frequency').val(site.jmr_Frequency).change();
		      $('#ddlSldc_Documentation').val(site.sldc_Documentation).change();
		       $('#ddlSldc_Frequency').val(site.sldc_Frequency).change();
		         $('#ddlSpares_Management').val(site.spares_Management).change();
		      $('#ddlSm_Frequency').val(site.sm_frequency).change();
		        
		       $('#ddlCleaningcycle').val(site.cleaningCycle).change();
		       $('#ddlCleaningCycle_Frequency').val(site.cleaningCycle_Frequency).change();
		         $('#ddlVegetation_Management').val(site.vegetation_Mangement).change();
		          $('#ddlVm_Frequency').val(site.vm_Frequency).change();
		      $('#ddlPlantdown_IssueIdentified').val(site.plantdown_Issueidentified).change();
		        $('#ddlPlantdown_Assigned').val(site.plantdown_Assigned).change();
		        $('#ddlPlantdown_Resolved').val(site.plantdown_Resolved).change();
		        
		     $('#ddlPlantwarning_IssueIdentified').val(site.plantwarning_Issueidentified).change();
		      $('#ddlPlantwarning_Assigned').val(site.plantwarning_Assigned).change();
		        $('#ddlPlantwarning_Resolved').val(site.plantwarning_Resolved).change();
		        $('#ddlEqugendown_Issueidentified').val(site.equgendown_Issueidentified).change();
		        $('#ddlEqugendown_Assigned').val(site.equgendown_Assigned).change();
		        $('#ddlEqugendown_Resolved').val(site.equgendown_Resolved).change();
		        
		     $('#ddlEqugenwarning_Issueidentified').val(site.equgenwarning_Issueidentified).change();
		      $('#ddlEqugenwarning_Assigned').val(site.equgenwarning_Assigned).change();
		    $('#ddlEqugenwarning_Resolved').val(site.equgenwarning_Resolved).change();
		       $('#ddlEqugenoffline_Issueidentified').val(site.equgenoffline_Issueidentified).change();
		          $('#ddlEqugenoffline_Assigned').val(site.equgenoffline_Assigned).change();
		     $('#ddlEqugenoffline_Resolved').val(site.equgenoffline_Resolved).change();
		        
		       $('#ddlEquNongendown_Issueidentified').val(site.equnongendown_Issueidentified).change();
		       $('#ddlEquNongendown_Assigned').val(site.equnongendown_Assigned).change();
		        $('#ddlEquNongendown_Resolved').val(site.equnongendown_Resolved).change();
		        $('#ddlEquNongenwarning_Issueidentified').val(site.equnongenwarning_Issueidentified).change();
		      $('#ddlEquNongenwarning_Assigned').val(site.equnongenwarning_Assigned).change();
		        $('#ddlEquNongenwarning_Resolved').val(site.equnongenwarning_Resolved).change();
		        
		       $('#ddlEquNongenoffline_Issueidentified').val(site.equnongenoffline_Issueidentified).change();
		         $('#ddlEquNongenoffline_Assigned').val(site.equnongenoffline_Assigned).change();
		       $('#ddlEquNongenoffline_Resolved').val(site.equnongenoffline_Resolved).change();
		        $('#txtCommercementofwork').val(site.commercementofworkText);
		       $('#txtPerformanceguaranteedamages').val(site.performanceguaranteedamages);
		        
		       
		       
		       $('#ddlCorrectiveVisits').val(site.correctiveVisits_Service).change();
		         $('#ddlCorrectiveVisits_Frequency').val(site.correctiveVisits_Frequency).change();
		       $('#ddlAssethealth').val(site.assethealth_Service).change();
		        $('#ddlAssethealth_Frequency').val(site.assethealth_Frequency).change();
		       $('#ddlEquipmentRepair').val(site.equipmentRepair).change();
		       
		       $('#ddlInsurance').val(site.insurance_Service).change();
		         $('#ddlInsurance_Frequency').val(site.insurance_Frequency).change();
		       $('#ddlWarranty').val(site.warranty_Service).change();
		        $('#ddlWarranty_Frequency').val(site.warranty_Frequency).change();
		       $('#ddlCompletePreventive').val(site.completePreventive_Service).change();
		       $('#ddlCompletePreventive_Frequency').val(site.completePreventive_Service).change();
		        
		       
		      
		      
		       
		       
		     $('#txtDecisionMaker').val(site.decisionMaker);
		       $('#txtDm_Address').val(site.dm_Address);
		      $('#txtDm_City').val(site.dm_City);
		      $('#txtDm_State').val(site.dm_State);
		          $('#txtDm_Country').val(site.dm_Country);
		        
		         $('#txtDm_PostalCode').val(site.dm_Postalcode);
		        $('#txtDm_EmailID').val(site.dm_Emailid);
		       $('#txtDm_PhoneNO').val(site.dm_Phoneno);
		         $('#txtChampion').val(site.champion);
		         $('#txtCh_Address').val(site.ch_Address);
		        
		        $('#txtCh_City').val(site.ch_City);
		        $('#txtCh_State').val(site.ch_State);
		      $('#txtCh_Country').val(site.ch_Country);
		        $('#txtCh_PostalCode').val(site.ch_Postalcode);
		       $('#txtCh_EmailID').val(site.ch_Emailid);
		        
		         $('#txtCh_PhoneNO').val(site.ch_Phoneno);
		        $('#txtRoadBlock').val(site.roadBlock);
		        
		         $('#txtRd_Address').val(site.rd_Address);
		          $('#txtRd_City').val(site.rd_City);
		        $('#txtRd_State').val(site.rd_State);
		        $('#txtRd_Country').val(site.rd_Country);
		          $('#txtRd_PostalCode').val(site.rd_Postalcode);
		        
		        $('#txtRd_EmailID').val(site.rd_Emailid);
		         $('#txtRd_PhoneNO').val(site.rd_Phoneno);
		       $('#txtKeyDeliverables').val(site.keyDeliverables);
		         $('#txtKd_Address').val(site.kd_Address);
		         $('#txtKd_City').val(site.kd_City);
		        
		         $('#txtKd_State').val(site.kd_State);
		        $('#txtKd_Country').val(site.kd_Country);
		       $('#txtKd_PostalCode').val(site.kd_Postalcode);
		       $('#txtKd_EmailID').val(site.kd_Emailid);
		          $('#txtKd_PhoneNO').val(site.kd_Phoneno);
   		       
				
		          $('#btnSubmit').text('Update');
          
          },
          error:function(msg) { 
          	
          	}
	}); 
	  
	  
      
      
     
}



  
      </script>






<script type="text/javascript">
        $(document).ready(function() {
        	
var classstate = $('#hdnstate').val();
			
$("#configurationid").addClass(classstate);
$("#siteconfigid").addClass(classstate);
        	
            $(".theme-loader").animate({
                opacity: "0"
            },50000);

      	  $('#btnCreate').click(function() {
        		if($('#ddlCategory').val== "") {
        	    	alert('Val Empty');
        	        $('.category ').addClass('error')
        	    }
        	})
        	
        	
        	
        	$('#ddlDaf_Service').on('change', function(){
        	
    if( $(this).val() == 'Yes' ){
    	
        $('#toshow14').show();
      
    } else {
    	  $('#toshow14').hide();
    	// $('#ddlDaf_Frequency').dropdown('clear');
    	// $('#ddlDaf_Frequency').show();
    }
});
      	  
      	  
      	$('#ddlCleaningcycle').on('change', function(){
    		
if( $(this).val() == 'Yes' ){
	
    $('#toshow1').show();
  
} else {
	  $('#toshow1').hide();
	//  $('#ddlCleaningCycle_Frequency').dropdown('clear');
	  
}
});
      	
    	$('#ddlCorrectiveVisits').on('change', function(){
    		
if( $(this).val() == 'Yes' ){
	
    $('#toshow2').show();
  
} else {
	  $('#toshow2').hide();
	//  $('#ddlCorrectiveVisits_Frequency').dropdown('clear');
	  
}
});
    	
    	$('#ddlAssethealth').on('change', function(){
    		
if( $(this).val() == 'Yes' ){
	
    $('#toshow3').show();
  
} else {
	  $('#toshow3').hide();
	//  $('#ddlAssethealth_Frequency').dropdown('clear');
}
});
    	
    	
    	$('#ddlCompletePreventive').on('change', function(){
    		
    		if( $(this).val() == 'Yes' ){
    			
    		    $('#toshow4').show();
    		  
    		} else {
    			  $('#toshow4').hide();
    			//  $('#ddlCompletePreventive_Frequency').dropdown('clear');
    		}
    		});
    	
    	
$('#ddlJmr_Service').on('change', function(){
    		
    		if( $(this).val() == 'Yes' ){
    			
    		    $('#toshow5').show();
    		  
    		} else {
    			  $('#toshow5').hide();
    			//  $('#ddlJmr_Frequency').dropdown('clear');
    		}
    		});
    		
    		
    		
    		
$('#ddlVegetation_Management').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow6').show();
	  
	} else {
		  $('#toshow6').hide();
		 // $('#ddlVm_Frequency').dropdown('clear');
	}
	});
	
	
$('#ddlSecurity_Service').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow7').show();
	  
	} else {
		  $('#toshow7').hide();
		//  $('#ddlSecurity_Frequency').dropdown('clear');
	}
	});
	
	
	
$('#ddlWarranty').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow8').show();
	  
	} else {
		  $('#toshow8').hide();
		//  $('#ddlWarranty_Frequency').dropdown('clear');
	}
	});
	
	
$('#ddlInsurance').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow9').show();
	  
	} else {
		  $('#toshow9').hide();
		//  $('#ddlInsurance_Frequency').dropdown('clear');
	}
	});
	
	
$('#ddlSpares_Management').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow10').show();
	  
	} else {
		  $('#toshow10').hide();
		//  $('#ddlSm_Frequency').dropdown('clear');
	}
	});
	
	
	
$('#ddlSym_Service').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow11').show();
	  
	} else {
		  $('#toshow11').hide();
		//  $('#ddlSym_Frequency').dropdown('clear');
	}
	});
	
	
	
$('#ddlRowfc_Service').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow12').show();
	  
	} else {
		  $('#toshow12').hide();
		//  $('#ddlRowfc_Frequency').dropdown('clear');
	}
	});
	
	
	
$('#ddlCom_Service').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow13').show();
	  
	} else {
		  $('#toshow13').hide();
		//  $('#ddlCom_Frequency').dropdown('clear');
	}
	});
	
	

	
	
$('#ddlDi_Service').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow15').show();
	  
	} else {
		  $('#toshow15').hide();
		//  $('#ddlDi_Frequency').dropdown('clear');
	}
	});
	
	
$('#ddlDti_Service').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow16').show();
	  
	} else {
		  $('#toshow16').hide();
		//  $('#ddlDti_Frequency').dropdown('clear');
	}
	});
	
	
$('#ddlEltml_Service').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow17').show();
	  
	} else {
		  $('#toshow17').hide();
		//  $('#ddlEltml_Frequency').dropdown('clear');
	}
	});
	
	
$('#ddlEltms_Service').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow18').show();
	  
	} else {
		  $('#toshow18').hide();
		//  $('#ddlEltms_Frequency').dropdown('clear');
	}
	});
	
	
	
$('#ddlRt_Service').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow19').show();
	  
	} else {
		  $('#toshow19').hide();
		//  $('#ddlRt_Frequency').dropdown('clear');
	}
	});
	
	
$('#ddlSldc_Documentation').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow20').show();
	  
	} else {
		  $('#toshow20').hide();
		//  $('#ddlSldc_Frequency').dropdown('clear');
	}
	});
	
	
$('#ddlSact_Service').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow21').show();
	  
	} else {
		  $('#toshow21').hide();
		//  $('#ddlSact_Frequency').dropdown('clear');
	}
	});
	
	
$('#ddlTbt_Service').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow22').show();
	  
	} else {
		  $('#toshow22').hide();
		//  $('#ddlTbt_Frequency').dropdown('clear');
	}
	});
	
	
$('#ddlTat_Service').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow23').show();
	  
	} else {
		  $('#toshow23').hide();
		//  $('#ddlTat_Frequency').dropdown('clear');
	}
	});
	
	
$('#ddlTls_Service').on('change', function(){
	
	if( $(this).val() == 'Yes' ){
		
	    $('#toshow24').show();
	  
	} else {
		  $('#toshow24').hide();
		//  $('#ddlTls_Frequency').dropdown('clear');
	}
	});
        	
        	
$("#txtCommercementofwork").datepicker({                  	
	dateFormat:'dd/mm/yy',         	
	minDate: new Date(2018, 01 -1, 01),
	maxDate: new Date(),
	maxDate: "0"
     
  });
        	
        	
        	$("#txtPoDate").datepicker({ dateFormat: "dd/mm/yy"});
      	$("#txtCOD").datepicker({ dateFormat: "dd/mm/yy"});
            
        	
        	$('#ddlSite').dropdown('clear');
        	$('#ddlType').dropdown('clear');
        	$('#ddlCategory').dropdown('clear');
        	$('#ddlPriority').dropdown('clear');
        	$('#txtSubject').val('');
        	$('#txtTktdescription').val('');
        	
        	
        /*First Tab Val Empty*/	
        	$('#ddlSiteType').dropdown('clear');
        	$('#ddlCustomer').dropdown('clear');
        	$('#ddlState').dropdown('clear');
        	$('#ddlCountry').dropdown('clear');
        	$('#ddlSiteStatus').dropdown('clear');        	
        	$('#txtSubject').val('');
        	$('#txtSiteName').val('');
        	$('#txtAddress').val('');
        	$('#txtCity').val('');
        	$('#txtLongitude').val('');
        	$('#txtLatitude').val('');
        	$('#txtContactPerson').val('');
        	$('#txtPostalCode').val('');
        	$('#txtMobile').val('');
        	$('#txtEmailID').val('');
        	$('#txtCustomerReference').val('');
        	$('#txtCustomerNaming').val('');
        	$('#txtInstallationCapacity').val('');
        	$('#txtLocationMap').val('');
        	
        	
        	/*Second Tab Val Empty*/
        	$('#txtDecisionMaker').val('');
        	$('#txtDm_Address').val('');
        	$('#txtDm_City').val('');
        	$('#txtDm_State').val('');
        	$('#txtDm_Country').val('');
        	$('#txtDm_PostalCode').val('');
        	$('#txtDm_EmailID').val('');
        	$('#txtDm_PhoneNO').val('');
        	
        	$('#lblSiteCode').val('');
        	
        	$('#txtChampion').val('');
        	$('#txtCh_Address').val('');
        	$('#txtCh_City').val('');
        	$('#txtCh_State').val('');
        	$('#txtCh_Country').val('');
        	$('#txtCh_PostalCode').val('');
        	$('#txtCh_EmailID').val('');
        	$('#txtCh_PhoneNO').val('');
        	
        	
        	$('#txtRoadBlock').val('')
        	$('#txtRd_Address').val('');
        	$('#txtRd_City').val('');
        	$('#txtRd_State').val('');
        	$('#txtRd_Country').val('');
        	$('#txtRd_PostalCode').val('');
        	$('#txtRd_EmailID').val('');
        	$('#txtRd_PhoneNO').val('');
        	
        	
        	$('#txtKeyDeliverables').val('')
        	$('#txtKd_Address').val('');
        	$('#txtKd_City').val('');
        	$('#txtKd_State').val('');
        	$('#txtKd_Country').val('');
        	$('#txtKd_PostalCode').val('');
        	$('#txtKd_EmailID').val('');
        	$('#txtKd_PhoneNO').val('');
        	
        	/*Third Tab Val Empty*/
        	$('#ddlTypeOfContract').dropdown('clear');
        	$('#ddlServicePackage').dropdown('clear');
        	$('#ddlVegetation_Management').dropdown('clear');
        	$('#ddlVm_Frequency').dropdown('clear');
        	
        	$('#ddlDaf_Service').dropdown('clear');
        	$('#ddlDaf_Frequency').dropdown('clear');
        	$('#ddlTbt_Service').dropdown('clear');
        	$('#ddlTbt_Frequency').dropdown('clear');
        	
        	$('#ddlTat_Service').dropdown('clear');
        	$('#ddlTat_Frequency').dropdown('clear');
        	$('#ddlSym_Service').dropdown('clear');
        	$('#ddlSym_Frequency').dropdown('clear');
        	
        	$('#ddlTls_Service').dropdown('clear');
        	$('#ddlTls_Frequency').dropdown('clear');
        	$('#ddlTlm_Service').dropdown('clear');
        	$('#ddlTlm_Frequency').dropdown('clear');
        	
        	$('#ddlCom_Service').dropdown('clear');
        	$('#ddlCom_Frequency').dropdown('clear');
        	$('#ddlRt_Service').dropdown('clear');
        	$('#ddlRt_Frequency').dropdown('clear');
        	
        	$('#ddlSecurity_Service').dropdown('clear');
        	$('#ddlSecurity_Frequency').dropdown('clear');
        	$('#ddlRowfc_Service').dropdown('clear');
        	$('#ddlRowfc_Frequency').dropdown('clear');
        	
        	$('#ddlDti_Service').dropdown('clear');
        	$('#ddlDti_Frequency').dropdown('clear');
        	$('#ddlDi_Service').dropdown('clear');
        	$('#ddlDi_Frequency').dropdown('clear');
        	
        	$('#ddlSact_Service').dropdown('clear');
        	$('#ddlSact_Frequency').dropdown('clear');
        	$('#ddlEltml_Service').dropdown('clear');
        	$('#ddlEltml_Frequency').dropdown('clear');
        	
        	$('#ddlEltms_Service').dropdown('clear');
        	$('#ddlEltms_Frequency').dropdown('clear');
        	$('#ddlIr_Service').dropdown('clear');
        	$('#ddlIr_Frequency').dropdown('clear');
        	
        	$('#ddlRevampdc_Service').dropdown('clear');
        	$('#ddlRevampdc_Frequency').dropdown('clear');
        	$('#ddlRevampaclt_Service').dropdown('clear');
        	$('#ddlRevampaclt_Frequency').dropdown('clear');
        	
        	$('#ddlRevampacht_Service').dropdown('clear');
        	$('#ddlRevampacht_Frequency').dropdown('clear');
        	$('#ddlRevampacht_Service_Civil').dropdown('clear');
        	$('#ddlRevampacht_Frequency_Civil').dropdown('clear');
        	
        	$('#ddlJmr_Service').dropdown('clear');
        	$('#ddlJmr_Frequency').dropdown('clear');
        	$('#ddlSldc_Documentation').dropdown('clear');
        	$('#ddlSldc_Frequency').dropdown('clear');
        	
        	$('#ddlSpares_Management').dropdown('clear');
        	$('#ddlSm_Frequency').dropdown('clear');
        	$('#ddlCleaningcycle').dropdown('clear');
        	$('#ddlCleaningCycle_Frequency').dropdown('clear');
        	
        	$('#ddlPlantdown_IssueIdentified').dropdown('clear');
        	$('#ddlPlantdown_Assigned').dropdown('clear');
        	$('#ddlPlantdown_Resolved').dropdown('clear');
        	$('#ddlPlantwarning_IssueIdentified').dropdown('clear');
        	
        	$('#ddlPlantwarning_Assigned').dropdown('clear');
        	$('#ddlPlantwarning_Resolved').dropdown('clear');
        	$('#ddlEqugendown_Issueidentified').dropdown('clear');
        	$('#ddlEqugendown_Assigned').dropdown('clear');
        	
        	$('#ddlEqugendown_Resolved').dropdown('clear');
        	$('#ddlEqugenwarning_Issueidentified').dropdown('clear');
        	$('#ddlEqugenwarning_Assigned').dropdown('clear');
        	$('#ddlEqugenwarning_Resolved').dropdown('clear');
        	
        	$('#ddlEqugenoffline_Issueidentified').dropdown('clear');
        	$('#ddlEqugenoffline_Assigned').dropdown('clear');
        	$('#ddlEqugenoffline_Resolved').dropdown('clear');
        	$('#ddlEquNongendown_Issueidentified').dropdown('clear');
        	
        	$('#ddlEquNongendown_Assigned').dropdown('clear');
        	$('#ddlEquNongendown_Resolved').dropdown('clear');
        	$('#ddlEquNongenwarning_Issueidentified').dropdown('clear');
        	$('#ddlEquNongenwarning_Assigned').dropdown('clear');
        	
        	$('#ddlEquNongenwarning_Resolved').dropdown('clear');
        	$('#ddlEquNongenoffline_Issueidentified').dropdown('clear');
        	$('#ddlEquNongenoffline_Assigned').dropdown('clear');
        	$('#ddlEquNongenoffline_Resolved').dropdown('clear');
        	
        	$('#txtCommercementofwork').val('');
        	$('#txtPerformanceguaranteedamages').val('');
        	
        	/*Fourth Tab Val Empty*/
        	
        	
        	$('#btnClear').click(function(){
        		$('#lblSiteCode').text('Auto-Generation');
        		$('#hdnSiteId').val('Auto-Generation');
        	
				$('#btnSubmit').text('Submit');
			});
        	
        	toastr.options = {
     	           "debug": false,
     	           "positionClass": "toast-bottom-right",
     	           "progressBar":true,
     	           "onclick": null,
     	           "fadeIn": 500,
     	           "fadeOut": 100,
     	           "timeOut": 1000,
     	           "extendedTimeOut": 2000
     	         }
     	
        	
        	 $('#example').DataTable({
        		 "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]]	
        });  
        	 $('.btnNext').click(function(){
				 var select = $(this);
				 var idSite = select.attr('id');
				 var selectVal = idSite.replace(/-next/,'');
				 $('#' + selectVal).trigger('click');
			});
			 
			 
			 $('.btnPrev').click(function(){
				 
				 var select = $(this);
				 var idSite = select.attr('id');
				 var selectVal = idSite.replace(/-prev/,'');
				 $('#' + selectVal).trigger('click');
			});
        	
        	
        /* 	 $('#addSite').submit(function(event) {
        	       
        	      var producer = $('#producer').val();
        	      var model = $('#model').val();
        	      var price = $('#price').val();
        	      var json = { "producer" : producer, "model" : model, "price": price};
        	       
        	    $.ajax({
        	        url: $("#newSmartphoneForm").attr( "action"),
        	        data: JSON.stringify(json),
        	        type: "POST",
        	         
        	        beforeSend: function(xhr) {
        	            xhr.setRequestHeader("Accept", "application/json");
        	            xhr.setRequestHeader("Content-Type", "application/json");
        	        },
        	        success: function(smartphone) {
        	            var respContent = "";
        	             
        	            respContent += "<span class='success'>Smartphone was created: [";
        	            respContent += smartphone.producer + " : ";
        	            respContent += smartphone.model + " : " ;
        	            respContent += smartphone.price + "]</span>";
        	             
        	            $("#sPhoneFromResponse").html(respContent);         
        	        }
        	    });
        	      
        	    event.preventDefault();
        	  });
        	  */
        	  
        	  var url = './sitedetailslist';
        	/*  $('#example').DataTable({ 
        	 	"order": [[ 0, "desc" ]],
        		 'processing': true,
                 'serverSide': true,
                 'ajax': {
                   type: 'POST',
                   'url': url                   
                 },
        	 "columns": [
                 { "data": "siteCode" },
                 { "data": "siteTypeName" },
                 { "data": "SiteName" },
                 { "data": "customerName" },
                 { "data": "stateName" },
                 { "data": "countryName" },
                 {"data":"activeFlag"},
                 {"data":"activeFlag"},
                 {"data":"activeFlag"}
             ]
        	 }); */
        	  
        	 $("#txtFromDate").datepicker({ dateFormat: "dd/mm/yy"});
       	     $("#txtToDate").datepicker({ dateFormat: "dd/mm/yy"});
       	  $('.ui.dropdown').dropdown({forceSelection:false}); 
       	   
       	 $("#searchSelect").change(function() {
             var value = $('#searchSelect option:selected').val();
             var uid =  $('#hdneampmuserid').val();
           redirectbysearch(value,uid);
        }); $('.dataTables_filter input[type="search"]').attr('placeholder','search');

        
            if($(window).width() < 767)
				{
				   $('.card').removeClass("slide-left");
				   $('.card').removeClass("slide-right");
				   $('.card').removeClass("slide-top");
				   $('.card').removeClass("slide-bottom");
				}
            
           /*  $('.tkts').click(function(){
            	
            	 $("input").trigger("select");
            }) */
            
            $('.clr').click(function(){
           	 
           	 $("#txtFromDate").val('');
           	 $("#txtToDate").val('');
           	 $(".text").empty();
           	 $(".text").addClass('default')
           	 $(".text").html("Select");
           	 $('#ddlSite option:selected').removeAttr('selected');
           	 $('#ddlState option:selected').removeAttr('selected');
           	 $('#ddlCate option:selected').removeAttr('selected');
           	 $('#ddlPriority option:selected').removeAttr('selected'); 
           	window.location.href = './eventdetails';
           	//location.reload();
           	   });
            
            
            $('.close').click(function(){
				$('.clear').click();
				$('#tktCreation').hide();
				$('.clear').click();
			    $('.category').dropdown();
			    $('.SiteNames').dropdown();
			});
			$('.clear').click(function(){
			    $('.search').val("");
			});

			$('#ddlType').change(function(){              
                $('.category').dropdown('clear')
               });

			
			
			$('body').click(function(){
            	$('#builder').removeClass('open'); 
         	 });
			
			$('#fetchdata').hide();
	        $('#receivedata').hide();
	        $('#apidata').hide();
	        
	        
			$("#ddlOperationMode").change(function() {
				
	              var value = $('#ddlOperationMode option:selected').val();
	             
	              if(value == 0) {
	               $('#fetchdata').hide();
	               $('#receivedata').show();
	               $('#apidata').hide();
	              }
	              else if(value == 1) {
	                  $('#fetchdata').show();
	                  $('#receivedata').hide();
	                  $('#apidata').hide();

	                 }
	              else if(value == 2) {
	                  $('#fetchdata').hide();
	                  $('#receivedata').hide();
	                  $('#apidata').show();

	                 }
	              else{
	               $('#fetchdata').hide();
	               $('#receivedata').hide();
	               $('#apidata').hide();
	              }
	               // alert(value);
	           });

			 $("#ddlCountry").change(function() {
		            var value = $('#ddlCountry option:selected').val();
		            
		           
		            AjaxCallForStateID(value,varEquipmentType);
		            varEquipmentType = 0;
		           
		           
		            
		        });
			
			AjaxCallForPageLoad();
			
			
        });
      </script>

      <script type='text/javascript'>//<![CDATA[
         $(window).load(function(){
        	 $('.ui.dropdown').dropdown({forceSelection:false});
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
         
         
         $('.ui.dropdown').dropdown({
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: true, 
             showOnFocus: true,
             on: "click" 
           });

		$('.ui.dropdown.oveallsearch').dropdown({
             onChange: function (value, text, $selectedItem) {
                
                var uid =  $('#hdneampmuserid').val();
                redirectbysearchvalue(value,uid);
             },
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: false, 
             showOnFocus: true,
             on: "click" 
           });
		
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );//]]> 

            var validation  = {
                    txtSubject: {
                               identifier: 'txtSubject',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               },
                               {
                                   type: 'maxLength[50]',
                                   prompt: 'Please enter a value'
                                 }]
                             },
                              ddlSite: {
                               identifier: 'ddlSite',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                             },
                            ddlType: {
                                  identifier: 'ddlType',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                            ddlCategory: {
                               identifier: 'ddlCategory',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                             ddlPriority: {
                               identifier: 'ddlPriority',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please select a dropdown value'
                               }]
                             },
                             description: {
                               identifier: 'description',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please Enter Description'
                               }]
                             }
             };
               var settings = {
                 onFailure:function(){
                     return false;
                   }, 
                 onSuccess:function(){    
                   $('#btnCreate').hide();
                   $('#btnCreateDummy').show()
                   //$('#btnReset').attr("disabled", "disabled");          
                   }};           
               $('.ui.form.validation-form').form(validation,settings);
              })
         
      </script>
    
    
<style type="text/css">
.dropdown-menu-right {
	left: -70px !important;
}
/* .ui.selection.dropdown {
    cursor: pointer;
    word-wrap: break-word;
    line-height: 12px;
    white-space: normal;
    outline: 0;
    -webkit-transform: rotateZ(0deg);
    transform: rotateZ(0deg);
    min-width: 14em;
    min-height: 2.7142em;
    background: #FFFFFF;
    display: inline-block;
    padding: 0.78571429em 2.1em 0.78571429em 0.5em;
    color: #000;
    box-shadow: none;
    border: 1px solid rgba(34, 36, 38, 0.15);
    border-radius: 0.28571429rem;
    -webkit-transition: box-shadow 0.1s ease, width 0.1s ease;
    transition: box-shadow 0.1s ease, width 0.1s ease;
} */
/* .ui.search.dropdown > input.search {
    background: none transparent !important;
    border: none !important;
    box-shadow: none !important;
    cursor: text;
    top: 0em;
    left: -2px;
    width: 100%;
    outline: none;
    -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
    padding: inherit;
} */
.txtheight {
	    min-height: 28px;
    height: 20px;
}
div.dataTables_wrapper div.dataTables_length select {
			    width: 50px;
			    display: inline-block;
			    margin-left: 3px;
			    margin-right: 4px;
			}
			
			.ui.form textarea:not([rows]) {
    height: 4em;
    min-height: 4em;
    max-height: 24em;
}
.ui.icon.input > i.icon:before, .ui.icon.input > i.icon:after {
    left: 0;
    position: absolute;
    text-align: center;
    top: 36%;
    width: 100%;
}

.editbtn {
 	cursor:pointer;
 }
.evtDropdown {
	/* min-height: 2.7142em !important; 
font-size:13px !important;
padding: 0.78571429em 2.1em 0.78571429em 1em !important;  */
}

.input-sm, .form-horizontal .form-group-sm .form-control {
    font-size: 13px;
    min-height: 25px;
    height: 32px;
    padding: 8px 9px;
}
.ui.form .field > label {
     padding-left: 0px; 
    display: block;
    margin: 0em 0em 0.28571429rem 0em;
    /* color: rgba(0, 0, 0, 0.87); */
    /* font-size: 0.92857143em; */
    font-weight: 100;
    text-transform: none;
}
  
 .card-title a:hover {
 	text-decoration:none;
 }
#example td a {
	color: #337ab7;
	font-weight: bold;
}
.dt-button-background {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 0 !important;
}
.dropevent {
	    min-width: 100px;
    margin-left: -35px;
}
.dropevent li a {
	color:#000 !important;
	font-weight:normal !important;
}
.tbh-width {
	width: 150px;
	padding-right: 30px !important;
}

.dt-buttons {
	margin-left: 50px;
}

.buttons-collection {
	height: 28px;
	padding: 3px;
	/*  z-index:9999; */
}


.buttons-excel {
	background: rgba(36, 95, 68, 0.87);
	color: #FFF;
}

.buttons-pdf {
	background: rgb(98, 165, 96);
	color: #FFF;
}

.dataTables_filter {
	float: right;
}

.dataTables_length {
	float: left;
}
.ui.search.dropdown .menu {
    max-height: 12.02857143rem !important;
}

.ui.selection.dropdown {
    width: 100%;
    margin-top: 0.1em;
   /*  height: 2.3em; */
}

.ui.selection.dropdown .menu {
    width: 100%;
    white-space: normal !important;
}

.ui.selection.dropdown .menu .item:first-child {
    border-top: 1px solid #ddd !important;
    min-height: 1.8em;
}

.accordian {
	background: #efefef;
border-radius: 0px;
border-bottom: 0px;
/* padding: 20px 20px 7px 20px; */
position: relative;
z-index: 3;
min-height: 38px;
border: 1px solid #CCC !important;
}
.card-title a:hover {
	text-decoration:none;
}


.ui.selection.dropdown .icon {
    text-align: right;
   /*  margin-left: 7px !important; */
}

.rem-space{
    padding: 0px !important;
}
</style>

</head>
<body class="fixed-header">


	<input type="hidden" value="${access.userID}" id="hdneampmuserid">

<input type="hidden" value="${classtate}" id="hdnstate">

	<jsp:include page="slider.jsp"/>
	


	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					 <a href="./dashboard"><img src="resources/img/logo01.png" alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx tktSearch" id="SearcSElect">                         
                           <div class="ui  search selection  dropdown  width oveallsearch">
                                         <input id="myInput" name="tags" type="text">
                                         <div class="default text">search</div>                                        
                                         <div id="myDropdown" class="menu">

                                                <jsp:include page="searchselect.jsp" />	


                                         </div>
                                  </div>
                          
                          
                          
                          
                          
                        </div>
				
				 <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user_01.png" width="32" height="32">
                  </span>
                  
                   <c:if test="${not empty access}">         
                              <p class="user-login">${access.userName}</p>
                  </c:if>
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="logout1" class="dropdown-item color logout"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>               
               	
						<div class="newticketpopup hidden">
							<span data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><p class="center m-t-5 tkts"><img class="m-t-5" src="resources/img/tkt.png"><p class="create-tkts">New Ticket</p></p></span>
               			</div>
					
			</div>
		</div>
		<div class="page-content-wrapper" id="QuickLinkWrapper1">
			<div class="content ">
				<div class="container-fixed-lg">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="./dashboard"><i class="fa fa-home mt-7"></i></a></li>
                     <li class="breadcrumb-item"><a>Configuration</a></li>
                     <li class="breadcrumb-item active">Site Configuration</li>
                  </ol>
               </div>
               <div class="card card-transparent mb-0">
						<!-- Table Start-->
						  <form method="post" class="ui form noremal" id="postformid" style="margin-bottom:3px;">							
						<div class="padd-3 sortable">	
							<div class="card card-default bg-default padd-5" data-pages="card" style="margin-bottom:3px;">								
								<div class="row col-md-offset-0">                              
                              <div class="card card-transparent " id="tabs">
                                 <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
                                 	<li class="nav-item" >
                                       <a href="#tab4" class="active" data-toggle="tab" data-target="#slide4" id="tab-asset"><span>Asset Details</span></a>
                                    </li>
                                     <li class="nav-item" >
                                       <a href="#tab2" data-toggle="tab" data-target="#slide2" id="tab-contact"><span>Contact Details</span></a>
                                    </li>
                                    <li class="nav-item" >
                                       <a href="#tab1"  data-toggle="tab" data-target="#slide1" id="tab-contract"><span>Contract Details</span></a>
                                    </li>                                 
                                    <!-- <li class="nav-item" >
                                       <a href="#tab3" data-toggle="tab" data-target="#slide3" id="tab-documentation"><span>Documentation Details</span></a>
                                    </li> -->                                    
                                 </ul>
                                 <div class="tab-content">
                                    <div class="tab-pane" id="slide1">    
                                    	<div class="col-md-12 padd-0">
                                    		 <div class="sm-m-l-5 sm-m-r-5">
                           <div class="card-group horizontal padd-5" id="accordion" role="tablist" aria-multiselectable="true">
                              <div class="card card-default m-b-0 accordian" style="overflow:auto">
                                 <div class="card-header" role="tab" id="headingOne" style="       padding: 0px 10px !important;">
                                    <h4 class="card-title">
                                       <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                       Contract
                                       </a>
                                    </h4>
                                 </div>
                                 <div id="collapseOne" class="collapse show m-b-15" role="tabcard" aria-labelledby="headingOne">
                                    <div class="card-block padd-0">
                                       <div class="col-md-12" style="padding: 0px 10px !important; min-height: 150px;">    
                                                
                                                
                                                            <div class="col-md-4">
                                                <div class="field required m-t-6">
                                                    <label class="fields">Contract Type</label>
                                                    <div class="field">
                                                    	<select class="ui search selection dropdown" name="ddlTypeOfContract" id="ddlTypeOfContract">
												        	<option value="">Select</option>
												         	<option value="Annual">Annual</option>
						 								 	<option value="Biennial">Biennial</option> 
						 								  	<option value="Triennial">Triennial</option>
						 								 	<option value="One Time">One Time</option> 
						 								  	<option value="Audit">Audit</option>
						 								</select>
						 							</div>
                                                </div>
                                            </div>
                        
                         <div class="col-md-4">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Service Package</label>
                                                 <div class="field">
                                                 <select class="ui search selection dropdown" name="ddlServicePackage" id="ddlServicePackage">
											        <option value="">Select</option>
											         <option value="360 Asset Management">360 Asset Management</option>
					  								<option value="Tech Management">Tech Management</option>
											      </select>
                                                </div>
                                                </div>
                                            </div>         
                                          
                                            <div class="col-md-4">
                        <div class="field required m-t-6">
                             <label class="fields">Commencement of work</label>
                             <div class="field">
                             <!-- <input type="text" name="txtCommercementofwork"  id="txtCommercementofwork" placeholder="Commencement of Work">
                             --><input id="txtCommercementofwork"  autocomplete="off" name="txtCommercementofwork" type="text" placeholder="From Date" class="date-picker form-control" name="txtCommercementofwork"/>
															<!-- <label for="txtCommercementofwork" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span> </label>
													 -->
                            </div>
                            </div>
                        </div>
                                            
                                           
                                                    
                                        </div>	
                                           
                                        
                                    </div>
                                 </div>
                              </div>
                             
                          <div class="card card-default m-b-0 accordian" style="overflow:auto">
                                 <div class="card-header " role="tab" id="headingTwo" style="       padding: 0px 10px !important;">
                                    <h4 class="card-title">
                                       <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                       Basic Services
                                       </a>
                                    </h4>
                                 </div>
                                 <div id="collapseTwo" class="collapse m-b-15" role="tabcard" aria-labelledby="headingTwo">
                                    <div class="card-block padd-0">
                                       <div class="col-md-12" style="padding: 0px 10px !important;">                       
                                           <div class="col-md-4">
                                           <label class="fields">Cleaning Cycle per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                                <select class="ui search selection dropdown" name="ddlCleaningcycle" id="ddlCleaningcycle">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow1">
                                                 <select class="ui search selection dropdown" name="ddlCleaningCycle_Frequency" id="ddlCleaningCycle_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields">Corrective Visits per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                                <select class="ui search selection dropdown" name="ddlCorrectiveVisits" id="ddlCorrectiveVisits">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow2">
                                                 <select class="ui search selection dropdown" name="ddlCorrectiveVisits_Frequency" id="ddlCorrectiveVisits_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields">Asses Health Check Visit per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                                <select class="ui search selection dropdown" name="ddlAssethealth" id="ddlAssethealth">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow3">
                                                 <select class="ui search selection dropdown" name="ddlAssethealth_Frequency" id="ddlAssethealth_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                                 
                                        </div>	
                                           <div class="col-md-12   m-t-5" style="padding: 0px 10px !important;">                       
                                           <div class="col-md-4">
                                           <label class="fields">Complete Preventive Check Visit per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                                <select class="ui search selection dropdown" name="ddlCompletePreventive" id="ddlCompletePreventive">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow4">
                                                 <select class="ui search selection dropdown" name="ddlCompletePreventive_Frequency" id="ddlCompletePreventive_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields">Joint Meter Reading Visit</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                                <select class="ui search selection dropdown" name="ddlJmr_Service" id="ddlJmr_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow5">
                                                <select class="ui search selection dropdown" name="ddlJmr_Frequency" id="ddlJmr_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select> 
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields">Vegetation Management per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                               <select class="ui search selection dropdown" name="ddlVegetation_Management" id="ddlVegetation_Management">
												        <option value="">Select</option>
												         <option value="Yes">Yes</option>
						  								<option value="No">No</option>
												      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow6">
                                                 <select class="ui search selection dropdown" name="ddlVm_Frequency" id="ddlVm_Frequency">
											        <option value="">Select</option>
											         <option value="Bi weekly">Bi weekly</option>
					 								 <option value="Monthly">Monthly</option> 
					 								  <option value="Quarterly">Quarterly</option>
					 								 <option value="Half yearly">Half yearly</option> 
					 								  <option value="Annually">Annually</option>
					 								   <option value="Once in two year">Once in two year</option> 
					 								  <option value="36 cycles">36 cycles</option>
					 								 <option value="48 cycles">48 cycles</option> 
					 								 
					 								</select>
                                                </div>
                                                                         
                                            </div>
                                          
                                        </div>
                                        
                                         <div class="col-md-12" style="padding: 0px 10px !important;">                       
                                           <div class="col-md-4">
                                           <label class="fields">Plant Security Service</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                                <select class="ui search selection dropdown" name="ddlSecurity_Service" id="ddlSecurity_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow7">
                                                 <select class="ui search selection dropdown" name="ddlSecurity_Frequency" id="ddlSecurity_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields">Warranty Support Management</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                                <select class="ui search selection dropdown" name="ddlWarranty" id="ddlWarranty">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow8">
                                                 <select class="ui search selection dropdown" name="ddlWarranty_Frequency" id="ddlWarranty_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields">Insurance Support Management</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                                <select class="ui search selection dropdown" name="ddlInsurance" id="ddlInsurance">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow9">
                                                 <select class="ui search selection dropdown" name="ddlInsurance_Frequency" id="ddlInsurance_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                                 
                                        </div>	
                                        
                                         <div class="col-md-12" style="padding: 0px 10px !important;min-height: 150px;">                       
                                           <div class="col-md-4" style="padding: 0px 7px;">
                                           <label class="fields">Spares Management</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                               <select class="ui search selection dropdown" name="ddlSpares_Management" id="ddlSpares_Management">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow10">
                                               <select class="ui search selection dropdown" name="ddlSm_Frequency" id="ddlSm_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                            
                                          
                                                 
                                        </div>	
                                        
                                        
                                    </div>
                                 </div>
                              </div>
                               <div class="card card-default m-b-0 accordian" style="overflow:auto">
                                 <div class="card-header " role="tab" id="headingThree" style="       padding: 0px 10px !important;">
                                    <h4 class="card-title">
                                       <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseTwo">
                                       Advanced Support
                                       </a>
                                    </h4>
                                 </div>
                                 <div id="collapseThree" class="collapse m-b-15" role="tabcard" aria-labelledby="headingThree">
                                    <div class="card-block padd-0">
                                      <div class="col-md-12" style="padding: 0px 10px !important;">                       
                                           <div class="col-md-4">
                                           <label class="fields">Switch Yard Maintenance per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                                <select class="ui search selection dropdown" name="ddlSym_Service" id="ddlSym_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow11">
                                                  <select class="ui search selection dropdown" name="ddlSym_Frequency" id="ddlSym_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields">RO Water Maintenance per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                               <select class="ui search selection dropdown" name="ddlRowfc_Service" id="ddlRowfc_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select> 
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow12">
                                                 <select class="ui search selection dropdown" name="ddlRowfc_Frequency" id="ddlRowfc_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields">Calibration of Meters per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                              <select class="ui search selection dropdown" name="ddlCom_Service" id="ddlCom_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow13">
                                                   <select class="ui search selection dropdown" name="ddlCom_Frequency" id="ddlCom_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                                 
                                        </div>
                                          <div class="col-md-12" style="padding: 0px 10px !important;">                       
                                           <div class="col-md-4">
                                           <label class="fields">Day Ahead Forecasting</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                              <select class="ui search selection dropdown" name="ddlDaf_Service" id="ddlDaf_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow14">
                                                 <select class="ui search selection dropdown" name="ddlDaf_Frequency" id="ddlDaf_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields">Drone Inspection per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                               <select class="ui search selection dropdown" name="ddlDi_Service" id="ddlDi_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow15">
                                              <select class="ui search selection dropdown" name="ddlDi_Frequency" id="ddlDi_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields">Drone Thermal Imaging per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                                <select class="ui search selection dropdown" name="ddlDti_Service" id="ddlDti_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow16">
                                               <select class="ui search selection dropdown" name="ddlDti_Frequency" id="ddlDti_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                                 
                                        </div>
                                        
                                        <div class="col-md-12" style="padding: 0px 10px !important;">                       
                                           <div class="col-md-4">
                                           <label class="fields">EL Testing of modules Lab per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                                 <select class="ui search selection dropdown" name="ddlEltml_Service" id="ddlEltml_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow17">
                                                 <select class="ui search selection dropdown" name="ddlEltml_Frequency" id="ddlEltml_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select> 
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields">EL Testing of modules Site per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                              <select class="ui search selection dropdown" name="ddlEltms_Service" id="ddlEltms_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow18">
                                               <select class="ui search selection dropdown" name="ddlEltms_Frequency" id="ddlEltms_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields">Relay Testing per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                                 <select class="ui search selection dropdown" name="ddlRt_Service" id="ddlRt_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow19">
                                                <select class="ui search selection dropdown" name="ddlRt_Frequency" id="ddlRt_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                                 
                                        </div>
                                        
                                        <div class="col-md-12" style="padding: 0px 10px !important;">                       
                                           <div class="col-md-4">
                                           <label class="fields">SLDC Documentation per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                             <select class="ui search selection dropdown" name="ddlSldc_Documentation" id="ddlSldc_Documentation">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow20">
                                               <select class="ui search selection dropdown" name="ddlSldc_Frequency" id="ddlSldc_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields">String Analysis per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                                <select class="ui search selection dropdown" name="ddlSact_Service" id="ddlSact_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow21">
                                              <select class="ui search selection dropdown" name="ddlSact_Frequency" id="ddlSact_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select> 
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields">Transformer Basic Test per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                                <select class="ui search selection dropdown" name="ddlTbt_Service" id="ddlTbt_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow22">
                                               <select class="ui search selection dropdown" name="ddlTbt_Frequency" id="ddlTbt_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                                 
                                        </div>
                                        
                                        <div class="col-md-12" style="padding: 0px 10px !important;min-height: 150px;">                       
                                           <div class="col-md-4">
                                           <label class="fields">Transformer Advance Test per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                               <select class="ui search selection dropdown" name="ddlTat_Service" id="ddlTat_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow23">
                                                <select class="ui search selection dropdown" name="ddlTat_Frequency" id="ddlTat_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields">Transmission Line Supervision per Year</label>
                                               
                                                      <div class="col-md-6 rem-space">
                                            <select class="ui search selection dropdown" name="ddlTls_Service" id="ddlTls_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select>
                                                </div>
                                                <div class="col-md-6 rem-space" id="toshow24">
                                              <select class="ui search selection dropdown" name="ddlTls_Frequency" id="ddlTls_Frequency">
						        <option value="">Select</option>
						         <option value="Bi weekly">Bi weekly</option>
 								 <option value="Monthly">Monthly</option> 
 								  <option value="Quarterly">Quarterly</option>
 								 <option value="Half yearly">Half yearly</option> 
 								  <option value="Annually">Annually</option>
 								   <option value="Once in two year">Once in two year</option> 
 								  <option value="36 cycles">36 cycles</option>
 								 <option value="48 cycles">48 cycles</option> 
 								 </select>
                                                </div>
                                                                         
                                            </div>
                                            
                                             <div class="col-md-4">
                                           <label class="fields"></label>
                                               
                                                      <div class="col-md-6 rem-space">
                                               
                                                </div>
                                                <div class="col-md-6 rem-space">
                                                
                                                </div>
                                                                         
                                            </div>
                                                 
                                        </div>
                                        
                                       
                                        
                                    </div>
                                 </div>
                              </div>
                              
                              
                              <div class="card card-default m-b-0 accordian" style="overflow:auto">
                                 <div class="card-header " role="tab" id="headingFour" style="       padding: 0px 10px !important;">
                                    <h4 class="card-title">
                                       <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                     Repair & Revamp
                                       </a>
                                    </h4>
                                 </div>
                                 <div id="collapseFour" class="collapse m-b-15" role="tabcard" aria-labelledby="headingThree">
                                    <div class="card-block padd-0">
                                      <div class="col-md-12 padd-0" style="padding: 0px 10px !important;">                       
                                             <div class="col-md-4">
					                           <div class="field required m-t-6">
                             <label class="fields">Equipment Repair</label>
                             <div class="field">
                               <select class="ui search selection dropdown" name="ddlEquipmentRepair" id="ddlEquipmentRepair">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select> </div>
                            </div>
					                        </div>  
					                        
					                         <div class="col-md-4">
					                        <div class="field required m-t-6">
                             <label class="fields">Revamp AC HT Side Service</label>
                             <div class="field">
                               <select class="ui search selection dropdown" name="ddlRevampacht_Service" id="ddlRevampacht_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select> </div>
                            </div>
					                        </div>  
					                        
					                         <div class="col-md-4">
					                        <div class="field required m-t-6">
                                <label class="fields">Revamp AC LT Side Service</label>
                                <div class="field">
                                <select class="ui search selection dropdown" name="ddlRevampaclt_Service" id="ddlRevampaclt_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select> </div>
                            </div>
					                        </div>  
                                            
                                                   
                                        </div>		
                                           <div class="col-md-12   m-t-5" style="padding: 0px 10px !important;min-height: 150px;">                       
                                            
                                           <div class="col-md-4">
					                        <div class="field required m-t-6">
                             <label class="fields">Revamp Civil Service</label>
                             <div class="field">
                              <select class="ui search selection dropdown" name="ddlRevampacht_Service_Civil" id="ddlRevampacht_Service_Civil">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select> </div>
                            </div>
					                        </div>  
					                        
					                         <div class="col-md-4">
					                        <div class="field required m-t-6">
                             <label class="fields">Revamp DC Side Service</label>
                             <div class="field">
                               <select class="ui search selection dropdown" name="ddlRevampdc_Service" id="ddlRevampdc_Service">
						        <option value="">Select</option>
						         <option value="Yes">Yes</option>
  								<option value="No">No</option>
						      </select> </div>
                            </div>
					                        </div>  
					                        
					                         
                                                    
                                        </div>
                                        
                                    </div>
                                 </div>
                              </div>
                                <div class="card card-default m-b-0 accordian" style="overflow:auto">
                                 <div class="card-header " role="tab" id="headingFive" style="       padding: 0px 10px !important;">
                                    <h4 class="card-title">
                                       <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                     Service Level Agreement
                                       </a>
                                    </h4>
                                 </div>
                                 <div id="collapseFive" class="collapse m-b-15" role="tabcard" aria-labelledby="headingThree">
                                    <div class="card-block padd-0">
                                      <div class="col-md-12 padd-0" style="padding: 0px 10px !important;">                       
                                             <div class="col-md-4">
					                         <div class="field required m-t-6">
                             <label class="fields">Plant Down Assigned</label>
                             <div class="field">
                              <select class="ui search selection dropdown" name="ddlPlantdown_Assigned" id="ddlPlantdown_Assigned">
						        <option value="">Select</option>
						         <option value="1 hour">1 hour</option>
 								 <option value="2 hours">2 hours</option> 
 								  <option value="4 hours">4 hours</option>
 								 <option value="8 hours">8 hours</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         <div class="col-md-4">
					                       <div class="field required m-t-6">
                             <label class="fields">Plant Down Resolved</label>
                             <div class="field">
                              <select class="ui search selection dropdown" name="ddlPlantdown_Resolved" id="ddlPlantdown_Resolved">
						        <option value="">Select</option>
						         <option value="1 hour">1 hour</option>
 								 <option value="2 hours">2 hours</option> 
 								  <option value="4 hours">4 hours</option>
 								 <option value="8 hours">8 hours</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         <div class="col-md-4">
					                        <div class="field required m-t-6">
                             <label class="fields">Plant Warning Assigned</label>
                             <div class="field">
                                <select class="ui search selection dropdown" name="ddlPlantwarning_Assigned" id="ddlPlantwarning_Assigned">
						        <option value="">Select</option>
						         <option value="1 hour">1 hour</option>
 								 <option value="2 hours">2 hours</option> 
 								  <option value="4 hours">4 hours</option>
 								 <option value="8 hours">8 hours</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
                                            
                                                   
                                        </div>		
                                           <div class="col-md-12   m-t-5" style="padding: 0px 10px !important;">                       
                                            
                                           <div class="col-md-4">
					                        <div class="field required m-t-6">
                             <label class="fields">Plant Down Issue Identified</label>
                             <div class="field">
                                <select class="ui search selection dropdown" name="ddlPlantdown_IssueIdentified" id="ddlPlantdown_IssueIdentified">
						        <option value="">Select</option>
						         <option value="1 hour">1 hour</option>
 								 <option value="2 hours">2 hours</option> 
 								  <option value="4 hours">4 hours</option>
 								 <option value="8 hours">8 hours</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         <div class="col-md-4">
					                        <div class="field required m-t-6">
                             <label class="fields">Plant Warning Issue Identified</label>
                             <div class="field">
                               <select class="ui search selection dropdown" name="ddlPlantwarning_IssueIdentified" id="ddlPlantwarning_IssueIdentified">
						        <option value="">Select</option>
						         <option value="1 hour">1 hour</option>
 								 <option value="2 hours">2 hours</option> 
 								  <option value="4 hours">4 hours</option>
 								 <option value="8 hours">8 hours</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                        
					                          <div class="col-md-4">
					                       <div class="field required m-t-6">
                             <label class="fields">Plant Warning Resolved</label>
                             <div class="field">
                               <select class="ui search selection dropdown" name="ddlPlantwarning_Resolved" id="ddlPlantwarning_Resolved">
						        <option value="">Select</option>
						         <option value="1 hour">1 hour</option>
 								 <option value="2 hours">2 hours</option> 
 								  <option value="4 hours">4 hours</option>
 								 <option value="8 hours">8 hours</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         
                                                    
                                        </div>
                                        
                                        
                                        
                                         <div class="col-md-12   m-t-5" style="padding: 0px 10px !important;">                       
                                            
                                           <div class="col-md-4">
					                      <div class="field required m-t-6">
                             <label class="fields">Equipment Generating Down Assigned</label>
                             <div class="field">
                                 <select class="ui search selection dropdown" name="ddlEqugendown_Assigned" id="ddlEqugendown_Assigned">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         <div class="col-md-4">
					                       <div class="field required m-t-6">
                             <label class="fields">Equipment Generating Down Issue Identified</label>
                             <div class="field">
                               <select class="ui search selection dropdown" name="ddlEqugendown_Issueidentified" id="ddlEqugendown_Issueidentified">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                        
					                          <div class="col-md-4">
					                     <div class="field required m-t-6">
                             <label class="fields">Equipment Generating Down Ressolved</label>
                             <div class="field">
                               <select class="ui search selection dropdown" name="ddlEqugendown_Resolved" id="ddlEqugendown_Resolved">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         
                                                    
                                        </div>
                                        
                                        
                                          <div class="col-md-12   m-t-5" style="padding: 0px 10px !important;">                       
                                            
                                           <div class="col-md-4">
					                      <div class="field required m-t-6">
                             <label class="fields">Equipment Generating Offline Assigned</label>
                             <div class="field">
                                <select class="ui search selection dropdown" name="ddlEqugenoffline_Assigned" id="ddlEqugenoffline_Assigned">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         <div class="col-md-4">
					                     <div class="field required m-t-6">
                             <label class="fields">Equipment Generating Offline Issue Identified</label>
                             <div class="field">
                                <select class="ui search selection dropdown" name="ddlEqugenoffline_Issueidentified" id="ddlEqugenoffline_Issueidentified">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                        
					                          <div class="col-md-4">
					                     <div class="field required m-t-6">
                             <label class="fields">Equipment Generating Offline Resolved</label>
                             <div class="field">
                                <select class="ui search selection dropdown" name="ddlEqugenoffline_Resolved" id="ddlEqugenoffline_Resolved">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         
                                                    
                                        </div>
                                        
                                        
                                        
                                        
                                          <div class="col-md-12   m-t-5" style="padding: 0px 10px !important;">                       
                                            
                                           <div class="col-md-4">
					                       <div class="field required m-t-6">
                             <label class="fields">Equipment Generating Warning Assigned</label>
                             <div class="field">
                                <select class="ui search selection dropdown" name="ddlEqugenwarning_Assigned" id="ddlEqugenwarning_Assigned">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         <div class="col-md-4">
					                      <div class="field required m-t-6">
                             <label class="fields">Equipment Generating Warning Issue Identified</label>
                             <div class="field">
                               <select class="ui search selection dropdown" name="ddlEqugenwarning_Issueidentified" id="ddlEqugenwarning_Issueidentified">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                        
					                          <div class="col-md-4">
					                     <div class="field required m-t-6">
                             <label class="fields">Equipment Generating Warning Resolved</label>
                             <div class="field">
                                 <select class="ui search selection dropdown" name="ddlEqugenwarning_Resolved" id="ddlEqugenwarning_Resolved">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         
                                                    
                                        </div>
                                        
                                        
                                        
                                          <div class="col-md-12   m-t-5" style="padding: 0px 10px !important;">                       
                                            
                                           <div class="col-md-4">
					                      <div class="field required m-t-6">
                             <label class="fields">Equipment Non Generating Down Assigned</label>
                             <div class="field">
                               <select class="ui search selection dropdown" name="ddlEquNongendown_Assigned" id="ddlEquNongendown_Assigned">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         <div class="col-md-4">
					                     <div class="field required m-t-6">
                             <label class="fields">Equipment Non Generating Down Issue Identified</label>
                             <div class="field">
                               <select class="ui search selection dropdown" name="ddlEquNongendown_Issueidentified" id="ddlEquNongendown_Issueidentified">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                        
					                          <div class="col-md-4">
					                     <div class="field required m-t-6">
                             <label class="fields">Equipment Non Generating Down Resolved</label>
                             <div class="field">
                                <select class="ui search selection dropdown" name="ddlEquNongendown_Resolved" id="ddlEquNongendown_Resolved">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         
                                                    
                                        </div>
                                        
                                        
                                        
                                         <div class="col-md-12   m-t-5" style="padding: 0px 10px !important;">                       
                                            
                                           <div class="col-md-4">
					                     <div class="field required m-t-6">
                             <label class="fields">Equipment Non Generating Offline Assigned</label>
                             <div class="field">
                                <select class="ui search selection dropdown" name="ddlEquNongenoffline_Assigned" id="ddlEquNongenoffline_Assigned">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         <div class="col-md-4">
					                     <div class="field required m-t-6">
                             <label class="fields">Equipment Non Generating Offline Issue Identified</label>
                             <div class="field">
                              <select class="ui search selection dropdown" name="ddlEquNongenoffline_Issueidentified" id="ddlEquNongenoffline_Issueidentified	">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                        
					                          <div class="col-md-4">
					                     <div class="field required m-t-6">
                             <label class="fields">Equipment Non Generating Offline Resolved</label>
                             <div class="field">
                             <select class="ui search selection dropdown" name="ddlEquNongenoffline_Resolved" id="ddlEquNongenoffline_Resolved">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         
                                                    
                                        </div>
                                        
                                        
                                        
                                        
                                         <div class="col-md-12   m-t-5" style="padding: 0px 10px !important; min-height: 150px;">                       
                                            
                                           <div class="col-md-4">
					                      <div class="field required m-t-6">
                             <label class="fields">Equipment Non Generating Warning Assigned</label>
                             <div class="field">
                               <select class="ui search selection dropdown" name="ddlEquNongenwarning_Assigned" id="ddlEquNongenwarning_Assigned">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         <div class="col-md-4">
					                    <div class="field  m-t-6">
                             <label class="fields">Equipment Non Generating Warning Issue Identified</label>
                             <div class="field">
                               <select class="ui search selection dropdown" name="ddlEquNongenwarning_Issueidentified" id="ddlEquNongenwarning_Issueidentified">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                        
					                          <div class="col-md-4">
					                    <div class="field required m-t-6">
                             <label class="fields">Equipment Non Generating Warning Resolved</label>
                             <div class="field">
                               <select class="ui search selection dropdown" name="ddlEquNongenwarning_Resolved" id="ddlEquNongenwarning_Resolved">
						        <option value="">Select</option>
						         <option value="5 mins">5 mins</option>
 								 <option value="10 mins">10 mins</option> 
 								  <option value="15 mins">15 mins</option>
 								 <option value="30 mins">30 mins</option> 
 								  <option value="45 mins">45 mins</option> 
 								 
 								 </select> </div>
                            </div>
					                        </div>  
					                        
					                         
                                                    
                                        </div>
                                        
                                        
                                        
                                    </div>
                                 </div>
                              </div>
                              
                              
                              <div class="col-md-12 m-t-15" >
                              	
                              			<div class="col-md-1 col-xs-2">
               					    		<a id="tab-contact-prev" class="btn btn-primary btnPrev confi-btn-width" >Prev</a>
               					    	</div>  
                                                
                                                <div class="col-md-10 col-xs-8 center">
               					    		<div class="btn btn-success submit confi-btn-width"  id="btnSubmit" >Submit</div>
                  							<div class="btn btn-primary clear m-l-15 confi-btn-width" id="btnClear">Reset</div>
               					    	</div>                          
                                        	<!-- <div class="btn btn-success  submit">Submit</div>
                 	  						<div class="btn btn-primary clear m-l-15">Reset</div> -->
                                        </div>
                             <!--  <div class="col-md-12 center m-t-15">
                              	<button class="btn btn-success">Submit</button>
                              </div> -->
                           </div>
                        </div>
                                    	</div>                                
                                    
                                        
                                        
                                        
                                     
                                    </div>
                                    <div class="tab-pane" id="slide2">    
                                    	<div class="col-md-12 padd-0">
                                    		 <div class="sm-m-l-5 sm-m-r-5">
                           <div class="card-group horizontal padd-5" id="accordion" role="tablist" aria-multiselectable="true">
                              <div class="card card-default m-b-0 accordian">
                                 <div class="card-header" role="tab" id="headingOne" style="       padding: 0px 10px !important;">
                                    <h4 class="card-title">
                                       <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1">
                                       Desicion Maker
                                       </a>
                                    </h4>
                                 </div>
                                 <div id="collapseOne1" class="collapse show m-b-15" role="tabcard" aria-labelledby="headingOne">
                                    <div class="card-block padd-0">
                                       <div class="col-md-12" style="padding: 0px 10px !important;">                       
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                      <label class="fields">Contact Person</label>
                                                      <div class="field">
                                                 <input type="text" name="txtDecisionMaker"  id="txtDecisionMaker" placeholder="Contact Person">
                                                </div>
                                                </div>                          
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                    <label class="fields">Address</label>
                                                    <div class="field">
                                                     <input type="text" name="txtDm_Address"  id="txtDm_Address" placeholder="Address">
                                                </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">City</label>
                                                 <div class="field">
                                                    <input type="text" name="txtDm_City"  id="txtDm_City" placeholder="City">
                                                </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                      <label class="fields">Country</label>
                                                      <div class="field">
                                                 <input type="text" name="txtDm_Country"  id="txtDm_Country" placeholder="Country">
                                                </div>
                                                </div>                          
                                            </div>
                                                    
                                        </div>	
                                           <div class="col-md-12   m-t-5" style="padding: 0px 10px !important;">                       
                                            
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">State</label>
                                                 <div class="field">
                                                    <input type="text" name="txtDm_State"  id="txtDm_State" placeholder="State">
                                                </div>
                                                </div>
                                            </div> 
                                            
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                    <label class="fields">Postal Code</label>
                                                    <div class="field">
                                                     <input type="text" name="txtDm_PostalCode"  id="txtDm_PostalCode" placeholder="Postal Code">
                                                </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Email Id</label>
                                                 <div class="field">
                                                    <input type="text" name="txtDm_EmailID"  id="txtDm_EmailID" placeholder="Email Id">
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Phone No</label>
                                                 <div class="field">
                                                    <input type="text" name="txtDm_PhoneNO"  id="txtDm_PhoneNO" placeholder="Phone No">
                                                </div>
                                                </div>
                                            </div>         
                                        </div>
                                        
                                    </div>
                                 </div>
                              </div>
                             
                          <div class="card card-default m-b-0 accordian">
                                 <div class="card-header " role="tab" id="headingTwo" style="       padding: 0px 10px !important;">
                                    <h4 class="card-title">
                                       <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                                       Champion
                                       </a>
                                    </h4>
                                 </div>
                                 <div id="collapseTwo2" class="collapse m-b-15" role="tabcard" aria-labelledby="headingTwo">
                                    <div class="card-block padd-0">
                                       <div class="col-md-12" style="padding: 0px 10px !important;">                       
                                           <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                      <label class="fields">Contact Person</label>
                                                      <div class="field">
                                                 <input type="text" name="txtChampion"  id="txtChampion" placeholder="Contact Person">
                                                </div>
                                                </div>                          
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                    <label class="fields">Address</label>
                                                    <div class="field">
                                                     <input type="text" name="txtCh_Address"  id="txtCh_Address" placeholder="Address">
                                                </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">City</label>
                                                 <div class="field">
                                                    <input type="text" name="txtCh_City"  id="txtCh_City" placeholder="City">
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">State</label>
                                                 <div class="field">
                                                    <input type="text" name="txtCh_State"  id="txtCh_State" placeholder="State">
                                                </div>
                                                </div>
                                            </div>          
                                        </div>	
                                           <div class="col-md-12   m-t-5" style="padding: 0px 10px !important;">                       
                                           <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                      <label class="fields">Country</label>
                                                      <div class="field">
                                                 <input type="text" name="txtCh_Country"  id="txtCh_Country" placeholder="Country">
                                                </div>
                                                </div>                          
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                    <label class="fields">Postal Code</label>
                                                    <div class="field">
                                                     <input type="text" name="txtCh_PostalCode"  id="txtCh_PostalCode" placeholder="Postal Code">
                                                </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Email Id</label>
                                                 <div class="field">
                                                    <input type="text" name="txtCh_EmailID"  id="txtCh_EmailID" placeholder="Email Id">
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Phone No</label>
                                                 <div class="field">
                                                    <input type="text" name="txtCh_PhoneNO"  id="txtCh_PhoneNO" placeholder="Phone No">
                                                </div>
                                                </div>
                                            </div>          
                                        </div>
                                        
                                        
                                    </div>
                                 </div>
                              </div>
                               <div class="card card-default m-b-0 accordian">
                                 <div class="card-header " role="tab" id="headingThree" style="       padding: 0px 10px !important;">
                                    <h4 class="card-title">
                                       <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3">
                                       Road Block
                                       </a>
                                    </h4>
                                 </div>
                                 <div id="collapseThree3" class="collapse m-b-15" role="tabcard" aria-labelledby="headingThree">
                                    <div class="card-block padd-0">
                                       <div class="col-md-12" style="padding: 0px 10px !important;">                       
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                      <label class="fields">Contact Person</label>
                                                      <div class="field">
                                                 <input type="text" name="txtRoadBlock"  id="txtRoadBlock" placeholder="Contact Person">
                                                </div>
                                                </div>                          
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                    <label class="fields">Address</label>
                                                    <div class="field">
                                                     <input type="text" name="txtRd_Address"  id="txtRd_Address" placeholder="Address">
                                                </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">City</label>
                                                 <div class="field">
                                                    <input type="text" name="txtRd_City"  id="txtRd_City" placeholder="City">
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">State</label>
                                                 <div class="field">
                                                    <input type="text" name="txtRd_State"  id="txtRd_State" placeholder="State">
                                                </div>
                                                </div>
                                            </div>                 
                                        </div>	
                                           <div class="col-md-12   m-t-5" style="padding: 0px 10px !important;">                       
                                          <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                      <label class="fields">Country</label>
                                                      <div class="field">
                                                 <input type="text" name="txtRd_Country"  id="txtRd_Country" placeholder="Country">
                                                </div>
                                                </div>                          
                                            </div>                                            
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                    <label class="fields">Postal Code</label>
                                                    <div class="field">
                                                     <input type="text" name="txtRd_PostalCode"  id="txtRd_PostalCode" placeholder="Postal Code">
                                                </div>
                                                </div>
                                            </div>                                            
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Email Id</label>
                                                 <div class="field">
                                                    <input type="text" name="txtRd_EmailID"  id="txtRd_EmailID" placeholder="Email Id">
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Phone No</label>
                                                 <div class="field">
                                                    <input type="text" name="txtRd_PhoneNO"  id="txtRd_PhoneNO" placeholder="Phone No">
                                                </div>
                                                </div>
                                            </div>         
                                        </div>
                                        
                                    </div>
                                 </div>
                              </div>
                              
                              
                              <div class="card card-default m-b-0 accordian">
                                 <div class="card-header " role="tab" id="headingFour" style="       padding: 0px 10px !important;">
                                    <h4 class="card-title">
                                       <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour4" aria-expanded="false" aria-controls="collapseFour4">
                                      Key Deliverables
                                       </a>
                                    </h4>
                                 </div>
                                 <div id="collapseFour4" class="collapse m-b-15" role="tabcard" aria-labelledby="headingThree">
                                    <div class="card-block padd-0">
                                      <div class="col-md-12 padd-0" style="padding: 0px 10px !important;">                       
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                      <label class="fields">Contact Person</label>
                                                      <div class="field">
                                                 <input type="text" name="txtKeyDeliverables"  id="txtKeyDeliverables" placeholder="Contact Person">
                                                </div>
                                                </div>                          
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                    <label class="fields">Address</label>
                                                    <div class="field">
                                                     <input type="text" name="txtKd_Address"  id="txtKd_Address" placeholder="Address">
                                                </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">City</label>
                                                 <div class="field">
                                                    <input type="text" name="txtKd_City"  id="txtKd_City" placeholder="City">
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">State</label>
                                                 <div class="field">
                                                    <input type="text" name="txtKd_State"  id="txtKd_State" placeholder="State">
                                                </div>
                                                </div>
                                            </div>         
                                        </div>		
                                           <div class="col-md-12   m-t-5" style="padding: 0px 10px !important;">                       
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                      <label class="fields">Country</label>
                                                      <div class="field">
                                                 <input type="text" name="txtKd_Country"  id="txtKd_Country" placeholder="Country">
                                                </div>
                                                </div>                          
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                    <label class="fields">Postal Code</label>
                                                    <div class="field">
                                                     <input type="text" name="txtKd_PostalCode"  id="txtKd_PostalCode" placeholder="Postal Code">
                                                </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Email Id</label>
                                                 <div class="field">
                                                    <input type="text" name="txtKd_EmailID"  id="txtKd_EmailID" placeholder="Email Id">
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Phone No</label>
                                                 <div class="field">
                                                    <input type="text" name="txtKd_PhoneNO"  id="txtKd_PhoneNO" placeholder="Phone No">
                                                </div>
                                                </div>
                                            </div>         
                                        </div>
                                        
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-12 m-t-15" >
                              	
                              			<a id="tab-asset-prev" class="btn btn-primary btnPrev confi-btn-width" style="float:left;" >Prev</a>
                                         <a id="tab-contract-next" class="btn btn-primary btnNext confi-btn-width" style="float:right;">Next</a>                                  
                                        	<!-- <div class="btn btn-success  submit">Submit</div>
                 	  						<div class="btn btn-primary clear m-l-15">Reset</div> -->
                                        </div>
                             <!--  <div class="col-md-12 center m-t-15">
                              	<button class="btn btn-success">Submit</button>
                              </div> -->
                           </div>
                        </div>
                                    	</div>                                
                                    
                                        
                                        
                                        
                                     
                                    </div>
                                   <!--  <div class="tab-pane slide-left" id="slide3">
                                    	<div class="col-md-12 m-t-15 center" >
                              				<div class="col-md-3 mm-10 padd-0">
                              					<div class="col-md-2">
                              						 <div class="field  m-b-0">
			                							<label class="fields p-l-0 m-t-5">Logo</label>
			             	   						 </div>
                              					</div>
                              					<div class="col-md-10">
                              						 <div class="form-group">
			             	   							 <div class="row">
									             	    	<div class="col-md-10">
									             	    		<input type="file" class="" data-icon="false" id="excelfile" name ="file" multiple="multiple"  style="border:none;">
									             	    	</div>
									             	    	<div class="col-md-2">
									             	    		<a href="#" data-toggle="tooltip" data-placement="bottom"
						       title="" data-original-title="Image format should be in jpg / png / svg. Image size should be less than 5 MB."
						       class="red-tooltip"><i class="fa fa-info-circle" style="font-size:32px;color:#000;"></i></a>
									             	    	</div>
			             	    						</div>
                                          			</div>
                              					</div>
			               	
			             	   
			             	 </div>                            
                                        </div>
                                      	<div class="col-md-12 m-t-15 center" >
                              				<a id="tab-contract-prev" class="btn btn-primary btnPrev confi-btn-width" >Prev</a>                               
                                        </div>
                                    </div> -->
                                    
                                    
                                    
                                      <div class="tab-pane active" id="slide4">
                                        
                                         <div class="col-md-12   m-t-5 padd-0">                       
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                       <label class="fields">Site Code</label>
                                                      <input type="hidden" id="hdnSiteId"  value="Auto-Generation"/>
    			 			<label class="fields" id="lblSiteCode">Auto-Generation</label>	
                                                </div>                          
                                            </div>
                                            
                                            
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Customer</label>
                                                 <div class="field">
    			 			 	<select class="ui search selection dropdown"  name="ddlCustomer" id="ddlCustomer">
						        <option value="">Select</option>
						      </select>
    			 			</div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                    <label class="fields">Site Type</label>
                                                    <div class="field">
    			 			 <select class="ui search selection dropdown" name="ddlSiteType" id="ddlSiteType">
						        <option value="">Select</option>
						      </select>
						    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Site Name</label>
                                                 <div class="field">
    			 				<input type="text" name="txtSiteName"  autocomplete="off" id="txtSiteName" placeholder="Site Name">
    			 			</div>
                                                </div>
                                            </div>         
                                        </div>
                                        
                                   <div class="col-md-12   m-t-5 padd-0">  
                                   
                                   
                                   
                                    <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                    <label class="fields">Latitude</label>
                                                    <div class="field">
    			 				<input id="txtLatitude" name="txtLatitude" autocomplete="off" placeholder="Latitude" type="text">
						    </div>
                                                </div>
                                            </div>
                                            
                                            
                                             <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Longitude</label>
                                                 <div class="field">
    			 				<input id="txtLongitude" name="txtLongitude" autocomplete="off" placeholder="Longitude" type="text">
    			 			</div>
                                                </div>
                                            </div>
                                            
                                            
                                             <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                    <label class="fields">Site Location URL</label>
                                                    <div class="field">
    			 				<input id="txtLocationMap" autocomplete="off" name="txtLocationMap" placeholder="Site Location URL" type="text">
						    </div>
                                                </div>
                                            </div>
                                            
                                             <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Installation Capacity (In kWp)</label>
                                                 <div class="field">
    			 				<input id="txtInstallationCapacity" autocomplete="off"  name="txtInstallationCapacity" placeholder="Installation Capacity" type="text">
    			 			</div>
                                                </div>
                                            </div>
                                            
                                            
                                          
                                            
                                           
                                                    
                                        </div>
                                        
                                        
                                        
                                        
                                       <div class="col-md-12   m-t-5 padd-0">                       
                                           
                                           
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Customer Reference</label>
                                                 <div class="field">
    			 			 		<input id="txtCustomerReference" autocomplete="off" name="txtCustomerReference" placeholder="Customer Reference" type="text">
    			 			</div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Customer Naming</label>
                                                 <div class="field">
    			 				<input id="txtCustomerNaming" autocomplete="off" name="txtCustomerNaming" placeholder="Customer Naming" type="text">
    			 			</div>
                                                </div>
                                            </div> 
                                            
                                                <div class="col-md-3 mm-10">
    			 			<div class="field  m-b-0">
    			 			 	<label class="fields p-l-0">COD</label>
    			 			</div>
    			 			<div class="controls">
							<div class="input-group">
								<input id="txtCOD" autocomplete="off" type="text" placeholder="COD" class="date-picker form-control txtCOD" name="fromDate"/>
								<label for="txtCOD" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span> </label>
							</div>
						</div>     
    			 		</div>
                                         
                                         
                                           <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Plant Module Area (m<sup>2</sup>)</label>
                                                 <div class="field">
    			 			 		<input id="txtPlantarea" name="txtPlantarea" autocomplete="off" placeholder="Plant Module Area" type="text">
    			 			</div>
                                                </div>
                                            </div>   
                                           
                                            
                                           
                                                  
                                        </div>
                                        
                                        
                                          <div class="col-md-12   m-t-5 padd-0">   
                                          
                                            <div class="col-md-3">
                                            <div class="field m-t-6">
                                                 <label class="fields">PO Number</label>
                                                 <div class="field">
    			 			 		<input id="txtPonumber" autocomplete="off" name="txtPonumber" placeholder="PO Number" type="text">
						 
    			 			</div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3 mm-10">
    			 			<div class="field  m-b-0">
    			 			 	<label class="fields p-l-0">PO Date</label>
    			 			</div>
    			 			<div class="controls">
							<div class="input-group">
								<input id="txtPoDate" autocomplete="off" type="text" placeholder="PO Date" class="date-picker form-control txtpodate" name="fromDate"/>
								<label for="txtPoDate" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span> </label>
							</div>
						</div>     
    			 		</div>
                                          
                                          
                                          
                                           
                                           
                                            
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Country</label>
                                                 <div class="field">
    			 				<select class="ui search selection dropdown"  name="ddlCountry" id="ddlCountry">
						        <option value="">Select</option>
						      </select>
    			 			</div>
                                                </div>
                                            </div> 
                                            
                                               <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">State</label>
                                                 <div class="field">
    			 			 		<select class="ui search selection dropdown"  autocomplete="off" name="ddlState" id="ddlState">
						        <option value="">Select</option>
						      </select>
						 
    			 			</div>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                           
                                            
                                           
                                            
                                                   
                                        </div>
                                        
                                           <div class="col-md-12   m-t-5 padd-0">                       
                                           
                                            
                                                                  
                                            <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Address</label>
                                                 <div class="field">
    			 				<input id="txtAddress" name="txtAddress" autocomplete="off" placeholder="Address" type="text">
    			 			</div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="field required m-t-6">
                                                    <label class="fields">City</label>
                                                    <div class="field">
    			 			<input id="txtCity" name="txtCity" autocomplete="off" placeholder="City" type="text">
						    </div>
                                                </div>
                                            </div>
                                            
                                          
                                            
                                            
                                             <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Postal Code</label>
                                                 <div class="field">
    			 				<input id="txtPostalCode" name="txtPostalCode" autocomplete="off" placeholder="Postal Code" type="text">
    			 			</div>
                                                </div>
                                            </div>  
                                            
                                            
                                              <div class="col-md-3">
                                            <div class="field required m-t-6">
                                                 <label class="fields">Site Status</label>
                                                 <div class="field">
    			 			 		<select class="ui search selection dropdown"  name="ddlSiteStatus" id="ddlSiteStatus" style="zIndex:9999;">
						        <option value="">Select</option>
						      </select>
    			 			</div>
                                                </div>
                                            </div>
                                            
                                          
                                                   
                                        </div>
                                        
                                        
                                         
                                        <div class="col-md-12 m-t-30" >
                                         <a id="tab-contact-next" class="btn btn-primary btnNext confi-btn-width" style="float:right;" >Next</a>                                  
                                        	<!-- <div class="btn btn-success  submit">Submit</div>
                 	  						<div class="btn btn-primary clear m-l-15">Reset</div> -->
                                        </div>                                        
                                    </div>
                                 </div>
                              </div>
                           </div>
                      
							</div>	 
    		 				  
						</div>
						
						</form>
						<!-- Table End-->
						
						<!-- Table Start-->
						<div class="padd-3 sortable" id="equipmentdata">
							<div class="row" id="QuickLinkWrapper2">								
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails" style="height:260px;">
									<div class="card card-default bg-default" data-pages="card">
										<!-- <div class="card-header">
											<div class="card-title tbl-head ml-15" style="text-transform:none !important;">
												 <i class="fa fa-list" aria-hidden="true"></i> List of Events
											</div>

										</div> -->
										<!-- <div class="col-md-12" style="margin-top:10px;height:220px !important;">
											<div class="col-md-4">
											<div id="barcontainer" style="min-width: 310px; height: 200px; margin: 0 auto"></div>
											</div>
											<div class="col-md-4">
												<div id="donutcontainer" style="height: 250px"></div>
											</div>
											<div class="col-md-4 padd-0">
												<div id="activitycontainer"></div>
											</div>
										</div> -->
										<div class="padd-5 table-responsive">
                                        	<table id="example" class="table table-striped table-bordered"
                                            width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                 <th style="width:10%;">Site Code</th>
                                                 <th style="width:10%;">Site Type</th>
                                                  <th style="width:20%;">Site Name</th>
                                                  <th style="width:15%;">Customer</th>
                                                  <th style="width:10%;">State</th>
                                               	  <th style="width:10%;">Country</th>
                                               	  <th style="width:10%;">Site Status</th>
                                                  <th style="width:7%;">Edit</th>
                                                 <!--  <th style="width:8%;">De-activate</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                               


                                            </tbody>
                                        </table>


                                    </div>
                                   </div>
								</div>


							</div>

						</div>
						

						<!-- Table End-->
						
						
					</div>
				

			</div>

			<div class="container-fixed-lg footer mb-0">
				<div class="container-fluid copyright sm-text-center">
					<p class="small no-margin pull-left sm-pull-reset">
						<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
							CLEAN ENERGY</span>. <span class="hint-text">All rights reserved.
						</span>
					</p>
					<p class="small no-margin pull-rhs sm-pull-reset">
						<span class="hint-text">Powered by</span> <span>MESTECH
							SERVICES PVT LTD</span>.
					</p>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Side Bar Content Start-->

	


	<div class="quickview-wrapper  builder hidden-sm hidden-xs"
		id="builder">
		<div class="p-l-10 p-r-10 ">
			<a class="builder-close quickview-toggle pg-close"></a>
			<a class="builder-toggle" data-toggle="quickview"
				data-toggle-element="#builder"><img
				src="resources/img/ImageResize_06.png"></a>
			<ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary"
				id="builderTabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#tabLayouts" role="tab"
					aria-controls="home"><span>Quick Link</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active " id="tabLayouts" role="tabcard">
					<div class="scrollable">
						<div class="p-r-50">
							<div class="list">
								<ul>
								<!-- 	<li><a href="#QuickLinkWrapper1">Ticket Filters</a></li> -->
									<li><a href="#QuickLinkWrapper2">List Of Sites</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>






	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min3.js" type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>









	<div id="gotop"></div>

	<!-- Share Popup End !--> 
<!-- Modal -->
<div id="tktCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog modal-top">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">New Ticket</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"><span class="errormess">*</span> All fields are mandatory</p>
				 </div>            
        		<form:form action="${addAction}"  modelAttribute="ticketcreation" class="ui form validation-form"> 
             
              <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width SiteNames"
									id="ddlSite" name="ddlSite" path="siteID" onchange="getEquipmentAgainstSite()">
									<form:option value="" tabindex="0">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>
						
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                                                <div class="col-md-4 col-xs-12">
                                                       <label class="fields required m-t-10">Equipment</label>
                                                </div>

                                                <div class="col-md-8 col-xs-12">
                                                       <form:select
                                                              class="field ui fluid search selection dropdown width equipments"
                                                              id="ddlEquipment" path="equipmentID" name="ddlEquipment">
                                                              <option value="">Select</option>
                                                       </form:select>
                                                </div>
                          </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Type</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width evtDropdown" id="ddlType" name="ddlType"  path="ticketType">
                        <form:option value="">Select</form:option>
                        <form:option value="Operation">Operation</form:option>
                        <form:option value="Maintenance">Maintenance</form:option>
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Category</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width category evtDropdown" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                         <form:option data-value="Operation" value="">Select</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Preventive Maintenance">Preventive Maintenance</form:option>
						
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                          <form:option data-value="Maintenance" value="">Select</form:option>
                    </form:select> 
                  </div>
                  
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                  <div class="col-md-4 col-xs-12"><label class="fields required m-t-10">Subject</label></div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<form:input  placeholder="Subject"   name="Subject"  type="text"  id="txtSubject" autocomplete="off" path="ticketDetail" maxLength="50"/>
                  	</div>
                  	</div>
               </div> 
              
              <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Priority</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width evtDropdown" id="ddlPriority" name="user" path="priority">
                        <form:option value="">Select </form:option>
                        <form:option data-value="3" value="3">High</form:option>
                        <form:option data-value="2" value="2">Medium</form:option>
                        <form:option data-value="1" value="1">Low</form:option>
                    </form:select> 
                  </div>
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Description</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<textarea id="txtTktdescription"  autocomplete="off" name="description" style="resize: none;" placeholder="Ticket Description" maxLength="120"></textarea>
                  	</div>
                    
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
				       <div class="btn btn-success  submit" id="btnCreate">Create</div>
				       <button class="btn btn-success" type="button" id="btnCreateDummy" tabindex="7"  style="display:none;">Create</button>
				        <div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
				  </div>
  <!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
  <!-- <div class="ui error message"></div> -->
</form:form>
				<!-- <div class="ui error message"></div> -->
				
	  		</div>
	  </div>
    </div>
 </div>
<!-- Address Popup End !--> 
<!-- Modal -->
<div id="evntCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog modal-top">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">Event Ticket Creation</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"> All fields are mandatory</p>
				 </div>            
				 <!-- <span class="errormess">*</span> -->
        		<form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form validation-form"> 
             
              <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                     <div class="col-md-4 col-xs-12">
                     <p class="fields  m-t-10">Site</p>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<p class="fields  m-t-10" id="txtSiteName" path="siteID"></p>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <p class="fields  m-t-10">Type</p>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<p class="fields  m-t-10" id="txtTypeName" path="ticketType">Operation</p>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                     <div class="col-md-4 col-xs-12">
                     <p class="fields  m-t-10">Priority</p>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<p class="fields  m-t-10" id="txtPriority" path="priority"></p>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Category</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width category evtDropdown" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                         <form:option data-value="Operation" value="">Select</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Preventive Maintenance">Preventive Maintenance</form:option>
										
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                          <form:option data-value="Maintenance" value="">Select</form:option>
                    </form:select> 
                  </div>
                  
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                  <div class="col-md-4 col-xs-12"><label class="fields required m-t-10">Subject</label></div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<form:input  placeholder="Subject"  autocomplete="off" name="Subject"  type="text"  id="txtSubject" path="ticketDetail" maxLength="50"/>
                  	</div>
                  	</div>
               </div> 
              
              
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Description</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<textarea id="txtTktdescription"  autocomplete="off" name="description" style="resize: none;" placeholder="Ticket Description" maxLength="120"></textarea>
                  	</div>
                    
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
				       <div class="btn btn-success  submit">Create</div>
				       <button class="btn btn-success" type="button" id="btnCreateDummy" tabindex="7"  style="display:none;">Create</button>
				        <div class="btn btn-primary clear m-l-15">Reset</div>
				  </div>
  <!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
  <!-- <div class="ui error message"></div> -->
</form:form>
				<!-- <div class="ui error message"></div> -->
				
	  		</div>
	  </div>
    </div>
 </div>
<!-- Address Popup End !--> 
	<script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
	<script>
         $(document).ready(function() {
            $(window).fadeThis({
               speed: 500,
            });
         });
      </script>


<script type="text/javascript">
      $(document).ready(function(){
var validation  = {
	ddlSiteType: {
        identifier: 'ddlSiteType',
        rules: [
          {
            type   : 'empty',
            prompt : 'Select Site Type'
          }
        ]
      },
      ddlCustomer:  {
          identifier: 'ddlCustomer',
          rules: [
            {
              type   : 'empty',
              prompt : 'Select Customer'
            }
          ]
        },
      txtSiteName:  {
          identifier: 'txtSiteName',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Site Name'
            }
          ]
        },
  
        
           txtContactPerson: {
        identifier: 'txtContactPerson',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Contact Person'
            }
          ] 
      },
      txtAddress: {
        identifier: 'txtAddress',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Address'
            }
          ] 
      },
      txtCity: {
        identifier: 'txtCity',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter City'
            }
          ] 
      },
      ddlState: {
        identifier: 'ddlState',
          rules: [
            {
              type   : 'empty',
              prompt : 'Select State'
            }
          ] 
      },
      
      ddlCountry: {
        identifier: 'ddlCountry',
          rules: [
            {
              type   : 'empty',
              prompt : 'Select Country'
            }
          ] 
      },
      txtPostalCode: {
        identifier: 'txtPostalCode',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Postal Code'
            }
          ] 
      },
      txtLongitude: {
        identifier: 'txtLongitude',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Longitude'
            }
          ] 
      },
      
      txtLatitude : {
    	  identifier: 'txtLatitude',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Longitude'
            }
          ]  
      },
      
      txtMobile : {
    	  identifier: 'txtMobile',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Mobile'
            }
          ]  
      },
      
      txtEmailID : {
    	  identifier: 'txtEmailID',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Email Id'
            }
          ]  
      },
      txtCustomerReference : {
    	  identifier: 'txtCustomerReference',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Customer Reference'
            }
          ]  
      },
           
      txtCustomerReference : {
    	  identifier: 'txtCustomerReference',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Customer Reference'
            }
          ]  
      },
      txtCustomerNaming : {
    	  identifier: 'txtCustomerNaming',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Customer Naming'
            }
          ] 
      },
      txtInstallationCapacity : {
    	  identifier: 'txtInstallationCapacity',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Installation Capacity'
            }
          ]   
      },
      txtInstallationCapacity : {
    	  identifier: 'txtInstallationCapacity',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Installation Capacity'
            }
          ]   
      },
      txtLocationMap : {
    	  identifier: 'txtLocationMap',
          rules: [
            {
              type   : 'empty',
              prompt : 'Enter Location Map'
            }
          ]  
      },
      ddlSiteStatus: {
         identifier: 'ddlSiteStatus',
         rules: [
             {
                type   : 'empty',
                prompt : 'Enter Site Status'
              }
            ] 
        },
             
                };
var settings = {
  onFailure:function(){ 
    //alert('fail');
      toastr.warning("Validation Failure");	
      return false;
    }, 
  onSuccess:function(){    
    //alert('Success');
    AjaxCallForSaveOrUpdate();
    return false; 
    }};
  
$('.ui.form.noremal').form(validation,settings);
      });
    </script>


</body>

<script type="text/javascript">
              function getEquipmentAgainstSite() {
                     var siteId = $('#ddlSite').val();
                   
                     $.ajax({
                           type : 'GET',
                           url : './equipmentsBysites?siteId=' + siteId,
                           success : function(msg) {
                                  if (msg) {
                                         for (i = 0; i < msg.length; i++) {
                                                $('#ddlEquipment').append(
                                                              "<option value="+msg[i]+">" + msg[i]
                                                                           + "</option>");
                                         }

                                  }
                                  
                           },
                           error : function(msg) {
                                  console.log(msg);
                           }
                     });
              }
       </script>
</html>


