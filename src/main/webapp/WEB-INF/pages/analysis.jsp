<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="true" %>


<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
      <meta charset="utf-8"/>
      <title>:: Welcome To EIRA ::</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
      <link rel="icon" type="image/x-icon" href="favicon.ico"/>
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-touch-fullscreen" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="default">
      <meta content name="description"/>
      <meta content name="author"/>

      
      <link href="resources/css/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/select2.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/pages-icons.css" rel="stylesheet" type="text/css">
      <link href="resources/css/animate.css" rel="stylesheet"  />
      <link href="resources/css/pages.css" class="main-stylesheet"  rel="stylesheet" type="text/css"/>
      <link href="resources/css/styles.css" rel="stylesheet" type="text/css">
      <!-- <link href="resources/css/jquerysctipttop.css" rel="stylesheet" type="text/css"> -->
      <link href="resources/css/site.css" rel="stylesheet" >
      <link href="resources/css/semantic.min.css" rel="stylesheet" type="text/css">
      
      <link href="resources/css/bootstrap.min.3.37.css" type="text/css" rel="stylesheet">
      <link href="resources/css/chartstyle.css" type="text/css" rel="stylesheet">
      <link href="resources/css/semantic.min.css" rel="stylesheet" type="text/css" >
      <!-- <link rel="stylesheet" type="text/css" href="resources/css/jquery-ui.css" /> -->
      <link href="resources/css/multipleaxishighcharts.css" rel="stylesheet" type="text/css" >
      <link href="resources/css/jquerysctipttop.css" type="text/css" rel="stylesheet">
      <script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
      
      <script src="resources/js/highchart.js" type="text/javascript"></script>
      <script src="resources/js/exporting.js" type="text/javascript"></script>
      <script src="resources/js/export-data.js" type="text/javascript"></script>
      <!-- <script src="https://code.highcharts.com/highcharts.js"></script> -->
      <!-- <script src="https://code.highcharts.com/modules/exporting.js"></script> -->
	  <!-- <script src="https://code.highcharts.com/modules/export-data.js"></script> -->
      
      <script src="resources/js/searchselect.js" type="text/javascript"></script>
      <script src="resources/js/controller/analysis.js" type="text/javascript"></script>
      <script src="resources/js/analysisChart.js" type="text/javascript"></script>  
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>
      
	  <script type="text/javascript" src="resources/js/jquery-ui.js"></script>
	  <!-- <link rel='stylesheet' href='resources/css/toastr.min.css'/>      
      <script src='resources/js/toastr.min.js'></script> -->
      
      <script type="text/javascript" src="resources/js/toastr.js"></script>
      <link rel="stylesheet" type="text/css" href="http://codeseven.github.com/toastr/toastr.css">
      <!-- <link rel="stylesheet" type="text/css" href="http://codeseven.github.com/toastr/toastr-responsive.css"> -->
	  
	  
	  <!-- Graph Type  Tree View Filter Plugins -->	  
	  <link href="resources/css/jquery.treefilter.css" type="text/css" rel="stylesheet">	  
	  <script src="resources/js/jquery.treefilter.js" type="text/javascript"></script>
	  
	  <!-- Date Time Picker Plugins -->
	  
	  <script type="text/javascript" src="resources/js/moment.min_date.js"></script>
	  <script type="text/javascript" src="resources/js/daterangepicker.min.js"></script>
	  <link rel="stylesheet" type="text/css" href="resources/css//daterangepicker.css" /> 
	  
	  <!-- Equipment Type Tree View -->
	  <!-- <script type="text/javascript" src="resources/js/jstree.min.js"></script>
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" /> -->
	  
	 <!--  Tree Equipment and Parameter  -->
	 <link href="resources/css/ui.fancytree.css" rel="stylesheet" class="skinswitcher">
	 <link href="resources/css/sample.css" rel="stylesheet">
	 <link href="resources/css/prettify.css" rel="stylesheet">
	 <script src="resources/js/jquery.fancytree.js"></script>
	 <script src="resources/js/jquery.fancytree.dnd.js"></script>
	 <script src="resources/js/jquery.fancytree.edit.js"></script>
	 <script src="resources/js/jquery.fancytree.glyph.js"></script>
	 <script src="resources/js/jquery.fancytree.wide.js"></script>
	 <script src="resources/js/sample.js"></script>	 
	 
	 <script type="text/javascript">
	 	$(document).ready(function (){
	 		
	 		$("#ddlSite").val($('#ddlSite option').eq(1).val());
	 		
	 		 var iHeight = $("#filter-labels").height();
	 		
	 		  //Get ScrollHeight of the div
	 		
	 		  var iScrollHeight = $("#filter-labels").prop("scrollHeight");
	 		
	 		  var msg = 'Height:' + iHeight + 'px & ScrollHeight:' + iScrollHeight + 'px';
	 		
	 		
	 		
	 		 // $("span").html(msg);

	 		
	 		
	 		/*  $('ul').slimScroll({
	 	        height: '250px'
	 	    }); */
	 		setTimeout(function(){ 
	 			/* $("#ddlSite").val($('#ddlSite option').eq(1).val()); */
	 			
    		 	SiteDropdownChange();   
    	   			 
    	   }, 1000);
	 	})
	 </script>
	 
	
	  <script type="text/javascript">	  
       $(document).ready(function () {
    	   
    	   var varjsondata = eval("[]");
    	   
    	   toastr.options = {
    	           "debug": false,
    	           "positionClass": "toast-top-left",
    	           "progressBar":true,
    	           "onclick": null,
    	           "fadeIn": 300,
    	           "fadeOut": 100,
    	           "timeOut": 1000,
    	           "extendedTimeOut": 1000
    	         }
    	   
    	   
    	         
    	         var showToastrs = false;
    	   		
    	    
    		  $('#btnCreate').click(function() {
    		  		if($('#ddlCategory').val== "") {
    		  	    	alert('Val Empty');
    		  	        $('.category ').addClass('error')
    		  	    }
    		  	})
    	   
    	   		
      	 
      	 var gHight = $(window).height();
      	 gHight = gHight - 133;
      	 var gChartHight = gHight - 125;
      	 
    	   $('.graphDetails').css("height", gHight + "px");
    	   $('#chartData1').css("height", gChartHight + "px");
    	   $('#chartData2').css("height", gChartHight + "px");
    	   $('#chartData3').css("height", gChartHight + "px");
    	   
    	   
    		var classstate = $('#hdnstate').val();
			
			$("#analyticsid").addClass(classstate);
    
    	   
         /*    $(".Equipmentsearch").keyup(function () {
                var searchString = $(this).val();
                $('#EquipmentTree').jstree('search', searchString);
            });
             */
          /*      
            $('#EquipmentTree').on("select_node.jstree", function (e, data) {            	
            	jstreecheckchangeevent();
            });
            
            $('#EquipmentJSTree').on("deselect_node.jstree", function (e, data) {            	
            	jstreecheckchangeevent();
            });
            
            
            $('#ParameterJSTree').on("select_node.jstree", function (e, data) {            	
            	jstreecheckchangeeventforparameter();
            });
            
            $('#ParameterJSTree').on("deselect_node.jstree", function (e, data) {            	
            	jstreecheckchangeeventforparameter();
            }); */
           
            
            
            $("#EquipmentSearch").on("keyup", function() {
			    var value = $(this).val().toLowerCase();
			    $("#EquipmentTree .ui-fancytree li").filter(function() {
			      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			    });
			  });
            
            $("#ParameterSearch").on("keyup", function() {
			    var value = $(this).val().toLowerCase();
			    $("#ParameterTree .ui-fancytree li").filter(function() {
			      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			    });
			  });
            
            
            $('.btn-equipement-apply').click(function(){
            	 DataPopulateAjaxCall();
   			});
         
       		$('.btn-parameter-apply').click(function(){
       	 		DataPopulateAjaxCall();
  		    });
       	
       	
        	$('.btn-equipement-reset').click(function(){        		
        		
        		
        		$("#EquipmentTree").fancytree("getTree").visit(function(node){
        		        node.setSelected(false);
        		 });
        		 
        		 $("#ParameterTree").fancytree("getTree").visit(function(node){
     		        node.setSelected(false);
     			 });
        		 
        		if($('#hdnGraphType').val() == 'Parameter Comparison')
         		{
         			$('#parameter-tab').addClass('disabled');
       			    $(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
         		}
        		
        		$('.filter-labels').empty();
        		
    		});
          
        	$('.btn-parameter-reset').click(function(){
        		
        		
        		try
        		{
        			var Node = $('#ParameterTree' ).fancytree('getTree').getSelectedNodes();
        			
        			for(var i =0; i < Node.length; i++)
        			{		
        				 if(Node[i].selected) {				    	
        				       	var title = Node[i].title;
        				       	var id = Node[i].data.id;				       	
        				       	$('#btnparaclose' + id).click();				       	
        				 }
        			}	
        			
        		}
        		catch(err)
        		{
        			
        		}
        		
        		
        		$("#ParameterTree").fancytree("getTree").visit(function(node){
    		        node.setSelected(false);
    		 	});
        		
        		
   		    });
        	
			/* function jstreecheckchangeevent()
			{
				var Equipmentids = $('#EquipmentJSTree').jstree('get_selected');	
				
            	if(Equipmentids.toString() == "")
            	{
            		$('.btn-equipement-reset').addClass('disabled');
            		$('.btn-equipement-apply').addClass('disabled');
            		
            		            		
            		if($('#hdnGraphType').val() == 'Parameter Comparison')
            		{
            			$('#parameter-tab').addClass('disabled');
          			    $(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
            		}
            		else
            		{
            			$('#parameter-tab').addClass('disabled');
          			    $(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
            		}
             	   
            	}
            	else
            	{
            		$('.btn-equipement-reset').removeClass('disabled');
            		$('.btn-equipement-reset').addClass('btnResetActive');
            		$('.btn-equipement-apply').removeClass('disabled');
            		
            		
            		if($('#hdnGraphType').val() == 'Parameter Comparison')
            		{
            			$('#parameter-tab').removeClass('disabled');
        				$(".data-toggle-parameter-add").attr("data-toggle",'tab');
            		}
            		else
            		{
            			$('#parameter-tab').addClass('disabled');
          			    $(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
            		}
            		
            	}
			}
			
			function jstreecheckchangeeventforparameter()
			{
				var Parameterids = $('#ParameterJSTree').jstree('get_selected');	
				
            	if(Parameterids.toString() == "")
            	{
            		$('.btn-equipement-reset').addClass('disabled');
            		$('.btn-equipement-apply').addClass('disabled');
            		$('.btn-parameter-reset').addClass('disabled');
            		$('.btn-parameter-apply').addClass('disabled');
            		
            		
            		
            		if($('#hdnGraphType').val() == 'Parameter Comparison')
            		{
            			$('#parameter-tab').addClass('disabled');
          			    $(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
            		}
            		else
            		{
            			$('#parameter-tab').addClass('disabled');
          			    $(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
            		}
             	   
            	}
            	else
            	{
            		$('.btn-equipement-reset').removeClass('disabled');
            		$('.btn-equipement-apply').removeClass('disabled');
            		$('.btn-parameter-reset').addClass('disabled');
            		$('.btn-parameter-apply').addClass('disabled');
            		
            		if($('#hdnGraphType').val() == 'Parameter Comparison')
            		{
            			$('#parameter-tab').removeClass('disabled');
        				$(".data-toggle-parameter-add").attr("data-toggle",'tab');
            		}
            		else
            		{
            			$('#parameter-tab').addClass('disabled');
          			    $(".data-toggle-parameter-add").removeAttr("data-toggle",'tab');
            		}
            		
            	}
			}
        	 */
         	function equipmentreset(){
         		$('#EquipmentJSTree').jstree(true).deselect_all();
         	}
         	
         	function parameterreset(){
         		$('#ParameterJSTree').jstree(true).deselect_all();
         	}
         	
         	function reset(){
         		$('#EquipmentJSTree').jstree(true).deselect_all();
         		$('#ParameterJSTree').jstree(true).deselect_all();
         	}           
        });        
    </script>
    
	<script type="text/javascript">
	 $(window).load(function(){
    	 $('.ui.dropdown').dropdown({forceSelection:false});
    });
        $(document).ready(function() {   
        	$('#ddlSiteSelect').dropdown('clear');
        	$('#ddlType').dropdown('clear');
        	$('#ddlCategory').dropdown('clear');
        	$('#ddlPriority').dropdown('clear');
        	$('#txtSubject').val('');
        	$('#txtTktdescription').val('');
        	 data();
        	 
        	$('#equipmentDropdown').hide();
        	$('#parameterDropdown').hide();
        	
        	//$('.loading-gif').hide();
        	/* 
        	$('#tab2').attr('class', 'disabled');
        	$('#tab3').attr('class', 'disabled');
        	 */
        	
        	 
        	 $('#example').DataTable({
        		 "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
        		 "bLengthChange": true,      
        	 });
        	 
       
        	 
        	 var tree = new treefilter($("#my-tree"), {
        			searcher : $("input#my-search"),
        			multiselect : false
        		});
        	 
        	 
			
			$('#fromDate').datepicker({
      		  minDate: 0,
      		   dateFormat: "dd/mm/yy",
      		  beforeShow: function() {
      		    $(this).datepicker('option', 'maxDate', $('#toDate').val());
      		  }
      		});
      	 $('#toDate').datepicker({        		
      		  beforeShow: function() {
      		    $(this).datepicker('option', 'minDate', $('#fromDate').val());
      		    if ($('#fromDate').val() === '') $(this).datepicker('option', 'minDate', 0);
      		  }
      		});
      	 
      
        			
            $('.dataTables_filter input[type="search"]').attr('placeholder','search');
            if($(window).width() < 767){   
			   $('.card').removeClass("slide-left");
			   $('.card').removeClass("slide-right");
			   $('.card').removeClass("slide-top");
			   $('.card').removeClass("slide-bottom");
			}
           /*  $('.close').click(function(){
				$('.clear').click();
			}); */
			

            $('.close').click(function(){
                $('#tktCreation').hide();
                $('.clear').click();
                $('.category').dropdown();
                $('.SiteNames').dropdown();
            });
            
            $('#ddlType').change(function(){              
                $('.category').dropdown('clear')
               });

			$('.clear').click(function(){
			      $('.search').val("");
			 });

			$('body').click(function(){
			    $('#builder').removeClass('open'); 
			  });
			
			 $("#searchSelect").change(function() {
        	         var value = $('#searchSelect option:selected').val();
        	         var uid =  $('#hdneampmuserid').val();
        	        redirectbysearch(value,uid);
        	   });

        	 
        	 
        	 
            
            $(".bars").click(function() {
               $(".sidebar-menu").invisible();
            })
            
            
            var start = moment();
            var end = moment();

            $('#hdnFromDate').val(start.format('DD-MM-YYYY'));
            $('#hdnToDate').val(end.format('DD-MM-YYYY'));
            
            
            
            function cb(start, end) {
               /*  $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY')); */
                if (start.format('MMM D, YYYY') == end.format('MMM D, YYYY') ) {
          				$('#reportrange span').html(start.format('MMM D, YYYY'));
        			}
        		else {
          			$('#reportrange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
        		}
                $('#hdnFromDate').val(start.format('DD-MM-YYYY'));
                $('#hdnToDate').val(end.format('DD-MM-YYYY'));
                $(document).trigger('myCustomEvent');
            }

            $('#reportrange').daterangepicker({
            	locale: {
                    format: ' DD-MM-YYYY'
                },

            	 maxDate: new Date(),
                ranges: {
                   'Today': [moment(), moment()],
                   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                   'This Month': [moment().startOf('month'), moment().endOf('month')],
                   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);
            
            
            $(document).on('myCustomEvent', function () {
            	$('.btn-display').show();
                // your code here
            });
         
             
        });
      </script>
    
     <script type='text/javascript'>//<![CDATA[
         $(window).load(function(){
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
         
         $('.ui.dropdown').dropdown({
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: true, 
             showOnFocus: true,
             on: "click" 
           });

		$('.ui.dropdown.oveallsearch').dropdown({
             onChange: function (value, text, $selectedItem) {                
                var uid =  $('#hdneampmuserid').val();
                redirectbysearchvalue(value,uid);
             },
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: false, 
             showOnFocus: true,
             on: "click" 
           });
           
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );//]]> 
            var validation  = {
                    txtSubject: {
                               identifier: 'txtSubject',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               },
                               {
                                   type: 'maxLength[50]',
                                   prompt: 'Please enter a value'
                                 }]
                             },
                             ddlSiteSelect: {
                               identifier: 'ddlSiteSelect',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                             },
                            ddlType: {
                                  identifier: 'ddlType',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                            ddlCategory: {
                               identifier: 'ddlCategory',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                             ddlPriority: {
                               identifier: 'ddlPriority',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please select a dropdown value'
                               }]
                             },
                             description: {
                               identifier: 'description',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please Enter Description'
                               }]
                             }
             };
               var settings = {
                 onFailure:function(){
                     return false;
                   }, 
                 onSuccess:function(){    
                   $('#btnCreate').hide();
                   $('#btnCreateDummy').show()
                   //$('#btnReset').attr("disabled", "disabled");          
                   }};           
               $('.ui.form.validation-form').form(validation,settings);

         })
         
      </script>
       <style type="text/css">
     /*  .ui.form textarea:not([rows]) {
		    height: 8em;
		    min-height: 8em;
		    max-height: 24em;
		}
			.ui-multiselect-menu {
     		z-index:99999;
     	}

		.ui.form textarea:not([rows]) {
		    height: 4em;
		    min-height: 4em;
		    max-height: 24em;
		} */
  
		  .input-sm, .form-horizontal .form-group-sm .form-control {
		    font-size: 13px;
		    min-height: 25px;
		    height: 32px;
		    padding: 8px 9px;
		}
		
		
		/* ul.fancytree-ext-wide span.fancytree-node span.fancytree-title {
	padding-left: 20px !important;
    display: contents !important;
	position:relative !important;
	}
	span.fancytree-node {
		display: inline-flex !important;
		white-space: normal !important;
		}
		 */
		 
		 ul.fancytree-container ul {
padding-left: 6px;
}

 .fancytree-level-1 span.fancytree-title {
    font-size: 13px;
    padding-left: 70px !important;
	}
	.fancytree-level-2 span.fancytree-title {
    font-size: 13px;
    padding-left: 75px !important;
	}
	span.fancytree-title {
    font-size: 13px;
   
	}
	.column-seperation{
	padding:0px
	}
		#GraphTypeSelect li{
		margin-left: 15px !important;
		}
      </style>
  
       
        
  </head>
 
  <body  class="fixed-header">
   <input type="hidden" id="hdnGraphType" value="Daily Energy Generation" />
  <input type="hidden" id="hdnSelectedDataRange" value="daily" />
  <input type="hidden" id="hdnFromDate">
  <input type="hidden" id="hdnToDate">
  
  <input type="hidden" value="${access.userID}" id="hdneampmuserid">
  <input type="hidden" value="${Irradiationunits}" id="IrradiationUnits">
  <input type="hidden" value="${classtate}" id="hdnstate">
  <jsp:include page="slider.jsp" />
  
        
  
  
     <div class="page-container">
         <div class="header">
            <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="sidebar">
            </a>
            <div>
               <div class="brand inline">
               <a href="./dashboard"><img src="resources/img/logo01.png" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management" class="logos"></a>   
               </div>
            </div>
            <div class="d-flex align-items-center">
                 <div class="form-group required field searchbx" id="SearcSElect">                         
                            <div class="ui  search selection  dropdown  width oveallsearch">
                                         <input id="myInput" name="tags" type="text">

                                         <div class="default text">search</div>
                               <!--           <i class="dropdown icon"></i> -->
                                         <div id="myDropdown" class="menu">
                                                
	<jsp:include page="searchselect.jsp" />	

                                         </div>
                                  </div>
                        </div>
                        <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user_01.png" width="32" height="32">
                  </span>
                  
                   <c:if test="${not empty access}">         
                              <p class="user-login">${access.userName}</p>
                  </c:if>
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="logout1" class="dropdown-item color logout"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
               
               
               
               
                                                      
                                         <div class="newticketpopup hidden">
                  <p class="center mb-1 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img src="resources/img/tkt.png"></p>
                  <p class="create-tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false">New Ticket</p>
               </div>
          					
            </div>
         </div>
       
         <div class="page-content-wrapper ">
            <div class="content" id="QuickLinkWrapper1">               
               <div class="container-fixed-lg">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="./dashboard"><i class="fa fa-home mt-7"></i></a></li>
                    <!--  <li class="breadcrumb-item"><a>Visualization</a></li> -->
                     <li class="breadcrumb-item active ">Analysis</li>
                      <li class="breadcrumb-item  analysispage"></li>
                  </ol>	
               </div>
         		<div class="container-fixed-lg padd-3">
                	 <div id="sitestatus" data-pages="card">
                      	<div class="containerItems">                          			  
                   			<div class="sortable" id="customerlist">                 		 
                    			<div class="card-default bg-default content-bg" data-pages="card">
                    			<div class="col-md-12 padd-0">
                    				<div class="siteDetails">
                    					<div class="col-md-4 col-xs-12 padd-0">
                    					<select class="ui fluid search selection dropdown width SitebgChange" id="ddlSite" name="ddlType">
													<option value="">Select Site</option>
													<c:forEach items="${lstSite}" var="lstsite">
													
													  <c:choose>
					<c:when test="${access.monitoringView == 'visible' && lstsite.siteName =='Repal Renewables, Komatikuntla'}">
					<option value="${lstsite.siteId}">Demo Site - ${lstsite.customerNaming}</option>
					</c:when>
					<c:otherwise>
					<option value="${lstsite.siteId}">${lstsite.siteName} - ${lstsite.customerNaming}</option>
													
					</c:otherwise>
					</c:choose>
													
														</c:forEach>
												</select>
                    				</div>
                    				<div class="col-md-8 col-xs-12 padd-0">
                    					<div class="filter-labels">
                    						
                    					</div>
                    				</div>
                    				</div>
                    			</div>
                    			
                    				<div class="col-md-12 padd-0 m-t-5">
                    			<div class="graphDetails">
                    				<!-- <div class="col-md-3 padd-0">
                    					<select class="ui fluid search selection dropdown width" id="ddlGraphType">
													<option value="">Select Graph Type</option>
													<option value="1">Daily Energy Generation</option>
													<option value="2">Energy Performance</option>
													<option value="3">Parameter Comparison</option>
												</select>
                    				</div>
                    				<div class="col-md-9 padd-0">
                    					<h4 class="center sname"></h4>
                    				</div> -->
                    				<div class="col-md-12 padd-0">
                    					<div class="col-md-3 padd-0">
                    						<div class="equipmentandparameter padd-3">
                    							<div class="card card-transparent " style="height: 100%;">
                                 <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx"><!-- nav-tabs-linetriangle -->
                                 	<!--  <li class="nav-item" id="graph-tab">
                                       <a  class="active data-toggle-graph-type" data-toggle="tab" data-target="#tab-fillup1"><span>Configured</span><br><img src="resources/img/GraphType.png" class="graph-img"></a>
                                    </li>
                                    <li class="nav-item disabled" id="equipment-tab">
                                       <a class="data-toggle-equipment-add" data-target="#tab-fillup2"><span>Equipment</span><br><img src="resources/img/Equipment.png" style="text-align:center;" class="equip-img"></a>
                                    </li>
                                    <li class="nav-item disabled" id="parameter-tab">
                                       <a  class="data-toggle-parameter-add"  data-target="#tab-fillup3"><span>Parameter</span><br><img src="resources/img/graph.png" style="text-align:center;" class="para-img"></a>
                                    </li>     -->  
                                   <li class="nav-item" id="graph-tab">
                                       <a  class="active data-toggle-graph-type" data-toggle="tab" data-target="#tab-fillup1"><span>Configured</span><br><img src="resources/img/mconfigured.png" height="32px" class="graph-img"></a>
                                    </li>
                                    <li class="nav-item disabled" id="equipment-tab">
                                       <a class="data-toggle-equipment-add" data-target="#tab-fillup2"><span>Equipment</span><br><img src="resources/img/mEquipment.png" style="text-align:center;" height="32px" class="equip-img"></a>
                                    </li>
                                    <li class="nav-item disabled" id="parameter-tab">
                                       <a  class="data-toggle-parameter-add"  data-target="#tab-fillup3"><span>Parameter</span><br><img src="resources/img/mparameters.png" style="text-align:center;" height="32px" class="para-img"></a>
                                    </li>                              
                                 </ul>
                                 <div class="tab-content">
                                 <div class="tab-pane active" id="tab-fillup1">                                  
                                       <div class="col-md-12 column-seperation padd-5">
                                      		<ul id="my-tree" class="">
														<li class="tf-open">															
															<ul id="GraphTypeSelect">
																<span style=""><img src="resources/img/mDailyEnergyGeneration.png" style="    width: 8%;float: left;    padding-top: 3px;margin-left: -12px;padding-right: 0px;"></span><li id="gtDailyEnergyGeneration"  class="tf-selected disabled"><div
																		class="GraphTypeClick" name="GraphType"
																		id="rdoDailyEnergy" value="Daily Energy Generation">Daily Energy Generation</div></li>
																<span style=""><img src="resources/img/mEnergyPerformance.png" style="    width: 8%;float: left;    padding-top: 3px;margin-left: -12px;padding-right: 0px;"></span><li id="gtEnergyPerformance" class="area-chart"><div
																		class="GraphTypeClick" name="GraphType"
																		id="rdoEnergyPerformance" value="Energy Performance">Energy Performance</div></li>
																<span style=""><img src="resources/img/Parametercomparison.png" style="    width: 8%;float: left;    padding-top: 3px;margin-left: -12px;padding-right: 0px;"></span><li id="gtParameterComparison" class="combinedchart"><div
																		class="GraphTypeClick" name="GraphType"
																		id="rdoParameterComparison" value="Parameter Comparison">Parameter Comparison</div></li>
															</ul>
														</li>
													</ul>
                                       </div>
                                    </div>
                                    <div class="tab-pane" id="tab-fillup2">                                  
                                      	<div class="col-md-12 column-seperation padd-5">
                                        	<input class="form-control" id="EquipmentSearch" type="text" placeholder="Equipment Search..">
											<div id="EquipmentTree" class="fancytree-colorize-hover fancytree-fade-expander firfox-brow"></div>
                                        </div>
                                       <div class="col-md-12 btn-footer">
                                       		<button type="button" disabled="disabled" class="btn btn-primary  btn-tab-width btn-equipement-apply"><i class="fa fa-check-circle icon-apply"></i>Apply</button>
                                        <button type="button" disabled="disabled" class="btn btn  btn-tab-width btn-equipement-reset resethover btn-normal-view"><i class="fa fa-undo icon-reset"></i>Reset</button>
                                       </div>                                        
                                    </div>
                                    <div class="tab-pane" id="tab-fillup3">
                                    	<div class="col-md-12 column-seperation padd-5">
                                        	<input class="form-control" id="ParameterSearch" type="text" placeholder="Parameter Search..">
											<div id="ParameterTree" class="fancytree-colorize-hover fancytree-fade-expander"> </div>
                                       </div>
                                       	 <div class="col-md-12 btn-footer">
                                       	 		<button type="button" disabled="disabled" class="btn btn-primary  btn-tab-width btn-parameter-apply"><i class="fa fa-check-circle icon-apply"></i>Apply</button>
                                       			 <button type="button" disabled="disabled" class="btn btn  btn-tab-width btn-parameter-reset btnResetActive  resethover"><i class="fa fa-undo icon-reset"></i>Reset</button>
                                       	 </div> 
                                       	 
                                    </div>
                               
                                <!--  <div class="button-type">
                                 		<button class="btn btn-success">Apply</button>
                                 		<button class="btn btn-default">Reset</button>
                                 </div> -->
                              </div>
                    						</div>
                    					</div>
                    					</div>
                    					<div class="col-md-9 padd-0">                    						
                    						<div class="graphselection">
                    							<div class="col-md-12 padd-0" style="height:30px;">
                    								<p class="txtGraphType">Daily Energy Generation</p>
                    							</div>
                    							<div class="col-md-12 padd-0">
                    								<div class="col-md-3 padd-0">
                    									<div class="calendar">
															<div id="reportrange" style="background: #fff; cursor: pointer; padding: 3px 10px; border: 1px solid #7cacc5;;font-size:11.5px;color:#000;height:30px;">
														    	<img src="resources/img/small-calendar.png" style="width:9%;"><!-- <i class="fa fa-calendar"></i> -->&nbsp;
														    	<span></span> <i class="fa fa-caret-down" style="float:right;margin-top: 4px;"></i>
															</div>
														</div>
                    								</div>
                    								<div class="col-md-1">
                    									<button class="btn btn-info  btn-tab-width btn-display" style="border: 1px solid #7cacc5;width: 75px;height: 27px;">View</button>
                    								</div>
                    							</div>	
                    							<div class="col-md-12 padd-0">
                    								<div class="loading-gif" style="width:100%;position:absolute;text-align: center;">
                    									<img src="resources/img/loader_02.gif" class="loading-icon" style="margin-top:17%;width:7%;">
                    									<p class="loading-icon" style="color:#6f8b9b;font-weight:bold;font-size:15px;margin-left:1%;">Loading...</p>
                    								</div>
                    								<div class="ndf-img" style="width:100%;position:absolute;text-align: center;">
                    									<img src="resources/img/ndf_02.png" class="ndf" style="width: 19%;">
                    										<!-- <h1 class="ndf">No Data Found</h1> -->
                    									</div>
                    									<div id="chartData1"></div>
                    									<div id="chartData2" style="display:none"></div>
                    									<div id="chartData3" style="display:none"></div>
                    									
                    									
                    							</div>
                    						</div>
                    			</div>
                    			</div>
                    		
                    				
                    			</div>
               				</div>
                      	</div>
                    </div>
              	</div>
               </div>
            
            </div>

            <div class="col-md-12 container-fixed-lg footer padd-0">
               <div class="container-fluid copyright sm-text-center">
                  <p class="small no-margin pull-left sm-pull-reset">
                     <span class="hint-text">Copyright &copy; 2017.</span>
                     <span>INSPIRE CLEAN ENERGY.</span>
                     <span class="hint-text">All rights reserved. </span>
                  </p>
                  <p class="small no-margin pull-rhs sm-pull-reset">
                     <span class="hint-text">Powered by</span>
                     <span>MESTECH SERVICES PVT LTD</span>.
                  </p>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
 
      <!-- Side Bar Content Start-->

      <!-- Side Bar Content End-->
     
      <!-- <div class="quickview-wrapper  builder hidden-sm hidden-xs" id="builder">
         <div class="p-l-10 p-r-0 ">
            <a class="builder-close quickview-toggle pg-close"  href="#"></a>
            <a class="builder-toggle" data-toggle="quickview" data-toggle-element="#builder"><img src="resources/img/ImageResize_06.png"></a>
            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary" id="builderTabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tabLayouts" role="tab" aria-controls="home"><span>Quick Links</span></a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active " id="tabLayouts" role="tabcard">
                  <div class="scrollable">
                     <div class="p-r-50">
                        <div class="list">
                           <ul>
                              <li><a href="#QuickLinkWrapper1">Customer List</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div> -->
    </div>
      <script src="resources/js/pace.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.gotop.js"></script>
      <script src="resources/js/modernizr.custom.js" type="text/javascript"></script>    
      
            
      <script src="resources/js/tether.min.js" type="text/javascript"></script>
      <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery-easy.js" type="text/javascript"></script>
      <script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.actual.min.js"></script>
      <script src="resources/js/jquery.scrollbar.min.js"></script>
      <script src="resources/js/select2.full.min.js" type="text/javascript" ></script>
      <script src="resources/js/classie.js" type="text/javascript"></script>
      <script src="resources/js/switchery.min.js" type="text/javascript"></script>
      <script src="resources/js/pages.min.js"></script>
      <script src="resources/js/card.js" type="text/javascript"></script>
      <script src="resources/js/scripts.js" type="text/javascript"></script>
      <script src="resources/js/demo.js" type="text/javascript"></script>
      <script src="resources/js/jquery.easing.min.js"></script>
      <script src="resources/js/jquery.fadethis.js"></script>

       
      <script src="resources/js/jquery.dataTables.min.js" type="text/javascript" ></script>
      <script src="resources/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
      <script src="resources/js/semantic.min.js" type="text/javascript" ></script>
      <script src="resources/js/calendar.min.js" type="text/javascript" ></script>
      <script src="resources/js/jquery.selectlistactions.js"></script>
    
    
    
    
    
    <div id="gotop"></div>
         <!-- Address Popup Start !-->
     <!-- Modal -->
<div id="tktCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog modal-top">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">New Ticket</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"><span class="errormess">*</span> All fields are mandatory</p>
				 </div>            
        		<form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form validation-form"> 
             
             <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width SiteNames"
									id="ddlSiteSelect" name="ddlSiteSelect" path="siteID" onchange="getEquipmentAgainstSite()">
									<form:option value="" tabindex="0">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>
						
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                                                <div class="col-md-4 col-xs-12">
                                                       <label class="fields required m-t-10">Equipment</label>
                                                </div>

                                                <div class="col-md-8 col-xs-12">
                                                       <form:select
                                                              class="field ui fluid search selection dropdown width equipments"
                                                              id="ddlEquipment" path="equipmentID" name="ddlEquipment">
                                                              <option value="">Select</option>
                                                       </form:select>
                                                </div>
                          </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Type</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width" id="ddlType" name="ddlType"  path="ticketType">
                        <form:option value="">Select</form:option>
                        <form:option value="Operation">Operation</form:option>
                        <form:option value="Maintenance">Maintenance</form:option>
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Category</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width category" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                         <form:option data-value="Operation" value="">Select</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Preventive Maintenance">Preventive Maintenance</form:option>										
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                          <form:option data-value="Maintenance" value="">Select</form:option>
                    </form:select> 
                  </div>
                  
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                  <div class="col-md-4 col-xs-12"><label class="fields required m-t-10">Subject</label></div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<form:input  placeholder="Subject"  name="Subject"  type="text"  id="txtSubject" path="ticketDetail" maxLength="50"/>
                  	</div>
                  	</div>
               </div> 
              
              <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Priority</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width" id="ddlPriority" name="user" path="priority">
                        <form:option value="">Select </form:option>
                        <form:option data-value="3" value="3">High</form:option>
                        <form:option data-value="2" value="2">Medium</form:option>
                        <form:option data-value="1" value="1">Low</form:option>
                    </form:select> 
                  </div>
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Description</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<textarea id="txtTktdescription"  name="description" style="resize: none;" placeholder="Ticket Description" maxLength="120"></textarea>
                  	</div>
                    
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
				       <div class="btn btn-success  submit" id="btnCreate">Create</div>
				       <button class="btn btn-success" type="button" id="btnCreateDummy" tabindex="7"  style="display:none;">Create</button>
				        <div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
				  </div>
  <!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
  <!-- <div class="ui error message"></div> -->
</form:form>
				<!-- <div class="ui error message"></div> -->
				
	  		</div>
	  </div>
    </div>
   </div>
<!-- Address Popup End !--> 
      <script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
      <script>
         $(document).ready(function() {
            $(window).fadeThis({
               speed: 500,
            });
         });
      </script>


  </body>
  
  
  <script type="text/javascript">
  function getEquipmentAgainstSite() {
      var siteId = $('#ddlSiteSelect').val();
     
      $.ajax({
            type : 'GET',
            url : './equipmentsBysites?siteId=' + siteId,
            success : function(msg) {
         	   
         	   var EquipmentList = msg.equipmentlist;
                   
                          for (i = 0; i < EquipmentList.length; i++) {
                                 $('#ddlEquipment').append(
                                               "<option value="+EquipmentList[i].equipmentId+">" + EquipmentList[i].customerNaming
                                                            + "</option>");
                          }

            },
            error : function(msg) {
                   console.log(msg);
            }
      });
}

       </script>
</html>

