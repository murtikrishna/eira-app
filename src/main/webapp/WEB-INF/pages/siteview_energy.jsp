<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="card card-default bg-default slide-right"
											id="divsiteenergy" data-pages="card">
											<div class="h-150">
												<div class="card-header ">
													<div class="card-title pull-left">energy</div>
													<div class="card-title pull-right center">
														<i class="fa fa-bolt font-size icon-color"
															aria-hidden="true"></i><br>
														<p>Today</p>
													</div>
												</div>
												<div class="card-block">
													<h3>
														<span class="semi-bold"><strong>${siteview.todayEnergy}</strong></span>
													</h3>
													<p>${siteview.energyLastUpdate}</p>
												</div>
											</div>
											<div class="card-footer">
												<p>Production Date : ${siteview.productionDate}</p>
											</div>
										</div>