 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 <div class="card card-default bg-default slide-right" id="tickets" data-pages="card">
                             
                              <c:choose>
    										<c:when test="${access.monitoringView == 'visible'}">
    										<div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">Open Tickets</div>
                                    <div class="card-title pull-right"><i class="fa fa-ticket font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block">
                                   <h3 class="" style="margin-bottom:0px;">
                                       <span class="semi-bold"><span style="font-size:14px;font-weight:bold;">Contact</span></span>
                                    </h3>
                                     <p> <span style="text-decoration:underline;cursor:pointer;color:blue;font-size:14px;"><a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=support@eira.io">support@eira.io</a></span></strong></p>
                                   
                                    <p class="m-t-10"><a><strong><a href=""></a></strong></a> </p>
                                  	 <%--  <p>${dashboard.ticketMessages}<a><strong><a href="./ticketdetails">Click here</a></strong></a> to view ticket details</p> --%>
                                 </div>
                              </div>
    										</c:when>
    										
    										<c:otherwise>
    										
    										<div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">Open Tickets</div>
                                    <div class="card-title pull-right"><i class="fa fa-ticket font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block">
                                    <h3 class="m-t-10">
                                       <span class="semi-bold"><strong>${customerview.openTicketCount}</strong></span>
                                    </h3>
                                    <p><strong>Operations & Maintenance</strong></p>
                                    <p class="m-t-10 tickets-page hidden"><a><strong><a href="./customerticketskpi${access.myCustomerID}">Click here</a></strong></a> to view ticket details</p>
                                  	 <%--  <p>${dashboard.ticketMessages}<a><strong><a href="./ticketdetails">Click here</a></strong></a> to view ticket details</p> --%>
                                 </div>
                              </div>
    										
    										</c:otherwise>
    										
    										</c:choose>
    										
    										<c:choose>
    										<c:when test="${access.monitoringView == 'visible'}">
    										 <div class="card-footer" id="QuickLinkWrapper3">
                                    <p style="color:white">.</p>
                                  <!--  <p style="float:right;"><a href="./ticketdetails">View all tickets</a></p>  -->
                                 </div>
    										</c:when>
    										
    										<c:otherwise>
    										 <div class="card-footer" id="QuickLinkWrapper3">
                                    <p>Closed Tickets : ${customerview.completedTicketCount}</p>
                                  <!--  <p style="float:right;"><a href="./ticketdetails">View all tickets</a></p>  -->
                                 </div>
    										</c:otherwise>
    										</c:choose>
                              
                             
                              </div>