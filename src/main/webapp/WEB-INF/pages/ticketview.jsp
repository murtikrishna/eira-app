<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet"
	rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/bootstrap.min.3.37.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="resources/css/semantic.min.css">
<link rel="stylesheet" type="text/css"
	href="resources/css/jquery-ui.css">
<link href="resources/css/formValidation.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/jquery-ui_cal.css" rel="stylesheet"
	type="text/css">

<!-- <link rel="stylesheet"
	href="http://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css"> -->
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>


<script type="text/javascript" src="resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="resources/js/formValidation.min.js"></script>
<script type="text/javascript"
	src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.val.js"></script>
<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script type="text/javascript">


function AjaxCallForSaveOrUpdate() {                
    //alert('Function Call');
	debugger;
	
    var jmrdetailData = [{    	
  		        "lmrTimestamptext": $("#DownloadFromdate").val(),
  		        "lmrValue": $('#DownloadValue').val(),
  		        "plantDowntime": $('#txtPlantDownTime').val(),
  		      "ticketid": $('#hdnticketid').val()
  		}];
    
   
    $.ajax({
        type: 'POST',
        contentType : 'application/json; charset=utf-8',
        dataType : 'json',
        url: './jmrdetails',
        data: JSON.stringify(jmrdetailData),
        success: function(msg){
        	 $('#MdlDownloadReport').modal('hide');
        	// alert(21);
        	 window.location.href = './ticketviews' + $('#hdnticketid').val();        	 /* 
        	 document.getElementById("jmrdetailbutton").style.visibility = "hidden";
        	 document.getElementById("ajaxbutton").style.visibility = "visible";
        	 $("#jmrdetailbutton").css("cursor","pointer");
        	 $("#ajaxbutton").css("cursor","pointer"); */
        },
        error:function(msg) {        	
        	//alert(0); alert(msg);
        	}
	}); 
}


	$(document).ready(function() {
		
		/* alert($(window).height());
   	 	alert($(window).width()); */
   	 	
   	 var classstate = $('#hdnstate').val();
		
   	 $("#o-mid").addClass(classstate);
		$("#ticketsid").addClass(classstate);
		 
   	
		 $('#ddlCategory').dropdown('clear');
	     $('#ddlPriority').dropdown('clear');
	     
		  $('#btnCreate').click(function() {
		  		if($('#ddlCategory').val== "") {
		  	    	alert('Val Empty');
		  	        $('.category ').addClass('error')
		  	    }
		  	})
	     
	$('#DownloadFromdate').blur(function(){
	      		var SelectedDate=$('#DownloadFromdate').val();
	      		 var d = new Date();
			
	 		    var month = d.getMonth()+1;
	 		    var day = d.getDate();
	 		    var CurrentDate = 
	 		        ((''+day).length<2 ? '0' : '')  + day + '/' + ((''+month).length<2 ? '0' : '') + month + '/' +
	 		         d.getFullYear();
	 		    
	 		       /* alert(SelectedDate);
	 		       alert(CurrentDate); */
	 		       
	 		       if (CurrentDate > SelectedDate) {
	 		    	  	//alert('Future');
	 		    	   $('#DownloadFromdate').val('');
	 		       }
	 		       else{
	 		    	  // alert('Past');
	 		       }
		      	})
	      	
		 $("#DownloadFromdate").mouseleave(function(){
		       /*  $("p").css("background-color", "lightgray"); */
		    var SelectedDate=$('#DownloadFromdate').val();
		    var d = new Date();

		    var month = d.getMonth()+1;
		    var day = d.getDate();
		    var CurrentDate = 
		        ((''+day).length<2 ? '0' : '')  + day + '/' + ((''+month).length<2 ? '0' : '') + month + '/' +
		         d.getFullYear();
		    
		       /* alert(SelectedDate);
		       alert(CurrentDate); */
		       
		       if (CurrentDate <= SelectedDate) {
		    	  // alert('Future');
		    	   $('#DownloadFromdate').val('');
		       }
		       else{
		    	  // alert('Past');
		       }
		    });
		 
	      	
	      	$('#AssignScheduledDate').blur(function(){
	      		var SelectedDate=$('#AssignScheduledDate').val();
	      		 var d = new Date();

	 		    var month = d.getMonth()+1;
	 		    var day = d.getDate();
	 		    var CurrentDate = 
	 		        ((''+day).length<2 ? '0' : '')  + day + '/' + ((''+month).length<2 ? '0' : '') + month + '/' +
	 		         d.getFullYear();
	 		    
	 		       /* alert(SelectedDate);
	 		       alert(CurrentDate); */
	 		       
	 		       if (CurrentDate > SelectedDate) {
	 		    	  	//alert('Future');
	 		    	   $('#AssignScheduledDate').val('');
	 		       }
	 		       else{
	 		    	  // alert('Past');
	 		       }
		      	})
		      	
		      	
		      	
		 $("#AssignScheduledDate").mouseleave(function(){
			 debugger;
			 //alert('Mouse Leave');
		       /*  $("p").css("background-color", "lightgray"); */
		    var SelectedDate=$('#AssignScheduledDate').val();
		    var d = new Date();

		    var month = d.getMonth()+1;
		    var day = d.getDate();
		    var CurrentDate = 
		        ((''+day).length<2 ? '0' : '')  + day + '/' + ((''+month).length<2 ? '0' : '') + month + '/' +
		         d.getFullYear();
		    
		       /* alert(SelectedDate);
		       alert(CurrentDate); */
		       
		       if (CurrentDate > SelectedDate) {
		    	  	//alert('Future');
		    	   $('#AssignScheduledDate').val('');
		       }
		       else{
		    	  // alert('Past');
		       }
		    });
		 
		 
		 $("#ScheduledDate").mouseleave(function(){
			 debugger;
			 //alert('Mouse Leave');
		       /*  $("p").css("background-color", "lightgray"); */
		    var SelectedDate=$('#ScheduledDate').val();
		    var d = new Date();

		    var month = d.getMonth()+1;
		    var day = d.getDate();
		    var CurrentDate = 
		        ((''+day).length<2 ? '0' : '')  + day + '/' + ((''+month).length<2 ? '0' : '') + month + '/' +
		         d.getFullYear();
		    
		       /* alert(SelectedDate);
		       alert(CurrentDate); */
		       
		       if (CurrentDate > SelectedDate) {
		    	  	//alert('Future');
		    	   $('#ScheduledDate').val('');
		       }
		       else{
		    	  // alert('Past');
		       }
		    });
		 
		 $('#ScheduledDate').blur(function(){
	      		var SelectedDate=$('#ScheduledDate').val();
	      		 var d = new Date();

	 		    var month = d.getMonth()+1;
	 		    var day = d.getDate();
	 		    var CurrentDate = 
	 		        ((''+day).length<2 ? '0' : '')  + day + '/' + ((''+month).length<2 ? '0' : '') + month + '/' +
	 		         d.getFullYear();
	 		    
	 		       /* alert(SelectedDate);
	 		       alert(CurrentDate); */
	 		       
	 		       if (CurrentDate > SelectedDate) {
	 		    	  	//alert('Future');
	 		    	   $('#ScheduledDate').val('');
	 		       }
	 		       else{
	 		    	  // alert('Past');
	 		       }
		      	})
		      	
		 
		      	
        		$("#ajaxbutton").click(function() {
        			
        			var SerUrl = $('#ajaxbutton').attr("name");
        			$.ajax({
        				url : SerUrl,
        				success : function(result) {
        					//alert(result);
        					var pdfVal = result;
        					//window.location.href= result;
        					window.open(result, '_blank');
        					/* $("#div1").html(result); */
        				}
        			});
        		});
		 
		
/* 		$('#example').DataTable(); */
		
	    $('#example').DataTable( {
			
	 		 "lengthMenu": [[5,10, 25, 50, -1], [5, 10,25, 50, "All"]],
			 "bLengthChange": true,        		
			 });
	

		$('.dataTables_filter input[type="search"]').attr('placeholder', 'search');

		if ($(window).width() < 767) {
			$('.card').removeClass("slide-left");
			$('.card').removeClass("slide-right");
			$('.card').removeClass("slide-top");
			$('.card').removeClass("slide-bottom");
		}


        $('.close').click(function(){
        	//alert(1);
            $('#tktCreation').hide();
            $('.clear').click();
            $('.category').dropdown();
            $('.SiteNames').dropdown();
            $('#ddlCategory').dropdown('clear')
            
          	$('#ddlPriority').dropdown('clear');
        });
        
		$('.clear').click(function() {
			$('.search').val("");
			$('#ddlCategory').dropdown('clear')
			$('.category').dropdown('clear');
		});
		
		$('#ddlType').change(function(){              
            $('.category').dropdown('clear');
           });


		$('body').click(function() {
			$('#builder').removeClass('open');
		});
				
		  var $ddlType = $( '#ddlType' ),
          $ddlCategory = $( '#ddlCategory' ),
          $options = $ddlCategory.find( 'option' );     
          $ddlType.on( 'change', function() {
          $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
       }).trigger( 'change' );

	});
</script>


<script type='text/javascript'>//<![CDATA[
         $(window).load(function(){
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
        
         
         $('.ui.dropdown').dropdown({
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: true, 
             showOnFocus: true,
             on: "click" 
           });
         
          $(".oveallsearch").dropdown({
        	  onChange: function (value, text, $selectedItem) {                
                  var uid =  $('#hdneampmuserid').val();
                  redirectbysearchvalue(value,uid);
               },
               fullTextSearch: true,
               forceSelection: false, 
               selectOnKeydown: false, 
               showOnFocus: true,
               on: "click" 
     	})
      
 		var validation  = {
                    txtSubject: {
                               identifier: 'txtSubject',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               },
                               {
                                   type: 'maxLength[50]',
                                   prompt: 'Please enter a value'
                                 }]
                             },
                              ddlSite: {
                               identifier: 'ddlSite',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                             },
                            ddlType: {
                                  identifier: 'ddlType',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                            ddlCategory: {
                               identifier: 'ddlCategory',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                             ddlPriority: {
                               identifier: 'ddlPriority',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please select a dropdown value'
                               }]
                             },
                             description: {
                               identifier: 'description',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please Enter Description'
                               }]
                             }
             };
               var settings = {
                 onFailure:function(){
                     return false;
                   }, 
                 onSuccess:function(){    
                   $('#btnCreate').hide();
                   $('#btnCreateDummy').show()
                   //$('#btnReset').attr("disabled", "disabled");          
                   }};           
               $('.ui.form.validation-form').form(validation,settings);
   
   // Assign Popup Validation
                $('#AssignValidation')
               .form({
            	   AssignScheduledDate: {
                       identifier : 'AssignScheduledDate',
                       rules: [
                           {
                               type : 'empty'
                           }
                       ]
                   },
            	   ddlAssign: {
                           identifier : 'ddlAssign',
                           rules: [
                               {
                                   type : 'empty'
                               }
                           ]
                       },
                       remarks: {
                           identifier  : 'remarks',
                           rules: [
                                   {
                                       type: 'empty',
                                 	   prompt: 'Please enter the password'
                                   }
                           		]
                       		},
                   }, {
                     onSuccess: function() {
                       //alert('Success');
                       $('#btnAssignCreate').hide();
                       $('#btn-assign-dummy').show();
                       //alert('Btn Hide');
                        // false is required if you do don't want to let it submit

                       },
                       onFailure: function() {                    	   
                       //alert('Failure');
                       return false; // false is required if you do don't want to let it submit                                            
                       }
                     }); 
   
             // ReAssign Popup Validation
                $('#ReassignValidation')
               .form({
            	   ddlReAssign: {
                           identifier : 'ddlReAssign',
                           rules: [
                               {
                                   type : 'empty'
                               }
                           ]
                       },
                       txtReassignRemarks: {
                           identifier  : 'txtReassignRemarks',
                           rules: [
                                   {
                                       type: 'empty',
                                 	   prompt: 'Please enter the password'
                                   }
                           		]
                       		},
                   }, {
                     onSuccess: function() {
                       //alert('Success');
                       $('#btn-Reassign').hide();
                       $('#btn-reassign-dummy').show();
                       //alert('Btn Hide');
                        // false is required if you do don't want to let it submit

                       },
                       onFailure: function() {
                       //alert('Failure');
                       return false; // false is required if you do don't want to let it submit                                            
                       }
                     }); 
             
             
             // Hold Popup Validation
                $('#HoldValidation')
               .form({
            	   txtHoldRemarks: {
                           identifier : 'txtHoldRemarks',
                           rules: [
                               {
                                   type : 'empty'
                               }
                           ]
                       },
                   }, {
                     onSuccess: function() {
                    	 debugger;
                       //alert('Success');
                       
                       $('#btn-Tkt-hold').hide();
                       //alert('Button Hided');
                       $('#btn-Tkt-hold-dummy').show();
                       //alert('Btn Hide');
                       },
                       onFailure: function() {
                       //alert('Failure');
                       return false; // false is required if you do don't want to let it submit                                            
                       }
                     }); 
            //Close Popup Validation
                $('#CloseValidation')
                .form({
                	txtCloseRemarks: {
                            identifier : 'txtCloseRemarks',
                            rules: [
                                {
                                    type : 'empty'
                                }
                            ]
                        },
                    }, {
                      onSuccess: function() {
                     	 debugger;
                        //alert('Success');
                        $('#btn-Close').hide();
                        $('#btn-close-dummy').show();
                        //alert('Btn Hide');
                        },
                        onFailure: function() {
                        //alert('Failure');
                        return false; // false is required if you do don't want to let it submit                                            
                        }
                      }); 
            

         })
         
      </script>
      
      
 <style type="text/css">
#popupwidth {
	width: 80%;
	margin-left: 10%;
}
.dropdown-menu-right {
	left: -70px !important;
}

#example td a {
	color: #337ab7;
	font-weight: bold;
}
.btn-hold-width{
	width:100px;
}
.btn-close-width{
	width:100px;
}
.txt-overflow{
	position: absolute;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    width: 93%;
}
.jmrDownload {
	cursor:pointer;
	font-weight:600;
	
}
.dt-button-background {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 0 !important;
}

.tbh-width {
	width: 150px;
	padding-right: 30px !important;
}

.dt-buttons {
	margin-left: 50px;
}

.buttons-collection {
	height: 28px;
	padding: 3px;
	/*  z-index:9999; */
}

.input-sm, .form-horizontal .form-group-sm .form-control {
	font-size: 13px;
	min-height: 25px;
	height: 32px;
	padding: 8px 9px;
}

.buttons-excel {
	background: rgba(36, 95, 68, 0.87);
	color: #FFF;
}
/* 
.ui.form textarea:not ([rows] ) {
	height: 2em;
	min-height: 6em;
	max-height: 24em;
}
 */
.buttons-pdf {
	background: rgb(98, 165, 96);
	color: #FFF;
}

.dataTables_filter {
	float: right;
}

.dataTables_length {
	float: left;
}

#txtCloseRemarks
 
#dateAndTime .ui.icon.input>i.icon:before, .ui.icon.input>i.icon:after {
	left: 0;
	position: absolute;
	text-align: center;
	top: 50%;
	width: 100%;
	margin-top: -1em;
}
/* .ui.form textarea:not([rows]) {
    height: 6em !important;
    min-height: 6em !important;
    max-height: 24em;
} */
.padd-15 {
	padding-left: 15px;
	padding-bottom: 15px;
}

.p-t-10 {
	padding-top: 10px;
}
.border-tkt-view{
	border: 1px solid #ccc; padding: 10px; border-radius: 5px;height:auto;
}
</style>

<script type="text/javascript">
	window.onload = function() {
		var $ddlSiteId = $('#ddlSiteId'), $ddlEquipmentId = $('#ddlEquipmentId'), $options = $ddlEquipmentId
				.find('option');

		$ddlSiteId.on(
				'change',
				function() {
					$ddlEquipmentId.html($options.filter('[data-value="'
							+ this.value + '"]'));
				}).trigger('change');
	}
</script>
<!-- 
<script type='text/javascript'>
	//<![CDATA[
	$(window).load(function() {
		$('.ui.dropdown').dropdown({
			forceSelection : false
		});
	});
</script> -->
</head>
<body class="fixed-header">
	<input type="hidden" value="${access.userID}" id="hdneampmuserid">
	<input type="hidden" value="${access.ticketid}" id="hdnticketid">
		<input type="hidden" value="${classtate}" id="hdnstate">

	<jsp:include page="slider.jsp"/>


	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					<a href="./dashboard"><img src="resources/img/logo01.png"
						alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
		<!-- 		<div class="ui selection dropdown select-language">
    <input name="language" type="hidden" value="fr-FR">
    <div class="text">French</div>
    <i class="dropdown icon"></i>
    <div class="menu ui transition hidden">
        <div class="item" data-value="en-US">English</div>
        <div class="item active" data-value="fr-FR">French</div>
    </div>
</div> -->

<div class="form-group required field searchbx">
                       	<div class="ui  search selection  dropdown  width oveallsearch select-language">
                        	<input id="myInput" name="tags" type="text">
                                <div class="default text">search</div>
                                   <div id="myDropdown" class="menu">
                                   		<jsp:include page="searchselect.jsp" />	
                                         </div>
                                  </div>
                        	</div> 
				
				<div class="dropdown pull-right hidden-md-down user-log">
					<button class="profile-dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 circular inline"> <img
							src="resources/img/user_01.png" alt
							data-src="resources/img/user_01.png"
							data-src-retina="resources/img/user_01.png" width="32"
							height="32">
						</span>

						<c:if test="${not empty access}">
							<p class="user-login">${access.userName}</p>
						</c:if>
					</button>
					<div class="dropdown-menu dropdown-menu-right profile-dropdown"
						role="menu">
						<a class="dropdown-item disablecolor"><i
							class="pg-settings_small"></i> Settings</a> <a
							class="dropdown-item disablecolor"><i class="pg-outdent"></i>
							Feedback</a> <a class="dropdown-item disablecolor"><i
							class="pg-signals"></i> Help</a> <a href="logout1"
							class="dropdown-item color logout"><i class="pg-power"></i>
							Logout</a>
					</div>
				</div>

				  
						<div class="newticketpopup hidden">
							<p class="center mb-1 tkts" data-toggle="modal"
								data-target="#tktCreation" data-backdrop="static"
								data-keyboard="false">
								<img src="resources/img/tkt.png">
							</p>
							<p class="create-tkts" data-toggle="modal"
								data-target="#tktCreation" data-backdrop="static"
								data-keyboard="false">New Ticket</p>
						</div>
						






				<%-- <div class="dropdown pull-right hidden-md-down user-log">
					<button class="profile-dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 circular inline"> <img
							src="resources/img/user_01.png" alt
							data-src="resources/img/user_01.png"
							data-src-retina="resources/img/user_01.png" width="32"
							height="32">
						</span>

						<!-- <p class="user-login">ICE</p>
                   -->

						<c:if test="${not empty access}">
							<p class="user-login">${access.userName}</p>
						</c:if>



					</button>
					<div
						class="dropdown-menu dropdown-menu-right profile-dropdown exportlog"
						role="menu">
						<a class="dropdown-item disablecolor"><i
							class="pg-settings_small"></i> Settings</a> <a
							class="dropdown-item disablecolor"><i class="pg-outdent"></i>
							Feedback</a> <a class="dropdown-item disablecolor"><i
							class="pg-signals"></i> Help</a> <a href="login"
							class="dropdown-item color"><i class="pg-power"></i> Logout</a>
					</div>
				</div>
				<a href="#"
					class="header-icon pg pg-alt_menu btn-link m-l-10 sm-no-margin d-inline-block"
					data-toggle="quickview" data-toggle-element="#quickview"></a> --%>
			</div>
		</div>
		<div class="page-content-wrapper ">

			<div class="content ">

				<div class="container-fixed-lg">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="./dashboard"><i
								class="fa fa-home mt-7"></i></a></li>
						<li class="breadcrumb-item hover-idle"><a>Operation &
								Maintenance</a></li>
						<!-- <li class="breadcrumb-item"><a href="./ticketskpi">Tickets KPI
							</a></li> -->
							<li class="breadcrumb-item"><a href="./ticketdetails">Tickets
							</a></li>
						<li class="breadcrumb-item active">Ticket Information</li>
					</ol>
				</div>



				<div id="QuickLinkWrapper1" class="">
					<div class="card card-transparent mb-0 padd-3">
						<!-- Table Start-->
						<div class="" id="equipmentdata"
							style="padding-top: 0px !important; margin-bottom: 30px;">
							<div class="row">
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
									<div class="card card-default bg-default" data-pages="card">
										<div class="card-title min-height" style="margin-bottom:0px;">
										<div class="card-header min-hei-20">
											<div class="col-md-12 padd-0">
												<div class="col-md-6 padd-0">
													<div class="card-title  tbl-head"><i class="fa fa-ticket" aria-hidden="true"></i> Ticket Information</div>
												</div>
												
										<div class="col-md-6 padd-0">
													<c:if test="${ticketview.ticketType == 'Maintenance' && ticketview.ticketCategory == 'Modules Cleaning'}">
														<c:if test="${ticketview.ticketStatus == 4 || ticketview.ticketStatus == 5 || ticketview.state == 2 }">
													<div class="card-title tbl-head ml-15 pull-right">
														<a id="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadmodulecleaningreport/${ticketview.ticketID}/${ticketview.dayCycle}/${ticketview.timeZone}"><i class="fa fa-download" aria-hidden="true"></i> Download Reports</a> </div>
												</c:if>
											</c:if>
											
											<c:if test="${ticketview.ticketType == 'Maintenance' && ticketview.ticketCategory == 'Mechanical PM'}">
														<c:if test="${ticketview.ticketStatus == 4 || ticketview.ticketStatus == 5 || ticketview.state == 2 }">
													<div class="card-title tbl-head ml-15 pull-right">
														<a id="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadmechanicalpmreport/${ticketview.ticketID}/${ticketview.dayCycle}/${ticketview.timeZone}"><i class="fa fa-download" aria-hidden="true"></i> Download Reports</a> </div>
												</c:if>
											</c:if>
											
										<%-- 	
											<c:if test="${ticketview.ticketType == 'Maintenance' && ticketview.ticketCategory == 'JMR Visit'}">
												<c:if test="${ticketview.ticketStatus == 3 && ticketview.uploadFlag == 1}">
													<div  class="card-title tbl-head ml-15 pull-right">
													   <p  id="jmrdetailbutton" class="tbl-head jmrDownload" data-toggle="modal" data-target="#MdlDownloadReport"	data-backdrop="static" data-keyboard="false"> <i class="fa fa-download" aria-hidden="true"></i> Generate JMR Reports</p>  
													   <a  id="ajaxbutton"  class="jmrDownload" style="visibility:hidden;"  name="http://www.inspirece.com/TestReportService/pdf/downloadjmrslip/${ticketview.ticketID}"><i class="fa fa-download" aria-hidden="true"></i> Download JMR Image</a> 
													</div>
													
														
												</c:if>
											
											</c:if>
											
											<c:if test="${ticketview.ticketType == 'Maintenance' && ticketview.ticketCategory == 'JMR Visit'}">
												<c:if test="${ticketview.ticketStatus == 3 && ticketview.uploadFlag == 2}">
													
													<div  class="card-title tbl-head ml-15 pull-right">
													   <p  id="jmrdetailbutton" style="visibility:hidden;" class="tbl-head jmrDownload" data-toggle="modal" data-target="#MdlDownloadReport"	data-backdrop="static" data-keyboard="false"> <i class="fa fa-download" aria-hidden="true"></i> Generate JMR Reports</p>  
													   <a  id="ajaxbutton"  class="jmrDownload"  name="http://www.inspirece.com/TestReportService/pdf/downloadjmrslip/${ticketview.ticketID}"><i class="fa fa-download" aria-hidden="true"></i> Download JMR Image</a> 
													</div>
													
													
													
													
												</c:if>
											
											</c:if> --%>
											
										<c:if test="${ticketview.ticketType == 'Maintenance' && ticketview.ticketCategory == 'JMR Visit'}">
												<c:if test="${ticketview.ticketStatus == 3 && ticketview.uploadFlag == 1}">
													<div class="card-title tbl-head ml-15 pull-right">
													
														<p class="jmrDownload  tbl-head" data-toggle="modal" data-target="#MdlDownloadReport"
																	data-backdrop="static" data-keyboard="false"> <i class="fa fa-download" aria-hidden="true"></i> Generate JMR Reports</p>  
													</div>
												</c:if>
											
											</c:if>
											
											<c:if test="${ticketview.ticketType == 'Maintenance' && ticketview.ticketCategory == 'JMR Visit'}">
												<c:if test="${ticketview.ticketStatus == 3 && ticketview.uploadFlag == 2}">
													<div class="card-title tbl-head ml-15 pull-right">
													
														<a id="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadjmrslip/${ticketview.ticketID}/${ticketview.timeZone}"><i class="fa fa-download" aria-hidden="true"></i> Download JMR Image</a> 
														</div>
												</c:if>
											
											</c:if>
											
											
											<c:if test="${ticketview.ticketType == 'Maintenance' && ticketview.ticketCategory == 'Preventive Maintenance'}">
												<c:if test="${ticketview.ticketStatus == 4 || ticketview.ticketStatus == 5 || ticketview.state == 2 }">
													<div class="card-title tbl-head ml-15 pull-right">
														
														<a id="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadpmticket/${ticketview.ticketID}/${ticketview.timeZone}">
														 <i class="fa fa-download" aria-hidden="true"></i> Download Reports</a>  
													</div>
												</c:if>
											
											</c:if>
											<c:if test="${ticketview.ticketType == 'Maintenance' && ticketview.ticketCategory == 'JMR Visit'}">
														<c:if test="${ticketview.ticketStatus == 4 || ticketview.ticketStatus == 5 || ticketview.state == 2 }">
													<div class="card-title tbl-head ml-15 pull-right">
														<a id="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadjmrreport/${ticketview.ticketID}/${ticketview.timeZone}">
														 <i class="fa fa-download" aria-hidden="true"></i> Download Reports</a> 
													</div>
												</c:if>
											</c:if>
											
											<c:if test="${ticketview.ticketType == 'Maintenance' && ticketview.ticketCategory == 'Vegetation'}">
														<c:if test="${ticketview.ticketStatus == 4 || ticketview.ticketStatus == 5 || ticketview.state == 2 }">
													<div class="card-title tbl-head ml-15 pull-right">
														<a id="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadvegetationreport/${ticketview.ticketID}/${ticketview.dayCycle}/${ticketview.timeZone}">
														 <i class="fa fa-download" aria-hidden="true"></i> Download Reports</a> 
													</div>
												</c:if>
											</c:if>
											<c:if test="${ticketview.ticketType == 'Maintenance' && ticketview.ticketCategory == 'Inverter Cleaning'}">
														<c:if test="${ticketview.ticketStatus == 4 || ticketview.ticketStatus == 5 || ticketview.state == 2 }">
													<div class="card-title tbl-head ml-15 pull-right">
														<a id="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadinvertercleaningreport/${ticketview.ticketID}/${ticketview.dayCycle}/${ticketview.timeZone}">
														 <i class="fa fa-download" aria-hidden="true"></i> Download Reports</a> 
													</div>
												</c:if>
											</c:if>
											<c:if test="${ticketview.ticketType == 'Maintenance' && ticketview.ticketCategory == 'DataLogger Cleaning'}">
														<c:if test="${ticketview.ticketStatus == 4 || ticketview.ticketStatus == 5 || ticketview.state == 2 }">
													<div class="card-title tbl-head ml-15 pull-right">
														<a id="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloaddlcleaningreport/${ticketview.ticketID}/${ticketview.dayCycle}/${ticketview.timeZone}">
														 <i class="fa fa-download" aria-hidden="true"></i> Download Reports</a> 
													</div>
												</c:if>
											</c:if>
											<c:if test="${ticketview.ticketType == 'Maintenance' && ticketview.ticketCategory == 'String Current Measurement'}">
														<c:if test="${ticketview.ticketStatus == 4 || ticketview.ticketStatus == 5 || ticketview.state == 2 }">
													<div class="card-title tbl-head ml-15 pull-right">
														<a id="ajaxbutton" name="http://www.inspirece.com/eirareportservice/pdf/aws/downloadstringmeasurementreport/${ticketview.ticketID}/${ticketview.dayCycle}/${ticketview.timeZone}">
														 <i class="fa fa-download" aria-hidden="true"></i> Download Reports</a> 
													</div>
												</c:if>
											</c:if>
											
											

										</div>
											</div>
                               			</div>
										</div>
										<!-- <div class="card-block" style="padding:5px !important">
											<div class="borders" style="border:1px solid #CCC;border-radius:5px;padding:10px;"> -->
											<div class="card-block borders" style="padding:5px !important;margin-left:7px;margin-right:7px;">
											<div> 
												
												<div class="col-md-12 padd-0">
													<div class="col-md-4 col-xs-12">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">
																<b>Ticket No</b>
															</p>
														</div>
														<div class="col-md-1 col-xs-1">
															<p class="tkttext"><b>:</b></p>
														</div>
														<div class="col-md-7 col-xs-6 padd-0">
															<p class="tkttext">
																<b>${ticketview.ticketCode}</b>
															</p>
															<%-- <span class="tkttext tkt-bg m-l-15"><b>${ticketview.ticketCode}</b></span> --%>

														</div>
													</div>
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Site Code</p>
														</div>
														<div class="col-md-1 col-xs-1">
															<p class="tkttext"><b>:</b></p>
														</div>
														<div class="col-md-7 col-xs-6  padd-0">
																<p class="tkttext">																
																<span>${ticketview.siteCode}</span>
															</p>
															
														</div>
													</div>
												   <div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Site Name</p>
														</div>
														<div class="col-md-1 col-xs-1">
															<p class="tkttext"><b>:</b></p>
														</div>
														<div class="col-md-7 col-xs-6 padd-0">
															<p class="tkttext txt-overflow" title="${ticketview.siteName}">${ticketview.siteName}
																<%-- : <span class="tkttext txt-overflow m-l-15" title="${ticketview.siteName}">${ticketview.siteName}</span>	 --%>														
															</p>														
														</div>
													</div>
												</div>
												<div class="col-md-12 m-t-15 padd-0">
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Status</p>
														</div>
														<div class="col-md-1 col-xs-1"><p class="tkttext"><b>:</b></p></div> 
														<div class="col-md-7 col-xs-6 padd-0">
															<p class="tkttext"
																style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
																 
															  <c:choose>
                                                            <c:when test="${ticketview.ticketStatus == 1}">
                                                                <td>Created</td>
                                                            </c:when>
                                                            
                                                            <c:when test="${ticketview.ticketStatus == 2}">
                                                                <td>Assigned</td>
                                                            </c:when>
                                                            <c:when test="${ticketview.ticketStatus == 3}">
                                                                <td>Inprogress</td>
                                                            </c:when>
                                                            <c:when test="${ticketview.ticketStatus == 4}">
                                                                <td>Unfinished</td>
                                                            </c:when>
                                                            <c:when test="${ticketview.ticketStatus == 5}">
                                                                <td>Finished</td>
                                                            </c:when>
                                                            <c:when test="${ticketview.ticketStatus == 6}">
                                                                <td>Closed</td>
                                                            </c:when>
                                                            <c:when test="${ticketview.ticketStatus == 7}">
                                                                <td>Hold</td>
                                                            </c:when>

                                                            <c:otherwise>
                                                                <td>-</td>
                                                            </c:otherwise>
                                                        </c:choose>
																
															</p>
														</div>
													</div>
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Priority</p>
														</div>
														<div class="col-md-1 col-xs-1"><p class="tkttext"><b>:</b></p></div> 
														<div class="col-md-7 col-xs-6  padd-0">
															<p class="tkttext">																
																<span>${ticketview.priorityText}</span>
															</p>
														</div>
														
														
													</div>
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Scheduled On</p>
														</div>
														<div class="col-md-1 col-xs-1"><p class="tkttext"><b>:</b></p></div> 
														<div class="col-md-7 col-xs-6 padd-0">
															<p class="tkttext">${ticketview.scheduledDateText}
																<%-- <span class="tkttext  m-l-15">${ticketview.scheduledDateText}</span> --%>
															</p>
														</div>
													</div>
												</div>
												
												
												<div class="col-md-12 m-t-15 padd-0">
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Subject</p>
														</div>
														<div class="col-md-1 col-xs-1"><p class="tkttext"><b>:</b></p></div>
														<div class="col-md-7 col-xs-6 padd-0">
															<p class="tkttext" title="${ticketview.ticketDetail}"
																style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">${ticketview.ticketDetail}
																<%-- <span class="tkttext  m-l-15">${ticketview.ticketDetail}</span> --%>
															</p>
														</div>
													</div>
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Type</p>
														</div>
														<div class="col-md-1 col-xs-1"><p class="tkttext"><b>:</b></p></div>
														<div class="col-md-7 col-xs-6  padd-0">
															<p class="tkttext">																
																<span>${ticketview.ticketType}</span>
															</p>
															
														</div>
													</div>
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Started On</p>
														</div>
														<div class="col-md-1 col-xs-1"><p class="tkttext"><b>:</b></p></div>
														<div class="col-md-7 col-xs-6 padd-0">
															<p class="tkttext">${ticketview.startedDateText}
																<%-- <span class="tkttext">${ticketview.startedDateText}</span> --%>
															</p>
														</div>
													</div>
												</div>
												
												<div class="col-md-12 m-t-15 padd-0">
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Created On</p>
														</div>
														<div class="col-md-1 col-xs-1"><p class="tkttext"><b>:</b></p></div>
														<div class="col-md-7 col-xs-6 padd-0">
															<p class="tkttext">${ticketview.createdDateText}
																<%-- <span class="tkttext  m-l-15">${ticketview.createdDateText}</span> --%>
															</p>
														</div>
													</div>
													<div class="col-md-4 col-xs-12  mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Category</p>
														</div>
														<div class="col-md-1 col-xs-1"><p class="tkttext"><b>:</b></p></div>
														<div class="col-md-7 col-xs-6  padd-0 mml--3">
																<p class="tkttext">	${ticketview.ticketCategory}															
																<%-- <span>${ticketview.ticketCategory}</span> --%>
															</p>
															
														</div>
													</div>
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Closed by</p>
														</div>
														<div class="col-md-1 col-xs-1"><p class="tkttext"><b>:</b></p></div>
														<div class="col-md-7  col-xs-6 padd-0">
															<p class="tkttext">${ticketview.closedByName}
																<%-- :<span class="tkttext  m-l-15">${ticketview.closedByName}</span> --%>
															</p>
														</div>
													</div>
													<!-- <div class="col-md-4">
                               					<div class="col-md-4"><p class="tkttext">Finished On</p></div>
                               					<div class="col-md-8">
                               						<p class="tkttext">:<span class="tkttext  m-l-15">21-01-2018 11:10 AM</span></p>
                               					</div>
                               				</div> -->
												</div>
												<div class="col-md-12 m-t-15 padd-0">
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Created by</p>
														</div>
														<div class="col-md-1 col-xs-1"><p class="tkttext"><b>:</b></p></div>
														<div class="col-md-7 col-xs-6 padd-0">
															<p class="tkttext">${ticketview.createdByName}
																<%-- :<span class="tkttext  m-l-15">${ticketview.createdByName}</span> --%>
															</p>
														</div>
													</div>

													<div class="col-md-4 col-xs-12  mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Assign To</p>
														</div>
														<div class="col-md-1 col-xs-1"><p class="tkttext"><b>:</b></p></div>
														<div class="col-md-7 col-xs-6  padd-0 mml--3">
																<p class="tkttext m-l-0">																
																<span>${ticketview.assignedToWhom}</span>
															</p>
															
														</div>
														
														
														
													</div>
													<div class="col-md-4 col-xs-12  mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Closed On</p>
														</div>
														<div class="col-md-1 col-xs-1"><p class="tkttext"><b>:</b></p></div>
														<div class="col-md-7 col-xs-6 padd-0">
															<p class="tkttext">${ticketview.closedDateText}
																<%-- :<span class="tkttext  m-l-15">${ticketview.closedDateText}</span> --%>
															</p>
														</div>
													</div>

												</div>
												
												<div class="col-md-12 col-xs-12 m-t-15 padd-0">	
												<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Description</p>
														</div>
														<div class="col-md-1 col-xs-1"><p class="tkttext"><b>:</b></p></div>
														<div class="col-md-7 col-xs-6 padd-0">
															<p class="tkttext">${ticketview.description}
																<%-- :<span class="tkttext  m-l-15">${ticketview.createdByName}</span> --%>
															</p>
														</div>
													</div>													
													<div class="col-md-4 col-xs-12  mm-10">
														<div class="col-md-4 col-xs-4 padd-0">
															<p class="tkttext">Equipment Name</p>
														</div>
														<div class="col-md-1 col-xs-1"><p class="tkttext"><b>:</b></p></div>
														<div class="col-md-7 col-xs-6  padd-0 mml--3">
																<p class="tkttext m-l-0">	
																 <span>
																	<c:choose>
																		<c:when test="${ticketview.equipmentName == '' || ticketview.equipmentName == null}">Not Applicable</c:when>
																		<c:otherwise>${ticketview.equipmentName}</c:otherwise>
																	</c:choose>

																</span> 
															</p>
														</div>
													</div>
												</div>
												<div class="col-md-12 m-t-15 center mm-10">
													<c:choose>
														<c:when test="${ticketview.state == 1}">
															<c:choose>
																<c:when test="${ticketview.ticketStatus == 1}">
																	<div class="col-md-3"></div>
																	   <c:if test="${access.customerListView == 'visible'}">
																	<div class="col-md-2">
																		<button class="btn btn-primary-sub btn-width"
																			data-toggle="modal" data-target="#MdlAssign"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt-assign.png"><span
																				class="m-l-10">Assign</span>
																		</button>
																	</div>
																	</c:if>
																	<c:if test="${access.customerListView == 'visible'}">
																	<div class="col-md-2">
																		<button class="btn btn-primary-hold btn-width"
																			data-toggle="modal" data-target="#MdlHold"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_Hold_06.png"><span
																				class="m-l-10">Hold</span>
																		</button>
																	</div>
																	</c:if>
																	<c:if test="${access.customerListView == 'visible'}">
																	<div class="col-md-2">
																		<button class="btn btn-primary btn-width"
																			data-toggle="modal" data-target="#MdlClose"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/btn-close.png"><span
																				class="m-l-10">Close</span>
																		</button>
																	</div>
																	</c:if>
																	<!-- <div class="col-md-2"><button class="btn btn-primary btn-width" data-toggle="modal" data-target="#MdlClose" data-backdrop="static" data-keyboard="false"><img src="resources/img/btn-close.png"><span class="m-l-10">Close</span></button></div> -->
																	<div class="col-md-3"></div>
																</c:when>
																<c:when test="${ticketview.ticketStatus == 2}">
																	<div class="col-md-3"></div>
																	<c:if test="${access.customerListView == 'visible'}">
																	<div class="col-md-2">
																	
																		<button class="btn btn-primary-sub btn-width"
																			data-toggle="modal" data-target="#MdlReassign"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_reassign.png"><span
																				class="m-l-10">Reassign</span>
																		</button>
																		
																	</div>
																	</c:if>
																	<c:if test="${access.customerListView == 'visible'}">
																	<div class="col-md-2">
																		<button class="btn btn-primary-hold btn-width"
																			data-toggle="modal" data-target="#MdlHold"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_Hold_06.png"><span
																				class="m-l-10">Hold</span>
																		</button>
																	</div>
																	</c:if>
																	<c:if test="${access.customerListView == 'visible'}">
																	<div class="col-md-2">
																		<button class="btn btn-primary btn-width"
																			data-toggle="modal" data-target="#MdlClose"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/btn-close.png"><span
																				class="m-l-10">Close</span>
																		</button>
																	</div>
																	</c:if>
																	
																	<div class="col-md-3"></div>
																</c:when>
																<c:when test="${ticketview.ticketStatus == 3}">
																	<div class="col-md-3"></div>
																	<c:if test="${access.customerListView == 'visible'}">
																	
																	<div class="col-md-3">
																	<c:if test="${ticketview.ticketCategory !='JMR Visit'}">
																		<button class="btn btn-primary-sub btn-width"
																			data-toggle="modal" data-target="#MdlReassign"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_reassign.png"><span
																				class="m-l-10">Reassign</span>
																		</button>
																	</c:if>
																	
																	
																	</div>
																	</c:if>
																	<c:if test="${access.customerListView == 'visible'}">
																	<div class="col-md-3">
																		<button class="btn btn-primary-hold btn-width"
																			data-toggle="modal" data-target="#MdlHold"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_Hold_06.png"><span
																				class="m-l-10">Hold</span>
																		</button>
																	</div>
																	</c:if>
																	<!-- <div class="col-md-2"><button class="btn btn-primary btn-width" data-toggle="modal" data-target="#MdlClose" data-backdrop="static" data-keyboard="false"><img src="resources/img/btn-close.png"><span class="m-l-10">Close</span></button></div> -->
																	<div class="col-md-3"></div>
																</c:when>
																<c:when test="${ticketview.ticketStatus == 4}">
																	<div class="col-md-3"></div>
																	<c:if test="${access.customerListView == 'visible'}">
																	<div class="col-md-2">
																	<c:if test="${ticketview.ticketCategory !='JMR Visit'}">
																		<button class="btn btn-primary-sub btn-width"
																			data-toggle="modal" data-target="#MdlReassign"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_reassign.png"><span
																				class="m-l-10">Reassign</span>
																		</button>
																		</c:if>
																	</div>
																	</c:if>
																	<c:if test="${access.customerListView == 'visible'}">
																	
																	<div class="col-md-2">
																		<button class="btn btn-primary-hold btn-width"
																			data-toggle="modal" data-target="#MdlHold"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_Hold_06.png"><span
																				class="m-l-10">Hold</span>
																		</button>
																	</div>
																	</c:if>
																	<c:if test="${access.customerListView == 'visible'}">
																	
																	<div class="col-md-2">
																		<button class="btn btn-primary btn-width"
																			data-toggle="modal" data-target="#MdlClose"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/btn-close.png"><span
																				class="m-l-10">Close</span>
																		</button>
																	</div>
																	</c:if>
																	
																	<div class="col-md-3"></div>
																</c:when>
																<c:when test="${ticketview.ticketStatus == 5}">
																	<div class="col-md-3"></div>
																	<c:if test="${access.customerListView == 'visible'}">
																	
																	<div class="col-md-2">
																	<c:if test="${ticketview.ticketCategory !='JMR Visit'}">
																		<button class="btn btn-primary-sub btn-width"
																			data-toggle="modal" data-target="#MdlReassign"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_reassign.png"><span
																				class="m-l-10">Reassign</span>
																		</button>
																		</c:if>
																	</div>
																	</c:if>
																	<c:if test="${access.customerListView == 'visible'}">
																	
																	<div class="col-md-2">
																		<button class="btn btn-primary-hold btn-width"
																			data-toggle="modal" data-target="#MdlHold"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_Hold_06.png"><span
																				class="m-l-10">Hold</span>
																		</button>
																	</div>
																	</c:if>
																	<c:if test="${access.customerListView == 'visible'}">
																	
																	<div class="col-md-2">
																		<button class="btn btn-primary btn-width"
																			data-toggle="modal" data-target="#MdlClose"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/btn-close.png"><span
																				class="m-l-10">Close</span>
																		</button>
																	</div>
																	</c:if>
																	<div class="col-md-3"></div>
																</c:when>
																<c:when test="${ticketview.ticketStatus == 6}">
																</c:when>
																<c:when test="${ticketview.ticketStatus == 7}">
																	<div class="col-md-3"></div>
																	<c:if test="${access.customerListView == 'visible'}">
																	
																	<div class="col-md-3">
																	<c:if test="${ticketview.ticketCategory !='JMR Visit'}">
																		<button class="btn btn-primary-sub btn-width"
																			data-toggle="modal" data-target="#MdlReassign"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_reassign.png"><span
																				class="m-l-10">Reassign</span>
																		</button>
																		</c:if>
																	</div>
																	</c:if>
																	<c:if test="${access.customerListView == 'visible'}">
																	
																	<div class="col-md-3">
																		<button class="btn btn-primary btn-width"
																			data-toggle="modal" data-target="#MdlClose"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/btn-close.png"><span
																				class="m-l-10">Close</span>
																		</button>
																	</div>
																	</c:if>
																	<div class="col-md-3"></div>
																</c:when>
																<c:otherwise>
																</c:otherwise>
															</c:choose>


															<%-- <c:choose>
																<c:when test="${ticketview.assignedTo == null}">

																</c:when>
																
																<c:when test="${ticketview.assignedTo == ''}">

																</c:when>
																<c:otherwise>

																</c:otherwise>
															</c:choose> --%>





														</c:when>
														<c:when test="${ticketview.state == 2}">
														</c:when>
														<c:when test="${ticketview.state == 3}">
															<div class="col-md-3"></div>
															<c:if test="${access.customerListView == 'visible'}">
																	
															<div class="col-md-3">
															<c:if test="${ticketview.ticketCategory !='JMR Visit'}">
																<button class="btn btn-primary-sub btn-width"
																	data-toggle="modal" data-target="#MdlReassign"
																	data-backdrop="static" data-keyboard="false">
																	<img src="resources/img/tkt_reassign.png"><span
																		class="m-l-10">Reassign</span>
																</button>
																</c:if>
															</div>
															</c:if>
															<c:if test="${access.customerListView == 'visible'}">
																	
															<div class="col-md-3">
																<button class="btn btn-primary btn-width"
																	data-toggle="modal" data-target="#MdlClose"
																	data-backdrop="static" data-keyboard="false">
																	<img src="resources/img/btn-close.png"><span
																		class="m-l-10">Close</span>
																</button>
															</div>
															</c:if>
															<div class="col-md-3"></div>

														</c:when>
														<c:otherwise>

														</c:otherwise>
													</c:choose>
												</div>
											</div>
												
										</div>
										<div class="table-responsive padd-5">
											<table id="example" class="table table-striped table-bordered" width="100%" cellspacing="0">
												<thead>
													<tr>
														<th style="width: 10%;">S.No</th>
														<th style="width: 30%;">Date Time</th>
														<th style="width: 30%;">Ticket Transactions</th>
														<th style="width: 15%;">Assigned To</th>
														<th style="width: 30%;">Remarks</th>
													</tr>
												</thead>
												<tbody>

													<c:forEach items="${tickethistorylist}" var="tickethistory">
														<tr>
														<td style="text-align: center;">${tickethistory.serialNo}</td>
															<td>${tickethistory.ticketDate}</td>
															<td>${tickethistory.ticketTransaction}</td>
															<td>${tickethistory.userName}</td>
															<td>${tickethistory.remarks}</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>

						</div>
						<!-- Table End-->
					</div>
				</div>


			</div>
<div class="container-fixed-lg footer mb-0">
				<div class="container-fluid copyright sm-text-center">
					<p class="small no-margin pull-left sm-pull-reset">
						<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
							CLEAN ENERGY</span>. <span class="hint-text">All rights reserved.
						</span>
					</p>
					<p class="small no-margin pull-rhs sm-pull-reset">
						<span class="hint-text">Powered by</span> <span>MESTECH
							SERVICES PVT LTD</span>.
					</p>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Side Bar Content Start-->

	


	<div class="quickview-wrapper  builder hidden-sm hidden-xs"
		id="builder">
		<div class="p-l-10 p-r-10 ">
			<a class="builder-close quickview-toggle pg-close"></a>
			<a class="builder-toggle" data-toggle="quickview"
				data-toggle-element="#builder"><img
				src="resources/img/ImageResize_06.png"></a>
			<ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary"
				id="builderTabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#tabLayouts" role="tab"
					aria-controls="home"><span>Quick Link</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active " id="tabLayouts" role="tabcard">
					<div class="scrollable">
						<div class="p-r-50">
							<div class="list">
								<ul>
									<li><a href="#QuickLinkWrapper1">Ticket Information</a></li>

								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>






	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>
	
	<div id="gotop"></div>

		<!-- Ticket Creation Popup Start-->
		<!-- Modal -->
<div id="tktCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog modal-top">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">New Ticket</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"><span class="errormess">*</span> All fields are mandatory</p>
				 </div>            
        		<form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form validation-form"> 
             
             <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width SiteNames"
									id="ddlSite" name="ddlSite" path="siteID" onchange="getEquipmentAgainstSite()">
									<form:option value="" tabindex="0">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>
						
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                                                <div class="col-md-4 col-xs-12">
                                                       <label class="fields required m-t-10">Equipment</label>
                                                </div>

                                                <div class="col-md-8 col-xs-12">
                                                       <form:select
                                                              class="field ui fluid search selection dropdown width equipments"
                                                              id="ddlEquipment" path="equipmentID" name="ddlEquipment">
                                                              <option value="">Select</option>
                                                       </form:select>
                                                </div>
                          </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Type</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width" id="ddlType" name="ddlType"  path="ticketType">
                        <form:option value="">Select</form:option>
                        <form:option value="Operation">Operation</form:option>
                        <form:option value="Maintenance">Maintenance</form:option>
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Category</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width category" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                         <form:option data-value="Operation" value="">Select</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Preventive Maintenance">Preventive Maintenance</form:option>
										
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                          <form:option data-value="Maintenance" value="">Select</form:option>
                    </form:select> 
                  </div>
                  
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                  <div class="col-md-4 col-xs-12"><label class="fields required m-t-10">Subject</label></div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<form:input  placeholder="Subject"  autocomplete="off" name="Subject"  type="text"  id="txtSubject" path="ticketDetail" maxLength="50"/>
                  	</div>
                  	</div>
               </div> 
              
              <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Priority</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width" id="ddlPriority" name="user" path="priority">
                        <form:option value="">Select </form:option>
                        <form:option data-value="3" value="3">High</form:option>
                        <form:option data-value="2" value="2">Medium</form:option>
                        <form:option data-value="1" value="1">Low</form:option>
                    </form:select> 
                  </div>
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Description</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<textarea id="txtTktdescription"  autocomplete="off" name="description" style="resize: none;" placeholder="Ticket Description" maxLength="120"></textarea>
                  	</div>
                    
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
				       <div class="btn btn-success  submit" id="btnCreate">Create</div>
				       <button class="btn btn-success" type="button" id="btnCreateDummy" tabindex="7"  style="display:none;">Create</button>
				        <div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
				  </div>
  <!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
  <!-- <div class="ui error message"></div> -->
</form:form>
				<!-- <div class="ui error message"></div> -->
				
	  		</div>
	  </div>
    </div>
  </div>
<!-- Address Popup End !--> 
	<!-- Ticket Creation Popup End -->
	
	<!--  Close Ticket Popup Start -->
<c:url var="addAction1" value="/closeticket${ticketview.ticketID}"></c:url>

	<form:form action="${addAction1}" modelAttribute="ticketview"
		class="ui form" method="post" id="CloseValidation">
		<form:hidden path="ticketID" />


		<!-- Close Popup Start -->
		<div id="MdlClose" class="modal fade" role="dialog">
			<div class="modal-dialog modal-top modal-popup modal-md">
				<!-- Modal content-->
				<div class="modal-content" id="popupwidth">
					<div class="modal-header Popup-header">
						<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
						<button type="button" class="close" data-dismiss="modal">
							<img src="resources/img/close.png">
						</button>
						<h5 class="modal-title">
							<img class="mt--7" src="resources/img/tkt_close.png"><span
								class="popup-head">Close Ticket</span>
						</h5>
					</div>
					<div class="modal-body" id="txtCloseRemarks">
					<form class="ui form">							
							<div class="col-md-12 m-t-15 padd-0">
								<div class="field">
									<div class="col-lg-12 padd-0">
										<form:textarea placeholder="Enter Remarks*"
											style="resize:none;" class="AssignDesc" name="txtCloseRemarks" autocomplete="off"
											id="txtCloseRemarks" maxlength="500" path="closedRemarks"></form:textarea>
									</div>
								</div>
							</div>
							<div class="col-md-12 center m-t-20">
							<div class="col-md-6 col-xs-6 padd-0">
								<button type="submit" id="btn-Close" class="btn btn-success submit btn-mres btn-close-width">Confirm</button>
								<button type="button" id="btn-close-dummy" class="btn btn-success btn-mres btn-close-width" style="display:none;">Confirm</button>
							</div>
							<div class="col-md-6 col-xs-6 padd-0">
							<button type="button" class="btn btn-primary clear btn-mres btn-close-width">Reset</button></div>
							 	 	<!-- <button type="submit" class="btn btn-success submit">Hold</button>
                                  	<button type="button" class="btn btn-primary clear" data-dismiss="modal">Cancel</button> -->
							</div>
						</form>
					</div>
					
				</div>

			</div>
		</div>
		<!--  Close Popup End -->
	</form:form>

<!--  Close Ticket Popup End -->
	
	<!--  Assign Ticket Popup Start -->
	<c:url var="addAction0" value="/assignticket${ticketview.ticketID}"></c:url>

	<form:form action="${addAction0}" modelAttribute="ticketview"
		class="ui form" method="post" id="AssignValidation">
		<form:hidden path="ticketID" />

		<!-- Close Popup Start -->
		<div id="MdlAssign" class="modal fade" role="dialog">
			<div class="modal-dialog modal-top modal-popup modal-md">
				<!-- Modal content-->
				<div class="modal-content" id="popupwidth">
					<div class="modal-header Popup-header">
						<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
						<button type="button" class="close" data-dismiss="modal">
							<img src="resources/img/close.png">
						</button>
						<h5 class="modal-title">
							<img  src="resources/img/tkt-assign.png"><span
								class="popup-head">Assign Ticket</span>
						</h5>
					</div>
					<div class="modal-body" id="txtCloseRemarks">
						<form class="ui form assign-validation">
								<div class="col-md-12 col-xs-12 col-lg-12 padd-0">
								<div class="ui form">
									<div class="col-md-4 m-t-8">
										<label class="fields">Date<span
											class="errormess">*</span></label>
									</div>
									<div class="col-md-8">
										<div class="input-group required">
											<form:input id="AssignScheduledDate" type="text"
												placeholder="Scheduled On" class="date-picker form-control" autocomplete="off"
												name="Datetime" path="scheduledOnText" required="required"
												style="z-index:9999 !important;" />
											<label for="AssignScheduledDate"
												class="input-group-addon btn"
												style="z-index: 9999 !important;"><span
												class="glyphicon glyphicon-calendar"
												style="z-index: 9999 !important;"></span> </label>
										</div>
									</div>
								</div>
							</div>
							    <div class="col-md-12 field m-t-25 padd-0">
                               <div class="col-md-4"> <label class="fields m-t-10">Assign To<span class="errormess">*</span></label></div>
                                   <div class="col-md-8" style="z-index:999999 !important;">                                     
						                   <form:select class="ui fluid search selection dropdown width required" id="ddlAssign" name="ddlAssign" path="assignedTo">
						                         <form:option value="">Select</form:option>
                       								<c:forEach items="${userList}" var="userlist">
												<form:option value="${userlist.userId}">${userlist.userName}</form:option>
											</c:forEach>
						                    </form:select>
                         </div>
                     		</div>
							
							<div class="col-md-12 mmt-40">
								<div class="field">
									<div class="col-md-12 mrs-40 padd-0"  style="width:100%;overflow:none;">
										<form:textarea placeholder="Enter Remarks*"
											style="margin-top: 10px;" class="AssignDesc mrs-10" name="descr"
											id="txtActivityDescription" autocomplete="off" path="remarks" maxLength="500"></form:textarea>
									</div>
								</div>
							</div>

							<div class="col-md-12 center m-t-15">
								<div class="col-md-6 col-xs-6 padd-0">
									<!--  <div  class="btn btn-success btn-width btn-mres submit mm-10 submit" id="btnAssignCreate" tabindex="7">Confirm</div> -->
									<!-- <button type="submit" id="btndummyAssignCreate" class="btn btn-success btn-width btn-mres submit mm-10">Confirm</button> -->
									<button type="submit" id="btnAssignCreate" class="btn btn-success btn-width btn-mres  mm-10 submit">Confirm</button>								
									<button type="button" id="btn-assign-dummy" class="btn btn-success btn-width btn-mres  mm-10" style="display:none;">Confirm</button>
									 <!-- <button type="submit"  class="btn btn-success btn-width btn-mres submit mm-10 submit" id="btnAssignCreate" tabindex="7">Confirm</button>
									<button type="button" id="btn-assign-dummy" class="btn btn-success btn-width btn-mres  mm-10" style="display:none;">Confirm</button> -->
								</div>
								<div class="col-md-6 col-xs-6 padd-0">
								<button type="button" class="btn btn-primary btn-width btn-mres clear mm-10">Reset</button></div>
							</div>
							<!-- <div class="ui blue submit button">Submit</div>
<div class="ui blue clear button">Clear</div> -->
							<!-- <div class="ui error message"></div>  -->
						</form>
					</div>

				</div>

			</div>
		</div>
</form:form>
	<!--  Assign Ticket Popup End -->
	
	<!--  Reassign Ticket Popup Start -->
	<c:url var="addAction2" value="/reassignticket${ticketview.ticketID}"></c:url>
	<form:form action="${addAction2}" modelAttribute="ticketview"
		class="ui form reassign" method="post" id="ReassignValidation">
		<!-- Close Popup Start -->
		<div id="MdlReassign" class="modal fade" role="dialog">
			<div class="modal-dialog modal-top modal-popup modal-md">
				<!-- Modal content-->
				<div class="modal-content" id="popupwidth">
					<div class="modal-header Popup-header">
						<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
						<button type="button" class="close" data-dismiss="modal">
							<img src="resources/img/close.png">
						</button>
						<h5 class="modal-title">
							<img  src="resources/img/tkt_reassign.png"><span
								class="popup-head">Reassign Ticket</span>
						</h5>
					</div>
					<div class="modal-body" id="txtCloseRemarks">
						<form:hidden path="ticketID" />
						
						<form class="ui form assign-validation">
								<div class="col-md-12 padd-0">
								<div class="ui form">
									<div class="col-md-4 padd-0">
										<label class="fields m-t-8">Date<span
											class="errormess">*</span></label>
									</div>
									<div class="col-md-8 padd-0">
										<div class="input-group required">
										<form:input id="ScheduledDate" type="text" autocomplete="off"
											placeholder="Scheduled On" class="date-picker form-control"
											name="Datetime" path="rescheduledOnText" required="required"
											style="z-index:9999 !important;" />
										<label for="ScheduledDate" class="input-group-addon btn"
											style="z-index: 9999 !important;"><span
											class="glyphicon glyphicon-calendar"
											style="z-index: 9999 !important;"></span> </label>
									</div>
									</div>
								</div>
							</div>
							    <div class="col-md-12 m-t-15 padd-0" style="margin:0px;">
                               <div class="col-md-4 padd-0"> <label class="fields m-t-10">Reassign<span class="errormess">*</span></label></div>
                                   <div class="col-md-8 padd-0" style="z-index:999999 !important;">  
                                   	<div class="field">
                                   		<form:select class="ui fluid search selection dropdown width required" id="ddlReAssign" name="ddlReAssign" path="reassignedTo">
						                         <form:option value="">Select</form:option>
                       								<c:forEach items="${userList}" var="userlist">
												<form:option value="${userlist.userId}">${userlist.userName}</form:option>
											</c:forEach>
						                    </form:select>
                                   	</div>                             
						                   
                        		 </div>
                     		</div>
						
							<div class="col-md-12 padd-0  m-t-15">
								
									<div class="col-lg-12 padd-0">
									<div class="field">
										<form:textarea placeholder="Enter Remarks*"
											style="resize:none;margin-top: 10px;" class="AssignDesc" name="txtReassignRemarks"
											id="txtReassignRemarks" autocomplete="off" path="reassignRemarks" maxLength="500"></form:textarea>
									</div>
								</div>
							</div>

							<div class="col-md-12 center m-t-15">
							<div class="col-md-6 col-xs-6 padd-0">
								<button type="submit" id="btn-Reassign" class="btn btn-success btn-reassign-width btn-mres submit">Confirm</button>
								
								<button type="button" id="btn-reassign-dummy" class="btn btn-success btn-reassign-width btn-mres  mm-10" style="display:none;">Confirm</button>
								
							</div>
							<div class="col-md-6 col-xs-6 padd-0"><button type="button" class="btn btn-primary btn-reassign-width btn-mres clear">Reset</button></div>
							</div>
						
						</form>
					</div>

				</div>

			</div>
		</div>
</form:form>
<!--  Reassign Popup End -->

<!-- Hold Ticket Popup Start -->
<c:url var="addAction3" value="/holdticket${ticketview.ticketID}"></c:url>
	<form:form action="${addAction3}" modelAttribute="ticketview" class="ui form" method="post" id="HoldValidation"> 
		<form:hidden path="ticketID" />
		<!--  Hold Popup Start -->
		<div id="MdlHold" class="modal fade" role="dialog">
			<div class="modal-dialog modal-top modal-popup modal-md">
				<!-- Modal content-->
				<div class="modal-content" id="popupwidth">
					<div class="modal-header popup-header">
						<button type="button" class="close" data-dismiss="modal">
							<img src="resources/img/close.png">
						</button>
						<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
						<h5 class="modal-title">
							<img  src="resources/img/tkt_Hold_06.png"><span
								class="popup-head">Hold Ticket</span>
						</h5>
					</div>
					<div class="modal-body" id="txtCloseRemarks">
						<!--  <h5 class="m-l-20">Do you want  hold this ticket?</h5> -->
						<form class="ui form assignValid">
 							<div class="col-md-12 m-t-15 padd-0"> 								   
  								<div class="field">
      								<form:textarea placeholder="Enter Remarks*" style="resize:none;" class="AssignDesc" name="txtHoldRemarks" id="txtHoldRemarks" autocomplete="off" path="holdRemarks" maxLength="500"></form:textarea>
  								</div>
 							</div>
							<div class="col-md-12 center m-t-15">
								<div class="col-md-6 col-xs-6 padd-0">

								
								<button type="submit" id="btn-Tkt-hold" class="btn btn-success btn-hold-width btn-mres submit mm-10">Confirm</button>
								<button type="button" id="btn-Tkt-hold-dummy" class="btn btn-success btn-hold-width btn-mres  mm-10" style="display:none;">Confirm</button>

								</div>
								<div class="col-md-6 col-xs-6 padd-0">
								<button type="button" class="btn btn-primary btn-hold-width btn-mres clear mm-10">Reset</button>
								</div>
							</div> 
  								<!-- <div class="ui primary submit button">Submit</div> -->
 								<!--  <div class="ui error message"></div> -->
						</form>
					</div>
				</div>
			</div>
		</div>
	</form:form>
<!-- Hold Ticket Popup End -->


<!--  Download Popup Start -->
	<div class="modal fade" id="MdlDownloadReport" role="dialog">
    <div class="modal-dialog modal-top modal-popup modal-md">    
      <!-- Modal content-->
      <div class="modal-content" id="popupwidth">
        <div class="modal-header popup-header">
          <button type="button" class="close" data-dismiss="modal">
							<img src="resources/img/close.png">
						</button>
         <!--  <h4 class="modal-title">Modal Header</h4> -->
         <h5 class="modal-title">
							<img  src="resources/img/tkt_Hold_06.png"><span
								class="popup-head">Last Month JMR Details</span>
						</h5>
        </div>
        <div class="modal-body">
        	 <form class="ui form assignValid">
        		<div class="col-md-12">
        			<div class="col-md-4 padd-0">
        				 <label for="date-picker-2" class="fields control-label m-t-5">Last Reading Date</label>
        			</div>
        			<div class="col-md-8">
        				<div class="controls">
                  			<div class="input-group">
                     			<input id="DownloadFromdate" autocomplete="off" name="fromDate" type="text" placeholder="Last Reading Date" class="date-picker form-control" required="required" style="z-index:9999 !important;"/>
                     			<label for="DownloadFromdate" class="input-group-addon btn" style="z-index:9999 !important;"><span style="z-index:9999 !important;" class="glyphicon glyphicon-calendar"></span></label>
                  			</div>
               			</div>
        			</div>
        		</div>
        		<div class="col-md-12  m-t-15">
        			<div class="col-md-4 padd-0">
        				 <label class="fields required m-t-5">Last Reading Value (kWh)</label>
        			</div>
        			<div class="col-md-8">
        				<div class="field">
                  		<input id="DownloadValue" autocomplete="off" name="DownloadValue" placeholder="Last Reading Value" type="text" maxlength="12" value="">
                  	</div>
        			</div>
        		</div>
        		<div class="col-md-12  m-t-15">
        			<div class="col-md-4 padd-0">
        				 <label class="fields required m-t-5">Plant Downtime</label>
        			</div>
        			<div class="col-md-8">
        				<div class="field">
                  		<input id="txtPlantDownTime" autocomplete="off" name="txtPlantDownTime" placeholder="PlantDown Time" type="text" maxlength="50" value="">
                  	</div>
        			</div>
        		</div>
        		<div class="col-md-12 center m-t-15 padd-0">
					<button type="submit" id="btnclick" class="btn btn-success btn-width  mm-10" >Submit</button> &nbsp;&nbsp;&nbsp;
					
					<button type="button" class="btn btn-primary btn-width clear mm-10">Reset</button>
			   </div> 
        			
        	</form>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>
	
<!-- Download Ticket Popup End -->

	<script>
		$('#gotop').gotop({
			customHtml : '<i class="fa fa-angle-up fa-2x"></i>',
			bottom : '2em',
			right : '2em'
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#ScheduledDate").datepicker({
				minDate : 0,
				dateFormat : "dd/mm/yy"
			});
			
			$("#DownloadFromdate").datepicker({ maxDate: '0', 
				dateFormat : "dd/mm/yy",
				minDate: new Date(2018, 01 -1, 01)
			});
			
			$("#AssignScheduledDate").datepicker({
				minDate : 0,
				dateFormat : "dd/mm/yy"
			});

			$(window).fadeThis({
				speed : 500,
			});
		});
	</script>

<script type="text/javascript">
      $(document).ready(function(){
    	  $('#DownloadValue').keypress(function(event) {
    		  
    		    if ((event.which != 46 || $(this).val().indexOf('.') != -1)&&(event.which < 48 || event.which > 57)) {
    		    //alert('hello');
    		   		if((event.which != 46 || $(this).val().indexOf('.') != -1)){
    		      	//alert('Multiple Decimals are not allowed');
    		      }
    		      event.preventDefault();
    		   }
    		   if(this.value.indexOf(".")>-1 && (this.value.split('.')[1].length > 1))		{
    		        //alert('Two numbers only allowed after decimal point');
    		        event.preventDefault();
    		    }
    		});
var validation  = {
		DownloadValue:  {
                    identifier: 'DownloadValue',
                    rules: [
                      {
                        type   : 'empty',
                        prompt : 'Enter the Download Value'
                      },
                      {
                    	  type:'number',
                    	  prompt:'Enter numbers only'
                      },
                      {
                    	  type:'regExp[^[0-9]*\.[0-9]{2}$]',
                    	  prompt: 'Only One dots'
                      }
                    ]
                  },
                  txtPlantDownTime:  {
                      identifier: 'txtPlantDownTime',
                      rules: [
                        {
                          type   : 'empty',
                          prompt : 'Select State'
                        }
                      ]
                    },};
var settings = {
  onFailure:function(){ 
    //alert('fail');
      return false;
    }, 
  onSuccess:function(){    
    //alert('Success');
  /*   $('#MdlDownloadReport').hide();
    $(".modal-backdrop.fade").css("opacity", "0"); */
    $('#MdlDownloadReport').modal('hide');
	  $(".modal-backdrop.fade").css("opacity", "0"); 
   
	AjaxCallForSaveOrUpdate();

	
	/* 	 window.location.href = './ticketviews + $('#hdnticketid').val()'; 
	
	 */
	 
    return false; 
    }};
  
$('.ui.form.assignValid').form(validation,settings);
      });
    </script>
    
   

</body>
<script type="text/javascript">
function getEquipmentAgainstSite() {
    var siteId = $('#ddlSite').val();
   
    $.ajax({
          type : 'GET',
          url : './equipmentsBysites?siteId=' + siteId,
          success : function(msg) {
       	   
       	   var EquipmentList = msg.equipmentlist;
                 
                        for (i = 0; i < EquipmentList.length; i++) {
                               $('#ddlEquipment').append(
                                             "<option value="+EquipmentList[i].equipmentId+">" + EquipmentList[i].customerNaming
                                                          + "</option>");
                        }

          },
          error : function(msg) {
                 console.log(msg);
          }
    });
}

       </script>
</html>


