<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="card card-default bg-default slide-right"
											id="divsiteco2" data-pages="card">
											<div class="h-150">
												<div class="card-header ">
													<div class="card-title pull-left">
														CO<sub>2</sub> Avoided
													</div>
													<div class="card-title pull-right">
														<img src="resources/img/ImageResize_04.png">
													</div>
												</div>
												<div class="card-block">
													<h3>
														<span class="semi-bold"><strong>${siteview.co}</strong></span>
													</h3>
													<p>Today Emission</p>
												</div>
											</div>
											<div class="card-footer">
												<p>Total : ${siteview.totalCo2}</p>
											</div>
										</div>