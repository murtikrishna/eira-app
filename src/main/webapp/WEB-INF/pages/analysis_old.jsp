<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To eAMPM ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />

<link href="resources/css/pace-theme-flash.css" rel="stylesheet" type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet"	type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet"	type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet"	type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet" type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet"	rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet"	type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/jquery.treefilter.css" type="text/css"	rel="stylesheet">
<link href="resources/css/bootstrap.min.3.37.css" type="text/css"	rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min.css" type="text/css"	rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet"	type="text/css">
<link href="resources/css/jquery-ui-1.12.css" rel="stylesheet">
<link href="resources/css/sol.css" rel="stylesheet">
<script src="resources/js/jquerynew.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript">
var jQuery_1_7_2 = $.noConflict(true);
</script> -->
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<!-- <script type="text/javascript">
var jQuery_1_12_4 = $.noConflict(true);
</script> -->
<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script src="resources/js/sol.js" type="text/javascript"></script>
<script src="resources/js/chart/jquery-ui.min.js"></script>
<script src="resources/js/controller/analysis.js"></script>



<!-- <link type="text/css" rel="stylesheet" href="resources/css/jquery.treefilter.css" />
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.19/themes/cupertino/jquery-ui.css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="resources/js/jquery.treefilter.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script> -->


<script src="resources/js/jquery.treefilter.js"></script>
<script src="resources/js/chart/highstock.js"></script>
<script src="resources/js/chart/exporting.js"></script>
<script type="text/javascript"	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>


<style>
.card-header {
	min-height: 30px !important;
}
.amChartsLegend {
	color: #6a6c6f !important;
}
.dropdown-menu-right {
	left: -100px !important;
}
.btn-day-width {
	width:130px;
	padding: 0px 8px !important;
}
.btn-primary.active, .btn-primary:active, .open>.dropdown-toggle.btn-primary {
    color: #fff;
    background-color: #286090  !important;
    border-color: #286090  !important;
}
.checkbox {
	margin:0px !important;
}
/* .ddfCustomer {
	min-height:25px !important;
	height:25px !important;
} */
.btn-range-selector {
	font-size:12px;
	    background: #64a27b;
    border: 1px solid #23a218;
}
.btn-range-selector:hover {
	 background: #286090  !important;
    border: 1px solid #286090  !important;
} 
#CurrentVal {
	    height: 25px;
	    width: 44%;
}
.ui.scrolling.dropdown .menu, .ui.dropdown .scrolling.menu {
    max-height: 15.42857143rem;
}
.modal .modal-lg {
        max-width: 1850px !important;
    margin-left: 70px !important;
    width: 1350px !important;
    margin-top:200px;
}
.close-icon {
	   font-size: 15px;
    cursor: pointer;
    background: red;
    color: #FFF;
    height: 17px;
    width: 17px;
    text-align: center;
    float: right;
    /* padding-top: 2px; */
    /* position: absolute; */
    margin-right: 5px;
    /* border-radius: 50%; */
}
.ui.checkbox .box, .ui.checkbox label {
    cursor: auto;
    position: relative;
    display: block;
    padding-left: 1.85714em;
    outline: none;
    font-size: 12px;
}
/* .ui.selection.dropdown {
    cursor: pointer;
    word-wrap: break-word;
    line-height: 0px;
    white-space: normal;
    outline: 0;
    -webkit-transform: rotateZ(0deg);
    transform: rotateZ(0deg);
    min-width: 14em;
    min-height: 10px;
    background: #FFFFFF;
    display: inline-block;
    padding: 0.78571429em 2.1em 0.78571429em 1em;
    color: #000;
    box-shadow: none;
    border: 1px solid rgba(34, 36, 38, 0.15);
    border-radius: 0.28571429rem;
    -webkit-transition: box-shadow 0.1s ease, width 0.1s ease;
    transition: box-shadow 0.1s ease, width 0.1s ease;
} */
/* .ui.selection.dropdown .menu > .item {
    border-top: 1px solid #FAFAFA;
    padding: 0.471429rem 1.14285714rem !important;
    white-space: normal;
    word-wrap: normal;
}
.ui.selection.dropdown > .search.icon, .ui.selection.dropdown > .delete.icon, .ui.selection.dropdown > .dropdown.icon {
    cursor: pointer;
    position: absolute;
    width: auto;
    height: auto;
    line-height: 5px;
    top: 0.78571429em;
    right: 1em;
    z-index: 3;
    margin: -0.78571429em;
    padding: 0.78571429em;
    opacity: 0.8;
    -webkit-transition: opacity 0.1s ease;
    transition: opacity 0.1s ease;
} */
.m-t-3 {
	margin-top:3px;
}
.filterbutton {
	width:90px;
}
.none {
	display:none;
}
.ui.dropdown .menu > .item {
    position: relative;
    cursor: pointer;
    display: block;
    border: none;
    height: auto;
    text-align: left;
    border-top: none;
    line-height: 1em;
    color: rgba(0, 0, 0, 0.87);
    /* padding: 0.78571429rem 1.14285714rem !important; */
    font-size: 1rem;
    text-transform: none;
    font-weight: normal;
    box-shadow: none;
    -webkit-touch-callout: none;
    font-size: 11px !important;
}
/* .ui.scrolling.dropdown .menu, .ui.dropdown .scrolling.menu {
    max-height: 14.571429rem;
} */
.ui.dropdown .menu > .item {
    position: relative;
    cursor: pointer;
    display: block;
    border: none;
    height: auto;
    text-align: left;
    border-top: none;
    line-height: 1em;
    color: rgba(0, 0, 0, 0.87);
    padding: 0.10rem 1.142857rem !important;
    font-size: 12px !important;
    text-transform: none;
    font-weight: normal;
    box-shadow: none;
    -webkit-touch-callout: none;
}
.dropdata {
	/* padding-top:15px !important; */
}
.info-window {
	width:100px;
	float:left;
	margin:4px 2px;
	cursor:pointer;
	    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}
#fromDate {
	margin-left: 10px;
    width: 70%;
    float: right;
    border-radius: 2px;
    height: 22px !important;
    min-height: 25px !important;
    text-align: center;
}
#toDate {
	margin-left: 10px;
	width: 70%;
	float: right;
	border-radius: 2px;
	height: 22px !important;
    min-height: 25px !important;
    text-align: center;
}
.btn-lhs {
	background: #398439;	    
	color:#FFF;
    border: 1px solid #398439;
    height: 20px;
}
.btn-rhs {
	background: #398439;
	color:#FFF;
    border: 1px solid #398439;
    height: 20px;
}
.btnSelected {
	background: #398439;
	border:1px solid #398439;
    color:#FFF;
}
#fromDate,#toDate {
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important;
	font-size:12px;
}
.searchbx i {
	display: none;
}
.closeAll {
	cursor:pointer;
	/* float:right; */
}
#load {
	/* width: 72%; */
	/* width: 66%;
	height: 285px;
	margin-top: 5%;
	position: fixed; */
	/* z-index: 9999; */
	/* opacity: 0.5; */
	/* background:url("resources/img/image_01.gif") no-repeat center; */
	/* background:url("resources/img/loader_green.gif") no-repeat center;  */
	/* background:url("resources/img/loader_graph.gif") no-repeat center; */
	/* background: url("resources/img/loading_green.gif") no-repeat center; */
}
.ui.form textarea:not([rows]){
	height:6em!important;
	min-height:4em!important;
	max-height:24em;
}
/* #ndf {
	/* width: 72%; 
	width: 66%;
	height: 120px;
	margin-top: 11%;
	position: fixed;
	z-index: 9999;
	opacity: 0.5;
	z-index: 0;
	/* background: url("resources/img/ndf-03.png") no-repeat center; 
} */

#ndf {
    /* width: 72%; */
    width: 94%;
    height: 120px;
    margin-top: 17%;
    position: fixed;
    z-index: 9999;
    /* opacity: 0.5; */
    font-weight: 600;
    text-align: center;
    z-index: 0;
    color: #b5aaaa;
    /* background: url(resources/img/ndf-03.png) no-repeat center; */
}

.modal-lg {
	width: 100%;
}

.reportType {
	background: rgba(231, 236, 234, 0.87);
	/* background:rgba(187, 202, 196, 0.87); */
	/* background: -webkit-linear-gradient(45deg, rgba(229, 234, 232, 0.87) 25%, rgb(172, 185, 175) 100%); */
	/* background: -webkit-linear-gradient(45deg, rgba(80, 140, 112, 0.87) 0%, rgb(164, 226, 162) 100%); */
	/* background: -webkit-linear-gradient(45deg, rgba(36, 95, 68, 0.87) 0%, rgb(98, 165, 96) 100%); */
	height: 445px;
	/*  margin: 15px; */
	margin-top: 0px;
	padding: 10px;9-
}

.filter-top {
	width: 1350px;
	margin: 30px;
	margin-top: 245px;
	margin-left: 85px;
}

.primary {
	background: url(../images/info_icon.png) 10px 11px #0079a9;
	background: url(../images/info_icon.png) 10px 11px, -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #009ddc), color-stop(100%, #0079a9));
  	background: url(../images/info_icon.png) 10px 11px, -moz-linear-gradient(top, #009ddc, #0079a9);
  	background: url(../images/info_icon.png) 10px 11px, -ms-linear-gradient(top, #009ddc, #0079a9);
	background: url(../images/info_icon.png) 10px 11px, -o-linear-gradient(top, #009ddc, #0079a9);
  	background: url(../images/info_icon.png) 10px 11px, linear-gradient(top, #009ddc, #0079a9);	
	background-repeat: no-repeat;
}
.SiteFilterList,.EquipmentFilterList,.ParameterFilterList {
	    margin-bottom: 0px;
	/* border:1px solid #CCC; */	
	/* overflow-y:scroll; */
	
}
.filterbox {
    height: 420px;
    position: absolute;
    z-index: 1;
    border: 1px solid #868686;
    background: rgba(218, 218, 218, 0.87);
    /* background: -webkit-linear-gradient(45deg, rgba(167, 190, 197,0.87) 0%, rgba(125, 137, 173, 0.87) 100%); */
    width: 97.7%;
    margin-top: -38.5%; 
    margin-top:-510px;
}

/* .active{
    background:green;
} */
button.btn:active{
    background:green;
}
.modal-footer {
	border: 0px;
}
.equ-tree {
	background: #FCF9F9;
	margin-top: 10px;
	border: 2px solid #CCC;
	height: 405px;
	overflow-y: scroll;
}
.inverterData {
	/* height: 200px; */
	height: 500px;
}
/* .info-close-window {
	display:none;
} */
.chart-type {
	background: #F0F0F0;
	border: 1px solid #ccc;
	border-radius: 5px;
	padding: 10px;
}
.ui.fluid.dropdown {
    display: block;
    width: 100%;
    min-width: 16em;
}
.equ-tree ul {
	margin-left: -30px;
}
.stylish-input-group .form-control { 
	border-right: 0;
	box-shadow: 0 0 0;
	border-color: #ccc;
}

#treeview li {
	list-style-type: none;
}

.stylish-input-group button {
	border: 0;
	background: transparent;
}
.h-scroll {
	background-color: #fcfdfd;
	height: 260px;
	overflow-y: scroll;
}
.alert {
    padding: 3px;
    margin-bottom: 0px;
    border: 1px solid transparent;
    border-radius: 4px;
}
.Sele {
	/* height: 90px; */
    min-height: 30px;
    overflow-x: hidden;
    /* border: 2px solid #CCC; */
    max-height: 94px;
}
#overlay {
	position: fixed;
	display: none;
	width: 100%;
	height: 100%;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	background-color: rgba(0, 0, 0, 0.5);
	z-index: 2;
	cursor: pointer;
}
/* .graphName {
	margin-top: 10px !important;
	font-weight: bold;
	text-align: center;
	font-size: 16px !important;
	color: #3a6f48;
	/* margin-bottom: 20px !important; */
	/* padding-bottom: 15px; 
	background: #FFF;
	/* padding-top: 10px; */
} */
.highcharts-vkhqvz3-0 {
	width: 875px;
}
.rtype {
	font-size: 12px !important;
	font-weight: bold;
	/* background: rgba(231, 236, 234, 0.87); */
	background: rgba(187, 202, 196, 0.87);
	color: #3a6f48;
	/* width: 89.9%; */
	margin-top: 14px !important;
	height: 40px;
	padding-top: 6px !important;
	text-align: center;
}
.rtype p{
	    padding-top: 7px;
}
.closeddd {
	color:#000;
	    color: #000;
    font-size: 17px;
}
.selectedValue {
	background:#7d7d7d;
	font-size: 13px;
	border-radius: 10px;
padding: 3px 20px;
font-size:13px;
color:#FFF;
cursor:pointer;
margin-top:5px;
width:150px;
}
.selectedValue:hover {
	background:#c2c2c2;
	color:#FFF;
}
.btn-padd {
	    padding: 2px 3px !important;
	    border-radius:0px !important;
	        width: 60px;
    height: 25px;
}
.oveallSearch .text {
    /* color: #eae9e9 !important; */
    color: #a49c9c !important;
}
.form-control {
	min-height: 28px !important;
	height: 28px;
}
#text {
	position: absolute;
	top: 50%;
	left: 50%;
	font-size: 50px;
	color: white;
	transform: translate(-50%, -50%);
	-ms-transform: translate(-50%, -50%);
}
</style>

    


<script type="text/javascript">
$(document).ready(function() {
/* alert($(window).height());
alert($(window).width()); */
	
	/* $('#btnView').attr("disabled",true); */
	/* $('#btnClrFilter').attr("disabled",true);
	 */
	/*  $('#ddlParameter').searchableOptionList();
	
		$('.chkboxSites').hide();
	  	$('.chkboxEquipment').hide();
	  	$('.chkboxParameter').hide();
	   */
	  
	  /* $("#ddlGraphType").change(function() {
		  var value = $('#ddlGraphType option:selected').val();
		  
		  if (value == 0) {
			  $('.chkboxSites').show();
			  $('.chkboxParameter').hide();
			  $('.chkboxEquipment').hide(); 
			  
			  if ($('.SiteFilterList').children().length == 0)
			  {$('#btnView').attr("disabled",true);}
			  else
			  {$('#btnView').attr("disabled",false);}
				
		  }
		  else  if(value == 1) {
		  	  $('.chkboxSites').show();
			  $('.chkboxEquipment').show();
			  $('.chkboxParameter').hide(); 
			  
			  if ($('.EquipmentFilterList').children().length == 0)
			  {$('#btnView').attr("disabled",true);}
			  else
			  {$('#btnView').attr("disabled",false);}
			  
		  }
		  else  if(value == 2) {
			  $('.chkboxSites').show();
			  $('.chkboxEquipment').show();
			  $('.chkboxParameter').show(); 
			  
			  if ($('.ParameterFilterList').children().length == 0)
			  {$('#btnView').attr("disabled",true);}
			  else
			  {$('#btnView').attr("disabled",false);}
		  }
		  
	  }); */

	$('.chkboxFilter').hide();
	$('.Sele').hide();
	$('.closeAll').hide();
	$('#btnApply').hide();
	
	var tree = new treefilter($("#my-tree"), {
		searcher : $("input#my-search"),
		multiselect : false
	});
	
	$('.closeAll').click(function(){
		$('.Sele').hide();
		$('.closeAll').hide();
		$('#btnApply').hide();
	})
	
	
	$("#my_styles .btn").click(function() {
		jQuery("#my_styles .btn").removeClass('active');
		jQuery(this).toggleClass('active');
	});
	
	



		
		var tree1 = $("#test-select").treeMultiselect({ enableSelectAll: true, sortable: true });

		/** Default - Current Day ***/
		Daily();


		/** Current Day ***/
		$('#daily').click(function() {
			Daily();
			$('#hdnSelectedDataRange').val('daily');
			ViewReport();
		})

		/** Current Week **/
		$('#weekly').click(function() {
			Weekly();
			$('#hdnSelectedDataRange').val('weekly');
			ViewReport();
		})

		/** Current Month ***/
		$('#monthly').click(function() {
			Monthly();
			$('#hdnSelectedDataRange').val('monthly');
			ViewReport();
		})

		/** Current Year ***/
		$('#yearly').click(function() {
			Yearly();
			$('#hdnSelectedDataRange').val('yearly');
			ViewReport();
		})



		/** Custom ***/
		$('#custom').click(function() {
			Custom();
			$('#hdnSelectedDataRange').val('custom');

		})


		$('#CurrentVal').click(function() {
			ViewReport();
		})
		
		$('#btnApply').click(function() {
			ViewReport();
		})


		$('#fromDate').on('focusout', function() {
			try {
				var fromdate = $.datepicker.parseDate('mm/dd/yy', $('#fromDate').val());
				if ($('#fromDate').val() == '') {
					$('#fromDate').val($('#toDate').val());
				}
			} catch (err) {
				$('#fromDate').val($('#toDate').val());
			}
			$('#custom').click();

		})



		$('#toDate').on('focusout', function() {
			try {
				var todate = $.datepicker.parseDate('mm/dd/yy', $('#toDate').val());
				if ($('#toDate').val() == '') {
					$('#toDate').val($('#fromDate').val());
				}

			} catch (err) {
				$('#toDate').val($('#fromDate').val());
			}
			$('#custom').click();
		})

		$("#fromDate").datepicker({
			dateFormat : 'dd/mm/yy',
			maxDate : new Date(),
			maxDate : "0",
			onSelect : function(selected) {
				$("#toDate").datepicker("option", "minDate", selected);
				//$('#response').text('day clicked '+cont);
				$('#custom').click();
			}
		});


		$("#toDate").datepicker({
			dateFormat : 'dd/mm/yy',
			maxDate : new Date(),
			maxDate : "0",
			onSelect : function(selected) {
				$("#fromDate").datepicker("option", "maxDate", selected);
				// $('#response').text('day clicked '+cont); 
				$('#custom').click();
			}
		});



		if ($(window).width() < 767) {
			$('.card').removeClass("slide-left");
			$('.card').removeClass("slide-right");
			$('.card').removeClass("slide-top");
			$('.card').removeClass("slide-bottom");
		}


		var tree = new treefilter($("#my-tree"), {
			searcher : $("input#my-search"),
			multiselect : false
		});
		
		
		$(window).fadeThis({
			speed : 500,
		});

		$('.btn-close').click(function() {
			$('#filter').hide();
		})

		$('#ddlType').change(function() {
			$('.category').dropdown('clear')
		});

		$('#ddlType').change(function(){              
            $('.category').dropdown('clear')
           });


		$('#btnChoose').click(function() {
			$('.chkboxFilter').show();
		})


		$('#btnfliterShow').click(function() {
			$('.filterbox').show();
		});

		$('.close-icon').click(function() {
			$('.filterbox').hide();
		})


		$('#dateTime').hide();
		
		
		$('.close').click(function() {
			$('.clear').click();
		});
		
		
		
		$('.clear').click(function() {
			$('.search').val("");
		});

		$('body').click(function() {
			$('#builder').removeClass('open');
		});


		$("#searchSelect").change(function() {
			var value = $('#searchSelect option:selected').val();
			var uid = $('#hdneampmuserid').val();
			redirectbysearch(value, uid);
		});

		$(".bars").click(function() {
			$(".sidebar-menu").invisible();
		})

		
		
		
		
		
	
	
	$('.SiteFilterList').on('click', '.sitefilterdivclose').on('click', '.sitefilterclose', function() {
		
		debugger;
		
	    // Do something on an existent or future .dynamicElement
	    var select = $(this);
		var idSelect = select.attr('id');	
		
		idSelect = idSelect.replace(/lbl/,'').replace(/filterclose/,'');	
		
		$('#' + idSelect).prop('checked', false);  
		SiteCheckedChange(idSelect);
		
		if ($('.SiteFilterList').children().length == 1 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 0) {
			$('.Sele').hide();
			$('.closeAll').hide();
			$('#btnApply').hide();
			
			if ($('#ddlGraphType').val() == 0) {
				$('#btnView').attr("disabled",true);
			}
			
		}
		else {
			$('#btnApply').show();
		}
		
		
		if ($('#ddlGraphType').val() == 1) {
			if ($('.EquipmentFilterList').children().length == 0)
			{
			$('#btnView').attr("disabled",true);
			$('#btnApply').hide();
			$('#ddfParameter').empty();	
			$('.ParameterFilterList').empty();
			}			
		}
		else if ($('#ddlGraphType').val() == 2) {						
							
			if ($('.EquipmentFilterList').children().length == 0)
			{
				$('#ddfParameter').empty();	
				$('.ParameterFilterList').empty();
				$('#btnView').attr("disabled",true);
				$('#btnApply').hide();
			}
			else
			{
				AjaxCallForStandardParameters();
				
				if ($('.ParameterFilterList').children().length == 0)
				{
					$('#ddfParameter').empty();	
					$('.ParameterFilterList').empty();
					$('#btnView').attr("disabled",true);
					$('#btnApply').hide();
				}
				else
				{
					$('#btnView').attr("disabled",false);
					$('#btnApply').show();
				}
			}
			
		}
		
		
		
		
	});

		
		
		
		$('.ParameterFilterList').on('click', '.parameterfilterdivclose').on('click', '.parameterfilterclose', function() {
			
			debugger;
		    // Do something on an existent or future .dynamicElement
		    var select = $(this);
			var idSelect = select.attr('id');	
			
			idSelect = idSelect.replace(/lbl/,'').replace(/filterparamclose/,'');	
			
			$('#' + idSelect).prop('checked', false);  
			SiteCheckedChange(idSelect);
			
			if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 1) {
				$('.Sele').hide();
				$('.closeAll').hide();
				$('#btnApply').hide();
				
				if ($('#ddlGraphType').val() == 2) {
					$('#btnView').attr("disabled",true);
				}
			}
			else {
				$('#btnApply').show();
			}
			
			
			
			
			if ($('#ddlGraphType').val() == 1) {
				if ($('.EquipmentFilterList').children().length == 0)
				{
				$('#btnView').attr("disabled",true);
				$('#btnApply').hide();
				$('#ddfParameter').empty();	
				$('.ParameterFilterList').empty();
				}			
			}
			else if ($('#ddlGraphType').val() == 2) {						
								
				if ($('.EquipmentFilterList').children().length == 0)
				{
					$('#ddfParameter').empty();	
					$('.ParameterFilterList').empty();
					$('#btnView').attr("disabled",true);
					$('#btnApply').hide();
				}
				else
				{
					
					if ($('.ParameterFilterList').children().length == 1)
					{
						$('#ddfParameter').empty();	
						$('.ParameterFilterList').empty();
						$('#btnView').attr("disabled",true);
						$('#btnApply').hide();
					}
					else
					{
						$('#btnView').attr("disabled",false);
						$('#btnApply').show();
					}
				}
				
			}
			
			
			
			
			
		});
		
		
		
		
		
 $('.EquipmentFilterList').on('click', '.equipmentfilterdivclose').on('click', '.equipmentfilterclose', function() {
		
		debugger;
	    // Do something on an existent or future .dynamicElement
	    var select = $(this);
		var idSelect = select.attr('id');	
		
		idSelect = idSelect.replace(/lbl/,'').replace(/filterequclose/,'');	
		
		$('#' + idSelect).prop('checked', false); 		
		$('#chkInverters').prop('checked', false);
		
		if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 1 && $('.ParameterFilterList').children().length == 0) {
			$('.Sele').hide();
			$('.closeAll').hide();
			$('#btnApply').hide();
			
			
		}
		else {
			$('#btnApply').show();
		}
		
		
		if ($('#ddlGraphType').val() == 1) {
			if($('.EquipmentFilterList').children().length <= 1)
			{
				$('#btnView').attr("disabled",true);
				$('#btnApply').hide();
			}
			
		}
		
	});
	
		
		
		 $('#chkSites').on('change', function() {
			//var CustomerId = $('#ddfCustomer').val();                             
			//var $inputs = $('#ddfSite').find('input').filter('[data-customer="' + CustomerId + '"]');    

			var $inputs = $('#ddfSite').find('input');

			debugger;
			if (this.checked) {
				for (var i = 0; i < $inputs.length; i++) {
					var chk = $inputs[i].id;
					var chktext = $('#lbl' + chk).text();
					
					
					chktext = $.trim(chktext.replace(/[\t\n]+/g, ' '));

					
					$('#' + chk).prop('checked', true); // Checks it      

					var filterid = "filter" + chk;
					var filterclodeid = "filterclose" + chk;

					if (window[filterid]) {
						//no need of adding new node
					} else {
						var newNode = '<div id="' + filterid + '" class="alert alert-success info-window sitefilterdivclose" role="alert" title="' + chktext + '"><button id="' + filterclodeid + '"  class="close info-close-window sitefilterclose" data-dismiss="alert"></button>' + chktext + '</div>';
						$('.SiteFilterList').append(newNode);
						$('.Sele').show();
						$('.closeAll').show();
					}


					SiteCheckedChange(chk);
				}
				
				if ($('#ddlGraphType').val() == 0) {
					$('#btnView').attr("disabled",false);
				}
				else if ($('#ddlGraphType').val() == 1) {
					if ($('.EquipmentFilterList').children().length == 0)
					{$('#btnView').attr("disabled",true);}
					else
					{$('#btnView').attr("disabled",false);}
				}
				
				
			} else {
				for (var i = 0; i < $inputs.length; i++) {
					var chk = $inputs[i].id;
					$('#' + chk).prop('checked', false); // Unchecks it                        
					
					$('.SiteFilterList').empty();
					$('.EquipmentFilterList').empty();
					$('.ParameterFilterList').empty();
				
					$('#chkInverters').prop('checked', false);
					
					
					
					if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 0) {
						$('.Sele').hide();
						$('.closeAll').hide();
					}
					SiteCheckedChange(chk);
				}
				
				if ($('#ddlGraphType').val() == 0) {
					$('#btnView').attr("disabled",true);
				}
				else if ($('#ddlGraphType').val() == 1) {
					if ($('.EquipmentFilterList').children().length == 0)
					{$('#btnView').attr("disabled",true);}
					else
					{$('#btnView').attr("disabled",false);}
				}
				else if ($('#ddlGraphType').val() == 2) {						
					
					
					if ($('.EquipmentFilterList').children().length == 0)
					{
						$('#ddfParameter').empty();	
						$('.ParameterFilterList').empty();
						$('#btnView').attr("disabled",true);
					}
					else
					{
						AjaxCallForStandardParameters();
						
						if ($('.ParameterFilterList').children().length == 0)
						{
							$('#ddfParameter').empty();	
							$('.ParameterFilterList').empty();
							$('#btnView').attr("disabled",true);
						}
						else
						{
							$('#btnView').attr("disabled",false);
						}
					}
					
				}
			}
		});
          
         
			
			 
	    $('.chksite').on('change', function() {
			var select = $(this);
			var idSite = select.attr('id');

			debugger;

			var chktext = $('#lbl' + idSite).text();
			chktext = $.trim(chktext.replace(/[\t\n]+/g, ' '));


			var filterid = "filter" + idSite;
			var filterclodeid = "filterclose" + idSite;
			
			
			if ($('#' + idSite).prop('checked')) {
				if (window[filterid]) {

				} else {
					var newNode = '<div id="' + filterid + '" class="alert alert-success info-window sitefilterdivclose " role="alert" title="' + chktext + '"><button id="' + filterclodeid + '"  class="close info-close-window sitefilterclose" data-dismiss="alert"></button>' + chktext + '</div>';
					$('.SiteFilterList').append(newNode);
					$('.Sele').show();
					$('.closeAll').show();
				}
			} else {
				
				$('#' + filterid).remove();
				$('#chkInverters').prop('checked', false);
				if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 0) {
					$('.Sele').hide();
					$('.closeAll').hide();
				}
				
				
			}


			
				
			
			SiteCheckedChange(idSite);
			
			if ($('#ddlGraphType').val() == 0) {
				if ($('.SiteFilterList').children().length == 0)
				{$('#btnView').attr("disabled",true);}
				else
				{$('#btnView').attr("disabled",false);}
			}
			else if ($('#ddlGraphType').val() == 1) {
				if ($('.EquipmentFilterList').children().length == 0)
				{$('#btnView').attr("disabled",true);}
				else
				{$('#btnView').attr("disabled",false);}
			}
			else if ($('#ddlGraphType').val() == 2) {						
								
				if ($('.EquipmentFilterList').children().length == 0)
				{
					$('#ddfParameter').empty();	
					$('.ParameterFilterList').empty();
					$('#btnView').attr("disabled",true);
				}
				else
				{
					AjaxCallForStandardParameters();
					
					if ($('.ParameterFilterList').children().length == 0)
					{
						$('#ddfParameter').empty();	
						$('.ParameterFilterList').empty();
						$('#btnView').attr("disabled",true);
					}
					else
					{
						$('#btnView').attr("disabled",false);
					}
				}
				
			}
			
			
		});
	          
	    
	          	          

		$('#chkInverters').on('change', function() {
			debugger;

			var $inputsInv = $('#ddfInverter').find('input');
			if (this.checked) {
				for (var i = 0; i < $inputsInv.length; i++) {
					var chk = $inputsInv[i].id;
					
					
					var chktext = $('#lbl' + chk).text();
					chktext = $.trim(chktext.replace(/[\t\n]+/g, ' '));

					if($('#div' + chk).css('display') == 'block')
					{
						$('#' + chk).prop('checked', true); // Checks it           				
						
						var filterid = "filterequ" + chk;
						var filterclodeid = "filterequclose" + chk;
						
						
						if (window[filterid]) {
							//no need of adding new node
						} else {
							var newNode = '<div id="' + filterid + '" class="alert alert-danger info-window equipmentfilterdivclose" role="alert" title="' + chktext + '"><button id="' + filterclodeid + '" class="close info-close-window equipmentfilterclose" data-dismiss="alert"></button>' + chktext + '</div>';
							$('.EquipmentFilterList').append(newNode);
							$('.Sele').show();
							$('.closeAll').show();
						}

						
						
						SiteCheckedChange(chk);
						
						if ($('#ddlGraphType').val() == 1) {
							$('#btnView').attr("disabled",false);
						}
						
						
						
					}
					
					
				}
				
				
				
			} 
			else {
				for (var i = 0; i < $inputsInv.length; i++) {
					var chk = $inputsInv[i].id;
						$('#' + chk).prop('checked', false); // Unchecks it						
					}
				
				
				$('.EquipmentFilterList').empty();	
				$('.ParameterFilterList').empty();	
				
				if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 0) {
					$('.Sele').hide();
					$('.closeAll').hide();
				}
				
				if ($('#ddlGraphType').val() == 1) {
					$('#btnView').attr("disabled",true);
				}

			}
			
			
			
			
			
			AjaxCallForStandardParameters();			
			if ($('#ddlGraphType').val() == 1) {
				if ($('.EquipmentFilterList').children().length == 0)
				{$('#btnView').attr("disabled",true);}
				else
				{$('#btnView').attr("disabled",false);}
			}
			else if ($('#ddlGraphType').val() == 2) {
				if ($('.ParameterFilterList').children().length == 0)
				{$('#btnView').attr("disabled",true);}
				else
				{$('#btnView').attr("disabled",false);}
			}
			
		});
	          
	          
	          
				
		
		 
	    $('.chkinverter').on('change', function() {
	    	debugger;
			var select = $(this);
			var idEquipment = select.attr('id');

			debugger;

			var chktext = $('#lbl' + idEquipment).text();
			chktext = $.trim(chktext.replace(/[\t\n]+/g, ' '));

			var filterid = "filterequ" + idEquipment;
			var filterclodeid = "filterequclose" + idEquipment;
			
			
			
			if ($('#' + idEquipment).prop('checked')) {
				
				if (window[filterid]) {
					//no need of adding new node
				} else {
					var newNode = '<div id="' + filterid + '" class="alert alert-danger info-window equipmentfilterdivclose" role="alert" title="' + chktext + '"><button id="' + filterclodeid + '" class="close info-close-window equipmentfilterclose" data-dismiss="alert"></button>' + chktext + '</div>';
					$('.EquipmentFilterList').append(newNode);
					$('.Sele').show();
					
					$('.closeAll').show();
				}
				
				
			} else {
				$('#' + filterid).remove();
				
				if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 0) {
					$('.Sele').hide();
					$('.closeAll').hide();
					
				}

			}

			

			

			AjaxCallForStandardParameters();
			
			
			if ($('#ddlGraphType').val() == 1) {
				if ($('.EquipmentFilterList').children().length == 0)
				{$('#btnView').attr("disabled",true);}
				else
				{$('#btnView').attr("disabled",false);}
			}
			else if ($('#ddlGraphType').val() == 2) {
				if ($('.ParameterFilterList').children().length == 0)
				{$('#btnView').attr("disabled",true);}
				else
				{$('#btnView').attr("disabled",false);}
			}
			
			
		});
	          
	         
	          	   
	    
	    
	   /*  $('.divchkparameter11').on('click', function() {

	    		
	    	var select = $(this);
	    	var idParameter = select.attr('id');
	    	alert(idParameter);
	    	var idParameterCheckBox = $.trim(chktext.replace(/div/, ''));
	    		    	
	    	if ($('#' + idParameterCheckBox).prop('checked')) {		
	    		$('#' + idParameterCheckBox).prop('checked', false); // Checks it	
	    		
	    	} else {
	    		$('#' + idParameterCheckBox).prop('checked', true); // Unchecks it
	    	}

	    	
	    });
 */
	    
	    
        

$('#chkParameters').on('change', function() {
	
	debugger;

	var $inputs = $('#ddfParameter').find('input');
	if (this.checked) {
		for (var i = 0; i < $inputs.length; i++) {
			var chk = $inputs[i].id;
			
			
			var chktext = $('#lbl' + chk).text();
			
			
			
			chktext = $.trim(chktext.replace(/[\t\n]+/g, ' '));

			if($('#div' + chk).css('display') == 'block')
			{
				$('#' + chk).prop('checked', true); // Checks it           				
				
				var filterid = "filterparam" + chk;
				var filtercloseid = "filterparamclose" + chk;
				
				
				if (window[filterid]) {
					//no need of adding new node
				} else {
					var newNode = '<div id="' + filterid + '" class="alert alert-info info-window equipmentfilterdivclose" role="alert" title="' + chktext + '"><button id="' + filtercloseid + '" class="close info-close-window equipmentfilterclose" data-dismiss="alert"></button>' + chktext + '</div>';
					$('.ParameterFilterList').append(newNode);
					$('.Sele').show();
					$('.closeAll').show();
				}

				if ($('#ddlGraphType').val() == 2) {
					$('#btnView').attr("disabled",false);
				}
				
			}
			
			
		}
	} 
	else {
		for (var i = 0; i < $inputs.length; i++) {
			var chk = $inputs[i].id;
				$('#' + chk).prop('checked', false); // Unchecks it
				
				
			}
		
		
		$('.ParameterFilterList').empty();			
		
		if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 0) {
			$('.Sele').hide();
			$('.closeAll').hide();
		}
		
		if ($('#ddlGraphType').val() == 2) {
			$('#btnView').attr("disabled",true);
		}

	}

});
    
    
    
    

       
     	
/*$(document).on('click', '.chkparameter', function(){
	alert("inside")
});*/

/*         $('chkparamevalue').on('click', function(value){
        	alert(value)
        }) */
        
     	
/* $(document).on('click', 'chkparamevalue', function(value){
	alert("-1-"+value)
});

$("chkparamevalue").live('click', function(value){
	alert("-2-"+value)
	
}); */
        
		 
	    $('.chkparameter').on('change', function() {
	    	
	    	
	    	debugger;
			var select = $(this);
			var idParameter = select.attr('id');


			var chktext = $('#lbl' + idParameter).text();
			chktext = $.trim(chktext.replace(/[\t\n]+/g, ' '));

			var filterid = "filterparam" + idParameter;
			var filterclodeid = "filterparamclose" + idParameter;
			
			
			
			if ($('#' + idParameter).prop('checked')) {
				
				if (window[filterid]) {
					//no need of adding new node
				} else {
					var newNode = '<div id="' + filterid + '" class="alert alert-info info-window parameterfilterdivclose" role="alert" title="' + chktext + '"><button id="' + filterclodeid + '" class="close info-close-window parameterfilterclose" data-dismiss="alert"></button>' + chktext + '</div>';
					$('.ParameterFilterList').append(newNode);
					$('.Sele').show();
					$('.closeAll').show();
					
				}
				
				
			} else {
				$('#' + filterid).remove();
				
				if ($('.SiteFilterList').children().length == 0 && $('.EquipmentFilterList').children().length == 0 && $('.ParameterFilterList').children().length == 0) {
					$('.Sele').hide();
					$('.closeAll').hide();
				}

			}
			
			
			if ($('#ddlGraphType').val() == 2) {
				if ($('.ParameterFilterList').children().length == 0)
				{
					
				$('#btnView').attr("disabled",true);
				}
				else
				{
					
				$('#btnView').attr("disabled",false);
				}
			}



		});
	          
	         
	    

	    
				
        var LastCheckedParam = '';
          $('#btnClrFilter').on('click', function() {

			
			var $inputs = $('#ddfSite').find('input');
			var $inputsInverter = $('#ddfInverter').find('input');
			var $inputsParameter = $('#ddfParameter').find('input').filter(':checked');


			for (var i = 0; i < $inputs.length; i++) {
				var chk = $inputs[i].id;
				$('#' + chk).prop('checked', false); // Unchecks it
				SiteCheckedChange(chk);
			}
			for (var i = 0; i < $inputsInverter.length; i++) {
				var chk = $inputsInverter[i].id;
				$('#' + chk).prop('checked', false);
			}
			for (var i = 0; i < $inputsParameter.length; i++) {
				var chk = $inputsParameter[i].id;
				$('#' + chk).prop('checked', false);
			}
			LastCheckedParam = '';
			$('#ddfParameter').empty();
			
			
			$('#chkSites').prop('checked', false);
			$('#chkInverters').prop('checked', false);
			$('#chkParameters').prop('checked', false);
			
			$('.SiteFilterList').empty();
			$('.EquipmentFilterList').empty();
			$('.ParameterFilterList').empty();
			
			$('.Sele').hide();
			$('.closeAll').hide();
		});
          
          
          
          
          $('#btnClearAll').on('click', function() {        	  
        	  $('#btnClrFilter').click();
          });
         
          
         
          
       
          $('#btnView').on('click', function() {     
          	debugger;
             ViewReport();
             //$('.Sele').show();
             $('.filterbox').hide();
             $('.closeAll').show();
             //$('#filter').hide();
             //$('#btnfliterHide').click();
                                          
          });
          
          
          $('.GraphTypeClick').on('click', function() {
             
          	debugger;
             var HiddenValue=  document.getElementById($(this).attr('id')).innerHTML;                          
             $('#hdnGraphType').val(HiddenValue);  
             document.getElementById('lblReportName').innerHTML = HiddenValue;
             
             //var CustomerId = $('#ddfCustomer').val();             
             var $inputsSite = $('#ddfSite').find('input').filter(':checked');
             var $inputsInverter = $('#ddfInverter').find('input').filter(':checked');               
             var $inputsAllInverter = $('#ddfInverter').find('input');


             var varSiteList = '';
             for(var i=0;i< $inputsSite.length; i++) {
					var chk =  $inputsSite[i].id;
                  var DataVal= $('#' + chk).attr('data-value');
                  if(i==0) {
                  	varSiteList = DataVal; 
                  }
                  else {
                  	varSiteList = varSiteList + ',' + DataVal;
                  }
               }
             
             
             var bolInvChecked = false;
             var varInverterList = '';
             for(var i=0;i< $inputsInverter.length; i++) {
					var chk =  $inputsInverter[i].id;
                  var DataVal= $('#' + chk).attr('data-value');
                 if(i==0) {
                   varInverterList = DataVal; 
                 }
                 else {
                		varInverterList = varInverterList + ',' + DataVal;
                  }
             }
             
             
             
             
             var bolCheckDefaultInverter = false;
             // var radioValue = $("input[name='GraphType']:checked").val(); 
             var radioValue = $('#hdnGraphType').val();
             
             if(radioValue=='Energy Performance' && varInverterList == '')
             {
                             for(var i=0;i< $inputsAllInverter.length; i++)
                                             {
                                             var chk =  $inputsAllInverter[i].id;
                                                             if($('#div' + chk).css('display') == 'block' && bolCheckDefaultInverter==false)
                                                             {
                                                                             varInverterList = $('#' + chk).attr('data-value');
                                                                             $('#' + chk).prop('checked', true);
                                                                             bolCheckDefaultInverter = true;                                                                                
                                                             }
                                                             
                                             }
                             
             }
             else if(radioValue=='Parameter Comparison' && varInverterList == '')
                 {
                                 for(var i=0;i< $inputsAllInverter.length; i++)
                                                 {
                                                 var chk =  $inputsAllInverter[i].id;
                                                                 if($('#div' + chk).css('display') == 'block' && bolCheckDefaultInverter==false)
                                                                 {
                                                                                 varInverterList = $('#' + chk).attr('data-value');
                                                                                 $('#' + chk).prop('checked', true);
                                                                                 bolCheckDefaultInverter = true;                                                                                
                                                                 }
                                                                 
                                                 }
                                 
                 }
             
             
             
                             AjaxCall(varSiteList,varInverterList,'','',$('#hdnSelectedDataRange').val(),$('#fromDate').val(),$('#toDate').val(),radioValue);
                             
                             
          });
          
          
          
          

	});
</script>


<script type="text/javascript">//<![CDATA[
   	window.onload=function(){                                                
			document.getElementById('load').style.visibility="hidden";
			document.getElementById('ndf').style.visibility="hidden";
                                                            
         $('.ui.dropdown').dropdown({
           action: 'nothing',
           fullTextSearch: true,
         });
         $('.ui.checkbox').checkbox({
        	 fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: false, 
             showOnFocus: true,
             on: "click" 
         });
         
 	}         
</script>



<script type='text/javascript'>//<![CDATA[
                 
                 
                 
               
                 
           
                                 
         $(window).load(function(){                
                $('.ui.dropdown').dropdown({forceSelection:false});
                $('.ui.dropdown').dropdown({action: 'nothing'});
				$('.ui.checkbox').checkbox();                               
                               
		        $.fn.dropdown.settings.selectOnKeydown = false;
		        $.fn.dropdown.settings.forceSelection = false;
         

        
         ViewReport();
         
         $('.ui.dropdown').dropdown({ fullTextSearch: true});
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );//]]> 

     
            $('.validation-form')
            .form({
               on: 'blur',
                 fields: {
                             txtSubject: {
                   identifier: 'txtSubject',
                   rules: [{
                     type: 'empty',
                     prompt: 'Please enter a value'
                   }]
                 },
                  ddlSite: {
                   identifier: 'ddlSite',
                   rules: [{
                     type: 'empty',
                     prompt: 'Please enter a value'
                   }]
                 },
                ddlType: {
                      identifier: 'ddlType',
                   rules: [{
                     type: 'empty',
                     prompt: 'Please enter a value'
                   }]
                },
                ddlCategory: {
                   identifier: 'ddlCategory',
                   rules: [{
                     type: 'empty',
                     prompt: 'Please enter a value'
                   }]
                },
                 ddlPriority: {
                   identifier: 'ddlPriority',
                   rules: [{
                     type: 'empty',
                     prompt: 'Please select a dropdown value'
                   }]
                 },
                 description: {
                   identifier: 'description',
                   rules: [{
                     type: 'empty',
                     prompt: 'Please Enter Description'
                   }]
                 },
                 
               }
            }); ('form').reset('clear');
       
            
           
            
          
      })
         
      
      
      
      </script>



<script>
 $(document).ready(function(){
	 




                                //Validation Start //
             
               //Validation End //
                })
</script>







</head>
<body class="fixed-header">
<input type="hidden" id="hdnSelectedDataRange" value="daily" />

	<input type="hidden" id="hdnGraphType" value="Daily Energy Generation" 	class="chartResponsiveHeightWidth">
	<input type="hidden" value="${access.userID}" id="hdneampmuserid">


	<nav class="page-sidebar" data-pages="sidebar">

		<div class="sidebar-header">
			<a href="./dashboard"> <img src="resources/img/logo01.png"
				class="m-t-10" alt="logo"
				title="Electronically Assisted Monitoring and Portfolio Management">
			</a> <i class="fa fa-bars bars"></i>
		</div>

		<div class="sidebar-menu">

			<c:if test="${not empty access}">

				<ul class="menu-items">



					<c:if test="${access.dashboard == 'visible'}">

						<li style="margin-top: 2px;"><a href="./dashboard"
							class="detailed"> <span class="title">Dashboard</span>
						</a> <span class="icon-thumbnail"><i class="fa fa-tachometer"></i></span>
						</li>
					</c:if>

					<c:if test="${access.overview == 'visible'}">
						<li><a href="#"><span class="title">Overview</span></a> <span
							class="icon-thumbnail"><i class="fa fa-list-alt"></i></span></li>
					</c:if>

					<c:if test="${access.systemMonitoring == 'visible'}">
						<li><a href="#"><span class="title">System
									Monitoring</span></a> <span class="icon-thumbnail"><i
								class="fa fa-desktop"></i></span></li>
					</c:if>

					<c:if test="${access.visualization == 'visible'}">
						<li><a href="javascript:;"><span class="title">Visualization</span>
								<span class=" arrow"></span></a> <span class="icon-thumbnail">
								<i class="fa fa-desktop" aria-hidden="true"></i>
						</span>
							<ul class="sub-menu">

								<c:if test="${access.customerListView == 'visible'}">
									<li><a href="./customerlist">Customer View</a> <span
										class="icon-thumbnail"><i class="fa fa-eye"></i></span></li>
								</c:if>



								<li><a href="./sitelist">Site View</a> <span
									class="icon-thumbnail"><i class="fa fa-map-marker"></i></span>
								</li>

							</ul></li>

					</c:if>

					<c:if test="${access.analytics == 'visible'}">
						<li><a href="#"> <span class="title">Analytics</span>
						</a> <span class="icon-thumbnail"><i class="fa fa-area-chart"></i></span>
						</li>
					</c:if>

					<c:if test="${access.portfolioManagement == 'visible'}">
						<li><a href="#"> <span class="title">Portfolio
									Manager</span>
						</a> <span class="icon-thumbnail"><i class="fa fa-briefcase"></i></span>
						</li>
					</c:if>

					<c:if test="${access.ticketing == 'visible'}">
						<li><a href="javascript:;"><span class="title">Operation
									& Maintenance</span> <span class=" arrow"></span></a> <span
							class="icon-thumbnail" style="padding-top: 5px !important;"><img
								src="resources/img/maintance.png"></span>
							<ul class="sub-menu">

									<c:if test="${access.customerListView == 'visible'}">
									<li><a href="./ticketdetails">Tickets</a> <span
										class="icon-thumbnail"><i class="fa fa-ticket"></i></span></li>

								
								<li><a href="./eventdetails">Events</a> <span
									class="icon-thumbnail"><i class="fa fa-calendar"></i></span></li>
									</c:if>


							</ul></li>





					</c:if>


					<c:if test="${access.forcasting == 'visible'}">
						<li><a href="#"><span class="title">Forecasting</span></a> <span
							class="icon-thumbnail"><i class="fa fa-desktop"></i></span></li>
					</c:if>

					<c:if test="${access.analysis == 'visible'}">
						<li class="active"><a href="./analysis"><span
								class="title">Analysis</span></a> <span class="icon-thumbnail"><i
								class="fa fa-area-chart"></i> </span></li>
					</c:if>


					
            <c:if test="${access.configuration == 'visible'}">
            
               <li>
                  <a href="javascript:;"><span class="title">Configuration</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="pg-tables"></i></span>
                  <ul class="sub-menu">
                     
                     <li>
                        <a href="./customers">Customer Config.</a>
                        <span class="icon-thumbnail">CU</span>
                     </li>
                     
                     <li>
                        <a href="./sites">Site Config.</a>
                        <span class="icon-thumbnail">SI</span>
                     </li>
                     
                     <li>
                        <a href="./equipments">Equipment Config.</a>
                        <span class="icon-thumbnail">EQ</span>
                     </li>
                     
                   <!--   <li>
                        <a href="#">Data Logger Config.</a>
                        <span class="icon-thumbnail">DL</span>
                     </li>
                     
                     <li>
                        <a href="#">Equ-Attrib Config.</a>
                        <span class="icon-thumbnail">EA</span>
                     </li>
                     
                      <li>
                        <a href="#">Configuration Loader</a>
                        <span class="icon-thumbnail">CL</span>
                     </li>
                     
                     <li>
                        <a href="./activities">Activity Config.</a>
                        <span class="icon-thumbnail">AC</span>
                     </li>
                     
                      <li>
                        <a href="./timezones">Timezone Config.</a>
                        <span class="icon-thumbnail">TZ</span>
                     </li>
                     
                      <li>
                        <a href="./currencies">Currency Config.</a>
                        <span class="icon-thumbnail">CY</span>
                     </li>
                     
                      <li>
                        <a href="./unitofmeasurements">Unit Measurement Config.</a>
                        <span class="icon-thumbnail">UM</span>
                     </li>
                     
                     
                     <li>
                        <a href="./countryregions">Country Region Config.</a>
                        <span class="icon-thumbnail">CR</span>
                     </li>
                     
                     <li>
                        <a href="./countries">Country Config.</a>
                        <span class="icon-thumbnail">CO</span>
                     </li>
                     
                     <li>
                        <a href="./states">State Config.</a>
                        <span class="icon-thumbnail">SE</span>
                     </li>
                     
                     <li>
                        <a href="./customertypes">Customer Type Config.</a>
                        <span class="icon-thumbnail">CT</span>
                     </li>
                     
                      <li>
                        <a href="./sitetypes">Site Type Config.</a>
                        <span class="icon-thumbnail">ST</span>
                     </li>
                     
                      <li>
                        <a href="./equipmentcategories">Equ Category Config.</a>
                        <span class="icon-thumbnail">EC</span>
                     </li>
                     
                      <li>
                        <a href="./equipmenttypes">Equ Type Config.</a>
                        <span class="icon-thumbnail">ET</span>
                     </li>
                     
                      <li>
                        <a href="#">Event Type Config.</a>
                        <span class="icon-thumbnail">EY</span>
                     </li>
                     
                     <li>
                        <a href="#">Event Config.</a>
                        <span class="icon-thumbnail">EV</span>
                     </li>
                     
                      <li>
                        <a href="#">Inspection Config.</a>
                        <span class="icon-thumbnail">IN</span>
                     </li> -->
                   <!--   
                      <li>
                        <a href="./userroles">User Role Config.</a>
                        <span class="icon-thumbnail">UR</span>
                     </li>  -->
                     
                      <li>
                        <a href="./users">User Config.</a>
                        <span class="icon-thumbnail">US</span>
                     </li>
                     
                  </ul>
               </li>
          
           </c:if>
				</ul>


			</c:if>




			<div class="clearfix"></div>
		</div>



	</nav>


	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					<a href="./dashboard"><img src="resources/img/logo01.png"
						alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx" id="SearcSElect">
					<select
						class="ui fluid search selection dropdown width oveallSearch"
						id="searchSelect">
						<!--  <option value="-2"></option> -->
						<option value="-1">search</option>
						<option value="0">Dashboard</option>

						<c:if test="${not empty access}">
							<c:if test="${access.customerListView == 'visible'}">
								<option value="1">Customer View</option>
							</c:if>
						</c:if>

						<option value="3">Site View</option>
						
						<c:if test="${access.customerListView == 'visible'}">
						<option value="6">Tickets</option>
						</c:if>
						
						<c:if test="${access.customerListView == 'visible'}">
						<option value="7">Events</option>
						</c:if>
						
							
						
						
						<c:if test="${access.customerListView == 'visible'}">
                             <option value="17">Site Configuration</option>
                             
                           </c:if>
            		
            		<c:if test="${access.customerListView == 'visible'}">
                             <option value="18">Equipment Configuration</option>
                             
                           </c:if>
                         
                              <c:if test="${access.customerListView == 'visible'}">
          				 			  <option value="19">Customer Configuration</option>
                    		   </c:if>
                    		   
                    		   
                    		     <c:if test="${access.customerListView == 'visible'}">
          				 			  <option value="20">User Configuration</option>
                    		   </c:if>



					</select>
				</div>
				<div class="dropdown pull-right hidden-md-down user-log">
					<button class="profile-dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 circular inline"> <img
							src="resources/img/user_01.png" alt
							data-src="resources/img/user_01.png"
							data-src-retina="resources/img/user_01.png" width="32"
							height="32">
						</span>

						<!--  <p class="user-login">ICE</p>
                  -->

						<c:if test="${not empty access}">
							<p class="user-login">${access.userName}</p>
						</c:if>



					</button>
					<div class="dropdown-menu dropdown-menu-right profile-dropdown"
						role="menu">
						<a class="dropdown-item disablecolor"><i
							class="pg-settings_small"></i> Settings</a> <a
							class="dropdown-item disablecolor"><i class="pg-outdent"></i>
							Feedback</a> <a class="dropdown-item disablecolor"><i
							class="pg-signals"></i> Help</a> <a href="logout1"
							class="dropdown-item color"><i class="pg-power"></i> Logout</a>
					</div>
				</div>

				 <c:if test="${access.customerListView == 'visible'}">
					<div>
						<p class="center mb-1 tkts" data-toggle="modal"
							data-target="#tktCreation" data-backdrop="static"
							data-keyboard="false">
							<img src="resources/img/tkt.png">
						</p>
						<p class="create-tkts">New Ticket</p>
					</div>
				</c:if>
				

				<!--  <span data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><p class="center m-t-5 tkts"><img class="m-t-5" src="resources/img/tkt.png"><p class="create-tkts">New Ticket</p></p></span> -->
				<!-- <span ><p class="center m-t-5 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img class="m-t-5" src="resources/img/tkt.png"><p class="create-tkts">New Ticket</p></p></span> -->
				<!-- <a href="#" class="header-icon  btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><i class="fa fa-ticket fa-2x"></i></a> -->
				<!-- <a href="#" class="header-icon pg pg-alt_menu btn-link  sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a> -->
			</div>
		</div>
		<div class="page-content-wrapper ">

			<div class="equ-cont content " id="QuickLinkWrapper1">

				<div class="container-fixed-lg">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="./dashboard"><i
								class="fa fa-home mt-7"></i></a></li>
						<li class="breadcrumb-item"><a>Analysis</a></li>
					</ol>
				</div>



				<div id="newconfiguration">
					<div class="card card-transparent">
						<div class="sortable padd-3">
							<div class="row">
								
								<div class="col-md-12 col-lg-12 col-xs-12 chart-mob">
									<div class="card card-default bg-default" id="sitestatus"
										data-pages="card">
										<div class="padd10">
										<div class="col-md-12 padd-0">
												<a  id="btnChoose" style="width: 100px;background: #208388;    margin-left: 3px;padding: 4px 6px !important;" class="btn btn-primary btn-padd">Select<i class="fa fa-filter m-l-15" aria-hidden="true"></i></a>
												<a  id="btnApply" style="width: 98px;    margin-left: 1px;padding: 4px 6px !important;color:#FFF;display:none;" class="btn btn-success btn-padd">Apply</a>
												<a  id="btnClearAll" style="width: 98px;    margin-left: 1px;padding: 4px 6px !important;color:#FFF;display:none;" class="btn btn-success btn-padd closeAll">Clear All</a>
												
											</div>
											<!-- <div class="col-md-2">
												<div class="alert alert-info closeAll info-window"  id="btnClearAll" role="alert">
                                        <button class="close" data-dismiss="alert"></button>
                                        <strong>Clear All </strong>
                                    </div>
											</div> -->
											<div class="col-md-12 m-t-15 padd-0">
											<div class="col-md-3 padd-0">
												<div class="ui-widget">
											        <!-- <label for="tags">Tags: </label> -->
											        <input id="tags" class="form-control" placeholder="Search..." />
											    </div>
											   </div>
											</div>
										<div class="col-md-12 Sele padd-0" style="display:none;">											
												<div class=" padd-0">
												<div class="SiteFilterList padd-0"></div> 												
												<div class="EquipmentFilterList padd-0"></div>
												<div class="ParameterFilterList  padd-0"></div>
											</div>
										</div>
											

										</div>
										<div class="chart-mob">
									<div class="card card-default bg-default eqchart-mob graphHeight"
										id="sitestatus" data-pages="card">
										<div class="col-md-12 padd-0" style="">
										<div class="col-md-3 col-xs-12 rtypemob"
												style="padding-left: 10px;">
												<div class="rtype">
												<!-- <div class="col-md-3 padd-0">
													<a id="btnChoose" style="width: 100px;margin-left: 15px;" class="btn btn-primary">Choose<i class="fa fa-filter m-l-15" aria-hidden="true"></i></a>
												</div> -->
												<div class="col-md-9">
													<p><b>Analysis</b></p>
												</div>
													
													
												</div>
												<div class="reportType">
													<input class="form-control" type="search" id="my-search"
														placeholder="search">
														
													<ul id="my-tree">
														<li class="tf-open">
															<div>Energy</div>
															<ul>
																<li id="lirdoDailyEnergy" class="tf-selected disabled"><div
																		class="GraphTypeClick" name="GraphType"
																		id="rdoDailyEnergy" value="Daily Energy Generation">Daily Energy Generation</div></li>
																<li id="lirdoEnergyPerformance"><div
																		class="GraphTypeClick" name="GraphType"
																		id="rdoEnergyPerformance" value="Energy Performance">Energy Performance</div></li>
																<li id="lirdoParameterComparison"><div
																		class="GraphTypeClick" name="GraphType"
																		id="rdoParameterComparison" value="Parameter Comparison">Parameter Comparison</div></li>
															</ul>
														</li>
													</ul>
																							<!-- <input class="GraphTypeClick" type="radio" name="GraphType" id="rdoDailyEnergy" value="Daily Energy Generation" checked> Daily Energy Generation <br>
                                                                                            <input class="GraphTypeClick" type="radio" name="GraphType" id="rdoEnergyPerformance" value="Energy Performance"> Energy Performance -->
												</div>
											</div>
											
											<div class="graphPart">
												<div class="col-md-9" style="padding:5px 0px;">
												<div class="col-md-12">
													<div class="col-md-6">
														<div class="ui basic right labeled dropdown icon button" style="z-index:999;width:100%;">
  <i class="dropdown icon"></i>
  <span class="ui">Equipment Filter</span>
  <div class="menu">
    <div class="ui icon search input">
      <i class="search icon"></i>
      <input type="text" name="Search" placeholder="Search&hellip;">
    </div>
    <div class="scrolling menu">
      <div class="ui item checkbox" data-value="item1">
        <input type="checkbox" name="item1">
        <label>First item</label>
      </div>
      <div class="ui item checkbox" data-value="item2">
        <input type="checkbox" name="item2">
        <label>Second item</label>
      </div>
      <div class="ui item checkbox" data-value="item3">
        <input type="checkbox" name="item3">
        <label>Third item</label>
      </div>
      <div class="ui item checkbox" data-value="item4">
        <input type="checkbox" name="item4">
        <label>Fourth item</label>
      </div>
      <div class="ui item checkbox" data-value="item5">
        <input type="checkbox" name="item5">
        <label>Fifth item</label>
      </div>
      <div class="ui item checkbox" data-value="item6">
        <input type="checkbox" name="item6">
        <label>Sixth item</label>
      </div>
      <div class="ui item checkbox" data-value="item7">
        <input type="checkbox" name="item7">
        <label>Seventh item</label>
      </div>
      <div class="ui item checkbox" data-value="item8">
        <input type="checkbox" name="item8">
        <label>Eighth item</label>
      </div>
    </div>
  </div>
</div>
														
													</div>
													
													<div class="col-md-6">
														<div class="ui basic right labeled dropdown icon button" style="z-index:999;width:100%;">
  <i class="dropdown icon"></i>
  <span class="ui">Parameter Filter</span>
  <div class="menu">
    <div class="ui icon search input">
      <i class="search icon"></i>
      <input type="text" name="Search" placeholder="Search&hellip;">
    </div>
    <div class="scrolling menu">
      <div class="ui item checkbox" data-value="item1">
        <input type="checkbox" name="item1">
        <label>First item</label>
      </div>
      <div class="ui item checkbox" data-value="item2">
        <input type="checkbox" name="item2">
        <label>Second item</label>
      </div>
      <div class="ui item checkbox" data-value="item3">
        <input type="checkbox" name="item3">
        <label>Third item</label>
      </div>
      <div class="ui item checkbox" data-value="item4">
        <input type="checkbox" name="item4">
        <label>Fourth item</label>
      </div>
      <div class="ui item checkbox" data-value="item5">
        <input type="checkbox" name="item5">
        <label>Fifth item</label>
      </div>
      <div class="ui item checkbox" data-value="item6">
        <input type="checkbox" name="item6">
        <label>Sixth item</label>
      </div>
      <div class="ui item checkbox" data-value="item7">
        <input type="checkbox" name="item7">
        <label>Seventh item</label>
      </div>
      <div class="ui item checkbox" data-value="item8">
        <input type="checkbox" name="item8">
        <label>Eighth item</label>
      </div>
    </div>
  </div>
</div>
														
													</div>
												</div> 
													<div class="col-md-4 padd-0 m-t-15" id="my_styles">
														<button class="btn btn-primary btn-padd btn-range-selector active" id="daily">Daily</button>
														<button class="btn btn-primary btn-padd btn-range-selector" id="weekly">Weekly</button>
														<button class="btn btn-primary btn-padd btn-range-selector" id="monthly">Monthly</button>
														<button class="btn btn-primary btn-padd btn-range-selector" id="yearly">Yearly</button>
														<button class="btn btn-primary btn-padd btn-range-selector" id="custom">Custom</button>
													</div>
													<div class="col-md-4 padd-0 center m-t-15">
													<button class="btn btn-primary btn-padd btn-range-selector btn-lhs" onclick="myFunctionMins()" id="daily">Prev</button>
														<!-- <button class="btn-lhs" onclick="myFunctionMins()"><i class="fa fa-angle-double-left"></i></button> -->
														<button class="btn btn-default btn-padd btn-day-width" id="CurrentVal"></button>
														<button class="btn btn-primary btn-padd btn-range-selector btn-rhs" onclick="myFunctionPlus()" id="daily">Next</button>
													
														<!-- <button class="btn-rhs" onclick="myFunctionPlus()"><i class="fa fa-angle-double-right"></i></button> -->
													</div>
													<div class="col-md-4 padd-0 m-t-15">
														<div class="col-md-6 padd-0">
															<input type="text" class="form-control" id="fromDate" name="from" maxlength="10">
															<label for="from" class="right">From</label>
														</div>
														<div class="col-md-6 padd-0">
															<input type="text" class="form-control" id="toDate" name="to" maxlength="10">
															<label for="to" class="right">To</label>
														</div>
														<span id="response"></span>
													</div>

												</div>
												
												<!-- <div id="load"></div> -->
											<!-- 	<div id="load1" style="text-align: center;"></div> -->
											
												<div id="ndf" style="visibility: hidden"><h2>No Data Found</h2></div>
												<div class="graphPart" style="border-top:0px;">
												<p class="center graphName" id="lblReportName" style="margin:0px;padding-top: 40px;">Daily
													Energy Generation</p>
													<div id="load"   style="position: absolute;width: 100%;text-align: center;">
													<img src="resources/img/loading_green.gif" style="text-align:center;margin-top:10%;">
												
													</div>
													
													<div id="container" ></div>
												</div>
											</div>
										</div>
									</div>


								</div>
									</div>


								</div>
							</div>
							

							<div class="row">
								<div class="col-lg-12  chart-mob">
									<div class="card card-default bg-default  filterbox chkboxFilter"  data-pages="card" style="display:none;">
										
										<div class="padd10">		
																		
											<div class="col-md-12 col-lg-12 col-xs-12 padd-0">
											<div class="row" style="float:right;">												
												<i class="fa fa-close close-icon"></i>											
											</div>
										<!-- 	<div class="col-md-2 padd-0">
												<div class="field required m-b-0">
    			 				  <label style="color:#000;"><b>Graph Type</b></label>
    			 			</div>  
    			 			<div class="field">
    			 			 <select class="ui search selection dropdown" name="ddlGraphType" id="ddlGraphType">
    			 			 		<option value="">Select</option>
						        	<option class="GraphTypeClick" name="GraphType" id="rdoDailyEnergy" value="0">Daily Energy Generation</option>
						        	<option class="GraphTypeClick" name="GraphType" id="rdoEnergyPerformance" value="1">Energy Performance</option>
						        	<option class="GraphTypeClick" name="GraphType" id="rdoParameterComparison" value="2">Parameter Comparison</option>						         
						          
						      </select>
						    </div>
											</div> -->
												<div class="col-md-6 col-xs-12">
													<div class="ui checkbox">
														<input type="checkbox" name="example" id="chkSites">
														<label><b>Sites</b></label>
													</div>
													<!--  <p> <input type="checkbox" id="chkSites"><b>Sites</b></p> -->
													<div class="ui basic  labeled dropdown icon" style="width: 100%;" multiple="multiple">
														<div class="menu transition visible" tabindex="-1"
															style="width: 100%; display: block !important;background: #dff0d8;">
															<div class="ui icon search input">
																<i class="search icon"></i> 
																<input type="text" name="Search" placeholder="Search&hellip;">
															</div>
															<div class="scrolling menu inverterData dropdata" id="ddfSite" style="padding-top: 10px;background: #dff0d8;">
															 <c:forEach items="${lstSite}" var="lstsite">
																	<div id="divchk${lstsite.siteId}"
																		class="ui item checkbox divchksite"
																		data-value="${lstsite.siteId}"
																		 data-customer="${lstsite.customerID}" >																		 
																		 <!-- style="display: none" -->																		 
																		<input type="checkbox" id="chk${lstsite.siteId}"
																			class="site chksite"
																			data-target=".site${lstsite.siteId}"
																			data-value="${lstsite.siteId}"
																			data-customer="${lstsite.customerID}"> <label id="lblchk${lstsite.siteId}">${lstsite.siteName}
																			- ${lstsite.customerNaming}</label>
																	</div>
																</c:forEach>
															</div>
														</div>
													</div>
												</div>
											 <div class="col-md-3">
													<div class="ui form  field checkbox">
														<input type="checkbox" name="example" id="chkInverters">
															<label><b>Equipment</b></label>
													</div>													
													<div class="ui basic  labeled dropdown icon" style="width: 100%;">
														<div class="menu transition visible" tabindex="-1"
															style="width: 100%; display: block !important;background: #f2dede;">
															<div class="ui icon search input">
																<i class="search icon"></i> <input type="text"
																	name="Search" placeholder="Search&hellip;">
															</div>
															<div class="scrolling menu inverterData dropdata" id="ddfInverter" style="background: #f2dede;    padding-top: 10px;">
																<c:forEach items="${lstInverter}" var="lstinverter">
																	<div id="divchkequ${lstinverter.equipmentId}"
																		class="ui item checkbox divchkinverter"
																		data-value="${lstinverter.equipmentId}"
																		data-site="${lstinverter.siteID}"
																		style="display: none">
																		<input type="checkbox"
																			id="chkequ${lstinverter.equipmentId}"
																			class="site${lstinverter.siteID} chkinverter"
																			data-value="${lstinverter.equipmentId}"
																			data-site="${lstinverter.siteID}"> <label id="lblchkequ${lstinverter.equipmentId}" >${lstinverter.customerNaming}
																			(${lstinverter.siteReference})</label>

																	</div>
																</c:forEach>
																<div class="message" id="divNoDataFoundInv">No Equipments found.</div>
															</div>
														</div>
													</div>
												</div> 
												<div class="col-md-3">
												<div class="ui form  field checkbox">
														<input type="checkbox" name="example" id="chkParameters" >
														<label><b>Parameters</b></label>
														</div>
												
											
											
											<div class="ui basic  labeled dropdown icon" style="width: 100%;background: #f2dede !important;">
														<div class="menu transition visible" tabindex="-1"
															style="width: 100%; display: block !important;background: #d9edf7  !important;">
															<div class="ui icon search input" style="background:#d9edf7 !important;">
																<i class="search icon"></i> <input type="text"
																	name="Search" placeholder="Search&hellip;">
															</div>
															<div class="scrolling menu inverterData dropdata" id="ddfParameter" style="background: #d9edf7;    padding-top: 10px;">
																
																<!-- <div class="message" id="divNoDataFoundInv">No Parameters found.</div> -->
															</div>
														</div>
													</div>
													
													
													
												
												
												<!-- 
												
												
												<div class="sol-container sol-selection-bottom sol-active" style="width: auto; visibility: initial;">
													<div class="sol-selection-container" style="border-top-right-radius: initial; top: 223px; left: 1216px; width: 192px;">
														<div class="sol-action-buttons" style="border-top-right-radius: initial;">
															<a href="#" class="sol-select-all">Select all</a><a href="#" class="sol-deselect-all">Select none</a>
															<div class="sol-clearfix"></div>
														</div>
														<div class="sol-no-results" style="display: none;">No entries found</div>
														<div class="sol-selection" id="ddlParameterList">
														
															 <div class="sol-option">
																<label class="sol-label">
																	<input type="checkbox" class="sol-checkbox" name="character" value="EnergyPerform">
																	<div class="sol-label-text">Energy Perform</div>
																</label>
															</div>  
															
															
															
															
														</div>
													</div>
												
													<div class="sol-current-selection"></div>
													<div class="sol-inner-container">
														<div class="sol-input-container"><input type="text" placeholder="Select"></div>
														<div class="sol-caret-container"><b class="sol-caret"></b></div>
													</div>
												</div>
												
												
												
												
												
												
												
												
													<select id="ddlParameter" name="character" multiple="multiple">   
													
													
													<option id="divchkparamEnergyPerform" value="EnergyPerform">Energy Perform</option> 
      											</select>
												
												-->
												
												
												
												
												
												
												
												
												
												
												
												
												</div>
											</div>
											<div class="col-md-12 filterfooter center" style="position: absolute;bottom:10px;">
											<!-- 	<div class="col-md-10"></div> -->
												<!-- <div class="col-md-2"> -->
												<button type="button" id="btnView" class="btn btn-default btn-success filterbutton">View</button> 
												<button type="button" id="btnClrFilter" class="btn btn-primary btn-info filterbutton">Clear</button>
												


											</div>

											
										</div>

									</div>



								</div>
								
							</div>

							




						</div>
					</div>
				</div>
				
			</div>
		</div>


	</div>


	<div class="container-fixed-lg">
		<div class="container-fluid copyright sm-text-center">
			<p class="small no-margin pull-left sm-pull-reset">
				<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
					CLEAN ENERGY.</span> <span class="hint-text">All rights reserved. </span>
			</p>
			<p class="small no-margin pull-rhs sm-pull-reset">
				<span class="hint-text">Powered by</span> <span>MESTECH
					SERVICES PVT LTD</span>.
			</p>
			<div class="clearfix"></div>
		</div>
	</div>
	</div>
	</div>

	<!-- Side Bar Content Start-->


	<!-- Side Bar Content End-->

	<div class="quickview-wrapper  builder hidden-sm hidden-xs"
		id="builder">
		<div class="p-l-10 p-r-10 ">
			<a class="builder-close quickview-toggle pg-close"></a> <a
				class="builder-toggle" data-toggle="quickview"
				data-toggle-element="#builder"><img
				src="resources/img/ImageResize_06.png"></a>
			<ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary"
				id="builderTabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#tabLayouts" role="tab"
					aria-controls="home"><span>Quick Links</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active " id="tabLayouts" role="tabcard">
					<div class="scrollable">
						<div class="p-r-50">
							<div class="list">
								<ul>
									<!--  <li><a href="#QuickLinkWrapper1">Equipment Grpah</a></li>
                              <li><a href="#QuickLinkWrapper2">Event History Details</a></li>        -->
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>





	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>
	<script src="resources/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>
	<script src="resources/js/jquery.tree-multiselect.js"></script>
	<div id="gotop"></div>



	<div id="tktCreation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header Popup-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="resources/img/close.png">
					</button>
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<p class="modal-title tkt-title">
						<img src="resources/img/tkt.png"><span class="tkt-title">New
							Ticket</span>
					</p>
				</div>
				<div class="modal-body">
					<c:url var="addAction" value="/addnewticket"></c:url>
					<div class="col-md-12">
						<p class="tkt-mandat">
							<span class="errormess">*</span> All fields are mandatory
						</p>
					</div>
					<form:form action="${addAction}" modelAttribute="ticketcreation"
						class="ui form validation-form">

						<div class="col-md-12  m-t-10">
							<div class="col-md-4">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8">
								<div class="field">
									<form:select class="ui fluid search selection dropdown width"
										id="ddlSite" name="ddlSite" path="siteID">
										<form:option value="">Select</form:option>
										<form:options items="${siteList}" />
									</form:select>
								</div>

							</div>
						</div>
						<div class="col-md-12 m-t-15">
							<div class="col-md-4">
								<label class="fields required m-t-10">Type</label>
							</div>
							<div class="col-md-8">
								<div class="field">
									<form:select class="ui fluid search selection dropdown width"
										id="ddlType" name="ddlType" path="ticketType">
										<form:option value="">Select</form:option>
										<form:option value="Operation">Operation</form:option>
										<form:option value="Maintenance">Maintenance</form:option>
									</form:select>
								</div>
							</div>
						</div>
						<div class="col-md-12 m-t-15">
							<div class="col-md-4">
								<label class="fields required m-t-10">Category</label>
							</div>
							<div class="col-md-8">
								<div class="field">

									<form:select
										class="ui fluid search selection dropdown width category"
										id="ddlCategory" name="ddlCategory" path="ticketCategory">
										<form:option value="">Select </form:option>
										<form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
										<form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
										<form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
										<form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
										<form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
										<form:option data-value="Operation"
											value="Equipment Replacement">Equipment Replacement</form:option>
										<form:option data-value="Operation"
											value="Communication Issue">Communication Issue</form:option>
										<form:option data-value="Operation" value="String Down">String Down</form:option>
										<form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
										<form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
										<form:option data-value="Operation" value="">Select</form:option>
										<form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
										<form:option data-value="Maintenance"
											value="Inverter Cleaning">Inverter Cleaning</form:option>
										<form:option data-value="Maintenance"
											value="DataLogger Cleaning">DataLogger Cleaning</form:option>
										<form:option data-value="Maintenance"
											value="String Current Measurement">String Current Measurement</form:option>
											<form:option data-value="Maintenance" value="Preventive Maintenance">Preventive Maintenance</form:option>
										
										<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
										
										<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
										<form:option data-value="Maintenance"
											value="Visual Inspection">Visual Inspection</form:option>
										<form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
										<form:option data-value="Maintenance" value="">Select</form:option>
									</form:select>
								</div>
							</div>

						</div>
						<div class="col-md-12 m-t-15">
							<div class="col-md-4">
								<label class="fields required m-t-10">Subject</label>
							</div>
							<div class="col-md-8">
								<div class="field">
									<form:input placeholder="Subject" name="Subject" type="text"
										id="txtSubject" path="ticketDetail" maxLength="50" />
								</div>
							</div>
						</div>

						<div class="col-md-12 m-t-15">
							<div class="col-md-4">
								<label class="fields required m-t-10">Priority</label>
							</div>
							<div class="col-md-8">
								<div class="field">
									<form:select class=" ui fluid search selection dropdown width"
										id="ddlPriority" name="user" path="priority">
										<form:option value="">Select </form:option>
										<form:option data-value="3" value="3">High</form:option>
										<form:option data-value="2" value="2">Medium</form:option>
										<form:option data-value="1" value="1">Low</form:option>
									</form:select>
								</div>

							</div>
						</div>
						<div class="col-md-12 m-t-15">
							<div class="col-md-4">
								<label class="fields required m-t-10">Description</label>
							</div>
							<div class="col-md-8">
								<div class="field">
									<textarea id="txtTktdescription" name="description"
										style="resize: none;" placeholder="Ticket Description"
										maxLength="120"></textarea>
								</div>

							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-15 center">
							<div class="btn btn-success  submit">Create</div>
							<div class="btn btn-primary clear m-l-15">Reset</div>
						</div>
						<!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
						<!-- <div class="ui error message"></div> -->
					</form:form>
					<!-- <div class="ui error message"></div> -->

				</div>
			</div>
		</div>



		<script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
      
    	
    	
    			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.8/semantic.min.js"></script>
    	
		<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script> -->
		
</body>
</html>
