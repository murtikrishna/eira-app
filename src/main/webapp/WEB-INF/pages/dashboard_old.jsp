<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="true" %>


<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
      <meta charset="utf-8"/>
      <title>:: Welcome To eAMPM ::</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
     <link rel="icon" type="image/x-icon" href="favicon.ico"/>
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-touch-fullscreen" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="default">
      <meta content name="description"/>
      <meta content name="author"/>

      
      <link href="resources/css/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/select2.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/pages-icons.css" rel="stylesheet" type="text/css">
      <link href="resources/css/animate.css" rel="stylesheet"  />
      <link href="resources/css/pages.css" class="main-stylesheet"  rel="stylesheet" type="text/css"/>
      <link href="resources/css/styles.css" rel="stylesheet" type="text/css">
      <link href="resources/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
      <link href="resources/css/site.css" rel="stylesheet" >      
      <link href="resources/css/bootstrap.min.3.37.css" type="text/css" rel="stylesheet">
      <link href="resources/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="resources/css/semantic.min.css">
      <script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
      <script src="resources/js/searchselect.js" type="text/javascript"></script>
      
      
      <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script> -->
		<script type="text/javascript" src="https://rawgit.com/Semantic-Org/Semantic-UI/next/dist/semantic.js"></script>
      
	 <script type="text/javascript">
        $(document).ready(function() {
        	 $('.ui.dropdown').dropdown({forceSelection:false});        	 
        	 $('#example').DataTable();        
            if($(window).width() < 767)
				{
				   $('.card').removeClass("slide-left");
				   $('.card').removeClass("slide-right");
				   $('.card').removeClass("slide-top");
				   $('.card').removeClass("slide-bottom");
				}
        	 
           $('body').click(function(event){
                $('#builder').removeClass('open');                 
           });           
              
            
            $('.close').click(function(){
				$('.clear').click();
			});
			            
        
	        $('.clear').click(function(){
	        	$('.search').val("");
	        });
        	
	        $('#ddlType').change(function(){              
	             $('.category').dropdown('clear')
	            });

        

        $("#searchSelect").change(function() {
            var value = $('#searchSelect option:selected').val();
            var uid =  $('#hdneampmuserid').val();
         	redirectbysearch(value,uid);
 	    });    
                 
        $('.dataTables_filter input[type="search"]').attr('placeholder','search');        
            });
           
				
        
      </script>

<script type='text/javascript'>//<![CDATA[
         $(window).load(function(){
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
         
         
         $('.ui.dropdown').dropdown({
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: true, 
             showOnFocus: true,
             on: "click" 
           });

		$('.ui.dropdown.oveallsearch').dropdown({
             onChange: function (value, text, $selectedItem) {
                
                var uid =  $('#hdneampmuserid').val();
                redirectbysearchvalue(value,uid);
             },
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: false, 
             showOnFocus: true,
             on: "click" 
           });
		
		
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );//]]> 



             $('.validation-form')
   .form({
     on: 'blur',
     onFailure: function(formErrors, fields) {
       $.each(fields, function(e) {
         var ele = $('#' + e);
         var fail = (ele.val() === false || ele.val().length === 0);
         if (fail) {
           console.log(ele, fail);
           ele.focus();
           $('html,body').animate({
             scrollTop: ele.offset().top - ($(window).height() - ele.outerHeight(true)) / 2
           }, 200);
           return false;
         }
       });
       return false;
     },
     fields: {
    	 txtSubject: {
         identifier: 'txtSubject',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         },
         {
             type: 'maxLength[50]',
             prompt: 'Please enter a value'
           }]
       },
        ddlSite: {
         identifier: 'ddlSite',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
       },
      ddlType: {
            identifier: 'ddlType',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
      },
      ddlCategory: {
         identifier: 'ddlCategory',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
      },
       ddlPriority: {
         identifier: 'ddlPriority',
         rules: [{
           type: 'empty',
           prompt: 'Please select a dropdown value'
         }]
       },
       description: {
         identifier: 'description',
         rules: [{
           type: 'empty',
           prompt: 'Please Enter Description'
         }]
       },
       
     }
   }); ('form').reset('clear');

         })
         
      </script>
      
      <style>
      		div.dataTables_wrapper div.dataTables_length select {
			    width: 50px;
			    display: inline-block;
			    margin-left: 3px;
			    margin-right: 4px;
			}
			/* .ui.form .fields.error .field .ui.dropdown, .ui.form .field.error .ui.dropdown {
    border-color: #E0B4B4 !important;
    line-height: 20.5px !important;
    
}
.ui.form .fields.error input[type="text"] {
    border-color: #E0B4B4 !important;
    line-height: 19px;
    
} */
	  		#overviewInfor {
                  display:none;
            }
			
			.ui.form textarea:not([rows]) {
			    height: 4em;
			    min-height: 4em;
			    max-height: 24em;
			}
			.ui.icon.input > i.icon:before, .ui.icon.input > i.icon:after {
			    left: 0;
			    position: absolute;
			    text-align: center;
			    top: 36%;
			    width: 100%;
			}
			.input-sm, .form-horizontal .form-group-sm .form-control {
			    font-size: 13px;
			    min-height: 25px;
			    height: 32px;
			    padding: 8px 9px;
			}
      </style>
      
      <script type='text/javascript'>
         function initialize() {

        
            var active = "resources/img/active.png";
            var warning = "resources/img/warning.png";
            var down = "resources/img/down.png";
            var offline = "resources/img/offline.png";

            
         var a = $('#MapJsonId').val();
            console.log(a);
            
            a = eval('(' + a + ')'); 
            // locations=[];
		 var locations = a;
          /*  var locations = [

              ['SITE 1', 13.3524101, 74.7906418, 'NEW DATA SET',down,9999],
              ['SITE 2',13.3694539,74.7367716,'Panel 01',active],
              ['<b>TATA Motors Pvt Ltd</b>' + '\n' + '<p>WD0086BF</p>' + '\n' + '<p>KG Halli, AshokeNagar,</p>'+ '\n' + '<p>Bengaluru - 560001</p>'+ '\n' + 'Karnataka, India', 29.1732234, 76.9003276, 'Site 03',warning],
              ['SITE 4',8.4548082,72.81203529999993,'Site 04',offline],
              ['SITE 5',19.2147067,72.91062020000004,'Site 05',down],
              ['SITE 5',11.6680848,78.1321082,'Site 06',active]
             ]; */
                   
            //var locations = [['<b>India Food Park - KART1003</b><p>KIADB Phase 3, Vasanthanarsapura Industrial Area, Tumkur.</p><p>Korahavali - 572201.</p>',13.4924793, 77.0411002,'India Food Park',active,99997,'/eampm-maven/siteview7&ref=2'],['<b>Kensington A - MHRT1009</b><p>Kensington A-Wing Business park, Hiranandani Gardens, Powai</p><p>Powai- 400076.</p>',19.111976, 72.911129,'Kensington A',offline,99997,'/eampm-maven/siteview5&ref=2']]
            
           
            
            var myJsonString = JSON.stringify(locations);
            console.log(myJsonString);
            window.map = new google.maps.Map(document.getElementById('dvMap'), {
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();

            var bounds = new google.maps.LatLngBounds();

            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    disableDefaultUI: true ,
                    Icon: locations[i][4],
                    title: locations[i][3],
                    zIndex: locations[i][5],
                });

                bounds.extend(marker.position);

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }

            map.fitBounds(bounds);

            var listener = google.maps.event.addListener(map, "idle", function() {
                fnZoom();
                google.maps.event.removeListener(listener);
            });

             function fnZoom() {
                // alert('Work');
                if (locations.length ==1) {
                    map.setZoom(15);
                }
            }
        }

       function loadScript() {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDEbIbuKRR_b0iHzErE79mYJsqkh3LIb_g&sensor=false&' + 'callback=initialize';
            document.body.appendChild(script);
        } 

        window.onload = loadScript;
        
    </script>
 
   
  </head>
  <body  class="fixed-header">
  
    <input type="hidden" value="${access.userID}" id="hdneampmuserid">
    
           <nav class="page-sidebar" data-pages="sidebar">
        
         <div class="sidebar-header">
            <a href="./dashboard">
            	<img src="resources/img/logo01.png"  class="m-t-10" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management">
            	</a>
            <i class="fa fa-bars bars"></i>
         </div>
         
         <div class="sidebar-menu">
         
          <c:if test="${not empty access}">
          
            <ul class="menu-items">
            
            
            
           <c:if test="${access.dashboard == 'visible'}">
          
                   <li class="active" style="margin-top:2px;">
                  <a href="./dashboard" class="detailed">
                  <span class="title">Dashboard</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-tachometer"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.overview == 'visible'}">
                   <li>
                  <a href="#"><span class="title">Overview</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-list-alt"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.systemMonitoring == 'visible'}">
                  <li>
                  <a href="#"><span class="title">System Monitoring</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-desktop"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.visualization == 'visible'}">
           <li>
                  <a href="javascript:;"><span class="title">Visualization</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="fa fa-desktop" aria-hidden="true"></i></span>
                  <ul class="sub-menu">
                       
                    <c:if test="${access.customerListView == 'visible'}">
          				<li>
                        	<a href="./customerlist">Customer View</a>
                        	<span class="icon-thumbnail"><i class="fa fa-eye"></i></span>
                     	</li>
                    </c:if>
           
           
           
                     <li>
                        <a href="./sitelist">Site View</a>
                        <span class="icon-thumbnail"><i class="fa fa-map-marker"></i></span>
                     </li>
                     
                  </ul>
               </li>
              
           </c:if>
           
            <c:if test="${access.analytics == 'visible'}">
           <li>
                  <a href="#">
                  <span class="title">Analytics</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-area-chart"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.portfolioManagement == 'visible'}">
          <li>
                  <a href="#">
                  <span class="title">Portfolio Manager</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-briefcase"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.ticketing == 'visible'}">
            <li>
                    <a href="javascript:;"><span class="title">Operation & Maintenance</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail" style="padding-top:5px !important;"><img src="resources/img/maintance.png"></span>
                  <ul class="sub-menu">
                       
                    <c:if test="${access.customerListView == 'visible'}">
          				<li>
                        	<a href="./ticketdetails">Tickets</a>
                        	<span class="icon-thumbnail"><i class="fa fa-ticket"></i></span>
                     	</li>
                     	
                    </c:if>
                     <li>
                        	<a href="./eventdetails">Events</a>
                        	<span class="icon-thumbnail"><i class="fa fa-calendar"></i></span>
                     	</li>
                 
                  </ul>
                    
                  
           
               </li>
           
           
           
           
           
           </c:if>
           
            <c:if test="${access.forcasting == 'visible'}">
          <li>
                  <a href="#"><span class="title">Forecasting</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-desktop"></i></span>
               </li>
           </c:if>
           
           	<c:if test="${access.analysis == 'visible'}">
          		<li>
                  <a href="./analysis">Analysis</a>
                  <span class="icon-thumbnail"><i class="fa fa-area-chart"></i> </span>
               </li>
           </c:if>
           
           
            <c:if test="${access.configuration == 'visible'}">
            
               <li>
                  <a href="javascript:;"><span class="title">Configuration</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="pg-tables"></i></span>
                  <ul class="sub-menu">
                     
                     <li>
                        <a href="./customers">Customer Config.</a>
                        <span class="icon-thumbnail">CU</span>
                     </li>
                     
                     <li>
                        <a href="./sites">Site Config.</a>
                        <span class="icon-thumbnail">SI</span>
                     </li>
                     
                     <li>
                        <a href="./equipments">Equipment Config.</a>
                        <span class="icon-thumbnail">EQ</span>
                     </li>
                     
                     <li>
                        <a href="#">Data Logger Config.</a>
                        <span class="icon-thumbnail">DL</span>
                     </li>
                     
                     <li>
                        <a href="#">Equ-Attrib Config.</a>
                        <span class="icon-thumbnail">EA</span>
                     </li>
                     
                      <li>
                        <a href="#">Configuration Loader</a>
                        <span class="icon-thumbnail">CL</span>
                     </li>
                     
                     <li>
                        <a href="./activities">Activity Config.</a>
                        <span class="icon-thumbnail">AC</span>
                     </li>
                     
                      <li>
                        <a href="./timezones">Timezone Config.</a>
                        <span class="icon-thumbnail">TZ</span>
                     </li>
                     
                      <li>
                        <a href="./currencies">Currency Config.</a>
                        <span class="icon-thumbnail">CY</span>
                     </li>
                     
                      <li>
                        <a href="./unitofmeasurements">Unit Measurement Config.</a>
                        <span class="icon-thumbnail">UM</span>
                     </li>
                     
                     
                     <li>
                        <a href="./countryregions">Country Region Config.</a>
                        <span class="icon-thumbnail">CR</span>
                     </li>
                     
                     <li>
                        <a href="./countries">Country Config.</a>
                        <span class="icon-thumbnail">CO</span>
                     </li>
                     
                     <li>
                        <a href="./states">State Config.</a>
                        <span class="icon-thumbnail">SE</span>
                     </li>
                     
                     <li>
                        <a href="./customertypes">Customer Type Config.</a>
                        <span class="icon-thumbnail">CT</span>
                     </li>
                     
                      <li>
                        <a href="./sitetypes">Site Type Config.</a>
                        <span class="icon-thumbnail">ST</span>
                     </li>
                     
                      <li>
                        <a href="./equipmentcategories">Equ Category Config.</a>
                        <span class="icon-thumbnail">EC</span>
                     </li>
                     
                      <li>
                        <a href="./equipmenttypes">Equ Type Config.</a>
                        <span class="icon-thumbnail">ET</span>
                     </li>
                     
                      <li>
                        <a href="#">Event Type Config.</a>
                        <span class="icon-thumbnail">EY</span>
                     </li>
                     
                     <li>
                        <a href="#">Event Config.</a>
                        <span class="icon-thumbnail">EV</span>
                     </li>
                     
                      <li>
                        <a href="#">Inspection Config.</a>
                        <span class="icon-thumbnail">IN</span>
                     </li>
                     
                      <li>
                        <a href="#">User Role Config.</a>
                        <span class="icon-thumbnail">UR</span>
                     </li>
                     
                      <li>
                        <a href="#">User Config.</a>
                        <span class="icon-thumbnail">US</span>
                     </li>
                     
                  </ul>
               </li>
          
           </c:if>

            </ul>
         
         
          </c:if>
          
          
          
          
            <div class="clearfix"></div>
         </div>
    
    
    
      </nav>
  
    
     <div class="page-container">
         <div class="header">
            <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="sidebar">
            </a>
            <div>
               <div class="brand inline">
                  <a href="./dashboard">
                  	<img src="resources/img/logo01.png" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management" class="logos">
                  </a>
               </div>
            </div>
            <div class="d-flex align-items-center">
            <div class="form-group required field searchbx" id="SearcSElect">
                          
                           <div class="ui loading search selection  dropdown  width oveallsearch">
                                         <input id="myInput" name="tags" type="text">

                                         <div class="default text">search</div>
                                         <i class="dropdown icon"></i>
                                         <div id="myDropdown" class="menu">

                                                <c:if test="${not empty access}">
                                                      <!--  <div class="item">Dashboard</div> -->

						       							<c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Customer View</div>
                                                       </c:if>
                                                       
                                                       <div class="item">Site View</div>

                                                       <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Tickets</div>
                                                       </c:if>
                                                       
                                                       <div class="item">Events</div>
                                                       
                                                       <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Analysis</div>
                                                       </c:if>
                                                       
                                                       <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Site Configuration</div>
                                                       </c:if>

                                                       <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Equipment Configuration</div>
                                                       </c:if>

                                                       <c:if test="${access.customerListView == 'visible'}">
                                                              <div class="item">Customer Configuration</div>
                                                       </c:if>

                                                       <c:if test="${access.customerListView == 'visible'}">
                                                             <div class="item">User Configuration</div>
                                                       </c:if>


                                                </c:if>


                                         </div>
                                  </div>
                          
                          
                          
                        </div>
               <!-- <div class="pull-left p-t-8 fs-14 font-heading hidden-md-down">
                  <input type="text" placeholder="search..." class="searchbox">
               </div> -->
                <!-- <div class="m-l-15">
               		<p class="center mb-1 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img src="resources/img/tkt.png"></p>
               		<p class="create-tkts">New Ticket</p>
               </div> -->
               <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user_01.png" width="32" height="32">
                  </span>
                  
                   <c:if test="${not empty access}">         
                              <p class="user-login">${access.userName}</p>
                  </c:if>
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="login" class="dropdown-item color logout"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
               
               
               
                <c:if test="${access.customerListView == 'visible'}">
          				 <div>
               				<p class="center mb-1 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img src="resources/img/tkt.png"></p>
               				<p class="create-tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false">New Ticket</p>
               			</div>
               
               	</c:if>
             <!-- <a href="#" class="header-icon  btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><i class="fa fa-ticket fa-2x"></i></a><br> -->
               <!-- <p class="user-login">Create Ticket</p> -->      
              <!--  <a href="#" class="header-icon pg pg-alt_menu btn-link  sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a> -->
            </div>
         </div>
         <div class="page-content-wrapper ">

         <c:if test="${not empty dashboard}">
         
            <div class="content" id="QuickLinkWrapper1">
               
               <div class="container-fixed-lg">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a><i class="fa fa-home mt-7"></i></a></li>
                     <li class="breadcrumb-item active">Dashboard</li>
                  </ol>
               </div>
               
               <div class=" container-fluid   container-fixed-lg padd-0">
                  <div class="card card-transparent ">                    
                     <!-- Content Start-->
                     <div class="sortable">
                     	<!-- <div class="row"> -->
                     		<div class="col-md-12 padd-0">
                           <div class="col-sm-12 col-md-3 col-lg-3  col-xl-12">
                              <div class="card card-default bg-default slide-left" id="allsites" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">total sites</div>
                                    <div class="card-title pull-right"><i class="fa fa-globe font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block">
                                    <h3 class="m-t-10">
                                       <span class="semi-bold"><strong>${dashboard.totalSiteCount}</strong></span>
                                    </h3>
                                    
                                    <div class="col-md-12 padd-0">
                                    		<div class="col-md-3 col-xs-4 padd-0">
                                    			<p>Rooftop</p>
                                    		</div>
                                    		<div class="col-md-1 col-xs-2 padd-0">
                                    			<p>-</p>
                                    		</div>
                                    		<div class="col-md-7 col-xs-6 padd-0">
                                    			<p>${dashboard.rooftopCount}</p>
                                    		</div>
                                    	</div>
                                    	<div class="col-md-12 padd-0 uitlity m-t-10">
                                    		<div class="col-md-3 col-xs-4 padd-0">
                                    			<p>Utility</p>
                                    		</div>
                                    		<div class="col-md-1 col-xs-2 padd-0">
                                    			<p>-</p>
                                    		</div>
                                    		<div class="col-md-7 col-xs-6 padd-0">
                                    			<p>${dashboard.utilityCount}</p>
                                    		</div>
                                    	</div>
                                  <%--   <p>Rooftop - ${dashboard.rooftopCount} </p> --%>
                                   <%--  <p>Utility - ${dashboard.utilityCount} </p> --%>
                                    <!-- <p>Rooftop & Utility - ${dashboard.rooftopUtilityCount} </p> -->
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper2">
                               <p>Last Down Time : ${dashboard.lastDownTime}</p> 
                                 </div>
                              </div>

                              <div class="card card-default bg-default slide-left" id="energy" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">today's energy</div>
                                    <div class="card-title pull-right"><i class="fa fa-bolt font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block">
                                    <h3 class="m-t-10">
                                       <span class="semi-bold"><strong>${dashboard.todayEnergy}</strong></span>
                                    </h3>
                                    	<div class="col-md-12 padd-0">
                                    		<div class="col-md-4 col-xs-4 padd-0">
                                    			<p>Last Checked</p>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<p>:</p>
                                    		</div>
                                    		<div class="col-md-6 col-xs-5 padd-0">
                                    			<p>${dashboard.lastUpdatedTime}</p>
                                    		</div>
                                    	</div>
                                    	<div class="col-md-12 padd-0 uitlity m-t-10">
                                    		<div class="col-md-4 col-xs-4 padd-0">
                                    			<p>Data Received</p>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<p>:</p>
                                    		</div>
                                    		<div class="col-md-6 col-xs-5 padd-0">
                                    			<p>${dashboard.eventTime}</p>
                                    		</div>
                                    	</div>
                                    	<div class="col-md-12 padd-0 uitlity m-t-10">
                                    		<div class="col-md-4 col-xs-4 padd-0">
                                    			<p>Total Energy</p>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<p>:</p>
                                    		</div>
                                    		<div class="col-md-6 col-xs-5 padd-0">
                                    			<p>${dashboard.totalEnergy}</p>
                                    		</div>
                                    	</div>
                                     <%-- <p>Event Time    : ${dashboard.lastUpdatedTime}</p> --%>
                                     <%-- <p>File Received : ${dashboard.eventTime}</p>
                                     <p>Total Energy  : ${dashboard.totalEnergy}</p> --%>
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper3">
                                    <p>Production Date : ${dashboard.productionDate}</p>
                                 </div>
                              </div>

                              <div class="card card-default bg-default slide-left" id="avoid" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">CO<sub>2</sub> Avoided</div>
                                    <div class="card-title pull-right"><img src="resources/img/ImageResize_01.png"></div>
                                 </div>
                                 <div class="card-block">
                                    <h3 class="m-t-10">
                                       <span class="semi-bold"><strong>${dashboard.todayCo2Avoided}</strong></span>
                                    </h3>
                                    <p>Today Emission</p>
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper2">
                                    <p>Total : ${dashboard.totalCo2Avoided}</p>
                                 </div>
                              </div>

                           </div>
                           <div class="col-lg-6 col-sm-12 col-md-6 col-xl-12" id="maps">
                                  <div class="card card-default bg-default" data-pages="card">
                                 <div class="map-padd card-block">
                                    <!-- <div id="map" class="map"></div> -->
                                    <div id="dvMap" class="map"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-3">
                              <div class="card card-default bg-default slide-right" id="sitestatus" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header  header-top">
                                    <div class="card-title pull-left m-t-5">Site Status</div>
                                    <div class="card-title pull-right"><i class="fa fa-sitemap font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block">
                                 <div class="col-md-12 col-xs-12 padd-0">
                                    		<div class="col-md-5 col-xs-7 padd-0">
                                    			 <span class="normal"></span>
                                    			 <span class="nortxt"> Active Sites  </span>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<span class="nortxt"> - </span>
                                    		</div>
                                    		
                                    		<div class="col-md-3 col-xs-4 padd-0">
                                    			<span class="nortxt">${dashboard.activeSiteCount}</span>
                                    		</div>
                                    	</div>
                                    	<div class="col-md-12 col-xs-12 padd-0">
                                    		<div class="col-md-5 col-xs-7 padd-0">
                                    			 <span class="warn"></span>
                                    			 <span class="nortxt"> Warning Sites  </span>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<span class="nortxt"> - </span>
                                    		</div>
                                    		
                                    		<div class="col-md-3 col-xs-4 padd-0">
                                    			<span class="nortxt">${dashboard.warningSiteCount}</span>
                                    		</div>
                                    	</div>
                                    	<div class="col-md-12 col-xs-12 padd-0">
                                    		<div class="col-md-5 col-xs-7 padd-0">
                                    			 <span class="err"></span>
                                    			 <span class="nortxt"> Down Sites  </span>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<span class="nortxt"> - </span>
                                    		</div>
                                    		
                                    		<div class="col-md-3 col-xs-4 padd-0">
                                    			<span class="nortxt">${dashboard.downSiteCount}</span>
                                    		</div>
                                    	</div> 
                                    	<div class="col-md-12 padd-0">
                                    		<div class="col-md-5 col-xs-7 padd-0">
                                    			 <span class="off"></span>
                                    			 <span class="offtxt">Offline Sites  </span>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<span class="offtxt"> - </span>
                                    		</div>
                                    		
                                    		<div class="col-md-3 col-xs-4 padd-0">
                                    			<span class="offtxt">${dashboard.offlineSiteCount}</span>
                                    		</div>
                                    	</div>
                                    <%-- <div class="row">
                                       <div  class="data ml-20">
                                          <span class="normal"></span><span class="nortxt"> Active Sites - ${dashboard.activeSiteCount}</span>
                                       </div>
                                    </div> --%>
                                    <%-- <div class="row">
                                       <div  class="data ml-20">
                                          <span class="warn"></span><span class="warntxt"> Warning Sites - ${dashboard.warningSiteCount}</span>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div  class="data ml-20">
                                          <span class="err"></span><span class="errtxt"> Down Sites - ${dashboard.downSiteCount}</span>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div  class="data ml-20">
                                          <span class="off"></span><span class="offtxt"> Offline Sites - ${dashboard.offlineSiteCount}</span>
                                       </div>
                                    </div> --%>
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper2">
                                    <p><a href="#QuickLinkWrapper4">View all sites</a></p>
                                   
                                 </div>
                              </div>
                              <div class="card card-default bg-default slide-right" id="tickets" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">open tickets</div>
                                    <div class="card-title pull-right"><i class="fa fa-ticket font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block">
                                    <h3 class="m-t-10">
                                       <span class="semi-bold"><strong>${dashboard.openTicketCount}</strong></span>
                                    </h3>
                                    <p><strong>Operations & Maintenance</strong></p>
                                    
                                    
                                     <c:if test="${access.customerListView == 'visible'}">
          									<p class="m-t-10"><a><strong><a href="./ticketdetails">Click here</a></strong></a> to view ticket details</p>
                                 	 </c:if>
                    
                    
                                     
                                 
                                 
                                   <%--  <p>${dashboard.ticketMessages}<a><strong><a href="./ticketdetails">Click here</a></strong></a> to view ticket details</p> --%>
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper3">
                                    <p>Closed Tickets : ${dashboard.completedTicketCount}</p>
                                 </div>
                              </div>
                              <div class="card card-default bg-default slide-right" id="events" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">events</div>
                                    <div class="card-title pull-right"><i class="fa fa-calendar font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block m-t-10">
                                    <div class="col-md-12 padd-0">
                                       <div class="col-lg-6 col-xs-6 col-md-6 padd-0">
                                          <p>Today</p>
                                          <p>${dashboard.todayEventCount}</p>
                                       </div>
                                       <div class="col-lg-6 col-xs-6 col-md-6">
                                          <p>Total</p>
                                          <p>${dashboard.totalEventCount}</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper4">
                                     <p><a href="./eventdetails">View all events</a></p>
                                 </div>
                              </div>
                           </div>
                      <!--   </div> -->
                     	</div>
                        
                     </div>
                     <!-- Content End-->
                     
                     <!-- Table Start-->
                 <div class="row tbl-padd">
                     	<div class="col-md-12">                     	
                     		<div class="card card-default bg-default mb-80" id="allsites" data-pages="card" style="padding-top:0px !important;">                     		
                              <div class="card-header">
                                    <div class="card-title m-l-15 tbl-head">Site Details</div>         
                               </div>                                 
                              	<div class="table-responsive" style="padding-bottom:10px !important;">
                              		<table id="example" class="table table-striped table-bordered"  width="100%" cellspacing="0">
                                       <thead class="bg-color">
                                          <tr>                                          
                                          <c:if test="${access.customerListView == 'visible'}">
                                          <th style="width:9%;">Site Code</th>
          								</c:if>          			
                                            <th style="width:9%;">Site Ref.</th>
                                             <th style="width:13%;">Site Name</th>
                                             <th style="width:8%;">Status</th>
                                             <th style="width:15%;">Today Energy (kWh)</th>
                                             <th style="width:15%;">Total Energy (MWh)</th>
                                             <th>Specific Yield</th>
                                             <th>Inverters</th>
                                             <c:if test="${access.customerListView == 'visible'}">
                                          <th>Last Checked</th>
          								</c:if>
          								
          								
                                          </tr>
                                       </thead>
                                       <tbody>
                                       <c:forEach items="${dashboardSiteList}" var="dashboardsite">
                                                      <tr>
                                                      
                                                        <c:if test="${access.customerListView == 'visible'}">
                                           <td><a href="./siteview${dashboardsite.siteID}&ref=${access.userID}" class="newlink">${dashboardsite.siteCode}</a></td>
                                       					</c:if>
          								
                                                            <td><a href="./siteview${dashboardsite.siteID}&ref=${access.userID}" class="newlink">${dashboardsite.siteReference}</a></td>
                                                            <td><a href="./siteview${dashboardsite.siteID}&ref=${access.userID}" class="newlink">${dashboardsite.siteName}</a></td>
                                                            
                                                            
                                                            <c:choose>
                                                                        <c:when test="${dashboardsite.networkStatus=='0'}">
                                                                        <td><div class="offl"></div>&nbsp;Offline</td>
                                                                        </c:when>
                                                                        <c:when test="${dashboardsite.networkStatus=='1'}">
                                                                        <td><div class="act"></div>&nbsp;Active</td>
                                                                        </c:when>
                                                                        <c:when test="${dashboardsite.networkStatus=='3'}">
                                                                        <td><div class="erro"></div>&nbsp;Error</td>
                                                                        </c:when>
                                                                        <c:when test="${dashboardsite.networkStatus=='2'}">
                                                                        <td><div class="warn"></div>Warning</td>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                        <td><div class="offl"></div>Offline</td>
                                                                        </c:otherwise>
                                                                  </c:choose>
                                                            <td>${dashboardsite.todayEnergy}</td>
                                                            <td>${dashboardsite.totalEnergy}</td>
                                                            <td>${dashboardsite.performanceRatio}</td>
                                                            <td>${dashboardsite.inverters}</td>
                                                          
                                                            
                                                            
                                                            
                                                             
                                                        <c:if test="${access.customerListView == 'visible'}">
                                            <td>${dashboardsite.lastUpdate}</td>
          								</c:if>
                                                            </tr>
                                                      </c:forEach>
                                                      
                                                      
                                                     

                                       </tbody>
                                    </table>
                              	</div>
                              </div>
                     	</div>
                     	
               		 </div> 
                  		
                     <!-- Table End-->
                      
                    
                  </div>
               </div>
            </div>
         
         </c:if>
        
           <div class="container-fixed-lg footer">
               <div class="container-fluid copyright sm-text-center">
                  <p class="small no-margin pull-left sm-pull-reset">
                     <span class="hint-text">Copyright &copy; 2017.</span>
                     <span>INSPIRE CLEAN ENERGY.</span>
                     <span class="hint-text">All rights reserved. </span>
                  </p>
                  <p class="small no-margin pull-rhs sm-pull-reset rhs-pull">
                     <span class="hint-text">Powered by</span>
                     <span>MESTECH SERVICES PVT LTD</span>.
                  </p>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>

      <!-- Side Bar Content Start-->
      
      <input type="hidden" value="${dashboard.mapJsonData}" id="MapJsonId">
      
      <!-- Side Bar Content End-->
    
    
    
      <div class="quickview-wrapper  builder hidden-sm hidden-xs" id="builder">
         <div class="p-l-10 p-r-10 ">
            <a class="builder-close  pg-close"   href="#"></a>
            <a class="builder-toggle" data-toggle="quickview" data-toggle-element="#builder"><img src="resources/img/ImageResize_06.png"></a>
            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary" id="builderTabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tabLayouts" role="tab" aria-controls="home"><span>Quick Links</span></a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active " id="tabLayouts" role="tabcard">
                  <div class="scrollable">
                     <div class="p-r-50">
                        <div class="list">
                           <ul>
                              <li><a href="#QuickLinkWrapper1">Total Sites</a></li>
                              <li><a href="#QuickLinkWrapper2" id="energy">Energy</a></li>
                              <li><a href="#QuickLinkWrapper3" id="co2">Co<sub>2</sub> Avoided</a></li>
                              <li><a href="#QuickLinkWrapper1" id="maps">Map</a></li>
                              <li><a href="#QuickLinkWrapper1" id="sstatus">Site Status</a></li>
                              <li><a href="#QuickLinkWrapper2" id="tkts">Tickets</a></li>
                              <li><a href="#QuickLinkWrapper3" id="ents">Events</a></li>
                              <li><a href="#QuickLinkWrapper4">Site Details</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div>
       <script src="resources/js/pace.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.gotop.js"></script>
      <script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
      <script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
      <script src="resources/js/tether.min.js" type="text/javascript"></script>
      <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery-easy.js" type="text/javascript"></script>
      <script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.actual.min.js"></script>
      <script src="resources/js/jquery.scrollbar.min.js"></script>
      <script type="text/javascript" src="resources/js/select2.full.min.js"></script>
      <script type="text/javascript" src="resources/js/classie.js"></script>
      <script src="resources/js/switchery.min.js" type="text/javascript"></script>
      <script src="resources/js/pages.min.js"></script>
      <script src="resources/js/card.js" type="text/javascript"></script>
      <script src="resources/js/scripts.js" type="text/javascript"></script>
      <script src="resources/js/demo.js" type="text/javascript"></script>
      <script src="resources/js/jquery.easing.min.js"></script>
      <script src="resources/js/jquery.fadethis.js"></script>

       
      <script src="resources/js/jquery.dataTables.min.js" type="text/javascript" ></script>
      <script src="resources/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
      <script src="resources/js/semantic.min.js" type="text/javascript" ></script>
      <script src="resources/js/calendar.min.js" type="text/javascript" ></script>
      <script src="resources/js/jquery.selectlistactions.js"></script>

      <script src="https:/www.amcharts.com/lib/3/amcharts.js"></script>
     
      <script src="https://www.amcharts.com/lib/3/serial.js"></script>
      <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
      <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
      <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>


    <div id="gotop"></div>
         <!-- Address Popup Start !-->

<!-- Modal -->
<div id="tktCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog modal-top">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">New Ticket</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"><span class="errormess">*</span> All fields are mandatory</p>
				 </div>            
        		<form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form validation-form"> 
             
              <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Site</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width" id="ddlSite" name="ddlSite" path="siteID">
                        <form:option value="">Select</form:option>
                        <form:options  items="${siteList}" />
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Type</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width" id="ddlType" name="ddlType"  path="ticketType">
                        <form:option value="">Select</form:option>
                        <form:option value="Operation">Operation</form:option>
                        <form:option value="Maintenance">Maintenance</form:option>
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Category</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width category" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                         <form:option data-value="Operation" value="">Select</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                          <form:option data-value="Maintenance" value="">Select</form:option>
                    </form:select> 
                  </div>
                  
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                  <div class="col-md-4 col-xs-12"><label class="fields required m-t-10">Subject</label></div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<form:input  placeholder="Subject"  name="Subject"  type="text"  id="txtSubject" path="ticketDetail" maxLength="50"/>
                  	</div>
                  	</div>
               </div> 
              
              <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Priority</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width" id="ddlPriority" name="user" path="priority">
                        <form:option value="">Select </form:option>
                        <form:option data-value="3" value="3">High</form:option>
                        <form:option data-value="2" value="2">Medium</form:option>
                        <form:option data-value="1" value="1">Low</form:option>
                    </form:select> 
                  </div>
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Description</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<textarea id="txtTktdescription"  name="description" style="resize: none;" placeholder="Ticket Description" maxLength="120"></textarea>
                  	</div>
                    
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
				       <div class="btn btn-success  submit">Create</div>
				        <div class="btn btn-primary clear m-l-15">Reset</div>
				  </div>
  <!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
  <!-- <div class="ui error message"></div> -->
</form:form>
				<!-- <div class="ui error message"></div> -->
				
	  		</div>
	  </div>
    </div>
<!-- Address Popup End !--> 
      <script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
      <script>
         $(document).ready(function() {
            $(window).fadeThis({
               speed: 500,
            });
         });
      </script>
  
       

  </body>
</html>


