<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet"
	rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/bootstrap.min.3.37.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="resources/css/semantic.min.css">

<link href="resources/css/loader.css" rel="stylesheet" type="text/css" />
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script src="resources/js/CustomeScript.js" type="text/javascript"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>
<!--  <script
	src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.7.7/xlsx.core.min.js"></script>  -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.14.1/xlsx.core.min.js"></script>


<script
	src="https://cdnjs.cloudflare.com/ajax/libs/xls/0.7.4-a/xls.core.min.js"></script>
<script src="resources/js/bootstrap-filestyle.min.js"
	type="text/javascript"></script>


<script type="text/javascript" src="resources/js/toastr.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/toastr.css">
<link rel="stylesheet" type="text/css"
	href="http://codeseven.github.com/toastr/toastr.css">


<script type="text/javascript">
	function AjaxCallForSaveOrUpdate(myjson) {

		$.ajax({
			type : 'POST',
			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			url : './newbulkdata',
			data : myjson,
			success : function(msg) {

				var errormessage = msg.Errorlog1;
				var errormessage1 = msg.Errorlog2;

				if (errormessage != null && errormessage != "") {

					toastr.error(errormessage);
				}
				if (errormessage1 != null && errormessage1 != "") {

					toastr.error(errormessage1);
				}

			},
			error : function(msg) {
				alert(fail);
			}
		});
	}
</script>
<script type="text/javascript">
	$(window).load(function() {
		$('.ui.dropdown').dropdown({
			forceSelection : false
		});

	});
	$(document)
			.ready(
					function() {
						
						
						var classstate = $('#hdnstate').val();

						$("#configurationid").addClass(classstate);
						$("#soppushconfigid").addClass(classstate);

						/* 	$('.for-delete').draggable({
								  revert: "invalid",
								  stack: ".for-delete"
								
								});
							
							$('.work-img-stl').draggable({
								  revert: "invalid",
								  stack: ".work-img-stl",
								  helper: 'clone'
								});
							
								$('#exampletb td').droppable({
								  accept: ".work-img-stl",
								  drop: function(event, ui) {
								    var droppable = $(this);
								    var draggable = ui.draggable;
							
								    var drag = $(droppable).has(ui.draggable).length ? draggable : draggable.clone().draggable({
								    
								    	 revert: "invalid",
										  stack: ".for-delete"
								    });
								    droppable.empty();
								    
								   drag= drag.removeClass('work-img-stl');
								    console.log(drag);
								    console.log(droppable);
								    drag.appendTo(droppable);
								    
								    
								    draggable.css({
								     
								    });
								  }
								});
							
								$('.delete-work-area').droppable({
									  accept: ".for-delete",
									  drop: function(event, ui) {
									    var droppable = $(this);
									    var draggable = ui.draggable;
									    
									  }
									});
						 */

						$(document)
								.on(
										'click',
										'.add-tbl-row',
										function() {
											
											if($(".tbody-area tr.placeholder-row").length){
												toastr.warning("Select Type and Category");
												return;
											}
											
											var num = $(".tbody-area tr").length + 1;
											$(".tbody-area")
													.append(
															"<tr><td>"
																	+ num
																	+ "</td><td contenteditable='true'></td><td contenteditable='true'></td><td contenteditable='true'></td><td class='work-frame'></td><td class='work-frame'></td><td class='work-frame'></td><td class='work-frame'></td></tr>");

										});
						$(document).on('click', '.del-tbl-row', function() {

							$(".tbody-area tr:last").remove();

						});

						$(document).on('click', '.work-frame', function() {

							$(".work-frame").removeClass('work-active');

							$(this).addClass('work-active');

						});
						
						

						$(document).on(
								'click',
								'.work-branch .work-img-stl',
								function() {
									$(".work-active").empty();
									$(".work-active").append(
											$(this).clone().removeClass(
													'.work-img-stl'));

									$(".work-active").removeClass('work-active');
								});

						$(document).on('click', '.work-delete-btn', function() {
							$(".work-active").empty();
							$(".work-active").removeClass('work-active');
						});

						$(".theme-loader").hide();
						toastr.options = {
							"debug" : false,
							"positionClass" : "toast-bottom-right",
							"onclick" : null,
							"fadeIn" : 500,
							"fadeOut" : 100,
							"timeOut" : 2000,
							"extendedTimeOut" : 2000
						}

						

						$('#viewfile').click(function() {

							AjaxCallForSaveOrUpdate(myjson)
							alert("The paragraph was clicked.");
						});

						$('#tktCreation').hide();
						$('#ddlSite').dropdown('clear');
						$('#ddlType').dropdown('clear');
						$('#ddlCategory').dropdown('clear');
						$('#ddlPriority').dropdown('clear');
						$('#txtSubject').val('');
						$('#txtTktdescription').val('');

						$('#example').DataTable(
								{
									"lengthMenu" : [ [ 15, 25, 50, -1 ],
											[ 15, 25, 50, "All" ] ],
									"bLengthChange" : true,
								});
						/* 	$('#exampletb').DataTable({  
						}); */

						$('.close').click(function() {
							$('.clear').click();
							$('#tktCreation').hide();
							$('.clear').click();
							$('.category').dropdown();
							$('.SiteNames').dropdown();
						});

						$('#btnCreate').click(function() {
							if ($('#ddlCategory').val == "") {
								alert('Val Empty');
								$('.category ').addClass('error')
							}
						})

						$('.clear').click(function() {
							$('.search').val('');
						})

						$('#ddlType').change(function() {
							$('.category').dropdown('clear')
						});

						$("#searchSelect").change(
								function() {
									var value = $(
											'#searchSelect option:selected')
											.val();
									var uid = $('#hdneampmuserid').val();
									redirectbysearch(value, uid);
								});

						var validation = {
							txtSubject : {
								identifier : 'txtSubject',
								rules : [ {
									type : 'empty',
									prompt : 'Please enter a value'
								}, {
									type : 'maxLength[50]',
									prompt : 'Please enter a value'
								} ]
							},
							ddlSite : {
								identifier : 'ddlSite',
								rules : [ {
									type : 'empty',
									prompt : 'Please enter a value'
								} ]
							},
							ddlType : {
								identifier : 'ddlType',
								rules : [ {
									type : 'empty',
									prompt : 'Please enter a value'
								} ]
							},
							ddlCategory : {
								identifier : 'ddlCategory',
								rules : [ {
									type : 'empty',
									prompt : 'Please enter a value'
								} ]
							},
							ddlPriority : {
								identifier : 'ddlPriority',
								rules : [ {
									type : 'empty',
									prompt : 'Please select a dropdown value'
								} ]
							},
							description : {
								identifier : 'description',
								rules : [ {
									type : 'empty',
									prompt : 'Please Enter Description'
								} ]
							}
						};
						var settings = {
							onFailure : function() {
								return false;
							},
							onSuccess : function() {
								$('#btnCreate').hide();
								$('#btnCreateDummy').show()
								//$('#btnReset').attr("disabled", "disabled");   		   	
							}
						};
						$('.ui.form.validation-form')
								.form(validation, settings);

					});
</script>

<script type='text/javascript'>
	//<![CDATA[
	$(window)
			.load(
					function() {
						$.fn.dropdown.settings.selectOnKeydown = false;
						$.fn.dropdown.settings.forceSelection = false;

						$('.ui.dropdown').dropdown({
							fullTextSearch : true,
							forceSelection : false,
							selectOnKeydown : true,
							showOnFocus : true,
							on : "click"
						});

						$('.ui.dropdown.oveallsearch').dropdown({
							onChange : function(value, text, $selectedItem) {
								var uid = $('#hdneampmuserid').val();
								redirectbysearchvalue(value, uid);
							},
							fullTextSearch : true,
							forceSelection : false,
							selectOnKeydown : false,
							showOnFocus : true,
							on : "click"
						});

						var $ddlType = $('#ddlType'), $ddlCategory = $('#ddlCategory'), $options = $ddlCategory
								.find('option');
						$ddlType.on(
								'change',
								function() {
									$ddlCategory.html($options
											.filter('[data-value="'
													+ this.value + '"]'));
								}).trigger('change');
					})
</script>
<style type="text/css">
��.ui.fluid.search.selection.dropdown.width{
    height: 30px !important;
}
.work-active {
	background: #9fa6abba !important;
}
</style>
<style type="text/css">
/* .ui.form textarea:not([rows]) {
		    height: 8em;
		    min-height: 8em;
		    max-height: 24em;
		}

		.ui.form textarea:not([rows]) {
		    height: 4em;
		    min-height: 4em;
		    max-height: 24em;
		}
  
		  .input-sm, .form-horizontal .form-group-sm .form-control {
		    font-size: 13px;
		    min-height: 25px;
		    height: 32px;
		    padding: 8px 9px;
		} */
.work-active {
	background: #9fa6abba !important;
}

.work-branch .work-img-stl {
	margin-left: 30px;
}

:root { -
	-c: #e47c00; -
	-c-b: bisque; -
	-c-c: #ea7f00;
}

.selected {
	background-color: brown;
	color: #FFF;
}

#count-table td {
	padding: 3px;
	font-size: 14px;
}

.error {
	background: #ff0000;
}

.delete-rowval .fa {
	color: red;
	font-size: 23px;
}

.mtable td {
	border: 1px solid #ddd;
	font-size: 13px;
	white-space: nowrap;
	text-align: center;
	padding: 1px;
}

.mtable th {
	border: 1px solid #ddd;
	vertical-align: top;
	padding: 7px;
	    background-color: #818181;
	    color:white;
	    font-size: 12px;
}

</style>


</head>
<body class="fixed-header">

	<div class="theme-loader">
		<div class="loader-track">
			<div class="loader-bar"></div>
		</div>
	</div>


	<input type="hidden" value="${access.userID}" id="hdneampmuserid">

	<input type="hidden" value="${classtate}" id="hdnstate">

	<jsp:include page="slider.jsp" />


	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					<a href="./dashboard"><img src="resources/img/logo01.png"
						alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx" id="SearcSElect">


					<div class="ui  search selection  dropdown  width oveallsearch">
						<input id="myInput" name="tags" type="text">

						<div class="default text">search</div>
						<!--           <i class="dropdown icon"></i> -->
						<div id="myDropdown" class="menu">

							<jsp:include page="searchselect.jsp" />


						</div>
					</div>










				</div>
				<div class="dropdown pull-right hidden-md-down user-log">
					<button class="profile-dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 circular inline"> <img
							src="resources/img/user_01.png" alt
							data-src="resources/img/user_01.png"
							data-src-retina="resources/img/user_01.png" width="32"
							height="32">
						</span>

						<c:if test="${not empty access}">
							<p class="user-login">${access.userName}</p>
						</c:if>
					</button>
					<div class="dropdown-menu dropdown-menu-right profile-dropdown"
						role="menu">
						<a class="dropdown-item disablecolor"><i
							class="pg-settings_small"></i> Settings</a> <a
							class="dropdown-item disablecolor"><i class="pg-outdent"></i>
							Feedback</a> <a class="dropdown-item disablecolor"><i
							class="pg-signals"></i> Help</a> <a href="logout1"
							class="dropdown-item color logout"><i class="pg-power"></i>
							Logout</a>
					</div>
				</div>




				<div class="newticketpopup hidden">
					<p class="center mb-1 tkts" data-toggle="modal"
						data-target="#tktCreation" data-backdrop="static"
						data-keyboard="false">
						<img src="resources/img/tkt.png">
					</p>
					<p class="create-tkts" data-toggle="modal"
						data-target="#tktCreation" data-backdrop="static"
						data-keyboard="false">New Ticket</p>
				</div>

				<%-- <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user.png" width="32" height="32">
                  </span>
                  
                  
                   <c:if test="${not empty access}">         
                  		<p class="user-login">${access.userName}</p>
                  </c:if>
                  
                  
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="login" class="dropdown-item color"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
               <span ><p class="center m-t-5 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img class="m-t-5" src="resources/img/tkt.png"><p class="create-tkts">New Ticket</p></p></span>
                --%>

				<!--  <p class="create-tkts">New Ticket</p> -->
				<!-- <a href="#" class="header-icon  btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><i class="fa fa-ticket fa-2x"></i></a> -->
				<!-- <a href="#" class="header-icon pg pg-alt_menu btn-link  sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a> -->
			</div>
		</div>
		<div class="page-content-wrapper ">
			<div class="content" id="QuickLinkWrapper1">
				<div class="container-fixed-lg">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="./dashboard"> <i
								class="fa fa-home mt-7"></i></a></li>
						<li class="breadcrumb-item ">Configuration</li>
						<li class="breadcrumb-item active">Standard Operating
							Procedure</li>
					</ol>
				</div>
				<div class="container-fixed-lg padd-3">
					<div id="sitestatus" data-pages="card">
						<div class="containerItems">
							<!-- Table Start-->
							<div class="sortable" id="customerlist">
								<div class="col-md-12 padd-0">
									<div class="card" data-pages="card">
										<ul class="nav nav-tabs nav-tabs-fillup hidden-sm-down"
											data-init-reponsive-tabs="dropdownfx">
											<li class="nav-item"><a href="#" class="active"
												data-toggle="tab" data-target="#slide1"
												data-original-title="" title=""><span>Standard
														Operating Procedure</span></a></li>

										</ul>
										<div class="card card-transparent  padd-5"
											style="padding-top: 0px !important;">
											<div class="row">
												<div class="col-lg-12">
													<div class="col-lg-4">
														<div class="form-group">
															<label class="fields">Type </label> <select id="sop-ddltype"
																class="ui fluid search selection dropdown width">
																<option value="">Select</option>
									<option value="Operation">Operation</option>
									<option value="Maintenance">Maintenance</option>
								</select>
														</div>
													</div>
													<div class="col-lg-4 ">
														<div class="form-group">
															<label class="fields">Category</label> <select
																id="sop-ddlcategory"
																class="ui fluid search selection dropdown width">
															<option data-value="" value="">Select</option>
														
									<option data-value="Operation" value="Inverter Down">Inverter Down </option>
									<option data-value="Operation" value="Plant Down">Plant Down</option>
									<option data-value="Operation" value="Module Damages">Module Damages</option>
									<option data-value="Operation" value="Plumbing Damages">Plumbing Damages</option>
									<option data-value="Operation" value="Equipment Failure">Equipment Failure</option>
									<option data-value="Operation"
										value="Equipment Replacement">Equipment Replacement</option>
									<option data-value="Operation" value="Communication Issue">Communication Issue</option>
									<option data-value="Operation" value="String Down">String Down</option>
									<option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</option>
									<option data-value="Operation" value="Plant Trip">Plant Trip</option>
									<option data-value="Operation" value="">Select</option>
									<option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</option>
									<option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</option>
									<option data-value="Maintenance"
										value="DataLogger Cleaning">DataLogger Cleaning</option>
									<option data-value="Maintenance"
										value="String Current Measurement">String Current Measurement</option>
									<option data-value="Maintenance"
										value="Preventive Maintenance">Preventive Maintenance</option>

									<option data-value="Maintenance" value="Mechanical PM">Mechanical PM</option>
									<option data-value="Maintenance" value="Vegetation">Vegetation</option>
									<option data-value="Maintenance" value="Visual Inspection">Visual Inspection</option>
									<option data-value="Maintenance" value="JMR Visit">JMR Visit</option>
									<option data-value="Maintenance" value="">Select</option>
															</select>

														</div>

													</div>
													<div class="col-lg-4"></div>
												</div>
											</div>
											<!-- 		table area -------------------->
											<div class="row" style="margin-left: 0px; margin-top: 15px;">
												<div class="col-md-12">
													<div class="col-md-11" style="padding:0px;">
														<label>Actions</label>
													</div>
													<div class="col-md-1">
														<span class=" add-tbl-row"><img alt=""
															src="resources/img/work_operarion_add.png"></span> <span
															class="del-tbl-row"><img alt=""
															src="resources/img/work_operation_minus.png"></span>
													</div>
												</div>
												<div class="col-md-12" style="">
													<table id="exampletb" class="table  mtable"
														tblcolumnnames="" style="margin-top: 10px;">
														<tr>
															<th>Steps</th>
															<th>Work Instruction</th>
															<th>Work Instuction Info</th>
															<th>Remarks</th>
															<th>Option 1</th>
															<th>Option 2</th>
															<th>Option 3</th>
															<th>Option 4</th>
													
														</tr>
														</thead>
														<tbody class="tbody-area">
															<tr class="placeholder-row">
																<td colspan="8">Select Type and Category to view Data.</td>
																<!-- <td contenteditable='true'></td>
																<td contenteditable='true'></td>
																<td contenteditable='true'></td>
																<td class="work-frame"></td>
																<td class="work-frame"></td>
																<td class="work-frame"></td>
																<td class="work-frame"></td> -->
											
															</tr>
														</tbody>
													</table>
												</div>
											</div>
											<div class="row" style="margin-left: 0px; margin-top: 15px;">
												<div class="col-md-12">
													<label>Options List</label>
												</div>
												<!-- 	<div class="row"> -->
												<div class="col-md-12">
													<div class="col-md-8 work-branch"
														style="border: solid #818181; border-radius: 16px; border-width: 2px; padding: 3px;">

														<span class="work-img-stl" work-img-id="1"><img alt=""
															src="resources/img/image_work_1.png"></span> <span
															class="work-img-stl " work-img-id="2"><img alt=""
															src="resources/img/image_work_2.png"></span> <span
															class="work-img-stl" work-img-id="3"><img alt=""
															src="resources/img/image_work_3.png"></span> <span
															class="work-img-stl" work-img-id="4"><img alt=""
															src="resources/img/image_work_4.png"></span> <span
															class="work-img-stl" work-img-id="5"><img alt=""
															src="resources/img/image_work_5.png"></span> <span
															class="work-img-stl" work-img-id="6"><img alt=""
															src="resources/img/image_work_6.png"></span> <span
															class="work-img-stl" work-img-id="7"><img alt=""
															src="resources/img/image_work_7.png"></span> <span
															class="work-img-stl" work-img-id="8"><img alt=""
															src="resources/img/image_work_8.png"></span> <span
															class="work-img-stl" work-img-id="9"><img alt=""
															src="resources/img/image_work_9.png"></span> <span
															class="work-img-stl" work-img-id="10"><img alt=""
															src="resources/img/image_work_10.png"></span> <span
															class="work-img-stl" work-img-id="11"><img alt=""
															src="resources/img/image_work_11.png"></span>
													</div>
													<div class="col-md-3 delete-work-area" style="">

														<span class="btn work-delete-btn"
															style="height: 52px; width: 52px; border: solid #818181; border-radius: 16px; border-width: 2px;"><i
															class="fa fa-close"
															style="margin-top:30%; font-size: 25px;"></i></span> <input
															type="button" style="margin-left: 25px;" class="btn btn-success mbt-save" value="Save">
													</div>

												</div>

												<!-- border: solid red; margin-left: 20px; border-radius: 16px; border-width: 2px; height: 53px; text-align: center; padding-top: 15px; -->



												<!-- </div> -->
											</div>
										</div>


										<div class="row">
											<div class="col-md-12" style="overflow: auto;">
												<table id="exceltable" class="mtable" tblcolumnnames=""
													style="margin-top: 10px;">
												</table>
												<!-- <input type="button" id="validate" class="btn btn-primary"
														value="Validate" /> -->
												<!-- <table id="nodata" class="table table-bordered">  
                                             <tr><td>No Data Found</td></tr>
                                          </table> -->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Table End-->
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="container-fixed-lg footer">
		<div class="container-fluid copyright sm-text-center">
			<p class="small no-margin pull-left sm-pull-reset">
				<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
					CLEAN ENERGY.</span> <span class="hint-text">All rights reserved. </span>
			</p>
			<p class="small no-margin pull-rhs sm-pull-reset">
				<span class="hint-text">Powered by</span> <span>MESTECH
					SERVICES PVT LTD</span>.
			</p>
			<div class="clearfix"></div>
		</div>
	</div>
	</div>
	

	<!-- Side Bar Content Start-->

	<!-- Side Bar Content End-->
	<!--   
      <div class="quickview-wrapper  builder hidden-sm hidden-xs" id="builder">
         <div class="p-l-10 p-r-0 ">
            <a class="builder-close quickview-toggle pg-close"  href="#"></a>
            <a class="builder-toggle" data-toggle="quickview" data-toggle-element="#builder"><img src="resources/img/ImageResize_06.png"></a>
            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary" id="builderTabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tabLayouts" role="tab" aria-controls="home"><span>Quick Links</span></a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active " id="tabLayouts" role="tabcard">
                  <div class="scrollable">
                     <div class="p-r-50">
                        <div class="list">
                           <ul>
                              <li><a href="#QuickLinkWrapper1">Customer List dfdfds</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div> -->
	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>





	<div id="gotop"></div>

	<!-- Address Popup Start !-->
	<!-- Modal -->
	<div id="tktCreation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header Popup-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="resources/img/close.png">
					</button>
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<p class="modal-title tkt-title">
						<img src="resources/img/tkt.png"><span class="tkt-title">New
							Ticket</span>
					</p>
				</div>
				<div class="modal-body">
					<c:url var="addAction" value="/addnewticket"></c:url>
					<div class="col-md-12">
						<p class="tkt-mandat">
							<span class="errormess">*</span> All fields are mandatory
						</p>
					</div>
					<form:form action="${addAction}" modelAttribute="ticketcreation"
						class="ui form validation-form">

						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlSite" name="ddlSite" path="siteID"
									onchange="getEquipmentAgainstSite()">
									<form:option value="">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Equipment</label>
							</div>

							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width equipments"
									id="ddlEquipment" path="equipmentID" name="ddlEquipment">
									<option value="">Select</option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Type</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlType" name="ddlType" path="ticketType">
									<form:option value="">Select</form:option>
									<form:option value="Operation">Operation</form:option>
									<form:option value="Maintenance">Maintenance</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Category</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width category"
									id="ddlCategory" name="ddlCategory" path="ticketCategory">
									<form:option value="">Select </form:option>
									<form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
									<form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
									<form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
									<form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
									<form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
									<form:option data-value="Operation"
										value="Equipment Replacement">Equipment Replacement</form:option>
									<form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
									<form:option data-value="Operation" value="String Down">String Down</form:option>
									<form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
									<form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
									<form:option data-value="Operation" value="">Select</form:option>
									<form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
									<form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="DataLogger Cleaning">DataLogger Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="String Current Measurement">String Current Measurement</form:option>
									<form:option data-value="Maintenance"
										value="Preventive Maintenance">Preventive Maintenance</form:option>

									<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
									<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
									<form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
									<form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
									<form:option data-value="Maintenance" value="">Select</form:option>
								</form:select>
							</div>

						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Subject</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<form:input placeholder="Subject" name="Subject" type="text"
										autocomplete="off" id="txtSubject" path="ticketDetail"
										maxLength="50" />
								</div>
							</div>
						</div>

						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Priority</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlPriority" name="user" path="priority">
									<form:option value="">Select </form:option>
									<form:option data-value="3" value="3">High</form:option>
									<form:option data-value="2" value="2">Medium</form:option>
									<form:option data-value="1" value="1">Low</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Description</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<textarea id="txtTktdescription" autocomplete="off"
										name="description" style="resize: none;"
										placeholder="Ticket Description" maxLength="120"></textarea>
								</div>

							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
							<div class="btn btn-success  submit" id="btnCreate">Create</div>
							<button class="btn btn-success" type="button" id="btnCreateDummy"
								tabindex="7" style="display: none;">Create</button>
							<div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
						</div>
						<!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
						<!-- <div class="ui error message"></div> -->
					</form:form>
					<!-- <div class="ui error message"></div> -->

				</div>
			</div>
		</div>
	</div>
	<select id="template-dropdown" style="display: none; width: 100%">

		<option value="">Select</option>
		<option value="Active">Active</option>
		<option value="Inactive">Inactive</option>
	</select>
	<select id="template-dropdown-pri-sec"
		style="display: none; width: 100%">

		<option value="">Select</option>
		<option value="Primary">Primary</option>
		<option value="Secondary">Secondary</option>
	</select>
	<!-- Address Popup End !-->
	<script>
		$('#gotop').gotop({
			customHtml : '<i class="fa fa-angle-up fa-2x"></i>',
			bottom : '2em',
			right : '2em'
		});
	</script>
	<script>
		$(document).ready(function() {
			$(window).fadeThis({
				speed : 500,
			});

		});
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			var $ddltype = $('#sop-ddltype'), $ddlcategory = $('#sop-ddlcategory'), $options = $ddlcategory
			.find('option');
			$ddlcategory.html($options
					.filter('[data-value="'+'"]')).trigger('change');
	$ddltype.on(
			'change',
			function() {
				$ddlcategory.dropdown("clear");
				$ddlcategory.html($options
						.filter('[data-value="'
								+ this.value + '"]'));
			}).trigger('change');
			
	 $ddlcategory.on(
			'change',
			function() {
				if(!($("#sop-ddltype").val()) || !($("#sop-ddlcategory").val())){
					
					return;
				}
				
				var type=$("#sop-ddltype").val();
				var cat=$("#sop-ddlcategory").val();
				
				$.ajax({
					url : './list?typeId='+type+'&'+'category='+cat,
					type : 'GET',
					   contentType :'application/json; charset=utf-8',
		               dataType : 'json',
					success : function(msg) {
						if(msg){
						if (msg.status == "Success") {
						var data=msg.data;
						if(data.length){
							 $(".tbody-area").empty();
							for(var i=0;i<data.length;i++){
								
								
								var opt1 = $("<div />").append($(".work-branch [work-img-id="+data[i].option1+"]").clone()).html();
								var opt2 = $("<div />").append($(".work-branch [work-img-id="+data[i].option2+"]").clone()).html();
								var opt3 = $("<div />").append($(".work-branch [work-img-id="+data[i].option3+"]").clone()).html();
								var opt4 = $("<div />").append($(".work-branch [work-img-id="+data[i].option4+"]").clone()).html();
								
								/* var opt1 = $("<div />").append($("[work-img-id="+data[i].option1+"]").clone()).html();
								var opt2 = $("<div />").append($("[work-img-id="+data[i].option2+"]").clone()).html();
								var opt3 = $("<div />").append($("[work-img-id="+data[i].option3+"]").clone()).html();
								var opt4 = $("<div />").append($("[work-img-id="+data[i].option4+"]").clone()).html(); */
								
								 $(".tbody-area").append("<tr><td>"+data[i].steps+"</td><td contenteditable='true'>"+data[i].workInstruction+"</td><td contenteditable='true'>"+data[i].workInstructionInfo+"</td><td contenteditable='true'>"+data[i].remarks+"</td><td class='work-frame'>"+opt1+"</td><td class='work-frame'>"+opt2+"</td><td class='work-frame'>"+opt3+"</td><td class='work-frame'>"+opt4+"</td></tr>");
									
							}
							
							
						}else{
							$(".tbody-area").empty();
							$(".add-tbl-row").trigger("click");
							toastr.warning("No Data Found");
						}
							
							  
						} else {
						}
						}
					},
					error : function(err) {

					}
				});
				
				
				
				
			});
			
			
			
			
			
			$(".mbt-save").on("click",function(){
				if(!($("#sop-ddltype").val()) || !($("#sop-ddlcategory").val())){
					toastr.error("Select Type and Category");
					return;
				}
				var flag=0;
				$(".tbody-area tr").each(function() {
			       if(!($.trim($(this).find("td:eq(1)").text()))){
			    	   flag=1;
			       }
			      
			});
				 if(flag==0){
			    	   save_work_operations_ajax();
			       }else{
			    	   toastr.error("Work Instruction not be empty");
			       }
			});
			
			
			function save_work_operations_ajax(){
				var object={};
				object.type=$("#sop-ddltype").val();
				object.category=$("#sop-ddlcategory").val();
				var arr=[];
				$(".tbody-area tr").each(function() {
					var iObj={};
					var opts=[4,5,6,7];
					iObj.steps=$.trim($(this).find("td:eq(0)").text());
					iObj.workInstruction=$.trim($(this).find("td:eq(1)").text());
					iObj.workInstructionInfo=$.trim($(this).find("td:eq(2)").text());
					iObj.remarks=$.trim($(this).find("td:eq(3)").text());
					
					if($(this).find("td:eq(4) span").length){
					
						iObj.option1=$(this).find("td:eq(4) span").attr("work-img-id");
					}else{
						iObj.option1="";
					}
					if($(this).find("td:eq(5) span").length){
						
						iObj.option2=$(this).find("td:eq(5) span").attr("work-img-id");
					}else{
						iObj.option2="";
					}
					if($(this).find("td:eq(6) span").length){
						
						iObj.option3=$(this).find("td:eq(6) span").attr("work-img-id");
					}else{
						iObj.option3="";
					}
					if($(this).find("td:eq(7) span").length){
						
						iObj.option4=$(this).find("td:eq(7) span").attr("work-img-id");
					}else{
						iObj.option4="";
					}
					/* $.each(opts,function(k,v){
						if($(this).find("td:eq("+v+") span").length){
							var j="option"+k;
							iObj.j=$(this).find("td:eq("+v+") span").attr("work-img-id");
						}else{
							iObj.j="";
						}
						console.log(k,v);
					}); */
					arr.push(iObj);
					
				
				});
				
				object.sopSubs=arr;
				console.log(object);
				
				
				  $.ajax({
                type: 'POST',
                 contentType :'application/json; charset=utf-8',
               dataType : 'json',
               url: './save',
               data: JSON.stringify(object),
               success: function(msg){
            	   if(msg.status=="Success"){
            		 
            		   $(".tbody-area").empty();
            		  /*  $(".add-tbl-row").trigger("click"); */
            		  $(".tbody-area").append('<tr class="placeholder-row"><td colspan="8">Select Type and Category to view Data.</td></tr>');
            		   $('#sop-ddltype').dropdown('clear');
            		   $('#sop-ddlcategory').dropdown('clear');
            		   
            		   toastr.success("Successcully uploaded");
            	   }else{	
            		   toastr.success("Upload Failed");
            	   }
            	  
                },
                error:function(msg) {
                	$(".theme-loader").hide();
                	toastr["error"]("Unfortunately some Error occured.", "Upload Failed"); 
                   }
}); 
				
				
			
				
			}
			
		});
	</script>
</html>

