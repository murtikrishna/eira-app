<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
                              <div class="card card-default bg-default slide-left" id="avoid" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">CO<sub>2</sub> Avoided</div>
                                    <div class="card-title pull-right"><img src="resources/img/ImageResize_04.png"></div>
                                 </div>
                                 <div class="card-block">
                                    <h3 class="m-t-10">
                                       <span class="semi-bold"><strong>${dashboard.todayCo2Avoided}</strong></span>
                                    </h3>
                                    <p>Today Emission</p>
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper2">
                                    <p>Total : ${dashboard.totalCo2Avoided}</p>
                                 </div>
                              </div>