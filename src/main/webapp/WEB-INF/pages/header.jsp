
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/loader.css" rel="stylesheet" type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet"
	rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/bootstrap.min3.3.37.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min3.css" type="text/css"
	rel="stylesheet">

<link rel="stylesheet" type="text/css"
	href="resources/css/jquery-ui.css">
<link href="resources/css/formValidation.min.css" rel="stylesheet"
	type="text/css">
<!--  <link href="resources/css/jquery-ui_cal.css" rel="stylesheet" type="text/css"> -->

<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>

<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script type="text/javascript" src="resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="resources/js/formValidation.min.js"></script>
<script type="text/javascript"
	src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.val.js"></script>


<script type="text/javascript" src="resources/js/toastr.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/toastr.css">
<link rel="stylesheet" type="text/css"
	href="http://codeseven.github.com/toastr/toastr.css">
<script src="resources/js/eventschartscript.js" type="text/javascript"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="resources/js/highcharts-3d.js"></script>
<script src="resources/js/exporting.js"></script>
<script src="resources/js/export-data.js"></script>
<!--  High Chart -->
<!-- <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
 -->






<style type="text/css">
.dropdown-menu-right {
	left: -70px !important;
}

.table-responsive {
	overflow-y: hidden;
}

.evttxtColor {
	color: #705b88;
}

.btnDeleteRecord {
	cursor: pointer;
}

.ui.search.dropdown .menu {
	max-height: 12.02857143rem;
}

#example_info, #example_paginate {
	padding-top: 20px;
}

div.dataTables_wrapper div.dataTables_length select {
	width: 50px;
	display: inline-block;
	margin-left: 3px;
	margin-right: 4px;
}

.ui








.form




 




textarea








:not




 




(
[
rows
]




 




)
{
height








:




 




4
em








;
min-height








:




 




4
em








;
max-height








:




 




24
em








;
}
.ui.icon.input>i.icon:before, .ui.icon.input>i.icon:after {
	left: 0;
	position: absolute;
	text-align: center;
	top: 36%;
	width: 100%;
}

.evtDropdown {
	/* min-height: 2.7142em !important; 
font-size:13px !important;
padding: 0.78571429em 2.1em 0.78571429em 1em !important;  */
	
}

.input-sm, .form-horizontal .form-group-sm .form-control {
	font-size: 13px;
	min-height: 25px;
	height: 32px;
	padding: 8px 9px;
}

#example td a {
	color: #337ab7;
	font-weight: bold;
}

.dt-button-background {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 0 !important;
}

.form-control {
	font-size: 12px;
}

.dropevent {
	min-width: 100px;
	margin-left: -35px;
}

.dropevent li a {
	color: #000 !important;
	font-weight: normal !important;
}

.tbh-width {
	width: 150px;
	padding-right: 30px !important;
}

.dt-buttons {
	margin-left: 50px;
}

.buttons-collection {
	height: 28px;
	padding: 3px;
	/*  z-index:9999; */
}

.buttons-excel {
	background: rgba(36, 95, 68, 0.87);
	color: #FFF;
}

.buttons-pdf {
	background: rgb(98, 165, 96);
	color: #FFF;
}

.dataTables_filter {
	float: right;
}

.dataTables_length {
	float: left;
}

.ui.selection.dropdown {
	width: 100%;
	margin-top: 0.1em;
	/*  height: 2.3em; */
}

.ui.selection.dropdown .menu {
	width: 100%;
	white-space: normal !important;
}

.ui.selection.dropdown .menu .item:first-child {
	border-top: 1px solid #ddd !important;
	min-height: 2.8em;
}

.ui.selection.dropdown .text {
	text-overflow: ellipsis;
	white-space: nowrap;
	width: 90%;
}

.oveallSearch .text {
	/* color: #eae9e9 !important; */
	color: #a49c9c !important;
}

.ui.selection.dropdown .icon {
	text-align: right;
	margin-left: 7px !important;
}
</style>

</head>