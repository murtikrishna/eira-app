<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="card card-default bg-default slide-right"
											id="divsiteproductionyield" data-pages="card">
											<div class="h-150">
												<div class="card-header ">
													<div class="card-title pull-left">production yield</div>
													<div class="card-title pull-right">
														<i class="fa fa-area-chart font-size yield icon-color"></i><br>
														<p>Total</p>
													</div>
												</div>
												<div class="card-block">
													<h3>
														<span class="semi-bold"><strong>${siteview.totalProductionYeild}</strong></span>
													</h3>
													<p>Yesterday : ${siteview.yesterdayProductionYeild}</p></br>
													<p>Annual Yield : ${siteview.annaulayeild}</p>
												</div>
											</div>
											<div class="card-footer" id="QuickLinkWrapper3">
												<p>Today Yield : ${siteview.todayProductionYeild}</p>
											</div>
										</div>