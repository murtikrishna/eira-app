 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 <div class="card card-default bg-default slide-right" id="sitestatus" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header  header-top">
                                    <div class="card-title pull-left m-t-5">Site Status</div>
                                    <div class="card-title pull-right"><i class="fa fa-sitemap font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block">
                                	<div class="col-md-12 padd-0">
                                		<div class="col-md-6 col-xs-6 padd-0">
                               <div id="sitenavActive" class="col-md-12 col-xs-12 padd-0 sitenavigation m-t-10"  >
                                 	<a href="#QuickLinkWrapper4">
                                 		<div class="col-md-8 col-xs-7 padd-0">
                                    			 <span class="normal m-t-5"></span>
                                    			 <span class="nortxt"> Active  </span>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<span class="nortxt"> - </span>
                                    		</div>
                                    		
                                    		<div class="col-md-3 col-xs-4 padd-0">
                                    			<span class="nortxt" id="ActiveCount">${customerview.activeSiteCount}</span>
                                    		</div>
                                 		</a>
                                    	</div> 
                                    	<div id="sitenavWarning" class="col-md-12 col-xs-12 padd-0 m-t-10 sitenavigation">
                                    		<a href="#QuickLinkWrapper4">
                                    		<div class="col-md-8 col-xs-7 padd-0">
                                    			 <span class="warn m-t-5"></span>
                                    			 <span class="nortxt"> Warning  </span>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<span class="nortxt"> - </span>
                                    		</div>
                                    		
                                    		<div class="col-md-3 col-xs-4 padd-0">
                                    			<span class="nortxt" id="WarningCount">${customerview.warningSiteCount}</span>
                                    		</div>
                                    		</a>
                                    	</div>
                                    	<div id="sitenavDown" class="col-md-12 col-xs-12 padd-0 m-t-10 sitenavigation">
                                    		<a href="#QuickLinkWrapper4">
                                    		<div class="col-md-8 col-xs-7 padd-0">
                                    			 <span class="err m-t-5"></span>
                                    			 <span class="nortxt"> Down  </span>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<span class="nortxt"> - </span>
                                    		</div>
                                    		
                                    		<div class="col-md-3 col-xs-4 padd-0">
                                    			<span class="nortxt" id="DownCount">${customerview.downSiteCount}</span>
                                    		</div>
                                    		</a>
                                    	</div> 
                                    	 <div id="sitenavOffline" class="col-md-12 padd-0 m-t-10 sitenavigation">
                                    		<a href="#QuickLinkWrapper4">
                                    		<div class="col-md-8 col-xs-7 padd-0">
                                    			 <span class="offl m-t-5"></span>
                                    			 <span class="offtxt">Offline </span>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<span class="offtxt m-l-10"> - </span>
                                    		</div>
                                    		
                                    		<div class="col-md-3 col-xs-4 padd-0">
                                    			<span class="offtxt" id="OfflineCount" style="margin-left:10px;"">${customerview.offlineSiteCount}</span>
                                    		</div>
                                    		</a>
                                    	</div>
                                 </div>
                                 <div class="col-md-6 col-xs-6 padd-0">
                                	<!--  <div id="statuschart" style="height: 145px";width="180px"></div>  -->
                                		<div id="statuschart" style="min-width: 110%; height: 125px; max-width: 180px; margin: 0 auto"></div>
                                		
                                		</div>
                                		</div>
                                 
                              </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper2">
                                    <p id="sitenav" class="sitenavigation"><a href="#QuickLinkWrapper4">View all sites</a></p>
                                   
                                 </div>
                              </div>