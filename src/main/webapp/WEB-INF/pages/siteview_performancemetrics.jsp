<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="card card-default bg-default slide-right"
											id="divsiteperformancemetrics" data-pages="card">
											<div class="h-150">
												<div class="card-header ">
													<div class="card-title pull-left">Performance Metrics</div>
													<div class="card-title pull-right">
														<img src="resources/img/ImageResize_01.png"><br>
														<p class="center">Today</p>
													</div>
												</div>
												<div class="card-block">
													<h3>
														<span class="semi-bold"><strong>${siteview.todayPerformanceRatio}</strong></span>
													</h3>
													<!-- <p>Today </p> -->
													<p>Specific Yield</p>
												</div>
											</div>
											<div class="card-footer">
												<p>Plant Load Factor : ${siteview.loadFactor}</p>
												<%-- <p>Over all : ${siteview.totalPerformanceRatio}</p> --%>
											</div>
										</div>