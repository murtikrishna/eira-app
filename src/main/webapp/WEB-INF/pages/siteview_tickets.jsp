<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

										<div class="card card-default bg-default slide-right"
											id="divsitetickets" data-pages="card">
											<div class="h-150">
												<div class="card-header ">
													<div class="card-title pull-left">tickets</div>
													<div class="card-title pull-right">
														<i class="fa fa-ticket fa-3x icon-color"
															aria-hidden="true"></i>
													</div>
												</div>




												<c:choose>
													<c:when test="${access.monitoringView == 'visible'}">

														<div class="col-lg-12 col-md-12">
															
																<p>
																	<b>Contact</b> <br><span style="text-decoration:underline;color:blue;"><a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=support@eira.io">support@eira.io</a></span>
														
																</p>


														</div>

													</c:when>
													<c:otherwise>
														<div class="card-block">
															<h3>

																<span class="semi-bold"><strong>${siteview.openTickets}</strong></span>


															</h3>
															<p>Open Tickets</p>
														</div>
													</c:otherwise>
												</c:choose>


											</div>


											<c:choose>
												<c:when test="${access.monitoringView == 'visible'}">

													<div class="card-footer" style="overflow: hidden";>
														<p style="float: left;"></p>

														<a style="float: right;" style="color:white" href=".">
															. </a>
													</div>

												</c:when>
												<c:otherwise>
													<div class="card-footer">
														<p style="float: left;">Closed Tickets :
															${siteview.completedTickets}</p>

														<a class="tickets-page hidden" style="float: right;"
															href="./siteticketskpi${siteview.siteID}">View all
															Tickets</a>
													</div>
												</c:otherwise>
											</c:choose>





										</div>