<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet"
	rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/bootstrap.min.3.37.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="resources/css/semantic.min.css">
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script src="resources/js/CustomeScript.js" type="text/javascript"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.7.7/xlsx.core.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/xls/0.7.4-a/xls.core.min.js"></script>
<script src="resources/js/bootstrap-filestyle.min.js"
	type="text/javascript"></script>


<script type="text/javascript" src="resources/js/toastr.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/toastr.css">
<link rel="stylesheet" type="text/css"
	href="http://codeseven.github.com/toastr/toastr.css">
<link
	href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css"
	rel="stylesheet">


<script type="text/javascript">
	
</script>
<style>
.toggle.ios, .toggle-on.ios, .toggle-off.ios {
	border-radius: 20px;
}

.toggle.ios .toggle-handle {
	border-radius: 20px;
}

.toggle-services {
	height: 20px
}
</style>
<script type="text/javascript">
	$(window).load(function() {
		$('.ui.dropdown').dropdown({
			forceSelection : false
		});

	});
	$(document).ready(
			function() {
				$.ajax({
					type : 'GET',
					url : './getAlldataloggertype',
					contentType : "application/json",
					dataType : "json",
					success : function(msg) {
						if (msg.data) {
							msg.data.forEach(function(item) {
								$('#mstatuslist').append(
										"<option value='"+item.dataloggertypeid+"'>"
												+ item.description
												+ "</option>");
							});
						}
					},
					error : function(msg) {
					}
				});

				$.ajax({
					type : 'GET',
					url : './getAllServiceCodes',
					contentType : "application/json",
					dataType : "json",
					success : function(msg) {
						console.log(msg.data);
						if (msg.data) {
							msg.data.forEach(function(item) {
								$('#finv-ext-logic').append(
										"<option value='"+item.servicecode+"'>"
												+ item.description
												+ "</option>");
								$('#fmod-ext-logic').append(
										"<option value='"+item.servicecode+"'>"
												+ item.description
												+ "</option>");
								$('#fsen-ext-logic').append(
										"<option value='"+item.servicecode+"'>"
												+ item.description
												+ "</option>");
								$('#fscb-ext-logic').append(
										"<option value='"+item.servicecode+"'>"
												+ item.description
												+ "</option>");
								$('#ftrack-ext-logic').append(
										"<option value='"+item.servicecode+"'>"
												+ item.description
												+ "</option>");
								$('#fenergy-ext-logic').append(
										"<option value='"+item.servicecode+"'>"
												+ item.description
												+ "</option>");

								$('#ainv-ext-logic').append(
										"<option value='"+item.servicecode+"'>"
												+ item.description
												+ "</option>");
								$('#asen-ext-logic').append(
										"<option value='"+item.servicecode+"'>"
												+ item.description
												+ "</option>");
								$('#aem-ext-logic').append(
										"<option value='"+item.servicecode+"'>"
												+ item.description
												+ "</option>");
								$('#ascb-ext-logic').append(
										"<option value='"+item.servicecode+"'>"
												+ item.description
												+ "</option>");

								$('#atra-ext-logic').append(
										"<option value='"+item.servicecode+"'>"
												+ item.description
												+ "</option>");

							});
						}
					},
					error : function(msg) {

					}
				});
				$.ajax({
					type : 'GET',
					url : './getAllDataLogger',
					contentType : "application/json",
					dataType : "json",
					success : function(msg) {
						console.log(msg.data);
						if (msg.data) {
							msg.data.forEach(function(item) {
								$('#finv-par-list').append(
										"<option value='"+item.dataloggerid+"'>"
												+ item.description
												+ "</option>");
								$('#fmod-par-list').append(
										"<option value='"+item.dataloggerid+"'>"
												+ item.description
												+ "</option>");
								$('#fsen-par-list').append(
										"<option value='"+item.dataloggerid+"'>"
												+ item.description
												+ "</option>");
								$('#fscb-par-list').append(
										"<option value='"+item.dataloggerid+"'>"
												+ item.description
												+ "</option>");
								$('#ftrack-par-list').append(
										"<option value='"+item.dataloggerid+"'>"
												+ item.description
												+ "</option>");
								$('#fenergy-par-list').append(
										"<option value='"+item.dataloggerid+"'>"
												+ item.description
												+ "</option>");


								$('#ainv-par-list-v').append(
										"<option value='"+item.dataloggerid+"'>"
												+ item.description
												+ "</option>");
								$('#asen-par-list-v').append(
										"<option value='"+item.dataloggerid+"'>"
												+ item.description
												+ "</option>");
								$('#aem-par-list-v').append(
										"<option value='"+item.dataloggerid+"'>"
												+ item.description
												+ "</option>");
								$('#ascb-par-list-v').append(
										"<option value='"+item.dataloggerid+"'>"
												+ item.description
												+ "</option>");
								$('#atra-par-list-v').append(
										"<option value='"+item.dataloggerid+"'>"
												+ item.description
												+ "</option>");

							});
						}
					},
					error : function(msg) {

					}
				});

				toastr.options = {
					"debug" : false,
					"positionClass" : "toast-bottom-right",
					"onclick" : null,
					"fadeIn" : 500,
					"fadeOut" : 100,
					"timeOut" : 2000,
					"extendedTimeOut" : 2000
				}

				$('#tktCreation').hide();
				$('#ddlSite').dropdown('clear');
				$('#ddlType').dropdown('clear');
				$('#ddlCategory').dropdown('clear');
				$('#ddlPriority').dropdown('clear');
				$('#txtSubject').val('');
				$('#txtTktdescription').val('');

				$('.close').click(function() {
					$('.clear').click();
					$('#tktCreation').hide();
					$('.clear').click();
					$('.category').dropdown();
					$('.SiteNames').dropdown();
				});

				$('#btnCreate').click(function() {
					if ($('#ddlCategory').val == "") {

						$('.category ').addClass('error')
					}
				})

				$('.clear').click(function() {
					$('.search').val('');
				})

				$('#ddlType').change(function() {
					$('.category').dropdown('clear')
				});

				$("#searchSelect").change(function() {
					var value = $('#searchSelect option:selected').val();
					var uid = $('#hdneampmuserid').val();
					redirectbysearch(value, uid);
				});

				var validation = {
					txtSubject : {
						identifier : 'txtSubject',
						rules : [ {
							type : 'empty',
							prompt : 'Please enter a value'
						}, {
							type : 'maxLength[50]',
							prompt : 'Please enter a value'
						} ]
					},
					ddlSite : {
						identifier : 'ddlSite',
						rules : [ {
							type : 'empty',
							prompt : 'Please enter a value'
						} ]
					},
					ddlType : {
						identifier : 'ddlType',
						rules : [ {
							type : 'empty',
							prompt : 'Please enter a value'
						} ]
					},
					ddlCategory : {
						identifier : 'ddlCategory',
						rules : [ {
							type : 'empty',
							prompt : 'Please enter a value'
						} ]
					},
					ddlPriority : {
						identifier : 'ddlPriority',
						rules : [ {
							type : 'empty',
							prompt : 'Please select a dropdown value'
						} ]
					},
					description : {
						identifier : 'description',
						rules : [ {
							type : 'empty',
							prompt : 'Please Enter Description'
						} ]
					}
				};
				var settings = {
					onFailure : function() {
						return false;
					},
					onSuccess : function() {
						$('#btnCreate').hide();
						$('#btnCreateDummy').show()
						//$('#btnReset').attr("disabled", "disabled");   		   	
					}
				};
				$('.ui.form.validation-form').form(validation, settings);

			});
</script>

<script type='text/javascript'>
	//<![CDATA[
	$(window)
			.load(
					function() {
						$.fn.dropdown.settings.selectOnKeydown = false;
						$.fn.dropdown.settings.forceSelection = false;

						$('.ui.dropdown').dropdown({
							fullTextSearch : true,
							forceSelection : false,
							selectOnKeydown : true,
							showOnFocus : true,
							on : "click"
						});

						$('.ui.dropdown.oveallsearch').dropdown({
							onChange : function(value, text, $selectedItem) {
								var uid = $('#hdneampmuserid').val();
								redirectbysearchvalue(value, uid);
							},
							fullTextSearch : true,
							forceSelection : false,
							selectOnKeydown : false,
							showOnFocus : true,
							on : "click"
						});

						var $ddlType = $('#ddlType'), $ddlCategory = $('#ddlCategory'), $options = $ddlCategory
								.find('option');
						$ddlType.on(
								'change',
								function() {
									$ddlCategory.html($options
											.filter('[data-value="'
													+ this.value + '"]'));
								}).trigger('change');

					})
</script>
<style type="text/css">
/* .ui.form textarea:not([rows]) {
		    height: 8em;
		    min-height: 8em;
		    max-height: 24em;
		}

		.ui.form textarea:not([rows]) {
		    height: 4em;
		    min-height: 4em;
		    max-height: 24em;
		}
  
		  .input-sm, .form-horizontal .form-group-sm .form-control {
		    font-size: 13px;
		    min-height: 25px;
		    height: 32px;
		    padding: 8px 9px;
		} */
:root { -
	-c: #e47c00; -
	-c-b: bisque; -
	-c-c: #ea7f00;
}

.selected {
	background-color: brown;
	color: #FFF;
}

#count-table td {
	padding: 3px;
	font-size: 14px;
}

.toggle.btn.btn-xs {
	width: 70px !important;
	height: 15px !important;
}

td>.toggle.btn.btn-xs {
	text-align: center;
}

.err-dropdown {
	border: 2px #ff6565 !important;
	border-style: solid !important;
}

.err-dropdown:hover:after {
	position: absolute;
	bottom: 30px;
	content: "Choose any One.";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #ec6c00;
	background-color: #ffeed3;
	border: 1px solid #e47c24;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
	left: 0px;
}

.err-textfield {
	border: 2px #ff6565 !important;
	border-style: solid !important;
}

.err-textfield:hover:after {
	position: absolute;
	bottom: 40px;
	content: "Not be Empty";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #ec6c00;
	background-color: #ffeed3;
	border: 1px solid #e47c24;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
	left: 10px
}

.err-row-text {
	border: 2px #ff6565 !important;
	border-style: solid !important;
}

.err-row-back {
	background: #ff8585;
}

.err-row-back:hover:after {
	position: absolute;
	bottom: 50px;
	content: "All four fields should not be empty";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #ec6c00;
	background-color: #ffeed3;
	border: 1px solid #e47c24;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
	left: 10px;
}

.stl-row-back {
	border-radius: 5px;
	padding-bottom: 3px;
}

#site-table_length label {
	width: fit-content;
	float: left;
}

#site-table_filter label {
	width: fit-content;
	float: right;
}

.fields:after {
	content: "*";
	color: red;
}
</style>



</head>
<body class="fixed-header">


	<input type="hidden" value="${access.userID}" id="hdneampmuserid">

	<input type="hidden" value="${classtate}" id="hdnstate">

	<jsp:include page="slider.jsp" />


	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					<a href="./dashboard"><img src="resources/img/logo01.png"
						alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx" id="SearcSElect">


					<div class="ui  search selection  dropdown  width oveallsearch">
						<input id="myInput" name="tags" type="text">

						<div class="default text">search</div>
						<!--           <i class="dropdown icon"></i> -->
						<div id="myDropdown" class="menu">

							<jsp:include page="searchselect.jsp" />


						</div>
					</div>










				</div>
				<div class="dropdown pull-right hidden-md-down user-log">
					<button class="profile-dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 circular inline"> <img
							src="resources/img/user_01.png" alt
							data-src="resources/img/user_01.png"
							data-src-retina="resources/img/user_01.png" width="32"
							height="32">
						</span>

						<c:if test="${not empty access}">
							<p class="user-login">${access.userName}</p>
						</c:if>
					</button>
					<div class="dropdown-menu dropdown-menu-right profile-dropdown"
						role="menu">
						<a class="dropdown-item disablecolor"><i
							class="pg-settings_small"></i> Settings</a> <a
							class="dropdown-item disablecolor"><i class="pg-outdent"></i>
							Feedback</a> <a class="dropdown-item disablecolor"><i
							class="pg-signals"></i> Help</a> <a href="logout1"
							class="dropdown-item color logout"><i class="pg-power"></i>
							Logout</a>
					</div>
				</div>



				<c:if test="${access.customerListView == 'visible'}">
					<div>
						<p class="center mb-1 tkts" data-toggle="modal"
							data-target="#tktCreation" data-backdrop="static"
							data-keyboard="false">
							<img src="resources/img/tkt.png">
						</p>
						<p class="create-tkts" data-toggle="modal"
							data-target="#tktCreation" data-backdrop="static"
							data-keyboard="false">New Ticket</p>
					</div>
				</c:if>
				<%-- <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user.png" width="32" height="32">
                  </span>
                  
                  
                   <c:if test="${not empty access}">         
                  		<p class="user-login">${access.userName}</p>
                  </c:if>
                  
                  
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="login" class="dropdown-item color"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
               <span ><p class="center m-t-5 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img class="m-t-5" src="resources/img/tkt.png"><p class="create-tkts">New Ticket</p></p></span>
                --%>

				<!--  <p class="create-tkts">New Ticket</p> -->
				<!-- <a href="#" class="header-icon  btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><i class="fa fa-ticket fa-2x"></i></a> -->
				<!-- <a href="#" class="header-icon pg pg-alt_menu btn-link  sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a> -->
			</div>
		</div>
		<div class="page-content-wrapper ">
			<div class="content" id="QuickLinkWrapper1">
				<div class="container-fixed-lg">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="./dashboard"><i
								class="fa fa-home mt-7"></i></a></li>
						<li class="breadcrumb-item"><a>Configuration</a></li>
						<li class="breadcrumb-item active">Data Collection</li>
					</ol>
				</div>
				<div class="container-fixed-lg padd-3">
					<div id="sitestatus" data-pages="card">
						<div class="containerItems">



							<!-- Table Start-->

							<div class="padd-3 sortable" id="equipmentdata">
								<div class="row">
									<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
										<div class="card card-default bg-default" data-pages="card">
											<div class="row">
												<div class="col-md-6">
													<p class="m-l-5 title-head">
														<b>Sites:</b>
													</p>
												</div>
												<div class="col-md-6"></div>
											</div>
											<div class="padd-5 table-responsive">
												<table id="site-table"
													class="table table-striped table-bordered" width="100%"
													cellspacing="0">
													<thead>
														<tr>
															<th>S.No.</th>
															<th>Site Code</th>
															<th>Site Name</th>
															<th>Operation Mode</th>
															<th>Energy Generation Based On</th>
															<th>Data Received Timezone</th>
															<th>Configure</th>
															<th class="hidden">SiteCapacity</th>
															<th class="hidden">CustomerReference</th>
														</tr>
													</thead>
													<tbody>
														<!-- 	<tr>
															<td>1</td>
															<td>SE00001</td>
															<td>India Food Park</td>
															<td>FTP</td>
															<td>Inverter</td>
															<td>UTC</td>
															<td><input class="to" type="checkbox" checked
																data-toggle="toggle" data-onstyle="success"
																data-offstyle="danger" data-size="small"></td>
															<td><input class="to" type="checkbox" checked
																data-toggle="toggle" data-onstyle="success"
																data-offstyle="danger" data-size="small"></td>
															<td><input class="to" type="checkbox" checked
																data-toggle="toggle" data-onstyle="success"
																data-offstyle="danger" data-size="mini"></td>
															<td><input type="button" class="btn btn-primary"
																value="configure"></td>

														</tr> -->

													</tbody>
												</table>


											</div>
										</div>
									</div>


								</div>

							</div>
							<!-- Table End-->

							<!-- Site Config Start-->
							<div class="sortable hidden" id="siteConfig">
								<div class="col-md-12 padd-0">
									<div class="card" data-pages="card">
										<div class="card-header p-l-0 padd-0 m-h-30">
											<div class="card-title pull-left tbl-head m-l-5">Site
												Config</div>
										</div>
										<div class="card card-transparent  padd-5"
											style="padding-top: 0px !important;">

											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-1 col-xs-12 m-t-5">Site Code</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<b><span id="siteCodeId"></span></b>
													</div>
													<div class="col-md-1 col-xs-12 m-t-5">Site Name</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<b><span id="siteNameId"></span></b>
													</div>
													<div class="col-md-1 col-xs-12 m-t-5">Site Capacity</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<b><span id="siteCapacityId"></span></b>
													</div>
													<div class="col-md-1 col-xs-12 m-t-5">Customer Ref.</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<b><span id="customerRefId"></span></b>
													</div>

												</div>

											</div>



											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-1 col-xs-12 m-t-5">
														<div class="txtwidth ">
															<label class="fields m-t-5">Mode</label>
														</div>
													</div>

													<div class="col-md-2 col-xs-12 m-t-5">
														<select class="ui search dropdown" id="mMode">
															<option value="">Select</option>
															<option value="FTP">FTP</option>
															<option value="API">API</option>
														</select>
													</div>
													<div class="col-md-1 col-xs-12 m-t-5">
														<div class="txtwidth ">
															<label class="fields m-t-5">Energy Gen. Based On
															</label>
														</div>
													</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<select
															class="ui fluid search selection dropdown width ddLPtype"
															name="ddlCategory" id="menergygbo">
															<option value="">Select</option>
															<option value="inverter">Inverter</option>
															<option value="energymeter">Energy Meter</option>

														</select>
													</div>
													<div class="col-md-1 col-xs-12 m-t-5">
														<div class="txtwidth ">
															<label class="fields m-t-5">Timezone </label>
														</div>
													</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<select
															class="ui fluid search selection dropdown width ddLPtype"
															name="ddlCategory" id="timeZoneSelectId">
															<option value="">Select</option>
														</select>
													</div>
													<div class="col-md-1 col-xs-12 m-t-5">
														<div class="txtwidth ">
															<label class="fields m-t-5">Today Energy Based On
															</label>
														</div>
													</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<select
															class="ui fluid search selection dropdown width ddLPtype"
															name="ddlCategory" id="mtodayebo">
															<option value="">Select</option>
															<option value="Today Energy">Today Energy</option>
															<option value="Total Energy">Total Energy</option>

														</select>
													</div>


												</div>

											</div>

											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-1 col-xs-12 m-t-5">
														<div class="txtwidth ">
															<label class="fields m-t-5">Status List</label>
														</div>
													</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<select
															class="ui fluid search selection dropdown width ddLPtype"
															name="ddlCategory" id="mstatuslist">
															<option value="">Select</option>


														</select>
													</div>
													<div class="col-md-1 col-xs-12 m-t-5">
														<div class="txtwidth ">
															<label class="fields m-t-5">Analysis Flag</label>
														</div>
													</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<select
															class="ui fluid search selection dropdown width ddLPtype"
															name="ddlCategory" id="prod-flag">
															<option value="">Select</option>
															<option value="0">Inactive</option>
															<option value="1">Active</option>


														</select>
													</div>
													<div class="col-md-6 col-xs-12 m-t-5">
														<div class="col-md-12" style="padding: 0px">
															<div class="col-md-4" style="padding: 0px;">
																<label>Extraction Service</label><span
																	style="margin-left: 10px;"><input
																	id="extraction-service-toggle" type="checkbox"
																	class="toggle-services" data-toggle="toggle"
																	data-onstyle="success" data-offstyle="danger"
																	data-style="ios" data-size="small"></span>
															</div>
															<div class="col-md-4" style="padding: 0px;">
																<label>Dump Service</label><span
																	style="margin-left: 10px;"><input
																	id="dump-service-toggle" type="checkbox"
																	class="toggle-services" data-toggle="toggle"
																	data-onstyle="success" data-offstyle="danger"
																	data-style="ios" data-size="small"></span>
															</div>
															<div class="col-md-4" style="padding: 0px;">
																<label>Curing Service</label><span
																	style="margin-left: 10px;"><input
																	id="curing-service-toggle" type="checkbox"
																	class="toggle-services" data-toggle="toggle"
																	data-onstyle="success" data-offstyle="danger"
																	data-style="ios" data-size="small"></span>
															</div>

														</div>

													</div>

												</div>



											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Site Config End-->


							<!-- Ftp config Start-->
							<div class="sortable hidden" id="ftpConfig">
								<div class="col-md-12 padd-0">
									<div class="card" data-pages="card">
										<div class="card-header p-l-0 padd-0 m-h-30">
											<div class="card-title pull-left tbl-head m-l-5">FTP
												Config</div>
										</div>
										<div class="card card-transparent  padd-5"
											style="padding-top: 0px !important;">
											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-1 col-xs-12 m-t-5">
														<div class="txtwidth ">
															<label class="fields m-t-5">File Type </label>
														</div>
													</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<select
															class="ui fluid search selection dropdown width ddLPtype"
															name="ddlCategory" id="ffiletype">
															<option value="">Select</option>
															<option value="CSV">CSV</option>
															<option value="XML">XML</option>
														</select>
													</div>
													<div class="col-md-1 col-xs-12 m-t-5">
														<div class="txtwidth ">
															<label class="fields m-t-5">FTP Home Directory </label>
														</div>
													</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<input id="fftphomedir" autocomplete="off"
															class="form-control" name="" placeholder="" type="text">
													</div>
													<div class="col-md-1 col-xs-12 m-t-5">
														<div class="txtwidth ">
															<label class="fields m-t-5">FTP User Name </label>
														</div>
													</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<input id="fftpusername" autocomplete="off"
															class="form-control" name="" placeholder="" type="text">
													</div>
													<div class="col-md-1 col-xs-12 m-t-5">
														<div class="txtwidth ">
															<label class="fields m-t-5">FTP Password </label>
														</div>
													</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<input id="fftppass" autocomplete="off"
															class="form-control" name="" placeholder="" type="text">
													</div>


												</div>

											</div>

											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class=" m-t-5"><b>Sub Folders </b></label>
														</div>
													</div>
													<div class="col-md-10">
														<div class="col-md-12">

															<div class="col-md-3 col-xs-12 m-t-5">
																<div class="txtwidth">
																	<label class=" m-t-5"><b>FTP Server Path </b></label>
																</div>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<div class="txtwidth">
																	<label class=" m-t-5"><b>Local Directory
																			Path </b></label>
																</div>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<div class="txtwidth">
																	<label class=" m-t-5"><b>Parameter List
																			Version </b></label>
																</div>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<div class="txtwidth">
																	<label class=" m-t-5"><b>Data Extraction
																			Logic </b></label>
																</div>
															</div>
														</div>
													</div>


												</div>

											</div>
											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class=" m-t-5">(eg:)</label>
														</div>
													</div>


													<div class="col-md-10">
														<div class="col-md-12">
															<div class="col-md-3 col-xs-12 m-t-5">
																<div class="txtwidth">
																	<label class=" m-t-5">\SERVERFOLDERPATH </label>
																</div>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<div class="txtwidth">
																	<label class=" m-t-5">\LOCALFOLDERPATH </label>
																</div>
															</div>
														</div>
													</div>

												</div>

											</div>
											<!-- table content -->
											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class="fields m-t-5">Inverter</label>
														</div>
													</div>


													<div class="col-md-10 ">
														<div class="col-md-12 stl-row-back">
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="finv-ser-path" autocomplete="off"
																	class="form-control finv-text " name="finv-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="finv-loc-path" autocomplete="off"
																	class="form-control finv-text " name="finv-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype finv-sel "
																	name="finv-flag" id="finv-par-list">
																	<option value="">Select</option>


																</select>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype finv-sel"
																	name="finv-flag" id="finv-ext-logic">
																	<option value="">Select</option>
																	<option value="9">All</option>

																</select>
															</div>
														</div>
													</div>

												</div>

											</div>
											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class="fields m-t-5">Modbus </label>
														</div>
													</div>

													<div class="col-md-10">
														<div class="col-md-12 stl-row-back">
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="fmod-ser-path" autocomplete="off"
																	class="form-control fmod-text " name="fmod-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="fmod-loc-path" autocomplete="off"
																	class="form-control fmod-text " name="fmod-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype fmod-sel "
																	name="fmod-flag" id="fmod-par-list">
																	<option value="">Select</option>


																</select>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype fmod-sel "
																	name="fmod-flag" id="fmod-ext-logic">
																	<option value="">Select</option>
																	<option value="9">All</option>

																</select>
															</div>
														</div>
													</div>

												</div>

											</div>
											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class="fields m-t-5">Sensor </label>
														</div>
													</div>
													<div class="col-md-10">
														<div class="col-md-12 stl-row-back">

															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="fsen-ser-path" autocomplete="off"
																	class="form-control fsen-text " name="fsen-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="fsen-loc-path" autocomplete="off"
																	class="form-control fsen-text " name="fsen-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype fsen-sel "
																	name="fsen-flag" id="fsen-par-list">
																	<option value="">Select</option>


																</select>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype fsen-sel "
																	name="fsen-flag" id="fsen-ext-logic">
																	<option value="">Select</option>
																	<option value="9">All</option>

																</select>
															</div>

														</div>
													</div>

												</div>

											</div>




											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class="fields m-t-5">SCB</label>
														</div>
													</div>
													<div class="col-md-10">
														<div class="col-md-12 stl-row-back">

															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="fscb-ser-path" autocomplete="off"
																	class="form-control fsen-text " name="fsen-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="fscb-loc-path" autocomplete="off"
																	class="form-control fsen-text " name="fsen-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype fsen-sel "
																	name="fsen-flag" id="fscb-par-list">
																	<option value="">Select</option>


																</select>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype fsen-sel "
																	name="fsen-flag" id="fscb-ext-logic">
																	<option value="">Select</option>
																	<option value="9">All</option>

																</select>
															</div>

														</div>
													</div>

												</div>

											</div>








											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class="fields m-t-5">Tracker</label>
														</div>
													</div>
													<div class="col-md-10">
														<div class="col-md-12 stl-row-back">

															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="ftrack-ser-path" autocomplete="off"
																	class="form-control fsen-text " name="fsen-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="ftrack-loc-path" autocomplete="off"
																	class="form-control fsen-text " name="fsen-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype fsen-sel "
																	name="fsen-flag" id="ftrack-par-list">
																	<option value="">Select</option>


																</select>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype fsen-sel "
																	name="fsen-flag" id="ftrack-ext-logic">
																	<option value="">Select</option>
																	<option value="9">All</option>

																</select>
															</div>

														</div>
													</div>

												</div>

											</div>







											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class="fields m-t-5">Energy Meter</label>
														</div>
													</div>
													<div class="col-md-10">
														<div class="col-md-12 stl-row-back">

															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="fenergy-ser-path" autocomplete="off"
																	class="form-control fsen-text " name="fsen-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="fenergy-loc-path" autocomplete="off"
																	class="form-control fsen-text" name="fsen-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype fsen-sel "
																	name="fsen-flag" id="fenergy-par-list">
																	<option value="">Select</option>


																</select>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype fsen-sel "
																	name="fsen-flag" id="fenergy-ext-logic">
																	<option value="">Select</option>
																	<option value="9">All</option>

																</select>
															</div>

														</div>
													</div>

												</div>

											</div>




											<!-- <div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class="fields m-t-5">Alarm </label>
														</div>
													</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<input id="" autocomplete="off" class="form-control"
															name="" placeholder="" type="text">
													</div>

												</div>

											</div>
											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class="fields m-t-5">Log </label>
														</div>
													</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<input id="" autocomplete="off" class="form-control"
															name="" placeholder="" type="text">
													</div>

												</div>

											</div> -->
										</div>
									</div>
								</div>
							</div>
							<!-- Ftp Config End-->

							<!-- Api config Start-->
							<div class="sortable hidden" id="apiConfig">
								<div class="col-md-12 padd-0">
									<div class="card" data-pages="card">
										<div class="card-header p-l-0 padd-0 m-h-30">
											<div class="card-title pull-left tbl-head m-l-5">API
												Config</div>
										</div>
										<div class="card card-transparent  padd-5"
											style="padding-top: 0px !important;">
											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth ">
															<label class="fields m-t-5">Request Time Interval
																(Min)</label>
														</div>
													</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<input id="areq-time-invl" autocomplete="off"
															class="form-control" name="" placeholder="" type="text">
													</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth ">
															<label class="fields m-t-5">Schedule Interval
																(Min)</label>
														</div>
													</div>
													<div class="col-md-2 col-xs-12 m-t-5">
														<select
															class="ui fluid  selection dropdown width ddLPtype"
															name="ddlCategory" id="areq-shd-invl">
															<option value="">Select</option>
															<option value="5">5Mins</option>
															<option value="10">10Mins</option>
															<option value="15">15Mins</option>
															<option value="30">30Mins</option>
															<option value="45">45Mins</option>
															<option value="60">1Hr</option>
														</select>
													</div>

												</div>

											</div>

											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class=" m-t-5"><b>Category </b></label>
														</div>
													</div>


													<div class="col-md-10">
														<div class="col-md-12">
															<div class="col-md-3 col-xs-12 m-t-5">
																<div class="txtwidth">
																	<label class=" m-t-5"><b>API Url </b></label>
																</div>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<div class="txtwidth">
																	<label class=" m-t-5"><b>API Key </b></label>
																</div>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<div class="txtwidth">
																	<label class=" m-t-5"><b>Parameter List
																			Version </b></label>
																</div>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<div class="txtwidth">
																	<label class=" m-t-5"><b>Data Extraction
																			Logic </b></label>
																</div>
															</div>
														</div>
													</div>

												</div>

											</div>
											<!-- <div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class="fields m-t-5">(eg:)</label>
														</div>
													</div>


												</div>

											</div> -->
											<!-- table content -->
											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class="fields m-t-5">Inverter</label>
														</div>
													</div>

													<div class="col-md-10">
														<div class="col-md-12 stl-row-back">

															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="ainv-api-url" autocomplete="off"
																	class="form-control ainv-text" name="ainv-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="ainv-api-key" autocomplete="off"
																	class="form-control ainv-text" name="ainv-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype"
																	name="ainv-flag" id="ainv-par-list-v">
																	<option value="">Select</option>


																</select>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype"
																	name="ainv-flag" id="ainv-ext-logic">
																	<option value="">Select</option>

																</select>
															</div>

														</div>
													</div>


												</div>

											</div>
											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class="fields m-t-5">Sensor </label>
														</div>
													</div>


													<div class="col-md-10">
														<div class="col-md-12 stl-row-back">
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="asen-api-url" autocomplete="off"
																	class="form-control asen-text" name="asen-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="asen-api-key" autocomplete="off"
																	class="form-control asen-text" name="asen-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype"
																	name="asen-flag" id="asen-par-list-v">
																	<option value="">Select</option>

																</select>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype"
																	name="asen-flag" id="asen-ext-logic">
																	<option value="">Select</option>
																</select>
															</div>
														</div>
													</div>



												</div>

											</div>
											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class="fields m-t-5">Emergy Meter </label>
														</div>
													</div>


													<div class="col-md-10">
														<div class="col-md-12 stl-row-back">
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="aem-api-url" autocomplete="off"
																	class="form-control aegm-text" name="aegm-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="aem-api-key" autocomplete="off"
																	class="form-control aegm-text" name="aegm-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype"
																	name="aegm-flag" id="aem-par-list-v">
																	<option value="">Select</option>


																</select>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype"
																	name="aegm-flag" id="aem-ext-logic">
																	<option value="">Select</option>

																</select>
															</div>
														</div>
													</div>


												</div>

											</div>
											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class="fields m-t-5">SCB </label>
														</div>
													</div>

													<div class="col-md-10">
														<div class="col-md-12 stl-row-back">
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="ascb-api-url" autocomplete="off"
																	class="form-control ascb-text" name="ascb-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="ascb-api-key" autocomplete="off"
																	class="form-control ascb-text" name="ascb-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype"
																	name="ascb-flag" id="ascb-par-list-v">
																	<option value="">Select</option>


																</select>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype"
																	name="ascb-flag" id="ascb-ext-logic">
																	<option value="">Select</option>
																</select>
															</div>
														</div>
													</div>
												</div>

											</div>
											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-2 col-xs-12 m-t-5">
														<div class="txtwidth">
															<label class="fields m-t-5">Tracker</label>
														</div>
													</div>

													<div class="col-md-10">
														<div class="col-md-12 stl-row-back">
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="atra-api-url" autocomplete="off"
																	class="form-control atra-text" name="atra-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<input id="atra-api-key" autocomplete="off"
																	class="form-control atra-text" name="atra-flag"
																	placeholder="" type="text">
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype"
																	name="atra-flag" id="atra-par-list-v">
																	<option value="">Select</option>


																</select>
															</div>
															<div class="col-md-3 col-xs-12 m-t-5">
																<select
																	class="ui fluid search selection dropdown width ddLPtype"
																	name="atra-flag" id="atra-ext-logic">
																	<option value="">Select</option>
																</select>
															</div>
														</div>
													</div>

												</div>

											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Api Config End-->
							<!-- save clear Start -->
							<div class="sortable hidden" id="saveClear">
								<div class="col-md-12 padd-0">
									<div class="card" data-pages="card">
										<div class="card-header p-l-0 padd-0 m-h-30">
											<div class="card-title pull-left tbl-head m-l-5"></div>
										</div>
										<div class="card card-transparent  padd-5"
											style="padding-top: 0px !important;">

											<div class="row">
												<div class="col-md-5"></div>
												<div class="col-md-1">
													<input type="button" value="Save"
														class="btn btn-primary btn-lg btn-block" id=""
														onclick="return validation();">
												</div>
												<div class="col-md-1">
													<input type="button" value="Clear"
														class="btn btn-primary btn-lg btn-block" id="mbtn-clear">
												</div>
												<div class="col-md-5"></div>

											</div>

										</div>
									</div>
								</div>
							</div>
							<!-- save clear End -->





						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="container-fixed-lg footer">
			<div class="container-fluid copyright sm-text-center">
				<p class="small no-margin pull-left sm-pull-reset">
					<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
						CLEAN ENERGY.</span> <span class="hint-text">All rights reserved.
					</span>
				</p>
				<p class="small no-margin pull-rhs sm-pull-reset">
					<span class="hint-text">Powered by</span> <span>MESTECH
						SERVICES PVT LTD</span>.
				</p>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	</div>

	<!-- Side Bar Content Start-->

	<!-- Side Bar Content End-->
	<!--   
      <div class="quickview-wrapper  builder hidden-sm hidden-xs" id="builder">
         <div class="p-l-10 p-r-0 ">
            <a class="builder-close quickview-toggle pg-close"  href="#"></a>
            <a class="builder-toggle" data-toggle="quickview" data-toggle-element="#builder"><img src="resources/img/ImageResize_06.png"></a>
            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary" id="builderTabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tabLayouts" role="tab" aria-controls="home"><span>Quick Links</span></a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active " id="tabLayouts" role="tabcard">
                  <div class="scrollable">
                     <div class="p-r-50">
                        <div class="list">
                           <ul>
                              <li><a href="#QuickLinkWrapper1">Customer List dfdfds</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div> -->
	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>
	<script
		src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>




	<div id="gotop"></div>

	<!-- Address Popup Start !-->
	<!-- Modal -->
	<div id="tktCreation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header Popup-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="resources/img/close.png">
					</button>
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<p class="modal-title tkt-title">
						<img src="resources/img/tkt.png"><span class="tkt-title">New
							Ticket</span>
					</p>
				</div>
				<div class="modal-body">
					<c:url var="addAction" value="/addnewticket"></c:url>
					<div class="col-md-12">
						<p class="tkt-mandat">
							<span class="errormess">*</span> All fields are mandatory
						</p>
					</div>
					<form:form action="${addAction}" modelAttribute="ticketcreation"
						class="ui form validation-form">

						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlSite" name="ddlSite" path="siteID">
									<form:option value="">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Type</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlType" name="ddlType" path="ticketType">
									<form:option value="">Select</form:option>
									<form:option value="Operation">Operation</form:option>
									<form:option value="Maintenance">Maintenance</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Category</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width category"
									id="ddlCategory" name="ddlCategory" path="ticketCategory">
									<form:option value="">Select </form:option>
									<form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
									<form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
									<form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
									<form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
									<form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
									<form:option data-value="Operation"
										value="Equipment Replacement">Equipment Replacement</form:option>
									<form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
									<form:option data-value="Operation" value="String Down">String Down</form:option>
									<form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
									<form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
									<form:option data-value="Operation" value="">Select</form:option>
									<form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
									<form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="DataLogger Cleaning">DataLogger Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="String Current Measurement">String Current Measurement</form:option>
									<form:option data-value="Maintenance"
										value="Preventive Maintenance">Preventive Maintenance</form:option>

									<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
									<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
									<form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
									<form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
									<form:option data-value="Maintenance" value="">Select</form:option>
								</form:select>
							</div>

						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Subject</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<form:input placeholder="Subject" name="Subject" type="text"
										autocomplete="off" id="txtSubject" path="ticketDetail"
										maxLength="50" />
								</div>
							</div>
						</div>

						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Priority</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlPriority" name="user" path="priority">
									<form:option value="">Select </form:option>
									<form:option data-value="3" value="3">High</form:option>
									<form:option data-value="2" value="2">Medium</form:option>
									<form:option data-value="1" value="1">Low</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Description</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<textarea id="txtTktdescription" autocomplete="off"
										name="description" style="resize: none;"
										placeholder="Ticket Description" maxLength="120"></textarea>
								</div>

							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
							<div class="btn btn-success  submit" id="btnCreate">Create</div>
							<button class="btn btn-success" type="button" id="btnCreateDummy"
								tabindex="7" style="display: none;">Create</button>
							<div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
						</div>
						<!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
						<!-- <div class="ui error message"></div> -->
					</form:form>
					<!-- <div class="ui error message"></div> -->

				</div>
			</div>
		</div>
	</div>
	<select id="template-dropdown" style="display: none; width: 100%">

		<option value="">Select</option>
		<option value="Active">Active</option>
		<option value="Inactive">Inactive</option>
	</select>
	<!-- Address Popup End !-->
	<script>
		$('#gotop').gotop({
			customHtml : '<i class="fa fa-angle-up fa-2x"></i>',
			bottom : '2em',
			right : '2em'
		});
	</script>
	<script>
		$(document).ready(function() {
			$(window).fadeThis({
				speed : 500,
			});

			var classstate = $('#hdnstate').val();

			$("#configurationid").addClass(classstate);
			$("#datacollectionconfigid").addClass(classstate);
		});
	</script>
	<!-- 	<script type="text/javascript">
		$(document).ready(function() {
			$('.mtoggle-style').bootstrapToggle({
				on : 'Enabled',
				off : 'Disabled'
				data-size:'mini'
			});
			$('#site-table').DataTable({});

		});
	</script> -->
	<script type="text/javascript">
		var map = new Map();
		$(document)
				.ready(
						function() {
							function operationModeCheck(key) {
								switch (key) {
								case 1:
									return "FTP"
									break;
								case 2:
									return "API"
									break;
								default:
									break;
								}
							}

							$
									.ajax({
										type : 'GET',
										url : './dataCollection/getAllSites',
										success : function(msg) {
											var siteLists = msg;
											if (siteLists) {
												for (i = 0; i < siteLists.length; i++) {
													var site = siteLists[i];
													var siteCode = site.siteCode;
													map.set(siteCode, site);
													var siteName = site.siteName;
													var operationMode = operationModeCheck(site.operationMode);
													var siteCapacity = site.installationCapacity;
													if (siteCapacity) {
														siteCapacity = (parseInt(siteCapacity) / 1000);
														siteCapacity = siteCapacity
																+ " MW";
													}
													var customerReference = site.customerReference;
													var energyGenerationBasedOn = (site.equipmentFlag == 1 ? "Energy Meter"
															: 'Inverter');
													var timeZone = site.timezone;
													if (timeZone == null)
														timeZone = '-';
													var extraction = (site.serviceFlag1 == 1 ? "checked"
															: '');
													var dumpService = (site.serviceFlag2 == 1 ? "checked"
															: '');
													var curringService = (site.serviceFlag3 == 1 ? "checked"
															: '');
													$('#site-table')
															.append(
																	'<tr><td>'
																			+ (i + 1)
																			+ '</td><td>'
																			+ siteCode
																			+ '</td><td>'
																			+ siteName
																			+ '</td><td>'
																			+ (operationMode ? operationMode
																					: "")
																			+ '</td><td>'
																			+ energyGenerationBasedOn
																			+ '</td><td>'
																			+ timeZone
																			+ '<td style="text-align: center;" ><input type="button" class="btn btn-primary config-btn" value="configure"></td>'
																			+ '<td class="hidden">'
																			+ siteCapacity
																			+ '</td>'
																			+ '<td class="hidden">'
																			+ customerReference
																			+ '</td></tr>');
												}
												$('.mtoggle-style')
														.bootstrapToggle({
															on : 'Enabled',
															off : 'Disabled'

														});

												$('.config-btn')
														.click(
																function() {
																	debugger;
																	$(
																			'#extraction-service-toggle')
																			.bootstrapToggle(
																					'off');
																	$(
																			'#dump-service-toggle')
																			.bootstrapToggle(
																					'off');
																	$(
																			'#curing-service-toggle')
																			.bootstrapToggle(
																					'off');
																	$(
																			'#timeZoneSelectId')
																			.dropdown(
																					'clear');
																	$(
																			'#mtodayebo')
																			.dropdown(
																					'clear');
																	$(
																			'#ftpConfig')
																			.find(
																					'input:text')
																			.each(
																					function() {
																						$(
																								this)
																								.val(
																										'');
																					});
																	$(
																			'#ftpConfig')
																			.find(
																					'select')
																			.each(
																					function() {
																						$(
																								this)
																								.dropdown(
																										'clear');
																					});

																	$(
																			'#apiConfig')
																			.find(
																					'input:text')
																			.each(
																					function() {
																						$(
																								this)
																								.val(
																										'');
																					});
																	$(
																			'#apiConfig')
																			.find(
																					'select')
																			.each(
																					function() {
																						$(
																								this)
																								.dropdown(
																										'clear');
																					});

																	$(
																			'#siteConfig')
																			.removeClass(
																					'hidden');
																	var data = $(
																			this)
																			.closest(
																					'tr')
																			.find(
																					'td');

																	$(
																			"#siteCodeId")
																			.text(
																					data
																							.eq(
																									'1')
																							.text());
																	$(
																			"#siteNameId")
																			.text(
																					data
																							.eq(
																									'2')
																							.text());
																	$(
																			"#siteCapacityId")
																			.text(
																					data
																							.eq(
																									'7')
																							.text());
																	$(
																			"#customerRefId")
																			.text(
																					data
																							.eq(
																									'8')
																							.text());
																	var site = map
																			.get(data
																					.eq(
																							'1')
																					.text());

																	var operationMode = operationModeCheck(site.operationMode);

																	var energyGenerationBasedOn = (site.equipmentFlag == 1 ? "Energy Meter"
																			: 'Inverter');
																	var timeZone = site.timezone;

																	$(
																			'#timeZoneSelectId')
																			.dropdown(
																					'set selected',
																					timeZone);
																	var todayEnergyFlag = site.todayEnergyFlag;

																	todayEnergyFlag = (site.todayEnergyFlag == 1 ? "Today Energy"
																			: 'Total Energy');
																	//$('#mMode').val(operationMode);

																	if (site.serviceFlag2) {
																		$(
																				'#extraction-service-toggle')
																				.bootstrapToggle(
																						'on');
																	}
																	if (site.serviceFlag3) {
																		$(
																				'#dump-service-toggle')
																				.bootstrapToggle(
																						'on');
																	}
																	if (site.serviceFlag4) {
																		$(
																				'#curing-service-toggle')
																				.bootstrapToggle(
																						'on');
																	}

																	$('#mMode')
																			.dropdown(
																					'set selected',
																					operationMode);
																	$(
																			'#mstatuslist')
																			.dropdown(
																					'set selected',
																					site.dataloggertypeid);

																	$(
																			'#prod-flag')
																			.dropdown(
																					'set selected',
																					site.prodFlag == null ? "0"
																							: "1");

																	if ($(
																			'#mMode')
																			.val() == 'FTP') {

																		$(
																				'#finv-par-list')
																				.dropdown(
																						'set selected',
																						site.dataLoggerID_Inverter);

																		$(
																				'#fmod-par-list')
																				.dropdown(
																						'set selected',
																						site.dataLoggerID_Modbus);

																		$(
																				'#fsen-par-list')
																				.dropdown(
																						'set selected',
																						site.dataLoggerID_Sensor);
																		
																		$(
																		'#fscb-par-list')
																		.dropdown(
																				'set selected',
																				site.dataLoggerID_SCB);
																		
																		$(
																		'#ftrack-par-list')
																		.dropdown(
																				'set selected',
																				site.dataLoggerID_TRACKER);
																		
																		$(
																		'#fenergy-par-list')
																		.dropdown(
																				'set selected',
																				site.dataLoggerID_ENERGYMETER);

																		$(
																				'#finv-ext-logic')
																				.dropdown(
																						'set selected',
																						site.serviceCode_Inverter);

																		$(
																				'#fmod-ext-logic')
																				.dropdown(
																						'set selected',
																						site.serviceCode_Modbus);
																		$(
																				'#fsen-ext-logic')
																				.dropdown(
																						'set selected',
																						site.serviceCode_Sensor);
																		
																		$(
																		'#fscb-ext-logic')
																		.dropdown(
																				'set selected',
																				site.serviceCode_SCB);
																		
																		$(
																		'#ftrack-ext-logic')
																		.dropdown(
																				'set selected',
																				site.serviceCode_TRACKER);
																		
																		$(
																		'#fenergy-ext-logic')
																		.dropdown(
																				'set selected',
																				site.serviceCode_ENERGYMETER);
																		var site = map
																				.get($(
																						"#siteCodeId")
																						.text());
																		$(
																				'#ffiletype')
																				.dropdown(
																						'set selected',
																						site.fileType);
																		$(
																				'#fftphomedir')
																				.val(
																						site.localFtpHomeDirectory);
																		$(
																				'#fftpusername')
																				.val(
																						site.localFtpUserName);
																		$(
																				'#fftppass')
																				.val(
																						site.localFtpUserName);
																		if (site.localFtpDirectoryPath_Inverter) {
																			$(
																					'#finv-ser-path')
																					.val(
																							site.localFtpDirectoryPath_Inverter
																									.replace(
																											site.localFtpHomeDirectory,
																											""));
																		}

																		if (site.localFtpDirectory_Inverter) {
																			$(
																					'#finv-loc-path')
																					.val(
																							site.localFtpDirectory_Inverter);
																		}

																		if (site.localFtpDirectoryPath_Modbus) {
																			$(
																					'#fmod-ser-path')
																					.val(
																							site.localFtpDirectoryPath_Modbus
																									.replace(
																											site.localFtpHomeDirectory,
																											""));
																		}

																		if (site.localFtpDirectory_Modbus) {
																			$(
																					'#fmod-loc-path')
																					.val(
																							site.localFtpDirectory_Modbus);
																		}

																		if (site.localFtpDirectoryPath_Sensor) {
																			$(
																					'#fsen-ser-path')
																					.val(
																							site.localFtpDirectoryPath_Sensor
																									.replace(
																											site.localFtpHomeDirectory,
																											""));
																		}

																		if (site.localFtpDirectory_Sensor) {
																			$(
																					'#fsen-loc-path')
																					.val(
																							site.localFtpDirectory_Sensor);
																		}

																	} else if ($(
																			'#mMode')
																			.val() == 'API') {

																		$(
																				'#ainv-par-list-v')
																				.dropdown(
																						'set selected',
																						site.dataLoggerID_Inverter);
																		$(
																				'#ainv-ext-logic')
																				.dropdown(
																						'set selected',
																						site.serviceCode_Inverter);

																		$(
																				'#asen-par-list-v')
																				.dropdown(
																						'set selected',
																						site.dataLoggerID_Sensor);
																		$(
																				'#asen-ext-logic')
																				.dropdown(
																						'set selected',
																						site.serviceCode_Sensor);

																		$(
																				'#aem-par-list-v')
																				.dropdown(
																						'set selected',
																						site.dataLoggerID_ENERGYMETER);
																		$(
																				'#aem-ext-logic')
																				.dropdown(
																						'set selected',
																						site.serviceCode_ENERGYMETER);

																		$(
																				'#ascb-par-list-v')
																				.dropdown(
																						'set selected',
																						site.dataLoggerID_SCB);
																		$(
																				'#ascb-ext-logic')
																				.dropdown(
																						'set selected',
																						site.serviceCode_SCB);

																		$(
																				'#atra-par-list-v')
																				.dropdown(
																						'set selected',
																						site.dataLoggerID_TRACKER);
																		$(
																				'#atra-ext-logic')
																				.dropdown(
																						'set selected',
																						site.serviceCode_TRACKER);

																		var site = map
																				.get($(
																						"#siteCodeId")
																						.text());
																		$(
																				'#areq-shd-invl')
																				.dropdown(
																						'set selected',
																						site.apiScheduleInterval);
																		$(
																				'#areq-time-invl')
																				.val(
																						site.apiDataInterval);
																		$(
																				'#ainv-api-url')
																				.val(
																						site.apiUrl);
																		$(
																				'#ainv-api-key')
																				.val(
																						site.apiKey);
																		$(
																				'#asen-api-url')
																				.val(
																						site.sensorUrl);
																		$(
																				'#asen-api-key')
																				.val(
																						site.sensorkey);
																		$(
																				'#aem-api-url')
																				.val(
																						site.energyMeterUrl);
																		$(
																				'#aem-api-key')
																				.val(
																						site.energyMeterkey);
																		$(
																				'#ascb-api-url')
																				.val(
																						site.scbUrl);
																		$(
																				'#ascb-api-key')
																				.val(
																						site.scbkey);
																		$(
																				'#atra-api-url')
																				.val(
																						site.trackerUrl);
																		$(
																				'#atra-api-key')
																				.val(
																						site.trackerkey);
																	}

																	$(
																			'#menergygbo')
																			.dropdown(
																					'set selected',
																					energyGenerationBasedOn);
																	$(
																			'#mtodayebo')
																			.dropdown(
																					'set selected',
																					todayEnergyFlag);

																});

												var table = $('#site-table')
														.DataTable(
																{
																	"targets" : [ 0 ],
																	"visible" : false,
																	"searchable" : false
																});
											}
										},
										error : function(msg) {

										}
									});

						});

		$('#mMode').change(function() {
			if ($('#mMode').val() == 'FTP') {
				$('#ftpConfig').removeClass('hidden');
				$('#saveClear').removeClass('hidden');
				$('#apiConfig').addClass('hidden');
			} else {
				$('#apiConfig').removeClass('hidden');
				$('#saveClear').removeClass('hidden');
				$('#ftpConfig').addClass('hidden');

			}

		});

		function getheringData() {

			var siteObj = {};
			var site = map.get($("#siteCodeId").text());
			/* 	SITE CONFIG CARD */
			siteObj['siteId'] = site.siteId;
			siteObj['collectionType'] = ($('#mMode option:selected').text() == '' ? null
					: $('#mMode option:selected').text());

			siteObj['operationMode'] = ($('#mMode option:selected').text() == 'FTP' ? 1
					: 2);
			siteObj['equipmentFlag'] = ($('#menergygbo option:selected').text() == 'Inverter' ? 0
					: 1);
			siteObj['timezone'] = ($('#timeZoneSelectId option:selected')
					.text() == '' ? null : $(
					'#timeZoneSelectId option:selected').text());
			siteObj['todayEnergyFlag'] = ($('#mtodayebo option:selected')
					.text() == 'Total Energy' ? 0 : 1);
			siteObj['dataloggertypeid'] = $('#mstatuslist').val();

			siteObj['prodFlag'] = $('#prod-flag').val();

			siteObj['serviceFlag2'] = (($('#extraction-service-toggle')
					.prop('checked')) ? 1 : 0);
			siteObj['serviceFlag3'] = (($('#dump-service-toggle')
					.prop('checked')) ? 1 : 0);
			siteObj['serviceFlag4'] = (($('#curing-service-toggle')
					.prop('checked')) ? 1 : 0);
			/* 	siteObj['customerID'] = site.customerID;
				siteObj['activeFlag'] = site.activeFlag;
				siteObj['createdBy'] = site.createdBy;
				siteObj['creationDate'] = site.creationDate;
				siteObj['siteTypeID'] = site.siteTypeID;
				siteObj['stateID'] = site.stateID
				siteObj['countryID'] = site.countryID;

				siteObj['siteCode'] = ($("#siteCodeId").text() == '' ? null
						: $("#siteCodeId").text()); 

				siteObj['siteName'] = ($("#siteNameId").text() == '' ? null
						: $("#siteNameId").text());
				var installCapacity = ($("#siteCapacityId").text() == '' ? null
						: $("#siteCapacityId").text().replace('MW',
								''));
				if (installCapacity) {
					siteObj['installationCapacity'] = (parseInt(installCapacity) / 1000);
				}
				siteObj['customerReference'] = ($("#customerRefId")
						.text() == '' ? null : $("#customerRefId")
						.text()); */

			if ($('#mMode option:selected').text() == 'FTP') {

				/* FTP CONFIG */

				siteObj['fileType'] = ($('#ffiletype option:selected').text() == '' ? null
						: $('#ffiletype option:selected').text());
				siteObj['localFtpHomeDirectory'] = ($('#fftphomedir').val() == '' ? null
						: $('#fftphomedir').val());
				siteObj['localFtpUserName'] = ($('#fftpusername').val() == '' ? null
						: $('#fftpusername').val());
				siteObj['localFtpPassword'] = ($('#fftppass').val() == '' ? null
						: $('#fftppass').val());
				
				siteObj['localFtpDirectoryPath_Inverter'] = ($('#finv-ser-path')
						.val() == '' ? null : $('#finv-ser-path').val());
				siteObj['localFtpDirectory_Inverter'] = ($('#finv-loc-path')
						.val() == '' ? null : $('#finv-loc-path').val());
				siteObj['localFtpDirectoryPath_Modbus'] = ($('#fmod-ser-path')
						.val() == '' ? null : $('#fmod-ser-path').val());
				siteObj['localFtpDirectory_Modbus'] = ($('#fmod-loc-path')
						.val() == '' ? null : $('#fmod-loc-path').val());
				siteObj['localFtpDirectoryPath_Sensor'] = ($('#fsen-ser-path')
						.val() == '' ? null : $('#fsen-ser-path').val());
				siteObj['localFtpDirectory_Sensor'] = ($('#fsen-loc-path')
						.val() == '' ? null : $('#fsen-loc-path').val());
				
				siteObj['dataLoggerIdInverter'] = $('#finv-par-list').val();
				siteObj['dataLoggerIdModBus'] = $('#fmod-par-list').val();
				siteObj['dataLoggerIdSensor'] = $('#fsen-par-list').val();
				siteObj['dataLoggerIdScb'] = $('#fscb-par-list').val();
				siteObj['dataLoggerIdTracker'] = $('#ftrack-par-list').val();
				siteObj['dataLoggerIdEnergyMeter'] = $('#fenergy-par-list').val();

				siteObj['serviceCodeInverter'] = $('#finv-ext-logic').val();
				siteObj['serviceCodeModeBus'] = $('#fmod-ext-logic').val();
				siteObj['serviceCodeSensor'] = $('#fsen-ext-logic').val();
				siteObj['serviceCodeScb'] = $('#fscb-ext-logic').val();
				siteObj['serviceCodeTracker'] = $('#ftrack-ext-logic').val();
				siteObj['serviceCodeEnergyMeter'] = $('#fenergy-ext-logic').val();
				
				
				
				
				
				
				

			} else if ($('#mMode option:selected').text() == 'API') {
				/* API CONFIG */
				siteObj['apiScheduleInterval'] = ($(
						'#areq-shd-invl  option:selected').text() == '' ? null
						: parseInt($('#areq-shd-invl  option:selected').text()));
				siteObj['apiDataInterval'] = ($('#areq-time-invl').val() == '' ? null
						: parseInt($('#areq-time-invl').val()));
				siteObj['apiUrl'] = ($('#ainv-api-url').val() == '' ? null : $(
						'#ainv-api-url').val());
				siteObj['apiKey'] = ($('#ainv-api-key').val() == '' ? null : $(
						'#ainv-api-key').val());
				siteObj['sensorUrl'] = ($('#asen-api-url').val() == '' ? null
						: $('#asen-api-url').val());
				siteObj['sensorkey'] = ($('#asen-api-key').val() == '' ? null
						: $('#asen-api-key').val());
				siteObj['energyMeterUrl'] = ($('#aem-api-url').val() == '' ? null
						: $('#aem-api-url').val());
				siteObj['energyMeterkey'] = ($('#aem-api-key').val() == '' ? null
						: $('#aem-api-key').val());
				siteObj['scbUrl'] = ($('#ascb-api-url').val() == '' ? null : $(
						'#ascb-api-url').val());
				siteObj['scbkey'] = ($('#ascb-api-key').val() == '' ? null : $(
						'#ascb-api-key').val());
				siteObj['trackerUrl'] = ($('#atra-api-url').val() == '' ? null
						: $('#atra-api-url').val());
				siteObj['trackerkey'] = ($('#atra-api-key').val() == '' ? null
						: $('#atra-api-key').val());

				siteObj['dataLoggerIdInverter'] = $('#ainv-par-list-v').val();
				siteObj['serviceCodeInverter'] = $('#ainv-ext-logic').val();
				siteObj['dataLoggerIdSensor'] = $('#asen-par-list-v').val();
				siteObj['serviceCodeSensor'] = $('#asen-ext-logic').val();
				siteObj['dataLoggerIdEnergyMeter'] = $('#aem-par-list-v').val();
				siteObj['serviceCodeEnergyMeter'] = $('#aem-ext-logic').val();
				siteObj['dataLoggerIdScb'] = $('#ascb-par-list-v').val();
				siteObj['serviceCodeScb'] = $('#ascb-ext-logic').val();
				siteObj['dataLoggerIdTracker'] = $('#atra-par-list-v').val();
				siteObj['serviceCodeTracker'] = $('#atra-ext-logic').val();
			}
			var siteJson = JSON.stringify(siteObj);
			console.log(siteJson);
			debugger;
			saveConfiguration(siteJson);
		}

		function saveConfiguration(siteJson) {

			$.ajax({
				type : 'POST',
				url : './dataCollection/saveConfiguration',
				data : siteJson,
				contentType : "application/json",
				dataType : "json",
				success : function(msg) {
					if (msg.status == 'Success') {
						toastr.info("Saved SuccessFully");

						location.reload();
					} else {
						toastr.info("Dat Not Saved");
					}

				},
				error : function(msg) {

				}
			});

		}
	</script>

	<script type="text/javascript">
		$(document).ready(
				function() {
					$.ajax({
						type : 'GET',
						url : './dataCollection/getAllTimeZone',
						success : function(msg) {
							var timeZones = msg;
							for (op = 0; op < timeZones.length; op++) {
								$('#timeZoneSelectId').append(
										"<option value="+timeZones[op]+">"
												+ timeZones[op] + "</option>");

							}
						},
						error : function(msg) {

						}
					});
				});

		function add_err(id) {
			$('#' + id).closest('.ui.search.dropdown').addClass('err-dropdown');
		}

		function validation() {
			var error = false;
			var error_config = false;

			$('.err-dropdown').removeClass('err-dropdown');
			$('.err-row-text').removeClass('err-row-text');
			$('.err-row-back').removeClass('err-row-back');

			if (!($('#mMode').val())) {
				error = true;
				add_err("mMode");
			}
			if (!($('#timeZoneSelectId').val())) {
				error = true;
				add_err("timeZoneSelectId");
			}
			if (!($('#menergygbo').val())) {
				error = true;
				add_err("menergygbo");
			}
			if (!($('#menergygbo').val())) {
				error = true;
				add_err("menergygbo");
			}
			if (!($('#mtodayebo').val())) {
				error = true;
				add_err("mtodayebo");
			}
			if (!($('#mstatuslist').val())) {
				error = true;
				add_err("mstatuslist");
			}
			if (!($('#prod-flag').val())) {
				error = true;
				add_err("prod-flag");
			}
			if ($("#mMode").val() == "API") {
				error_config = validate_api();
			} else if ($("#mMode").val() == "FTP") {
				//error_config = validate_ftp();
			}

			if (error || error_config) {
				toastr.error("Validation failure");
			} else {
				getheringData();
			}

		}

		function validate_api() {
			var inv_flag = false;
			var sen_flag = false;
			var egm_flag = false;
			var scb_flag = false;
			var tra_flag = false;
			var error = false;

			$('[name=ainv-flag]').each(function() {
				if ($(this).val()) {
					inv_flag = true;
				}
			});
			$('[name=asen-flag]').each(function() {
				if ($(this).val()) {
					sen_flag = true;
				}
			});
			$('[name=aegm-flag]').each(function() {
				if ($(this).val()) {
					egm_flag = true;
				}
			});
			$('[name=ascb-flag]').each(function() {
				if ($(this).val()) {
					scb_flag = true;
				}
			});
			$('[name=atra-flag]').each(function() {
				if ($(this).val()) {
					tra_flag = true;
				}
			});

			if (inv_flag) {
				$('.ainv-text').each(
						function() {
							if (!($(this).val())) {
								$(this).addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
				$('select[name=ainv-flag]').each(
						function() {
							if (!($(this).val())) {
								$(this).closest('.ui.search.dropdown')
										.addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
			}
			if (sen_flag) {
				$('.asen-text').each(
						function() {
							if (!($(this).val())) {
								$(this).addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
				$('select[name=asen-flag]').each(
						function() {
							if (!($(this).val())) {
								$(this).closest('.ui.search.dropdown')
										.addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
			}
			if (egm_flag) {
				$('.aegm-text').each(
						function() {
							if (!($(this).val())) {
								$(this).addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
				$('select[name=aegm-flag]').each(
						function() {
							if (!($(this).val())) {
								$(this).closest('.ui.search.dropdown')
										.addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
			}
			if (scb_flag) {
				$('.ascb-text').each(
						function() {
							if (!($(this).val())) {
								$(this).addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
				$('select[name=ascb-flag]').each(
						function() {
							if (!($(this).val())) {
								$(this).closest('.ui.search.dropdown')
										.addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
			}
			if (tra_flag) {
				$('.atra-text').each(
						function() {
							if (!($(this).val())) {
								$(this).addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
				$('select[name=atra-flag]').each(
						function() {
							if (!($(this).val())) {
								$(this).closest('.ui.search.dropdown')
										.addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
			}
			return error;
		}

		function validate_ftp() {
			var inv_flag = false;
			var mod_flag = false;
			var sen_flag = false;
			var error = false;

			$('[name=finv-flag]').each(function() {
				if ($(this).val()) {
					inv_flag = true;
					console.log("yes");
				}
			});
			$('[name=fmod-flag]').each(function() {
				if ($(this).val()) {
					mod_flag = true;
				}
			});
			$('[name=fsen-flag]').each(function() {
				if ($(this).val()) {
					sen_flag = true;
				}
			});

			if (inv_flag) {
				$('.finv-text').each(
						function() {
							if (!($(this).val())) {
								$(this).addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
				$('select[name=finv-flag]').each(
						function() {
							if (!($(this).val())) {
								$(this).closest('.ui.search.dropdown')
										.addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
			}

			if (mod_flag) {
				$('.fmod-text').each(
						function() {
							if (!($(this).val())) {
								$(this).addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
				$('select[name=fmod-flag]').each(
						function() {
							if (!($(this).val())) {
								$(this).closest('.ui.search.dropdown')
										.addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
			}
			if (sen_flag) {
				$('.fsen-text').each(
						function() {
							if (!($(this).val())) {
								$(this).addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
				$('select[name=fsen-flag]').each(
						function() {
							if (!($(this).val())) {
								$(this).closest('.ui.search.dropdown')
										.addClass('err-row-text');
								$(this).closest('.stl-row-back').addClass(
										'err-row-back');
								error = true;
							}
						});
			}

			console.log(inv_flag + " " + mod_flag + " " + sen_flag);

			return error;
		}

		$(document).ready(function() {
			$('.test').tooltip();
		});
	</script>




</body>
</html>

