
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>

<html>
<head>
<title></title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="robots" content="noindex, nofollow">
<meta name="googlebot" content="noindex, nofollow">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="resources/css/pages.css" type="text/css" rel="stylesheet">
<link href="resources/css/styles.css" type="text/css" rel="stylesheet">
<link href="resources/css/login.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="resources/css/semantic.min.css">
<link href="resources/css/bootstrap.min.3.37.css" type="text/css"
	rel="stylesheet">
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script src="resources/js/semantic.min.js" type="text/javascript"></script>

<style>
.bg {
	height: 100%;
	weight: 100%;
	/*background: #5cb85c;*/
	/*opacity: 0.8;*/
	/* background: url(resources/img/bg_02.jpg) no-repeat; */
	background: url(resources/img/homepage.jpg) no-repeat;
	/*opacity: 0.5;*/
	overflow: hidden;
	background-size: cover;
	background-position: center;
}

.m-t-10 {
	margin-top: 10px;
}

@charset "ISO-8859-1";

.invalid {
	color: #DB2828;
	/* height:15px; */
	font-size: 12px;
	text-align: center;
	font-weight: bold;
	margin: 5px 0px 0px 0px !important;
}

.ui.segment {
	position: relative;
	background: #FFFFFF;
	opacity: 0.8 !important;
	box-shadow: 0px 1px 2px 0 rgba(34, 36, 38, 0.15);
	margin: 1rem 0em;
	padding: 1em 1em;
	border-radius: 0.28571429rem;
	border: 1px solid rgba(34, 36, 38, 0.15);
}
</style>

<c:if test="${not empty clear_window}">
	<script type='text/javascript'>
		history.pushState(null, null, location.href);
		window.onpopstate = function() {
			history.go(1);
		};
	</script>
</c:if>


<script type='text/javascript'>
	$(function() {

		var d = new Date();
		var n = -1 * d.getTimezoneOffset();

		var OSName = "Others";
		if (navigator.appVersion.indexOf("Mac") != -1)
			OSName = "MacOS";

		if (navigator.appVersion.indexOf("Trident/") != -1)
			OSName = "Microsoft Internet Explorer";

		if (navigator.appVersion.indexOf("Edge") != -1)
			OSName = "Microsoft Edge";

		function getIp() {
			var myIP;
			window.RTCPeerConnection = window.RTCPeerConnection
					|| window.mozRTCPeerConnection
					|| window.webkitRTCPeerConnection;//compatibility for Firefox and chrome
			var pc = new RTCPeerConnection({
				iceServers : []
			}), noop = function() {
			};
			pc.createDataChannel('');//create a bogus data channel
			pc.createOffer(pc.setLocalDescription.bind(pc), noop);// create offer and set local description
			pc.onicecandidate = function(ice) {
				if (ice && ice.candidate && ice.candidate.candidate) {
					myIP = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/
							.exec(ice.candidate.candidate)[1];

					var ipaddr = String(myIP);

					$('#hdneampmuserid').val(myIP);

					pc.onicecandidate = noop;
				}

				AjaxCallForSaveOrUpdateBySite(n, $('#hdneampmuserid').val());
			};
		}

		if (OSName == "MacOS" || OSName == "Microsoft Internet Explorer"
				|| OSName == "Microsoft Edge") {
			AjaxCallForSaveOrUpdateBySite(n, $('#hdneampmuserid').val());
		}

		else {
			getIp();
		}

		//$('#hdnSubject').val(n);
		//var check1 = $('#hdnSubject').val();
		//  AjaxCallForSaveOrUpdateBySite(n);

		function AjaxCallForSaveOrUpdateBySite(check1, ipaddr) {

			if (ipaddr == '' || ipaddr == null) {
				ipaddr = '127.0.0.1';

			}

			$.ajax({
				type : 'POST',
				url : './timezoneoffsetmin',
				data : {
					'timezonevalue' : check1,
					'IpAddress' : ipaddr
				},

				success : function(msg) {

				},
				error : function(msg) {

				}
			});
		}

		var marginTop = Math.round($(window).height() * 0.35);
		var marginBottom = Math.round($(window).height() * 0.35);

		$('.wrapper').css("margin-top", marginTop);
		$('.wrapper').css("margin-bottom", marginBottom);

		if ($(window).width() < 767) {
			$('.ftexts').removeClass("footer");
			$('.cname').removeClass("pull-right").addClass("center");
		}

		$('.reset').click(function() {
			$('.invalid').hide();
			$('.invalid').css("height", "15px");
		});

		$('#password').keypress(function() {
			$('.invalid').hide();
			$('.invalid').css("height", "15px");
		})
		$('#username').keypress(function() {
			$('.invalid').hide();
			$('.invalid').css("height", "15px");
		});

		$(".ui.dropdown").dropdown();
		$(".ui.checkbox").checkbox("uncheck");

		$('.ui.form').form({
			inline : true,
			on : "blur",
			fields : {
				username : {
					identifier : 'username',
					rules : [ {
						type : 'empty',
						prompt : 'Please enter  username'
					} ]
				},
				txtPassword : {
					identifier : 'txtPassword',
					rules : [ {
						type : 'empty',
						prompt : 'Please enter  password'
					} ]
				}
			}
		});

		$('.form').form('reset');
	});
</script>

</head>

<body class="bg">

	<div class="row">

		<div class="col-md-12 col-lg-12 col-sm-12">


			<!--  <p style="color:white;"> EIRA-Web Alpha v2.0.04  </p>  -->
			<div class="col-sm-12 col-md-4  col-lg-4 col-xl-4  "></div>
			<div class="col-sm-12 col-md-4 col-lg-4  col-xl-12">



				<div class="wrapper">
					<c:url var="loginValidation" value="./loginauthendication"></c:url>
					<form:form action="./j_spring_security_check"
						modelAttribute="login" class="ui form">
						<div class="ui form segment" class="keyevent">
							<div class="padd-top-20 required field ">
								<label>Username</label>
								<form:input placeholder="Username" id="username" name="username"
									type="text" path="userName" autocomplete="off" />
							</div>

							<div class="required field " style="margin-top: 15px !important;">
								<label>Password</label>
								<form:input placeholder="Password" id="txtPassword"
									name="txtPassword" type="password" path="password"
									autocomplete="off" />
							</div>

							<form:input type="hidden" id="hdneampmuserid" path="localIp" />

							<%-- <div class="m-t-10 required field">
            					<div class="">
            						 <label>Password</label>
					            <form:input placeholder="Password" id="txtPassword" name="txtPassword" type="password"  path="password" autocomplete="off"  />
            					</div>					           
					        </div> --%>
							<p class="invalid">
								<c:out value="${sessionScope.eampmerrormsg}"></c:out>
							</p>

							<c:choose>
								<c:when test="${param.error}">
									<p class="invalid">
										<c:out value="${param.msg}"></c:out>
									</p>
								</c:when>
							</c:choose>
							<div class="button center m-t-10">
								<input class="btn btn-success ui submit" id="btLogin"
									class="keylogin" type="submit" value="Login" name="submitLogin" />
								<input class="btn btn-primary ui reset" type="button"
									value="Reset" />
							</div>

							<!-- <div class="center">
					         	<div class="col-md-12 center m-t-10">					         	
					         	  <input class="btn btn-success ui submit" id="btLogin" class="keylogin"  type="submit" value="Login" name="submitLogin" />
					         	  <input class="btn btn-primary ui reset" type="button" value="Reset" />
					         		
					         	</div>
					         </div> -->
						</div>
					</form:form>

				</div>


			</div>



			<div class="col-md-4  col-lg-4 col-sm-12 col-xs-12"></div>
		</div>
		<div></div>

	</div>



	<div class="row">
		<div class="ftexts col-md-12 col-lg-12 col-xs-12 footer">
			<div class="col-md-6 col-lg-6 col-xs-12">
				<p class="ftext">
					<span class="copyrits">Copyrights &copy; 2018.</span><strong>Inspire
						Clean Energy.</strong> All Rights Reserved
				</p>
			</div>
			<div class="col-md-6 col-lg-6 col-xs-12 ">
				<p class="ftext cname pull-right">
					Powered By<a href='https://www.mestechservices.com/'><img
						src="resources/img/Comp_logo.png"></a>
				</p>
			</div>
		</div>
	</div>
</body>
</html>