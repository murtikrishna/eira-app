<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet" type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet"	type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet" rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/kpiStyles.css" type="text/css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet" type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet" type="text/css">
<link href="resources/css/bootstrap.min3.3.37.css" type="text/css" rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min3.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css"	href="resources/css/jquery-ui.css">

<link href="resources/css/formValidation.min.css" rel="stylesheet" type="text/css">
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!--  <link href="resources/css/jquery-ui_cal.css" rel="stylesheet" type="text/css"> -->

<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>

 

  <script src="resources/js/highcharts.js"></script> 
      <script src="resources/js/exporting.js"></script>
      <script src="resources/js/export-data.js"></script> 
       <script src="resources/js/highcharts-3d.js"></script> 
      
       <script src="resources/js/no-data-to-display.js"></script>
       
 
   

<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script type="text/javascript" src="resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="resources/js/formValidation.min.js"></script>
<script type="text/javascript" src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.val.js"></script>
<script type="text/javascript" src="resources/js/toastr.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/toastr.css">
<link rel="stylesheet" type="text/css" href="http://codeseven.github.com/toastr/toastr.css">


<style type="text/css">
.content-bg {
	    background: #FFF;
    height: 45px;
    /* margin: 5px; */
    padding: 5px;
	
}
.ticketdetails {
	float:right;
color:#000; 
    padding-top: 8px;
  /*   position: absolute; */

}
.ticketdetails a {
	font-weight:bold;
	color:#4b51a7; 
}
.selectSite .text {
	color:#000 !important;
}
.selectSite .search {
    color: #000 !important;
}
.highcharts-title {
    fill: #434348;
    font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif !important;  
    font-size: 12px;
}
.highcharts-axis-title {
	fill: #434348;
    font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif !important;  
    font-size: 12px;
}
.highcharts-axis-labels {
	fill: #434348;
    font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif !important;  
    font-size: 18px;
}
.highcharts-legend-item{
	font-size: 10px !important;
}


.form-control {
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 5px;
    font-size: 12px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
.carousel-indicators {
    bottom: 0px;
    margin-bottom: 4px;
}
	.carousel-control {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    width: 2%;
    font-size: 20px;
    color: #fff;
    text-align: center;
    text-shadow: 0 1px 2px rgba(0, 0, 0, .6);
    background-color: rgba(0, 0, 0, 0);
    filter: alpha(opacity=50);
    opacity: .5;
}
.glyphicon-chevron-right:before {
    content: "\e080";
    color: #818181;
}
.glyphicon-chevron-left:before {
    content: "\e079";
    color: #818181;
}
.carousel-indicators li {
    display: inline-block;
    width: 10px;
    height: 10px;
    margin: 1px;
    text-indent: -999px;
    cursor: pointer;
    background-color: #000\9;
    background-color: #AAAAAA;
    border: 1px solid #fff;
    border-radius: 10px;
}
.carousel-control.right,.carousel-control.left{
	background-image: none !important;
}
.carousel-indicators .active {
    width: 12px;
    height: 12px;
    margin: 0;
    background-color: #CCCCCC;
}
.carousel-caption {
    position: absolute;
    right: 15%;
    bottom: 20px;
    left: 15%;
    top:0px;
    z-index: 10;
    padding-top: 0px;
    padding-bottom: 0px;
    color: #fff;
    text-align: center;
    text-shadow: none;
}
.carousel-caption {
    right: 0% !important;
    left: 1% !important;
    padding-bottom: 0px;
}
.paragrapth{
font-size: 11px;
color:black;
font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
}


/*.carousel-caption {
    right: 10%;
    left: 10%;
    padding-bottom: 30px;
}*/

.mcar-span{
float: right;
    color: black;
    font-size: 15px;
    margin-right: 15px;
    font-weight: bold;}
</style>
<script type="text/javascript">

function handlenullable(param){
	if(param == null || param == 'null'){
		return '';
	}
	else{
		return param;
	}
	
}


function LoadDataTable(ticketdetaillist)
{
	 var ticketrows = "";
	 if(ticketdetaillist.length == 0)
	 {
	 toastr.warning("No Data Available"); 
	 }
     for(var i=0; i < ticketdetaillist.length; i++)
		{
			var priority = ticketdetaillist[i].priority;
			if(priority == 1)
			{
				priority = "Low";
			}
			else if(priority == 2)
			{
				priority = "Medium";
			}
			else if(priority == 3)
			{
				priority = "High";
			}
			
			var state = ticketdetaillist[i].state;
			if(state == 1)
			{
				state = "Open";
			}
			else if(state == 2)
			{
				state = "Closed";
			}
			else if(state == 3)
			{
				state = "Hold";
			}
			
			var ticketStatus = ticketdetaillist[i].ticketStatus;
			
			if(ticketStatus == 1)
			{
				ticketStatus = "Created";
			}
			else if(ticketStatus == 2)
			{
				ticketStatus = "Assigned";
			}
			else if(ticketStatus == 3)
			{
				ticketStatus = "Inprogress";
			}
			
			if(ticketStatus == 4)
			{
				ticketStatus = "Unfinished";
			}
			else if(ticketStatus == 5)
			{
				ticketStatus = "Finished";
			}
			else if(ticketStatus == 6)
			{
				ticketStatus = "Closed";
			}
			else if(ticketStatus == 7)
			{
				ticketStatus = "Hold";
			}
			
			    
			
			
			<c:choose>
			<c:when test="${access.customerListView == 'visible'}">
			
			var href = '.\\ticketviews' + ticketdetaillist[i].ticketID;
			
			if(i==0)
			{
				
				ticketrows = "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketdetaillist[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketdetaillist[i].siteName) +"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketdetaillist[i].createdDateText) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketCategory) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketDetail) +"</td><td>" + handlenullable(ticketdetaillist[i].assignedToWhom) +"</td><td>" + handlenullable(ticketdetaillist[i].createdByName) +"</td><td>" + handlenullable(ticketdetaillist[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
			}
			else
			{
				ticketrows = ticketrows + "<tr><td>" + "<a href='"+ href +"'>" + handlenullable(ticketdetaillist[i].ticketCode)+"</a>" +"</td><td>" + handlenullable(ticketdetaillist[i].siteName) +"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketdetaillist[i].createdDateText) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketCategory) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketDetail) +"</td><td>" + handlenullable(ticketdetaillist[i].assignedToWhom) +"</td><td>" + handlenullable(ticketdetaillist[i].createdByName) +"</td><td>" + handlenullable(ticketdetaillist[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
			}
			
			</c:when>
			
			<c:otherwise>
			
			if(i==0)
			{
				
				ticketrows = "<tr><td>" + handlenullable(ticketdetaillist[i].siteName) +"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketdetaillist[i].createdDateText) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketCategory) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketDetail) +"</td><td>" + handlenullable(ticketdetaillist[i].assignedToWhom) +"</td><td>" + handlenullable(ticketdetaillist[i].createdByName) +"</td><td>" + handlenullable(ticketdetaillist[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
			}
			else
			{
				ticketrows = ticketrows + "<tr><td>" + handlenullable(ticketdetaillist[i].siteName) +"</td><td>" + handlenullable(priority) +"</td><td>" + handlenullable(ticketdetaillist[i].createdDateText) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketCategory) +"</td><td>" + handlenullable(ticketdetaillist[i].ticketDetail) +"</td><td>" + handlenullable(ticketdetaillist[i].assignedToWhom) +"</td><td>" + handlenullable(ticketdetaillist[i].createdByName) +"</td><td>" + handlenullable(ticketdetaillist[i].scheduledDateText) +"</td><td>" + handlenullable(state) +"</td><td>" + handlenullable(ticketStatus) +"</td></tr>";
				
			}
				
			
			</c:otherwise>
			</c:choose>

			
			
			
			
			
				
		
		} 
     
     $('#example').append(ticketrows);
     
    
    	var table = $('#example').DataTable( {
    	 //"dom": '<"row"<"col-sm-9"l><"col-sm-1"B><"col-sm-2"f>>'+'t<"row"<"col-sm-6"i><"col-sm-6"p>>',
		 "order": [[ 0, "desc" ]],
 		 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		 "bLengthChange": true, 
		 "buttons": [
			 {
    			 extend:'excelHtml5',
    			 				title: 'Tickets List _'+ dateTime,

    		 }
	        ]
			 }); 
      
      table.buttons().container()
		.appendTo('#example_wrapper .col-sm-6:eq(0)');
      
      $('.dt-buttons').append('<span class="c-excel-click" style="margin-left: 5px;cursor:pointer;"><i class="fa fa-download" aria-hidden="true" style="font-size: 20px;margin-left: 5px;"></i><span style="font-weight: 600;margin-left: 5px;">Excel</span></span>');
     $(".buttons-excel.buttons-html5").addClass("hidden");
     $(".dt-buttons").css("margin-top","6px");
  

}

$(document).on('click','.c-excel-click',function(){
	$('.buttons-excel').trigger('click');
});


var ajax_request1;
var ajax_request2;
var ajax_request3;
var ajax_request4;
var ajax_request5;

function handlenullable(param){
	if(param == null || param == 'null'){
		return '';
	}
	else{
		return param;
	}
	
}
var today = new Date();
var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
var dateTime = date + ' ' + time;

function AjaxCallForStateandStatusDetails(){
	try
	{
		if(typeof ajax_request2 !== 'undefined')
		{
			ajax_request2.abort();
		}
		
	}
	catch(err)
	{
		
	}
	ajax_request2 =  $.ajax({
		type: 'GET',
		url: './slide2',
		success: function(msg){
		var chartdata1=msg.chart1;
		var chartdata2=msg.chart2;
		var chartdata3=msg.chart3;
		var ticketgriddata =msg.ticketgriddata;
		
	
		 var seriesdata1 = eval("[" + chartdata1 + "]");
		 var seriesdata2 = eval("[" + chartdata2 + "]");
		 var seriesdata3 = eval("[" + chartdata3 + "]");
		
		 var siteList = msg.siteList;
		 
		 $( "#ddlSiteList" ).append("<option value='0'>All Sites</option>");
		 $.map(siteList, function(val, key) {                                    		 
	   		 $( "#ddlSiteList" ).append("<option value="+ key +">" + val +"</option>");
	   		});
		 
		 Highcharts
			.chart(
					'Chartcontainer4',
					{
						chart : {
							renderTo:'Chartcontainer4',
							backgroundColor:'transparent',
							type : 'column',
							options3d: {
 					            enabled: true,
 					            alpha: 4,
 					            beta: 11,
 					            depth: 50,
 					            viewDistance: 25
 					        } 
 					 
						},
						xAxis : {
							// minorGridLineWidth: 0,
						type:'category',
						 lineWidth:1,
						 gridLineWidth: 0

						},
						yAxis : {
							 lineWidth:1,
							 gridLineWidth: 0,
							 allowDecimals: false,
							title : {
								text : ' ',
								 style: {
						                color: '#000',
						                fontWeight:'bold'
						            }
							}
						},
						lang: {
		  		            noData: "No Data Found"
		  		        },
		  		        noData: {
		  		            style: {
		  		                fontWeight: 'bold',
		  		                fontSize: '25px',
		  		                color: '#303030'
		  		            }
		  		        },
						title : {
							text : ''
						},
						subtitle : {
							text : ''
						},
						 credits: {
				      	        enabled: false
				      	    },
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{categories.name} </td>'
									+ '<td style="padding:0"><b>{point.y}</b></td></tr>',
							footerFormat : '</table>',
						
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								depth:25,
								
								events : {
									legendItemClick : function() {
										return false;
									}
								}
							},
							 series: {
				                    
				                    cursor: 'pointer'
				                
				                }
						},
						exporting: {
					        menuItemDefinitions: {
					            // Custom definition
					            label: {
					                onclick: function () {
					                    this.renderer.label(
					                        'You just clicked a custom menu item',
					                        100,
					                        100
					                    )
					                    .attr({
					                        fill: '#a4edba',
					                        r: 5,
					                        padding: 10,
					                        zIndex: 10
					                    })
					                    .css({
					                        fontSize: '1.5em'
					                    })
					                    .add();
					                },
					              
					            }
					        },
					        filename: 'Open Tickets' + '_' +  dateTime,
					        buttons: {
					            contextButton: {
					                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
					            }
					        }
					    },
					  
						series : [ {
							showInLegend: false,  
							name: 'Count',
							data : seriesdata2,
							  point: {
				                   events: {
				                       click: function () {
				                           //alert('Category: ' + this.name + ', value: ' + this.y);
				                           var Data = (this.name);
				                           var Type="1";
				                         
				                           if(Data == "Created")
				    						{
				                        	   Data = "1";
				    						}
				    						else if(Data == "Assigned")
				    						{
				    							Data = "2";
				    						}
				    						else if(Data == "Inprogress")
				    						{
				    							Data = "3";
				    						}
				    						
				    						if(Data == "Unfinished")
				    						{
				    							Data = "4";
				    						}
				    						else if(Data == "Finished")
				    						{
				    							Data = "5";
				    						}
				    						else if(Data == "Closed")
				    						{
				    							Data = "6";
				    						}
				    						else if(Data == "Hold")
				    						{
				    							Data = "7";
				    						}
				                        
				                           $('#example tbody').empty();
				                           $('#example').DataTable().clear().draw();
				    		 				 $('#example').DataTable().destroy(); 
				    		 				AjaxCallForTicketTypeandStatusfileterdetails(Type,Data);

				                       }
				                   }
				               }

						} ]
					});
		
		 
		 
		
		
		  Highcharts
		 .chart('Chartcontainer5',{
		 			chart : {
		 				renderTo:'Chartcontainer5',
		 				backgroundColor:'transparent',
		 				type : 'column',
		 				 
		 				options3d: {
					            enabled: true,
					            alpha: 4,
					            beta: 11,
					            depth: 50,
					            viewDistance: 25
					        } 
		 					 

		 			},
		 			xAxis : {
		 				// minorGridLineWidth: 0,
		 				 type: 'category',
		 					 lineWidth:1,
							 gridLineWidth: 0
							
		 			},
		 			yAxis : {
		 				 lineWidth:1,
						 gridLineWidth: 0,
						 allowDecimals: false,
		 				title : {
		 					text : ' ',
		 					 style: {
		 			                color: '#000',
		 			                fontWeight:'bold'
		 			            }
		 				}
		 			},
		 			lang: {
	  		            noData: "No Data Found"
	  		        },
	  		        noData: {
	  		            style: {
	  		                fontWeight: 'bold',
	  		                fontSize: '25px',
	  		                color: '#303030'
	  		            }
	  		        },
		 			title : {
		 				text : ''
		 			},
		 			subtitle : {
		 				text : ''
		 			},
		 			 credits: {
		 	      	        enabled: false
		 	      	    },
		 			tooltip : {
		 				headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
		 				pointFormat : '<tr><td style="color:{series.color};padding:0">{categories.name} </td>'
		 						+ '<td style="padding:0"><b>{point.y}</b></td></tr>',
		 				footerFormat : '</table>',
		 			
		 				shared : true,
		 				useHTML : true
		 			},
		 			plotOptions : {
		 				column : {
		 					depth:25,
		 					borderWidth : 0
		 					
		 					
		 				}
		 				
		 			},
		 			exporting: {
		 		        menuItemDefinitions: {
		 		            // Custom definition
		 		            label: {
		 		                onclick: function () {
		 		                    this.renderer.label(
		 		                        'You just clicked a custom menu item',
		 		                        100,
		 		                        100
		 		                    )
		 		                    .attr({
		 		                        fill: '#a4edba',
		 		                        r: 5,
		 		                        padding: 10,
		 		                        zIndex: 10
		 		                    })
		 		                    .css({
		 		                        fontSize: '1.5em'
		 		                    })
		 		                    .add();
		 		                },
		 		              
		 		            }
		 		        },
		 		       filename: 'Close Tickets' + '_' +  dateTime,
		 		        buttons: {
		 		            contextButton: {
		 		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		 		            }
		 		        }
		 		    },
		 		  
		 			series : [ {
		 				showInLegend: false,  
		 				name: 'Count',
		 				data : seriesdata3,
		 				 point: {
			                   events: {
			                       click: function () {
			                           //alert('Category: ' + this.name + ', value: ' + this.y);
			                           var Data = (this.name);
			                           var Type="2";
			                         
			                           if(Data == "Created")
			    						{
			                        	   Data = "1";
			    						}
			    						else if(Data == "Assigned")
			    						{
			    							Data = "2";
			    						}
			    						else if(Data == "Inprogress")
			    						{
			    							Data = "3";
			    						}
			    						
			    						if(Data == "Unfinished")
			    						{
			    							Data = "4";
			    						}
			    						else if(Data == "Finished")
			    						{
			    							Data = "5";
			    						}
			    						else if(Data == "Closed")
			    						{
			    							Data = "6";
			    						}
			    						else if(Data == "Hold")
			    						{
			    							Data = "7";
			    						}
			                        
			                           $('#example tbody').empty();
			                           $('#example').DataTable().clear().draw();
			    		 				 $('#example').DataTable().destroy(); 
			    		 				AjaxCallForTicketTypeandStatusfileterdetails(Type,Data);

			                       }
			                   }
			               }

		 			} ]
		 		});
		 Highcharts
		 .chart('Chartcontainer6',{
		 			chart : {
		 				renderTo:'Chartcontainer6',
		 				backgroundColor:'transparent',
		 				type : 'column',
		 				 
		 				options3d: {
					            enabled: true,
					            alpha: 4,
					            beta: 11,
					            depth: 50,
					            viewDistance: 25
					        } 
		 					 

		 			},
		 			xAxis : {
		 				// minorGridLineWidth: 0,
		 				 type: 'category',
		 				 lineWidth:1,
						 gridLineWidth: 0

		 			},
		 			yAxis : {
		 				 lineWidth:1,
						 gridLineWidth: 0,
						 allowDecimals: false,
		 				title : {
		 					text : ' ',
		 					 style: {
		 			                color: '#000',
		 			                fontWeight:'bold'
		 			            }
		 				}
		 			},
		 			lang: {
	  		            noData: "No Data Found"
	  		        },
	  		        noData: {
	  		            style: {
	  		                fontWeight: 'bold',
	  		                fontSize: '25px',
	  		                color: '#303030'
	  		            }
	  		        },
		 			title : {
		 				text : ''
		 			},
		 			subtitle : {
		 				text : ''
		 			},
		 			 credits: {
		 	      	        enabled: false
		 	      	    },
		 			tooltip : {
		 				headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
		 				pointFormat : '<tr><td style="color:{series.color};padding:0">{categories.name} </td>'
		 						+ '<td style="padding:0"><b>{point.y}</b></td></tr>',
		 				footerFormat : '</table>',
		 			
		 				shared : true,
		 				useHTML : true
		 			},
		 			plotOptions : {
		 				column : {
		 					depth:25,
		 					borderWidth : 0
		 					
		 					
		 				}
		 				
		 			},
		 			exporting: {
		 		        menuItemDefinitions: {
		 		            // Custom definition
		 		            label: {
		 		                onclick: function () {
		 		                    this.renderer.label(
		 		                        'You just clicked a custom menu item',
		 		                        100,
		 		                        100
		 		                    )
		 		                    .attr({
		 		                        fill: '#a4edba',
		 		                        r: 5,
		 		                        padding: 10,
		 		                        zIndex: 10
		 		                    })
		 		                    .css({
		 		                        fontSize: '1.5em'
		 		                    })
		 		                    .add();
		 		                },
		 		              
		 		            }
		 		        },
		 		       filename: 'Hold Tickets' + '_' +  dateTime,
		 		        buttons: {
		 		            contextButton: {
		 		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		 		            }
		 		        }
		 		    },
		 		  
		 			series : [ {
		 				showInLegend: false,  
		 				name: 'Count',
		 				data : seriesdata1,
		 				 point: {
			                   events: {
			                       click: function () {
			                           //alert('Category: ' + this.name + ', value: ' + this.y);
			                           var Data = (this.name);
			                           var Type="3";
			                         
			                           if(Data == "Created")
			    						{
			                        	   Data = "1";
			    						}
			    						else if(Data == "Assigned")
			    						{
			    							Data = "2";
			    						}
			    						else if(Data == "Inprogress")
			    						{
			    							Data = "3";
			    						}
			    						
			    						if(Data == "Unfinished")
			    						{
			    							Data = "4";
			    						}
			    						else if(Data == "Finished")
			    						{
			    							Data = "5";
			    						}
			    						else if(Data == "Closed")
			    						{
			    							Data = "6";
			    						}
			    						else if(Data == "Hold")
			    						{
			    							Data = "7";
			    						}
			                        
			                           $('#example tbody').empty();
			                           $('#example').DataTable().clear().draw();
			    		 				 $('#example').DataTable().destroy(); 
			    		 				AjaxCallForTicketTypeandStatusfileterdetails(Type,Data);

			                       }
			                   }
			               }

		 			} ]
		 		});
		
			LoadDataTable(ticketgriddata);
			 $(".theme-loader").hide();
		
		},
	error:function(msg) { 
		
		
		}
		}); 
}
function AjaxCallForStateandTypeDetails(){
	try
	{
		if(typeof ajax_request3 !== 'undefined')
		{
			ajax_request3.abort();
		}
		
	}
	catch(err)
	{
		
	}
	ajax_request3 =  $.ajax({
		type: 'GET',
		url: './slide1',
		success: function(msg){
		var chartdata1=msg.chart1;
		var chartdata2=msg.chart2;
		var chartdata3=msg.chart3;
		
		
		 var seriesdata1 = eval("[" + chartdata1 + "]");
		 var seriesdata2 = eval("[" + chartdata2 + "]");
		 var seriesdata3 = eval("[" + chartdata3 + "]");
		  
		
		 Highcharts.chart('Chartcontainer1', {
	          chart: {
	        	  type: 'pie',
	        	  backgroundColor:'transparent',
	 	         options3d: {
	 	             enabled: true,
	 	           alpha: 15
	 	         }
		 
	          },
	          title: {
	              text: ''
	          },
	          legend: {
	         itemStyle: {
	             color: 'black',
	             fontWeight: 'bold',
	             fontSize: '20px'
	         }
	     },
	          tooltip: {
	              pointFormat: '{point.y}'
	          },
	          credits: {
	        	    enabled: false
	        	  },
	          plotOptions: {
	        	  pie: {
	            	   innerSize: 120,
	    	             depth: 40,
	    	             showInLegend:true,
	                  
	                   dataLabels: {
	                       enabled: false,
	                       format: '{point.name}'
	                   }
	               }
	             
	          
	          },
	          lang: {
		            noData: "No Data Found"
		        },
		        noData: {
		            style: {
		                fontWeight: 'bold',
		                fontSize: '25px',
		                color: '#303030'
		            }
		        },
	          legend:{
	        	   layout:'vertical', 
	        	   align:'right',
	        	  
	        	   verticalAlign:'bottom'
	        	 
	           },
	          exporting: {
			        menuItemDefinitions: {
			            // Custom definition
			            label: {
			                onclick: function () {
			                    this.renderer.label(
			                        'You just clicked a custom menu item',
			                        100,
			                        100
			                    )
			                    .attr({
			                        fill: '#a4edba',
			                        r: 5,
			                        padding: 10,
			                        zIndex: 10
			                    })
			                    .css({
			                        fontSize: '1.5em'
			                    })
			                    .add();
			                },
			              
			            }
			        },
			        filename: 'Total Tickets' + '_' +  dateTime,
			        buttons: {
			            contextButton: {
			                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
			            }
			        }
			    },
	          series: [{
	              type: 'pie',
	              name: 'Count',
	               data: seriesdata1,
	              
	               point: {
	                   events: {
	                       click: function () {
	                           //alert('Category: ' + this.name + ', value: ' + this.y);
	                           var Data = (this.name);
	                          
	                           if(Data == "Open")
	               			{
	                        	   Data = 'State=1 and ticketstatus not in(3)';
	               			}
	               			else if(Data == "Closed")
	               			{
	               				Data = 'State=2 and ticketstatus not in(3)';
	               			}
	               			else if(Data == "Hold")
	               			{
	               				Data = 'State=3 and ticketstatus not in(3)';
	               			}
	               			else if(Data == "Inprogress")
	               			{
	               				Data = 'Ticketstatus=3';
	               			}
	                          /*  alert(Dtaa); */
	                           $('#example tbody').empty();
	                           $('#example').DataTable().clear().draw();
	    		 				 $('#example').DataTable().destroy(); 
	                           AjaxCallForOpenTicketfileterdetails(Data);

	                       }
	                   }
	               }
	          }]
	      });
		
		 
		 Highcharts.chart('Chartcontainer2', {
	          chart: {
	             
	              type: 'pie',
	              backgroundColor:'transparent',
	 	         options3d: {
	 	             enabled: true,
	 	           alpha: 15
	 	         }
	          },
	          title: {
	              text: ''
	          },
	          legend: {
	         itemStyle: {
	             color: 'black',
	             fontWeight: 'bold',
	             fontSize: '20px'
	         }
	     },
	     lang: {
	            noData: "No Data Found"
	        },
	        noData: {
	            style: {
	                fontWeight: 'bold',
	                fontSize: '25px',
	                color: '#303030'
	            }
	        },
	          tooltip: {
	              pointFormat: '{point.y}'
	          },
	          credits: {
	        	    enabled: false
	        	  },
	          plotOptions: {
	        	  pie: {
	            	   innerSize: 120,
	    	             depth: 40,
	    	             showInLegend:true,
	    	           
	                  
	                   dataLabels: {
	                       enabled: false,
	                       format: '{point.name}'
	                   }
	               }
	             
	          
	          },
	          legend:{
	        	   layout:'vertical', 
	        	   align:'right',
	        	  
	        	   verticalAlign:'bottom'
	        	 
	           },
	          exporting: {
			        menuItemDefinitions: {
			            // Custom definition
			            label: {
			                onclick: function () {
			                    this.renderer.label(
			                        'You just clicked a custom menu item',
			                        100,
			                        100
			                    )
			                    .attr({
			                        fill: '#a4edba',
			                        r: 5,
			                        padding: 10,
			                        zIndex: 10
			                    })
			                    .css({
			                        fontSize: '1.5em'
			                    })
			                    .add();
			                },
			              
			            }
			        },
			        filename: 'Operations Tickets' + '_' +  dateTime,
			        buttons: {
			            contextButton: {
			                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
			            }
			        }
			    },
	          series: [{
	              type: 'pie',
	              name: 'Count',
	               data: seriesdata2,
	              
	               point: {
	                   events: {
	                       click: function () {
	                           //alert('Category: ' + this.name + ', value: ' + this.y);
	                           var Data = (this.name);
	                           var Type="Operation";
	                        
	                           if(Data == "Open")
		               			{
		                        	   Data = 'State=1 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Closed")
		               			{
		               				Data = 'State=2 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Hold")
		               			{
		               				Data = 'State=3 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Inprogress")
		               			{
		               				Data = 'Ticketstatus=3';
		               			}
	                          /*  alert(Dtaa); */
	                           $('#example tbody').empty();
	                           $('#example').DataTable().clear().draw();
	    		 				 $('#example').DataTable().destroy(); 
	    		 				AjaxCallForTicketTypefileterdetails(Type,Data);

	                       }
	                   }
	               }
	          }]
	      });
		  
		
		  Highcharts.chart('Chartcontainer3', {
	          chart: {
	             
	              type: 'pie',
	              backgroundColor:'transparent',
	 	         options3d: {
	 	             enabled: true,
	 	           alpha: 15
	 	         }
	          },
	          title: {
	              text: ''
	          },
	          legend: {
	         itemStyle: {
	             color: 'black',
	             fontWeight: 'bold',
	             fontSize: '20px'
	         }
	     },
	     lang: {
	            noData: "No Data Found"
	        },
	        noData: {
	            style: {
	                fontWeight: 'bold',
	                fontSize: '25px',
	                color: '#303030'
	            }
	        },
	          tooltip: {
	              pointFormat: '{point.y}'
	          },
	          credits: {
	        	    enabled: false
	        	  },
	          plotOptions: {
	        	  pie: {
	            	   innerSize: 120,
	    	             depth: 40,
	    	             showInLegend:true,
	    	           
	                  
	                   dataLabels: {
	                       enabled: false,
	                       format: '{point.name}'
	                   }
	               }
	             
	          
	          },
	          legend:{
	        	   layout:'vertical', 
	        	   align:'right',
	        	  
	        	   verticalAlign:'bottom'
	        	 
	           },
	          exporting: {
			        menuItemDefinitions: {
			            // Custom definition
			            label: {
			                onclick: function () {
			                    this.renderer.label(
			                        'You just clicked a custom menu item',
			                        100,
			                        100
			                    )
			                    .attr({
			                        fill: '#a4edba',
			                        r: 5,
			                        padding: 10,
			                        zIndex: 10
			                    })
			                    .css({
			                        fontSize: '1.5em'
			                    })
			                    .add();
			                },
			              
			            }
			        },
			        filename: 'Maintenance Tickets' + '_' +  dateTime,
			        buttons: {
			            contextButton: {
			                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
			            }
			        }
			    },
	          series: [{
	              type: 'pie',
	              name: 'Count',
	               data: seriesdata3,
	              
	               point: {
	                   events: {
	                       click: function () {
	                           //alert('Category: ' + this.name + ', value: ' + this.y);
	                           var Data = (this.name);
	                           var Type="Maintenance";
	                         
	                           if(Data == "Open")
		               			{
		                        	   Data = 'State=1 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Closed")
		               			{
		               				Data = 'State=2 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Hold")
		               			{
		               				Data = 'State=3 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Inprogress")
		               			{
		               				Data = 'Ticketstatus=3';
		               			}
	                          /*  alert(Dtaa); */
	                           $('#example tbody').empty();
	                           $('#example').DataTable().clear().draw();
	    		 				 $('#example').DataTable().destroy(); 
	    		 				AjaxCallForTicketTypefileterdetails(Type,Data);

	                       }
	                   }
	               }
	          }]
	      });
		
		
		
		
		
		},
	error:function(msg) { 
		
		
		}
		}); 
}


function AjaxCallForStateandStatusDetailsBySite(siteid){
	try
	{
		if(typeof ajax_request2 !== 'undefined')
		{
			ajax_request2.abort();
		}
		
	}
	catch(err)
	{
		
	}
	ajax_request2 =  $.ajax({
		type: 'GET',
		url: './slide2BySite',
		data: {'SiteId' :siteid },
		success: function(msg){
		var chartdata1=msg.chart1;
		var chartdata2=msg.chart2;
		var chartdata3=msg.chart3;
		var sitedetails=msg.sitedetails;
		var ticketgriddata =msg.ticketgriddata;
		
	
		 var seriesdata1 = eval("[" + chartdata1 + "]");
		 var seriesdata2 = eval("[" + chartdata2 + "]");
		 var seriesdata3 = eval("[" + chartdata3 + "]");
		
 	var siteList = msg.siteList;
		 
		
		 $.map(siteList, function(val, key) {                                    		 
	   		 $( "#ddlSiteList" ).append("<option value="+ key +">" + val +"</option>");
	   		});
		 
		 Highcharts
			.chart(
					'Chartcontainer4',
					{
						chart : {
							renderTo:'Chartcontainer4',
							backgroundColor:'transparent',
							type : 'column',
							options3d: {
 					            enabled: true,
 					            alpha: 4,
 					            beta: 11,
 					            depth: 50,
 					            viewDistance: 25
 					        } 
 					 
						},
						xAxis : {
							// minorGridLineWidth: 0,
						type:'category',
						 lineWidth:1,
						 gridLineWidth: 0

						},
						lang: {
		  		            noData: "No Data Found"
		  		        },
		  		        noData: {
		  		            style: {
		  		                fontWeight: 'bold',
		  		                fontSize: '25px',
		  		                color: '#303030'
		  		            }
		  		        },
						yAxis : {
							 lineWidth:1,
							 gridLineWidth: 0,
							 allowDecimals: false,
							title : {
								text : ' ',
								 style: {
						                color: '#000',
						                fontWeight:'bold'
						            }
							}
						},
						title : {
							text : ''
						},
						subtitle : {
							text : ''
						},
						 credits: {
				      	        enabled: false
				      	    },
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{categories.name} </td>'
									+ '<td style="padding:0"><b>{point.y}</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								depth:25,
								
								events : {
									legendItemClick : function() {
										return false;
									}
								}
							},
							 series: {
				                    
				                    cursor: 'pointer'
				                
				                }
						},
						exporting: {
					        menuItemDefinitions: {
					            // Custom definition
					            label: {
					                onclick: function () {
					                    this.renderer.label(
					                        'You just clicked a custom menu item',
					                        100,
					                        100
					                    )
					                    .attr({
					                        fill: '#a4edba',
					                        r: 5,
					                        padding: 10,
					                        zIndex: 10
					                    })
					                    .css({
					                        fontSize: '1.5em'
					                    })
					                    .add();
					                },
					              
					            }
					        },
					        filename: 'Open Tickets_'+ sitedetails.siteName +'_' +  dateTime,
					        
					        buttons: {
					            contextButton: {
					                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
					            }
					        }
					    },
					  
						series : [ {
							showInLegend: false,  
		                	 name: ' ',
							data : seriesdata2,
							  point: {
				                   events: {
				                       click: function () {
				                           //alert('Category: ' + this.name + ', value: ' + this.y);
				                           var Data = (this.name);
				                           var Type="1";
				                         
				                           if(Data == "Created")
				    						{
				                        	   Data = "1";
				    						}
				    						else if(Data == "Assigned")
				    						{
				    							Data = "2";
				    						}
				    						else if(Data == "Inprogress")
				    						{
				    							Data = "3";
				    						}
				    						
				    						if(Data == "Unfinished")
				    						{
				    							Data = "4";
				    						}
				    						else if(Data == "Finished")
				    						{
				    							Data = "5";
				    						}
				    						else if(Data == "Closed")
				    						{
				    							Data = "6";
				    						}
				    						else if(Data == "Hold")
				    						{
				    							Data = "7";
				    						}
				                        
				                           $('#example tbody').empty();
				                           $('#example').DataTable().clear().draw();
				    		 				 $('#example').DataTable().destroy(); 
				    		 				AjaxCallForTicketTypeandStatusfileterdetailsBySite(Type,Data,siteid);

				                       }
				                   }
				               }

						} ]
					});
		
		 
		 
		
		
		 Highcharts
		 .chart('Chartcontainer5',{
		 			chart : {
		 				renderTo:'Chartcontainer5',
		 				backgroundColor:'transparent',
		 				type : 'column',
		 				 
		 				options3d: {
					            enabled: true,
					            alpha: 4,
					            beta: 11,
					            depth: 50,
					            viewDistance: 25
					        } 
		 					 

		 			},
		 			xAxis : {
		 				// minorGridLineWidth: 0,
		 				 type: 'category',
		 					 lineWidth:1,
							 gridLineWidth: 0
							
		 			},
		 			lang: {
	  		            noData: "No Data Found"
	  		        },
	  		        noData: {
	  		            style: {
	  		                fontWeight: 'bold',
	  		                fontSize: '25px',
	  		                color: '#303030'
	  		            }
	  		        },
		 			yAxis : {
		 				 lineWidth:1,
						 gridLineWidth: 0,
						 allowDecimals: false,
		 				title : {
		 					text : ' ',
		 					 style: {
		 			                color: '#000',
		 			                fontWeight:'bold'
		 			            }
		 				}
		 			},
		 			title : {
		 				text : ''
		 			},
		 			subtitle : {
		 				text : ''
		 			},
		 			 credits: {
		 	      	        enabled: false
		 	      	    },
		 			tooltip : {
		 				headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
		 				pointFormat : '<tr><td style="color:{series.color};padding:0">{categories.name} </td>'
		 						+ '<td style="padding:0"><b>{point.y}</b></td></tr>',
		 				footerFormat : '</table>',
		 				shared : true,
		 				useHTML : true
		 			},
		 			plotOptions : {
		 				column : {
		 					depth:25,
		 					borderWidth : 0
		 					
		 					
		 				}
		 				
		 			},
		 			exporting: {
		 		        menuItemDefinitions: {
		 		            // Custom definition
		 		            label: {
		 		                onclick: function () {
		 		                    this.renderer.label(
		 		                        'You just clicked a custom menu item',
		 		                        100,
		 		                        100
		 		                    )
		 		                    .attr({
		 		                        fill: '#a4edba',
		 		                        r: 5,
		 		                        padding: 10,
		 		                        zIndex: 10
		 		                    })
		 		                    .css({
		 		                        fontSize: '1.5em'
		 		                    })
		 		                    .add();
		 		                },
		 		              
		 		            }
		 		        },
		 		       filename: 'Close Tickets_'+ sitedetails.siteName +'_' +  dateTime,
		 		        buttons: {
		 		            contextButton: {
		 		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		 		            }
		 		        }
		 		    },
		 		  
		 			series : [ {
		 				showInLegend: false,  
	                	 name: ' ',
		 				data : seriesdata3,
		 				  point: {
			                   events: {
			                       click: function () {
			                           //alert('Category: ' + this.name + ', value: ' + this.y);
			                           var Data = (this.name);
			                           var Type="2";
			                         
			                           if(Data == "Created")
			    						{
			                        	   Data = "1";
			    						}
			    						else if(Data == "Assigned")
			    						{
			    							Data = "2";
			    						}
			    						else if(Data == "Inprogress")
			    						{
			    							Data = "3";
			    						}
			    						
			    						if(Data == "Unfinished")
			    						{
			    							Data = "4";
			    						}
			    						else if(Data == "Finished")
			    						{
			    							Data = "5";
			    						}
			    						else if(Data == "Closed")
			    						{
			    							Data = "6";
			    						}
			    						else if(Data == "Hold")
			    						{
			    							Data = "7";
			    						}
			                        
			                           $('#example tbody').empty();
			                           $('#example').DataTable().clear().draw();
			    		 				 $('#example').DataTable().destroy(); 
			    		 				AjaxCallForTicketTypeandStatusfileterdetailsBySite(Type,Data,siteid);

			                       }
			                   }
			               }

		 			} ]
		 		});
		 Highcharts
		 .chart('Chartcontainer6',{
		 			chart : {
		 				renderTo:'Chartcontainer6',
		 				backgroundColor:'transparent',
		 				type : 'column',
		 				 
		 				options3d: {
					            enabled: true,
					            alpha: 4,
					            beta: 11,
					            depth: 50,
					            viewDistance: 25
					        } 
		 					 

		 			},
		 			xAxis : {
		 				// minorGridLineWidth: 0,
		 				 type: 'category',
		 				 lineWidth:1,
						 gridLineWidth: 0

		 			},
		 			lang: {
	  		            noData: "No Data Found"
	  		        },
	  		        noData: {
	  		            style: {
	  		                fontWeight: 'bold',
	  		                fontSize: '25px',
	  		                color: '#303030'
	  		            }
	  		        },
		 			yAxis : {
		 				 lineWidth:1,
						 gridLineWidth: 0,
						 allowDecimals: false,
		 				title : {
		 					text : ' ',
		 					 style: {
		 			                color: '#000',
		 			                fontWeight:'bold'
		 			            }
		 				}
		 			},
		 			title : {
		 				text : ''
		 			},
		 			subtitle : {
		 				text : ''
		 			},
		 			 credits: {
		 	      	        enabled: false
		 	      	    },
		 			tooltip : {
		 				headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
		 				pointFormat : '<tr><td style="color:{series.color};padding:0">{categories.name} </td>'
		 						+ '<td style="padding:0"><b>{point.y}</b></td></tr>',
		 				footerFormat : '</table>',
		 				shared : true,
		 				useHTML : true
		 			},
		 			plotOptions : {
		 				column : {
		 					depth:25,
		 					borderWidth : 0
		 					
		 					
		 				}
		 				
		 			},
		 			exporting: {
		 		        menuItemDefinitions: {
		 		            // Custom definition
		 		            label: {
		 		                onclick: function () {
		 		                    this.renderer.label(
		 		                        'You just clicked a custom menu item',
		 		                        100,
		 		                        100
		 		                    )
		 		                    .attr({
		 		                        fill: '#a4edba',
		 		                        r: 5,
		 		                        padding: 10,
		 		                        zIndex: 10
		 		                    })
		 		                    .css({
		 		                        fontSize: '1.5em'
		 		                    })
		 		                    .add();
		 		                },
		 		              
		 		            }
		 		        },
		 		       filename: 'Hold Tickets_'+ sitedetails.siteName +'_' +  dateTime,
		 		        buttons: {
		 		            contextButton: {
		 		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		 		            }
		 		        }
		 		    },
		 		  
		 			series : [ {
		 				showInLegend: false,  
	                	 name: ' ',
		 				data : seriesdata1,
		 				  point: {
			                   events: {
			                       click: function () {
			                           //alert('Category: ' + this.name + ', value: ' + this.y);
			                           var Data = (this.name);
			                           var Type="3";
			                         
			                           if(Data == "Created")
			    						{
			                        	   Data = "1";
			    						}
			    						else if(Data == "Assigned")
			    						{
			    							Data = "2";
			    						}
			    						else if(Data == "Inprogress")
			    						{
			    							Data = "3";
			    						}
			    						
			    						if(Data == "Unfinished")
			    						{
			    							Data = "4";
			    						}
			    						else if(Data == "Finished")
			    						{
			    							Data = "5";
			    						}
			    						else if(Data == "Closed")
			    						{
			    							Data = "6";
			    						}
			    						else if(Data == "Hold")
			    						{
			    							Data = "7";
			    						}
			                        
			                           $('#example tbody').empty();
			                           $('#example').DataTable().clear().draw();
			    		 				 $('#example').DataTable().destroy(); 
			    		 				AjaxCallForTicketTypeandStatusfileterdetailsBySite(Type,Data,siteid);

			                       }
			                   }
			               }

		 			} ]
		 		});
		
			LoadDataTable(ticketgriddata);
		
		},
	error:function(msg) { 
		
		
		}
		}); 
}
function AjaxCallForStateandTypeDetailsBySite(siteid){
	try
	{
		if(typeof ajax_request3 !== 'undefined')
		{
			ajax_request3.abort();
		}
		
	}
	catch(err)
	{
		
	}
	ajax_request3 =  $.ajax({
		type: 'GET',
		url: './slide1BySite',
		data: {'SiteId' :siteid },
		success: function(msg){
		var chartdata1=msg.chart1;
		var chartdata2=msg.chart2;
		var chartdata3=msg.chart3;
		var sitedetails=msg.sitedetails;
		
		 var seriesdata1 = eval("[" + chartdata1 + "]");
		 var seriesdata2 = eval("[" + chartdata2 + "]");
		 var seriesdata3 = eval("[" + chartdata3 + "]");
		  
		
		  Highcharts.chart('Chartcontainer1', {
	          chart: {
	        	  type: 'pie',
	        	  backgroundColor:'transparent',
	 	         options3d: {
	 	             enabled: true,
	 	           alpha: 15
	 	         }
		 
	          },
	          title: {
	              text: ''
	          },
	          lang: {
		            noData: "No Data Found"
		        },
		        noData: {
		            style: {
		                fontWeight: 'bold',
		                fontSize: '25px',
		                color: '#303030'
		            }
		        },
	          legend: {
	         itemStyle: {
	             color: 'black',
	             fontWeight: 'bold',
	             fontSize: '20px'
	         }
	     },
	          tooltip: {
	              pointFormat: '{point.y}'
	          },
	          credits: {
	        	    enabled: false
	        	  },
	          plotOptions: {
	        	  pie: {
	            	   innerSize: 120,
	    	             depth: 40,
	    	             showInLegend:true,
	                  
	                   dataLabels: {
	                       enabled: false,
	                       format: '{point.name}'
	                   }
	               }
	             
	          
	          },
	          legend:{
	        	   layout:'vertical', 
	        	   align:'right',
	        	  
	        	   verticalAlign:'bottom'
	        	 
	           },
	          exporting: {
			        menuItemDefinitions: {
			            // Custom definition
			            label: {
			                onclick: function () {
			                    this.renderer.label(
			                        'You just clicked a custom menu item',
			                        100,
			                        100
			                    )
			                    .attr({
			                        fill: '#a4edba',
			                        r: 5,
			                        padding: 10,
			                        zIndex: 10
			                    })
			                    .css({
			                        fontSize: '1.5em'
			                    })
			                    .add();
			                },
			              
			            }
			        },
			        filename: 'Total Tickets_'+ sitedetails.siteName +'_' +  dateTime,
			        buttons: {
			            contextButton: {
			                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
			            }
			        }
			    },
	          series: [{
	              type: 'pie',
	              name: '',
	               data: seriesdata1,
	               point: {
	                   events: {
	                       click: function () {
	                           //alert('Category: ' + this.name + ', value: ' + this.y);
	                           var Data = (this.name);
	                         
	                           if(Data == "Open")
		               			{
		                        	   Data = 'State=1 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Closed")
		               			{
		               				Data = 'State=2 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Hold")
		               			{
		               				Data = 'State=3 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Inprogress")
		               			{
		               				Data = 'Ticketstatus=3';
		               			}
	                          /*  alert(Dtaa); */
	                           $('#example tbody').empty();
	                           $('#example').DataTable().clear().draw();
	    		 				 $('#example').DataTable().destroy(); 
	                           AjaxCallForOpenTicketfileterdetailsBySite(Data,siteid);

	                       }
	                   }
	               }
	          }]
	      });
		
		 
		 Highcharts.chart('Chartcontainer2', {
	          chart: {
	             
	              type: 'pie',
	              backgroundColor:'transparent',
	 	         options3d: {
	 	             enabled: true,
	 	           alpha: 15
	 	         }
	          },
	          title: {
	              text: ''
	          },
	          lang: {
		            noData: "No Data Found"
		        },
		        noData: {
		            style: {
		                fontWeight: 'bold',
		                fontSize: '25px',
		                color: '#303030'
		            }
		        },
	          legend: {
	         itemStyle: {
	             color: 'black',
	             fontWeight: 'bold',
	             fontSize: '20px'
	         }
	     },
	          tooltip: {
	              pointFormat: '{point.y}'
	          },
	          credits: {
	        	    enabled: false
	        	  },
	        	  plotOptions: {
		        	  pie: {
		            	   innerSize: 120,
		    	             depth: 40,
		    	             showInLegend:true,
		                  
		                   dataLabels: {
		                       enabled: false,
		                       format: '{point.name}'
		                   }
		               }
		             
		          
		          },
		          legend:{
		        	   layout:'vertical', 
		        	   align:'right',
		        	  
		        	   verticalAlign:'bottom'
		        	 
		           },
	          exporting: {
			        menuItemDefinitions: {
			            // Custom definition
			            label: {
			                onclick: function () {
			                    this.renderer.label(
			                        'You just clicked a custom menu item',
			                        100,
			                        100
			                    )
			                    .attr({
			                        fill: '#a4edba',
			                        r: 5,
			                        padding: 10,
			                        zIndex: 10
			                    })
			                    .css({
			                        fontSize: '1.5em'
			                    })
			                    .add();
			                },
			              
			            }
			        },
			        filename: 'Operations Tickets_'+ sitedetails.siteName +'_' +  dateTime,
			        buttons: {
			            contextButton: {
			                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
			            }
			        }
			    },
	          series: [{
	              type: 'pie',
	              name: 'Count',
	               data: seriesdata2,
	               point: {
	                   events: {
	                       click: function () {
	                           //alert('Category: ' + this.name + ', value: ' + this.y);
	                           var Data = (this.name);
	                           var Type="Operation";
	                          
	                           if(Data == "Open")
		               			{
		                        	   Data = 'State=1 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Closed")
		               			{
		               				Data = 'State=2 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Hold")
		               			{
		               				Data = 'State=3 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Inprogress")
		               			{
		               				Data = 'Ticketstatus=3';
		               			}
	                          /*  alert(Dtaa); */
	                           $('#example tbody').empty();
	                           $('#example').DataTable().clear().draw();
	    		 				 $('#example').DataTable().destroy(); 
	    		 				AjaxCallForTicketTypefileterdetailsBySite(Type,Data,siteid);

	                       }
	                   }
	               }
	          }]
	      });
		  
		
		 Highcharts.chart('Chartcontainer3', {
	          chart: {
	             
	              type: 'pie',
	              backgroundColor:'transparent',
	 	         options3d: {
	 	             enabled: true,
	 	           alpha: 15
	 	         }
	          },
	          title: {
	              text: ''
	          },
	          lang: {
		            noData: "No Data Found"
		        },
		        noData: {
		            style: {
		                fontWeight: 'bold',
		                fontSize: '25px',
		                color: '#303030'
		            }
		        },
	          legend: {
	         itemStyle: {
	             color: 'black',
	             fontWeight: 'bold',
	             fontSize: '20px'
	         }
	     },
	          tooltip: {
	              pointFormat: '{point.y}'
	          },
	          credits: {
	        	    enabled: false
	        	  },
	        	  plotOptions: {
		        	  pie: {
		            	   innerSize: 120,
		    	             depth: 40,
		    	             showInLegend:true,
		                  
		                   dataLabels: {
		                       enabled: false,
		                       format: '{point.name}'
		                   }
		               }
		             
		          
		          },
		          legend:{
		        	   layout:'vertical', 
		        	   align:'right',
		        	  
		        	   verticalAlign:'bottom'
		        	 
		           },
	          exporting: {
			        menuItemDefinitions: {
			            // Custom definition
			            label: {
			                onclick: function () {
			                    this.renderer.label(
			                        'You just clicked a custom menu item',
			                        100,
			                        100
			                    )
			                    .attr({
			                        fill: '#a4edba',
			                        r: 5,
			                        padding: 10,
			                        zIndex: 10
			                    })
			                    .css({
			                        fontSize: '1.5em'
			                    })
			                    .add();
			                },
			              
			            }
			        },
			        filename: 'Maintenance Tickets_'+ sitedetails.siteName +'_' +  dateTime,
			        buttons: {
			            contextButton: {
			                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
			            }
			        }
			    },
	          series: [{
	              type: 'pie',
	              name: 'Count',
	               data: seriesdata3,
	              
	               point: {
	                   events: {
	                       click: function () {
	                           //alert('Category: ' + this.name + ', value: ' + this.y);
	                           var Data = (this.name);
	                           var Type="Maintenance";
	                         
	                           if(Data == "Open")
		               			{
		                        	   Data = 'State=1 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Closed")
		               			{
		               				Data = 'State=2 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Hold")
		               			{
		               				Data = 'State=3 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Inprogress")
		               			{
		               				Data = 'Ticketstatus=3';
		               			}
	                          /*  alert(Dtaa); */
	                           $('#example tbody').empty();
	                           $('#example').DataTable().clear().draw();
	    		 				 $('#example').DataTable().destroy(); 
	    		 				AjaxCallForTicketTypefileterdetailsBySite(Type,Data,siteid);

	                       }
	                   }
	               }
	          }]
	      });
		
		
		
		
		
		},
	error:function(msg) { 
		
		
		}
		}); 
}


function AjaxCallForStateandStatusDetailsByCustomer(customerId){
	try
	{
		if(typeof ajax_request2 !== 'undefined')
		{
			ajax_request2.abort();
		}
		
	}
	catch(err)
	{
		
	}
	ajax_request2 =  $.ajax({
		type: 'GET',
		url: './slide2ByCustomer',
		data: {'CustomerId' :customerId },
		success: function(msg){
		var chartdata1=msg.chart1;
		var chartdata2=msg.chart2;
		var chartdata3=msg.chart3;
		var customerdetails=msg.customerdetails;
		
		var ticketgriddata =msg.ticketgriddata;
		
	
		 var seriesdata1 = eval("[" + chartdata1 + "]");
		 var seriesdata2 = eval("[" + chartdata2 + "]");
		 var seriesdata3 = eval("[" + chartdata3 + "]");
		
 		var siteList = msg.siteList;
		 
		 $( "#ddlSiteList" ).append("<option value='0'>All Sites</option>");
		 $.map(siteList, function(val, key) {                                    		 
	   		 $( "#ddlSiteList" ).append("<option value="+ key +">" + val +"</option>");
	   		});
		 
		 Highcharts
			.chart(
					'Chartcontainer4',
					{
						chart : {
							renderTo:'Chartcontainer4',
							backgroundColor:'transparent',
							type : 'column',
							options3d: {
 					            enabled: true,
 					            alpha: 4,
 					            beta: 11,
 					            depth: 50,
 					            viewDistance: 25
 					        } 
 					 
						},
						xAxis : {
							// minorGridLineWidth: 0,
						type:'category',
						 lineWidth:1,
						 gridLineWidth: 0

						},
						lang: {
		  		            noData: "No Data Found"
		  		        },
		  		        noData: {
		  		            style: {
		  		                fontWeight: 'bold',
		  		                fontSize: '25px',
		  		                color: '#303030'
		  		            }
		  		        },
						yAxis : {
							 lineWidth:1,
							 gridLineWidth: 0,
							 allowDecimals: false,
							title : {
								text : ' ',
								 style: {
						                color: '#000',
						                fontWeight:'bold'
						            }
							}
						},
						title : {
							text : ''
						},
						subtitle : {
							text : ''
						},
						 credits: {
				      	        enabled: false
				      	    },
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{categories.name} </td>'
									+ '<td style="padding:0"><b>{point.y}</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								depth:25,
								
								events : {
									legendItemClick : function() {
										return false;
									}
								}
							},
							 series: {
				                    
				                    cursor: 'pointer'
				                
				                }
						},
						exporting: {
					        menuItemDefinitions: {
					            // Custom definition
					            label: {
					                onclick: function () {
					                    this.renderer.label(
					                        'You just clicked a custom menu item',
					                        100,
					                        100
					                    )
					                    .attr({
					                        fill: '#a4edba',
					                        r: 5,
					                        padding: 10,
					                        zIndex: 10
					                    })
					                    .css({
					                        fontSize: '1.5em'
					                    })
					                    .add();
					                },
					              
					            }
					        },
					        filename: 'Open Tickets_'+ customerdetails.customerName +'_' +  dateTime,
					        buttons: {
					            contextButton: {
					                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
					            }
					        }
					    },
					  
						series : [ {
							showInLegend: false,  
		                	 name: ' ',
							data : seriesdata2,
							 point: {
				                   events: {
				                       click: function () {
				                           //alert('Category: ' + this.name + ', value: ' + this.y);
				                           var Data = (this.name);
				                           var Type="1";
				                         
				                           if(Data == "Created")
				    						{
				                        	   Data = "1";
				    						}
				    						else if(Data == "Assigned")
				    						{
				    							Data = "2";
				    						}
				    						else if(Data == "Inprogress")
				    						{
				    							Data = "3";
				    						}
				    						
				    						if(Data == "Unfinished")
				    						{
				    							Data = "4";
				    						}
				    						else if(Data == "Finished")
				    						{
				    							Data = "5";
				    						}
				    						else if(Data == "Closed")
				    						{
				    							Data = "6";
				    						}
				    						else if(Data == "Hold")
				    						{
				    							Data = "7";
				    						}
				                        
				                           $('#example tbody').empty();
				                           $('#example').DataTable().clear().draw();
				    		 				 $('#example').DataTable().destroy(); 
				    		 				AjaxCallForTicketTypeandStatusfileterdetailsByCustomer(Type,Data,customerId);

				                       }
				                   }
				               }

						} ]
					});
		
		 
		 
		
		
		 Highcharts
		 .chart('Chartcontainer5',{
		 			chart : {
		 				renderTo:'Chartcontainer5',
		 				backgroundColor:'transparent',
		 				type : 'column',
		 				 
		 				options3d: {
					            enabled: true,
					            alpha: 4,
					            beta: 11,
					            depth: 50,
					            viewDistance: 25
					        } 
		 					 

		 			},
		 			lang: {
	  		            noData: "No Data Found"
	  		        },
	  		        noData: {
	  		            style: {
	  		                fontWeight: 'bold',
	  		                fontSize: '25px',
	  		                color: '#303030'
	  		            }
	  		        },
		 			xAxis : {
		 				// minorGridLineWidth: 0,
		 				 type: 'category',
		 					 lineWidth:1,
							 gridLineWidth: 0
							
		 			},
		 			yAxis : {
		 				 lineWidth:1,
						 gridLineWidth: 0,
						 allowDecimals: false,
		 				title : {
		 					text : ' ',
		 					 style: {
		 			                color: '#000',
		 			                fontWeight:'bold'
		 			            }
		 				}
		 			},
		 			title : {
		 				text : ''
		 			},
		 			subtitle : {
		 				text : ''
		 			},
		 			 credits: {
		 	      	        enabled: false
		 	      	    },
		 			tooltip : {
		 				headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
		 				pointFormat : '<tr><td style="color:{series.color};padding:0">{categories.name} </td>'
		 						+ '<td style="padding:0"><b>{point.y}</b></td></tr>',
		 				footerFormat : '</table>',
		 				shared : true,
		 				useHTML : true
		 			},
		 			plotOptions : {
		 				column : {
		 					depth:25,
		 					borderWidth : 0
		 					
		 					
		 				}
		 				
		 			},
		 			exporting: {
		 		        menuItemDefinitions: {
		 		            // Custom definition
		 		            label: {
		 		                onclick: function () {
		 		                    this.renderer.label(
		 		                        'You just clicked a custom menu item',
		 		                        100,
		 		                        100
		 		                    )
		 		                    .attr({
		 		                        fill: '#a4edba',
		 		                        r: 5,
		 		                        padding: 10,
		 		                        zIndex: 10
		 		                    })
		 		                    .css({
		 		                        fontSize: '1.5em'
		 		                    })
		 		                    .add();
		 		                },
		 		              
		 		            }
		 		        },
		 		       filename: 'Close Tickets_'+ customerdetails.customerName +'_' +  dateTime,
		 		        buttons: {
		 		            contextButton: {
		 		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		 		            }
		 		        }
		 		    },
		 		  
		 			series : [ {
		 				showInLegend: false,  
	                	 name: ' ',
		 				data : seriesdata3,
		 				 point: {
			                   events: {
			                       click: function () {
			                           //alert('Category: ' + this.name + ', value: ' + this.y);
			                           var Data = (this.name);
			                           var Type="2";
			                         
			                           if(Data == "Created")
			    						{
			                        	   Data = "1";
			    						}
			    						else if(Data == "Assigned")
			    						{
			    							Data = "2";
			    						}
			    						else if(Data == "Inprogress")
			    						{
			    							Data = "3";
			    						}
			    						
			    						if(Data == "Unfinished")
			    						{
			    							Data = "4";
			    						}
			    						else if(Data == "Finished")
			    						{
			    							Data = "5";
			    						}
			    						else if(Data == "Closed")
			    						{
			    							Data = "6";
			    						}
			    						else if(Data == "Hold")
			    						{
			    							Data = "7";
			    						}
			                        
			                           $('#example tbody').empty();
			                           $('#example').DataTable().clear().draw();
			    		 				 $('#example').DataTable().destroy(); 
			    		 				AjaxCallForTicketTypeandStatusfileterdetailsByCustomer(Type,Data,customerId);

			                       }
			                   }
			               }

		 			} ]
		 		});
		 Highcharts
		 .chart('Chartcontainer6',{
		 			chart : {
		 				renderTo:'Chartcontainer6',
		 				backgroundColor:'transparent',
		 				type : 'column',
		 				 
		 				options3d: {
					            enabled: true,
					            alpha: 4,
					            beta: 11,
					            depth: 50,
					            viewDistance: 25
					        } 
		 					 

		 			},
		 			lang: {
	  		            noData: "No Data Found"
	  		        },
	  		        noData: {
	  		            style: {
	  		                fontWeight: 'bold',
	  		                fontSize: '25px',
	  		                color: '#303030'
	  		            }
	  		        },
		 			xAxis : {
		 				// minorGridLineWidth: 0,
		 				 type: 'category',
		 				 lineWidth:1,
						 gridLineWidth: 0

		 			},
		 			yAxis : {
		 				 lineWidth:1,
						 gridLineWidth: 0,
						 allowDecimals: false,
		 				title : {
		 					text : ' ',
		 					 style: {
		 			                color: '#000',
		 			                fontWeight:'bold'
		 			            }
		 				}
		 			},
		 			title : {
		 				text : ''
		 			},
		 			subtitle : {
		 				text : ''
		 			},
		 			 credits: {
		 	      	        enabled: false
		 	      	    },
		 			tooltip : {
		 				headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
		 				pointFormat : '<tr><td style="color:{series.color};padding:0">{categories.name} </td>'
		 						+ '<td style="padding:0"><b>{point.y}</b></td></tr>',
		 				footerFormat : '</table>',
		 				shared : true,
		 				useHTML : true
		 			},
		 			plotOptions : {
		 				column : {
		 					depth:25,
		 					borderWidth : 0
		 					
		 					
		 				}
		 				
		 			},
		 			exporting: {
		 		        menuItemDefinitions: {
		 		            // Custom definition
		 		            label: {
		 		                onclick: function () {
		 		                    this.renderer.label(
		 		                        'You just clicked a custom menu item',
		 		                        100,
		 		                        100
		 		                    )
		 		                    .attr({
		 		                        fill: '#a4edba',
		 		                        r: 5,
		 		                        padding: 10,
		 		                        zIndex: 10
		 		                    })
		 		                    .css({
		 		                        fontSize: '1.5em'
		 		                    })
		 		                    .add();
		 		                },
		 		              
		 		            }
		 		        },
		 		       filename: 'Hold Tickets_'+ customerdetails.customerName +'_' +  dateTime,
		 		        buttons: {
		 		            contextButton: {
		 		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		 		            }
		 		        }
		 		    },
		 		  
		 			series : [ {
		 				showInLegend: false,  
	                	 name: ' ',
		 				data : seriesdata1,
		 				 point: {
			                   events: {
			                       click: function () {
			                           //alert('Category: ' + this.name + ', value: ' + this.y);
			                           var Data = (this.name);
			                           var Type="3";
			                         
			                           if(Data == "Created")
			    						{
			                        	   Data = "1";
			    						}
			    						else if(Data == "Assigned")
			    						{
			    							Data = "2";
			    						}
			    						else if(Data == "Inprogress")
			    						{
			    							Data = "3";
			    						}
			    						
			    						if(Data == "Unfinished")
			    						{
			    							Data = "4";
			    						}
			    						else if(Data == "Finished")
			    						{
			    							Data = "5";
			    						}
			    						else if(Data == "Closed")
			    						{
			    							Data = "6";
			    						}
			    						else if(Data == "Hold")
			    						{
			    							Data = "7";
			    						}
			                        
			                           $('#example tbody').empty();
			                           $('#example').DataTable().clear().draw();
			    		 				 $('#example').DataTable().destroy(); 
			    		 				AjaxCallForTicketTypeandStatusfileterdetailsByCustomer(Type,Data,customerId);

			                       }
			                   }
			               }

		 			} ]
		 		});
		
			LoadDataTable(ticketgriddata);
		
		},
	error:function(msg) { 
		
		
		}
		}); 
}
function AjaxCallForStateandTypeDetailsByCustomer(customerId){
	try
	{
		if(typeof ajax_request3 !== 'undefined')
		{
			ajax_request3.abort();
		}
		
	}
	catch(err)
	{
		
	}
	ajax_request3 =  $.ajax({
		type: 'GET',
		url: './slide1ByCustomer',
		data: {'CustomerId' :customerId },
		success: function(msg){
		var chartdata1=msg.chart1;
		var chartdata2=msg.chart2;
		var chartdata3=msg.chart3;
		var customerdetails=msg.customerdetails;
		
		 var seriesdata1 = eval("[" + chartdata1 + "]");
		 var seriesdata2 = eval("[" + chartdata2 + "]");
		 var seriesdata3 = eval("[" + chartdata3 + "]");
		  
		
		 Highcharts.chart('Chartcontainer1', {
	          chart: {
	        	  type: 'pie',
	        	  backgroundColor:'transparent',
	 	         options3d: {
	 	             enabled: true,
	 	           alpha: 15
	 	         }
		 
	          },
	          title: {
	              text: ''
	          },
	          legend: {
	         itemStyle: {
	             color: 'black',
	             fontWeight: 'bold',
	             fontSize: '20px'
	         }
	     },
	          tooltip: {
	              pointFormat: '{point.y}'
	          },
	          credits: {
	        	    enabled: false
	        	  },
	        	  lang: {
	  		            noData: "No Data Found"
	  		        },
	  		        noData: {
	  		            style: {
	  		                fontWeight: 'bold',
	  		                fontSize: '25px',
	  		                color: '#303030'
	  		            }
	  		        },
	          plotOptions: {
	        	  pie: {
	            	   innerSize: 120,
	    	             depth: 40,
	    	             showInLegend:true,
	                  
	                   dataLabels: {
	                       enabled: false,
	                       format: '{point.name}'
	                   }
	               }
	             
	          
	          },
	          legend:{
	        	   layout:'vertical', 
	        	   align:'right',
	        	  
	        	   verticalAlign:'bottom'
	        	 
	           },
	          exporting: {
			        menuItemDefinitions: {
			            // Custom definition
			            label: {
			                onclick: function () {
			                    this.renderer.label(
			                        'You just clicked a custom menu item',
			                        100,
			                        100
			                    )
			                    .attr({
			                        fill: '#a4edba',
			                        r: 5,
			                        padding: 10,
			                        zIndex: 10
			                    })
			                    .css({
			                        fontSize: '1.5em'
			                    })
			                    .add();
			                },
			              
			            }
			        },
			        filename: 'Total Tickets_'+ customerdetails.customerName +'_' +  dateTime,
			        buttons: {
			            contextButton: {
			                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
			            }
			        }
			    },
	          series: [{
	              type: 'pie',
	              name: '',
	               data: seriesdata1,
	               point: {
	                   events: {
	                       click: function () {
	                           //alert('Category: ' + this.name + ', value: ' + this.y);
	                           var Data = (this.name);
	                          
	                           if(Data == "Open")
		               			{
		                        	   Data = 'State=1 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Closed")
		               			{
		               				Data = 'State=2 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Hold")
		               			{
		               				Data = 'State=3 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Inprogress")
		               			{
		               				Data = 'Ticketstatus=3';
		               			}
	                          /*  alert(Dtaa); */
	                           $('#example tbody').empty();
	                           $('#example').DataTable().clear().draw();
	    		 				 $('#example').DataTable().destroy(); 
	                           AjaxCallForOpenTicketfileterdetailsByCustomer(Data,customerId);

	                       }
	                   }
	               }
	          }]
	      });
		
		 
		  Highcharts.chart('Chartcontainer2', {
	          chart: {
	             
	              type: 'pie',
	              backgroundColor:'transparent',
	 	         options3d: {
	 	             enabled: true,
	 	           alpha: 15
	 	         }
	          },
	          title: {
	              text: ''
	          },
	          legend: {
	         itemStyle: {
	             color: 'black',
	             fontWeight: 'bold',
	             fontSize: '20px'
	         }
	     },
	     lang: {
	            noData: "No Data Found"
	        },
	        noData: {
	            style: {
	                fontWeight: 'bold',
	                fontSize: '25px',
	                color: '#303030'
	            }
	        },
	          tooltip: {
	              pointFormat: '{point.y}'
	          },
	          credits: {
	        	    enabled: false
	        	  },
	          plotOptions: {
	        	  pie: {
	            	   innerSize: 120,
	    	             depth: 40,
	    	             showInLegend:true,
	                  
	                   dataLabels: {
	                       enabled: false,
	                       format: '{point.name}'
	                   }
	               }
	             
	          
	          },
	          legend:{
	        	   layout:'vertical', 
	        	   align:'right',
	        	  
	        	   verticalAlign:'bottom'
	        	 
	           },
	          exporting: {
			        menuItemDefinitions: {
			            // Custom definition
			            label: {
			                onclick: function () {
			                    this.renderer.label(
			                        'You just clicked a custom menu item',
			                        100,
			                        100
			                    )
			                    .attr({
			                        fill: '#a4edba',
			                        r: 5,
			                        padding: 10,
			                        zIndex: 10
			                    })
			                    .css({
			                        fontSize: '1.5em'
			                    })
			                    .add();
			                },
			              
			            }
			        },
			        filename: 'Operations Tickets_'+ customerdetails.customerName +'_' +  dateTime,
			        buttons: {
			            contextButton: {
			                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
			            }
			        }
			    },
	          series: [{
	              type: 'pie',
	              name: 'Count',
	               data: seriesdata2,
	               point: {
	                   events: {
	                       click: function () {
	                           //alert('Category: ' + this.name + ', value: ' + this.y);
	                           var Data = (this.name);
	                           var Type="Operation";
	                          
	                           if(Data == "Open")
		               			{
		                        	   Data = 'State=1 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Closed")
		               			{
		               				Data = 'State=2 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Hold")
		               			{
		               				Data = 'State=3 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Inprogress")
		               			{
		               				Data = 'Ticketstatus=3';
		               			}
	                          /*  alert(Dtaa); */
	                           $('#example tbody').empty();
	                           $('#example').DataTable().clear().draw();
	    		 				 $('#example').DataTable().destroy(); 
	    		 				AjaxCallForTicketTypefileterdetailsByCustomer(Type,Data,customerId);

	                       }
	                   }
	               }
	          }]
	      });
		  
		
		 Highcharts.chart('Chartcontainer3', {
	          chart: {
	             
	              type: 'pie',
	              backgroundColor:'transparent',
	 	         options3d: {
	 	             enabled: true,
	 	           alpha: 15
	 	         }
	          },
	          title: {
	              text: ''
	          },
	          legend: {
	         itemStyle: {
	             color: 'black',
	             fontWeight: 'bold',
	             fontSize: '20px'
	         }
	     },
	     lang: {
	            noData: "No Data Found"
	        },
	        noData: {
	            style: {
	                fontWeight: 'bold',
	                fontSize: '25px',
	                color: '#303030'
	            }
	        },
	          tooltip: {
	              pointFormat: '{point.y}'
	          },
	          credits: {
	        	    enabled: false
	        	  },
	          plotOptions: {
	        	  pie: {
	            	   innerSize: 120,
	    	             depth: 40,
	    	             showInLegend:true,
	                  
	                   dataLabels: {
	                       enabled: false,
	                       format: '{point.name}'
	                   }
	               }
	             
	          
	          },
	          legend:{
	        	   layout:'vertical', 
	        	   align:'right',
	        	  
	        	   verticalAlign:'bottom'
	        	 
	           },
	          exporting: {
			        menuItemDefinitions: {
			            // Custom definition
			            label: {
			                onclick: function () {
			                    this.renderer.label(
			                        'You just clicked a custom menu item',
			                        100,
			                        100
			                    )
			                    .attr({
			                        fill: '#a4edba',
			                        r: 5,
			                        padding: 10,
			                        zIndex: 10
			                    })
			                    .css({
			                        fontSize: '1.5em'
			                    })
			                    .add();
			                },
			              
			            }
			        },
			        filename: 'Maintenance Tickets_'+ customerdetails.customerName +'_' +  dateTime,
			        buttons: {
			            contextButton: {
			                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
			            }
			        }
			    },
	          series: [{
	              type: 'pie',
	              name: 'Count',
	               data: seriesdata3,
	               point: {
	                   events: {
	                       click: function () {
	                           //alert('Category: ' + this.name + ', value: ' + this.y);
	                           var Data = (this.name);
	                           var Type="Maintenance";
	                          
	                           if(Data == "Open")
		               			{
		                        	   Data = 'State=1 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Closed")
		               			{
		               				Data = 'State=2 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Hold")
		               			{
		               				Data = 'State=3 and ticketstatus not in(3)';
		               			}
		               			else if(Data == "Inprogress")
		               			{
		               				Data = 'Ticketstatus=3';
		               			}
	                          /*  alert(Dtaa); */
	                           $('#example tbody').empty();
	                           $('#example').DataTable().clear().draw();
	    		 				 $('#example').DataTable().destroy(); 
	    		 				AjaxCallForTicketTypefileterdetailsByCustomer(Type,Data,customerId);

	                       }
	                   }
	               }
	          }]
	      });
		
		
		
		
		
		},
		error:function(msg) { 
			
			
		}
		}); 
}


function AjaxCallForOpenTicketfileterdetails(state) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 =  $.ajax({
type: 'POST',
url: './openticketfilter',
data: {'State' :state },
success: function(msg){
	
	  
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 
	 var ticketgriddata = msg.ticketgriddata;
	
	 LoadDataTable(ticketgriddata);
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}



function AjaxCallForOpenTicketfileterdetailsBySite(state,siteId) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 =  $.ajax({
type: 'POST',
url: './sitestateticketfilter',
data: {'State' :state,'Siteid' :siteId},
success: function(msg){
	
	  
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 
	 var ticketgriddata = msg.ticketgriddata;
	
	 LoadDataTable(ticketgriddata);
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}

function AjaxCallForOpenTicketfileterdetailsByCustomer(state,customerId) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 =  $.ajax({
type: 'POST',
url: './customerstateticketfilter',
data: {'State' :state,'Customerid' :customerId},
success: function(msg){
	
	  
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 
	 var ticketgriddata = msg.ticketgriddata;
	
	 LoadDataTable(ticketgriddata);
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}

function AjaxCallForTicketTypefileterdetails(type,state) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 =  $.ajax({
type: 'POST',
url: './typeticketfilter',
data: {'Type':type,'State' :state},
success: function(msg){
	
	  
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 
	 var ticketgriddata = msg.ticketgriddata;
	
	 LoadDataTable(ticketgriddata);
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForTicketTypeandStatusfileterdetails(type,status) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 =  $.ajax({
type: 'POST',
url: './Allstateandstatusticketfilter',
data: {'Type':type,'Status' :status},
success: function(msg){
	
	  
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 
	 var ticketgriddata = msg.ticketgriddata;
	
	 LoadDataTable(ticketgriddata);
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}

function AjaxCallForTicketTypefileterdetailsBySite(type,state,siteId) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 =  $.ajax({
type: 'POST',
url: './typeticketfilterBySite',
data: {'Type':type,'State' :state,'SiteId' :siteId},
success: function(msg){
	
	  
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 
	 var ticketgriddata = msg.ticketgriddata;
	
	 LoadDataTable(ticketgriddata);
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForTicketTypeandStatusfileterdetailsBySite(type,status,siteId) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 =  $.ajax({
type: 'POST',
url: './AllstateandstatusticketfilterBySite',
data: {'Type':type,'Status' :status,'SiteId' :siteId},
success: function(msg){
	
	  
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 
	 var ticketgriddata = msg.ticketgriddata;
	
	 LoadDataTable(ticketgriddata);
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}
function AjaxCallForTicketTypefileterdetailsByCustomer(type,state,customerId) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 =  $.ajax({
type: 'POST',
url: './typeticketfilterByCustomer',
data: {'Type':type,'State' :state,'CustomerId' :customerId},
success: function(msg){
	
	  
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 
	 var ticketgriddata = msg.ticketgriddata;
	
	 LoadDataTable(ticketgriddata);
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}


function AjaxCallForTicketTypeandStatusfileterdetailsByCustomer(type,status,customerId) {  
	
	try
	{
		if(typeof ajax_request5 !== 'undefined')
		{
			ajax_request5.abort();
		}
		
	}
	catch(err)
	{
		
	}
	
	ajax_request5 =  $.ajax({
type: 'POST',
url: './AllstateandstatusticketfilterByCustomer',
data: {'Type':type,'Status' :status,'CustomerId' :customerId},
success: function(msg){
	
	  
	  $('#example tbody').empty();
	  $('#example').DataTable().clear().draw();
		  $('#example').DataTable().destroy(); 
	 
	 var ticketgriddata = msg.ticketgriddata;
	
	 LoadDataTable(ticketgriddata);
	 },
error:function(msg) { 
	
	//alert(0);
	//alert(msg);
	}
	}); 
	  
}
</script>

<script type="text/javascript">
$(document).ready(function() {
/* 	alert($(window).height());
	alert($(window).width()); */
	
	var classstate = $('#hdnstate').val();
	
	 $("#o-mid").addClass(classstate);
		$("#ticketsid").addClass(classstate);
	 
	 $(".theme-loader").show();
	 $(".theme-loader").animate({
         opacity: "0"
     },10000);
	
	$('#ddlSite').dropdown('clear');
	$('#ddlType').dropdown('clear');
	$('#ddlCategory').dropdown('clear');
	$('#ddlPriority').dropdown('clear');
	$('#txtSubject').val('');
	$('#txtTktdescription').val('');
	
	
	/* Highcharts.chart('Chartcontainer', {
	    chart: {
	        type: 'pie',
	        options3d: {
	            enabled: true,
	            alpha:75,
	            beta: 0
	        }
	    },
	    title: {
	        text: 'Count'
	    },
	    legend: {
	   itemStyle: {
	       color: 'black',
	       fontWeight: 'bold',
	       fontSize: '20px'
	   }
	},
	    tooltip: {
	        pointFormat: '{point.y}'
	    },
	    credits: {
	  	    enabled: false
	  	  },
	    plotOptions: {
	        pie: {
	            allowPointSelect: true,
	            cursor: 'pointer',
	            depth: 35,
	            dataLabels: {
	                enabled: true,
	                format: '{point.name}'
	            }
	        }
	       
	    
	    },
	    exporting: {
		        menuItemDefinitions: {
		            // Custom definition
		            label: {
		                onclick: function () {
		                    this.renderer.label(
		                        'You just clicked a custom menu item',
		                        100,
		                        100
		                    )
		                    .attr({
		                        fill: '#a4edba',
		                        r: 5,
		                        padding: 10,
		                        zIndex: 10
		                    })
		                    .css({
		                        fontSize: '1.5em'
		                    })
		                    .add();
		                },
		              
		            }
		        },
		        buttons: {
		            contextButton: {
		                menuItems: ['downloadPNG','downloadJPEG', 'downloadPDF','downloadSVG','downloadCSV', 'downloadXLS']
		            }
		        }
		    },
	    series: [{
	        type: 'pie',
	        name: 'Count',
	         data:[{name:'Open',y:10,color:'blue'},{name:'Hold',y:10,color:'black'},{name:'Closed',y:10,color:'red'}],
	        
	        point: {
	            
	        }
	    }]
	});
 */
 if($('#hdnmysitefilter').val() == "true") 
	 {
	 AjaxCallForStateandTypeDetailsBySite($('#hdnmysiteid').val());
	 	AjaxCallForStateandStatusDetailsBySite($('#hdnmysiteid').val());
	 }
  else if($('#hdnmycustomerfilter').val() == "true") 
	 {
	  AjaxCallForStateandTypeDetailsByCustomer($('#hdnmycustomerid').val());
	 	AjaxCallForStateandStatusDetailsByCustomer($('#hdnmycustomerid').val());
	 }

  else{
		AjaxCallForStateandTypeDetails();
	 	AjaxCallForStateandStatusDetails();	
              
  	
  }
	
 $("#ddlSiteList").change(function(){
	    var sitevalue = $("#ddlSiteList option:selected").val();
	    $('#example tbody').empty();
	    $('.search').val('');
	    $('#example').DataTable().clear().draw();
		$('#example').DataTable().destroy(); 
	 	
	 	
	 	if($('#hdnmysitefilter').val() == "true") {
	 		
	 		 AjaxCallForStateandTypeDetailsBySite(sitevalue);
		 	 	AjaxCallForStateandStatusDetailsBySite(sitevalue);
	 	}
	 	 else if($('#hdnmycustomerfilter').val() == "true"){
	 		 if(sitevalue==0)
 			 {
	 			  AjaxCallForStateandTypeDetailsByCustomer($('#hdnmycustomerid').val());
	 			 	AjaxCallForStateandStatusDetailsByCustomer($('#hdnmycustomerid').val());
 			 }
 		 else
 			 {
 			 AjaxCallForStateandTypeDetailsBySite(sitevalue);
 	 	 	AjaxCallForStateandStatusDetailsBySite(sitevalue);
 			 }
	 		 
	 		
	 	}
	 	 else{
	 		 if(sitevalue==0)
	 			 {
	 			AjaxCallForStateandTypeDetails();
	 		 	AjaxCallForStateandStatusDetails();	
	 			 }
	 		 else
	 			 {
	 			 AjaxCallForStateandTypeDetailsBySite(sitevalue);
	 	 	 	AjaxCallForStateandStatusDetailsBySite(sitevalue);
	 			 }
	 		
	 	 }
	 	
	 	

  
	    //alert("You have selected the SiteID - " + sitevalue);
	});
 		 
        	
        	
      
        	
        	
   	     $('.clear').click(function(){
			    $('.search').val("");
			});
   	     
   	     $("#txtFromDate").datepicker({                  	
         	dateFormat:'dd/mm/yy',
         	maxDate: new Date(),
         	maxDate: "0",
                onSelect: function(selected) {
                  $("#txtToDate").datepicker("option","minDate", selected)                 
                }
            });
         $("#txtToDate").datepicker({         	
         	dateFormat:'dd/mm/yy',
         	maxDate: new Date(),
         	maxDate: "0",
         	onSelect: function(selected) {                                
             $("#txtFromDate").datepicker("option","maxDate", selected)      
          }                                
   	 }); 

        
        	
        	 
        	 $.fn.dataTable.moment('DD-MM-YYYY');
        	 $.fn.dataTable.moment('DD-MM-YYYY HH:mm');
        	  
     /*     $('#example').DataTable( {
        		 "order": [[ 0, "desc" ]],
        	 	//  "order": [[ 3, "desc" ]], 
        	//	 "order": [[ 2, "desc" ]], 
         		 "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
        		 "bLengthChange": true,        		
        		 } );  */
        	
       	 $("#searchSelect").change(function() {
             var value = $('#searchSelect option:selected').val();
             var uid =  $('#hdneampmuserid').val();
           redirectbysearch(value,uid);
        }); 
       	 $('.dataTables_filter input[type="search"]').attr('placeholder','search');

	
            if($(window).width() < 767)
				{
				   $('.card').removeClass("slide-left");
				   $('.card').removeClass("slide-right");
				   $('.card').removeClass("slide-top");
				   $('.card').removeClass("slide-bottom");
				   
				   
				   
				   $('.carousel-inner').append('<div class="item active" style="height: 380px;background:#FFF;border-radius:20px;"><div class="carousel-caption"><div class="col-md-12 padd-0"><div id="Chartcontainer1" style="height: 330px"></div><p class="paragrapth" style="font-size:15px"><b>Total</b></p></div></div></div>');
				   $('.carousel-inner').append('<div class="item" style="height: 380px;background:#FFF;border-radius:20px;"><div class="carousel-caption"><div class="col-md-12 padd-0"><div id="Chartcontainer2" style="height: 330px;margin: 0 auto"></div><p class="paragrapth" style="font-size:15px"><b>Operations</b></p></div></div></div>');
				   $('.carousel-inner').append('<div class="item" style="height: 380px;background:#FFF;border-radius:20px;"><div class="carousel-caption"><div class="col-md-12 padd-0"><div id="Chartcontainer3" style="height: 330px; margin: 0 auto"></div><p class="paragrapth" style="font-size:15px"><b>Maintenance</b></p></div></div></div>');
				   
				   $('.carousel-inner').append('<div class="item" style="height: 380px;background:#FFF;border-radius:20px;"><div class="carousel-caption"><div class="col-md-12 padd-0"><div id="Chartcontainer4" style="height: 320px"></div><p class="paragrapth" style="font-size:15px"><b>Open </b></p></div></div></div>');
				   $('.carousel-inner').append('<div class="item" style="height: 380px;background:#FFF;border-radius:20px;"><div class="carousel-caption"><div class="col-md-12 padd-0"><div id="Chartcontainer5" style="height: 320px;margin: 0 auto"></div><p class="paragrapth" style="font-size:15px"><b>Closed</b></p></div></div></div>');
				   $('.carousel-inner').append('<div class="item" style="height: 380px;background:#FFF;border-radius:20px;"><div class="carousel-caption"><div class="col-md-12 padd-0"><div id="Chartcontainer6" style="height: 320px; margin: 0 auto"></div><p class="paragrapth" style="font-size:15px"><b>Hold</b></p></div></div></div>');
				   
				}
            else
            	{
            		$('.carousel-inner').append('<div class="item active" style="height: 380px;background:#FFF;border-radius:20px;"><div class="carousel-caption"><div class="col-md-12 padd-0"><span class="mcar-span">For the last 60 days</span></div><div class="col-md-12 padd-0"><div class="col-md-4 padd-0"><div id="Chartcontainer1" style="height: 330px"></div><p class="paragrapth" style="font-size:15px;float:left;margin-left:33%"><b>Total</b></p></div><div class="col-md-4 padd-0" ><div id="Chartcontainer2" style="height: 330px; margin: 0 auto"></div><p class="paragrapth" style="font-size:15px;float:left;margin-left:27%"><b>Operations</b></p></div><div class="col-md-4 padd-0" ><div id="Chartcontainer3" style="height: 330px; margin: 0 auto"></div><p class="paragrapth" style="font-size:15px;float:left;margin-left:26%"><b>Maintenance</b></p></div></div></div></div></div>');
				   	$('.carousel-inner').append('<div class="item" style="height: 380px;background:#FFF;border-radius:20px;"><div class="carousel-caption"><div class="col-md-12 padd-0"><span class="mcar-span">For the last 60 days</span></div><div class="col-md-12 padd-0"><div class="col-md-4 padd-0"><div id="Chartcontainer4" style="height: 320px"></div><p class="paragrapth" style="font-size:15px"><b>Open</b></p></div><div class="col-md-4 padd-0" ><div id="Chartcontainer5" style="height: 320px; margin: 0 auto"></div><p class="paragrapth" style="font-size:15px"><b>Closed</b></p></div><div class="col-md-4 padd-0" ><div id="Chartcontainer6" style="height: 320px; margin: 0 auto"></div><p class="paragrapth" style="font-size:15px"><b>Hold</b></p></div></div></div></div></div>');
				}
           

               $('.close').click(function(){
                   $('#tktCreation').hide();
                   $('.clear').click();
                   $('.category').dropdown();
                   $('.SiteNames').dropdown();
               });
			$('.clear').click(function(){
			    $('.search').val("");
			});
			
			$('#ddlType').change(function(){              
                $('.category').dropdown('clear')
               });


			$('body').click(function(){
            	$('#builder').removeClass('open'); 
         	 });
        });
      </script>
   
   
     
<script type="text/javascript">
         $(window).load(function(){
        	/*  $('.ui.dropdown').dropdown({forceSelection:false});
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
          */
         
         $('.ui.dropdown').dropdown({
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: true, 
             showOnFocus: true,
             on: "click" 
           });

		$('.ui.dropdown.oveallsearch').dropdown({
             onChange: function (value, text, $selectedItem) {                
                var uid =  $('#hdneampmuserid').val();
                redirectbysearchvalue(value,uid);
             },
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: false, 
             showOnFocus: true,
             on: "click" 
           });
		
		
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );//]]> 

            var validation  = {
                    txtSubject: {
                               identifier: 'txtSubject',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               },
                               {
                                   type: 'maxLength[50]',
                                   prompt: 'Please enter a value'
                                 }]
                             },
                              ddlSite: {
                               identifier: 'ddlSite',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                             },
                            ddlType: {
                                  identifier: 'ddlType',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                            ddlCategory: {
                               identifier: 'ddlCategory',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                             ddlPriority: {
                               identifier: 'ddlPriority',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please select a dropdown value'
                               }]
                             },
                             description: {
                               identifier: 'description',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please Enter Description'
                               }]
                             }
             };
               var settings = {
                 onFailure:function(){
                     return false;
                   }, 
                 onSuccess:function(){    
                   $('#btnCreate').hide();
                   $('#btnCreateDummy').show()
                   //$('#btnReset').attr("disabled", "disabled");          
                   }};           
               $('.ui.form.validation-form').form(validation,settings);

         })
         
      </script>
<style type="text/css">
.highcharts-tooltip span {
    width:75px!important;
    overflow:auto;
    white-space:normal !important;
    }
.dropdown-menu-right {
	left: -70px !important;
}
/* .ui.search.dropdown .menu {
    max-height: 12.02857143rem;
}
.ui.search.dropdown > input.search {
    background: none transparent !important;
    border: none !important;
    box-shadow: none !important;
    cursor: text;
    top: 0em;
    left: -2px;
    width: 100%;
    outline: none;
    -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
    padding: inherit;
} */
 	div.dataTables_wrapper div.dataTables_length select {
			    width: 50px;
			    display: inline-block;
			    margin-left: 3px;
			    margin-right: 4px;
			}
			
	/* 		.ui.form textarea:not([rows]) {
    height: 4em;
    min-height: 4em;
    max-height: 24em;
} */
.ui.icon.input > i.icon:before, .ui.icon.input > i.icon:after {
    left: 0;
    position: absolute;
    text-align: center;
    top: 36%;
    width: 100%;
}


.input-sm, .form-horizontal .form-group-sm .form-control {
    font-size: 13px;
    min-height: 25px;
    height: 32px;
    padding: 8px 9px;
}
  
#example td a {
	color: #337ab7;
	font-weight: bold;
}
.dt-button-background {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 0 !important;
}

.tbh-width {
	width: 150px;
	padding-right: 30px !important;
}

.dt-buttons {
	margin-left: 50px;
}

.buttons-collection {
	height: 28px;
	padding: 3px;
	/*  z-index:9999; */
}


.buttons-excel {
	background: rgba(36, 95, 68, 0.87);
	color: #FFF;
}

.buttons-pdf {
	background: rgb(98, 165, 96);
	color: #FFF;
}

.dataTables_filter {
	float: right;
}

.dataTables_length {
	float: left;
}


.ui.selection.dropdown {
    width: 100%;
    margin-top: 0.1em;
   /*  height: 2.3em; */
}

.ui.selection.dropdown .menu {
    width: 100%;
    white-space: normal !important;
}

.ui.selection.dropdown .menu .item:first-child {
    border-top: 1px solid #ddd !important;
    min-height: 2.8em;
}

.ui.selection.dropdown .text {

    text-overflow: ellipsis;
    white-space: nowrap;
    width: 90%;
}
.oveallSearch .text {
    /* color: #eae9e9 !important; */
    color: #a49c9c !important;
}

.ui.selection.dropdown .icon {
    text-align: right;
    margin-left: 7px !important;
}

</style>

</head>
<body class="fixed-header" style="    padding-right: 0px !important;">
  <div class="theme-loader">
        <div class="loader-track">
            <div class="loader-bar"></div>
        </div>
</div> 


	<input type="hidden" value="${access.userID}" id="hdneampmuserid">
	
	<input type="hidden" value="${access.mySiteFilter}" id="hdnmysitefilter">
	
	<input type="hidden" value="${access.mySiteID}" id="hdnmysiteid">
	
	<input type="hidden" value="${access.myCustomerFilter}" id="hdnmycustomerfilter">
	
	<input type="hidden" value="${access.myCustomerID}" id="hdnmycustomerid">

<input type="hidden" value="${classtate}" id="hdnstate">

	<jsp:include page="slider.jsp"/>
	

	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					 <a href="./dashboard"><img src="resources/img/logo01.png" alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx tktSearch" id="SearcSElect">
                          <div class="ui  search selection  dropdown  width oveallsearch">
                                         <input id="myInput" name="tags" type="text">

                                         <div class="default text">search</div>
                                        
                                         <div id="myDropdown" class="menu">

                                                <jsp:include page="searchselect.jsp" />	

                                         </div>
                                  </div>
                          
                          
                          
                          
                          
                          
                        </div>
				
				 <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user_01.png" width="32" height="32">
                  </span>
                  
                            
                             <c:if test="${not empty access}">         
                              <p class="user-login">${access.userName}</p>
                  </c:if>
                  
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="logout1" class="dropdown-item color logout"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
               
                  
					
					 <div class="newticketpopup hidden">
								  <span data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><p class="center m-t-5 tkts"><img class="m-t-5" src="resources/img/tkt.png"><p class="create-tkts">New Ticket</p></p></span>
					</div>
				
              
               

						
						
						
              
			
				
			</div>
		</div>
		<div class="page-content-wrapper" id="QuickLinkWrapper1">

			<div class="content ">

				<div class="container-fixed-lg">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="./dashboard"><i
								class="fa fa-home mt-7"></i></a></li>
						<li class="breadcrumb-item hover-idle"><a>Operation &
								Maintenance</a></li>

						<li class="breadcrumb-item active">Tickets KPI</li>
					</ol>
				</div>



				<div id="newconfiguration" class="">
					<div class="card card-transparent mb-0">
						<!-- Table Start-->
						<div class="padd-3 sortable" id="equipmentdata">
							<div class="row">
								<!-- <div class="card-header">
                                    <div class="card-title tbl-head ml-15">Equipment Curing Data</div>
                                 </div> -->
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
									<div class="card-default bg-default content-bg" data-pages="card" style="    margin-bottom: 3px;">
										<div class="col-md-3 padd-0">
												<select class="ui search dropdown forced selectSite" id="ddlSiteList">
      											<option value="">Select Site</option>
     											<!-- <option value="0">All</option> -->
     
   											 </select>
										</div>
										<div class="col-md-6"></div>
										<div class="col-md-3">
											<c:choose>
                                                           <c:when test="${access.mySiteFilter == 'true'}">
                                                                 <%-- <a class="ticketdetails"  href="./siteticketdetails${access.mySiteID}"><span>Click Here</span> to view detailed ticketing page</a> --%>
                                                                   <p class="ticketdetails"><a href="./siteticketdetails${access.mySiteID}">Tickets</a> Details</p>
								
                                                            </c:when>
                                                            
                                                            <c:when test="${access.myCustomerFilter == 'true'}">
                                                                <p class="ticketdetails"><a href="./customerticketdetails${access.myCustomerID}">Tickets</a> Details</p>
								
                                                            </c:when>
                                                            <c:otherwise>
                                                                 <p class="ticketdetails"><a href="./ticketdetails">Tickets</a> View</p>
														 </c:otherwise>
                                        </c:choose>
										</div>
										
									</div>
								</div>

								
							</div>



						</div>


						<!-- Table End-->

						
						<!-- Chart Part -->
						<div class="padd-3 sortable" id="equipmentdata">
							<div class="row">								
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
									<div class="card card-default bg-default" data-pages="card" style="margin-bottom:3px !important;">
										<!-- <div class="card-header">
											<div class="card-title tbl-head ml-15">
												 <i class="fa fa-list" aria-hidden="true"></i> List of Tickets
											</div>
										</div> -->
										<div class="padd-5 table-responsive">
                                       	  <div id="myCarousel" class="carousel slide" data-ride="carousel">
											    <!-- Indicators -->
											    <ol class="carousel-indicators">
											      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
											      <li data-target="#myCarousel" data-slide-to="1"></li>
											    </ol>
											    <!-- Wrapper for slides -->
											    <div class="carousel-inner">
											     <!-- <div class="item active" style="height: 450px;background: #FFF;">
											       <div class="carousel-caption">
											        	<div class="col-md-12 padd-0">
															<div class="col-md-4 padd-0">
																<div id="Chartcontainer" style="height: 330px"></div>
																<p class="paragrapth">KPI Accumulated for Last 60 Days</p>
															</div>
															<div class="col-md-4 padd-0" >
																<div id="BarChart" style="height: 330px; margin: 0 auto"></div>
															<p class="paragrapth">KPI Accumulated for Last 60 Days</p>
															</div>
															<div class="col-md-4 padd-0" >
																<div id="BarChart" style="height: 330px; margin: 0 auto"></div>
															<p class="paragrapth">KPI Accumulated for Last 60 Days</p>
															</div>
														</div>
											        </div>
											      </div>  --> 

											   <!--   <div class="item" style="height: 450px;">
											        <div class="carousel-caption">
											        <div class="col-md-11">
											        	<div id="DataChart" style="height: 400px"></div>
											        	<p class="paragrapth">KPI Accumulated for Last 60 Days</p>
											        </div>
											       
											        </div>
											      </div> 	 -->
											      
											      
											      
											      
											      				    
											    </div>

											    <!-- Left and right controls -->
											    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
											      <span class="glyphicon glyphicon-chevron-left"></span>
											      <span class="sr-only">Previous</span>
											    </a>
											    <a class="right carousel-control" href="#myCarousel" data-slide="next">
											      <span class="glyphicon glyphicon-chevron-right"></span>
											      <span class="sr-only">Next</span>
											    </a>
											  </div>


                                    	</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Chart End -->
						<!-- Table Start-->
						<div class="padd-3 sortable" id="equipmentdata">
							<div class="row">								
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
									<div class="card card-default bg-default" data-pages="card">
										<!-- <div class="card-header">
											<div class="card-title tbl-head ml-15">
												 <i class="fa fa-list" aria-hidden="true"></i> List of Tickets
											</div>
										</div> -->
										
                                    	 <div class="padd-5 table-responsive">
                                       <table id="example" class="table table-striped table-bordered dataTable no-footer" width="100%" cellspacing="0" role="grid" aria-describedby="example_info" style="width: 100%;">
                                            <thead>
                                                <tr role="row">
                                                <c:if test="${access.customerListView == 'visible'}">
	
                                                <th style="width: 69px;" class="sorting_desc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Ticket No: activate to sort column ascending" aria-sort="descending">Ticket No</th>
                                                </c:if>
                                                <th style="width: 150px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Site Name: activate to sort column ascending">Site Name</th><th style="width: 45px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Priority: activate to sort column ascending">Priority</th><th style="width: 109px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Created Time: activate to sort column ascending">Created Time</th><th style="width: 123px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Category: activate to sort column ascending">Category</th><th style="width: 163px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Subject: activate to sort column ascending">Subject</th><th style="width: 123px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Assign To: activate to sort column ascending">Assign To</th><th style="width: 96px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Created By: activate to sort column ascending">Created By</th><th style="width: 96px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Scheduled On: activate to sort column ascending">Scheduled On</th><th style="width: 42px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="State: activate to sort column ascending">State</th><th style="width: 43px;" class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Status</th></tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                           </table>
                                    </div>
									</div>
								</div>
							</div>
						</div>			
						<!-- Table End-->
					</div>
				</div>
			</div>
			<div class="container-fixed-lg footer mb-0">
				<div class="container-fluid copyright sm-text-center">
					<p class="small no-margin pull-left sm-pull-reset">
						<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
							CLEAN ENERGY</span>. <span class="hint-text">All rights reserved.
						</span>
					</p>
<!-- 					<p class="small no-margin pull-rhs sm-pull-reset">
						<span class="hint-text">Powered by</span> <span>MESTECH
							SERVICES PVT LTD</span>.
					</p> -->
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Side Bar Content Start-->
	<!-- <div class="quickview-wrapper  builder hidden-sm hidden-xs"
		id="builder">
		<div class="p-l-10 p-r-10 ">
			<a class="builder-close quickview-toggle pg-close"></a>
			<a class="builder-toggle" data-toggle="quickview"
				data-toggle-element="#builder"><img
				src="resources/img/ImageResize_06.png"></a>
			<ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary"
				id="builderTabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#tabLayouts" role="tab"
					aria-controls="home"><span>Quick Link</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active " id="tabLayouts" role="tabcard">
					<div class="scrollable">
						<div class="p-r-50">
							<div class="list">
								<ul>
									<li><a href="#QuickLinkWrapper1">Ticket Filters</a></li>
									<li><a href="#QuickLinkWrapper2">List Of Tickets</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div> -->
	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min3.js" type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min3.js" type="text/javascript"></script>
	<script type="text/javascript" src="resources/js/moment.min.js"></script>
    <script type="text/javascript" src="resources/js/datetime-moment.js"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>
	
	<script src="resources/js/dataTables.buttons.min.js"></script>

<script src="resources/js/buttons.html5.min.js"></script>
<script src="resources/js/jszip.min.js"></script>
 
	
	<div id="gotop"></div>
	<!-- Share Popup End !--> 
<!-- Modal -->
<div id="tktCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">New Ticket</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"><span class="errormess">*</span> All fields are mandatory</p>
				 </div>            
        		<form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form validation-form"> 
             
           <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width SiteNames"
									id="ddlSite" name="ddlSite" path="siteID" onchange="getEquipmentAgainstSite()">
									<form:option value="" tabindex="0">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>
						
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                                                <div class="col-md-4 col-xs-12">
                                                       <label class="fields required m-t-10">Equipment</label>
                                                </div>

                                                <div class="col-md-8 col-xs-12">
                                                       <form:select
                                                              class="field ui fluid search selection dropdown width equipments"
                                                              id="ddlEquipment" path="equipmentID" name="ddlEquipment">
                                                              <option value="">Select</option>
                                                       </form:select>
                                                </div>
                          </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Type</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width" id="ddlType" name="ddlType"  path="ticketType">
                        <form:option value="">Select</form:option>
                        <form:option value="Operation">Operation</form:option>
                        <form:option value="Maintenance">Maintenance</form:option>
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Category</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width category" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                         <form:option data-value="Operation" value="">Select</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Preventive Maintenance">Preventive Maintenance</form:option>										
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                          <form:option data-value="Maintenance" value="">Select</form:option>
                    </form:select> 
                  </div>
                  
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                  <div class="col-md-4 col-xs-12"><label class="fields required m-t-10">Subject</label></div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<form:input  placeholder="Subject" autocomplete="off"  name="Subject"  type="text"  id="txtSubject" path="ticketDetail" maxLength="50"/>
                  	</div>
                  	</div>
               </div> 
              
              <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Priority</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width" id="ddlPriority" name="user" path="priority">
                        <form:option value="">Select </form:option>
                        <form:option data-value="3" value="3">High</form:option>
                        <form:option data-value="2" value="2">Medium</form:option>
                        <form:option data-value="1" value="1">Low</form:option>
                    </form:select> 
                  </div>
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Description</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<textarea id="txtTktdescription"  autocomplete="off" name="description" style="resize: none;" placeholder="Ticket Description" maxLength="120"></textarea>
                  	</div>
                    
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
				       <div class="btn btn-success  submit" id="btnCreate">Create</div>
				       <button class="btn btn-success" type="button" id="btnCreateDummy" tabindex="7"  style="display:none;">Create</button>
				        <div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
				  </div>
  <!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
  <!-- <div class="ui error message"></div> -->
</form:form>
				<!-- <div class="ui error message"></div> -->
				
	  		</div>
	  </div>
    </div>
 </div>
	<script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
	<script>
         $(document).ready(function() {
            $(window).fadeThis({
               speed: 500,
            });
         });
      </script>

</body>

<script type="text/javascript">
function getEquipmentAgainstSite() {
    var siteId = $('#ddlSite').val();
   
    $.ajax({
          type : 'GET',
          url : './equipmentsBysites?siteId=' + siteId,
          success : function(msg) {
       	   
       	   var EquipmentList = msg.equipmentlist;
                 
                        for (i = 0; i < EquipmentList.length; i++) {
                               $('#ddlEquipment').append(
                                             "<option value="+EquipmentList[i].equipmentId+">" + EquipmentList[i].customerNaming
                                                          + "</option>");
                        }

          },
          error : function(msg) {
                 console.log(msg);
          }
    });
}

       </script>
</html>

