<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />

<link href="resources/css/pace-theme-flash.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet"
	rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/bootstrap.min.3.37.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="resources/css/semantic.min.css">

<link href="resources/css/loader.css" rel="stylesheet" type="text/css" />
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script src="resources/js/CustomeScript.js" type="text/javascript"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>
<!--  <script
	src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.7.7/xlsx.core.min.js"></script>  -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.14.1/xlsx.core.min.js"></script>


<script
	src="https://cdnjs.cloudflare.com/ajax/libs/xls/0.7.4-a/xls.core.min.js"></script>
<script src="resources/js/bootstrap-filestyle.min.js"
	type="text/javascript"></script>


<script type="text/javascript" src="resources/js/toastr.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/toastr.css">
<link rel="stylesheet" type="text/css"
	href="http://codeseven.github.com/toastr/toastr.css">


<script type="text/javascript">
function AjaxCallForSaveOrUpdate(myjson) { 

	 $.ajax({
	                        type: 'POST',
	                         contentType : 'application/json; charset=utf-8',
	                       dataType : 'json',
	                       url: './newbulkdata',
	                       data: myjson,
	                       success: function(msg){

	                    	   var errormessage = msg.Errorlog1;
	                    	   var errormessage1 = msg.Errorlog2;
	                       	
	                       	if(errormessage!=null && errormessage!="")
	             				{

	               	    		toastr.error(errormessage);        					
	             				}
	                    	if(errormessage1!=null && errormessage1!="")
             				{

               	    		toastr.error(errormessage1);        					
             				}
	                     
	                   
	                       
	                        },
	                        error:function(msg) { 
	                           alert(fail);
	                           }
	      }); 
	}


</script>
<script type="text/javascript">
	 $(window).load(function(){
    	 $('.ui.dropdown').dropdown({forceSelection:false});
    	 
    });
        $(document).ready(function() {  
        	
        	$(".theme-loader").hide();
        	toastr.options = {
      	           "debug": false,
      	           "positionClass": "toast-bottom-right",
      	           "onclick": null,
      	           "fadeIn": 500,
      	           "fadeOut": 100,
      	           "timeOut": 2000,
      	           "extendedTimeOut": 2000
      	         }
         	
			var classstate = $('#hdnstate').val();
			
			$("#configurationid").addClass(classstate);
			$("#masteruploadconfigid").addClass(classstate);
        	
        	$('#viewfile').click(function(){
        	
        		AjaxCallForSaveOrUpdate(myjson)
        	    alert("The paragraph was clicked.");
        	});
        	
        	$('#tktCreation').hide();        	
        	$('#ddlSite').dropdown('clear');
        	$('#ddlType').dropdown('clear');
        	$('#ddlCategory').dropdown('clear');
        	$('#ddlPriority').dropdown('clear');
        	$('#txtSubject').val('');
        	$('#txtTktdescription').val('');
        	
        	
        	$('#example').DataTable({
        		 "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
        		 "bLengthChange": true,      
        	});
           
            $('.close').click(function(){
				$('.clear').click();
				$('#tktCreation').hide();
				$('.clear').click();
			    $('.category').dropdown();
			    $('.SiteNames').dropdown();
			});
            
            $('#btnCreate').click(function() {
        		if($('#ddlCategory').val== "") {
        	    	alert('Val Empty');
        	        $('.category ').addClass('error')
        	    }
        	})
            
            
            $('.clear').click(function(){
            	$('.search').val('');
            })
            
            $('#ddlType').change(function(){              
                $('.category').dropdown('clear')
               });
			
			 $("#searchSelect").change(function() {
        	                var value = $('#searchSelect option:selected').val();
        	                var uid =  $('#hdneampmuserid').val();
        	             	redirectbysearch(value,uid);
        	  });
			 
			 var validation  = {
     				 txtSubject: {
     	                  identifier: 'txtSubject',
     	                  rules: [{
     	                    type: 'empty',
     	                    prompt: 'Please enter a value'
     	                  },
     	                  {
     	                      type: 'maxLength[50]',
     	                      prompt: 'Please enter a value'
     	                    }]
     	                },
     	                 ddlSite: {
     	                  identifier: 'ddlSite',
     	                  rules: [{
     	                    type: 'empty',
     	                    prompt: 'Please enter a value'
     	                  }]
     	                },
     	               ddlType: {
     	                     identifier: 'ddlType',
     	                  rules: [{
     	                    type: 'empty',
     	                    prompt: 'Please enter a value'
     	                  }]
     	               },
     	               ddlCategory: {
     	                  identifier: 'ddlCategory',
     	                  rules: [{
     	                    type: 'empty',
     	                    prompt: 'Please enter a value'
     	                  }]
     	               },
     	                ddlPriority: {
     	                  identifier: 'ddlPriority',
     	                  rules: [{
     	                    type: 'empty',
     	                    prompt: 'Please select a dropdown value'
     	                  }]
     	                },
     	                description: {
     	                  identifier: 'description',
     	                  rules: [{
     	                    type: 'empty',
     	                    prompt: 'Please Enter Description'
     	                  }]
     	                }
			};
     		var settings = {
     		  onFailure:function(){
     		      return false;
     		    }, 
     		  onSuccess:function(){    
     		    $('#btnCreate').hide();
     		    $('#btnCreateDummy').show()
     		  	//$('#btnReset').attr("disabled", "disabled");   		   	
     		    }};     		  
     		$('.ui.form.validation-form').form(validation,settings);

             
        });
      </script>

<script type='text/javascript'>//<![CDATA[
         $(window).load(function(){
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
         
         $('.ui.dropdown').dropdown({
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: true, 
             showOnFocus: true,
             on: "click" 
           });

		$('.ui.dropdown.oveallsearch').dropdown({
             onChange: function (value, text, $selectedItem) {                
                var uid =  $('#hdneampmuserid').val();
                redirectbysearchvalue(value,uid);
             },
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: false, 
             showOnFocus: true,
             on: "click" 
           });
           
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );


         })
         
      </script>
<style type="text/css">
/* .ui.form textarea:not([rows]) {
		    height: 8em;
		    min-height: 8em;
		    max-height: 24em;
		}

		.ui.form textarea:not([rows]) {
		    height: 4em;
		    min-height: 4em;
		    max-height: 24em;
		}
  
		  .input-sm, .form-horizontal .form-group-sm .form-control {
		    font-size: 13px;
		    min-height: 25px;
		    height: 32px;
		    padding: 8px 9px;
		} */
:root { -
	-c: #e47c00; -
	-c-b: bisque; -
	-c-c: #ea7f00;
}

.selected {
	background-color: brown;
	color: #FFF;
}

#count-table td {
	padding: 3px;
	font-size: 14px;
}

.error {
	background: #ff0000;
}

.delete-rowval .fa {
	color: red;
	font-size: 23px;
}

.err-empty {
	color: #ea7f00;
	border-color: #e47c00 !important;
	position: relative;
	border-width: 2px !important;
	background: bisque;
}

.err-empty:hover:after {
	position: absolute;
	bottom: 30px;
	content: "Not be Empty.";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #ec6c00;
	background-color: #ffeed3;
	border: 1px solid #e47c24;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.err-notnum {
	color: #ea7f00;
	border-color: #e47c00 !important;
	position: relative;
	border-width: 2px !important;
	background: bisque;
}

.err-notnum:hover:after {
	position: absolute;
	bottom: 30px;
	content: "Only Numbers are allowed.";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #ec6c00;
	background-color: #ffeed3;
	border: 1px solid #e47c24;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.err-notchar {
	color: #ea7f00;
	border-color: #e47c00 !important;
	position: relative;
	border-width: 2px !important;
	background: bisque;
}

.err-notchar:hover:after {
	position: absolute;
	bottom: 30px;
	content: "Only characters are allowed.";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #ec6c00;
	background-color: #ffeed3;
	border: 1px solid #e47c24;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.err-length {
	color: #ea7f00;
	border-color: #e47c00 !important;
	position: relative;
	border-width: 2px !important;
	background: bisque;
}

.err-length:hover:after {
	position: absolute;
	bottom: 30px;
	content: "Must have 10 characters.";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #ec6c00;
	background-color: #ffeed3;
	border: 1px solid #e47c24;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.err-ddact {
	color: #ea7f00;
	border-color: #e47c00 !important;
	position: relative;
	border-width: 2px !important;
	background: bisque;
}

.err-ddact:hover:after {
	position: absolute;
	bottom: 30px;
	content: "Only Active or Inactive are allowed.";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #ec6c00;
	background-color: #ffeed3;
	border: 1px solid #e47c24;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.err-ddpri-sec {
	color: #ea7f00;
	border-color: #e47c00 !important;
	position: relative;
	border-width: 2px !important;
	background: bisque;
}

.err-ddpri-sec:hover:after {
	position: absolute;
	bottom: 30px;
	content: "Only Primary or Secondary are allowed.";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #ec6c00;
	background-color: #ffeed3;
	border: 1px solid #e47c24;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.err-unique {
	color: #ea7f00;
	border-color: #e47c00 !important;
	position: relative;
	border-width: 2px !important;
	background: bisque;
}

.err-unique:hover:after {
	position: absolute;
	bottom: 30px;
	content: "Must be unique";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #ec6c00;
	background-color: #ffeed3;
	border: 1px solid #e47c24;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.err-size100 {
	color: #ea7f00;
	border-color: #e47c00 !important;
	position: relative;
	border-width: 2px !important;
	background: bisque;
}

.err-size100:hover:after {
	position: absolute;
	bottom: 30px;
	content: "character must not exceed 100";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #ec6c00;
	background-color: #ffeed3;
	border: 1px solid #e47c24;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.err-size50 {
	color: #ea7f00;
	border-color: #e47c00 !important;
	position: relative;
	border-width: 2px !important;
	background: bisque;
}

.err-size50:hover:after {
	position: absolute;
	bottom: 30px;
	content: "character must not exceed 50";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #ec6c00;
	background-color: #ffeed3;
	border: 1px solid #e47c24;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.err-size25 {
	color: #ea7f00;
	border-color: #e47c00 !important;
	position: relative;
	border-width: 2px !important;
	background: bisque;
}

.err-size25:hover:after {
	position: absolute;
	bottom: 30px;
	content: "character must not exceed 25";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #ec6c00;
	background-color: #ffeed3;
	border: 1px solid #e47c24;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.err-notdate {
	color: #ea7f00;
	border-color: #e47c00 !important;
	position: relative;
	border-width: 2px !important;
	background: bisque;
}

.err-notdate:hover:after {
	position: absolute;
	bottom: 30px;
	content: "Wrong Date Format.";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #ec6c00;
	background-color: #ffeed3;
	border: 1px solid #e47c24;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.err-par-sta-ser {
	color: #d40000;
	border-color: #fb444db8 !important;
	position: relative;
	border-width: 2px !important;
	background: #fdb7b7;
}

.err-par-sta-ser:hover:after {
	position: absolute;
	bottom: 30px;
	content: "This parameter does not exists";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #cc0033;
	background-color: #fce4e4;
	border: 1px solid #ffb8b9;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.err-unique-ser {
	color: #d40000;
	border-color: #fb444db8 !important;
	position: relative;
	border-width: 2px !important;
	background: #fdb7b7;
}

.err-unique-ser:hover:after {
	position: absolute;
	bottom: 30px;
	content: "This field already exists";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #cc0033;
	background-color: #fce4e4;
	border: 1px solid #ffb8b9;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.err-sitenot-ser {
	color: #d40000;
	border-color: #fb444db8 !important;
	position: relative;
	border-width: 2px !important;
	background: #fdb7b7;
}

.err-sitenot-ser:hover:after {
	position: absolute;
	bottom: 30px;
	content: "This site doesn't exists";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #cc0033;
	background-color: #fce4e4;
	border: 1px solid #ffb8b9;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.err-catnamenot-ser {
	color: #d40000;
	border-color: #fb444db8 !important;
	position: relative;
	border-width: 2px !important;
	background: #fdb7b7;
}

.err-catnamenot-ser:hover:after {
	position: absolute;
	bottom: 30px;
	content: "This category name doesn't exists";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #cc0033;
	background-color: #fce4e4;
	border: 1px solid #ffb8b9;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.err-equtnnot-ser {
	color: #d40000;
	border-color: #fb444db8 !important;
	position: relative;
	border-width: 2px !important;
	background: #fdb7b7;
}

.err-equtnnot-ser:hover:after {
	position: absolute;
	bottom: 30px;
	content: "This equipment type doesn't exists";
	font-family: Helvetica;
	font-size: 13px;
	font-weight: bold;
	color: #cc0033;
	background-color: #fce4e4;
	border: 1px solid #ffb8b9;
	padding: 7px 11px 4px;
	width: auto;
	border-radius: 5px;
}

.mtable td {
	border: 1px solid #ddd;
	font-size: 13px;
	white-space: nowrap;
	text-align: center;
	padding: 1px;
}

.mtable th {
	border: 1px solid #ddd;
	vertical-align: top;
	padding: 7px;
}

.tr-bg {
	background: #fbebd2;
}

.tr-bg-ser {
	background: #fad2d2;
}

label.btn {
	padding: 5px !important;
}

#iframeloading {
	background-color: rgba(226, 226, 226, 0.4);
	z-index: 1001;
	position: fixed;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	display: none;
}

td <#template-dropdown {
	padding: 0px;
}
��
</style>


</head>
<body class="fixed-header">

	<div class="theme-loader">
		<div class="loader-track">
			<div class="loader-bar"></div>
		</div>
	</div>


	<input type="hidden" value="${access.userID}" id="hdneampmuserid">

	<input type="hidden" value="${classtate}" id="hdnstate">

	<jsp:include page="slider.jsp" />


	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					<a href="./dashboard"><img src="resources/img/logo01.png"
						alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx" id="SearcSElect">


					<div class="ui  search selection  dropdown  width oveallsearch">
						<input id="myInput" name="tags" type="text">

						<div class="default text">search</div>
						<!--           <i class="dropdown icon"></i> -->
						<div id="myDropdown" class="menu">

							<jsp:include page="searchselect.jsp" />


						</div>
					</div>










				</div>
				<div class="dropdown pull-right hidden-md-down user-log">
					<button class="profile-dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 circular inline"> <img
							src="resources/img/user_01.png" alt
							data-src="resources/img/user_01.png"
							data-src-retina="resources/img/user_01.png" width="32"
							height="32">
						</span>

						<c:if test="${not empty access}">
							<p class="user-login">${access.userName}</p>
						</c:if>
					</button>
					<div class="dropdown-menu dropdown-menu-right profile-dropdown"
						role="menu">
						<a class="dropdown-item disablecolor"><i
							class="pg-settings_small"></i> Settings</a> <a
							class="dropdown-item disablecolor"><i class="pg-outdent"></i>
							Feedback</a> <a class="dropdown-item disablecolor"><i
							class="pg-signals"></i> Help</a> <a href="logout1"
							class="dropdown-item color logout"><i class="pg-power"></i>
							Logout</a>
					</div>
				</div>




				<div class="newticketpopup hidden">
					<p class="center mb-1 tkts" data-toggle="modal"
						data-target="#tktCreation" data-backdrop="static"
						data-keyboard="false">
						<img src="resources/img/tkt.png">
					</p>
					<p class="create-tkts" data-toggle="modal"
						data-target="#tktCreation" data-backdrop="static"
						data-keyboard="false">New Ticket</p>
				</div>

				<%-- <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user.png" width="32" height="32">
                  </span>
                  
                  
                   <c:if test="${not empty access}">         
                  		<p class="user-login">${access.userName}</p>
                  </c:if>
                  
                  
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="login" class="dropdown-item color"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
               <span ><p class="center m-t-5 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img class="m-t-5" src="resources/img/tkt.png"><p class="create-tkts">New Ticket</p></p></span>
                --%>

				<!--  <p class="create-tkts">New Ticket</p> -->
				<!-- <a href="#" class="header-icon  btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><i class="fa fa-ticket fa-2x"></i></a> -->
				<!-- <a href="#" class="header-icon pg pg-alt_menu btn-link  sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a> -->
			</div>
		</div>
		<div class="page-content-wrapper ">
			<div class="content" id="QuickLinkWrapper1">
				<div class="container-fixed-lg">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="./dashboard"><i
								class="fa fa-home mt-7"></i></a></li>
						<li class="breadcrumb-item"><a>Configuration</a></li>
						<li class="breadcrumb-item active">Master Uploads</li>
					</ol>
				</div>
				<div class="container-fixed-lg padd-3">
					<div id="sitestatus" data-pages="card">
						<div class="containerItems">
							<!-- Table Start-->
							<div class="sortable" id="customerlist">
								<div class="col-md-12 padd-0">
									<div class="card" data-pages="card">

										<div class="card card-transparent  padd-5"
											style="padding-top: 0px !important;">

											<div class="row">
												<div class="col-md-12 padd-0"
													style="padding-left: 10px !important;">

													<ul class="nav nav-tabs nav-tabs-fillup hidden-sm-down"
														data-init-reponsive-tabs="dropdownfx">
														<li class="nav-item"><a href="#" class="active"
															data-toggle="tab" data-target="#slide1"><span>Master
																	Uploads</span></a></li>
													</ul>


												</div>
											</div>
											<div class="row">
												<div class="col-md-12 padd-0">
													<div class="col-md-10 padd-0"></div>
													<div class="col-md-2">

														<div class="dropdown" style="float: right;position:absolute;top:-33px;left:45px;">
															<button class="btn btn-default dropdown-toggle btn-sm"
																type="button" id="menu1" data-toggle="dropdown">
																<i class="fa fa-download"
														aria-hidden="true" style="margin-right: 5px;"></i>
																Download Template
															</button>
															<ul class="dropdown-menu" role="menu"
																aria-labelledby="menu1">
																<li ><a
														href="http://www.inspirece.com/pdf/Equipment.xlsx">Equipment</a></li>
														<li ><a
														href="http://www.inspirece.com/pdf/Parameters.xlsx">Parameters</a></li>
															</ul>
														</div>



														<!-- <a
														href="http://www.inspirece.com/pdf/Equipment.xlsx"
														style="float: right;position:absolute;top:-22px;left:51px;"><i class="fa fa-download"
														aria-hidden="true" style="margin-right: 5px;"></i>Download
														Template</a> -->
													</div>
												</div>
											</div>



											<div class="row">
												<div class="col-lg-12">
													<div class="col-lg-4">
														<div class="form-group">
															<label class="fields">Choose Master Screens</label> <select
																id="dropdown"
																class="ui fluid search selection dropdown width">
																<option value="">Master Screens</option>
																<!--   <option value="Activity">Activity</option>
                                                   <option value="Country">Country</option>
                                                   <option value="Country Region">Country Region</option>
                                                   <option value="Currency">Currency</option>
                                                   <option value="Customer">Customer</option>
                                                   <option value="Customer Type">Customer Type</option> -->
																<option value="Equipment">Equipment</option>
																<!--  <option value="Equipment Category">Equipment Category</option>
                                                   <option value="Equipment parameter">Equipment parameter</option>
                                                   <option value="Equipment Type">Equipment Type</option>
                                                   <option value="Event">Event</option>
                                                   <option value="Event Type">Event Type</option>
                                                   <option value="Parameter List">Parameter List</option>
                                                   <option value="Part">Part</option>
                                                   <option value="Role Mapping">Role Mapping</option>
                                                   <option value="Site">Site</option>
                                                   <option value="Site Type">Site Type</option>
                                                   <option value="SOP Detail">SOP Detail</option>
                                                   <option value="State">State</option>
                                                   <option value="Status">Status</option>
                                                   <option value="Time Zone">Time Zone</option>
                                                   <option value="Unit Of Measurement">Unit Of Measurement</option>
                                                   <option value="User">User</option>
                                                   <option value="User Role">User Role</option>
                                                   <option value="Version">Version</option> -->
																<option value="Parameters">Parameters</option>
															</select>

														</div>
													</div>
													<div class="col-lg-4 m-t-25">
														<div class="form-group">
															<input type="file" class="filestyle" data-icon="false"
																id="excelfile">


														</div>

													</div>
													<div class="col-lg-4 m-t-25">
														<!-- 	<input type="button" id="viewfile" class="btn btn-primary"
														value="Export To Table" onclick="ExportToTable()" /> <input
														type="button" id="" class="btn btn-primary"
														value="ValidateM" onclick="validate_mtable()" /> -->

														<div class="col-lg-12">
															<div class="col-lg-6">

																<button id="upload_toserver" class="btn btn-primary"
																	onclick="upload_toserver();" style="background: green;">
																	<i class="fa fa-upload" aria-hidden="true"
																		style="font-size: 15px; margin-left: 5px;"></i> Upload
																</button>

																<!--  <span class="" onclick="upload_toserver();" style="margin-left: 5px;cursor:pointer;">
<i class="fa fa-upload" aria-hidden="true" style="font-size: 20px;margin-left: 5px;"></i>
<span style="font-weight: 600;margin-left: 5px;">Upload</span></span> -->

															</div>
															<div class="col-lg-6">
																<table id="count-table">
																	<tr>
																		<td>Total Rows</td>
																		<td id="total-row-count" style="font-weight: bold;">-</td>
																	</tr>
																	<tr>
																		<td style="color: #ea7f00;">Rows with Error</td>
																		<td style="color: #ea7f00; font-weight: bold;"
																			id="error-row-count">-</td>
																	</tr>
																</table>
															</div>

														</div>



													</div>
												</div>
												<div class="col-lg-12 hidden" id="row-datalogger">
													<div class="col-lg-4">

														<div class="form-group">
															<label class="fields">Datalogger version</label> <select
																id="par-dropdown"
																class="ui fluid search selection dropdown width">
																<option value="">Select</option>
															</select>
														</div>
													</div>
												</div>
											</div>


											<div class="row">
												<div class="col-md-12" style="overflow: auto;">
													<table id="exceltable" class="mtable" tblcolumnnames=""
														style="margin-top: 10px;">
													</table>
													<!-- <input type="button" id="validate" class="btn btn-primary"
														value="Validate" /> -->
													<!-- <table id="nodata" class="table table-bordered">  
                                             <tr><td>No Data Found</td></tr>
                                          </table> -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Table End-->
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="container-fixed-lg footer">
			<div class="container-fluid copyright sm-text-center">
				<p class="small no-margin pull-left sm-pull-reset">
					<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
						CLEAN ENERGY.</span> <span class="hint-text">All rights reserved.
					</span>
				</p>
				<p class="small no-margin pull-rhs sm-pull-reset">
					<span class="hint-text">Powered by</span> <span>MESTECH
						SERVICES PVT LTD</span>.
				</p>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	</div>

	<!-- Side Bar Content Start-->

	<!-- Side Bar Content End-->
	<!--   
      <div class="quickview-wrapper  builder hidden-sm hidden-xs" id="builder">
         <div class="p-l-10 p-r-0 ">
            <a class="builder-close quickview-toggle pg-close"  href="#"></a>
            <a class="builder-toggle" data-toggle="quickview" data-toggle-element="#builder"><img src="resources/img/ImageResize_06.png"></a>
            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary" id="builderTabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tabLayouts" role="tab" aria-controls="home"><span>Quick Links</span></a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active " id="tabLayouts" role="tabcard">
                  <div class="scrollable">
                     <div class="p-r-50">
                        <div class="list">
                           <ul>
                              <li><a href="#QuickLinkWrapper1">Customer List dfdfds</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div> -->
	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>





	<div id="gotop"></div>

	<!-- Address Popup Start !-->
	<!-- Modal -->
	<div id="tktCreation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header Popup-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="resources/img/close.png">
					</button>
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<p class="modal-title tkt-title">
						<img src="resources/img/tkt.png"><span class="tkt-title">New
							Ticket</span>
					</p>
				</div>
				<div class="modal-body">
					<c:url var="addAction" value="/addnewticket"></c:url>
					<div class="col-md-12">
						<p class="tkt-mandat">
							<span class="errormess">*</span> All fields are mandatory
						</p>
					</div>
					<form:form action="${addAction}" modelAttribute="ticketcreation"
						class="ui form validation-form">

						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlSite" name="ddlSite" path="siteID"
									onchange="getEquipmentAgainstSite()">
									<form:option value="">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Equipment</label>
							</div>

							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width equipments"
									id="ddlEquipment" path="equipmentID" name="ddlEquipment">
									<option value="">Select</option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Type</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlType" name="ddlType" path="ticketType">
									<form:option value="">Select</form:option>
									<form:option value="Operation">Operation</form:option>
									<form:option value="Maintenance">Maintenance</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Category</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width category"
									id="ddlCategory" name="ddlCategory" path="ticketCategory">
									<form:option value="">Select </form:option>
									<form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
									<form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
									<form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
									<form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
									<form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
									<form:option data-value="Operation"
										value="Equipment Replacement">Equipment Replacement</form:option>
									<form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
									<form:option data-value="Operation" value="String Down">String Down</form:option>
									<form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
									<form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
									<form:option data-value="Operation" value="">Select</form:option>
									<form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
									<form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="DataLogger Cleaning">DataLogger Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="String Current Measurement">String Current Measurement</form:option>
									<form:option data-value="Maintenance"
										value="Preventive Maintenance">Preventive Maintenance</form:option>

									<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
									<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
									<form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
									<form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
									<form:option data-value="Maintenance" value="">Select</form:option>
								</form:select>
							</div>

						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Subject</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<form:input placeholder="Subject" name="Subject" type="text"
										autocomplete="off" id="txtSubject" path="ticketDetail"
										maxLength="50" />
								</div>
							</div>
						</div>

						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Priority</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlPriority" name="user" path="priority">
									<form:option value="">Select </form:option>
									<form:option data-value="3" value="3">High</form:option>
									<form:option data-value="2" value="2">Medium</form:option>
									<form:option data-value="1" value="1">Low</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Description</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<textarea id="txtTktdescription" autocomplete="off"
										name="description" style="resize: none;"
										placeholder="Ticket Description" maxLength="120"></textarea>
								</div>

							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
							<div class="btn btn-success  submit" id="btnCreate">Create</div>
							<button class="btn btn-success" type="button" id="btnCreateDummy"
								tabindex="7" style="display: none;">Create</button>
							<div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
						</div>
						<!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
						<!-- <div class="ui error message"></div> -->
					</form:form>
					<!-- <div class="ui error message"></div> -->

				</div>
			</div>
		</div>
	</div>
	<select id="template-dropdown" style="display: none; width: 100%">

		<option value="">Select</option>
		<option value="Active">Active</option>
		<option value="Inactive">Inactive</option>
	</select>
	<select id="template-dropdown-pri-sec"
		style="display: none; width: 100%">

		<option value="">Select</option>
		<option value="Primary">Primary</option>
		<option value="Secondary">Secondary</option>
	</select>
	<!-- Address Popup End !-->
	<script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
	<script>
         $(document).ready(function() {
            $(window).fadeThis({
               speed: 500,
            });
         });
      </script>
	<script type="text/javascript">

          var ddlNames = "Activity|Country|Country Region|Currency|Customer|Customer Type|Equipment|Equipment Category|Equipment parameter|Equipment Type|Event|Event Type|Parameter List|Part|Role Mapping|Site|Site Type|SOP Detail|State|Status|TimeZone|Unit Of Measurement|User|User Role|Version|Parameters";
         var ddlArrayList = ddlNames.split('|');


         var ActivityColumn      = "Activity Name|Activity Description|Status";
         var CountryColumn       = "Country Code|Country Name|Region|Status";
         var CountryRegionColumn =  "Country Region Name|Status";
         var CurrencyColumn      =  "Currency Name|Symbol|Status";
         var CustomerColumn      =  "Customer Name|Customer Type|Customer Organization|Customer Description|Customer Group|Primary Customer|Contact Person|Address|City|State|Country|Postal Code|Email ID|Mobile|Telephone|Fax|Website|Status";
         var CustomerTypeColumn  =  "Customer Type|Short Name|Description|Status";
         var EquipmentColumn     =  "SiteName|CategoryName|EquipmentTypeName|PrimarySerialNumber|Description|Remarks|CustomerReference|CustomerNaming|Capacity|ActiveFlag|installationDate|warrantyDate|equipmentSelection|components|disconnectRating|disconnectType";
         var EquipementCateColumn = "Equipment Category|Site Type|Category Group|Category Description|Short name|Status";
         var EquipemetParaColumn  = "ParameterName|EquipmentTypeID|Sequence|ListID|UOMID|SourceName|StandardName|Equipmentparameter Status";
         var EquipemetTypeColumn  = "Equipment Type|Category|Part Type|Description|Display Name|Parameter Version|SOP Version|Remarks|Customer Reference|Customer Naming|Capacity|Unit of Measurement|Manufacturer|Module Type|Status";
         var EventColumn          = "EventCode|Name|Event Type|Description|Type ID|Severity|Status";
         var EventTypeColumn      = "Event Type|Short Name|Description|DataLogger ID|Severity|Event Type Status";
         var ParameterListColumn  = "Equipment Type ID|Status";
         var PartColumn           = "Part Type|Status";
         var RoleMappingColumn    = "Role|Activity|Status";
         var SiteColumn           = "Customer|Site Type|Site Code|Site Name|Site Description|Contact Person|Address|City|State|Country|Postal Code|Longitude|Latitude|Altitude|Site Image|Installation Capacity|Site PO Number|Site PO Date|Site Handover Date|Site Commisioning Date|Installed On|Site Operator|Site Manafacturer|Module Name|Communication Type|Collection Type|File Type|Customer Reference|Customer Naming|Income|Currency|Email ID|Mobile|Telephone|Fax|Data Logger|Operation Mode|Remote FTP Server|Remote FTP Username|Remote FTP Password|Remote FTP Server port|Remote FTP Directorypath|File Prefix|File Suffix|File Substring|Sub Folder|LocalFTP Directory|Local FTP Directorypath|Local FTP Home Directory|LocalFTP Username|Local FTP Password|API Url|API Key|Service Code|Status";
         var SiteTypeColumn       = "Site Type|Short Name|Description|Status";
         var SOPDetailColumn      = "SOP Name|Version ID|Equipment Type ID|File Name|File Path|Status";
         var StateColumn          = "State Code|State Name|Zone|State Type|Country|Region|Status";
         var StatusColumn         = "Status Code|Status|Category|Description|Reference ID";
         var TimezoneColumn       = "Timezone Code|Timezone Name|UTC Offset|Timezone Format|Status";
         var UOMColumn            = "Unit of Measurement|Description|Symbol|Group|Status";
         var UserColumn           = "User Code|User Name|Designation|Department|Role|Blood Group|Date Of Birth|Mobile Number|Email ID|Status";
         var UserRoleColumn       =  "Name|Description|Status";
         var VersionColumn        = "Version Code|Status";
         var Parameters           = "parametername|parameteruom|standardname|sequenceid|coefficient";


         var myHeaderArrayList = [];
         myHeaderArrayList.push(ActivityColumn.split('|'));
         myHeaderArrayList.push(CountryColumn.split('|'));
         myHeaderArrayList.push(CountryRegionColumn.split('|'));
         myHeaderArrayList.push(CurrencyColumn.split('|'));
         myHeaderArrayList.push(CustomerColumn.split('|'));
         myHeaderArrayList.push(CustomerTypeColumn.split('|'));
         myHeaderArrayList.push(EquipmentColumn.split('|'));
         myHeaderArrayList.push(EquipementCateColumn.split('|'));
         myHeaderArrayList.push(EquipemetParaColumn.split('|'));
         myHeaderArrayList.push(EquipemetTypeColumn.split('|'));
         myHeaderArrayList.push(EventColumn.split('|'));
         myHeaderArrayList.push(EventTypeColumn.split('|'));
         myHeaderArrayList.push(ParameterListColumn.split('|'));
         myHeaderArrayList.push(PartColumn.split('|'));
         myHeaderArrayList.push(RoleMappingColumn.split('|'));
         myHeaderArrayList.push(SiteColumn.split('|'));
         myHeaderArrayList.push(SiteTypeColumn.split('|'));
         myHeaderArrayList.push(SOPDetailColumn.split('|'));
         myHeaderArrayList.push(StateColumn.split('|'));
         myHeaderArrayList.push(StatusColumn.split('|'));
         myHeaderArrayList.push(TimezoneColumn.split('|'));
         myHeaderArrayList.push(UOMColumn.split('|'));
         myHeaderArrayList.push(UserColumn.split('|'));
         myHeaderArrayList.push(UserRoleColumn.split('|'));
         myHeaderArrayList.push(VersionColumn.split('|'));
         myHeaderArrayList.push(Parameters.split('|'));

         

         $(function () {            
        $('select#dropdown').change(function () {
        	$('#row-datalogger').addClass('hidden');
            var SelectedValue = $(this).val();
            // alert(SelectedValue = $(this).val());
            if (SelectedValue==ddlArrayList[0])
            {
               alert('Sucess');
               $("#exceltable").attr("tblcolumnnames",ActivityColumn);
            }
            else if(SelectedValue==ddlArrayList[1])
            {
               alert('hai');
                $("#exceltable").attr("tblcolumnnames",CountryColumn);
            }
            else if(SelectedValue==ddlArrayList[2])
            {
                $("#exceltable").attr("tblcolumnnames",CountryRegionColumn);
            }
            else if(SelectedValue==ddlArrayList[3])
            {
                $("#exceltable").attr("tblcolumnnames",CurrencyColumn);
            }
            else if (SelectedValue ==ddlArrayList[4])
            {
               $("#exceltable").attr("tblcolumnnames",CustomerColumn);
            }
            else if (SelectedValue ==ddlArrayList[5])
            {
               $("#exceltable").attr("tblcolumnnames",CustomerTypeColumn);
            }
            else if (SelectedValue ==ddlArrayList[6])
            {
               $("#exceltable").attr("tblcolumnnames",EquipmentColumn);
            }
            else if (SelectedValue ==ddlArrayList[7])
            {
               $("#exceltable").attr("tblcolumnnames",EquipementCateColumn)
            }
            else if (SelectedValue ==ddlArrayList[8])
            {
               $("#exceltable").attr("tblcolumnnames",EquipemetParaColumn)
            }
            else if (SelectedValue ==ddlArrayList[9])
            {
               $("#exceltable").attr("tblcolumnnames",EquipemetTypeColumn)
            }
            else if (SelectedValue ==ddlArrayList[10])
            {
               $("#exceltable").attr("tblcolumnnames",EventColumn)
            }
            else if (SelectedValue ==ddlArrayList[11])
            {
               $("#exceltable").attr("tblcolumnnames",EventTypeColumn)
            }
            else if (SelectedValue ==ddlArrayList[12])
            {
               $("#exceltable").attr("tblcolumnnames",ParameterListColumn)
            }
            else if (SelectedValue ==ddlArrayList[13])
            {
               $("#exceltable").attr("tblcolumnnames",PartColumn)
            }
            else if (SelectedValue ==ddlArrayList[14])
            {
               $("#exceltable").attr("tblcolumnnames",RoleMappingColumn)
            }
            else if (SelectedValue ==ddlArrayList[15])
            {
               $("#exceltable").attr("tblcolumnnames",SiteColumn)
           }
            else if (SelectedValue ==ddlArrayList[16])
            {
               $("#exceltable").attr("tblcolumnnames",SiteTypeColumn)
            }
            else if (SelectedValue ==ddlArrayList[17])
            {
               $("#exceltable").attr("tblcolumnnames",SOPDetailColumn)
            }
            else if (SelectedValue ==ddlArrayList[18])
            {
               $("#exceltable").attr("tblcolumnnames",StateColumn)
            }
            else if (SelectedValue ==ddlArrayList[19])
            {
               $("#exceltable").attr("tblcolumnnames",StatusColumn)
            }
            else if (SelectedValue ==ddlArrayList[20])
            {
               $("#exceltable").attr("tblcolumnnames",TimezoneColumn)
            }
            else if (SelectedValue ==ddlArrayList[21])
            {
               $("#exceltable").attr("tblcolumnnames",UOMColumn)
            }
            else if (SelectedValue ==ddlArrayList[22])
            {
               $("#exceltable").attr("tblcolumnnames",UserColumn)
            }
            else if (SelectedValue ==ddlArrayList[23])
            {
               $("#exceltable").attr("tblcolumnnames",UserRoleColumn)
            }
            else if (SelectedValue ==ddlArrayList[24])
            {
               $("#exceltable").attr("tblcolumnnames",VersionColumn)
            }
            else if (SelectedValue ==ddlArrayList[25])
            {
               $("#exceltable").attr("tblcolumnnames",Parameters);
               $('#row-datalogger').removeClass('hidden');
               load_drop_datalogger();
               
            }
        });
    });

</script>

	<script type="text/javascript">
       function ExportToTable() {
    	   if(!($("#dropdown").val())){
          	 toastr["error"]("Select Master Screen!");
          	 $('#excelfile').val('');
          	 return;
           }
         var tHeaderNames = $("#exceltable").attr('tblcolumnnames');
         $("#exceltable tr").remove();
     	$('#total-row-count').text('-');
    	$('#error-row-count').text('-');
            var tHeaderNamesArray = tHeaderNames.split("|");   
           /*  var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;   */
         var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx)$/;  
         /*Checks whether the file is a valid excel file*/  
         if (regex.test($("#excelfile").val().toLowerCase())) {  
             var xlsxflag = false; /*Flag for checking whether excel is .xls format or .xlsx format*/  
             if ($("#excelfile").val().toLowerCase().indexOf(".xlsx") > 0) {  
                 xlsxflag = true;  
             }  
             /*Checks whether the browser supports HTML5*/  
             if (typeof (FileReader) != "undefined") {  
                 var reader = new FileReader();  
                 reader.onload = function (e) {  
                     var data = e.target.result; 
                     
                      
                     var binary = "";
                     var bytes = new Uint8Array(data);
                     var length = data.byteLength;
                     for (var i = 0; i < length; i++) {
                       binary += String.fromCharCode(bytes[i]);
                     } 
                     
                     console.log(e);
                     /*Converts the excel data in to object*/  
                     if (xlsxflag) {  
                         var workbook = XLSX.read(binary, { type:'binary'});  
                     }  
                     else {  
                         var workbook = XLS.read(binary, { type: 'binary' });  
                     }  
                     /*Gets all the sheetnames of excel in to a variable*/  
                     var sheet_name_list = workbook.SheetNames;  
      
                     var cnt = 0; /*This is used for restricting the script to consider only first sheet of excel*/  
                     sheet_name_list.forEach(function (y) { /*Iterate through all sheets*/  
                         /*Convert the cell value to Json*/  
                         if (xlsxflag) {  

                             var exceljson = XLSX.utils.sheet_to_json(workbook.Sheets[y],{skipUndfendVale: false,defval:""});  
                         }  
                         else {  
                             var exceljson = XLS.utils.sheet_to_row_object_array(workbook.Sheets[y],{skipUndfendVale: false,defval:""});  
                         } 
                     if(exceljson.length==0){
                    	 toastr["error"]("No Data available!");
                    	 return;
                     }
                         if (exceljson.length > 0 && cnt == 0) {  
                           var count =0;
                         for (x in exceljson[0]) {
                        count +=1;
                     }  
                     
                  /*   $.each(exceljson, function(index, element) {
                    	    console.log(index);
                    	    console.log(element);
                    	    console.log(Object.keys(element).length); 
                    	    if(count<Object.keys(element).length){
                    	    	count=Object.keys(element).length;
                    	    }
                    	});  */
                    console.log(count);
                     
                     var headercount =tHeaderNamesArray.length;
                     /* count==headercount  */
                        if(count==headercount)
                        {
                        for(var i=0;i<headercount;i++)
                           {
                              if (count == headercount) {
                                 $('#exceltable').show(); 
                              }
                              else {
                                 $('#exceltable').hide(); 
                                   }
                              }
                        console.log("exceljson-----");
                     console.log(exceljson);
                                  BindTable(exceljson,'#exceltable');  
                                  $("#nodata").hide();
                                 cnt++;  
                     }
                     else {
                        
                        toastr["error"]("Upload data as per template");
                        $('#excelfile').val('');
                     }
                  }  
                     }); 
                 }  
                 if (xlsxflag) {/*If excel file is .xlsx extension than creates a Array Buffer from excel*/  
                     reader.readAsArrayBuffer($("#excelfile")[0].files[0]);  
                 }  
                 else {  
                     reader.readAsArrayBuffer($("#excelfile")[0].files[0]);  
                    /*  reader.readAsBinaryString($("#excelfile")[0].files[0]);   */
                 }  
             }  
             else {  
                 toastr["error"]("Sorry! Your browser does not support HTML5!");
             }  
         }  
         else {  
             $('#exceltable').show(); 
             toastr["error"]("Upload data in .xlsx format");
         }  
         
         $("#excelfile").val('');
     }  
</script>
	<script type="text/javascript">
	
	 var columnsName = null;
   function BindTable(jsondata, tableid) {/*Function used to convert the JSON array to Html Table*/  
     var columns = BindTableHeader(jsondata, tableid); /*Gets all the column headings of Excel*/  
     columnsName=columns;
      var firstrowvalues;
       var jsonData=[];
     for (var i = 0; i < jsondata.length; i++) {  
         var row$ = $('<tr id="tblRow-' + i + '"></tr>');  
         for (var colIndex = 0; colIndex < columns.length; colIndex++) {  
             var cellValue = jsondata[i][columns[colIndex]];  
             if (cellValue == null)  
                 cellValue = "";  
             row$.append($('<td contenteditable="true" id="tblRowCell-' + i + '-' + colIndex + '"></td>').html(cellValue));  
             var remainingrows;
             if(colIndex==0){
                var columnvalues = columns[colIndex];
                 firstrowvalues = '{' + '"'+ columnvalues +'"' +':'+ '"'+ cellValue +'"' ;
               // alert(firstrowvalues);

             }
             else{
                var columnvalues = columns[colIndex];
                firstrowvalues = firstrowvalues + ',' + '"'+  columnvalues +'"' +':'+ '"'+ cellValue +'"' ;
             }

         }  
/*           console.log(firstrowvalues + '}'); */



          jsonData.push(firstrowvalues + '}');


          

         //MOHAN
     /*      row$.append($('<td class="center"><a><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>')); */
          /* row$.append($('<td class="center"><span class="delete-rowval"><a><i class="fa fa-trash-o"></i></a></span></td>')); */
         $(tableid).append(row$);  
     }  



var myjson = '['+ jsonData +']'

/* alert(myjson); */

// AjaxCallForSaveOrUpdate(myjson);

 
     console.log(myjson); 
     debugger;
     if($("#dropdown").val()=="Equipment"){
    	 post_equipment();
    	 post_equipment2();
     }
     
     if(!validate_mtable()){
    	 
    	 if($("#dropdown").val()=="Parameters"){
    		 
    	 }else{
    	 toastr["error"]("There are some errors on fields.", "Validation failed.");
    	 }
     }
 }  
 
 function BindTableHeader(jsondata,tableid){/*Function used to get all column names from JSON and bind the html table header*/  
     var columnSet = [];  
     var headerTr$ = $('<tr style="background:burlywood;"/>');  
     for (var i = 0; i < jsondata.length; i++) {  
         var rowHash = jsondata[i];  
         for (var key in rowHash) {  
             if (rowHash.hasOwnProperty(key)) {  
                 if ($.inArray(key, columnSet) == -1) {/*Adding each unique column names to a variable array*/  
                     columnSet.push(key);  
                     headerTr$.append($('<th/>').html(key));  
                 }  
             }  
         }  
     }  
/*      headerTr$.append($('<th>Edit</th>')); */
   /*   headerTr$.append($('<th>Delete</th>')); */
     $(tableid).append(headerTr$);  
     return columnSet;  
 }
</script>
	<!-- Grid Validation -->
	<script type="text/javascript">
function checknotempty (value)
{
    var boolResult = false;

   if(value == null)
   {
      return false;
   }
   else if(value.length ==0 || value.trim() =="")
   {
      return false;
   }
   else
   {
      return true;
   }

}


function checkstatus (value)
{
   var boolResult = false;

 if(value != null)
   {
        if(value == 0 || value == 1)
         {
         boolResult = true;
         }
   }
 

  

   return boolResult;
}


function checkequallength (value,checknotnull,length) 
{
    var boolResult = false;

if(value != null)
   {
   if(value.length == length)
   {
      boolResult = true;
   }
}

   if (checknotnull == true) 
   {
      if(value == null || value.trim() == "")
      {
         boolResult = false;
      }
   }

   return boolResult;
}


function checkmaxlength (value,checknotnull,maxlength) 
{
    var boolResult = false;

if(value != null)
   {
   if(value.length <= maxlength)
   {
      boolResult = true;
   }
}


    if(checknotnull==true )
   {
      if(value==null || value.trim() == "")
      {
         boolResult = false;
      }
   }
  
     return boolResult;
}


function checklengthrange (value,checknotnull,minlength,maxlength)
{
   var boolResult = false;

if(value != null)
   {
  if(value.length >= minlength || value.length <= maxlength)
   {
       boolResult = true;
   } 
}

   if(checknotnull==true )
   {
      if(value==null || value.trim() == "")
      {
         boolResult = false;
      }
   }
  
     return boolResult ;
}


function checkemailformat (value,checknotnull)
{
   debugger;
   var boolResult = false;
   var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

if(value != null)
   {
      alert(Email);
      if(value == emailPattern) {
         boolResult = true;
      }

   //logic required
      
   
}


   if(checknotnull == true)
   {
      if(value == null || value.trim() == "")
      {
         boolResult = false;
      }
   }

   return boolResult;
}


function checkdateformat (value,checknotnull,format)
{
   var boolResult = false;

if(value != null)
   {
  
   //logic required
      boolResult = true;

}



   if(checknotnull == true)
   {
      if(value == null || value.trim() == "")
      {
         boolResult = false;
      }
   }

   return boolResult;
}


function checkpastdate (value,checknotnull,format)
{
   var boolResult = false;

if(value != null)
   {
    //logic required
      boolResult = true;
}



   if(checknotnull == true)
   {
      if(value == null || value.trim() == "")
      {
         boolResult = false;
      }
   }

   return boolResult;
}

function checkfuturedate (value,checknotnull,format)
{
   var boolResult = false;

if(value != null)
   {
   //logic required
      boolResult = true;
}

   if(checknotnull == true)
   {
      if(value == null || value.trim() == "")
      {
         boolResult = false;
      }
   }

   return boolResult;
}


function checkboolean (value,checknotnull)
{
   var boolResult = false;

if(value != null)
   {
    //logic required
      boolResult = true;
}

   if(checknotnull == true)
   {
      if(value == null || value.trim() == "")
      {
         boolResult = false;
      }
   }

   return boolResult;
}



function checklength (value,checknotnull,length)
{
  var boolResult = false;

   if(value.length == length)
   {
      boolResult = true;
   }
  


   if(checknotnull==true )
   {
      if(value==null || value == "")
      {
         boolResult = false;
      }
   }
  
     return boolResult ;
}


function checkwebsiteformat (value,checknotnull)
{
   var boolResult = false;

   if(value != null)
   {
    //logic required
      boolResult = true;
}

   if(checknotnull == true)
   {
      if(value == null || value == "")
      {
         boolResult = false;
      }
   }

   return boolResult;
}


function checknumeric (value,checknotnull,numeric)
{
    var boolResult = false;

   if(value.length <= numeric)
   {
      boolResult = true;
   }


   if (checknotnull == true) 
   {
      if(value == null || value == " ")
      {
         boolResult = false;
      }
   }

   return boolResult;
}


function checkfloat (value,checknotnull,float)
{
   var boolResult = false;

   if(value.length <=float)
   {
      boolResult = true;
   }

   if(checknotnull == true)
   {
      if(value == null || value == " ")
      {
         boolResult = false;
      }
   }

   return boolResult;
}


function checkalphabetonly (value,checknotnull,alphabetsonly)
{
   var boolResult = false;

   if(value.length <= alphabetsonly)
   {
      boolResult = true;
   }

   if(checknotnull == true)
   {
      if(value == null || value == " ")
      {
         boolResult = false;
      }
   }

   return boolResult;
}



function checkalphabetspecial (value,checknotnull,alphabetspl)
{
   var boolResult = false;

   if(value.length <=alphabetspl)
   {
      boolResult = true;
   }

   if(checknotnull == true)
   {
      if(value == null || value == " ")
      {
         boolResult = false;
      }
   }

   return boolResult;
}


function checkalphanumericonly (value,checknotnull,numericonly)
{
   var boolResult = false;

   if(value.length <= numericonly)
   {
      boolResult = true;
   }

   if(checknotnull == true)
   {
      if(value == null || value == " ")
      {
         boolResult = false;
      }
   }

   return boolResult;
}


function checkalphanumericspecial (value,checknotnull, alphanumeric)
{
   var boolResult = false;

   if(value.length <= alphanumeric)
   {
      boolResult = true;
   }

   if(checknotnull == true)
   {
      if(value == null || value == " ")
      {
         boolResult = false;
      }
   }

   return boolResult;
}

function checkcustomvalidation (value,checknotnull, customvalidate)
{
   var boolResult = false;

   if(value.length <= customvalidate)
   {
      boolResult = true;
   }

   if(checknotnull == true)
   {
      if(value == null || value == " ")
      {
         boolResult = false;
      }
   }

   return boolResult;
}

/*** Error Message ****/


function checknotemptyerror (column,value,checknotnull)
{  
 return column + " is empty or null. Value must be required.";
}

function checkstatuserror (column, value,checknotnull)
{
   return column + " Choose Status";
}


function checkequallengtherror(column,value,checknotnull)
{

}

function checklengtherror (column,value,length)
{  
   return column + " length not matching with criteria. Length must be " + length;
}


function checklengthrangeerror (column,value,minlength,maxlength)
{
  
 return column + " length not exist with in range. Length exist in range (Min: " + minlength+" & Max: " + maxlength + ") ";
}



function checkmaxlengtherror (column,value,checknotnull) 
{
   return column + " is maxlength"
}

function checknumericerror (column,value,checknotnull)
{
   return column + " is not numeric value";
}

function checkfloatingerror (column,value,checknotnull)
{
   return column + " is not floating value. Enter Floating value";
}


function checkemailformaterror(column,value,checknotnull)
{
   return column + " Enter valid Email";
}

function checkalphabetspecialerror (column, value, checknotnull)
{
   return column + " Special Characters are allowed";
}

function CheckAlphaWithoutSplChar (column,value,checknotnull)
{
   return column + " Special Characters are not allowed";
}


function checkdateformaterror (column,value,checknotnull)
{
   return column + " Enter Date Only";
}

function checkwebsiteformat (column, value, checknotnull)
{
   return column + " Enter Valid url address";
}




   $(document).ready(function(){
      $("#validate").click(function(){
     var myTableArray = [];

$("table#exceltable tr").each(function() {
    var arrayOfThisRow = [];
    var tableData = $(this).find('td');
    if (tableData.length > 0) {
        tableData.each(function() { arrayOfThisRow.push($(this).text()); });
        myTableArray.push(arrayOfThisRow);
    }
});

 var filevalidation = true;


for(var i=0;i< myTableArray.length;i++)
{
   var row="";
   var rowvalidation = true;

   for(var j=0;j< myTableArray[i].length-2;j++)
{

     if($("#dropdown").val()== ddlArrayList[0])
     {

         var cellheader = myHeaderArrayList[0][j];
         var cellvalue = myTableArray[i][j];
         var cellvalidation = true;

         var cellerrorbgcolor = "#f35454";
         var cellerrorfgcolor = "#ffffff";

         var cellerrormessage = "";

        if(cellheader==myHeaderArrayList[0][0])
        {
         if(checklength(cellvalue,5) == false)
            {
                  filevalidation = false;
                  cellvalidation = false;
                  cellerrormessage = cellerrormessage + "</br>" + checklengtherror(cellheader,cellvalue,5);
            }
            else if(checklengthrange(cellvalue,3,150) == false)
            {
                  filevalidation = false;
                  cellvalidation = false;
                  cellerrormessage = cellerrormessage + "</br>" + checklengthrangeerror(cellheader,cellvalue,3,50);
            } 
            else if(checknotempty(cellvalue) == false)
            {
                  filevalidation = false;
                  cellvalidation = false;
                  cellerrormessage = cellerrormessage + "</br>" + checknotemptyeerror(cellheader,cellvalue);
            } 
         // if(checklength(cellvalue,1) == false)
         //    {
         //       alert(5);
         //          filevalidation = false;
         //          cellvalidation = false;
         //          cellerrormessage = cellerrormessage + "</br>" + checklengtherror(cellheader,cellvalue,1);
         //    }
         //     if(checkmaxlength(cellvalue,1) == false)
         //    {
         //       debugger;
         //          filevalidation = false;
         //          cellvalidation = false;
         //          cellerrormessage = cellerrormessage + "</br>" + checklengtherror(cellheader,cellvalue,1);
         //    }
         //    else if(checklengthrange(cellvalue,2,5) == false)
         //    {
         //       debugger;
         //       alert(4);
         //          filevalidation = false;
         //          cellvalidation = false;
         //          cellerrormessage = cellerrormessage + "</br>" + checklengthrangeerror(cellheader,cellvalue,2,8);
         //    } 
         //    else if(checknotempty(cellvalue) == false)
         //    {
         //       debugger;
         //          filevalidation = false;
         //          cellvalidation = false;
         //          cellerrormessage = cellerrormessage + "</br>" + checknotemptyeerror(cellheader,cellvalue);
         //    } 
         //    else if (checkmaxlength(cellvalue) == false)
         //    {
         //       filevalidation = false;
         //       cellvalidation = false;
         //       cellerrormessage = cellerrormessage + "<br>" + checkmaxlength(cellheader,cellvalue);
         //    }

            if(cellvalidation == false)
            {
               debugger;
               var tblRowCellId = "tblRowCell-" + i + "-" + j;
               alert(tblRowCellId);
               console.log(tblRowCellId);
               // $("#" + tblRowCellId).addClass('.error');
               $('#' + tblRowCellId).css('background','#f35454');
            }

        }

        if(cellheader==myHeaderArrayList[0][1])
        {
             if(checknotempty(cellvalue) == false)
            {  
               debugger;
               alert('First Column');
                  filevalidation = false;
                  cellvalidation = false;
                  cellerrormessage = cellerrormessage + "</br>" + checknotemptyeerror(cellheader,cellvalue);
            } 

             if(cellvalidation == false)
            {
               debugger;

               var tblRowCellId = "tblRowCell-" + i + "-" + j;
               alert(tblRowCellId);
               console.log(tblRowCellId);
               // $("#" + tblRowCellId).addClass('.error');
               $('#' + tblRowCellId).css('background','#f35454');
            }
        }
   

     }
     else if($("#dropdown").val()== ddlArrayList[1])
     {

     }
}

}
 }) 
});
   
 /*   $("#exceltable tr:gt(0)").each(function () {
	      /*   var this_row = $(this); */
	      /*   var productId = $.trim(this_row.find('td:eq(0)').html());//td:eq(0) means first td of this row
	        var product = $.trim(this_row.find('td:eq(1)').html())
	        var Quantity = $.trim(this_row.find('td:eq(2)').html()) 
	        this.find('td:eq(0)').css("background","red");
	    }); */
	    
	    
	    
	    
	    function load_drop_datalogger(){

			$.ajax({
						url : './getAlldataloggerFromStandards',
						type : 'GET',
						success : function(msg) {
							var responseStatus = msg.status;
							if (responseStatus == "Success") {
								var siteDocuments = msg.data;
								if (siteDocuments.length) {
									$('#par-dropdown').dropdown('clear');
									$('#par-dropdown').dropdown('empty');
									$('#par-dropdown').append("<option value=''>Select</option>");
									for (i = 0; i < siteDocuments.length; i++) {
										
										$('#par-dropdown').append("<option value="+siteDocuments[i].dataloggerid+">"+siteDocuments[i].description+"</option>");
									}
								}else{
									toastr["warning"]("No new dataloggers found.", "No dataloggers."); 
								}
								
							} else {
								
							}
						},
						error : function(err) {

						}
					});
		}	
	    
	    $('#par-dropdown').on("change",function(){
	    	validate_mtable();
	    });
	    
</script>

	<script type="text/javascript">
  //------------------------------------------------------------------Table validation-----------------------------------------------------------//
  var sel_file_type;
  
  
  $('#upload_toserver').prop('disabled',true);	
  function ServerCallForSaveOrUpdate(myjson) { 
	/*   $('#iframeloading').show(); */
	$(".theme-loader").show();
	 $.ajax({
	                       type: 'POST',
	                       contentType : 'application/json; charset=utf-8',
	                       dataType : 'json',
	                       url: './masterSheet?sheetType='+sel_file_type,
	                       data: myjson,
	                       success: function(msg){
	                    	   if(msg.status=="Success"){	
	                    		   toastr["success"]("", "Successfully uploaded.");
	                    		   $(".theme-loader").hide();
	                    		   $("#exceltable tr").remove();	
	                    		   $("#dropdown").dropdown("clear"); 	
	                    		   $("#par-dropdown").empty();
	                    			$('#total-row-count').text('-');
	                    	    	$('#error-row-count').text('-');
	                    	   }else{	
	                    		   toastr["error"]("There are some errors in the document you uploaded.", "Upload Failed"); 
	                    		   $(".theme-loader").hide();
	                    		   server_validation(msg.data);
	                    	   }
	                    	   console.log("result...........");
                           console.log(msg);
	                    	 /*   var errormessage = msg.Errorlog1;
	                    	   var errormessage1 = msg.Errorlog2;
	                       	if(errormessage!=null && errormessage!="")
	             				{

	               	    		toastr.error(errormessage);        					
	             				}
	                    	if(errormessage1!=null && errormessage1!="")
             				{

               	    		toastr.error(errormessage1);        					
             				} */
	                     
	                  
	                        },
	                        error:function(msg) {
	                        	$(".theme-loader").hide();
	                        	toastr["error"]("Unfortunately some Error occured.", "Upload Failed"); 
	                           }
	      }); 
	}
  
  function server_validation(array){
	 
	  array.forEach(function(item){
		 
		  var row=item.row;
		  var str=item.errorDetectors;	                    			
		  for(var i=0;i<str.length;i++){ 
			var errObj=str[i];
		
			for(let j=0;j<columnsName.length;j++){
			var column=columnsName[j].toLowerCase();
				if(column==errObj.fieldName.toLowerCase()){
					 $('#tblRowCell-'+row+"-"+j).addClass(errObj.errors[0]).closest('tr').addClass('tr-bg-ser');
				}
			} 
		  }
		  });
  }
  
  
  
	    
  $('body').on('click', '.delete-rowval',function() {
	  $target=$(this).closest('tr').css('background','red');
	  $target.fadeOut(400,function(){ $target.remove();
	  $('#total-row-count').text($("#exceltable tr").length-1);
		$('#error-row-count').text($(".tr-bg").length);
	  });
});
   function mremove_errors(){
	   $('.err-ddpri-sec').removeClass("err-ddpri-sec");
	   $('.err-notdate').removeClass("err-notdate");
	   $('.err-unique').removeClass("err-unique");
	   $('.err-empty').removeClass("err-empty");
	   $('.err-notnum').removeClass("err-notnum");
	   $('.err-notchar').removeClass("err-notchar");
	   $('.err-length').removeClass("err-length");
	   $('.err-ddact').removeClass("err-ddact");
	   $('.err-size100').removeClass("err-size100");
	   $('.err-size50').removeClass("err-size50");
	   $('.err-size25').removeClass("err-size25");
	   $('.tr-bg').removeClass("tr-bg");
   }
   function mremove_errors_ser(){
	   $('.err-unique-ser').removeClass("err-unique-ser");
	   $('.tr-bg-ser').removeClass("tr-bg-ser");
	   $('.err-sitenot-ser').removeClass("err-sitenot-ser");
	   $('.err-catnamenot-ser').removeClass("err-catnamenot-ser");
	   $('.err-equtnnot-ser').removeClass("err-equtnnot-ser");
	   $('.err-par-sta-ser').removeClass("err-par-sta-ser");
	   
   }
	    
   function val_unique(cid){
   	var flag=true;
   	var rows=$("#exceltable tr").length-2;
   	for(i=0;i<=rows;i++){
   		var v1=$.trim($("#tblRowCell-"+i+"-"+cid).text());
   		if(v1){
   			
   		for(j=i+1;j<=rows;j++){
	    		if(v1==$.trim($("#tblRowCell-"+j+"-"+cid).text())){
	    			$("#tblRowCell-"+j+"-"+cid).addClass("err-unique").closest("tr").addClass("tr-bg");
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-unique").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    	} 
   		}else{
   			$("#tblRowCell-"+i+"-"+cid).addClass("err-empty").closest("tr").addClass("tr-bg");
   			flag=false;
   		}
   	}
   	return flag;
   }
	    function val_unique50(cid){
	    	var flag=true;
	    	var rows=$("#exceltable tr").length-2;
	    	for(i=0;i<=rows;i++){
	    		var v1=$.trim($("#tblRowCell-"+i+"-"+cid).text());
	    		if(v1){
	    			if(v1.length>50){
	    				$("#tblRowCell-"+i+"-"+cid).addClass("err-size50").closest("tr").addClass("tr-bg");
		    			flag=false;
	    			}else{
	    		for(j=i+1;j<=rows;j++){
		    		if(v1==$.trim($("#tblRowCell-"+j+"-"+cid).text())){
		    			$("#tblRowCell-"+j+"-"+cid).addClass("err-unique").closest("tr").addClass("tr-bg");
		    			$("#tblRowCell-"+i+"-"+cid).addClass("err-unique").closest("tr").addClass("tr-bg");
		    			flag=false;
		    		}
		    	} }
	    		}else{
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-empty").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    	}
	    	return flag;
	    }
	    function val_character(cid){
	    	var flag=true;
	    	var rows=$("#exceltable tr").length-2;
	    	for(i=0;i<=rows;i++){
	    		var v=$.trim($("#tblRowCell-"+i+"-"+cid).text());
	    		if(v){
	    		if(!(/^[a-z]+$/i.test(v))){
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-notchar").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    		}else{
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-empty").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    	}
	    	return flag;
	    }
	    function val_date(cid){
	    	var flag=true;
	    	var rows=$("#exceltable tr").length-2;
	    	for(i=0;i<=rows;i++){
	    		var v=$.trim($("#tblRowCell-"+i+"-"+cid).text());
	    		if(v){
	    		if(!(/^\d{2}\-\d{2}\-\d{4}$/.test(v))){
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-notdate").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    		}else{
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-empty").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    	}
	    	return flag;
	    }
	    function val_number(cid){  
	    	var flag=true;
	    	var rows=$("#exceltable tr").length-2;
	    	for(i=0;i<=rows;i++){
	    		var v=$.trim($("#tblRowCell-"+i+"-"+cid).text());
	    		if(v){
	    		if(!(/^\d+$/.test(v))){
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-notnum").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    		}else{
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-empty").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    	}
	    	return flag;
	    }
	    function val_length(cid){
	    	var flag=true;
	    	var rows=$("#exceltable tr").length-2;
	    	for(i=0;i<=rows;i++){
	    		var v=$.trim($("#tblRowCell-"+i+"-"+cid).text());
	    		if(v){
	    		if(v.length<=9){
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-length").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    		}else{
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-empty").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    	}
	    	return flag;
	    }
	    
	    function val_ddact(cid){
	    	var flag=true;
	    	var rows=$("#exceltable tr").length-2;
	    	for(i=0;i<=rows;i++){
	    		var v=$.trim($("#tblRowCell-"+i+"-"+cid).text());
	    		if(v){
	    		if(v.toLowerCase()!="active" && v.toLowerCase()!="inactive"){
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-ddact").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    		}else{
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-empty").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    	}
	    	return flag;
	    }
	    
	    function val_pri_sec(cid){
	    	var flag=true;
	    	var rows=$("#exceltable tr").length-2;
	    	for(i=0;i<=rows;i++){
	    		var v=$.trim($("#tblRowCell-"+i+"-"+cid).text());
	    		if(v){
	    		if(v.toLowerCase()!="primary" && v.toLowerCase()!="secondary"){
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-ddpri-sec").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    		}else{
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-empty").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    	}
	    	return flag;
	    }
	    function val_empty(cid){
	    	var flag=true;
	    	var rows=$("#exceltable tr").length-2;
	    	for(i=0;i<=rows;i++){
	    		var v=$.trim($("#tblRowCell-"+i+"-"+cid).text());
	    		if(!(v)){
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-empty").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    	}
	    	return flag;
	    }
	   /* --------------------- size only check ----------------*/
	    function val_size100(cid){
	    	var flag=true;
	    	var rows=$("#exceltable tr").length-2;
	    	for(i=0;i<=rows;i++){
	    		var v=$.trim($("#tblRowCell-"+i+"-"+cid).text());
	    		if(v){
	    			if(v.length>100){
	    				$("#tblRowCell-"+i+"-"+cid).addClass("err-size100").closest("tr").addClass("tr-bg");
		    			flag=false;
	    			}
	    		}
	    	}
	    	return flag;
	    }
	    function val_size50(cid){
	    	var flag=true;
	    	var rows=$("#exceltable tr").length-2;
	    	for(i=0;i<=rows;i++){
	    		var v=$.trim($("#tblRowCell-"+i+"-"+cid).text());
	    		if(v){
	    			if(v.length>50){
	    				$("#tblRowCell-"+i+"-"+cid).addClass("err-size50").closest("tr").addClass("tr-bg");
		    			flag=false;
	    			}
	    		}
	    	}
	    	return flag;
	    }
	    function val_size25(cid){
	    	var flag=true;
	    	var rows=$("#exceltable tr").length-2;
	    	for(i=0;i<=rows;i++){
	    		var v=$.trim($("#tblRowCell-"+i+"-"+cid).text());
	    		if(v){
	    			if(v.length>25){
	    				$("#tblRowCell-"+i+"-"+cid).addClass("err-size25").closest("tr").addClass("tr-bg");
		    			flag=false;
	    			}
	    		}
	    	}
	    	return flag;
	    }
	    
	    /* --------------------- empty and size check ----------------*/
	    function val_empty100(cid){
	    	var flag=true;
	    	var rows=$("#exceltable tr").length-2;
	    	for(i=0;i<=rows;i++){
	    		var v=$.trim($("#tblRowCell-"+i+"-"+cid).text());
	    		if(v){
	    			if(v.length>100){
	    				$("#tblRowCell-"+i+"-"+cid).addClass("err-size100").closest("tr").addClass("tr-bg");
		    			flag=false;
	    			}
	    		}else{
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-empty").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    	}
	    	return flag;
	    }
	    function val_empty50(cid){
	    	var flag=true;
	    	var rows=$("#exceltable tr").length-2;
	    	for(i=0;i<=rows;i++){
	    		var v=$.trim($("#tblRowCell-"+i+"-"+cid).text());
	    		if(v){
	    			if(v.length>50){
	    				$("#tblRowCell-"+i+"-"+cid).addClass("err-size50").closest("tr").addClass("tr-bg");
		    			flag=false;
	    			}
	    		}else{
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-empty").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    	}
	    	return flag;
	    }
	    function val_empty25(cid){
	    	var flag=true;
	    	var rows=$("#exceltable tr").length-2;
	    	for(i=0;i<=rows;i++){
	    		var v=$.trim($("#tblRowCell-"+i+"-"+cid).text());
	    		if(v){
	    			if(v.length>25){
	    				$("#tblRowCell-"+i+"-"+cid).addClass("err-size25").closest("tr").addClass("tr-bg");
		    			flag=false;
	    			}
	    		}else{
	    			$("#tblRowCell-"+i+"-"+cid).addClass("err-empty").closest("tr").addClass("tr-bg");
	    			flag=false;
	    		}
	    	}
	    	return flag;
	    }
	    
	    function validate_Equipment(){
	     	var a=val_empty(0);
	    	var b=val_empty(1);
	    	var c=val_empty(2);
	    	var d=val_unique50(3);
	    	
	    	var e=val_size100(4);
	    	var f=val_size50(5);
	    	
	    	var g=val_empty50(6);
	    	var h=val_empty50(7);
	    	var i=val_number(8);
	    	
	    	var j=val_ddact(9);
	    	var k=val_date(10);
	    	var l=val_date(11);
	    	var m=val_pri_sec(12);
	    	
	    	var n=val_size25(13);
	    	var o=val_size25(14);
	    	var p=val_size25(15);
	    	
	  	var val= a && b && c && d && e && f && g && h && i && j && k && l && m && n && o && p;
	  	/* console.log("values :"+a+b+c+d+f+g);  */
	  	return val;
	    }
	    
	    function validate_Parameters(){
	    	if($("#par-dropdown").val()){
	    		
	    		return true;
	    	}
	    	 toastr["error"]("Select Datalogger Version."); 
	  	return false;
	    }
	    
  function validate_mtable(){
	  if(sel_file_type){
	var ret=window["validate_"+sel_file_type]();
	$('#total-row-count').text($("#exceltable tr").length-1);
	$('#error-row-count').text($(".tr-bg").length);
	console.log("ret :"+ret);
	if(ret){
		$('#upload_toserver').prop('disabled',false);	
	   }else{
		   $('#upload_toserver').prop('disabled',true);
	   }
	return ret;
	  }
	  return false;
  }
  
  $('#excelfile').change(function(){
	  if($('#excelfile').val()){
		  sel_file_type=$('#dropdown').val();
		  ExportToTable();
	  }
  });
  
  function upload_toserver(){
	  if(validate_mtable()){
		  mremove_errors_ser();
		  exp_json();
	  }else{
		  alert("Failed to Upload");
	  }
  }
  
  function post_equipment(){
	  var cid=9;
	  var rows=$("#exceltable tr").length-1;
	  for(i=0;i<=rows;i++){
  		$("#tblRowCell-"+i+"-"+cid).addClass("for-dd");
  	}
  }
  
  function post_equipment2(){
	  var cid=12;
	  var rows=$("#exceltable tr").length-1;
	  for(i=0;i<=rows;i++){
  		$("#tblRowCell-"+i+"-"+cid).addClass("for-ddpri-sec");
  	}
  }
  
  $('#exceltable').on('click','.for-dd',function(){
	  if(!($(this).find('select').length)){
		  $(this).attr("val",$(this).text());
		  $(this).empty();
		  var temp=$('#template-dropdown').clone();
		  temp.css('display','block');
	      $(this).append(temp);
	  }
	  });
  
  $('#exceltable').on('blur','.for-dd',function(eventObject){
	  console.log(eventObject);
	  console.log("fout");
	  if (!(eventObject.relatedTarget.id === "template-dropdown")){
	 
	var v=$(this).find('select').val();
	$(this).find('select').remove();
	if(v){
		$(this).text(v);
	}else{
		$(this).text($(this).attr('val'));
	}
	  }
	  });
  
  $('#exceltable').on('click','.for-ddpri-sec',function(){
	  if(!($(this).find('select').length)){
		  $(this).attr("val",$(this).text());
		  $(this).empty();
		  var temp=$('#template-dropdown-pri-sec').clone();
		  temp.css('display','block');
	      $(this).append(temp);
	  }
	  });
  
  $('#exceltable').on('blur','.for-ddpri-sec',function(eventObject){
	  console.log(eventObject);
	  console.log("fout");
	  if (!(eventObject.relatedTarget.id === "template-dropdown-pri-sec")){
	 
	var v=$(this).find('select').val();
	$(this).find('select').remove();
	if(v){
		$(this).text(v);
	}else{
		$(this).text($(this).attr('val'));
	}
	  }
	  });
	  	   
  $('#exceltable').on('blur','td',function(){
	  mremove_errors();
	  validate_mtable();
	  });
	  	   
  function pre_json_export(json){
	  if(sel_file_type=="Equipment"){
	  $.each(json, function (index, value) {
	        console.log(value);
	       if(json[index]['activeFlag']=='Active'){
	    	   json[index]['activeFlag']=1;
	       }else if(json[index]['activeFlag']=='Inactive'){
	    	   json[index]['activeFlag']=0;
	       }
	       if(json[index]['installationDate']){
	    	 var datearray=  json[index]['installationDate'].split("-");
	    	  
	    	 json[index]['installationDate']=datearray[2]+"-"+datearray[1]+"-"+datearray[0];
	    	   
	       }
	       if(json[index]['warrantyDate']){
	    	   
	    	   var wardatearray=  json[index]['warrantyDate'].split("-");
		    	  
		        json[index]['warrantyDate']=wardatearray[2]+"-"+wardatearray[1]+"-"+wardatearray[0];
		
	       }
	    });
	  }else if(sel_file_type=="Parameters"){
		  var dataloggerId=$("#par-dropdown").val();
		  $.each(json,function(index,value){
			  json[index]['dataloggerId']=dataloggerId;
			  json[index]['activeFlag']=1;
			  if(!(json[index]['coefficient'])){
				json[index]['coefficient']=1;
				  �������������������� � }
		  });
	  }
  }
  
  jQuery.fn.shift = [].shift;
function exp_json() {
	  var $rows = $('#exceltable').find('tr:not(:hidden)');
	  var headers = [];
	  var data = [];
	  var data = [];
	//  data.push({'table':sel_file_type});
	  // Get the headers (add special header logic here)
	  $($rows.shift()).find('th:not(:contains("Delete"))').each(function () {
	    headers.push($(this).text());
	  });
	  
	  // Turn all existing rows into a loopable array
	  $rows.each(function (index){
	    var $td = $(this).find('td');
	    var h = {};
	    
	    // Use the headers from earlier to name our hash keys
	    headers.forEach(function (header, i) {
	    	if(i==0){
	    		 h['row'] =index;
	    	}
	      h[header] = $td.eq(i).text();   
	    });
	    
	    data.push(h);
	  });
	  // Output the result
	 // $EXPORT.text(JSON.stringify(data));
	 pre_json_export(data);
	 console.log(JSON.stringify(data));
	 
	 var masterUploadRequest={};
	 if(sel_file_type=="Equipment"){
		 masterUploadRequest["equipment"]=data;
	 }else{
		 masterUploadRequest["integratedStandards"]=data;
	 }
	 
	 ServerCallForSaveOrUpdate(JSON.stringify(masterUploadRequest));   
	}
	
</script>

</body>
<script type="text/javascript">

function getEquipmentAgainstSite() {
    var siteId = $('#ddlSite').val();
    $.ajax({
          type : 'GET',
          url : './equipmentsBysites?siteId=' + siteId,
          success : function(msg) {
       	   
       	   var EquipmentList = msg.equipmentlist;
                        for (i = 0; i < EquipmentList.length; i++) {
                               $('#ddlEquipment').append(
                                             "<option value="+EquipmentList[i].equipmentId+">" + EquipmentList[i].customerNaming
                                                          + "</option>");
                        }
          },
          error : function(msg) {
                 console.log(msg);
          }
    });
}

</script>
</html>

