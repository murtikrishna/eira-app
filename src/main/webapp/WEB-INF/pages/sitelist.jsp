<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="true" %>


<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
      <meta charset="utf-8"/>
      <title>:: Welcome To EIRA ::</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
      <link rel="icon" type="image/x-icon" href="favicon.ico"/>
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-touch-fullscreen" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="default">
      <meta content name="description"/>
      <meta content name="author"/>

      
      <link href="resources/css/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/select2.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/pages-icons.css" rel="stylesheet" type="text/css">
      <link href="resources/css/animate.css" rel="stylesheet"  />
      <link href="resources/css/pages.css" class="main-stylesheet"  rel="stylesheet" type="text/css"/>
      <link href="resources/css/styles.css" rel="stylesheet" type="text/css">
      <link href="resources/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
      <link href="resources/css/site.css" rel="stylesheet" >
      <link href="resources/css/semantic.min.css" rel="stylesheet" type="text/css">
      <link href="resources/css/calendar.min.css" rel="stylesheet" type="text/css">
      <link href="resources/css/bootstrap.min.3.37.css" type="text/css" rel="stylesheet">
      <link href="resources/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="resources/css/sol.css">
      <link rel="stylesheet" type="text/css" href="resources/css/semantic.min.css">
      <script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
      <script src="resources/js/searchselect.js" type="text/javascript"></script>
      
      
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>
  
    
       <script type="text/javascript">
      $(document).ready(function(){
    	  
    	  var classstate = $('#hdnstate').val();
    	  $("#visulaizationid").addClass(classstate);
			$("#siteviewid").addClass(classstate);
    
    	    $(".theme-loader").animate({
    	        opacity: "0"
    	    },70000);
    	    setTimeout(function() {
    	        $(".theme-loader").remove();
    	    }, 800);

    	  
    	$('#ddlSite').dropdown('clear');
      	$('#ddlType').dropdown('clear');
      	$('#ddlCategory').dropdown('clear');
      	$('#ddlPriority').dropdown('clear');
      	$('#txtSubject').val('');
      	$('#txtTktdescription').val('');
      	

          $('.close').click(function(){
              $('#tktCreation').hide();
              $('.clear').click();
              $('.category').dropdown();
              $('.SiteNames').dropdown();
          });
    	  
          $('#btnCreate').click(function() {
      		if($('#ddlCategory').val== "") {
      	    	alert('Val Empty');
      	        $('.category ').addClass('error')
      	    }
      	})
      	
    	  
			$('.clear').click(function(){
		      	$('.search').val("");
		      });

			$('#ddlType').change(function(){              
                $('.category').dropdown('clear')
               });

			$('body').click(function(){
			          $('#builder').removeClass('open'); 
			        });
			
			$("#myInputs").on("keyup", function() {
			    var value = $(this).val().toLowerCase();
			    $("#myList li").filter(function() {
			      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			    });
			  });
			
			var validation  = {
		             txtSubject: {
		                        identifier: 'txtSubject',
		                        rules: [{
		                          type: 'empty',
		                          prompt: 'Please enter a value'
		                        },
		                        {
		                            type: 'maxLength[50]',
		                            prompt: 'Please enter a value'
		                          }]
		                      },
		                       ddlSite: {
		                        identifier: 'ddlSite',
		                        rules: [{
		                          type: 'empty',
		                          prompt: 'Please enter a value'
		                        }]
		                      },
		                     ddlType: {
		                           identifier: 'ddlType',
		                        rules: [{
		                          type: 'empty',
		                          prompt: 'Please enter a value'
		                        }]
		                     },
		                     ddlCategory: {
		                        identifier: 'ddlCategory',
		                        rules: [{
		                          type: 'empty',
		                          prompt: 'Please enter a value'
		                        }]
		                     },
		                      ddlPriority: {
		                        identifier: 'ddlPriority',
		                        rules: [{
		                          type: 'empty',
		                          prompt: 'Please select a dropdown value'
		                        }]
		                      },
		                      description: {
		                        identifier: 'description',
		                        rules: [{
		                          type: 'empty',
		                          prompt: 'Please Enter Description'
		                        }]
		                      }
		      };
		        var settings = {
		          onFailure:function(){
		              return false;
		            }, 
		          onSuccess:function(){    
		            $('#btnCreate').hide();
		            $('#btnCreateDummy').show()
		            //$('#btnReset').attr("disabled", "disabled");          
		            }};           
		        $('.ui.form.validation-form').form(validation,settings);
		 });
    </script>
     <script type='text/javascript'>//<![CDATA[
         $(window).load(function(){
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
         
         
         $('.ui.dropdown').dropdown({
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: true, 
             showOnFocus: true,
             on: "click" 
           });
    
		$(".oveallsearch").dropdown({
			onChange: function (value, text, $selectedItem) {                
                var uid =  $('#hdneampmuserid').val();
                redirectbysearchvalue(value,uid);
             },
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: false, 
             showOnFocus: true,
             on: "click" 
		})
		
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );//]]> 

  })
         
      </script>
      
      <script type="text/javascript">
         $(document).ready(function() {         
            $(".bars").click(function() {
               $(".sidebar-menu").invisible();
            })
             if($(window).width() < 767)
			{
			   $('.card').removeClass("slide-left");
			   $('.card').removeClass("slide-right");
			   $('.card').removeClass("slide-top");
			   $('.card').removeClass("slide-bottom");
			}
          
            $('.dataTables_filter input[type="search"]').attr('placeholder','search');
         } );
      </script>
  
   <style type="text/css">
  
/* .ui.form textarea:not([rows]) {
    height: 8em;
    min-height: 8em;
    max-height: 24em;
} */
   .input-sm, .form-horizontal .form-group-sm .form-control {
    font-size: 13px;
    min-height: 25px;
    height: 32px;
    padding: 8px 9px;
}  
    .page-container .page-content-wrapper .content {
    z-index: 10;
    /* padding-top: 33px !important; */
    /* padding-bottom: 69px; */
    min-height: 100%;
    -webkit-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
    .sitemap {
    height: 130px;
    width: 100%;
}
    .hoverpart {
		  position: relative;
		  /*margin-top: 50px;*/
		  width: 100%;
		  z-index:1;
		  height: 167px;
		  margin-bottom: 0px;   
}
.data-set {
    background: #F5F5F5;
    height: 130px;
    border-radius: 5px;
    margin: 5px;
}
.data-set-two {
	 background: #F5F5F5;
    height: 130px;
    border-radius: 5px;
    margin-top: 5px;
        margin-left: 5px;
     margin-bottom: 5px;
   
}

.overlay {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 180px;
  background: rgba(0, 0, 0, 0);
  transition: background 0.5s ease;
}

.hoverpart:hover .overlay {
  display: block;
  background: rgba(0, 0, 0, .3);
/*  z-index: 99; */
}

.hoverpart:hover .title {
  top: 90px;
}
.ui.form textarea:not([rows]) {
    height: 4em;
    min-height: 4em;
    max-height: 24em;
}

.button {
  position: absolute;
  width: 100%;
  left:0;
  top: 70px;
  text-align: center;
  opacity: 0;
  transition: opacity .35s ease;
}

.button a {
  width: 100%;
  padding: 12px 48px;
  text-align: center;
  color: #FFF;
  border: solid 2px white;
     background-color: #337ab7;
  z-index: 1;
  border-radius: 5px;
}

.hoverpart:hover .button {
  opacity: 1;
 /*  z-index: 100; */
  text-decoration: none !important;
}
.data {
   border:1px solid #CCC;
   border-radius: 5px;
   height: 143px;
       margin: 0px;
       padding-left:5px;
       padding-right:0px;
}

</style>


       
    
  </head>
  <body  class="fixed-header">
  
  <input type="hidden" value="${access.userID}" id="hdneampmuserid">
  
     <input type="hidden" value="${classtate}" id="hdnstate">

	<jsp:include page="slider.jsp"/>   
  
  
     <div class="page-container">
         <div class="header">
            <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="sidebar">
            </a>
            <div>
               <div class="brand inline">
               <a href="./dashboard"><img src="resources/img/logo01.png" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management" class="logos"></a>
                 <!--  <img src="resources/img/logo.png" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management" class="logos"> -->
               </div>
            </div>
            	<div class="d-flex align-items-center">
            		<!-- <div class="ui selection dropdown select-language">
					    <input name="language" type="hidden" value="fr-FR">
					    <div class="text">French</div>
					    <i class="dropdown icon"></i>
					    <div class="menu ui transition hidden">
					        <div class="item" data-value="en-US">English</div>
					        <div class="item active" data-value="fr-FR">French</div>
					    </div>
					</div> -->
                       <div class="form-group required field searchbx">
                       	<div class="ui  search selection  dropdown  width oveallsearch select-language">
                        	<input id="myInput" name="tags" type="text">
                                <div class="default text">search</div>
                                   <div id="myDropdown" class="menu">
                                   		<jsp:include page="searchselect.jsp" />	
                                         </div>
                                  </div>
                        	</div> 
                         <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <!-- <a href="./dashboard"><img src="resources/img/logo.png" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management" class="logos"></a> -->
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user_01.png" width="32" height="32">
                  </span>
                  
                   <c:if test="${not empty access}">         
                              <p class="user-login">${access.userName}</p>
                  </c:if>
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="logout1" class="dropdown-item color logout"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
                 
          				  <div class="newticketpopup hidden">
               		<p class="center mb-1 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img src="resources/img/tkt.png"></p>
               		<p class="create-tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false">New Ticket</p>
               </div>
                  
            </div>
         </div>
         <div class="page-content-wrapper ">
            <div class="content ">
               
               <div class="container-fixed-lg">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="./dashboard"><i class="fa fa-home mt-7"></i></a></li>
                     <li class="breadcrumb-item"><a>Visualization</a></li>
                     <li class="breadcrumb-item active">Site View</li>
                  </ol>
               </div>
               
            
       
         
         <div class=" container-fluid   container-fixed-lg padd-0" id="QuickLinkWrapper1">
                  <div class="card card-transparent padd-3">                     
                     <!-- <div class="card-header p-l-0">
                          <div class="col-md-9 card-title pull-left">SITE LIST  </div>
                           <div class="col-md-2"><input type="text" class="form-control search-game" placeholder="Search..."> </div>
                          </div> -->

					 <div class="card card-default bg-default" id="sitelist" data-pages="card">
                          <div class="card-header min-hei-20">
                         	<div class="col-md-10 col-xs-6 padd-0">
                         		 <div class="card-title pull-left tbl-head">Site List </div>
                         	</div>
                          <div class="col-md-2 col-xs-6">
                          	 <input class="form-control" id="myInputs" type="text" placeholder="Search..">
                          	<!--  <input type="text" class="form-control search-game pull-right sitsearch" placeholder="search"> -->
                          </div>
                          
                          </div>
                          <div class="row containerItems padd-20 p-t-0" >
                      
                      
                      <c:if test="${not empty sitelist}">      			
         			 	<!--<input type='hidden' id='ChartData' value='[{"sites":"day-6","totalenergy":6.02,"color": "#5B835B"},{"sites":"day-5","totalenergy":8.29,"color": "#5B835B"},{"sites":"day-4","totalenergy":10.10,"color": "#5B835B"},{"sites":"day-3","totalenergy":12.03,"color": "#5B835B"},{"sites":"day-2","totalenergy":13.66,"color": "#5B835B"},{"sites":"day-1","totalenergy":0.00,"color": "#5B835B"}]||[{"sites":"day-6","totalenergy":3.17,"color": "#5B835B"},{"sites":"day-5","totalenergy":4.37,"color": "#5B835B"},{"sites":"day-4","totalenergy":5.33,"color": "#5B835B"},{"sites":"day-3","totalenergy":6.34,"color": "#5B835B"},{"sites":"day-2","totalenergy":7.20,"color": "#5B835B"},{"sites":"day-1","totalenergy":0.00,"color": "#5B835B"}]||[{"sites":"day-6","totalenergy":3.00,"color": "#5B835B"},{"sites":"day-5","totalenergy":4.13,"color": "#5B835B"},{"sites":"day-4","totalenergy":5.04,"color": "#5B835B"},{"sites":"day-3","totalenergy":6.00,"color": "#5B835B"},{"sites":"day-2","totalenergy":6.81,"color": "#5B835B"},{"sites":"day-1","totalenergy":0.00,"color": "#5B835B"}]||[{"sites":"day-6","totalenergy":2.87,"color": "#5B835B"},{"sites":"day-5","totalenergy":3.96,"color": "#5B835B"},{"sites":"day-4","totalenergy":4.83,"color": "#5B835B"},{"sites":"day-3","totalenergy":5.75,"color": "#5B835B"},{"sites":"day-2","totalenergy":6.53,"color": "#5B835B"},{"sites":"day-1","totalenergy":0.00,"color": "#5B835B"}]||[{"sites":"day-6","totalenergy":3.72,"color": "#5B835B"},{"sites":"day-5","totalenergy":5.05,"color": "#5B835B"},{"sites":"day-4","totalenergy":6.16,"color": "#5B835B"},{"sites":"day-3","totalenergy":7.31,"color": "#5B835B"},{"sites":"day-2","totalenergy":8.32,"color": "#5B835B"},{"sites":"day-1","totalenergy":0.00,"color": "#5B835B"}]'> -->
         				<%--<input type='hidden' id='ChartData' value='${chart.chartData}'>--%>                  
                     	<!--<input type='hidden' id='ChartData' value='[{"sites":"day-6","totalenergy":6.02 MWh,"color": "#5B835B"},{"sites":"day-5","totalenergy":8.29 MWh,"color": "#5B835B"},{"sites":"day-4","totalenergy":10.10 MWh,"color": "#5B835B"},{"sites":"day-3","totalenergy":12.03 MWh,"color": "#5B835B"},{"sites":"day-2","totalenergy":13.66 MWh,"color": "#5B835B"},{"sites":"day-1","totalenergy":0.00 MWh,"color": "#5B835B"}]||[{"sites":"day-6","totalenergy":3.17 MWh,"color": "#5B835B"},{"sites":"day-5","totalenergy":4.37 MWh,"color": "#5B835B"},{"sites":"day-4","totalenergy":5.33 MWh,"color": "#5B835B"},{"sites":"day-3","totalenergy":6.34 MWh,"color": "#5B835B"},{"sites":"day-2","totalenergy":7.20 MWh,"color": "#5B835B"},{"sites":"day-1","totalenergy":0.00 MWh,"color": "#5B835B"}]||[{"sites":"day-6","totalenergy":3.00 MWh,"color": "#5B835B"},{"sites":"day-5","totalenergy":4.13 MWh,"color": "#5B835B"},{"sites":"day-4","totalenergy":5.04 MWh,"color": "#5B835B"},{"sites":"day-3","totalenergy":6.00 MWh,"color": "#5B835B"},{"sites":"day-2","totalenergy":6.81 MWh,"color": "#5B835B"},{"sites":"day-1","totalenergy":0.00 MWh,"color": "#5B835B"}]||[{"sites":"day-6","totalenergy":2.87 MWh,"color": "#5B835B"},{"sites":"day-5","totalenergy":3.96 MWh,"color": "#5B835B"},{"sites":"day-4","totalenergy":4.83 MWh,"color": "#5B835B"},{"sites":"day-3","totalenergy":5.75 MWh,"color": "#5B835B"},{"sites":"day-2","totalenergy":6.53 MWh,"color": "#5B835B"},{"sites":"day-1","totalenergy":0.00 MWh,"color": "#5B835B"}]||[{"sites":"day-6","totalenergy":3.72 MWh,"color": "#5B835B"},{"sites":"day-5","totalenergy":5.05 MWh,"color": "#5B835B"},{"sites":"day-4","totalenergy":6.16 MWh,"color": "#5B835B"},{"sites":"day-3","totalenergy":7.31 MWh,"color": "#5B835B"},{"sites":"day-2","totalenergy":8.32 MWh,"color": "#5B835B"},{"sites":"day-1","totalenergy":0.00 MWh,"color": "#5B835B"}]'> -->  
         			 	<!--  <input type='hidden' id='ChartData' value='[{"sites":"day-1","totalenergy":100,"color": "#5B835B"},{"sites":"day-2","totalenergy":120,"color": "#5B835B"},{"sites":"day-3","totalenergy":115,"color": "#5B835B"},{"sites":"day-4","totalenergy":110,"color": "#5B835B"},{"sites":"day-5","totalenergy":90,"color": "#5B835B"},{"sites":"day-6","totalenergy":103,"color": "#5B835B"}]||[{"sites":"day-1","totalenergy":110,"color": "#5B835B"},{"sites":"day-2","totalenergy":110,"color": "#5B835B"},{"sites":"day-3","totalenergy":125,"color": "#5B835B"},{"sites":"day-4","totalenergy":120,"color": "#5B835B"},{"sites":"day-5","totalenergy":95,"color": "#5B835B"},{"sites":"day-6","totalenergy":102,"color": "#5B835B"}]||[{"sites":"day-1","totalenergy":120,"color": "#5B835B"},{"sites":"day-2","totalenergy":110,"color": "#5B835B"},{"sites":"day-3","totalenergy":105,"color": "#5B835B"},{"sites":"day-4","totalenergy":110,"color": "#5B835B"},{"sites":"day-5","totalenergy":94,"color": "#5B835B"},{"sites":"day-6","totalenergy":101,"color": "#5B835B"}]'> -->      
    					<c:forEach items="${sitelist}" var="customers">
        					<ul class="list-group col-md-12" id="myList">
    <li class="list-group-item">
     <div data-search="${customers.dataSearch}" class="col-12 col-sm-6 col-md-12  hoverpart padd-0">
                                <p class="sitename">${customers.siteRef} - ${customers.siteName} </p>
                       
                                  <div class="col-md-12 data">                       
                                    <div class="col-md-6 col-lg-3 firstCol padd-0">                                      
                                      <iframe src="${customers.siteLocation}" width="100%" class="siteMap" frameborder="0" style="margin-top: 5px;" allowfullscreen></iframe>                              
                                    </div>
                                  <div class="col-md-6 col-lg-4 secondCol padd-0">
                                   <div class="data-set-two">
                                     <div class="col-md-12 col-xs-12 mt-5">
                                        <div class="col-md-4 mt-5 padd-0">
                                           <p class="status">Today Energy</p>
                                        </div>
                                       <div class="col-md-1 mt-5">
                                           <p class="status">:</p>
                                        </div>
                                        <div class="col-md-6 mt-5">
                                           <p class="status"> ${customers.todayEnergy}</p>
                                        </div>
                                     </div>
                                     <div class="col-md-12 col-xs-12">
                                        <div class="col-md-4 mt-5 padd-0">
                                           <p class="status">Total Energy</p>
                                        </div>
                                       <div class="col-md-1 mt-5">
                                           <p class="status">:</p>
                                        </div>
                                        <div class="col-md-6 mt-5">
                                           <p class="status"> ${customers.totalEnergy}</p>
                                        </div>
                                     </div>
                                     <div class="col-md-12 col-xs-12">
                                        <div class="col-md-4 mt-5 padd-0">
                                           <p class="status">Last Update</p>
                                        </div>
                                        <div class="col-md-1 mt-5">
                                           <p class="status">:</p>
                                        </div>
                                        <div class="col-md-6 mt-5">
                                           <p class="status"> ${customers.energyLastUpdate}</p>
                                        </div>
                                     </div>
                                     <div class="col-md-12 col-xs-12">
                                        <div class="col-md-4 mt-5 padd-0">
                                           <p class="status">Total Inverters</p>
                                        </div>
                                       <div class="col-md-1 mt-5">
                                           <p class="status">:</p>
                                        </div>
                                        <div class="col-md-6 mt-5">
                                           <p class="status"> ${customers.totalEquipment}</p>
                                        </div>
                                     </div>
                                     
                                     <div class="col-md-12 col-xs-12">
                                        <div class="col-md-4 mt-5 padd-0">
                                           <p class="status">Capacity (kW)</p>
                                        </div>
                                       <div class="col-md-1 mt-5">
                                           <p class="status">:</p>
                                        </div>
                                        <div class="col-md-6 mt-5">
                                           <p class="status"> ${customers.installationCapacity}</p>
                                        </div>
                                     </div>

                                     <div class="row"></div>
                                     <div class="row"></div>
                                   </div>
                                  </div>
                                  <div class="col-md-6 col-lg-5 thirdCol padd-0">
                                    <div class="data-set">
                                        <div class="col-md-12 col-xs-12">
                                        	<div class="col-md-2  col-xs-4 mt-15 padd-0"><p class="status">Address</p></div>
                                        <div class="col-md-1 mt-15">
                                           <p class="status">:</p>
                                        </div> 
                                        <div class="col-md-7 col-xs-8 mt-15">
                                           <p class="status ml-10">${customers.siteAddress}</p>
                                        </div>
                                        </div>
                                        
                                        <div class="col-md-12 col-xs-12">
                                        
                                        <div class="col-md-2 col-xs-4 mt-15 padd-0"><p class="status">Mobile No</p></div>
                                        <div class="col-md-1 mt-15">
                                           <p class="status">:</p>
                                        </div>
                                        <div class="col-md-7 col-xs-8 mt-15">
                                           <p class="status"> ${customers.siteMobileNo}</p>
                                        </div>
                                         </div>    
                                         
                                         <div class="col-md-12 col-xs-12">
                                        
                                        <div class="col-md-2 col-xs-4 mt-15 padd-0"><p class="status">COD</p></div>
                                        <div class="col-md-1 mt-15">
                                           <p class="status">:</p>
                                        </div>
                                        <div class="col-md-7 col-xs-8 mt-15">
                                           <p class="status"> ${customers.commisiongDate}</p>
                                        </div>
                                         </div>  
                                         
                                                                          
                                         
                                   </div> 
                                   

                                  </div>
                                 </div>
                          
                          <div class="overlay"></div>
                          <div class="button"><a href="siteview${customers.siteID}"> View </a></div>
                        </div>
  
    </li>
  
  </ul>  	        			
        				</c:forEach>
        			  </c:if> 
        
                             
                    
                    
                    
                      </div>
                    </div>
                  </div>
               </div>
            </div>
            <div class="container-fixed-lg footer">
               <div class="container-fluid copyright sm-text-center">
                  <p class="small no-margin pull-left sm-pull-reset">
                     <span class="hint-text">Copyright &copy; 2017.</span>
                     <span>INSPIRE CLEAN ENERGY.</span>
                     <span class="hint-text">All rights reserved. </span>
                  </p>
                  <p class="small no-margin pull-rhs sm-pull-reset">
                     <span class="hint-text">Powered by</span>
                     <span>MESTECH SERVICES PVT LTD</span>.
                  </p>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
 
      <!-- Side Bar Content Start-->

      
    
      <!-- Side Bar Content End-->
     
    <!--   <div class="quickview-wrapper  builder hidden-sm hidden-xs" id="builder">
         <div class="p-l-10 p-r-10 ">
            <a class="builder-close quickview-toggle pg-close" href="#"></a>
            <a class="builder-toggle" data-toggle="quickview" data-toggle-element="#builder"><img src="resources/img/ImageResize_06.png"></a>
            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary" id="builderTabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tabLayouts" role="tab" aria-controls="home"><span>Quick Links</span></a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active " id="tabLayouts" role="tabcard">
                  <div class="scrollable">
                     <div class="p-r-50">
                        <div class="list">
                           <ul>
                              <li><a href="#QuickLinkWrapper1">Site List</a></li>
                              
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div> -->
      <script src="resources/js/pace.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.gotop.js"></script>
      <script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
      <script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
      <script src="resources/js/tether.min.js" type="text/javascript"></script>
      <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery-easy.js" type="text/javascript"></script>
      <script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.actual.min.js"></script>
      <script src="resources/js/jquery.scrollbar.min.js"></script>
      <script type="text/javascript" src="resources/js/select2.full.min.js"></script>
      <script type="text/javascript" src="resources/js/classie.js"></script>
      <script src="resources/js/switchery.min.js" type="text/javascript"></script>
      <script src="resources/js/pages.min.js"></script>
      <script src="resources/js/card.js" type="text/javascript"></script>
      <script src="resources/js/scripts.js" type="text/javascript"></script>
      <script src="resources/js/demo.js" type="text/javascript"></script>
      <script src="resources/js/jquery.easing.min.js"></script>
      <script src="resources/js/jquery.fadethis.js"></script>
      <script src="resources/js/semantic.min.js" type="text/javascript" ></script>
      
      
      <script src="resources/js/e-search.js"></script>
 
       
    <!-- <script src="resources/js/jquery.dataTables.min.js" type="text/javascript" ></script> 
     <script src="resources/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
      <script src="resources/js/semantic.min.js" type="text/javascript" ></script>
      <script src="resources/js/calendar.min.js" type="text/javascript" ></script>
     <script src="resources/js/jquery.selectlistactions.js"></script> 
     -->
    
    
    
    
    <div id="gotop"></div>
    
  
         <!-- Address Popup Start !-->
      
 

<!-- Modal -->
<div id="tktCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog modal-top">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">New Ticket</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"><span class="errormess">*</span> All fields are mandatory</p>
				 </div>            
        		<form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form validation-form"> 
             
             <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width SiteNames"
									id="ddlSite" name="ddlSite" path="siteID" onchange="getEquipmentAgainstSite()">
									<form:option value="" tabindex="0">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>
						
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                                                <div class="col-md-4 col-xs-12">
                                                       <label class="fields required m-t-10">Equipment</label>
                                                </div>

                                                <div class="col-md-8 col-xs-12">
                                                       <form:select
                                                              class="field ui fluid search selection dropdown width equipments"
                                                              id="ddlEquipment" path="equipmentID" name="ddlEquipment">
                                                              <option value="">Select</option>
                                                       </form:select>
                                                </div>
                          </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Type</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width" id="ddlType" name="ddlType"  path="ticketType">
                        <form:option value="">Select</form:option>
                        <form:option value="Operation">Operation</form:option>
                        <form:option value="Maintenance">Maintenance</form:option>
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Category</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width category" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                         <form:option data-value="Operation" value="">Select</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Preventive Maintenance">Preventive Maintenance</form:option>
										
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                          <form:option data-value="Maintenance" value="">Select</form:option>
                    </form:select> 
                  </div>
                  
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                  <div class="col-md-4 col-xs-12"><label class="fields required m-t-10">Subject</label></div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<form:input  placeholder="Subject"  autocomplete="off" name="Subject"  type="text"  id="txtSubject" path="ticketDetail" maxLength="50"/>
                  	</div>
                  	</div>
               </div> 
              
              <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Priority</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width" id="ddlPriority" name="user" path="priority">
                        <form:option value="">Select </form:option>
                        <form:option data-value="3" value="3">High</form:option>
                        <form:option data-value="2" value="2">Medium</form:option>
                        <form:option data-value="1" value="1">Low</form:option>
                    </form:select> 
                  </div>
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Description</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<textarea id="txtTktdescription"  autocomplete="off" name="description" style="resize: none;" placeholder="Ticket Description" maxLength="120"></textarea>
                  	</div>
                    
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
				       <div class="btn btn-success  submit" id="btnCreate">Create</div>
				       <button class="btn btn-success" type="button" id="btnCreateDummy" tabindex="7"  style="display:none;">Create</button>
				        <div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
				  </div>
  <!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
  <!-- <div class="ui error message"></div> -->
</form:form>
				<!-- <div class="ui error message"></div> -->
				
	  		</div>
	  </div>
    </div>
  </div>
<!-- Address Popup End !--> 
 
      <script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
      <script>
         $(document).ready(function() {
            $(window).fadeThis({
               speed: 500,
            });
         });
      </script>

  </body>
  <script type="text/javascript">
  function getEquipmentAgainstSite() {
	    var siteId = $('#ddlSite').val();
	   
	    $.ajax({
	          type : 'GET',
	          url : './equipmentsBysites?siteId=' + siteId,
	          success : function(msg) {
	       	   
	       	   var EquipmentList = msg.equipmentlist;
	                 
	                        for (i = 0; i < EquipmentList.length; i++) {
	                               $('#ddlEquipment').append(
	                                             "<option value="+EquipmentList[i].equipmentId+">" + EquipmentList[i].customerNaming
	                                                          + "</option>");
	                        }

	          },
	          error : function(msg) {
	                 console.log(msg);
	          }
	    });
	}

       </script>
</html>
