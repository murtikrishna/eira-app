
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet" type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet" type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet" rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet" type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet" type="text/css">
<link href="resources/css/bootstrap.min3.3.37.css" type="text/css" rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min3.css" type="text/css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="resources/css/jquery-ui.css">
<link href="resources/css/formValidation.min.css" rel="stylesheet" type="text/css">
<!--  <link href="resources/css/jquery-ui_cal.css" rel="stylesheet" type="text/css"> -->

<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>

<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script type="text/javascript" src="resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="resources/js/formValidation.min.js"></script>
<script type="text/javascript" src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.val.js"></script>


<script type="text/javascript" src="resources/js/toastr.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/toastr.css">
<link rel="stylesheet" type="text/css" href="http://codeseven.github.com/toastr/toastr.css">

<script type="text/javascript">  

var varEquipmentType=0;
function AjaxCallForStateID(TypeId,varEquipmentType) { 
	
	 $('#ddlState').empty();
 	
	  $('#ddlState').dropdown('clear');
	 
	  $('#ddlState').append('<option value="">Select</option>');

		
	  $.ajax({
         type: 'POST',
         url: './getstates',
         data: {'CountryID' : TypeId },
         success: function(msg){
       	  debugger;
       	     	 
       	  
       	  var States = msg.state;        	  
    	  for(var i=0;i< States.length; i++)
    	  {
    		 var StateId = States[i].stateId;
    		 var StateName = States[i].stateName;
    		 
    		 $('#ddlState').append('<option value="' + StateId + '">' + StateName + '</option>');
    	  }  
    	  
    	  if(varEquipmentType!=0){
    		 
    		  setTimeout(function(){  $("#ddlState").val(varEquipmentType).change(); }, 1000);
    		 
    	  }
       	
			 
	 },
         error:function(msg) { 
         	
         	//alert(0);
         	//alert(msg);
         	}
	}); 
}



var varCustomerType=0;
function AjaxCallForPrimaryID(TypeId,varCustomerType) { 
	
	 $('#ddlPrimaryCustomer').empty();
 	
	  $('#ddlPrimaryCustomer').dropdown('clear');
	 
	  $('#ddlPrimaryCustomer').append('<option value="">Select</option>');

		
	  $.ajax({
         type: 'POST',
         url: './getprimarycustomer',
         data: {'Group' : TypeId },
         success: function(msg){
       	  debugger;
       	     	 
       	  
       	  var customerlist = msg.customerlist;        	  
    	  for(var i=0;i< customerlist.length; i++)
    	  {
    		 var CustomerId = customerlist[i].customerId;
    		 var CustomerName = customerlist[i].customerName;
    		 
    		 $('#ddlPrimaryCustomer').append('<option value="' + CustomerId + '">' + CustomerName + '</option>');
    	  }  
    	  
    	  if(varCustomerType!=0){
    		 
    		  setTimeout(function(){  $("#ddlPrimaryCustomer").val(varCustomerType).change(); }, 1000);
    		 
    	  }
       	
			 
	 },
         error:function(msg) { 
         	
         	//alert(0);
         	//alert(msg);
         	}
	}); 
}


  function AjaxCallForPageLoad() {              
                  $.ajax({
                                    type: 'GET',
                                    url: './customerpageload',
                                    success: function(msg){
                                
                                    	
                                    	var status = msg.status;
                                    
                                    	var primarycustomer=msg.primarycustomerList;
                                    	var customertypelist=msg.customertypeList;
                                    	var activeStatusList = msg.activeStatusList;
                                    	var customerList = msg.customerList;
                                    	var customergroup = msg.customergroupList;
                                    	var countrylist = msg.countryList;
                                    	var statelist = msg.stateList;
                                    	
                                    	
                                    	 $.map(primarycustomer, function(val, key) {                                    		 
                                    		 $( "#ddlPrimaryCustomer" ).append("<option value="+ key +">" + val +"</option>");
                                    		});
                                    	 $.map(customertypelist, function(val, key) {                                    		 
                                    		 $( "#ddlCustomerType" ).append("<option value="+ key +">" + val +"</option>");
                                    		});
                                    	 $.map(customerList, function(val, key) {                                    		 
                                    		 $( "#ddlCustomer" ).append("<option value="+ key +">" + val +"</option>");
                                    		});
                                    	 $.map(customergroup, function(val, key) {                                    		 
                                    		 $( "#ddlCustomerGroup" ).append("<option value="+ key +">" + val +"</option>");
                                    		});
                                    	 $.map(countrylist, function(val, key) {                                    		 
                                    		 $( "#ddlCountry" ).append("<option value="+ key +">" + val +"</option>");
                                    		});
                                    	 $.map(statelist, function(val, key) {                                    		 
                                    		 $( "#ddlState" ).append("<option value="+ key +">" + val +"</option>");
                                    		});
                                    	
                                    	 $.map(activeStatusList, function(val, key) {                                    		 
                                    		 $( "#ddlCustomerStatus" ).append("<option value="+ key +">" + val +"</option>");
                                    		});
                                    	 
                                    	 
                                    	 
                                    	$('#hdncustomerlist').val(customerList);
                                    	
                                     	
                                    
                                    },
                                    error:function(msg) { 
                                    	//alert(0); alert(msg);
                                    }
                                                      });
  }
  
  
  
  
  
  function AjaxCallForLazyLoad() {              
      $.ajax({
                        type: 'GET',
                        url: './customergrid',
                        success: function(msg){
                                            	
                        	var status = msg.status;                        
                        	var responsedata=msg.responsedata;
                        	var customerList = msg.data;
                        	
                        	 
                        	for(var i=0; i < customerList.length; i++)
                        		{
                        			
                        			var activeFlag = customerList[i].activeFlag;
                        			if(activeFlag != null && activeFlag == 1)
                        				{
                        				activeFlag = "Active";
                        				}
                        			else {activeFlag = "In-active"; }
                        			
                        			var logdata = customerList[i].imagePath;
                        			
                        			var logo = "<img src='"+ logdata +"' width='"+ 120 +"' height='"+ 60 +"'>"
                        			
                        			//$('#example').DataTable();
                        			 $('#example').DataTable().row.add([customerList[i].customerCode,customerList[i].customerName,customerList[i].customerTypeName,customerList[i].customerOrganization,customerList[i].stateName,customerList[i].countryName,activeFlag,logo,'<a href="#QuickLinkWrapper1" class="editbtn btn btn-primary" id="btnedit" onclick="AjaxCallForFetch(' + customerList[i].customerId + ')">Edit</a>']).draw(false);
                             		
                        			
                        		}
                        	
                        	
                        
                        },
                        error:function(msg) { 
                        	//alert(0); alert(msg);
                        }
                                          });
}


  
  function AjaxCallForSaveOrUpdate() {                
      
	var varCustomerId = null; 
	var varCustomerCode = null; 
	var formData = new FormData();
	
	if($('#hdnCustomerId').val() != null && $('#hdnCustomerId').val() != "Auto-Generation" )  
		{
		varCustomerId = $('#hdnCustomerId').val();
		varCustomerCode = $('#lblCustomerCode').text();
		}
	
	 
	
      var CustomerData = [{
    		    	
    		        "customerId": varCustomerId,
    		        "customerCode": varCustomerCode,
    		        "customerTypeID": $("#ddlCustomerType option:selected").val(),
    		        "customerName": $('#txtCustomerName').val(),
    		        "customerOrganization": $('#txtCustomerOrganization').val(),
    		        "customerDescription": $('#txtCustomerDescription').val(),
    		        "customerGroup": $("#ddlCustomerGroup option:selected").val(),
    		        "primaryCustomer": $("#ddlPrimaryCustomer option:selected").val(),
    		        "address": $('#txtAddress').val(),
    		        "city": $('#txtCity').val(),
    		        "countryID": $("#ddlCountry option:selected").val(),
    		        "stateID": $("#ddlState option:selected").val(),
    		        "postalCode":$('#txtPostalCode').val(),
    		        
    		        "contactPerson":$('#txtContactPerson').val(),
    		        "emailID":$('#txtEmailId').val(),
    		        "mobile":$('#txtMobileNumber').val(),
    		        "telephone":$('#txtTelephoneNumber').val(),
    		        "fax":$('#txtFax').val(),
    		        "customerReference":$('#txtCustomerReference').val(),
    		        
    		       /*  "customerLogo":$('input[type=file]').val(''), */
    		        
    		        
    		        "website":$('#txtWebsite').val(),
    		        "customerPONumber":$('#txtPoNumber').val(),
    		        "customerPODateText":$('#txtPoDate').val(),
    		      	"activeFlag":  $("#ddlCustomerStatus option:selected").val(),
    		     	"creationDate":null,
     		        "createdBy":null,
     		        "lastUpdatedDate":null,
     		        "lastUpdatedBy":null
    		          
    		       
    		      
    		        
    		    
    		}];
      
      
      
      if(document.getElementById("excelfile").files[0] == undefined)
    	  {
    	  
    	  
    	  $.ajax({
              type: 'POST',
              contentType : 'application/json; charset=utf-8',
              dataType : 'json',
              url: './newcustomer',
              data: JSON.stringify(CustomerData),
              success: function(msg){

    			  var customer = msg.customer;
    			  var errormessage = msg.Errorlog1;
    		/* 	alert(errormessage); */
				
    			var varCustomerType = $('#ddlCustomerType option:selected').text();
				var varCountry = $("#ddlCountry option:selected").text();          				
				var varState = $("#ddlState option:selected").text();
				
				
				if(errormessage!=null && errormessage!="")
				{

  	    		toastr.error(errormessage);        					
				}
				else
					{
  			var activeFlag = customer.activeFlag;
  			if(activeFlag != null && activeFlag == 1)
  				{
  				activeFlag = "Active";
  				}
  			else {activeFlag = "In-active"; }
  			
  			  
              	if($('#hdnCustomerId').val() != null && $('#hdnCustomerId').val() != "Auto-Generation" )  
              	{
              		
              	}
              	else
              		{
              		
              		var logdata = customer.imagePath;
         			
              		var logo = "<img src='"+ logdata +"' width='"+ 120 +"' height='"+ 60 +"'>"
              		/* $('#example').DataTable().row.add([customer.customerCode,customer.customerName,varCustomerType,customer.customerOrganization,varState,varCountry,activeFlag,'<a  onclick="AjaxCallForFetch(' + customer.customerId + ')">Edit</a>','<a  href="removeequipment' + customer.customerId + '">De-activate</a>']).draw(false);
           			 */
              		$('#example').DataTable().row.add([customer.customerCode,customer.customerName,varCustomerType,customer.customerOrganization,varState,varCountry,activeFlag,logo,'<a  onclick="AjaxCallForFetch(' + customer.customerId + ')">Edit</a>']).draw(false);
               		
              		}
              	
              	
              	
              	$('#hdnCustomerId').val('Auto-Generation');
      	    	$('#lblCustomerCode').text('Auto-Generation');
      	    	  //toastr.info("Successfully Created");
      	    	  
      	    	  var btnText = $('#btnSubmit').text();              	    	
      	    	
      	    	if (btnText == 'Save') {
      	    		toastr.info("Successfully Created");
      	    	}else {
      	    		toastr.info("Successfully Updated");	
      	    	}
      	    	 $('.clear').click();
      	    	
					}
              	
              
              },
              error:function(msg) { 
              	toastr.error("There was a some error. Please Contect admin");
              	//alert(0); alert(msg);
              	}
}); 
    	  
    	  
    	  
    	  
    	  }
      else{
    	  
    	  
    	  var NAME = document.getElementById("excelfile").files[0].name;
          var Size = document.getElementById("excelfile").files[0].size;
         /*  alert(NAME);
          alert(Size); */
          if((NAME.endsWith(".png") || NAME.endsWith(".jpg") || NAME.endsWith(".svg")) && Size <=5000000)
        	  {
        	  
        	 /*  alert(document.getElementById("excelfile").files[0]);
        	     
        	     alert(JSON.stringify(CustomerData)); */
        	     
        	      formData.append("data",JSON.stringify(CustomerData));        
        	     formData.append("image", document.getElementById("excelfile").files[0]);
        	     
        	    
        	  /*    alert(document.getElementById("excelfile").files[0]);
        	      
        	     console.log(formData);
        	     alert(formData); */
        	     
        	  
        	  $.ajax({
            	  url : 'newcustomerwithimage',
            	 
                  cache: false,
                  processData : false,
                  contentType : false,
                 	data : formData,

                  type : 'POST',
                  
                                success: function(msg){
                                	
                                   

                      			   var customer = msg.customer;
                      			/*   alert(customer);  */
                      			  var errormessage = msg.Errorlog1;
                      		/* alert(errormessage); */ 
                  				
                      			var varCustomerType = $('#ddlCustomerType option:selected').text();
                  				var varCountry = $("#ddlCountry option:selected").text();          				
                  				var varState = $("#ddlState option:selected").text();
                  				
                  				
                  				if(errormessage!=null && errormessage!="")
                  				{

                    	    		toastr.error(errormessage);        					
                  				}
                  				else
                  					{
                    			var activeFlag = customer.activeFlag;
                    			if(activeFlag != null && activeFlag == 1)
                    				{
                    				activeFlag = "Active";
                    				}
                    			else {activeFlag = "In-active"; }
                    			
                    			  
                                	if($('#hdnCustomerId').val() != null && $('#hdnCustomerId').val() != "Auto-Generation" )  
                                	{
                                		
                                	}
                                	else
                                		{
                                		/* $('#example').DataTable().row.add([customer.customerCode,customer.customerName,varCustomerType,customer.customerOrganization,varState,varCountry,activeFlag,'<a  onclick="AjaxCallForFetch(' + customer.customerId + ')">Edit</a>','<a  href="removeequipment' + customer.customerId + '">De-activate</a>']).draw(false);
                             			 */
                             			 
                             			var logdata = customer.imagePath;
                             			
                             			var logo = "<img src='"+ logdata +"' width='"+ 120 +"' height='"+ 60 +"'>"
                             			
                                		$('#example').DataTable().row.add([customer.customerCode,customer.customerName,varCustomerType,customer.customerOrganization,varState,varCountry,activeFlag,logo,'<a  onclick="AjaxCallForFetch(' + customer.customerId + ')">Edit</a>']).draw(false);
                                 	 
                                		}
                                	
                                	
                                	
                                	$('#hdnCustomerId').val('Auto-Generation');
                        	    	$('#lblCustomerCode').text('Auto-Generation');
                        	    	  //toastr.info("Successfully Created");
                        	    	  
                        	    	  var btnText = $('#btnSubmit').text();              	    	
                        	    	
                        	    	if (btnText == 'Save') {
                        	    		toastr.info("Successfully Created");
                        	    	}else {
                        	    		toastr.info("Successfully Updated");	
                        	    	}
                        	    	 $('.clear').click();
                        	    	
                  					}
                                	
                                
                                },
                                error:function(msg,error,data) {
                                	
                                	/* debugger;
                                	alert(error); */
                                	toastr.error("There was a some error. Please Contect admin");
                                	//alert(0); alert(msg);
                                	} 
              }); 
        	  }
          else if((NAME.endsWith(".png") || NAME.endsWith(".jpg") || NAME.endsWith(".svg")) && Size >=5000000)
    	  {
        	  toastr.error("Image size grater than 5 MB");  
    	  }
         
          else{
        	  toastr.error("Please upload jpg / png / svg format");  
          }
    	  
      }
      
     
     
    
    
      
     
}




  
  function AjaxCallForFetch(customerId) {        
	  
	  $.ajax({
          type: 'POST',
          url: './editcustomer',
          data: {'CustomerId' : customerId },
          success: function(msg){
        	  
        	  var customer = msg.customer;
        	  
				$('#hdnCustomerId').val(customer.customerId);
				$('#lblCustomerCode').html(customer.customerCode);
				
				
				$("#ddlCustomerType").val(customer.customerTypeID).change();
				
				
				/* $("#ddlPrimaryCustomer").val(customer.primaryCustomer).change(); */
				
				varEquipmentType = customer.stateID;
				varCustomerType = customer.primaryCustomer;
				
				
				$("#ddlCountry").val(customer.countryID).change();
				
			
				
				$("#ddlCustomerGroup").val(customer.customerGroup).change();
				
				
				
				$('#txtCustomerName').val(customer.customerName);
				$('#txtCustomerOrganization').val(customer.customerOrganization);
				$('#txtCustomerDescription').val(customer.customerDescription);
				$('#txtAddress').val(customer.address);
				$('#txtCity').val(customer.city);
				$('#txtPostalCode').val(customer.postalCode);
				$('#txtContactPerson').val(customer.contactPerson);
				$('#txtEmailId').val(customer.emailID);
				
				
				$('#txtMobileNumber').val(customer.mobile);
				$('#txtTelephoneNumber').val(customer.telephone);
				$('#txtFax').val(customer.fax);
				$('#txtWebsite').val(customer.website);
				$('#txtPoNumber').val(customer.customerPONumber);
				$('#txtPoDate').val(customer.customerPODateText);
				$('#txtCustomerReference').val(customer.customerReference);
				
				$("#ddlCustomerStatus").val(customer.activeFlag).change();
		        
				/* var btnText = $('#btnSubmit').text();              	    	
    	    	
    	    	if (btnText == 'Save') {
    	    		toastr.info("Successfully Created");
    	    	}else {
    	    		toastr.info("Successfully Updated");	
    	    	} */
				$('#btnSubmit').text('Update');
          },
          error:function(msg) { 
        	  toastr.error("There was a some error. Please Contect admin");
          	//alert(0); alert(msg);
          	}
	}); 
	 
}
  </script>






<script type="text/javascript">
        $(document).ready(function() {
        	$('#btnCreateDummy').attr("disabled", "disabled");
        	
      	  $('#btnCreate').click(function() {
        		if($('#ddlCategory').val== "") {
        	    	alert('Val Empty');
        	        $('.category ').addClass('error')
        	    }
        	})
        	
        	
        
        	
        	//('form').reset('clear');
        	$('#ddlSite').dropdown('clear');
        	$('#ddlType').dropdown('clear');
        	$('#ddlCategory').dropdown('clear');
        	$('#ddlPriority').dropdown('clear');
        	$('#txtCity').dropdown('clear');
        	$('#txtSubject').val('');
        	$('#txtPostalCode').val('');
        	$('#txtCustomerName').val('');
        	$('#txtCustomerOrganization').val('');
        	$('#txtContactPerson').val('');
        	$('#txtEmailId').val('');
        	$('#txtMobileNumber').val('');
        	$('#txtTelephoneNumber').val('');
        	$('#txtCustomerDescription').val('');
        	$('#txtAddress').val('');
        	$('#txtCustomerReference').val('');
        	$('#txtPoNumber').val('');
        	$('#txtTktdescription').val('');
        	$('#txtWebsite').val('');
        	$('#txtPoDate').val('');
        	
        	toastr.options = {
        	           "debug": false,
        	           "positionClass": "toast-bottom-right",
        	           "progressBar":true,
        	           "onclick": null,
        	           "fadeIn": 500,
        	           "fadeOut": 100,
        	           "timeOut": 1500,
        	           "extendedTimeOut": 3000
        	         }
        	
        	
        	$('#btnClear').click(function(){
        		$('#lblid').text('Auto-Generation');
        		$('#hdnCustomerId').val('Auto-Generation');        		
				$('#btnSubmit').text('Submit');
			});
        	
        	
        	$('#btnSubmit').click(function() {
        		debugger;
        		if ($('#txtPoDate').val() == '' )  {
        			 $('.txtpodate ').addClass('dateError');
        			 $('.glyphicon-calendar').addClass('dateError');
        		}
        		else {
       			 $('.txtpodate ').removeClass('dateError');
       			 $('.glyphicon-calendar').removeClass('dateError');
       		}
        	});
        	
        	$('#btnClear').click(function() {
        		debugger;
        		if ($('#txtPoDate').val() == '' )  {
        			 $('.txtpodate ').removeClass('dateError');
        			 $('.glyphicon-calendar').removeClass('dateError');
        		}
        	});
        	
        	var classstate = $('#hdnstate').val();
			
			$("#configurationid").addClass(classstate);
			$("#customerconfigid").addClass(classstate);
			
        	
        	 $("#ddlCountry").change(function() {
		            var value = $('#ddlCountry option:selected').val();
		            
		           
		            AjaxCallForStateID(value,varEquipmentType);
		            varEquipmentType = 0;
		           
		           
		            
		        });
        	 
        	 $("#ddlCustomerGroup").change(function() {
		            var value = $('#ddlCustomerGroup option:selected').val();
		            
		           
		            AjaxCallForPrimaryID(value,varCustomerType);
		            varCustomerType = 0;
		           
		           
		            
		        });
        	 
        	
        	 
        	 
        	 AjaxCallForPageLoad();
 			AjaxCallForLazyLoad();
        	
        	/* 
        	$('#example').DataTable( {
                "processing": true,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
       		 "bLengthChange": true,        		
                "serverSide": true,
                "ajax": "./customergrid",
                "columns": [
                    { "data": "customerCode" },
                    { "data": "customerName" },
                    { "data": "customerTypeName" },
                    { "data": "customerOrganization" },
                    { "data": "stateName" },
                    { "data": "countryName" },
                    { "data": "activeFlag" },
                    { "data": "activeFlag" }
                ]
            } );
        	
        	 */
        	/* 
        	 var dataSetString = $('#hdncustomerlist').attr('value');
        	 var dataSet = eval(dataSetString);
        	 
        	 
        	 var data = [];
             for ( var i=0 ; i<50000 ; i++ ) {
                 data.push( [ i, i, i, i, i, i, i, i ] );
             }
              
             $('#example').DataTable( {
                 data:           data,
                 deferRender:    true,
                 scrollCollapse: true,
                 scroller:       false,
                 processing: true,
                 deferLoading: 4
             } ); */
             
             
        	
        	 ////$('#example').DataTable().row.add([customer.customerCode,customer.customerName,varCustomerType,customer.customerOrganization,varState,varCountry,activeFlag,'<a  onclick="AjaxCallForFetch(' + customer.customerId + ')">Edit</a>']).draw(false);
              /*  var table = $('#example').dataTable( { 
            		"data": dataSet,
            		"processing": true,
            	    "rowId": "customerId",
            		"columns": [
                        { "data": "customerCode"},
                        { "data": "customerName" },
                        { "data": "customerTypeName" },
                        { "data": "customerOrganization" },
                        { "data": "stateName" },
                        { "data": "countryName" },
                        { "data": "activeFlag" }
            		],
            		"columnDefs": [ //title column has hyperlinks
                      	{
                        	"render": function ( data, type, row ) {
                                   		return '<a  onclick="AjaxCallForFetch(' + data.customerId + ')">Edit</a>' ;
                               		},
                            "targets": 0
                      	}],
            "order": [[ 0, "desc" ]]
        } ); */
               
        	    
        $('#example').DataTable({
        		 "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]]	
        });        
      
        	 $("#txtPoDate").datepicker({ dateFormat: "dd/mm/yy"});
        	
       
        	
        	 $("#txtPoDate").datepicker({ dateFormat: "dd/mm/yy"});
        
       	  $('.ui.dropdown').dropdown({forceSelection:false}); 
       	   
       	 $("#searchSelect").change(function() {
             var value = $('#searchSelect option:selected').val();
             var uid =  $('#hdneampmuserid').val();
           redirectbysearch(value,uid);
        }); $('.dataTables_filter input[type="search"]').attr('placeholder','search');

        
            if($(window).width() < 767)
				{
				   $('.card').removeClass("slide-left");
				   $('.card').removeClass("slide-right");
				   $('.card').removeClass("slide-top");
				   $('.card').removeClass("slide-bottom");
				}
            
           /*  $('.tkts').click(function(){
            	
            	 $("input").trigger("select");
            }) */
            
            $('.clr').click(function(){
           	 
           	 $("#txtFromDate").val('');
           	 $("#txtToDate").val('');
           	 $(".text").empty();
           	 $(".text").addClass('default')
           	 $(".text").html("Select");
           	 $('#ddlSite option:selected').removeAttr('selected');
           	 $('#ddlState option:selected').removeAttr('selected');
           	 $('#ddlCate option:selected').removeAttr('selected');
           	 $('#ddlPriority option:selected').removeAttr('selected'); 
           	window.location.href = './eventdetails';
           	//location.reload();
           	   });
            
            

            $('.close').click(function(){
                $('#tktCreation').hide();
                $('.clear').click();
                $('.category').dropdown();
                $('.SiteNames').dropdown();
            });
			$('.clear').click(function(){
			    $('.search').val("");
			});

			$('#ddlType').change(function(){              
                $('.category').dropdown('clear')
               });

			
			
			$('body').click(function(){
            	$('#builder').removeClass('open'); 
         	 });
			
			
	        
	    
			
			
			
        });
      </script>

      <script type='text/javascript'>//<![CDATA[
         $(window).load(function(){
        	 $('.ui.dropdown').dropdown({forceSelection:false});
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
         
         $('.ui.dropdown').dropdown({
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: true, 
             showOnFocus: true,
             allowLabels:false,
             on: "click" 
           });

		$('.ui.dropdown.oveallsearch').dropdown({
             onChange: function (value, text, $selectedItem) {
                
                var uid =  $('#hdneampmuserid').val();
                redirectbysearchvalue(value,uid);
             },
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: false, 
             showOnFocus: true,
             on: "click" 
           });
           
           
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );//]]> 


            var validation  = {
                    txtSubject: {
                               identifier: 'txtSubject',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               },
                               {
                                   type: 'maxLength[50]',
                                   prompt: 'Please enter a value'
                                 }]
                             },
                              ddlSite: {
                               identifier: 'ddlSite',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                             },
                            ddlType: {
                                  identifier: 'ddlType',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                            ddlCategory: {
                               identifier: 'ddlCategory',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                             ddlPriority: {
                               identifier: 'ddlPriority',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please select a dropdown value'
                               }]
                             },
                             description: {
                               identifier: 'description',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please Enter Description'
                               }]
                             }
             };
               var settings = {
                 onFailure:function(){
                     return false;
                   }, 
                 onSuccess:function(){    
                   $('#btnCreate').hide();
                   $('#btnCreateDummy').show()
                   //$('#btnReset').attr("disabled", "disabled");          
                   }};           
               $('.ui.form.validation-form').form(validation,settings);
         })
         
      </script>
    
<style type="text/css">
.dropdown-menu-right {
	left: -70px !important;
}
.red-tooltip + .tooltip > .tooltip-inner {background-color: #f00;}
.dateError {
background: #FFF6F6 !important;
 color: #9F3A38 !important;
 border-color: #E0B4B4 !important;
    }
    .tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 300px;
    background-color: #555;
    color: #fff;
    text-align: left;
    border-radius: 6px;
    padding: 5px 0;
    padding-left:10%;
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -60px;
    opacity: 0;
    transition: opacity 0.3s;
}

.tooltip .tooltiptext::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
    opacity: 1;
}
.ui.selection.dropdown .text {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    width: 90%;
}
 .ui.search.dropdown .menu {
    max-height: 12.02857143rem !important;
}
.toast-top-center {margin-top:5%;}
.hover {
	cursor:pointer;
}
.ui.search.dropdown > input.search {
    background: none transparent !important;
    border: none !important;
    box-shadow: none !important;
    cursor: text;
    top: 0em;
    left: -2px;
    width: 100%;
    outline: none;
    -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
    padding: inherit;
}
.txtheight {
	    min-height: 28px;
    height: 20px;
}
div.dataTables_wrapper div.dataTables_length select {
			    width: 50px;
			    display: inline-block;
			    margin-left: 3px;
			    margin-right: 4px;
			}
			
			.ui.form textarea:not([rows]) {
    height: 4em;
    min-height: 4em;
    max-height: 24em;
}
.ui.icon.input > i.icon:before, .ui.icon.input > i.icon:after {
    left: 0;
    position: absolute;
    text-align: center;
    top: 36%;
    width: 100%;
}
/* .evtDropdown {
	min-height: 2.7142em !important; 
font-size:13px !important;
padding: 0.78571429em 2.1em 0.78571429em 1em !important; 
} */

.input-sm, .form-horizontal .form-group-sm .form-control {
    font-size: 13px;
    min-height: 25px;
    height: 32px;
    padding: 8px 9px;
}
  
#example td a {
	color: #337ab7;
	font-weight: bold;
	cursor: pointer;
}
.dt-button-background {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 0 !important;
}
.dropevent {
	    min-width: 100px;
    margin-left: -35px;
}
.dropevent li a {
	color:#000 !important;
	font-weight:normal !important;
}
.tbh-width {
	width: 150px;
	padding-right: 30px !important;
}

.dt-buttons {
	margin-left: 50px;
}

.buttons-collection {
	height: 28px;
	padding: 3px;
	/*  z-index:9999; */
}


.buttons-excel {
	background: rgba(36, 95, 68, 0.87);
	color: #FFF;
}

.buttons-pdf {
	background: rgb(98, 165, 96);
	color: #FFF;
}

.dataTables_filter {
	float: right;
}

.dataTables_length {
	float: left;
}


.ui.selection.dropdown {
    width: 100%;
    margin-top: 0.1em;
   /*  height: 2.3em; */
}

.ui.selection.dropdown .menu {
    width: 100%;
    white-space: normal !important;
}

.ui.selection.dropdown .menu .item:first-child {
    border-top: 1px solid #ddd !important;
    min-height: 2.8em;
}

.ui.selection.dropdown .icon {
    text-align: right;
    margin-left: 7px !important;
}



</style>

</head>
<body class="fixed-header">

	<input type="hidden" value="${access.userID}" id="hdneampmuserid">
    <input type="hidden" value="" id="hdncustomerlist">
<input type="hidden" value="${classtate}" id="hdnstate">

	<jsp:include page="slider.jsp" />


	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					 <a href="./dashboard"><img src="resources/img/logo01.png" alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx tktSearch" id="SearcSElect">
                          
                           <div class="ui  search selection  dropdown  width oveallsearch">
                                         <input id="myInput" name="tags" type="text">
                                         <div class="default text">search</div>                                       
                                         <div id="myDropdown" class="menu">
                                              <jsp:include page="searchselect.jsp" />	


                                         </div>
                                  </div>
                          
                          
                        </div>
				
				 <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user_01.png" width="32" height="32">
                  </span>
                  
                   <c:if test="${not empty access}">         
                              <p class="user-login">${access.userName}</p>
                  </c:if>
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="logout1" class="dropdown-item color logout"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
               
              
                                                      	<div class="newticketpopup hidden">
								  <span data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false">
								  <p class="center  tkts"><img class="" src="resources/img/tkt.png"></p>
								  <p class="create-tkts">New Ticket</p></span>
               					</div>
               
						
						
              
			
				
			</div>
		</div>
		<div class="page-content-wrapper" id="QuickLinkWrapper1">

			<div class="content ">

				<div class="container-fixed-lg">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="./dashboard"><i class="fa fa-home mt-7"></i></a></li>
                     <li class="breadcrumb-item"><a>Configuration</a></li>
                     <li class="breadcrumb-item active">Customer Configuration</li>
                  </ol>
               </div>



				<div id="newconfiguration" class="">
					<div class="card card-transparent mb-0">
						<!-- Table Start-->
						<div class="padd-3 sortable">
			
			               
    		 <form method="post" class="ui form noremal" style="margin-bottom:0px; enctype="multipart/form-data">
							<div class="row">
								<div class="col-lg-12 col-xs-12 col-md-12 padd-0" id="datadetails">								
									<div class="card card-default bg-default" data-pages="card" style="margin-bottom:3px;">									
                              		<div class="card card-transparent " style="padding:5px 5px;margin-bottom:0px;" >
                                 <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
                                    <li class="nav-item">
                                       <a href="#" class="active" data-toggle="tab" data-target="#slide1"><span>Customer Details</span></a>
                                    </li>
                                    
                                 </ul>
                                 <div class="tab-content">
                                    <div class="tab-pane  active" id="slide1">
                                       <div class="col-md-12 m-t-10 column-seperation">
                                       	<div class="col-md-3">
    			 			 					<label class="fields">Code</label>
    			 			 					<input type="hidden" id="hdnCustomerId"  value="Auto-Generation"/>
    			 								<label class="fields m-t-10" id="lblid">Auto-Generation</label>		 
    			 						</div>    			 		
    			 		<div class="col-md-3 mm-10">
    			 			<div class="field required m-b-0">
    			 				  <label class="fields p-l-0">Type</label>
    			 			</div>  
    			 			<div class="field">
    			 			 <select class="ui search selection dropdown" name="ddlCustomerType" id="ddlCustomerType">
						        <option value="">Select</option>
						      </select>
						    </div>  			 			
    			 		</div>
    			 		
    			 		<div class="col-md-3 mm-10">
    			 		<div class="field required m-b-0">
    			 			 <label class="fields p-l-0">Name</label>
    			 			</div>
    			 			<div class="field">
    			 				<input type="text" name="txtCustomerName"  autocomplete="off" id="txtCustomerName" maxLength="100" placeholder="Customer Name">
    			 			</div>
    			 		</div>
    			 		
    			 		<div class="col-md-3 mm-10">
    			 			<div class="field required m-b-0">
    			 			 	<label class="fields p-l-0">Organization</label>
    			 			</div>
    			 			<div class="field">
    			 			 	<input type="text" name="txtCustomerOrganization" autocomplete="off" id="txtCustomerOrganization" placeholder="Customer Organization">
    			 		
    			 			</div>
    			 		</div>
    			 		    </div>
                                       
                           <div class="col-md-12   m-t-10">
		    			 		<div class="col-md-3 mm-10">
		    			 			<div class="field required m-b-0">
		    			 			 	<label class="fields p-l-0">City</label>
		    			 			</div>
		    			 			<div class="field">
    			 			<input id="txtCity" autocomplete="off" name="txtCity" placeholder="City" type="text">
    			 			</div>
		    			 		</div>
    			 		
    			 		<div class="col-md-3 mm-10">
    			 			<div class="field required m-b-0">
    			 			 	<label class="fields p-l-0">Country</label>
    			 			</div>
    			 			<div class="field">
    			 			<select class="ui search selection dropdown" name="ddlCountry" id="ddlCountry">
						        <option value="">Select</option>
						      </select>	</div>
    			 		</div>
    			 		
    			 		<div class="col-md-3 mm-10">
    			 			<div class="field required m-b-0">
    			 			 	<label class="fields p-l-0">State</label>
    			 			</div>
    			 			<div class="field">
    			 			<select class="ui search selection dropdown" name="ddlState" id="ddlState">
						        <option value="">Select</option>
						      </select>
    			 			</div>
    			 		</div>
    			 		
    			 		
    			 		<div class="col-md-3 mm-10">
    			 			<div class="field required m-b-0">
    			 			 	<label class="fields p-l-0">Postal Code</label>
    			 			</div>
    			 			<div class="field">
    			 			<input id="txtPostalCode" autocomplete="off" name="txtPostalCode" placeholder="Postal Code" type="text">
    			 			</div>
    			 		</div>
    			 		</div> 
    			 		
    			 		 <div class="col-md-12   m-t-10">
    			 		<div class="col-md-3 mm-10 ">
    			 			<div class="field m-b-0">
    			 			 	<label class="fields p-l-0">Contact</label>
    			 			</div>
    			 			<div class="field">
    			 			<input id="txtContactPerson" autocomplete="off" name="txtContactPerson" placeholder="Contact Person" type="text">
    			 			</div>
    			 		</div>
    			 		
    			 		<div class="col-md-3 mm-10">
    			 			<div class="field required m-b-0">
    			 			 	<label class="fields p-l-0">Email Id</label>
    			 			</div>
    			 			<div class="field">
    			 			<input id="txtEmailId" autocomplete="off" name="txtEmailId" placeholder="Email Id" type="text">    			 		
    			 			</div>
    			 		</div>
    			 		<div class="col-md-3 mm-10">
    			 			<div class="field required m-b-0">
    			 			 	<label class="fields p-l-0">Mobile No</label>
    			 			</div>
    			 			<div class="field">
    			 				<input id="txtMobileNumber" autocomplete="off" name="txtMobileNumber" placeholder="Mobile No (9999999999)" type="text">    			 		
							</div>
    			 		</div>
    			 		
    			 		<div class="col-md-3 mm-10">
    			 			<div class="field required m-b-0">
    			 			 	<label class="fields p-l-0">Telephone No</label>
    			 			</div>
    			 			<div class="field">
    			 			<input id="txtTelephoneNumber" autocomplete="off" name="txtTelephoneNumber" placeholder="Telephone No (999/9999 - 99999999)" type="text">
    			 			</div>
    			 		</div>
    			 		</div>
    			 		<div class="col-md-12   m-t-10">
    			 		<div class="col-md-3 mm-10">
    			 			<div class="field m-b-0">
    			 			 	<label class="fields p-l-0">Description</label>
    			 			</div>
    			 			<div class="field">
    			 				<input id="txtCustomerDescription" autocomplete="off" name="txtCustomerDescription" placeholder="Description" type="text">
    			 			</div>
    			 		</div>
    			 		
    			 		<div class="col-md-3 mm-10">
    			 			<div class="field m-b-0">
    			 			 	<label class="fields p-l-0">Category</label>
    			 			</div>
    			 			<div class="field">
    			 			<select class="ui search selection dropdown" name="ddlCustomerGroup" id="ddlCustomerGroup">
						        <option value="">Select</option>
						      </select>
    			 			</div>
    			 		</div>
    			 		<div class="col-md-3 mm-10">
    			 			<div class="field  m-b-0">
    			 			 	<label class="fields p-l-0">Primary Customer</label>
    			 			</div>
    			 			<div class="field">
    			 			<select class="ui search selection dropdown" name="ddlPrimaryCustomer" id="ddlPrimaryCustomer">
						        <option value="">select</option>
						        
						      </select>
    			 			</div>
    			 		</div>
    			 		<div class="col-md-3 mm-10">
    			 			<div class="field required m-b-0">
    			 			 	<label class="fields p-l-0">Address</label>
    			 			</div>
    			 			<div class="field">
    			 			<input id="txtAddress" autocomplete="off" name="txtAddress" placeholder="Address" type="text">    			 			 	
    			 			</div>
    			 		</div>
    			 	</div>
    			 	
    			 	
    			 		<div class="col-md-12   m-t-10">    			 
    			 		<div class="col-md-3  mm-10">
    			 			<div class="field  m-b-0">
    			 			 	<label class="fields p-l-0">Website</label>
    			 			</div>
    			 			<div class="field">
    			 			<input id="txtWebsite" autocomplete="off" name="txtWebsite" placeholder="Website" type="text">    			 		
    			 			</div>
    			 		</div>
    			 		
    			 		<div class="col-md-3  mm-10">
    			 			<div class="field required  m-b-0">
    			 			 	<label class="fields p-l-0">Customer Reference Number</label>
    			 			</div>
    			 			<div class="field">
    			 			<input id="txtCustomerReference" autocomplete="off" name="txtCustomerReference" placeholder="Customer Reference Number" type="text" maxlength="5">    			 		
    			 			</div>
    			 		</div>
    			 		
    			 		<div class="col-md-3 mm-10">
    			 			<div class="field  m-b-0">
    			 			 	<label class="fields p-l-0">PO Number</label>
    			 			</div>
    			 			<div class="field">
    			 				<input id="txtPoNumber" autocomplete="off" name="txtPoNumber" placeholder="PO Number" type="text">    			 		
							</div>
    			 		</div>
    			 		
    			 		<div class="col-md-3 mm-10">
    			 			<div class="field required m-b-0">
    			 			 	<label class="fields p-l-0">PO Date</label>
    			 			</div>
    			 			<div class="controls">
							<div class="input-group">
								<input id="txtPoDate" autocomplete="off" type="text" placeholder="PO Date" class="date-picker form-control txtpodate" name="fromDate"/>
								<label for="txtPoDate" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span> </label>
							</div>
						</div>     
    			 		</div>
    			 		
    			 	
    			 		</div> 
    			 		 
    			 		<div class="col-md-12   m-t-10">    			 
    			 		
    			 					 		
    			 		<div class="col-md-3 mm-10">
			               	 <div class="field required m-b-0">
			                	  <label class="fields p-l-0">Status</label>
			             	   </div>
			             	   <div class="field">
			               	 <select class="ui search selection dropdown"  name="ddlCustomerStatus" id="ddlCustomerStatus">
			                    <option value="">Select</option>
			                	  </select>
			               	 </div>
			             	 </div>
			             	 <div class="col-md-3 mm-10">
			               	 <div class="field  m-b-0">
			                	<label class="fields p-l-0">Logo</label>
			             	   </div>
			             	    <div class="form-group">
			             	    <div class="row">
			             	    	<div class="col-md-8">
			             	    		<input type="file" class="" data-icon="false" id="excelfile" name ="file"   style="border:none;">
			             	    	</div>
			             	    	<div class="col-md-4">
			             	    		<a href="#" data-toggle="tooltip" data-placement="bottom"
       title="" data-original-title="Image format should be in jpg / png / svg. Image size should be less than 5 MB."
       class="red-tooltip"><i class="fa fa-info-circle" style="font-size:32px;color:#000;"></i></a>
			             	    	</div>
			             	    </div>
                                             
                                        
                                          </div>
			             	 </div>
			             	 <div class="col-md-3 mm-10 m-t-15">
			               	 <div class="field required m-b-0">
			               	 
			                	 <!-- <a href="#" data-toggle="tooltip" title="Image format should be in jpg / png / svg. "><i class="fa fa-info-circle" style="font-size:48px;color:red"></i></a> --> 
			                	<!-- <p>Hello</p>
			                	<div class="tooltip">Hover over me
  <span class="tooltiptext xccsacasc">Image format should be in jpg / png / svg.<br>Image size should be less than 5 MB.</span>
</div> -->
			             	   </div>
			             	   <div class="field">
			               	<!--  <select class="ui search selection dropdown"  name="ddlCustomerStatus" id="ddlCustomerStatus">
			                    <option value="">Select</option>
			                	  </select> -->
			               	 </div>
			             	 </div>
    			 		</div> 
    			 			
    			 		<div class="col-md-12 center  m-b-5 m-t-30">
              							<!-- <div class="btn btn-success submit confi-btn-width"  id="btnSubmit" >Submit</div> -->
              							<button class="btn btn-success submit confi-btn-width" id="btnSubmit" type="submit">Submit</button>
                  						<div class="btn btn-primary clear m-l-15 confi-btn-width" id="btnClear">Reset</div>
          				</div>
                                    </div>

                                    
                                 </div>
                              </div>
    			 	</div>
    			 
    			 					
    			 
									
								</div>
							</div>
							
							
							
							
						</form>
						
						
						</div>


						<!-- Table End-->
						
						<!-- Table Start-->
						<div class="padd-3 sortable" id="equipmentdata">
							<div class="row" id="QuickLinkWrapper2">								
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails" style="height:260px;">
									<div class="card card-default bg-default table-responsive" data-pages="card">
										<!-- <div class="card-header">
											<div class="card-title tbl-head ml-15" style="text-transform:none !important;">
												 <i class="fa fa-list" aria-hidden="true"></i> List of Events
											</div>
										</div> -->
										<!-- <div class="col-md-12" style="margin-top:10px;height:220px !important;">
											<div class="col-md-4">
											<div id="barcontainer" style="min-width: 310px; height: 200px; margin: 0 auto"></div>
											</div>
											<div class="col-md-4">
												<div id="donutcontainer" style="height: 250px"></div>
											</div>
											<div class="col-md-4 padd-0">
												<div id="activitycontainer"></div>
											</div>
										</div> -->
										<div class="padd-5 table-responsive">
                                        	<table id="example" class="table table-striped table-bordered"
                                            width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                 <th style="width:9%;">Customer Code</th>                                                
                                                  <th style="width:13%;">Customer Name</th>
                                                  <th style="width:10%;">Customer Type</th>
                                                  <th style="width:14%;">Customer Organization</th>
                                               	   <th style="width:8%;">State</th>
                                               	  <th style="width:8%;">Country</th>                                               	  
                                               	  <th style="width:10%;">Customer Status</th>
                                               	   <th style="width:10%;">Customer Logo</th>
                                                  <th style="width:5%;">Edit</th>
                                                 <!--  <th style="width:8%;">De-activate</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                               


                                            </tbody>
                                        </table>


                                    </div>
                                   </div>
								</div>


							</div>

						</div>
						

						<!-- Table End-->
					</div>
				</div>


			</div>

			<div class="container-fixed-lg footer mb-0">
				<div class="container-fluid copyright sm-text-center">
					<p class="small no-margin pull-left sm-pull-reset">
						<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
							CLEAN ENERGY</span>. <span class="hint-text">All rights reserved.
						</span>
					</p>
					<p class="small no-margin pull-rhs sm-pull-reset">
						<span class="hint-text">Powered by</span> <span>MESTECH
							SERVICES PVT LTD</span>.
					</p>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Side Bar Content Start-->

	


	<div class="quickview-wrapper  builder hidden-sm hidden-xs"
		id="builder">
		<div class="p-l-10 p-r-10 ">
			<a class="builder-close quickview-toggle pg-close"></a>
			<a class="builder-toggle" data-toggle="quickview"
				data-toggle-element="#builder"><img
				src="resources/img/ImageResize_06.png"></a>
			<ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary"
				id="builderTabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#tabLayouts" role="tab"
					aria-controls="home"><span>Quick Link</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active " id="tabLayouts" role="tabcard">
					<div class="scrollable">
						<div class="p-r-50">
							<div class="list">
								<ul>
								<!-- 	<li><a href="#QuickLinkWrapper1">Ticket Filters</a></li> -->
									<li><a href="#QuickLinkWrapper2">List Of Customer</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>






	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min3.js" type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>









	<div id="gotop"></div>

	<!-- Share Popup End !--> 
<!-- Modal -->
<div id="tktCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog modal-top">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">New Ticket</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"><span class="errormess">*</span> All fields are mandatory</p>
				 </div>            
        		<form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form validation-form"> 
             
            <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width SiteNames"
									id="ddlSite" name="ddlSite" path="siteID" onchange="getEquipmentAgainstSite()">
									<form:option value="" tabindex="0">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>
						
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                                                <div class="col-md-4 col-xs-12">
                                                       <label class="fields required m-t-10">Equipment</label>
                                                </div>

                                                <div class="col-md-8 col-xs-12">
                                                       <form:select
                                                              class="field ui fluid search selection dropdown width equipments"
                                                              id="ddlEquipment" path="equipmentID" name="ddlEquipment">
                                                              <option value="">Select</option>
                                                       </form:select>
                                                </div>
                          </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Type</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width evtDropdown" id="ddlType" name="ddlType"  path="ticketType">
                        <form:option value="">Select</form:option>
                        <form:option value="Operation">Operation</form:option>
                        <form:option value="Maintenance">Maintenance</form:option>
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Category</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width category evtDropdown" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                         <form:option data-value="Operation" value="">Select</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Preventive Maintenance">Preventive Maintenance</form:option>
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                          <form:option data-value="Maintenance" value="">Select</form:option>
                    </form:select> 
                  </div>
                  
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                  <div class="col-md-4 col-xs-12"><label class="fields required m-t-10">Subject</label></div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<form:input  placeholder="Subject"  autocomplete="off" name="Subject"  type="text"  id="txtSubject" path="ticketDetail" maxLength="50"/>
                  	</div>
                  	</div>
               </div> 
              
              <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Priority</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                     <form:select class="field ui fluid search selection dropdown width evtDropdown" id="ddlPriority" name="user" path="priority">
                        <form:option value="">Select </form:option>
                        <form:option data-value="3" value="3">High</form:option>
                        <form:option data-value="2" value="2">Medium</form:option>
                        <form:option data-value="1" value="1">Low</form:option>
                    </form:select> 
                  </div>
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Description</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<textarea id="txtTktdescription"  autocomplete="off" name="description" style="resize: none;" placeholder="Ticket Description" maxLength="120"></textarea>
                  	</div>
                    
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
				       <div class="btn btn-success  submit" id="btnCreate">Create</div>
				        <button class="btn btn-success" type="button" id="btnCreateDummy" tabindex="7"  style="display:none;">Create</button>
				      
				        <div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
				  </div>
  <!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
  <!-- <div class="ui error message"></div> -->
</form:form>
				<!-- <div class="ui error message"></div> -->
				
	  		</div>
	  </div>
    </div>
 </div>
<!-- Address Popup End !--> 
<!-- Modal -->
<div id="evntCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog modal-top">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">Event Ticket Creation</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"> All fields are mandatory</p>
				 </div>            
				 <!-- <span class="errormess">*</span> -->
        		<form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form validation-form"> 
             
              <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                     <div class="col-md-4 col-xs-12">
                     <p class="fields  m-t-10">Site</p>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<p class="fields  m-t-10" autocomplete="off" id="txtSiteName" path="siteID"></p>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <p class="fields  m-t-10">Type</p>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<p class="fields  m-t-10" autocomplete="off" id="txtTypeName" path="ticketType">Operation</p>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                     <div class="col-md-4 col-xs-12">
                     <p class="fields  m-t-10">Priority</p>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<p class="fields  m-t-10" autocomplete="off" id="txtPriority" path="priority"></p>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12 m-t-15 mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Category</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <form:select class="field ui fluid search selection dropdown width category evtDropdown" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                         <form:option data-value="Operation" value="">Select</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
					<form:option data-value="Maintenance" value="Preventive Maintenance">Preventive Maintenance</form:option>
										
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                          <form:option data-value="Maintenance" value="">Select</form:option>
                    </form:select> 
                  </div>
                  
               </div> 
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                  <div class="col-md-4 col-xs-12"><label class="fields required m-t-10">Subject</label></div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<form:input  placeholder="Subject"  autocomplete="off" name="Subject"  type="text"  id="txtSubject" path="ticketDetail" maxLength="50"/>
                  	</div>
                  	</div>
               </div> 
              
              
               <div class="col-md-12 col-xs-12 m-t-15  mmt-15">
                     <div class="col-md-4 col-xs-12">
                      <label class="fields required m-t-10">Description</label>
                  </div>
                  <div class="col-md-8 col-xs-12">
                  	<div class="field">
                  		<textarea id="txtTktdescription" autocomplete="off" name="description" style="resize: none;" placeholder="Ticket Description" maxLength="120"></textarea>
                  	</div>
                    
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
				       <div class="btn btn-success  submit">Create</div>
				       <button class="btn btn-success" type="button" id="btnCreateDummy" tabindex="7"  style="display:none;">Create</button>
				        <div class="btn btn-primary clear m-l-15">Reset</div>
				  </div>
  <!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
  <!-- <div class="ui error message"></div> -->
</form:form>
				<!-- <div class="ui error message"></div> -->
				
	  		</div>
	  </div>
    </div>
 </div>
<!-- Address Popup End !--> 
	<script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
	<script>
         $(document).ready(function() {
        	 $("a").tooltip();
            $(window).fadeThis({
               speed: 500,
            });
         });
      </script>


<script type="text/javascript">
      $(document).ready(function(){
var validation  = {
		ddlCustomerType:{
      identifier:'ddlCustomerType',
      rules:[
        { type:'empty'}
      ]
    },
    ddlSite:  {
        identifier: 'ddlSite',
        rules: [
          {
            type   : 'empty',
            prompt : 'Select Site'
          }
        ]
      },
      /* ddlCustomerGroup:  {
          identifier: 'ddlCustomerGroup',
          rules: [
            {
              type   : 'empty',
              prompt : 'Select Customer Group'
            }
          ]
        }, */
        
          txtCustomerName: {
              identifier: 'txtCustomerName',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Enter Customer Organization'
                  },
                  {
                      type:'regExp[/^[ A-Za-z]*$/]',
                      prompt : 'Enter Numbers Only'
                  },
                  {
                      type   : 'minLength[1]',
                      prompt : 'CustomerName Must Be at Least 6 Characters'
                    },
                  {
                      type   : 'maxLength[100]',
                      prompt : 'CustomerName Must Be at Least 6 Characters'
                    }
                ] 
            },
          txtCustomerOrganization: {
              identifier: 'txtCustomerOrganization',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Enter Customer Organization'
                  },
                  {
                      type   : 'minLength[1]',
                      prompt : 'PostalCode Must Be at Least 6 Characters'
                    },
                  {
                      type   : 'maxLength[100]',
                      prompt : 'PostalCode Must Be at Least 6 Characters'
                    }
                ] 
            },
            /* txtCustomerDescription: {
              identifier: 'txtCustomerDescription',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Enter CustomerDescription'
                  },
                  {
                      type   : 'minLength[1]',
                      prompt : 'PostalCode Must Be at Least 6 Characters'
                    },
                  {
                      type   : 'maxLength[200]',
                      prompt : 'PostalCode Must Be at Least 6 Characters'
                    }
                ] 
            }, */
            txtAddress: {
                identifier: 'txtAddress',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Enter Address'
                    },
                    {
                        type   : 'minLength[1]',
                        prompt : 'PostalCode Must Be at Least 6 Characters'
                      },
                    {
                        type   : 'maxLength[300]',
                        prompt : 'PostalCode Must Be at Least 6 Characters'
                      }
                  ] 
              },
              txtCity: {
                identifier: 'txtCity',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Enter City'
                    },
                    {
                        type:'regExp[/^[ A-Za-z]*$/]',
                        prompt : 'Enter Numbers Only'
                    },
                    {
                        type   : 'minLength[1]',
                        prompt : 'PostalCode Must Be at Least 6 Characters'
                      },
                    {
                        type   : 'maxLength[50]',
                        prompt : 'PostalCode Must Be at Least 6 Characters'
                      }
                  ] 
              },
              txtPostalCode: {
                identifier: 'txtPostalCode',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Enter PostalCode'
                    },
                    {
                        type:'number',
                        prompt : 'Enter Numbers Only'
                    },
                    {
                        type   : 'minLength[6]',
                        prompt : 'PostalCode Must Be at Least 6 Characters'
                      },
                    {
                        type   : 'maxLength[10]',
                        prompt : 'PostalCode Must Be at Least 6 Characters'
                      }
                  ] 
              },
              
              txtCustomerReference: {
                  identifier: 'txtCustomerReference',
                    rules: [
                      {
                        type   : 'empty',
                        prompt : 'Enter Customer Reference'
                      },
                      {
                    	  type:'regExp[/^[ A-Za-z0-9]*$/]',
                          prompt : 'Enter Numbers Only'
                      },
                      {
                          type   : 'minLength[1]',
                          prompt : 'Customer Reference Must Be at Least 6 Characters'
                        },
                      {
                          type   : 'maxLength[5]',
                          prompt : 'Customer Reference Must Be at Least 6 Characters'
                        }
                    ] 
                },
             /*  txtContactPerson: {
                identifier: 'txtContactPerson',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Enter Contact Person'
                    },
                    {
                        type:'regExp[/^[ A-Za-z]*$/]',
                        prompt : 'Enter Numbers Only'
                    },
                    {
                        type   : 'minLength[1]',
                        prompt : 'PostalCode Must Be at Least 6 Characters'
                      },
                    {
                        type   : 'maxLength[100]',
                        prompt : 'PostalCode Must Be at Least 6 Characters'
                      }
                  ] 
              }, */
              txtEmailId: {
                identifier: 'txtEmailId',
                  rules: [
                    {
                      type   : 'email',
                      prompt : 'Enter Email'
                    }
                  ] 
              },
              txtMobileNumber: {
                identifier: 'txtMobileNumber',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Enter MobileNumber'
                    },
                    {
                        type:'regExp[/^[ 0-9+-]*$/]',
                        prompt : 'Enter Numbers Only'
                    },
                    {
                        type   : 'minLength[10]',
                        prompt : 'PostalCode Must Be at Least 6 Characters'
                      },
                    {
                        type   : 'maxLength[14]',
                        prompt : 'PostalCode Must Be at Least 6 Characters'
                      }
                  ] 
              },
           /*   txtWebsite: {
                identifier: 'txtWebsite',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Enter Website'
                    },
                    {
                        type   : 'url',
                        prompt : 'Enter Website'
                      }
                  ] 
              }, */
             /*  txtFax: {
                identifier: 'txtFax',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Enter Fax'
                    }
                  ] 
              }, */
               txtTelephoneNumber: {
                identifier: 'txtTelephoneNumber',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Enter TelephoneNumber'
                    },
                    {
                    	type:'regExp[/^[0-9-]*$/]',
                        prompt : 'Enter TelephoneNumber'
                      },
                      {
                          type   : 'minLength[12]',
                          prompt : 'PostalCode Must Be at Least 6 Characters'
                        },
                      {
                          type   : 'maxLength[17]',
                          prompt : 'PostalCode Must Be at Least 6 Characters'
                        }
                  ] 
              }, 
              /* txtPoNumber: {
                identifier: 'txtPoNumber',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Enter PoNumber'
                    },
                    {
                        type:'regExp[/^[ a-z0-9]*$/]',
                        prompt : 'Enter Numbers Only'
                    },
                    {
                        type   : 'minLength[1]',
                        prompt : 'PostalCode Must Be at Least 6 Characters'
                      },
                    {
                        type   : 'maxLength[50]',
                        prompt : 'PostalCode Must Be at Least 6 Characters'
                      }
                  ] 
              }, */
              
              ddlCustomerStatus: {
                identifier: 'ddlCustomerStatus',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Enter Customer Status'
                    }
                  ] 
              },
              /* ddlPrimaryCustomer:  {
                  identifier: 'ddlPrimaryCustomer',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Select Primary Customer'
                    }
                  ]
                }, */
                ddlCountry:  {
                    identifier: 'ddlCountry',
                    rules: [
                      {
                        type   : 'empty',
                        prompt : 'Select Country'
                      }
                    ]
                  },
                  excelfile: {
                      required: true, 
                      extension: "png|svg|jpg",
                      filesize: 5000000,   
                      rule: [
                      { 
                    type   : 'empty',
                       file: "File must be JPG or PNG, less than 5MB" 
                      }]
                  },
                  ddlState:  {
                      identifier: 'ddlState',
                      rules: [
                        {
                          type   : 'empty',
                          prompt : 'Select State'
                        }
                      ]
                    },};
var settings = {
  onFailure:function(){ 
    //alert('fail');
      toastr.warning("Validation Failure");	
      return false;
    }, 
  onSuccess:function(){    
    //alert('Success');
    AjaxCallForSaveOrUpdate();
    return false; 
    }};
  
$('.ui.form.noremal').form(validation,settings);
      });
    </script>


</body>

<script type="text/javascript">
function getEquipmentAgainstSite() {
    var siteId = $('#ddlSite').val();
   
    $.ajax({
          type : 'GET',
          url : './equipmentsBysites?siteId=' + siteId,
          success : function(msg) {
       	   
       	   var EquipmentList = msg.equipmentlist;
                 
                        for (i = 0; i < EquipmentList.length; i++) {
                               $('#ddlEquipment').append(
                                             "<option value="+EquipmentList[i].equipmentId+">" + EquipmentList[i].customerNaming
                                                          + "</option>");
                        }

          },
          error : function(msg) {
                 console.log(msg);
          }
    });
}

       </script>

</html>

