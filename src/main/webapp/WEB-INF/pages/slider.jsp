
<input type="hidden" value="${accessPages}" id="hdnaccesspage">
<nav class="page-sidebar" data-pages="sidebar">

	<div class="sidebar-header">
		<a href="./dashboard"><img src="resources/img/logo01.png"
			class="eira-logo" alt="logo"
			title="Electronically Assisted Monitoring and Portfolio Management"></a>
		<i class="fa fa-bars bars"></i>
	</div>

	<div class="sidebar-menu">


			<ul class="menu-items">



				

					<li class="dashboard-page hidden" style="margin-top: 2px;" id="dashboardid"><a href="./dashboard"
						class="detailed"> <span class="title">Dashboard</span>
					</a> <span class="icon-thumbnail"><i class="fa fa-tachometer"></i></span>
					</li>
				

				
				
			
					<li class="visulaization-page hidden" id="visulaizationid"><a href="javascript:;"><span class="title">Visualization</span>
							<span class=" arrow"></span></a> <span class="icon-thumbnail">
							<i class="fa fa-desktop" aria-hidden="true"></i>
					</span>
						<ul class="sub-menu">

							
								<li class="customerview-page hidden" id="customerviewid"><a href="./customerlist">Customer View</a> <span
									class="icon-thumbnail"><i class="fa fa-eye"></i></span></li>
							



							<li class="siteview-page hidden" id="siteviewid"><a href="./sitelist">Site View</a> <span
								class="icon-thumbnail"><i class="fa fa-map-marker"></i></span></li>

						</ul></li>

				

				<li class="O-Mmenu-page hidden" id="o-mid"><a href="javascript:;"><span
						class="title">Operation & Maintenance</span> <span class=" arrow"></span></a>
					<span class="icon-thumbnail" style="padding-top: 5px !important;"><img
						src="resources/img/maintance.png"></span>
					<ul class="sub-menu">


						<li class="tickets-page hidden" id="ticketsid"><a href="./ticketskpi">Tickets</a> <span
							class="icon-thumbnail"><i class="fa fa-ticket"></i></span></li>

						<li class="events-page hidden" id="eventsid"><a href="./eventskpi">Events</a> <span
							class="icon-thumbnail"><i class="fa fa-calendar"></i></span></li>

						

					</ul></li>





				<li class="analysis-page hidden" id="analyticsid"><a href="./analysis"><span class="title">Analytics</span></a>
					<span class="icon-thumbnail"><i class="fa fa-area-chart"></i></span>
				</li>
				
				
				<li class="documentation-page hidden" id="documentationid"><a href="javascript:;"><span class="title">Documentation</span>
							<span class=" arrow"></span></a> <span class="icon-thumbnail">
							<i class="fa fa-folder" aria-hidden="true"></i>
					</span>
						<ul class="sub-menu">

							
								<li class="documents-upload hidden" id="documentsid"><a href="./DocumentsUpload"><span class="title">Documents Upload</span></a>
					<span class="icon-thumbnail"><i class="fa fa-upload"></i></span></li>
					
					<li class="documents-download hidden" id="documentsdownloadid"><a href="./DocumentsDownload"><span class="title">Documents Download</span></a>
					<span class="icon-thumbnail"><i class="fa fa-download"></i></span></li>
							


						</ul></li>

					

				<li class="reports-page hidden" id="reportsid"><a href="./downloadreports"><span class="title">Reports</span></a>
					<span class="icon-thumbnail"><i class="fa fa-file"></i></span></li>


			

					<li class="configurationmenu-page hidden" id="configurationid"><a href="javascript:;"><span class="title">Configuration</span>
							<span class=" arrow"></span></a> <span class="icon-thumbnail"><i
							class="pg-tables"></i></span>
						<ul class="sub-menu">

						<li class="customer-page hidden" id="customerconfigid"><a href="./customers">Customer Config.</a> <span
							class="icon-thumbnail">CU</span></li>


						<li class="site-page hidden" id="siteconfigid"><a href="./sites">Site Config.</a> <span
							class="icon-thumbnail">SI</span></li>

						<li class="equipmentcategory-page hidden" id="ecconfigid"><a href="./equipmentcategories">Equipment Category
								Config.</a> <span class="icon-thumbnail">EC</span></li>

						<li class="equipmenttype-page hidden" id="etconfigid"><a href="./equipmenttypes">Equipment Type Config.</a> <span
							class="icon-thumbnail">ET</span></li>


						<li class="equipment-page hidden" id="econfigid"><a href="./equipments">Equipment Config.</a> <span
							class="icon-thumbnail">EQ</span></li>

						<li class="parameter-page hidden" id="parameterconfigid"><a href="./parameterstandards">Standard Parameter</a> <span
							class="icon-thumbnail">SP</span></li>

						<li class="errorcode-page hidden" id="errorcodeconfigid"><a href="./errorcodes">Error Code Config</a> <span
							class="icon-thumbnail">EC</span></li>
							
						<li class="status-page hidden" id="statusconfigid"><a href="./sitestatus">Status Config</a> <span
							class="icon-thumbnail">SC</span></li>
							
							<li class="dataloggermaster-page hidden" id="dataloggerconfigid"><a href="./dataloggermaster">Datalogger Config</a> <span
							class="icon-thumbnail">DC</span></li>
							
					<li class="master-upload hidden" id="masteruploadconfigid"><a href="./masteruploads">Master Upload</a> <span
							class="icon-thumbnail">MU</span></li>
							
							<li class="historicupload-page hidden" id="historicuploadconfigid"><a href="./historicaluploads">Historic Upload</a> <span
							class="icon-thumbnail">HU</span></li>
							
							<li class="datacollection-page hidden" id="datacollectionconfigid"><a href="./datacollection">Data Collection</a> <span
							class="icon-thumbnail">DC</span></li>
							
							
						 <li class="sop-push hidden" id="soppushconfigid"><a href="./sop">SOP</a> <span
							class="icon-thumbnail">SOP</span></li> 


				
							<li class="user-page hidden" id="userconfigid"><a href="./users">User Config.</a> <span
							class="icon-thumbnail">US</span></li>
							
								<li class="roleaccess-page hidden" id="roleaccessconfigid"><a href="./roleaccess">Role Access Config.</a> <span
							class="icon-thumbnail">RA</span></li>


						</ul></li>

			



			</ul>


	


		<div class="clearfix"></div>
	</div>



</nav>


<script>
$(document).ready(
		function() {
		
			var pagelist = $('#hdnaccesspage').val();
			
			var Arrpagelist = pagelist.split("#");
			if (Arrpagelist) {
				for (i = 0; i < Arrpagelist.length; i++) {
					if(Arrpagelist[i]){
						var pagename = Arrpagelist[i];
						$('.' + pagename).removeClass('hidden');
					}
					
				}
			}
		});


</script>
