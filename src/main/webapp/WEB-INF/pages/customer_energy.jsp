<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 <div class="card card-default bg-default slide-left" id="energy" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">today's energy</div>
                                    <div class="card-title pull-right"><i class="fa fa-bolt font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block">
                                    <h3 class="m-t-10">
                                       <span class="semi-bold"><strong>${customerview.todayEnergy}</strong></span>
                                    </h3>
                                    	<div class="col-md-12 padd-0">
                                    		<div class="col-md-4 col-xs-4 padd-0">
                                    			<p>Last Checked</p>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<p>:</p>
                                    		</div>
                                    		<div class="col-md-6 col-xs-5 padd-0">
                                    			<p>${customerview.lastUpdatedTime}</p>
                                    		</div>
                                    	</div>
                                    	<div class="col-md-12 padd-0 uitlity m-t-10">
                                    		<div class="col-md-4 col-xs-4 padd-0">
                                    			<p>Data Received</p>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<p>:</p>
                                    		</div>
                                    		<div class="col-md-6 col-xs-5 padd-0">
                                    			<p>${customerview.eventTime}</p>
                                    		</div>
                                    	</div>
                                    	<div class="col-md-12 padd-0 uitlity m-t-10">
                                    		<div class="col-md-4 col-xs-4 padd-0">
                                    			<p>Total Energy</p>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<p>:</p>
                                    		</div>
                                    		<div class="col-md-6 col-xs-5 padd-0">
                                    			<p>${customerview.totalEnergy}</p>
                                    		</div>
                                    	</div>
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper3">
                                    <p>Production Date : ${customerview.productionDate}</p>
                                 </div>
                              </div>