<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="card card-default bg-default slide-left" id="allsites" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">total sites</div>
                                    <div class="card-title pull-right"><i class="fa fa-globe font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block">
                                   
                                    <div class="col-md-12 padd-0">
                                    	<div class="col-md-6 col-xs-6 padd-0">
                                    	 <h3 class="m-t-10">
                                       <span class="semi-bold" id="totalsites"><strong>${customerview.totalSiteCount}</strong></span>
                                    </h3>
                                    <a href="#QuickLinkWrapper4">
                                    		<div id="sitenavRooftop" class="col-md-12 padd-0 sitetype">
                                    		<div class="col-md-8	 col-xs-7 padd-0 ">
                                    			<span class="rooftop-color"></span>
                                    			<p style="color:#6a6c6f;">Rooftop</p>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<p>-</p>
                                    		</div>
                                    		<div class="col-md-3 col-xs-4 padd-0">
                                    			<p id="rooftop" style="color:#6a6c6f;">${customerview.rooftopCount}</p>
                                    		</div>
                                    	</div>
                                    	</a>
                                    	 <a href="#QuickLinkWrapper4">
                                    	<div id="sitenavUtility" class="col-md-12 padd-0 uitlity m-t-10 sitetype">
                                    		<div class="col-md-8 col-xs-7 padd-0">
                                    		<span class="utility-color"></span>
                                    			<p style="color:#6a6c6f;">Utility</p>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<p>-</p>
                                    		</div>
                                    		<div class="col-md-3 col-xs-4 padd-0">
                                    			<p id="utilitycount" style="color:#6a6c6f;">${customerview.utilityCount}</p>
                                    		</div>
                                    	</div>
                                    	</a>
                                    	</div>
                                    	
                                    	<div class="col-md-6 col-xs-6 padd-0">
                                    		<div id="chart" style="min-width: 90%; height: 130px; max-width: 180px; margin: 0 auto"></div> 
                                    	</div>
                                    </div>
                                    
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper2">
                               <p>Last Down Time : ${customerview.lastDownTime}</p> 
                                 </div>
                              </div>