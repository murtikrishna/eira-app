<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

										
										<div class="card card-default bg-default slide-right"
											id="divsitesensors" data-pages="card">
											<div class="h-150">
												<div class="card-header ">
													<div class="card-title pull-left">Sensors</div>
													<div class="card-title pull-right">
														<img src="resources/img/ImageResize_03.png">
													</div>
												</div>
												<div class="card-block">
													<h3>

														<c:if test="${not empty siteview.irradiation}">
															<c:choose>
																<c:when test="${siteview.irradiation==''}">
																	<span class="semi-bold"><strong>-</strong></span>
																</c:when>
																<c:when test="${siteview.irradiation=='-'}">
																	<span class="semi-bold"><strong>-</strong></span>
																</c:when>

																<c:otherwise>
																	<span class="semi-bold"><strong>${siteview.irradiation}
																			W/m<sup>2</sup>
																	</strong></span>
																</c:otherwise>

															</c:choose>

														</c:if>
													</h3>
													<p>Irradiation</p>
													<%-- <p>${siteview.irradiationLastUpdate}</p> --%>
												</div>
											</div>
											<div class="card-footer">
												<p>Module Temperature : ${siteview.moduleTemp}</p>
											</div>
										</div>