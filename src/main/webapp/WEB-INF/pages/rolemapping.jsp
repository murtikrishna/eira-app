<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet"
	rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/bootstrap.min3.3.37.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min3.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/loader.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css"
	href="resources/css/jquery-ui.css">
<link href="resources/css/formValidation.min.css" rel="stylesheet"
	type="text/css">
<link
	href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css"
	rel="stylesheet">
<!--  <link href="resources/css/jquery-ui_cal.css" rel="stylesheet" type="text/css"> -->

<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>

<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script type="text/javascript" src="resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="resources/js/formValidation.min.js"></script>
<script type="text/javascript"
	src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.val.js"></script>

<script type="text/javascript" src="resources/js/toastr.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/toastr.css">
<link rel="stylesheet" type="text/css"
	href="http://codeseven.github.com/toastr/toastr.css">




 <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script> 
 

<style type="text/css">
.dropdown-menu-right {
	left: -70px !important;
}


.toggle.btn{
width:90px !important;} 
 

.ui.search.dropdown>input.search {
	background: none transparent !important;
	border: none !important;
	box-shadow: none !important;
	cursor: text;
	top: 0em;
	left: -2px;
	width: 100%;
	outline: none;
	-webkit-tap-highlight-color: rgba(255, 255, 255, 0);
	padding: inherit;
}

.txtheight {
	min-height: 28px;
	height: 20px;
}

div.dataTables_wrapper div.dataTables_length select {
	width: 50px;
	display: inline-block;
	margin-left: 3px;
	margin-right: 4px;
}

.ui.form
textarea:not([rows])
{
height:4em;
min-height:4em;
max-height:24em;
}
.ui.icon.input>i.icon:before, .ui.icon.input>i.icon:after {
	left: 0;
	position: absolute;
	text-align: center;
	top: 36%;
	width: 100%;
}

.evtDropdown {
	/* min-height: 2.7142em !important; 
font-size:13px !important;
padding: 0.78571429em 2.1em 0.78571429em 1em !important;  */
	
}

.input-sm, .form-horizontal .form-group-sm .form-control {
	font-size: 13px;
	min-height: 25px;
	height: 32px;
	padding: 8px 9px;
}

#example td a {
	color: #337ab7;
	font-weight: bold;
	cursor: pointer;
}
#example1 td a {
	color: #337ab7;
	font-weight: bold;
	cursor: pointer;
}

.dt-button-background {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 0 !important;
}

.dropevent {
	min-width: 100px;
	margin-left: -35px;
}

.dropevent li a {
	color: #000 !important;
	font-weight: normal !important;
}

.tbh-width {
	width: 150px;
	padding-right: 30px !important;
}

.content-bg {
	background: #FFF;
}

.dt-buttons {
	margin-left: 50px;
}

.buttons-collection {
	height: 28px;
	padding: 3px;
	/*  z-index:9999; */
}

.buttons-excel {
	background: rgba(36, 95, 68, 0.87);
	color: #FFF;
}

.buttons-pdf {
	background: rgb(98, 165, 96);
	color: #FFF;
}

.dataTables_filter {
	float: right;
}

.dataTables_length {
	float: left;
}

.ui.selection.dropdown {
	width: 100%;
	margin-top: 0.1em;
	/*  height: 2.3em; */
}

.ui.search.dropdown .menu {
	max-height: 12.02857143rem !important;
}

.ui.selection.dropdown .menu {
	width: 100%;
	white-space: normal !important;
}

.editbtn {
	cursor: pointer;
}

.ui.selection.dropdown .menu .item:first-child {
	border-top: 1px solid #ddd !important;
	min-height: 2.8em;
}

.ui.selection.dropdown .icon {
	text-align: right;
	margin-left: 7px !important;
}

.mstl-label {
	margin-top: 10px;
}

.rem-padd {
	padding: 0px;
}
</style>
</head>
<body class="fixed-header">
	<div class="theme-loader">
		<div class="loader-track">
			<div class="loader-bar"></div>
		</div>
	</div>

	<input type="hidden" value="${access.userID}" id="hdneampmuserid">
	<input type="hidden" value="${classtate}" id="hdnstate">

	<jsp:include page="slider.jsp" />




	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					<a href="./dashboard"><img src="resources/img/logo01.png"
						alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx tktSearch"
					id="SearcSElect">

					<div class="ui  search selection  dropdown  width oveallsearch">
						<input id="myInput" name="tags" type="text">
						<div class="default text">search</div>

						<div id="myDropdown" class="menu">

							<jsp:include page="searchselect.jsp" />


						</div>
					</div>



				</div>

				<div class="dropdown pull-right hidden-md-down user-log">
					<button class="profile-dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 circular inline"> <img
							src="resources/img/user_01.png" alt
							data-src="resources/img/user_01.png"
							data-src-retina="resources/img/user_01.png" width="32"
							height="32">
						</span>

						<c:if test="${not empty access}">
							<p class="user-login">${access.userName}</p>
						</c:if>
					</button>
					<div class="dropdown-menu dropdown-menu-right profile-dropdown"
						role="menu">
						<a class="dropdown-item disablecolor"><i
							class="pg-settings_small"></i> Settings</a> <a
							class="dropdown-item disablecolor"><i class="pg-outdent"></i>
							Feedback</a> <a class="dropdown-item disablecolor"><i
							class="pg-signals"></i> Help</a> <a href="logout1"
							class="dropdown-item color logout"><i class="pg-power"></i>
							Logout</a>
					</div>
				</div>


				<div class="newticketpopup hidden">
					<span data-toggle="modal" data-target="#tktCreation"
						data-backdrop="static" data-keyboard="false"><p
							class="center m-t-5 tkts">
							<img class="m-t-5" src="resources/img/tkt.png">
						<p class="create-tkts">New Ticket</p>
						</p></span>

				</div>



			</div>
		</div>
		<div class="page-content-wrapper" id="QuickLinkWrapper1">

			<div class="content ">

				<div class="container-fixed-lg">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="./dashboard"><i
								class="fa fa-home mt-7"></i></a></li>
						<li class="breadcrumb-item"><a>Configuration</a></li>
						<li class="breadcrumb-item active">Role Mapping Configuration</li>
					</ol>
				</div>
				<div id="newconfiguration" class="">
					<div class="card card-transparent mb-0">
						<!-- Table Start-->
						<div class="padd-3 sortable">
							
								<div class="row">
									<div class="col-lg-12 col-xs-12 col-md-12 padd-0"
										id="datadetails">
										<div class="card bg-default" data-pages="card"
											style="margin-bottom: 3px;">
											<div class="card card-transparent " style="padding: 5px 5px;">
												<ul class="nav nav-tabs nav-tabs-fillup"
													data-init-reponsive-tabs="dropdownfx">
													<li class="nav-item"><a href="#" class="active"
													data-toggle="tab" data-target="#slide1" id="ClickShow1"><span>User Role</span></a></li>
												<li class="nav-item"><a href="#tab1" data-toggle="tab"
													data-target="#slide2" id="ClickShow2"><span>Role Mapping</span></a></li>
													
												</ul>
												<div class="tab-content">
												
												
												<div class="tab-pane active" id="slide1">
													<form method="post" class="ui form noremal1"
														style="margin-bottom: 0px;">
														<div class="col-md-12   m-t-5 padd-0">
															<div class="col-md-3 field">
																<label>Role ID</label> <input type="hidden"
																	id="hdnRoleId" value="Auto-Generation" /> <label
																	id="lblUserroleCode">Auto-Generation</label>
															</div>

															<div class="col-md-3">
																<div class="field required m-t-6">
																	<label>Role Name</label>
																</div>
																<div class="field">
																	<input type="text" name="txtRoleName"
																		autocomplete="off" id="txtRoleName"
																		placeholder="Role Name">
																</div>
															</div>

															<div class="col-md-3 ">
																<div class="field required  m-t-6">
																	<label>Description</label>
																</div>
																<div class="field">
																	<input type="text" name="txtRoleDescription"
																		autocomplete="off" id="txtRoleDescription"
																		placeholder="Role Designation">
																</div>
															</div>

															<div class="col-md-3">
																<div class="field required m-t-6">
																	<label>Role Status</label>
																</div>
																<div class="field">
																	<select class="ui search selection dropdown"
																		name="ddlUserRoleStatus" id="ddlUserRoleStatus">
																		<option value="">Select</option>
																	</select>
																</div>
															</div>

														</div>


														<div class="col-md-12 center m-t-30 ">
															<div class="btn btn-success submit confi-btn-width"
																id="btnMasterSubmit">Submit</div>
															<div class="btn btn-primary clear m-l-15 confi-btn-width"
																id="btnMasterClear">Reset</div>
														</div>
													</form>
												</div>
												
													<div class="tab-pane slide-left" id="slide2">
														<div class="col-md-12   m-t-5 padd-0">
															<div class="col-md-4"></div>
															<div class="col-md-1 ">
																<label style="margin-top:5px">Role Name</label>
															</div>
															<div class="col-md-2">
																<!-- <div class="field required m-t-6">
																	<label>Role Name</label>
																</div> -->
																<div class="field">
																	<form:select class="field ui fluid search selection dropdown"
																		id="rolesCombo" path="roleDropDown"
																		items="${roleDropDown}">
																	</form:select>

																</div>
															</div>

															<div class="col-md-5"></div>

														</div>


														<!-- 1 -->
														<div class="col-md-12 m-t-10 padd-0">
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label> Dashboard</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" id="Dashboard"
																			class="mbt-toggle" dbid="1" data-toggle="toggle"
																			data-on="Enabled" data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Customer View</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="2" id="CustomerView"
																			class="mbt-toggle" data-toggle="toggle"
																			data-on="Enabled" data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>SiteView</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="3" id="SiteView"
																			class="mbt-toggle" data-toggle="toggle"
																			data-on="Enabled" data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Tickets</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="4" id="Tickets"
																			class="mbt-toggle" data-toggle="toggle"
																			data-on="Enabled" data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>


														</div>


														<!-- 2 -->
														<div class="col-md-12 m-t-10 padd-0">
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Events</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="5" id="Events"
																			class="mbt-toggle" data-toggle="toggle"
																			data-on="Enabled" data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>New Ticket</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="6" id="NewTicket"
																			class="mbt-toggle" data-toggle="toggle"
																			data-on="Enabled" data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Analysis</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="7" id="Analysis"
																			class="mbt-toggle" data-toggle="toggle"
																			data-on="Enabled" data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Documents Upload</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="8" id="DocumentsUpload"
																			class="mbt-toggle" data-toggle="toggle"
																			data-on="Enabled" data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>

														</div>

														<!-- 3 -->
														<div class="col-md-12 m-t-10 padd-0">
														
														
														<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Documents Download</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="20" id="DocumentsDownload"
																			class="mbt-toggle" data-toggle="toggle"
																			data-on="Enabled" data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Reports</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="9" id="Reports"
																			class="mbt-toggle" data-toggle="toggle"
																			data-on="Enabled" data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Customer Config</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="10"
																			id="CustomerConfiguration" class="mbt-toggle"
																			data-toggle="toggle" data-on="Enabled"
																			data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Site Config</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="11"
																			id="SiteConfiguration" class="mbt-toggle"
																			data-toggle="toggle" data-on="Enabled"
																			data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															

														</div>
														<!-- 4 -->
														<div class="col-md-12 m-t-10 padd-0">
														
														<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Equipment Category Config</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="12"
																			id="EquipmentCategoryConfiguration"
																			class="mbt-toggle" data-toggle="toggle"
																			data-on="Enabled" data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Equipment Type Config</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="13"
																			id="EquipmentTypeConfiguration" class="mbt-toggle"
																			data-toggle="toggle" data-on="Enabled"
																			data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Equipment Config</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="14"
																			id="EquipmentConfiguration" class="mbt-toggle"
																			data-toggle="toggle" data-on="Enabled"
																			data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Standard Parameter Config</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="15"
																			id="StandardParameterConfiguration"
																			class="mbt-toggle" data-toggle="toggle"
																			data-on="Enabled" data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															

														</div>
														<!-- 5 -->
														<div class="col-md-12 m-t-10 padd-0">
														
														<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Error Code Config</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="16"
																			id="ErrorCodeConfiguration" class="mbt-toggle"
																			data-toggle="toggle" data-on="Enabled"
																			data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Master Upload</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="21"
																			id="MasterUpload" class="mbt-toggle"
																			data-toggle="toggle" data-on="Enabled"
																			data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															
															
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>User Config</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="18"
																			id="UserConfiguration" class="mbt-toggle"
																			data-toggle="toggle" data-on="Enabled"
																			data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Role Access Config</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="19"
																			id="RoleAccessConfiguration" class="mbt-toggle"
																			data-toggle="toggle" data-on="Enabled"
																			data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															

														</div>
														
													   <div class="col-md-12 m-t-10 padd-0">

															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>SOP Config</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="22"
																			id="SOP" class="mbt-toggle"
																			data-toggle="toggle" data-on="Enabled"
																			data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Status Config</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="23"
																			id="StatusConfiguration" class="mbt-toggle"
																			data-toggle="toggle" data-on="Enabled"
																			data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Historic Upload</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="24"
																			id="HistoricUpload" class="mbt-toggle"
																			data-toggle="toggle" data-on="Enabled"
																			data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Datalogger Config</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="26"
																			id="DataloggerConfiguration" class="mbt-toggle"
																			data-toggle="toggle" data-on="Enabled"
																			data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															
															</div>  
															
															
															 <div class="col-md-12 m-t-10 padd-0">

															<div class="col-md-3 ">
																<div class="col-md-12 rem-padd">
																	<div class="col-md-6">
																		<label>Data Collection</label>
																	</div>
																	<div class="col-md-5">
																		<input type="checkbox" dbid="25"
																			id="DataCollection" class="mbt-toggle"
																			data-toggle="toggle" data-on="Enabled"
																			data-off="Disabled">
																	</div>
																	<div class="col-md-1"></div>
																</div>
															</div>
															<div class="col-md-3 ">
																
															</div>
															<div class="col-md-3 ">
																
															</div>
															<div class="col-md-3 ">
																
															</div>
															
															</div>  



														<div class="col-md-12 center m-t-30 ">
															<div class="btn btn-success submit confi-btn-width"
																id="btnSubmit" onclick="submit()">Submit</div>
															<div
																class="btn btn-primary mbt-clear m-l-15 confi-btn-width"
																id="btnClear" onclick="reset()">Reset</div>
														</div>
													</div>
													
													
													
													
												
												
												</div>
											</div>
										</div>
									</div>
								</div>
						</div>
					

					</div>


					<!-- Table End-->

					<!-- Table Start-->
					
					<div class="sortable" id="equipmentdata1">
						<div class="row">
							<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
								<div class="card card-default bg-default" data-pages="card"
									style="margin-bottom: 3px;">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6 col-xs-6" style="padding: 0px">
												<span class="m-l-5 title-head"
													style="text-decoration: none!importatnt; float: left; margin-right: 0px;"
													href=""><b>User Role</b></span>

												<!-- <p class="m-l-5 title-head"><b>Events</b></p> -->
											</div>
											<div class="col-md-6 col-xs-6">
												<%-- <a class="m-l-5 title-head" style="text-decoration:none !importatnt;float:right;margin-right:10px;"  href="./siteeventskpi${access.mySiteID}"><b>Events</b></a>	
											 --%>
											</div>
										</div>
									</div>
									<div class="padd-5 table-responsive">
										<table id="example1"
											class="table table-striped table-bordered" width="100%"
											cellspacing="0">
											<thead>
												<tr>
													<th style="width: 10%;">Role ID</th>

													<th style="width: 20%;">Role Name</th>
													<th style="width: 15%;">Role Description</th>
													<th style="width: 10%;">UserRole Status</th>
													<th style="width: 10%;">Edit</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="padd-3 sortable" id="equipmentdata">
						<div class="row" id="QuickLinkWrapper2">
							<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails"
								style="height: 260px;">
								<div class="card card-default bg-default" data-pages="card">

									<div class="padd-5 table-responsive">
										<table id="example" class="table table-striped table-bordered"
											width="100%" cellspacing="0">
											<thead>
												<tr>
												
												<th style="width:20%;">Role Name</th>

													<th style="width:70%;">Access Pages</th>
													
													<th style="width: 10%;">Edit</th>
													
													
													<!--  <th style="width:8%;">De-activate</th> -->
												</tr>
											</thead>
											<tbody>



											</tbody>
										</table>


									</div>
								</div>
							</div>


						</div>

					</div>
					
					
					


					<!-- Table End-->
				</div>
			</div>


		</div>

		<div class="container-fixed-lg footer mb-0">
			<div class="container-fluid copyright sm-text-center">
				<p class="small no-margin pull-left sm-pull-reset">
					<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
						CLEAN ENERGY</span>. <span class="hint-text">All rights reserved.
					</span>
				</p>
				<p class="small no-margin pull-rhs sm-pull-reset">
					<span class="hint-text">Powered by</span> <span>MESTECH
						SERVICES PVT LTD</span>.
				</p>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	</div>

	<!-- Side Bar Content Start-->




	<div class="quickview-wrapper  builder hidden-sm hidden-xs"
		id="builder">
		<div class="p-l-10 p-r-10 ">
			<a class="builder-close quickview-toggle pg-close"></a> <a
				class="builder-toggle" data-toggle="quickview"
				data-toggle-element="#builder"><img
				src="resources/img/ImageResize_06.png"></a>
			<ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary"
				id="builderTabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#tabLayouts" role="tab"
					aria-controls="home"><span>Quick Link</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active " id="tabLayouts" role="tabcard">
					<div class="scrollable">
						<div class="p-r-50">
							<div class="list">
								<ul>
									<!-- 	<li><a href="#QuickLinkWrapper1">Ticket Filters</a></li> -->
									<li><a href="#QuickLinkWrapper2">List Of Role Access</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>






	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min3.js"
		type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>









	<div id="gotop"></div>

	<!-- Share Popup End !-->
	<!-- Modal -->
	<div id="tktCreation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header Popup-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="resources/img/close.png">
					</button>
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<p class="modal-title tkt-title">
						<img src="resources/img/tkt.png"><span class="tkt-title">New
							Ticket</span>
					</p>
				</div>
				<div class="modal-body">
					<c:url var="addAction" value="/addnewticket"></c:url>
					<div class="col-md-12">
						<p class="tkt-mandat">
							<span class="errormess">*</span> All fields are mandatory
						</p>
					</div>
					<form:form action="${addAction}" modelAttribute="ticketcreation"
						class="ui form validation-form">

						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width SiteNames"
									id="ddlSite" name="ddlSite" path="siteID" onchange="getEquipmentAgainstSite()">
									<form:option value="" tabindex="0">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>
						
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                                                <div class="col-md-4 col-xs-12">
                                                       <label class="fields required m-t-10">Equipment</label>
                                                </div>

                                                <div class="col-md-8 col-xs-12">
                                                       <form:select
                                                              class="field ui fluid search selection dropdown width equipments"
                                                              id="ddlEquipment" path="equipmentID" name="ddlEquipment">
                                                              <option value="">Select</option>
                                                       </form:select>
                                                </div>
                          </div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Type</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width evtDropdown"
									id="ddlType" name="ddlType" path="ticketType">
									<form:option value="">Select</form:option>
									<form:option value="Operation">Operation</form:option>
									<form:option value="Maintenance">Maintenance</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Category</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width category evtDropdown"
									id="ddlCategory" name="ddlCategory" path="ticketCategory">
									<form:option value="">Select </form:option>
									<form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
									<form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
									<form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
									<form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
									<form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
									<form:option data-value="Operation"
										value="Equipment Replacement">Equipment Replacement</form:option>
									<form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
									<form:option data-value="Operation" value="String Down">String Down</form:option>
									<form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
									<form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
									<form:option data-value="Operation" value="">Select</form:option>
									<form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
									<form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="DataLogger Cleaning">DataLogger Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="String Current Measurement">String Current Measurement</form:option>
									<form:option data-value="Maintenance"
										value="Preventive Maintenance">Preventive Maintenance</form:option>

									<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
									<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
									<form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
									<form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
									<form:option data-value="Maintenance" value="">Select</form:option>
								</form:select>
							</div>

						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Subject</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<form:input placeholder="Subject" name="Subject" type="text"
										autocomplete="off" id="txtSubject" path="ticketDetail"
										maxLength="50" />
								</div>
							</div>
						</div>

						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Priority</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width evtDropdown"
									id="ddlPriority" name="user" path="priority">
									<form:option value="">Select </form:option>
									<form:option data-value="3" value="3">High</form:option>
									<form:option data-value="2" value="2">Medium</form:option>
									<form:option data-value="1" value="1">Low</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Description</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<textarea id="txtTktdescription" autocomplete="off"
										name="description" style="resize: none;"
										placeholder="Ticket Description" maxLength="120"></textarea>
								</div>

							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
							<div class="btn btn-success  submit" id="btnCreate">Create</div>
							<button class="btn btn-success" type="button" id="btnCreateDummy"
								tabindex="7" style="display: none;">Create</button>
							<div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
						</div>
						<!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
						<!-- <div class="ui error message"></div> -->
					</form:form>
					<!-- <div class="ui error message"></div> -->

				</div>
			</div>
		</div>
	</div>
	<!-- Address Popup End !-->
	<!-- Modal -->
	<div id="evntCreation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header Popup-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="resources/img/close.png">
					</button>
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<p class="modal-title tkt-title">
						<img src="resources/img/tkt.png"><span class="tkt-title">Event
							Ticket Creation</span>
					</p>
				</div>
				<div class="modal-body">
					<c:url var="addAction" value="/addnewticket"></c:url>
					<div class="col-md-12">
						<p class="tkt-mandat">All fields are mandatory</p>
					</div>
					<!-- <span class="errormess">*</span> -->
					<form:form action="${addAction}" modelAttribute="ticketcreation"
						class="ui form validation-form">

						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<p class="fields  m-t-10">Site</p>
							</div>
							<div class="col-md-8 col-xs-12">
								<p class="fields  m-t-10" id="txtSiteName" path="siteID"></p>
							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<p class="fields  m-t-10">Type</p>
							</div>
							<div class="col-md-8 col-xs-12">
								<p class="fields  m-t-10" id="txtTypeName" path="ticketType">Operation</p>
							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<p class="fields  m-t-10">Priority</p>
							</div>
							<div class="col-md-8 col-xs-12">
								<p class="fields  m-t-10" id="txtPriority" path="priority"></p>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Category</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width category evtDropdown"
									id="ddlCategory" name="ddlCategory" path="ticketCategory">
									<form:option value="">Select </form:option>
									<form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
									<form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
									<form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
									<form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
									<form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
									<form:option data-value="Operation"
										value="Equipment Replacement">Equipment Replacement</form:option>
									<form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
									<form:option data-value="Operation" value="String Down">String Down</form:option>
									<form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
									<form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
									<form:option data-value="Operation" value="">Select</form:option>
									<form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
									<form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="DataLogger Cleaning">DataLogger Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="String Current Measurement">String Current Measurement</form:option>
									<form:option data-value="Maintenance"
										value="Preventive Maintenance">Preventive Maintenance</form:option>

									<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
									<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
									<form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
									<form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
									<form:option data-value="Maintenance" value="">Select</form:option>
								</form:select>
							</div>

						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Subject</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<form:input placeholder="Subject" name="Subject" type="text"
										autocomplete="off" id="txtSubject" path="ticketDetail"
										maxLength="50" />
								</div>
							</div>
						</div>


						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Description</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<textarea id="txtTktdescription" autocomplete="off"
										name="description" style="resize: none;"
										placeholder="Ticket Description" maxLength="120"></textarea>
								</div>

							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
							<div class="btn btn-success  submit">Create</div>

							<div class="btn btn-primary clear m-l-15">Reset</div>
						</div>
						<!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
						<!-- <div class="ui error message"></div> -->
					</form:form>
					<!-- <div class="ui error message"></div> -->

				</div>
			</div>
		</div>
	</div>
	<!-- Address Popup End !-->
	<script>
		$('#gotop').gotop({
			customHtml : '<i class="fa fa-angle-up fa-2x"></i>',
			bottom : '2em',
			right : '2em'
		});
	</script>
	<script>
		$(document).ready(function() {
			
			$('#example').DataTable(
					{
						"lengthMenu" : [ [ 15, 25, 50, -1 ],
								[ 15, 25, 50, "All" ] ]
					});
			$(window).fadeThis({
				speed : 500,
			});
			
			
			

			toastr.options = {
				"debug" : false,
				"positionClass" : "toast-bottom-right",
				"progressBar" : true,
				"onclick" : null,
				"fadeIn" : 500,
				"fadeOut" : 100,
				"timeOut" : 1000,
				"extendedTimeOut" : 2000
			}
			
			$('#equipmentdata').hide();
			$('#ClickShow2').click(function() {
				$('#equipmentdata1').hide();
				$('#equipmentdata').show();
			});
			$('#ClickShow1').click(function() {
				$('#equipmentdata').hide();
				$('#equipmentdata1').show();

			});

			
			var classstate = $('#hdnstate').val();
			
			$("#configurationid").addClass(classstate);
			$("#roleaccessconfigid").addClass(classstate);
			
		
			$('#example1').DataTable(
					{
						"lengthMenu" : [ [ 10, 25, 50, -1 ],
								[ 10, 25, 50, "All" ] ]
					});
			
			
		
			
			
			  $('.ui.dropdown').dropdown({forceSelection:false}); 
	       	   
	       	 $("#searchSelect").change(function() {
	             var value = $('#searchSelect option:selected').val();
	             var uid =  $('#hdneampmuserid').val();
	           redirectbysearch(value,uid);
	        }); $('.dataTables_filter input[type="search"]').attr('placeholder','search');

	        $('.close').click(function(){
                $('#tktCreation').hide();
                $('.clear').click();
                $('.category').dropdown();
                $('.SiteNames').dropdown();
            });
			$('.clear').click(function(){
			    $('.search').val("");
			});

			$('#ddlType').change(function(){              
                $('.category').dropdown('clear')
               });
			
		


			AjaxCallForUserrolePageLoad();
			
		});
	</script>



	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$(".theme-loader").animate({
								opacity : "0"
							}, 60000);
						

							$(".theme-loader").show();
							$
									.ajax({
										type : 'GET',
										url : './getRolePermissions',
										success : function(msg) {
											var roleaccess = msg.data;
											for (var i = 0; i < roleaccess.length; i++) {
												//$('#example').DataTable().row.add([siteList[i].siteCode,siteList[i].siteTypeName,siteList[i].siteName,siteList[i].customerName,siteList[i].stateName,siteList[i].countryName,activeFlag,'<a  href="editsite' + siteList[i].siteId + '">Edit</a>','<a href="removesite' + siteList[i].siteId + '">De-activate</a>']).draw(false);
												$('#example').DataTable().row
														.add(
																[
																		roleaccess[i].roleName,
																		roleaccess[i].accessScreens,
																		'<span class="editbtn btn btn-primary" id="btnedit">Edit</span>' ])
														.draw(false);

											}
											$(".theme-loader").hide();

										},
										error : function(msg) {
											//alert(0); alert(msg);
										}
									});
						});
	</script>

	<script type="text/javascript">
		$(document)
				.on(
						'click',
						'.editbtn',
						function() {
							$('.mbt-toggle').bootstrapToggle('off');
							var te = $(this).closest('tr').find('td').eq('0')
									.text();
							
							$('#rolesCombo').dropdown('set selected',$('#rolesCombo option:contains('+te+')').val());
							var s = $(this).closest('tr').find('td').eq('1')
									.text();
							s = s.replace(/\s/g, "");
							var strArr = s.split(",");
							for (i = 0; i < strArr.length; i++) {
								$('#' + strArr[i]).bootstrapToggle('on');
							}
						});
	</script>


	<script type="text/javascript">
	 function reset() {
			$('.mbt-toggle').bootstrapToggle('off');
			 $('#rolesCombo').dropdown('clear');
		} 
		function submit() {
			$(".theme-loader").show();
			var roleId = $('#rolesCombo').val();
			var activityArray = new Array();
			$('.mbt-toggle').each(function() {
				if ($(this).prop('checked')) {
					var activityId = $(this).attr("dbid");
					activityArray.push(parseInt(activityId));
				}

			});
			var roleMap = {};
			roleMap['roleId'] = parseInt(roleId);
			roleMap['activityId'] = activityArray;
			console.log(roleMap);
			var siteJson = JSON.stringify(roleMap);
			$.ajax({
				type : 'POST',
				contentType : 'application/json',
				url : './updateRoleMapping',
				data : siteJson,
				dataType : "json",
				success : function(msg) {
					if (msg.status == "Success") {
						$(".theme-loader").hide();
						toastr.info("Role access granted successfully..");
						$('.mbt-toggle').bootstrapToggle('off');
						$('#rolesCombo').val('null');
						location.reload();
					} else {
						toastr.error("Failed to grant access,try again..");
						$(".theme-loader").hide();
					}

				},
				error : function(msg) {
					$(".theme-loader").hide();
					toastr.error("Failed to grant access,try again..");
				}
			});

		}
	</script>




</body>
<script type="text/javascript">
              function getEquipmentAgainstSite() {
                     var siteId = $('#ddlSite').val();
                    
                     $.ajax({
                           type : 'GET',
                           url : './equipmentsBysites?siteId=' + siteId,
                           success : function(msg) {
                        	   
                        	   var EquipmentList = msg.equipmentlist;
                                  
                                         for (i = 0; i < EquipmentList.length; i++) {
                                                $('#ddlEquipment').append(
                                                              "<option value="+EquipmentList[i].equipmentId+">" + EquipmentList[i].customerNaming
                                                                           + "</option>");
                                         }

                           },
                           error : function(msg) {
                                  console.log(msg);
                           }
                     });
              }
              
              
             
       </script>
       
       <script type='text/javascript'>//<![CDATA[
         $(window).load(function(){
        	 $('.ui.dropdown').dropdown({forceSelection:false});
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
         
         $('.ui.dropdown').dropdown({
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: true, 
             showOnFocus: true,
             allowLabels:false,
             on: "click" 
           });

		$('.ui.dropdown.oveallsearch').dropdown({
             onChange: function (value, text, $selectedItem) {
                
                var uid =  $('#hdneampmuserid').val();
                redirectbysearchvalue(value,uid);
             },
             fullTextSearch: true,
             forceSelection: false, 
             selectOnKeydown: false, 
             showOnFocus: true,
             on: "click" 
           });
           
           
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );//]]> 


            var validation  = {
                    txtSubject: {
                               identifier: 'txtSubject',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               },
                               {
                                   type: 'maxLength[50]',
                                   prompt: 'Please enter a value'
                                 }]
                             },
                              ddlSite: {
                               identifier: 'ddlSite',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                             },
                            ddlType: {
                                  identifier: 'ddlType',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                            ddlCategory: {
                               identifier: 'ddlCategory',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter a value'
                               }]
                            },
                             ddlPriority: {
                               identifier: 'ddlPriority',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please select a dropdown value'
                               }]
                             },
                             description: {
                               identifier: 'description',
                               rules: [{
                                 type: 'empty',
                                 prompt: 'Please Enter Description'
                               }]
                             }
             };
            
            
            var validation1 = {
    				
    				ddlUserRoleStatus : {
    					identifier : 'ddlUserRoleStatus',
    					rules : [ {
    						type : 'empty',
    						prompt : 'Enter Role'
    					} ]
    				},
    				txtRoleDescription : {
    					identifier : 'txtRoleDescription',
    					rules : [ {
    						type : 'empty',
    						prompt : 'Enter Short Name'
    					}, {
    						type : 'regExp[/^[ A-Za-z]*$/]',
    						prompt : 'Enter Numbers Only'
    					}, {
    						type : 'minLength[1]',
    						prompt : 'Short Name Must Be at Least 1 Characters'
    					}, {
    						type : 'maxLength[30]',
    						prompt : 'Short Name Must Be at Least 10 Characters'
    					} ]
    				},

    				txtRoleName : {
    					identifier : 'txtRoleName',
    					rules : [ {
    						type : 'empty',
    						prompt : 'Enter UserName'
    					}, {
    						type : 'regExp[/^[ A-Za-z0-9()]*$/]',
    						prompt : 'Enter Numbers Only'
    					}, {
    						type : 'minLength[1]',
    						prompt : 'UserName Must Be at Least 1 Characters'
    					}, {
    						type : 'maxLength[100]',
    						prompt : 'UserName Must Be at Least 100 Characters'
    					} ]
    				},

    			};
            var settings1 = {
    				onFailure : function() {
    					toastr.warning("Validation Failure");
    					// alert('fail');
    					return false;
    				},
    				onSuccess : function() {
    					//alert('Success');
    					AjaxCallForUserroleSaveOrUpdate();
    					return false;

    				}
    			};
    		
               var settings = {
                 onFailure:function(){
                     return false;
                   }, 
                 onSuccess:function(){    
                   $('#btnCreate').hide();
                   $('#btnCreateDummy').show()
                   //$('#btnReset').attr("disabled", "disabled");          
                   }};           
               $('.ui.form.validation-form').form(validation,settings);
           	$('.ui.form.noremal1').form(validation1, settings1);
           	
           	
         })
         
      </script>
      
      
      <script type="text/javascript">
	

	function AjaxCallForUserrolePageLoad() {

		$
				.ajax({
					type : 'GET',
					url : './userrole',
					success : function(msg) {

						var status = msg.status;
						var siteList = msg.siteList;
						var userrolestatus = msg.activeStatusList;
						var userroleList = msg.userroleList;

						$.map(userrolestatus, function(val, key) {
							$("#ddlUserRoleStatus").append(
									"<option value="+ key +">" + val
											+ "</option>");
						});

						$.map(siteList, function(val, key) {
							$("#ddlSite").append(
									"<option value="+ key +">" + val
											+ "</option>");
						});

						for (var i = 0; i < userroleList.length; i++) {

							var activeFlag = userroleList[i].activeFlag;
							if (activeFlag != null && activeFlag == 1) {
								activeFlag = "Active";
							} else {
								activeFlag = "In-active";
							}

							$('#example1').DataTable().row
									.add(
											[
													userroleList[i].roleId,
													userroleList[i].roleName,
													userroleList[i].roleDescription,
													activeFlag,
													'<a href="#QuickLinkWrapper1" class="editbtn btn btn-primary" id="btnedit" onclick="AjaxCallForFetchUserrole('
															+ userroleList[i].roleId
															+ ')">Edit</a>' ])
									.draw(false);

						}

					},
					error : function(msg) {
						//alert(0); alert(msg);
					}
				});
	}

	

	function AjaxCallForUserroleSaveOrUpdate() {

		var varRoleId = null;

		if ($('#hdnRoleId').val() != null
				&& $('#hdnRoleId').val() != "Auto-Generation") {
			varRoleId = $('#hdnRoleId').val();
			/* varUserCode = $('#lblUserCode').text(); */
		}

		var UserRoleData = [ {

			"roleId" : varRoleId,
			"roleName" : $("#txtRoleName").val(),
			"roleDescription" : $("#txtRoleDescription").val(),

			"activeFlag" : $("#ddlUserRoleStatus option:selected").val(),
			"creationDate" : null,

			"lastUpdatedDate" : null

		} ];

		$.ajax({
			type : 'POST',
			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			url : './newuserrole',
			data : JSON.stringify(UserRoleData),
			success : function(msg) {

				var errormessage = msg.Errorlog1;

				if (errormessage != null && errormessage != "") {

					toastr.error(errormessage);
				} else {

					if ($('#hdnRoleId').val() != null
							&& $('#hdnRoleId').val() != "Auto-Generation") {

					} else {

					}

					$('#hdnRoleId').val('Auto-Generation');
					var btnText = $('#btnMasterSubmit').text();

					if (btnText == 'Save') {
						toastr.info("Successfully Created");
					} else {
						toastr.info("Successfully Updated");
					}

					$('.clear').click();

				}

			},
			error : function(msg) {
				toastr.error("There was a some error. Please Contect admin");
				//alert(0); alert(msg);
			}
		});
	}

	

	function AjaxCallForFetchUserrole(roleId) {

		$.ajax({
			type : 'POST',
			url : './edituserrole',
			data : {
				'RoleId' : roleId
			},
			success : function(msg) {

				var userrole = msg.userrole;

				$('#hdnRoleId').val(userrole.roleId);
				$("#txtRoleName").val(userrole.roleName)
				$("#txtRoleDescription").val(userrole.roleDescription)

				$("#ddlUserRoleStatus").val(userrole.activeFlag).change();

				$('#btnMasterSubmit').text('Update');

			},
			error : function(msg) {

				//alert(0); alert(msg);
			}
		});

	}
</script>

      

</html>


