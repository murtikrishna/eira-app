<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />





<!-- <link href="resources/css/pages3.css" class="main-stylesheet" rel="stylesheet" type="text/css" />
<link href="resources/css/styles3.css" rel="stylesheet" type="text/css">
<link href="resources/css/semantic.min3.css" rel="stylesheet" type="text/css">
<link href="resources/css/bootstrap.min3.3.37.css" type="text/css" rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min3.css" type="text/css" rel="stylesheet">



<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>
<script type="text/javascript" src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script> -->


  <link href="resources/css/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/dashboard.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/select2.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/pages-icons.css" rel="stylesheet" type="text/css">
      <link href="resources/css/animate.css" rel="stylesheet"  />
      <link href="resources/css/pages.css" class="main-stylesheet"  rel="stylesheet" type="text/css"/>
      <link href="resources/css/styles.css" rel="stylesheet" type="text/css">
      <link href="resources/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
      <link href="resources/css/site.css" rel="stylesheet" >      
      <link href="resources/css/bootstrap.min.3.37.css" type="text/css" rel="stylesheet">
      <link href="resources/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="resources/css/semantic.min.css">
      <script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
      <script src="resources/js/searchselect.js" type="text/javascript"></script>
      <script src="resources/js/controller/dashboard.js" type="text/javascript"></script>



<script type="text/javascript" src="resources/js/semantic.min.js"></script>

<script type="text/javascript" src="resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="resources/js/formValidation.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.val.js"></script>

<script type="text/javascript">
      $(document).ready(function(){
    	/*   alert($(window).height());
     	 alert($(window).width()); */
    	  
    	    $(".theme-loader").animate({
    	        opacity: "0"
    	    },1000);
    	    setTimeout(function() {
    	        $(".theme-loader").remove();
    	    }, 800);

     
    	$('#ddlSite').dropdown('clear');
      	$('#ddlType').dropdown('clear');
      	$('#ddlCategory').dropdown('clear');
      	$('#ddlPriority').dropdown('clear');
      	$('#txtSubject').val('');
      	$('#txtTktdescription').val('');
      	
      	var classstate = $('#hdnstate').val();
		
		$("#dashboardid").addClass(classstate);
      	
      	
    	  $("#ddlSiteId").prop("selectedIndex", 1); 
    	  $("#ddlEquipmentId").prop("selectedIndex", 1);
      	/* $("#ddlEquipmentId").val($("#ddlEquipmentId option:first").val()); */
    	/*   $('.dt-buttons').attr('disabled',true); */
    	 /*  $('#btnView').attr("disabled",true); */
    	  
    	  $("#txtFromDate").datepicker({                  	
           	dateFormat:'dd/mm/yy',
           	maxDate: new Date(),
           	maxDate: "0",
                  onSelect: function(selected) {
                    $("#txtToDate").datepicker("option","minDate", selected)                 
                  }
              });
           $("#txtToDate").datepicker({
           	
           	dateFormat:'dd/mm/yy',
           	maxDate: new Date(),
           	maxDate: "0",
           	onSelect: function(selected) {                                
               $("#txtFromDate").datepicker("option","maxDate", selected) 
           	}
            });       
    	/*  
    	  $("#txtFromDate").datepicker({ dateFormat: "dd/mm/yy"});
    	     $("#txtToDate").datepicker({ dateFormat: "dd/mm/yy"});
    	     */ 
    	     
    	     
    	  if($(window).width() < 767)
			{
			   $('.card').removeClass("slide-left");
			   $('.card').removeClass("slide-right");
			   $('.card').removeClass("slide-top");
			   $('.card').removeClass("slide-bottom");
			}
    	 /*  $('.dt-buttons').css("border","10px solid #ccc"); */
    	  
    	  
            $('.close').click(function(){
                $('#tktCreation').hide();
                $('.clear').click();
                $('.category').dropdown();
                $('.SiteNames').dropdown();
            });
    	 
    	 
			$('.clear').click(function(){
			      	$('.search').val("");
			      });
			
			$('#ddlType').change(function(){              
                $('.category').dropdown('clear')
               });

			
			$('body').click(function(){
			          $('#builder').removeClass('open'); 
			        });


    	  $('.clears').click(function(){
    		  
    		 	var sCount = document.getElementById("ddlSiteId").length;
               	var sid = document.getElementById("ddlSiteId").value ;
               	var uid = $('#hdneampmuserid').val();
               	
               /* 	alert(sCount);
               	alert(sid); 
               	 */
             	var varhref =  './dataexport';
               	
               	if(sCount==2)
               	{
               	 	varhref =  './dataexport' + ${access.mySiteID} + '&ref=' + uid;
               	}
    		  
    		
    		  
    		  $("#txtFromDate").val('');
    	      $("#txtToDate").val('');
    	      $(".text").empty();
    	      $(".text").addClass('default')
    	      $(".text").html("Select");
    	      $('#ddlSiteId option:selected').removeAttr('selected');
            	 $('#ddlEquipmentId option:selected').removeAttr('selected');
            window.location.href = varhref;
            	//window.location.href = './dataexport'; 
    	  });
    	 

      });
    </script>
<script type='text/javascript'>	//<![CDATA[
	$(window).load(function() {
		$.fn.dropdown.settings.selectOnKeydown = false;
		$.fn.dropdown.settings.forceSelection = false;


		$('.ui.dropdown').dropdown({
			fullTextSearch : true,
			forceSelection : false,
			selectOnKeydown : true,
			showOnFocus : true,
			on : "click"
		});

		$('.ui.dropdown.oveallsearch').dropdown({
			onChange : function(value, text, $selectedItem) {
				var uid = $('#hdneampmuserid').val();
				redirectbysearchvalue(value, uid);
			},
			fullTextSearch : true,
			forceSelection : false,
			selectOnKeydown : false,
			showOnFocus : true,
			on : "click"
		});

		var $ddlType = $('#ddlType'),
			$ddlCategory = $('#ddlCategory'),
			$options = $ddlCategory.find('option');
		$ddlType.on('change', function() {
			$ddlCategory.html($options.filter('[data-value="' + this.value + '"]'));
		}).trigger('change'); //]]> 


		var validation = {
			txtSubject : {
				identifier : 'txtSubject',
				rules : [ {
					type : 'empty',
					prompt : 'Please enter a value'
				},
					{
						type : 'maxLength[50]',
						prompt : 'Please enter a value'
					} ]
			},
			ddlSite : {
				identifier : 'ddlSite',
				rules : [ {
					type : 'empty',
					prompt : 'Please enter a value'
				} ]
			},
			ddlType : {
				identifier : 'ddlType',
				rules : [ {
					type : 'empty',
					prompt : 'Please enter a value'
				} ]
			},
			ddlCategory : {
				identifier : 'ddlCategory',
				rules : [ {
					type : 'empty',
					prompt : 'Please enter a value'
				} ]
			},
			ddlPriority : {
				identifier : 'ddlPriority',
				rules : [ {
					type : 'empty',
					prompt : 'Please select a dropdown value'
				} ]
			},
			description : {
				identifier : 'description',
				rules : [ {
					type : 'empty',
					prompt : 'Please Enter Description'
				} ]
			}
		};
		var settings = {
			onFailure : function() {
				return false;
			},
			onSuccess : function() {
				$('#btnCreate').hide();
				$('#btnCreateDummy').show()
			//$('#btnReset').attr("disabled", "disabled");          
			}
		};
		$('.ui.form.validation-form').form(validation, settings);

	})
</script>


<style type="text/css">
.dropdown-menu {
	left: 50px !important;
	margin-left: 0px !important;
}

.dt-button-collection {
	height: 140px;
	overflow-y: scroll;
}

.dropdown-menu>li>a {
	display: block;
	padding: 3px 10px;
	clear: both;
	font-weight: 400;
	line-height: 1.42857143;
	color: #333;
	white-space: nowrap;
}

.buttons-excel {
	height: 29px;
}

.ui
.form
 
textarea
:not
 
(
[
rows
]
 
)
{
height
:
 
4
em
;

	
min-height
:
 
4
em
;

	
max-height
:
 
24
em
;


}
.has-error .input-group-addon {
	color: #a94442;
	background-color: #eee !important;
	border-color: transparent;
}

.dt-button-background {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 0 !important;
}

.tbh-width {
	width: 150px;
	padding-right: 30px !important;
}

.dt-buttons {
	margin-left: 50px;
}

.buttons-collection {
	height: 28px;
	padding: 3px;
	/*  z-index:9999; */
}

.input-sm, .form-horizontal .form-group-sm .form-control {
	font-size: 13px;
	min-height: 25px;
	height: 32px;
	padding: 8px 9px;
}

.buttons-excel {
	background: rgba(36, 95, 68, 0.87);
	color: #FFF;
}

.buttons-pdf {
	background: rgb(98, 165, 96);
	color: #FFF;
}

.dataTables_filter {
	float: right;
}

.dataTables_length {
	float: left;
}

.larginleft {
	margin-left: 150px;
}
</style>


<script type="text/javascript">
	$(document).ready(function() {

		var today = new Date();
		var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
		var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
		var dateTime = date + ' ' + time;
		$("#searchSelect").change(function() {
			var value = $('#searchSelect option:selected').val();
			var uid = $('#hdneampmuserid').val();
			redirectbysearch(value, uid);
		});
		var table = $('#example').DataTable({
			lengthChange : true,
			"scrollX" : true,

			"scrollX" : true,
			/* buttons: ['excel', 'pdf', 'colvis' ] */

			buttons : [
				/*  {
				     extend: 'print',
				     exportOptions: {
				         columns: ':visible'
				     },
				 }, */
				{
					extend : 'excel',
					text : '<i class="fa fa-file-excel-o excel"></i><i class="fa fa-download ml-15" aria-hidden="true"></i>',
					filename : 'Equipment Details' + dateTime,
					titleAttr : 'Excel',
					exportOptions : {
						columns : ':visible'
					},
				},


				'colvis'
			],
		});

		table.buttons().container()
			.appendTo('#example_wrapper .col-sm-6:eq(0)');

		$('.dataTables_filter input[type="search"]').attr('placeholder', 'search');

	});
</script>





</head>
<body class="fixed-header">

	<input type="hidden" value="${access.userID}" id="hdneampmuserid">
<input type="hidden" value="${classtate}" id="hdnstate">

	<jsp:include page="slider.jsp" />

	


	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					<a href="./dashboard"><img src="resources/img/logo01.png"
						alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx" id="SearcSElect">


					<div class="ui  search selection  dropdown  width oveallsearch">
						<input id="myInput" name="tags" type="text">
						<div class="default text">search</div>
						<div id="myDropdown" class="menu">

						<jsp:include page="searchselect.jsp" />	


						</div>
					</div>



				</div>
				<div class="dropdown pull-right hidden-md-down user-log">
					<button class="profile-dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 circular inline"> <img
							src="resources/img/user_01.png" alt
							data-src="resources/img/user_01.png"
							data-src-retina="resources/img/user_01.png" width="32"
							height="32">
						</span>

						<!-- <p class="user-login">ICE</p>
                   -->

						<c:if test="${not empty access}">
							<p class="user-login">${access.userName}</p>
						</c:if>



					</button>
					<div
						class="dropdown-menu dropdown-menu-right profile-dropdown exportlog"
						role="menu">
						<a class="dropdown-item disablecolor"><i
							class="pg-settings_small"></i> Settings</a> <a
							class="dropdown-item disablecolor"><i class="pg-outdent"></i>
							Feedback</a> <a class="dropdown-item disablecolor"><i
							class="pg-signals"></i> Help</a> <a href="logout1"
							class="dropdown-item color"><i class="pg-power"></i> Logout</a>
					</div>
				</div>

				
					<div class="newticketpopup hidden">
						<span data-toggle="modal" data-target="#tktCreation"
							data-backdrop="static" data-keyboard="false"><p
								class="center m-t-5 tkts">
								<img class="m-t-5" src="resources/img/tkt.png">
							<p class="create-tkts">New Ticket</p>
							</p></span>
						<!-- <p class="center mb-1 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img src="resources/img/tkt.png"></p> -->
						<!--  <p class="center mb-1 tkts"><img src="resources/img/tkt.png"></p>
                  <p class="create-tkts">New Ticket</p> -->
					</div>
				
				<!--  <span ><p class="center m-t-5 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img class="m-t-5" src="resources/img/tkt.png"><p class="create-tkts">New Ticket</p></p></span> -->
				<!-- <a href="#" class="header-icon  btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><i class="fa fa-ticket fa-2x"></i></a> -->
				<!-- <a href="#" class="header-icon pg pg-alt_menu btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a> -->
			</div>
		</div>
		<div class="page-content-wrapper ">
			<div class="content">
				<div class="container-fixed-lg">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="./dashboard"><i
								class="fa fa-home mt-7"></i></a></li>
						<li class="breadcrumb-item hover-idle"><a>Visualization</a></li>
						<c:if test="${not empty access}">
							<c:choose>
								<c:when test="${access.customerListView == 'visible'}">
									<li class="breadcrumb-item"><a
										href="./customerview${access.myCustomerID}">${access.myCustomerName}</a></li>
								</c:when>
								<c:otherwise>
									<li class="breadcrumb-item"><a href="./dashboard">${access.myCustomerName}</a></li>
								</c:otherwise>
							</c:choose>
						</c:if>
						<li class="breadcrumb-item"><a
							href="./siteview${access.mySiteID}">${access.mySiteName}</a></li>
						<li class="breadcrumb-item active">Download Equipment Details</li>
					</ol>
				</div>



				<div id="QuickLinkWrpper1" class="">
					<div class="card card-transparent mb-0">
						<!-- Table Start-->

						<div class="padd-3 sortable mb-80" id="equipmentdata">
							<div class="row">
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
									<div class="card card-default bg-default" data-pages="card">
										<div class="card-header min-hei-20">
											<div class="card-title  tbl-head">Equipment Details</div>
										</div>
										<c:url var="viewAction"
											value="/dataexport${access.mySiteID}&ref=${access.userID}"></c:url>
										<form:form action="${viewAction}" modelAttribute="dataexport"
											class="ui form validation-form " method="post"
											style="margin-bottom:0px;">
											<div class="col-md-12 col-xs-12 col-lg-12 padd-0">
												<div class="col-md-1 col-xl-1 ">
													<div class="form-group   field">
														<label class="fields required m-t-5">Site Name<span
															class="errormess">*</span></label>
													</div>
												</div>
												<div class="col-md-2 col-xl-2">
													<form:select
														class="ui fluid search selection dropdown width dataSite"
														name="ddlSiteId" id="ddlSiteId" path="siteID">
														<form:option value="" label="Select" />
														<form:options items="${site1List}" />
													</form:select>
												</div>
												<div class="col-md-1 col-xl-1  mrs-40">
													<div class="form-group  field">
														<label class="fields required m-t-5">Equipment<span
															class="errormess">*</span></label>
													</div>
												</div>
												<div class="col-md-2 col-xl-2">
													<form:select
														class="ui fluid search selection dropdown width dataSite"
														name="ddlEquipmentId" id="ddlEquipmentId"
														path="equipmentID">
														<form:option site="-2" value="" label="Select" />
														<form:option site="-2" value="0" label="All" />
														<form:options site="7" items="${equipmentList}" />
													</form:select>
													<%-- <form:select class="field ui fluid search selection dropdown width" id="ddlEquipmentId" name="ddlEquipmentId" path="equipmentID">
                 
                        <form:option value="0">All</form:option>
                        <form:options  items="${siteList}" />
                    </form:select> --%>
												</div>
												<div class="col-md-1 col-xl-1  mrs-40">
													<div class="form-group  field">
														<label class="fields  m-t-5">From Date <span
															class="errormess">*</span></label>
													</div>
												</div>
												<div class="col-md-2 col-xl-2">
													<div class="controls">
														<div class="input-group">
															<form:input id="txtFromDate" type="text"
																placeholder="From Date" class="date-picker form-control" autocomplete="off"
																name="fromDate" path="fromDate" required="required" />
															<label for="txtFromDate" class="input-group-addon btn"><span
																class="glyphicon glyphicon-calendar"></span> </label>
														</div>
													</div>
												</div>
												<div class="col-md-1 col-xl-1  mrs-20">
													<div class="txtwidth">
														<label class="fields  m-t-5">To Date <span
															class="errormess">*</span></label>
													</div>
												</div>
												<div class="col-md-2 col-xl-2">
													<div class="controls">
														<div class="input-group">
															<form:input id="txtToDate" type="text"
																placeholder="To Date" class="date-picker form-control" autocomplete="off"
																name="toDate" path="toDate" required="required" />
															<label for="txtToDate" class="input-group-addon btn"><span
																class="glyphicon glyphicon-calendar"></span> </label>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-12 col-lg-12 center">
												<input type="submit"
													class="btn btn-success m-t-20 profileForm btn-data-export-width" value="View">
												 <input type="button" class="btn btn-primary  clears  m-t-20 btn-data-export-width"
													value="Clear">
											</div>
										</form:form>
										<!-- <div class="row">
     				       					<div class="col-md-12 col-lg-12 center">
                                  	       <input type="submit" class="btn btn-success m-t-20 profileForm" value="View">
                                          <input type="button"   class="btn btn-primary  clears  m-t-20" value="Clear"> 
                                  		</div>
     				       				</div> -->
										<div class="padd-5 ">
											<table id="example"
												class="table table-striped table-bordered"
												style="width: 100%">
												<thead>
													<tr>
														<th>Equipment Serial No</th>
														<th>Time stamp</th>
														<th>Input Current-01 (A)</th>
														<th>Input Current-02 (A)</th>
														<th>Input Current-03 (A)</th>
														<th>Input Current-04 (A)</th>
														<th>Input Current-05 (A)</th>
														<th>Input Current-06 (A)</th>
														<th>Input Current-07 (A)</th>
														<th>Input Current-08 (A)</th>
														<th>Input Current-09 (A)</th>
														<th>Input Current-10 (A)</th>
														<th>Input Voltage-01 (V)</th>
														<th>Input Voltage-02 (V)</th>
														<th>Input Voltage-03 (V)</th>
														<th>Input Voltage-04 (V)</th>
														<th>Input Voltage-05 (V)</th>
														<th>Input Voltage-06 (V)</th>
														<th>Input Voltage-07 (V)</th>
														<th>Input Voltage-08 (V)</th>
														<th>Input Voltage-09 (V)</th>
														<th>Input Voltage-10 (V)</th>
														<th>Input Power-01 (W)</th>
														<th>Input Power-02 (W)</th>
														<th>Input Power-03 (W)</th>
														<th>Input Power-04 (W)</th>
														<th>Input Power-05 (W)</th>
														<th>Input Power-06 (W)</th>
														<th>Input Power-07 (W)</th>
														<th>Input Power-08 (W)</th>
														<th>Input Power-09 (W)</th>
														<th>Input Power-10 (W)</th>
														<th>Phase Current (A)</th>
														<th>Phase Current-L1 (A)</th>
														<th>Phase Current-L2 (A)</th>
														<th>Phase Current-L3 (A)</th>
														<th>Phase Voltage (V)</th>
														<th>Phase Voltage-L1 (V)</th>
														<th>Phase Voltage-L2 (V)</th>
														<th>Phase Voltage-L3 (V)</th>
														<th>Apparent Power (VA)</th>
														<th>Active Power (W)</th>
														<th>Re-active Power (VAr)</th>
														<th>Power Factor</th>
														<th>Total Energy (kWh)</th>
														<th>Today Energy (kWh)</th>
														<th>Isolation Resistance (Ohm)</th>
														<th>Output Frequency (Hz)</th>
														<th>Ambient Temperature (�C)</th>
														<th>Module Temperature (�C)</th>
														<th>Inverter Temperature (�C)</th>
														<th>Wind Speed (m/s)</th>
														<th>Rain Fall</th>
														<th>Total Hours On (h)</th>
														<th>Today Hours On (h)</th>
														<th>Phase Power Balancer</th>
														<th>Differential Current (mA)</th>
														<th>Status</th>
														<th>Error Code</th>
														<th>Irradiation</th>
														
														<th>PhaseToPhaseVoltage_L1_L2 (V)</th>
														<th>PhaseToPhaseVoltage_L1_L3 (V)</th>
														<th>PhaseToPhaseVoltage_L3_L1 (V)</th>
														<th>HeatSinkTemp (�C)</th>
														<th>ACBreakerCount</th>
														<th>DCBreakerCount</th>
														<th>ApparentPower_L1 (kVA)</th>
														<th>ApparentPower_L2 (kVA)</th>
														<th>ApparentPower_L3 (kVA)</th>
														<th>ActivePower_L1 (kW)</th>
														<th>ActivePower_L2 (kW)</th>
														<th>ActivePower_L3 (kW)</th>
														<th>ReactivePower_L1 (kVAr)</th>
														<th>ReactivePower_L2 (kVAr)</th>
														<th>ReactivePower_L3 (kVAr)</th>
														<th>PowerFactor_L1</th>
														<th>PowerFactor_L2</th>
														<th>PowerFactor_L3</th>
														<th>ExportTotalEnergy (kWh)</th>
														<th>ImportTotalEnergy (kWh)</th>
														<th>ExportActiveEnergy (kWh)</th>
														<th>ImportActiveEnergy (kWh)</th>
														<th>ReactiveEnergyLAG (kVAr)</th>
														<th>ReactiveEnergyLEAD (kVAr)</th>
														<th>WindDirection (�)</th>
														<th>GobalIrradiance (W/m2)</th>
														<th>Humidity (%)</th>
														<th>TrackerAngle (�)</th>
														<th>SunAngle (�)</th>
														<th>BatteryVoltage (V)</th>
														<th>StartRoboPosition</th>
														<th>MidRoboPosition</th>
														<th>EndRoboPosition</th>
														<th>PhaseFrequency_L1 (Hz)</th>
														<th>PhaseFrequency_L2 (Hz)</th>
														<th>PhaseFrequency_L3 (Hz)</th>
														<th>PhasePower_L1 (kW)</th>
														<th>PhasePower_L2 (kW)</th>
														<th>PhasePower_L3 (kW)</th>
														<th>PhaseEnergy_L1 (kWh)</th>
														<th>PhaseEnergy_L2 (kWh)</th>
														<th>PhaseEnergy_L3 (kWh)</th>
														<th>InputCurrent_11 (A)</th>
														<th>InputCurrent_12 (A)</th>
														<th>InputCurrent_13 (A)</th>
														<th>InputCurrent_14 (A)</th>
														<th>InputCurrent_15 (A)</th>
														<th>InputCurrent_16 (A)</th>
														<th>InputCurrent_17 (A)</th>
														<th>InputCurrent_18 (A)</th>
														<th>InputCurrent_19 (A)</th>
														<th>InputCurrent_20 (A)</th>
														<th>InputCurrent_21 (A)</th>
														<th>InputCurrent_22 (A)</th>
														<th>InputCurrent_23 (A)</th>
														<th>InputCurrent_24 (A)</th>
														<th>InputVoltage_11 (V)</th>
														<th>InputVoltage_12 (V)</th>
														<th>InputVoltage_13 (V)</th>
														<th>InputVoltage_14 (V)</th>
														<th>InputVoltage_15 (V)</th>
														<th>InputVoltage_16 (V)</th>
														<th>InputVoltage_17 (V)</th>
														<th>InputVoltage_18 (V)</th>
														<th>InputVoltage_19 (V)</th>
														<th>InputVoltage_20 (V)</th>
														<th>InputVoltage_21 (V)</th>
														<th>InputVoltage_22 (V)</th>
														<th>InputVoltage_23 (V)</th>
														<th>InputVoltage_24 (V)</th>
														<th>InputPower_11 (W)</th>
														<th>InputPower_12 (W)</th>
														<th>InputPower_13 (W)</th>
														<th>InputPower_14 (W)</th>
														<th>InputPower_15 (W)</th>
														<th>InputPower_16 (W)</th>
														<th>InputPower_17 (W)</th>
														<th>InputPower_18 (W)</th>
														<th>InputPower_19 (W)</th>
														<th>InputPower_20 (W)</th>
														<th>InputPower_21 (W)</th>
														<th>InputPower_22 (W)</th>
														<th>InputPower_23 (W)</th>
														<th>InputPower_24 (W)</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${datatransaction}" var="datalsit">
														<tr>
															<td>${datalsit.equipmentID}</td>
															<td>${datalsit.timestamp}</td>
															<td>${datalsit.inputCurrent_01}</td>
															<td>${datalsit.inputCurrent_02}</td>
															<td>${datalsit.inputCurrent_03}</td>
															<td>${datalsit.inputCurrent_04}</td>
															<td>${datalsit.inputCurrent_05}</td>
															<td>${datalsit.inputCurrent_06}</td>
															<td>${datalsit.inputCurrent_07}</td>
															<td>${datalsit.inputCurrent_08}</td>
															<td>${datalsit.inputCurrent_09}</td>
															<td>${datalsit.inputCurrent_10}</td>
															<td>${datalsit.inputVoltage_01}</td>
															<td>${datalsit.inputVoltage_02}</td>
															<td>${datalsit.inputVoltage_03}</td>
															<td>${datalsit.inputVoltage_04}</td>
															<td>${datalsit.inputVoltage_05}</td>
															<td>${datalsit.inputVoltage_06}</td>
															<td>${datalsit.inputVoltage_07}</td>
															<td>${datalsit.inputVoltage_08}</td>
															<td>${datalsit.inputVoltage_09}</td>
															<td>${datalsit.inputVoltage_10}</td>
															<td>${datalsit.inputPower_01}</td>
															<td>${datalsit.inputPower_02}</td>
															<td>${datalsit.inputPower_03}</td>
															<td>${datalsit.inputPower_04}</td>
															<td>${datalsit.inputPower_05}</td>
															<td>${datalsit.inputPower_06}</td>
															<td>${datalsit.inputPower_07}</td>
															<td>${datalsit.inputPower_08}</td>
															<td>${datalsit.inputPower_09}</td>
															<td>${datalsit.inputPower_10}</td>
															<td>${datalsit.phaseCurrent}</td>
															<td>${datalsit.phaseCurrent_L1}</td>
															<td>${datalsit.phaseCurrent_L2}</td>
															<td>${datalsit.phaseCurrent_L3}</td>
															<td>${datalsit.phaseVoltage}</td>
															<td>${datalsit.phaseVoltage_L1}</td>
															<td>${datalsit.phaseVoltage_L2}</td>
															<td>${datalsit.phaseVoltage_L3}</td>
															<td>${datalsit.apparentPower}</td>
															<td>${datalsit.activePower}</td>
															<td>${datalsit.reactivePower}</td>
															<td>${datalsit.powerFactor}</td>
															<td>${datalsit.totalEnergy}</td>
															<td>${datalsit.todayEnergy}</td>
															<td>${datalsit.isolationResistance}</td>
															<td>${datalsit.outputFrequency}</td>
															<td>${datalsit.ambientTemperature}</td>
															<td>${datalsit.moduleTemperature}</td>
															<td>${datalsit.inverterTemperature}</td>
															<td>${datalsit.windSpeed}</td>
															<td>${datalsit.rainfall}</td>
															<td>${datalsit.totalHoursOn}</td>
															<td>${datalsit.todayHoursOn}</td>
															<td>${datalsit.phasePowerBalancer}</td>
															<td>${datalsit.differentialCurrent}</td>
															<td>${datalsit.status}</td>
															<td>${datalsit.errorCode}</td>
															<td>${datalsit.irradiation}</td>
															
															<td>${datalsit.phaseToPhaseVoltage_L1_L2}</td>
															<td>${datalsit.phaseToPhaseVoltage_L1_L3}</td>
															<td>${datalsit.phaseToPhaseVoltage_L3_L1}</td>
															<td>${datalsit.heatSinkTemp}</td>
															<td>${datalsit.acBreakerCount}</td>
															<td>${datalsit.dcBreakerCount}</td>
															<td>${datalsit.apparentPower_L1}</td>
															<td>${datalsit.apparentPower_L2}</td>
															<td>${datalsit.apparentPower_L3}</td>
															<td>${datalsit.activePower_L1}</td>
															<td>${datalsit.activePower_L2}</td>
															<td>${datalsit.activePower_L3}</td>
															<td>${datalsit.reactivePower_L1}</td>
															<td>${datalsit.reactivePower_L2}</td>
															<td>${datalsit.reactivePower_L3}</td>
															<td>${datalsit.powerFactor_L1}</td>
															<td>${datalsit.powerFactor_L2}</td>
															<td>${datalsit.powerFactor_L3}</td>
															<td>${datalsit.exportTotalEnergy}</td>
															<td>${datalsit.importTotalEnergy}</td>
															<td>${datalsit.exportActiveEnergy}</td>
															<td>${datalsit.importActiveEnergy}</td>
															<td>${datalsit.reactiveEnergyLAG}</td>
															<td>${datalsit.reactiveEnergyLEAD}</td>
															<td>${datalsit.windDirection}</td>
															<td>${datalsit.gobalIrradiance}</td>
															<td>${datalsit.humidity}</td>
															<td>${datalsit.trackerAngle}</td>
															<td>${datalsit.sunAngle}</td>
															<td>${datalsit.batteryVoltage}</td>
															<td>${datalsit.startRoboPosition}</td>
															<td>${datalsit.midRoboPosition}</td>
															<td>${datalsit.endRoboPosition}</td>
															<td>${datalsit.phaseFrequency_L1}</td>
															<td>${datalsit.phaseFrequency_L2}</td>
															<td>${datalsit.phaseFrequency_L3}</td>
															<td>${datalsit.phasePower_L1}</td>
															<td>${datalsit.phasePower_L2}</td>
															<td>${datalsit.phasePower_L3}</td>
															<td>${datalsit.phaseEnergy_L1}</td>
															<td>${datalsit.phaseEnergy_L2}</td>
															<td>${datalsit.phaseEnergy_L3}</td>
															<td>${datalsit.inputCurrent_11}</td>
															<td>${datalsit.inputCurrent_12}</td>
															<td>${datalsit.inputCurrent_13}</td>
															<td>${datalsit.inputCurrent_14}</td>
															<td>${datalsit.inputCurrent_15}</td>
															<td>${datalsit.inputCurrent_16}</td>
															<td>${datalsit.inputCurrent_17}</td>
															<td>${datalsit.inputCurrent_18}</td>
															<td>${datalsit.inputCurrent_19}</td>
															<td>${datalsit.inputCurrent_20}</td>
															<td>${datalsit.inputCurrent_21}</td>
															<td>${datalsit.inputCurrent_22}</td>
															<td>${datalsit.inputCurrent_23}</td>
															<td>${datalsit.inputCurrent_24}</td>
															<td>${datalsit.inputVoltage_11}</td>
															<td>${datalsit.inputVoltage_12}</td>
															<td>${datalsit.inputVoltage_13}</td>
															<td>${datalsit.inputVoltage_14}</td>
															<td>${datalsit.inputVoltage_15}</td>
															<td>${datalsit.inputVoltage_16}</td>
															<td>${datalsit.inputVoltage_17}</td>
															<td>${datalsit.inputVoltage_18}</td>
															<td>${datalsit.inputVoltage_19}</td>
															<td>${datalsit.inputVoltage_20}</td>
															<td>${datalsit.inputVoltage_21}</td>
															<td>${datalsit.inputVoltage_22}</td>
															<td>${datalsit.inputVoltage_23}</td>
															<td>${datalsit.inputVoltage_24}</td>
															<td>${datalsit.inputPower_11}</td>
															<td>${datalsit.inputPower_12}</td>
															<td>${datalsit.inputPower_13}</td>
															<td>${datalsit.inputPower_14}</td>
															<td>${datalsit.inputPower_15}</td>
															<td>${datalsit.inputPower_16}</td>
															<td>${datalsit.inputPower_17}</td>
															<td>${datalsit.inputPower_18}</td>
															<td>${datalsit.inputPower_19}</td>
															<td>${datalsit.inputPower_20}</td>
															<td>${datalsit.inputPower_21}</td>
															<td>${datalsit.inputPower_22}</td>
															<td>${datalsit.inputPower_23}</td>
															<td>${datalsit.inputPower_24}</td>



														</tr>
													</c:forEach>
												</tbody>
											</table>
											<%-- <table id="example" class="table table-striped table-bordered" style="width:100%">
        							<thead>
             								<tr>
								            <th>Equipment Serial No</th>
                                             <th>Time stamp</th>
                                             <th>Input Current-01 (A)</th>
                                             <th>Input Current-02 (A)</th>
                                             <th>Input Current-03 (A)</th>
                                             <th>Input Current-04 (A)</th>
                                             <th>Input Current-05 (A)</th>
                                             <th>Input Current-06 (A)</th>
                                             <th>Input Current-07 (A)</th>
                                             <th>Input Current-08 (A)</th>
                                             <th>Input Current-09 (A)</th>
                                             <th>Input Current-10 (A)</th>
                                            <th>Input Voltage-01 (V)</th>
                                             <th>Input Voltage-02 (V)</th>
                                             <th>Input Voltage-03 (V)</th>
                                             <th>Input Voltage-04 (V)</th>
                                             <th>Input Voltage-05 (V)</th>
                                             <th>Input Voltage-06 (V)</th>
                                             <th>Input Voltage-07 (V)</th>
                                             <th>Input Voltage-08 (V)</th>
                                             <th>Input Voltage-09 (V)</th>
                                             <th>Input Voltage-10 (V)</th>
                                             <th>Input Power-01 (W)</th>
                                             <th>Input Power-02 (W)</th>
                                             <th>Input Power-03 (W)</th>
                                             <th>Input Power-04 (W)</th>
                                             <th>Input Power-05 (W)</th>
                                             <th>Input Power-06 (W)</th>
                                             <th>Input Power-07 (W)</th>
                                             <th>Input Power-08 (W)</th>
                                             <th>Input Power-09 (W)</th>
                                             <th>Input Power-10 (W)</th>
                                             <th>Phase Current (A)</th>
                                             <th>Phase Current-L1 (A)</th>
                                             <th>Phase Current-L2 (A)</th>
                                             <th>Phase Current-L3 (A)</th>
                                             <th>Phase Voltage (V)</th>
                                             <th>Phase Voltage-L1 (V)</th>
                                             <th>Phase Voltage-L2 (V)</th>
                                             <th>Phase Voltage-L3 (V)</th>
                                             <th>Apparent Power (VA)</th>
                                             <th>Active Power (W)</th>
                                             <th>Re-active Power (VAr)</th>
                                             <th>Power Factor</th>
                                             <th>Total Energy (kWh)</th>
                                             <th>Today Energy (kWh)</th>
                                             <th>Isolation Resistance (Ohm)</th>
                                             <th>Output Frequency (Hz)</th>
                                             <th>Ambient Temperature (Â°C)</th>
                                             <th>Module Temperature (Â°C)</th>
                                             <th>Inverter Temperature (Â°C)</th>
                                             <th>Wind Speed (m/s)</th>
                                             <th>Rain Fall</th>
                                             <th>Total Hours On (h)</th>
                                             <th>Today Hours On (h)</th>
                                             <th>Phase Power Balancer</th>
                                             <th>Differential Current (mA)</th>
                                             <th>Status</th>
                                             <th>Error Code</th>
                                             <th>Irradiation</th>
								           </tr>
								        </thead>
								        <tbody>
								           <c:forEach items="${datatransaction}" var="datalsit">
											<tr>                                                             
                                             <td>${datalsit.equipmentID}</td>
                                             <td>${datalsit.timestamp}</td>
                                             <td>${datalsit.inputCurrent_01}</td>
                                             <td>${datalsit.inputCurrent_02}</td>
                                             <td>${datalsit.inputCurrent_03}</td>
                                             <td>${datalsit.inputCurrent_04}</td>
                                             <td>${datalsit.inputCurrent_05}</td>
                                             <td>${datalsit.inputCurrent_06}</td>
                                             <td>${datalsit.inputCurrent_07}</td>
                                             <td>${datalsit.inputCurrent_08}</td>
                                             <td>${datalsit.inputCurrent_09}</td>
                                             <td>${datalsit.inputCurrent_10}</td>
                                             <td>${datalsit.inputVolatge_01}</td>
                                             <td>${datalsit.inputVolatge_02}</td>
                                             <td>${datalsit.inputVolatge_03}</td>
                                             <td>${datalsit.inputVolatge_04}</td>
                                             <td>${datalsit.inputVolatge_05}</td>
                                             <td>${datalsit.inputVolatge_06}</td>
                                             <td>${datalsit.inputVolatge_07}</td>
                                             <td>${datalsit.inputVolatge_08}</td>
                                             <td>${datalsit.inputVolatge_09}</td>
                                             <td>${datalsit.inputVolatge_10}</td>
                                             <td>${datalsit.inputPower_01}</td>
                                             <td>${datalsit.inputPower_02}</td>
                                             <td>${datalsit.inputPower_03}</td>
                                             <td>${datalsit.inputPower_04}</td>
                                             <td>${datalsit.inputPower_05}</td>
                                             <td>${datalsit.inputPower_06}</td>
                                             <td>${datalsit.inputPower_07}</td>
                                             <td>${datalsit.inputPower_08}</td>
                                             <td>${datalsit.inputPower_09}</td>
                                             <td>${datalsit.inputPower_10}</td>
                                             <td>${datalsit.phaseCurrent}</td>
                                             <td>${datalsit.phaseCurrent_L1}</td>
                                             <td>${datalsit.phaseCurrent_L2}</td>
                                             <td>${datalsit.phaseCurrent_L3}</td>
                                             <td>${datalsit.phaseVolatge}</td>
                                             <td>${datalsit.phaseVolatge_L1}</td>
                                             <td>${datalsit.phaseVolatge_L2}</td>
                                             <td>${datalsit.phaseVolatge_L3}</td>
                                             <td>${datalsit.apparentPower}</td>
                                             <td>${datalsit.activePower}</td>
                                             <td>${datalsit.reactivePower}</td>
                                             <td>${datalsit.powerFactor}</td>
                                             <td>${datalsit.totalEnergy}</td>
                                             <td>${datalsit.todayEnergy}</td>
                                             <td>${datalsit.isolationResistance}</td>
                                             <td>${datalsit.outputFrequency}</td>
                                             <td>${datalsit.ambientTemperature}</td>
                                             <td>${datalsit.moduleTemperature}</td>
                                             <td>${datalsit.inverterTemperature}</td>
                                             <td>${datalsit.windSpeed}</td>
                                             <td>${datalsit.rainfall}</td>
                                             <td>${datalsit.totalHoursOn}</td>
                                             <td>${datalsit.todayHoursOn}</td>
                                             <td>${datalsit.phasePowerBalancer}</td>
                                             <td>${datalsit.differentialCurrent}</td>
                                             <td>${datalsit.status}</td>
                                             <td>${datalsit.errorCode}</td>    
                                             <td>${datalsit.irradiation}</td>                                             
                                         </tr>                                                             
                                     </c:forEach>
        </tbody>
    </table> --%>
										</div>
									</div>
								</div>

							</div>
						</div>


						<!-- Table End-->
					</div>
				</div>
			</div>
			<div class="container-fixed-lg footer mb-0">
				<div class="container-fluid copyright sm-text-center">
					<p class="small no-margin pull-left sm-pull-reset">
						<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
							CLEAN ENERGY</span>. <span class="hint-text">All rights reserved.
						</span>
					</p>
					<p class="small no-margin pull-rhs sm-pull-reset">
						<span class="hint-text">Powered by</span> <span>MESTECH
							SERVICES PVT LTD</span>.
					</p>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Side Bar Content Start-->



	<div class="quickview-wrapper  builder hidden-sm hidden-xs"
		id="builder">
		<div class="p-l-10 p-r-10 ">
			<a class="builder-close quickview-toggle pg-close"></a> <a
				class="builder-toggle" data-toggle="quickview"
				data-toggle-element="#builder"><img
				src="resources/img/ImageResize_06.png"></a>
			<ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary"
				id="builderTabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#tabLayouts" role="tab"
					aria-controls="home"><span>Quick Links</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active " id="tabLayouts" role="tabcard">
					<div class="scrollable">
						<div class="p-r-50">
							<div class="list">
								<ul>
									<li><a href="#QuickLinkWrpper1">Equipment Details</a></li>

								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>






	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>

	<script src="resources/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script type="text/javascript"
		src="resources/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript"
		src="resources/js/buttons.bootstrap.min.js"></script>
	<script type="text/javascript" src="resources/js/jszip.min.js"></script>
	<script type="text/javascript" src="resources/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="resources/js/buttons.colVis.min.js"></script>



	<div id="gotop"></div>




	<!-- Address Popup Start !-->



	<!-- Share Popup End !-->
	<!-- Modal -->
	<div id="tktCreation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header Popup-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="resources/img/close.png">
					</button>
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<p class="modal-title tkt-title">
						<img src="resources/img/tkt.png"><span class="tkt-title">New
							Ticket</span>
					</p>
				</div>
				<div class="modal-body">
					<c:url var="addAction" value="/addnewticket"></c:url>
					<div class="col-md-12">
						<p class="tkt-mandat">
							<span class="errormess">*</span> All fields are mandatory
						</p>
					</div>
					<form:form action="${addAction}" modelAttribute="ticketcreation"
						class="ui form validation-form">

					<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width SiteNames"
									id="ddlSite" name="ddlSite" path="siteID" onchange="getEquipmentAgainstSite()">
									<form:option value="" tabindex="0">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>
						
						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
                                                <div class="col-md-4 col-xs-12">
                                                       <label class="fields required m-t-10">Equipment</label>
                                                </div>

                                              <div class="col-md-8 col-xs-12">
                                                       <form:select
                                                              class="field ui fluid search selection dropdown width equipments"
                                                              id="ddlEquipment" path="equipmentID" name="ddlEquipment">
                                                              <option value="">Select</option>
                                                       </form:select>
                                                </div>
                          </div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Type</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlType" name="ddlType" path="ticketType">
									<form:option value="">Select</form:option>
									<form:option value="Operation">Operation</form:option>
									<form:option value="Maintenance">Maintenance</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Category</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width category"
									id="ddlCategory" name="ddlCategory" path="ticketCategory">
									<form:option value="">Select </form:option>
									<form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
									<form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
									<form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
									<form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
									<form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
									<form:option data-value="Operation"
										value="Equipment Replacement">Equipment Replacement</form:option>
									<form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
									<form:option data-value="Operation" value="String Down">String Down</form:option>
									<form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
									<form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
									<form:option data-value="Operation" value="">Select</form:option>
									<form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
									<form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="DataLogger Cleaning">DataLogger Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="String Current Measurement">String Current Measurement</form:option>
									<form:option data-value="Maintenance"
										value="Preventive Maintenance">Preventive Maintenance</form:option>
									<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
									<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
									<form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
									<form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
									<form:option data-value="Maintenance" value="">Select</form:option>
								</form:select>
							</div>

						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Subject</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<form:input placeholder="Subject" name="Subject" type="text" autocomplete="off"
										id="txtSubject" path="ticketDetail" maxLength="50" />
								</div>
							</div>
						</div>

						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Priority</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlPriority" name="user" path="priority">
									<form:option value="">Select </form:option>
									<form:option data-value="3" value="3">High</form:option>
									<form:option data-value="2" value="2">Medium</form:option>
									<form:option data-value="1" value="1">Low</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Description</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<textarea id="txtTktdescription" name="description" autocomplete="off"
										style="resize: none;" placeholder="Ticket Description"
										maxLength="120"></textarea>
								</div>

							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
							<div class="btn btn-success  submit" id="btnCreate">Create</div>
							<button class="btn btn-success" type="button" id="btnCreateDummy"
								tabindex="7" style="display: none;">Create</button>
							<div class="btn btn-primary clear m-l-15" id="btnReset">Reset</div>
						</div>
						<!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
						<!-- <div class="ui error message"></div> -->
					</form:form>
					<!-- <div class="ui error message"></div> -->

				</div>
			</div>
		</div>
	</div>
	<!-- Address Popup End !-->




	<script>
		$('#gotop').gotop({
			customHtml : '<i class="fa fa-angle-up fa-2x"></i>',
			bottom : '2em',
			right : '2em'
		});
	</script>
	<script>
         $(document).ready(function() {
            $(window).fadeThis({
               speed: 500,
            });
         });
      </script>


	<script type='text/javascript'>
       window.onload=function(){
       $('.dropdown').on('show.bs.dropdown', function () {
          var length = parseInt($('.dropdown-menu').css('width'), 10) * -1;
          $('.dropdown-menu').css('left', length);
       });
       }
       </script>

	<script type="text/javascript">
      $(document).ready(function(){
var validation  = {
		
		/* ddlEquipmentId:  {
        identifier: 'ddlEquipmentId',
        rules: [
          {
            type   : 'empty',
            prompt : 'Select Site'
          }
        ]
      }, */
      ddlSiteId: {
                identifier: 'ddlSiteId',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Enter Customer Status'
                    }
                  ] 
              },
             
               };
var settings = {
  onFailure:function(){ 
    alert('fail');
      return false;
    }, 
  onSuccess:function(){    
    alert('Success');
    //AjaxCallForSaveOrUpdate();
    return false; 
    }};
  
$('.ui.form.validation-form').form(validation,settings);
      });
    </script>


</body>

<script type="text/javascript">
function getEquipmentAgainstSite() {
    var siteId = $('#ddlSite').val();
   
    $.ajax({
          type : 'GET',
          url : './equipmentsBysites?siteId=' + siteId,
          success : function(msg) {
       	   
       	   var EquipmentList = msg.equipmentlist;
                 
                        for (i = 0; i < EquipmentList.length; i++) {
                               $('#ddlEquipment').append(
                                             "<option value="+EquipmentList[i].equipmentId+">" + EquipmentList[i].customerNaming
                                                          + "</option>");
                        }

          },
          error : function(msg) {
                 console.log(msg);
          }
    });
}

       </script>
</html>


