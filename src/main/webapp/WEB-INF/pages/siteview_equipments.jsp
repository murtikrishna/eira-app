<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
	
										<div class="card card-default bg-default slide-right"
											id="divsiteequipments" data-pages="card">
											<div class="h-150">
												<div class="card-header ">
													<div class="card-title pull-left">Equipment</div>
													<div class="card-title pull-right">
														<img src="resources/img/ImageResize_02.png">
													</div>
												</div>
												<div class="col-lg-12 col-md-12">
													<div class="col-lg-6 col-xs-6 col-md-6">
														<h3>
															<span class="semi-bold"><strong>
																	${siteview.activeInverters} /
																	${siteview.totalInverters}</strong></span>
														</h3>
														<p>Inverter</p>
													</div>
													<div class="col-lg-6 col-xs-6 col-md-6">

														<h3>


															<span class="semi-bold"> <c:choose>
																	<c:when test="${siteview.energymeterCount == '0'}">
																		<strong> - </strong>
																	</c:when>
																	<c:otherwise>
																		<strong> ${siteview.activeEms} /
																			${siteview.energymeterCount}</strong>

																	</c:otherwise>
																</c:choose>


															</span>

														</h3>

														<p>Energy Meter</p>
													</div>
												</div>
												<%-- <h3>
                                       <span class="semi-bold"><strong>${siteview.activeInverters} / ${siteview.totalInverters}</strong></span>
                                    </h3> --%>
												<div class="col-lg-12 col-md-12 m-t-15">
													<div class="col-lg-6 col-xs-6 col-md-6">
														<p>Error Inverter : ${siteview.errorInverters}</p>
													</div>
													<div class="col-lg-6 col-xs-6 col-md-6">
														<p>Error Meter : ${siteview.errorEms}</p>
													</div>
												</div>
											</div>
											<div class="card-footer">
												<p>Equipment Communication Count :
													${siteview.equipmentCount}</p>
											</div>
										</div>