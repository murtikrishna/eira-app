<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
										<div class="card card-default bg-default slide-right"
											id="divsiteevents" data-pages="card">
											<div class="h-150">
												<div class="card-header">
													<div class="card-title pull-left">events</div>
													<div class="card-title pull-right">
														<i class="fa fa-calendar-check-o font-size icon-color"
															aria-hidden="true"></i>
													</div>
												</div>

												<div class="col-lg-12 col-md-12">
													<div class="col-lg-6 col-xs-6 col-md-6">
														<h3>
															<span class="semi-bold"><strong>${siteview.todayEvents}</strong></span>
														</h3>
														<p>Today</p>
													</div>
													<div class="col-lg-6 col-xs-6 col-md-6">
														<h3>
															<span class="semi-bold"><strong>${siteview.totalEvents}</strong></span>
														</h3>
														<p>Total</p>
													</div>
												</div>

											</div>
											<div class="card-footer">

												 <c:choose>
    										<c:when test="${access.monitoringView == 'visible'}">
    										 <p><a href="./siteeventskpi${siteview.siteID}">Events</a></p>  
    										</c:when> 
    										<c:otherwise>
    										 <p class="events-page hidden"><a href="./siteeventskpi${siteview.siteID}">View all Events</a></p>
    										</c:otherwise> 
    							</c:choose>  

											</div>
										</div>