  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
  <div class="card card-default bg-default slide-right" id="events" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">events</div>
                                    <div class="card-title pull-right"><i class="fa fa-calendar font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block m-t-10">
                                    <div class="col-md-12 padd-0">
                                       <div class="col-lg-6 col-xs-6 col-md-6 padd-0">
                                          <p>Today</p>
                                          <p>${dashboard.todayEventCount}</p>
                                       </div>
                                       <div class="col-lg-6 col-xs-6 col-md-6">
                                          <p>Total</p>
                                          <p>${dashboard.totalEventCount}</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper4">   
                              <c:choose>
    										<c:when test="${access.monitoringView == 'visible'}">
    										 <p><a href="./eventskpi">Events</a></p>  
    										</c:when> 
    										<c:otherwise>
    										
    										 <p class="events-page hidden"><a href="./eventskpi">View all Events</a></p>
    										</c:otherwise> 
    							</c:choose>                           
                                                                      
                                 </div>
                              </div>