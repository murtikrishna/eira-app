<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="card card-default bg-default slide-right"
											id="divsiteschedulejobs" data-pages="card">
											<div class="h-150">
												<div class="card-header ">
													<div class="card-title pull-left">scheduled jobs</div>
													<div class="card-title pull-right">
														<i class="fa fa-calendar font-size icon-color"
															aria-hidden="true"></i>
													</div>
												</div>



												<c:choose>
													<c:when test="${access.monitoringView == 'visible'}">

														<div class="col-lg-12 col-md-12">
															<p>
																<b>Contact</b> <br><span style="text-decoration:underline;color:blue;"><a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=support@eira.io">support@eira.io</a></span>
															</p>
														</div>

													</c:when>
													<c:otherwise>

														<div class="col-lg-12 col-md-12">
															<div class="col-lg-6 col-xs-6 col-md-6">
																<h3>
																	<span class="semi-bold"><strong>${siteview.todayScheduledJobs}</strong></span>
																</h3>
																<p>Today</p>
															</div>
															<div class="col-lg-6 col-xs-6 col-md-6">
																<h3>
																	<span class="semi-bold"><strong>${siteview.todayScheduledCleaningJobs}
																			: ${siteview.todayScheduledPMJobs}</strong></span>
																</h3>
																<p>Ratio</p>
															</div>
														</div>
													</c:otherwise>
												</c:choose>


											</div>

											<c:choose>
												<c:when test="${access.monitoringView == 'visible'}">

													<div class="card-footer" id="QuickLinkWrapper4"
														style="overflow: hidden;">
														<p style="color: white">.</p>
													</div>

												</c:when>
												<c:otherwise>
													<div class="card-footer" id="QuickLinkWrapper4">
														<p>Total : ${siteview.totalScheduledJobs}</p>
													</div>

												</c:otherwise>
											</c:choose>



										</div>