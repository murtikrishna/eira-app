<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="card card-default bg-default slide-right"
											id="divsiteweather" data-pages="card">
											<div class="h-150">
												<div class="card-header ">
													<div class="card-title pull-left">weather</div>
													<div class="card-title pull-right">
														<i class="fa fa-sun-o font-size icon-color" style="margin-left:4px;"></i><br>
														<p>${siteview.weatherStatus}</p>
													</div>
												</div>
												<div class="card-block">
													<h3>
														<span class="semi-bold"><strong>${siteview.weatherTodayTemp}</strong></span>
													</h3>
													<p>Tomorrow : ${siteview.weatherYesterdayTemp}</p>
												</div>
											</div>
											<div class="card-footer" id="QuickLinkWrapper3">
												<p>Wind Speed : ${siteview.windSpeed}</p>
											</div>
										</div>