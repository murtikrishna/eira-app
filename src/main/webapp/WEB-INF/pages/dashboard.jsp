<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To EIRA ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/dashboard.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet"
	rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/bootstrap.min.3.37.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="resources/css/semantic.min.css">
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script src="resources/js/controller/dashboard.js"
	type="text/javascript"></script>
<script src="resources/js/CustomeScript.js" type="text/javascript"></script>
<script src="resources/js/highchart.js"></script>
<script src="https://code.highcharts.com/highcharts-2d.js"></script>

<script type="text/javascript"
	src="https://rawgit.com/Semantic-Org/Semantic-UI/next/dist/semantic.js"></script>



<script type="text/javascript">
	$(document).ready(
			function() {

				$('#ddlSite').dropdown('clear');
				$('#ddlType').dropdown('clear');
				$('#ddlCategory').dropdown('clear');
				$('#ddlPriority').dropdown('clear');
				$('#txtSubject').val('');
				$('#txtTktdescription').val('');

				$('#ddlType').change(function() {
					$('.category').dropdown('clear')
				});

				/* document.onkeydown = function(evt) {
				    evt = evt || window.event;
				    if (evt.keyCode == 27) {
				        alert('Esc key pressed.');
				        $('#tktCreation').modal('hide')
				        $('.modal-backdrop.fade').css("opacity", "0");;
				    }
				};  */

				$('.sitenavigation').click(function() {
					debugger;
					var select = $(this);
					var idSelect = select.attr('id');
					var SelectVal = idSelect.replace(/sitenav/, '');
					var table = $('#example').DataTable();
					//table.search(SelectVal).draw();
					table.column(3).search(SelectVal).draw();

				})

				$('.sitetype').click(function() {
					debugger;
					var select = $(this);
					var idSelect = select.attr('id');
					var SelectVal = idSelect.replace(/sitenav/, '');
					var table = $('#example').DataTable();
					table.search(SelectVal).draw();

				})

				$('#gotop').gotop({
					customHtml : '<i class="fa fa-angle-up fa-2x"></i>',
					bottom : '-5px',
					right : '2em'
				});

				$('#btnCreate').click(function() {
					if ($('#ddlCategory').val == "") {
						alert('Val Empty');
						$('.category ').addClass('error')
					}
				})

				$('.close').click(function() {
					$('.clear').click();
					$('#tktCreation').hide();
					$('.clear').click();
					$('.category').dropdown();
					$('.SiteNames').dropdown();
				});

				$('.clear').click(function() {
					$('.SiteNames').dropdown();
					$('.search').val('');
				});

				$("#searchSelect").change(function() {
					var value = $('#searchSelect option:selected').val();
					var uid = $('#hdneampmuserid').val();
					redirectbysearch(value, uid);
				});

				$('.dataTables_filter input[type="search"]').attr(
						'placeholder', 'search');

				setTimeout(function() {
					AjaxCallForDashBoardDetails();

					LoadChartOnDashboard();
				}, 1000);

				// Ticket Creation Validation //
				var validation = {
					txtSubject : {
						identifier : 'txtSubject',
						rules : [ {
							type : 'empty',
							prompt : 'Please enter a value'
						}, {
							type : 'maxLength[50]',
							prompt : 'Please enter a value'
						} ]
					},
					ddlSite : {
						identifier : 'ddlSite',
						rules : [ {
							type : 'empty',
							prompt : 'Please enter a value'
						} ]
					},
					ddlType : {
						identifier : 'ddlType',
						rules : [ {
							type : 'empty',
							prompt : 'Please enter a value'
						} ]
					},
					ddlCategory : {
						identifier : 'ddlCategory',
						rules : [ {
							type : 'empty',
							prompt : 'Please enter a value'
						} ]
					},
					ddlPriority : {
						identifier : 'ddlPriority',
						rules : [ {
							type : 'empty',
							prompt : 'Please select a dropdown value'
						} ]
					},
					description : {
						identifier : 'description',
						rules : [ {
							type : 'empty',
							prompt : 'Please Enter Description'
						} ]
					}
				};
				var settings = {
					onFailure : function() {

						return false;
					},
					onSuccess : function() {
						$('#btnCreate').hide();
						$('#btnCreateDummy').show();
						//$('#btnReset').attr("disabled", "disabled");   		   	
					}
				};
				$('.ui.form.validation-form').form(validation, settings);

				var classstate = $('#hdnstate').val();

				$("#dashboardid").addClass(classstate);

			});
</script>

<script type='text/javascript'>
	//<![CDATA[
	$(window)
			.load(
					function() {
						$.fn.dropdown.settings.selectOnKeydown = false;
						$.fn.dropdown.settings.forceSelection = false;

						$('.ui.dropdown').dropdown(
								{
									fullTextSearch : true,
									forceSelection : false,
									selectOnKeydown : true,
									showOnFocus : false,
									on : "click",
									onChange : function(value, text, choice) {
										$(this).children(
												'option[value='
														+ $(choice).data(
																'value') + ']')
												.data('value')
										// Access the data-wsg attribute of the selected option.
									}
								});

						$('.ui.dropdown.oveallsearch').dropdown({
							onChange : function(value, text, $selectedItem) {
								var uid = $('#hdneampmuserid').val();
								redirectbysearchvalue(value, uid);
							},
							fullTextSearch : true,
							forceSelection : false,
							selectOnKeydown : false,
							showOnFocus : true,
							on : "click"
						});

						var ddlType = $('#ddlType'), ddlCategory = $('#ddlCategory'), options = ddlCategory
								.find('option');
						console.log(ddlType);
						ddlType.on(
								'change',
								function() {
									console.log(this.value);
									ddlCategory.html(options
											.filter('[data-value=""]'));
									ddlCategory.html(options
											.filter('[data-value="'
													+ this.value + '"]'));

								}).trigger('change');

					})
</script>

<style>
div.dataTables_wrapper div.dataTables_length select {
	width: 50px;
	display: inline-block;
	margin-left: 3px;
	margin-right: 4px;
}

#overviewInfor {
	display: none;
}

/* .ui.form textarea:not([rows]) {
			    height: 4em;
			    min-height: 4em;
			    max-height: 24em;
			}
			.ui.icon.input > i.icon:before, .ui.icon.input > i.icon:after {
			    left: 0;
			    position: absolute;
			    text-align: center;
			    top: 36%;
			    width: 100%;
			}
			.input-sm, .form-horizontal .form-group-sm .form-control {
			    font-size: 13px;
			    min-height: 25px;
			    height: 32px;
			    padding: 8px 9px;
			} */
			
#example span{
  display:none;
}
</style>

<script type='text/javascript'>
	function getPng(statusName) {

		var status = "";

		switch (statusName) {
		case "active":
			status = "resources/img/active.png";
			break;
		case "warning":
			status = "resources/img/warning.png";
			break;

		case "down":
			status = "resources/img/down.png";
			break;

		case "offline":
			status = "resources/img/offline.png";
			break;
		default:
			// code block
		}
		return status;
	}
	function initialize() {

		var a = $('#MapJsonId').val();
		console.log(a);

		var locations = eval('(' + a + ')');

		window.map = new google.maps.Map(document.getElementById('dvMap'), {
			mapTypeId : google.maps.MapTypeId.ROADMAP
		});

		var infowindow = new google.maps.InfoWindow();

		var bounds = new google.maps.LatLngBounds();

		for (i = 0; i < locations.length; i++) {
			marker = new google.maps.Marker({
				position : new google.maps.LatLng(locations[i].latitude,
						locations[i].longitude),
				map : map,
				disableDefaultUI : true,
				Icon : getPng(locations[i].icon),
				title : locations[i].title,
				zIndex : locations[i].index,
			});

			bounds.extend(marker.position);

			google.maps.event.addListener(marker, 'click',
					(function(marker, i) {
						return function() {
							infowindow.setContent(locations[i].content);
							infowindow.open(map, marker);
						}
					})(marker, i));
		}

		map.fitBounds(bounds);

		var listener = google.maps.event.addListener(map, "idle", function() {
			fnZoom();
			google.maps.event.removeListener(listener);
		});

		function fnZoom() {
			// alert('Work');
			if (locations.length == 1) {
				map.setZoom(15);
			}
		}
	}

	function loadScript() {
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBsXBn_yVijE0j6Bm5d0WGsclNJ_4UCleo&sensor=false&'
				+ 'callback=initialize';
		//script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDueFfOCP3Rm6GtweYWdwCqMFP9BUXxLEE&sensor=false&' + 'callback=initialize';
		//script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDEbIbuKRR_b0iHzErE79mYJsqkh3LIb_g&callback=initMap" type="text/javascript";
		document.body.appendChild(script);
	}

	window.onload = loadScript;
</script>


</head>
<body class="fixed-header">

	<input type="hidden" value="${access.userID}" id="hdneampmuserid">
	<input type="hidden" value="${classtate}" id="hdnstate">




	<jsp:include page="slider.jsp" />




	<!-- <nav class="page-sidebar" data-pages="sidebar">

		<div class="sidebar-header">
			<a href="./dashboard"> <img src="resources/img/logo01.png"
				class="eira-logo" alt="logo"
				title="Electronically Assisted Monitoring and Portfolio Management">
			</a> <i class="fa fa-bars bars"></i>
		</div>

		



	</nav> -->
	<!--  <div class="col-md-12" style="background:#CCC;height:50px;z-index:9999;">
     	<p>Hello</p>
     </div> -->

	<div class="page-container">
		<!-- <div class="header" style="background:green;height:51px;">
    	</div> -->
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					<a href="./dashboard"> <img src="resources/img/logo01.png"
						alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos">
					</a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx" id="SearcSElect">
					<div class="ui  search selection  dropdown  width oveallsearch">
						<input id="myInput" name="tags" type="text">

						<div class="default text">search</div>
						<i class="fa fa-search"></i>
						<div id="myDropdown" class="menu">

							<!--  <div class="item">Dashboard</div> -->

							<jsp:include page="searchselect.jsp" />






						</div>
					</div>




				</div>

				<div class="dropdown pull-right hidden-md-down user-log">
					<button class="profile-dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 circular inline"> <img
							src="resources/img/user_01.png" alt
							data-src="resources/img/user_01.png"
							data-src-retina="resources/img/user_01.png" width="32"
							height="32">
						</span>


						<p class="user-login">${access.userName}</p>

					</button>
					<div class="dropdown-menu dropdown-menu-right profile-dropdown"
						role="menu">
						<a class="dropdown-item disablecolor"><i
							class="pg-settings_small"></i> Settings</a> <a
							class="dropdown-item disablecolor"><i class="pg-outdent"></i>
							Feedback</a> <a class="dropdown-item disablecolor"><i
							class="pg-signals"></i> Help</a> <a href="logout1"
							class="dropdown-item color logout"><i class="pg-power"></i>
							Logout</a>
					</div>
				</div>


				<div class="newticketpopup hidden">
					<p class="center mb-1 tkts" data-toggle="modal"
						data-target="#tktCreation" data-backdrop="static"
						data-keyboard="false">
						<img src="resources/img/tkt.png">
					</p>
					<p class="create-tkts" data-toggle="modal"
						data-target="#tktCreation" data-backdrop="static"
						data-keyboard="false">New Ticket</p>
				</div>


			</div>
		</div>
		<div class="page-content-wrapper ">



			<div class="content" id="QuickLinkWrapper1">

				<div class="container-fixed-lg">
					<ol class="breadcrumb padd-0">
						<li class="breadcrumb-item"><a><i class="fa fa-home mt-7"></i></a></li>
						<li class="breadcrumb-item active">Dashboard</li>
					</ol>
				</div>

				<div class=" container-fluid   container-fixed-lg">
					<div class="card card-transparent ">
						<!-- Content Start-->
						<div class="sortable padd-0">
							<!-- <div class="row"> -->
							<div class="col-md-12 padd-0 m-t-5">
								<div class="col-sm-12 col-md-3 col-lg-3  col-xl-12 col-padd">



									<c:choose>
										<c:when test="${access.monitoringView == 'visible'}">

											<jsp:include page="dashboard_allsites.jsp" />

											<jsp:include page="dashboard_tickets.jsp" />

										</c:when>
										<c:otherwise>

											<jsp:include page="dashboard_allsites.jsp" />

											<jsp:include page="dashboard_energy.jsp" />

											<jsp:include page="dashboard_co2.jsp" />

										</c:otherwise>
									</c:choose>








								</div>

								<c:choose>
									<c:when test="${access.monitoringView == 'visible'}">

										<div class="col-lg-3 col-padd">
											<jsp:include page="dashboard_sitestatus.jsp" />

											<jsp:include page="dashboard_co2.jsp" />

										</div>

										<div class="col-lg-3 col-padd">

											<jsp:include page="dashboard_energy.jsp" />

											<jsp:include page="dashboard_performancemetrics.jsp" />

										</div>

									</c:when>
									<c:otherwise>

										<div
											class="col-lg-6 col-sm-12 col-md-6 col-xl-12 padd-0 paddi-10"
											id="maps">
											<jsp:include page="dashboard_map.jsp" />
										</div>

									</c:otherwise>
								</c:choose>


								<div class="col-lg-3 col-padd">



									<c:choose>
										<c:when test="${access.monitoringView == 'visible'}">


											<jsp:include page="dashboard_events.jsp" />


											<jsp:include page="dashboard_portfoliomanagement.jsp" />

										</c:when>
										<c:otherwise>

											<jsp:include page="dashboard_sitestatus.jsp" />

											<jsp:include page="dashboard_tickets.jsp" />

											<jsp:include page="dashboard_events.jsp" />

										</c:otherwise>
									</c:choose>



								</div>
								<!--   </div> -->
							</div>
						</div>
						<!-- Content End-->

						<!-- Table Start-->
						<div class="row padd-10">
							<div class="col-md-12">
								<div class="card card-default bg-default " id="allsites"
									data-pages="card" style="padding-top: 0px !important;">
									<div class="card-header min-hei-20">
										<div class="card-title  tbl-head">Site Details</div>
									</div>
									<div class="table-responsive padd-5">
										<table id="example" class="table table-striped table-bordered"
											width="100%" cellspacing="0">
											<thead class="bg-color">
												<tr>
													<%--  <c:if test="${access.customerListView == 'visible'} || ${}">
                                          <th style="width:8%;">Site Code</th>
          								   --%>

													<!--  <th style="width:8%;">Site Code</th>  -->
													<th style="width: 8%;">Site ID</th>
													<th style="width: 8%;">Site Type</th>
													<th style="width: 15%;">Site Name</th>
													<th style="width: 7%;">Status</th>
													<th style="width: 13%;">Today Energy (kWh)</th>
													<th style="width: 13%;">Total Energy (MWh)</th>
													<th style="width: 10%;">Specific Yield</th>
													<th style="width: 6%;">Inverters</th>
													<th style="width: 6%;">Meters</th>
													<th style="width: 10%;">Capacity (kW)</th>
													<th style="width: 18%;">Last Updated</th>



													<%--   <c:if test="${access.customerListView == 'visible'} || ${}">
                                          <th style="width:10%;">Last Updated</th>
          								    --%>

												</tr>
											</thead>
											<tbody>



											</tbody>
										</table>
									</div>
								</div>
							</div>

						</div>

						<!-- Table End-->


					</div>
				</div>
			</div>



			<div class="container-fixed-lg footer">
				<div class="container-fluid copyright sm-text-center">
					<p class="small no-margin pull-left sm-pull-reset">
						<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
							CLEAN ENERGY.</span> <span class="hint-text">All rights reserved.
						</span>
					</p>
					<p class="small no-margin pull-rhs sm-pull-reset rhs-pull">
						<span class="hint-text">Powered by</span> <span>MESTECH
							SERVICES PVT LTD</span>.
					</p>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Side Bar Content Start-->

	<input type="hidden" value="${mapData}" id="MapJsonId">

	<!-- Side Bar Content End-->



	<!--  <div class="quickview-wrapper  builder hidden-sm hidden-xs" id="builder">
         <div class="p-l-10 p-r-10 ">
            <a class="builder-close  pg-close"   href="#"></a>
            <a class="builder-toggle" data-toggle="quickview" data-toggle-element="#builder"><img src="resources/img/ImageResize_06.png"></a>
            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary" id="builderTabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tabLayouts" role="tab" aria-controls="home"><span>Quick Links</span></a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active " id="tabLayouts" role="tabcard">
                  <div class="scrollable">
                     <div class="p-r-50">
                        <div class="list">
                           <ul>
                              <li><a href="#QuickLinkWrapper1">Total Sites</a></li>
                              <li><a href="#QuickLinkWrapper2" id="energy">Energy</a></li>
                              <li><a href="#QuickLinkWrapper3" id="co2">Co<sub>2</sub> Avoided</a></li>
                              <li><a href="#QuickLinkWrapper1" id="maps">Map</a></li>
                              <li><a href="#QuickLinkWrapper1" id="sstatus">Site Status</a></li>
                              <li><a href="#QuickLinkWrapper2" id="tkts">Tickets</a></li>
                              <li><a href="#QuickLinkWrapper3" id="ents">Events</a></li>
                              <li><a href="#QuickLinkWrapper4">Site Details</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div> -->
	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>
	<script src="resources/js/moment.min.js" type="text/javascript"></script>
	<script src="resources/js/datetime-moment.js" type="text/javascript"></script>


	<div id="gotop"></div>
	<!-- Address Popup Start !-->

	<!-- Modal -->
	<div id="tktCreation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header Popup-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="resources/img/close.png">
					</button>
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<p class="modal-title tkt-title">
						<img src="resources/img/tkt.png"><span class="tkt-title">New
							Ticket</span>
					</p>
				</div>
				<div class="modal-body">
					<c:url var="addAction" value="/addnewticket"></c:url>
					<div class="col-md-12">
						<p class="tkt-mandat">
							<span class="errormess">*</span> All fields are mandatory
						</p>
					</div>
					<form:form action="${addAction}" modelAttribute="ticketcreation"
						class="ui form validation-form">

						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Site Name</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width SiteNames"
									id="ddlSite" name="ddlSite" path="siteID"
									onchange="getEquipmentAgainstSite()">
									<form:option value="" tabindex="0">Select</form:option>
									<form:options items="${siteList}" />
								</form:select>
							</div>
						</div>

						<div class="col-md-12 col-xs-12  m-t-10 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Equipment</label>
							</div>

							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width equipments"
									id="ddlEquipment" path="equipmentID" name="ddlEquipment">
									<option value="">Select</option>
								</form:select>
							</div>
						</div>


						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Type</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlType" name="ddlType" path="ticketType">
									<form:option value="" tabindex="1">Select</form:option>
									<form:option value="Operation">Operation</form:option>
									<form:option value="Maintenance">Maintenance</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15 mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Category</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width category"
									id="ddlCategory" name="ddlCategory" path="ticketCategory">
									<form:option value="" tabindex="0">Select </form:option>
									<form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
									<form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
									<form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
									<form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
									<form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
									<form:option data-value="Operation"
										value="Equipment Replacement">Equipment Replacement</form:option>
									<form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
									<form:option data-value="Operation" value="String Down">String Down</form:option>
									<form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
									<form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
									<form:option data-value="Operation" value="">Select</form:option>
									<form:option data-value="Maintenance" value="">Select</form:option>
									<form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
									<form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="DataLogger Cleaning">DataLogger Cleaning</form:option>
									<form:option data-value="Maintenance"
										value="String Current Measurement">String Current Measurement</form:option>
									<form:option data-value="Maintenance"
										value="Preventive Maintenance">Preventive Maintenance</form:option>
									<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
									<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
									<form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
									<form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
									<form:option data-value="Maintenance" value="">Select</form:option>
								</form:select>
							</div>

						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Subject</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<form:input placeholder="Subject" name="Subject" type="text"
										autocomplete="off" id="txtSubject" path="ticketDetail"
										maxLength="50" />
								</div>
							</div>
						</div>

						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Priority</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<form:select
									class="field ui fluid search selection dropdown width"
									id="ddlPriority" name="user" path="priority">
									<form:option value="">Select </form:option>
									<form:option data-value="3" value="3">High</form:option>
									<form:option data-value="2" value="2">Medium</form:option>
									<form:option data-value="1" value="1">Low</form:option>
								</form:select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 m-t-15  mmt-15">
							<div class="col-md-4 col-xs-12">
								<label class="fields required m-t-10">Description</label>
							</div>
							<div class="col-md-8 col-xs-12">
								<div class="field">
									<textarea id="txtTktdescription" autocomplete="off"
										name="description" style="resize: none;"
										placeholder="Ticket Description" maxLength="120"></textarea>
								</div>

							</div>
						</div>


						<div class="col-md-12 col-xs-12  m-t-15  mmt-15 center">
							<div class="btn btn-success  submit" id="btnCreate" tabIndex="1">Create</div>
							<button class="btn btn-success" type="button" id="btnCreateDummy"
								style="display: none;">Create</button>
							<div class="btn btn-primary clear m-l-15" id="btnReset"
								tabIndex="2">Reset</div>
						</div>
						<!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
						<!-- <div class="ui error message"></div> -->
					</form:form>
					<!-- <div class="ui error message"></div> -->

				</div>
			</div>
		</div>
	</div>
	<!-- Address Popup End !-->

</body>


<script type="text/javascript">
	function getEquipmentAgainstSite() {
		var siteId = $('#ddlSite').val();

		$.ajax({
			type : 'GET',
			url : './equipmentsBysites?siteId=' + siteId,
			success : function(msg) {

				var EquipmentList = msg.equipmentlist;

				for (i = 0; i < EquipmentList.length; i++) {
					$('#ddlEquipment').append(
							"<option value="+EquipmentList[i].equipmentId+">"
									+ EquipmentList[i].customerNaming
									+ "</option>");
				}

			},
			error : function(msg) {
				console.log(msg);
			}
		});
	}
</script>
</html>

