
/******************************************************
 * 
 *    	Filename	: EquipmentDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Equipment related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.bean.SiteCommonBean;
import com.mestech.eampm.bean.SiteViewBean;
import com.mestech.eampm.bean.StandardParameterBean;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentCategory;

public interface EquipmentDAO {

	public void addEquipment(Equipment equipment);

	public void updateEquipment(Equipment equipment);

	public Equipment getEquipmentById(int id);

	public List<Equipment> listEquipmentsByIds(String Ids);

	public Equipment getEquipmentByMax(String MaxColumnName);

	public void removeEquipment(int id);

	public List<Equipment> listEquipments();

	public List<Equipment> listEquipmentsBySiteId(int siteId);

	public List<Equipment> listEquipmentsByUserId(int userId);

	public List<Equipment> listInverters();

	public List<Equipment> listEnergymeters();

	public List<Equipment> listScbs();

	public List<Equipment> listInvertersBySiteId(int siteId);

	public List<Equipment> listInvertersByUserId(int userId);

	public List<Equipment> listEnergymeterByUserId(int userId);

	public List<Equipment> listScbByUserId(int userId);

	public List<Equipment> listFilterInvertersByUserId(int userId);

	public List<Equipment> listFilterInvertersBySiteId(int siteId);

	public List<Equipment> listSensors();

	public List<Equipment> listSensorsBySiteId(int siteId);

	public List<Equipment> listSensorsByUserId(int userId);

	public List<StandardParameterBean> getStandardParameterByEquipmentIds(String equipmentIds, String EquipmentsIDs);

	public List<StandardParameterBean> getStandardParameterBySiteIds(String siteIds);

	public List<Equipment> listallequipments(int customerid);

	public List<Equipment> listConfigEquipments();

	public List<SiteCommonBean> listInverterDetails(int siteid);

	public List<Equipment> listEquipmentComunicationCount(int siteid);

	public List<Equipment> listTrackers(int siteid);

	public List<EquipmentCategory> getCategoryByEquipmentIds(int equipmentIds);

}
