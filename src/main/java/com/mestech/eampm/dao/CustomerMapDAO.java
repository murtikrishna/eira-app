
/******************************************************
 * 
 *    	Filename	: CustomerMapDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Customer map related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.CustomerMap;

public interface CustomerMapDAO {

	public void addCustomerMap(CustomerMap customermap);

	public void updateCustomerMap(CustomerMap customermap);

	public CustomerMap getCustomerMapById(int id);

	public void removeCustomerMap(int id);

	public List<CustomerMap> listCustomerMaps();

	public List<CustomerMap> listCustomerMaps(int mapid);
}
