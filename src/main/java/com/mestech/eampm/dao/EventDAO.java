
/******************************************************
 * 
 *    	Filename	: EventDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for event related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.Event;

public interface EventDAO {

	public void addEvent(Event event);

	public void updateEvent(Event event);

	public Event getEventById(int id);

	public void removeEvent(int id);

	public List<Event> listEvents();
}
