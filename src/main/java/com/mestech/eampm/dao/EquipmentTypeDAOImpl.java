
/******************************************************
 * 
 *    	Filename	: EquipmentTypeDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Equipment Type operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.model.CustomerType;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentType;

@Repository
public class EquipmentTypeDAOImpl implements EquipmentTypeDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addEquipmentType(EquipmentType equipmenttype) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(equipmenttype);

	}

	// @Override
	public void updateEquipmentType(EquipmentType equipmenttype) {
		Session session = sessionFactory.getCurrentSession();
		session.update(equipmenttype);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<EquipmentType> listEquipmentTypes() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<EquipmentType> EquipmentTypesList = session.createQuery("from EquipmentType").list();

		return EquipmentTypesList;
	}

	// @Override
	public EquipmentType getEquipmentTypeById(int id) {
		Session session = sessionFactory.getCurrentSession();
		EquipmentType equipmenttype = (EquipmentType) session.get(EquipmentType.class, new Integer(id));
		return equipmenttype;
	}

	public EquipmentType getEquipmentTypeByMax(String MaxColumnName) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from EquipmentType ORDER BY " + MaxColumnName + " DESC";
		EquipmentType equipmenttype = (EquipmentType) session.createQuery(Query).setMaxResults(1).getSingleResult();
		return equipmenttype;
	}

	// @Override
	public void removeEquipmentType(int id) {
		Session session = sessionFactory.getCurrentSession();
		EquipmentType equipmenttype = (EquipmentType) session.get(EquipmentType.class, new Integer(id));

		// De-activate the flag
		equipmenttype.setActiveFlag(0);

		if (null != equipmenttype) {
			// session.delete(equipmenttype);
			session.update(equipmenttype);
		}
	}

	public List<EquipmentType> listallequipments(int equipmentid) {
		Session session = sessionFactory.getCurrentSession();

		String Query = "from EquipmentType where EquipmentTypeID not in('" + equipmentid + "')";
		List<EquipmentType> EquipmentList = session.createQuery(Query).list();

		return EquipmentList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<EquipmentType> listFilterEquipmentsByTypeId(int typeId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from EquipmentType where ActiveFlag='1' and CategoryID ='" + typeId + "' ";
		List<EquipmentType> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

}
