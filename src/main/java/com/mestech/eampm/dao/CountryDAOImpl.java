
/******************************************************
 * 
 *    	Filename	: CountryDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Country related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.Country;

@Repository
public class CountryDAOImpl implements CountryDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addCountry(Country country) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(country);

	}

	// @Override
	public void updateCountry(Country country) {
		Session session = sessionFactory.getCurrentSession();
		session.update(country);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Country> listCountries() {
		Session session = sessionFactory.getCurrentSession();
		List<Country> CountriesList = session.createQuery("from Country where ActiveFlag=1").list();

		return CountriesList;
	}

	// @Override
	public Country getCountryById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Country country = (Country) session.get(Country.class, new Integer(id));
		return country;
	}

	// @Override
	public void removeCountry(int id) {
		Session session = sessionFactory.getCurrentSession();
		Country country = (Country) session.get(Country.class, new Integer(id));

		// De-activate the flag
		country.setActiveFlag(0);

		if (null != country) {
			// session.delete(country);

			session.update(country);
		}
	}
}
