
/******************************************************
 * 
 *    	Filename	: StatusDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for status related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.Status;

public interface StatusDAO {

	public void addStatus(Status status);

	public void updateStatus(Status status);

	public Status getStatusById(Integer id);

	public void removeStatus(int id);

	public List<Status> listStatuses();
}
