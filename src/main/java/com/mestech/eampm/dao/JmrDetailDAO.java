
/******************************************************
 * 
 *    	Filename	: JmrDetailDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Jmt report related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.JmrDetail;
import com.mestech.eampm.model.Site;

public interface JmrDetailDAO {

	public void addJmrDetail(JmrDetail jmrdetail);

	public void updateJmrDetail(JmrDetail jmrdetail);

	public JmrDetail getJmrDetailById(int id);

	public JmrDetail getJmrDetailByticketId(int ticketid);

	public JmrDetail getticketID(Integer ticketid);

	public void removeJmrDetail(int id);

	public List<JmrDetail> listJmrDetails();
}
