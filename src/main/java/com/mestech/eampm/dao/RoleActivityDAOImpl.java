
/******************************************************
 * 
 *    	Filename	: RoleActivityDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Role operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.RoleActivity;

@Repository
public class RoleActivityDAOImpl implements RoleActivityDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addRoleActivity(RoleActivity roleactivity) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(roleactivity);

	}

	// @Override
	public void updateRoleActivity(RoleActivity roleactivity) {
		Session session = sessionFactory.getCurrentSession();
		session.update(roleactivity);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<RoleActivity> listRoleActivities() {
		Session session = sessionFactory.getCurrentSession();
		List<RoleActivity> RoleActivitiesList = session.createQuery("from RoleActivity where ActiveFlag=1").list();

		return RoleActivitiesList;
	}

	// @Override
	public RoleActivity getRoleActivityById(int id) {
		Session session = sessionFactory.getCurrentSession();
		RoleActivity roleactivity = (RoleActivity) session.get(RoleActivity.class, new Integer(id));
		return roleactivity;
	}

	// @Override
	public void removeRoleActivity(int id) {
		Session session = sessionFactory.getCurrentSession();
		RoleActivity roleactivity = (RoleActivity) session.get(RoleActivity.class, new Integer(id));
		if (null != roleactivity) {
			session.delete(roleactivity);
		}
	}

	@Override
	@SuppressWarnings("deprecation")
	public List<RoleActivity> getRoleActivityByRoleId(int roleId) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from RoleActivity where RoleID=:RoleID");
		query.setParameter("RoleID", roleId);
		List<RoleActivity> RoleActivitiesList = query.list();
		return RoleActivitiesList;
	}

	@Override
	@SuppressWarnings("deprecation")
	public void deleteRoleActivity(RoleActivity roleActivity) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(roleActivity);
	}
}
