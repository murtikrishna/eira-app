
/******************************************************
 * 
 *    	Filename	: ErrorCodeDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Error Code related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.ErrorCode;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;

@Repository
public class ErrorCodeDAOImpl implements ErrorCodeDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addErrorCode(ErrorCode errorcode) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(errorcode);

	}

	// @Override
	public void updateErrorCode(ErrorCode errorcode) {
		Session session = sessionFactory.getCurrentSession();
		session.update(errorcode);
	}

	// @Override
	public List<ErrorCode> listErrorCodes(int TypeID) {
		Session session = sessionFactory.getCurrentSession();

		// Common for Oracle & PostgreSQL
		List<ErrorCode> ErrorCodesList = session.createQuery("from ErrorCode Where EquipmentTypeId='" + TypeID + "' ")
				.list();

		return ErrorCodesList;
	}

	public List<ErrorCode> listErrorCodes() {
		Session session = sessionFactory.getCurrentSession();

		// Common for Oracle & PostgreSQL
		List<ErrorCode> ErrorCodesList = session.createQuery("from ErrorCode").list();

		return ErrorCodesList;
	}

	// @Override
	public ErrorCode getErrorCodeById(int id) {
		Session session = sessionFactory.getCurrentSession();
		ErrorCode errorcode = (ErrorCode) session.get(ErrorCode.class, new Integer(id));
		return errorcode;
	}

	// @Override
	public void removeErrorCode(int id) {
		Session session = sessionFactory.getCurrentSession();
		ErrorCode errorcode = (ErrorCode) session.get(ErrorCode.class, new Integer(id));

		// De-activate the flag
		errorcode.setActiveFlag(0);

		if (null != errorcode) {
			session.update(errorcode);
			// session.delete(activity);
		}
	}

	// 03-02-19
	/*
	 * public List<ErrorCode> getErrorCodeListByUserId(int userId) { Session session
	 * = sessionFactory.getCurrentSession(); //Common for Oracle & PostgreSQL String
	 * Query =
	 * "from ErrorCode where  ActiveFlag='1' and SiteID in (Select SiteId from Site where CustomerID in (Select CustomerID from CustomerMap where UserID='"
	 * + userId +
	 * "') or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userId + "'))"; List<ErrorCode> ErrorCodeList =
	 * session.createQuery(Query).list();
	 * 
	 * return ErrorCodeList; }
	 */

	public List<ErrorCode> getErrorCodeListByUserId(int userId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from ErrorCode where  ActiveFlag='1' and SiteID in (Select SiteId from SiteMap where UserID='"
				+ userId + "' and ActiveFlag=1)";
		List<ErrorCode> ErrorCodeList = session.createQuery(Query).list();

		return ErrorCodeList;
	}

	public ErrorCode getErrorCodeByMax(String MaxColumnName) {
		Session session = sessionFactory.getCurrentSession();
		ErrorCode errorcode = null;
		// Common for Oracle & PostgreSQL
		try {
			errorcode = (ErrorCode) session.createQuery("from ErrorCode Order By " + MaxColumnName + " desc")
					.setMaxResults(1).getSingleResult();
		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return errorcode;
	}

	public List<ErrorCode> getErrorCodeListBySiteId(int siteId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from ErrorCode where ActiveFlag='1' and SiteID='" + siteId + "'";
		List<ErrorCode> ErrorCodeList = session.createQuery(Query).list();

		return ErrorCodeList;
	}

	public List<ErrorCode> getErrorCodeListByCustomerId(int customerId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from ErrorCode where  ActiveFlag='1' and SiteID in (Select SiteId from Site where CustomerID ='"
				+ customerId + "')";
		List<ErrorCode> ErrorCodeList = session.createQuery(Query).list();

		return ErrorCodeList;
	}

	public List<ErrorCode> listexitsErrorCodes(int equipmenttypeid, int errorid) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from ErrorCode where  ActiveFlag='1' and EquipmentTypeId not in ('" + equipmenttypeid
				+ "') and ErrorId not in ('" + errorid + "')";
		List<ErrorCode> ErrorCodeList = session.createQuery(Query).list();

		return ErrorCodeList;
	}

}
