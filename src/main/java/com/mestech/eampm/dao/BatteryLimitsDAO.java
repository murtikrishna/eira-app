
/******************************************************
 * 
 *    	Filename	: BatteryLimitsDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Battery Limits operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.BatteryLimits;

public interface BatteryLimitsDAO {

	public void addBatteryLimits(BatteryLimits batterylimits);

	public void updateBatteryLimits(BatteryLimits batterylimits);

	public BatteryLimits getBatteryLimitsById(int id);

	public void removeBatteryLimits(int id);

	public List<BatteryLimits> listBatteryLimits();

}
