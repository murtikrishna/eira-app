
/******************************************************
 * 
 *    	Filename	: StandardOperatingDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for sop operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.StandardOperatingProcedure;
import com.mestech.eampm.utility.SOPRequest;

public interface StandardOperatingDAO {

	public void saveOrUpdate(SOPRequest sopRequest);

	public List<StandardOperatingProcedure> getSop(String categoryId, String typeId);

	public void delete(StandardOperatingProcedure operatingProcedure);
}
