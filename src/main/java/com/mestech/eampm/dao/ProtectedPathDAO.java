
/******************************************************
 * 
 *    	Filename	: ProtectedPathDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for URl operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.ProtectedUrl;

public interface ProtectedPathDAO {

	public List<ProtectedUrl> getAll();
}
