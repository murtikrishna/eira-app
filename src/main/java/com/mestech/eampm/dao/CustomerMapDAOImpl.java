

/******************************************************
 * 
 *    	Filename	: CustomerMapDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Customer map related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
 
import com.mestech.eampm.model.CustomerMap;
import com.mestech.eampm.model.User;

@Repository
public class CustomerMapDAOImpl implements CustomerMapDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addCustomerMap(CustomerMap customermap) {
	   
       Session session = sessionFactory.getCurrentSession();
       session.persist(customermap);
       
   }

   //@Override
   public void updateCustomerMap(CustomerMap customermap) {
       Session session = sessionFactory.getCurrentSession();
       session.update(customermap);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<CustomerMap> listCustomerMaps() {
       Session session = sessionFactory.getCurrentSession();
       List<CustomerMap> CustomerMapsList = session.createQuery("from CustomerMap").list();
       
       
       
       return CustomerMapsList;
   }

   //@Override
   public CustomerMap getCustomerMapById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       CustomerMap customermap = (CustomerMap) session.get(CustomerMap.class, new Integer(id));
       return customermap;
   }

   //@Override
   public void removeCustomerMap(int id) {
       Session session = sessionFactory.getCurrentSession();
       CustomerMap customermap = (CustomerMap) session.get(CustomerMap.class, new Integer(id));
       
       //De-activate the flag
       customermap.setActiveFlag(0);
       
       if(null != customermap){
    	   session.update(customermap);
           //session.delete(activity);
       }
   }
   
   
   @SuppressWarnings("unchecked")
   //@Override
   public List<CustomerMap> listCustomerMaps(int mapid) {
       Session session = sessionFactory.getCurrentSession();
     //Common for Oracle & PostgreSQL
       List<CustomerMap> CustomerList = session.createQuery("from CustomerMap where MapID not in('"+ mapid +"')").list();
       
       return CustomerList;
   }
}
