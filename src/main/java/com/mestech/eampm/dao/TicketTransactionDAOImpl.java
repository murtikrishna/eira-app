
/******************************************************
 * 
 *    	Filename	: TicketTransactionDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for ticket transaction operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.TicketTransaction;
import com.mestech.eampm.utility.UtilityCommon;
import com.mestech.eampm.utility.UtilityCommonDAO;

@Repository
public class TicketTransactionDAOImpl implements TicketTransactionDAO {

	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();
	private UtilityCommon utilityCommon = new UtilityCommon();

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addTicketTransaction(TicketTransaction ticketdetail) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(ticketdetail);

	}

	// @Override
	public void updateTicketTransaction(TicketTransaction ticketdetail) {
		Session session = sessionFactory.getCurrentSession();
		session.update(ticketdetail);
	}

	// @Override
	public void removeTicketTransaction(int id) {
		Session session = sessionFactory.getCurrentSession();
		TicketTransaction ticketdetail = (TicketTransaction) session.get(TicketTransaction.class, new Integer(id));

		// De-activate the flag
		ticketdetail.setActiveFlag(0);

		if (null != ticketdetail) {
			session.update(ticketdetail);
			// session.delete(activity);
		}
	}

	private List<TicketTransaction> GetCastedTicketTransactionList(List<TicketTransaction> lstTicketTransaction) {
		List<TicketTransaction> lstTicketTransactionCasted = new ArrayList<TicketTransaction>();

		if (lstTicketTransaction.size() > 0) {

			Integer transactionid = 0;
			Integer ticketid = 0;
			String description = "";
			Integer assignedto = 0;
			Date timestamp = null;
			String remarks = "";
			Integer ticketstatus = 0;
			Integer activeflag = 0;
			Date creationdate = null;
			Integer createdby = 0;
			Date lastupdateddate = null;
			Integer lastupdatedby = 0;
			Integer daycycle = 0;

			for (Object object : lstTicketTransaction) {
				Object[] obj = (Object[]) object;

				transactionid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[0]);
				ticketid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[1]);
				description = utilityCommon.StringFromStringObject(obj[2]);
				assignedto = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[3]);
				timestamp = utilityCommon.DateFromDateObject(obj[4]);
				remarks = utilityCommon.StringFromStringObject(obj[5]);
				ticketstatus = utilityCommon.IntegerFromShortObject(obj[6]);
				activeflag = utilityCommon.IntegerFromShortObject(obj[7]);
				creationdate = utilityCommon.DateFromDateObject(obj[8]);
				createdby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[9]);
				lastupdateddate = utilityCommon.DateFromDateObject(obj[10]);
				lastupdatedby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[11]);
				daycycle = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[12]);

				TicketTransaction objTicketTransactionCasted = new TicketTransaction();
				objTicketTransactionCasted.setTransactionID(transactionid);
				objTicketTransactionCasted.setTicketID(ticketid);
				objTicketTransactionCasted.setDescription(description);
				objTicketTransactionCasted.setAssignedTo(assignedto);
				objTicketTransactionCasted.setTimeStamp(timestamp);
				objTicketTransactionCasted.setRemarks(remarks);
				objTicketTransactionCasted.setTicketStatus(ticketstatus);
				objTicketTransactionCasted.setActiveFlag(activeflag);
				objTicketTransactionCasted.setCreationDate(creationdate);
				objTicketTransactionCasted.setCreatedBy(createdby);
				objTicketTransactionCasted.setLastUpdatedDate(lastupdateddate);
				objTicketTransactionCasted.setLastUpdatedBy(lastupdatedby);
				objTicketTransactionCasted.setDayCycle(daycycle);

				lstTicketTransactionCasted.add(objTicketTransactionCasted);

			}
		}

		return lstTicketTransactionCasted;
	}

	private TicketTransaction GetCastedTicketTransaction(Object objTicketTransaction) {
		TicketTransaction objTicketTransactionCasted = new TicketTransaction();

		if (objTicketTransaction != null) {

			Integer transactionid = 0;
			Integer ticketid = 0;
			String description = "";
			Integer assignedto = 0;
			Date timestamp = null;
			String remarks = "";
			Integer ticketstatus = 0;
			Integer activeflag = 0;
			Date creationdate = null;
			Integer createdby = 0;
			Date lastupdateddate = null;
			Integer lastupdatedby = 0;
			Integer daycycle = 0;

			Object[] obj = (Object[]) objTicketTransaction;
			transactionid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[0]);
			ticketid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[1]);
			description = utilityCommon.StringFromStringObject(obj[2]);
			assignedto = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[3]);
			timestamp = utilityCommon.DateFromDateObject(obj[4]);
			remarks = utilityCommon.StringFromStringObject(obj[5]);
			ticketstatus = utilityCommon.IntegerFromShortObject(obj[6]);
			activeflag = utilityCommon.IntegerFromShortObject(obj[7]);
			creationdate = utilityCommon.DateFromDateObject(obj[8]);
			createdby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[9]);
			lastupdateddate = utilityCommon.DateFromDateObject(obj[10]);
			lastupdatedby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[11]);
			daycycle = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[12]);

			objTicketTransactionCasted.setTransactionID(transactionid);
			objTicketTransactionCasted.setTicketID(ticketid);
			objTicketTransactionCasted.setDescription(description);
			objTicketTransactionCasted.setAssignedTo(assignedto);
			objTicketTransactionCasted.setTimeStamp(timestamp);
			objTicketTransactionCasted.setRemarks(remarks);
			objTicketTransactionCasted.setTicketStatus(ticketstatus);
			objTicketTransactionCasted.setActiveFlag(activeflag);
			objTicketTransactionCasted.setCreationDate(creationdate);
			objTicketTransactionCasted.setCreatedBy(createdby);
			objTicketTransactionCasted.setLastUpdatedDate(lastupdateddate);
			objTicketTransactionCasted.setLastUpdatedBy(lastupdatedby);
			objTicketTransactionCasted.setDayCycle(daycycle);

		}

		return objTicketTransactionCasted;
	}

	// @Override
	public TicketTransaction getTicketTransactionById(int id, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		TicketTransaction tickettransaction = null;

		try {
			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					String tablecolumnnames = "transactionid,ticketid,description,assignedto,(timestamp"
							+ TimeConversionFromUTC + ") as timestamp,remarks,ticketstatus,activeflag,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby,daycycle";
					Object TicketTransactionObject = session
							.createSQLQuery("Select " + tablecolumnnames
									+ " from tTicketTransaction where TransactionID='" + id + "' limit 1")
							.list().get(0);
					tickettransaction = GetCastedTicketTransaction(TicketTransactionObject);
				} else {
					tickettransaction = (TicketTransaction) session.get(TicketTransaction.class, new Integer(id));
				}
			} else { // Oracle
				tickettransaction = (TicketTransaction) session.get(TicketTransaction.class, new Integer(id));
			}

		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return tickettransaction;

	}

	public TicketTransaction getTicketTransactionByMax(String MaxColumnName, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		TicketTransaction tickettransaction = null;

		try {
			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					String tablecolumnnames = "transactionid,ticketid,description,assignedto,(timestamp"
							+ TimeConversionFromUTC + ") as timestamp,remarks,ticketstatus,activeflag,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby,daycycle";
					String Query = "Select " + tablecolumnnames + " from tTicketTransaction Order By " + MaxColumnName
							+ " desc limit 1";
					Object TicketTransactionObject = session.createSQLQuery(Query).list().get(0);
					tickettransaction = GetCastedTicketTransaction(TicketTransactionObject);
				} else {
					tickettransaction = (TicketTransaction) session
							.createQuery("from TicketTransaction Order By " + MaxColumnName + " desc").setMaxResults(1)
							.getSingleResult();
				}
			} else { // Oracle
				tickettransaction = (TicketTransaction) session
						.createQuery("from TicketTransaction Order By " + MaxColumnName + " desc").setMaxResults(1)
						.getSingleResult();
			}

		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return tickettransaction;

	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<TicketTransaction> listTicketTransactions(String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<TicketTransaction> tickettransactionlist = null;

		try {
			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					String tablecolumnnames = "transactionid,ticketid,description,assignedto,(timestamp"
							+ TimeConversionFromUTC + ") as timestamp,remarks,ticketstatus,activeflag,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby,daycycle";
					String Query = "Select " + tablecolumnnames + " from tTicketTransaction";
					List<TicketTransaction> TicketTransactionObject = session.createSQLQuery(Query).list();
					tickettransactionlist = GetCastedTicketTransactionList(TicketTransactionObject);
				} else {
					tickettransactionlist = session.createQuery("from TicketTransaction").list();
				}
			} else { // Oracle
				tickettransactionlist = session.createQuery("from TicketTransaction").list();
			}

		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return tickettransactionlist;
	}

	// 03-02-19

	/*
	 * public List<TicketTransaction> getTicketTransactionListByUserId(int userId,
	 * String TimezoneOffset) {
	 * 
	 * Session session = sessionFactory.getCurrentSession(); List<TicketTransaction>
	 * tickettransactionlist = null;
	 * 
	 * try { //PostgreSQL
	 * if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * String tablecolumnnames =
	 * "transactionid,ticketid,description,assignedto,(timestamp" +
	 * TimeConversionFromUTC +
	 * ") as timestamp,remarks,ticketstatus,activeflag,(creationdate" +
	 * TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" +
	 * TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby,daycycle";
	 * String Query = "Select " + tablecolumnnames +
	 * " from tTicketTransaction where  ActiveFlag='1' and SiteID in (Select SiteID from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userId + "'))"; List<TicketTransaction> TicketTransactionObject =
	 * session.createSQLQuery(Query).list(); tickettransactionlist =
	 * GetCastedTicketTransactionList(TicketTransactionObject); } else { String
	 * Query =
	 * "from TicketTransaction where  ActiveFlag='1' and SiteID in (Select SiteID from Site where CustomerID in (Select CustomerID from CustomerMap where UserID='"
	 * + userId +
	 * "') or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userId + "'))"; tickettransactionlist = session.createQuery(Query).list();
	 * } } else { //Oracle String Query =
	 * "from TicketTransaction where  ActiveFlag='1' and SiteID in (Select SiteID from Site where CustomerID in (Select CustomerID from CustomerMap where UserID='"
	 * + userId + "'))"; tickettransactionlist = session.createQuery(Query).list();
	 * }
	 * 
	 * } catch(NoResultException ex) {
	 * 
	 * } catch(Exception ex) {
	 * 
	 * }
	 * 
	 * 
	 * 
	 * return tickettransactionlist;
	 * 
	 * }
	 */

	public List<TicketTransaction> getTicketTransactionListByUserId(int userId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<TicketTransaction> tickettransactionlist = null;

		try {
			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					String tablecolumnnames = "transactionid,ticketid,description,assignedto,(timestamp"
							+ TimeConversionFromUTC + ") as timestamp,remarks,ticketstatus,activeflag,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby,daycycle";
					String Query = "Select " + tablecolumnnames
							+ " from tTicketTransaction where  ActiveFlag='1' and SiteID in  (Select SiteId from mSiteMap where UserID='"
							+ userId + "' and ActiveFlag=1)";
					List<TicketTransaction> TicketTransactionObject = session.createSQLQuery(Query).list();
					tickettransactionlist = GetCastedTicketTransactionList(TicketTransactionObject);
				} else {
					String Query = "from TicketTransaction where  ActiveFlag='1' and SiteID in  (Select SiteId from mSiteMap where UserID='"
							+ userId + "' and ActiveFlag=1)";
					tickettransactionlist = session.createQuery(Query).list();
				}
			}

		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return tickettransactionlist;

	}

	public List<TicketTransaction> getTicketTransactionListBySiteId(int siteId, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketTransaction> tickettransactionlist = null;

		try {
			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					String tablecolumnnames = "transactionid,ticketid,description,assignedto,(timestamp"
							+ TimeConversionFromUTC + ") as timestamp,remarks,ticketstatus,activeflag,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby,daycycle";
					String Query = "Select " + tablecolumnnames
							+ " from tTicketTransaction where ActiveFlag='1' and SiteID='" + siteId + "'";
					List<TicketTransaction> TicketTransactionObject = session.createSQLQuery(Query).list();
					tickettransactionlist = GetCastedTicketTransactionList(TicketTransactionObject);
				} else {
					String Query = "from TicketTransaction where ActiveFlag='1' and SiteID='" + siteId + "'";
					tickettransactionlist = session.createQuery(Query).list();
				}
			} else { // Oracle
				String Query = "from TicketTransaction where ActiveFlag='1' and SiteID='" + siteId + "'";
				tickettransactionlist = session.createQuery(Query).list();
			}

		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return tickettransactionlist;

	}

	public List<TicketTransaction> getTicketTransactionListByTicketId(int ticketId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<TicketTransaction> tickettransactionlist = null;

		try {
			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					String tablecolumnnames = "transactionid,ticketid,description,assignedto,(timestamp"
							+ TimeConversionFromUTC + ") as timestamp,remarks,ticketstatus,activeflag,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby,daycycle";
					String Query = "Select " + tablecolumnnames
							+ " from tTicketTransaction  where ActiveFlag='1' and TicketID='" + ticketId
							+ "' order by TransactionId";

					List<TicketTransaction> TicketTransactionObject = session.createSQLQuery(Query).list();
					tickettransactionlist = GetCastedTicketTransactionList(TicketTransactionObject);
				} else {
					String Query = "from TicketTransaction where ActiveFlag='1' and TicketID='" + ticketId
							+ "' order by TransactionId";
					tickettransactionlist = session.createQuery(Query).list();
				}
			} else { // Oracle
				String Query = "from TicketTransaction where ActiveFlag='1' and TicketID='" + ticketId
						+ "' order by TransactionId";
				tickettransactionlist = session.createQuery(Query).list();
			}

		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return tickettransactionlist;

	}

}
