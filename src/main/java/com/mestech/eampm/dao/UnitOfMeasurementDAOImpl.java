
/******************************************************
 * 
 *    	Filename	: UnitOfMeasurementDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for UOM operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.UnitOfMeasurement;

@Repository
public class UnitOfMeasurementDAOImpl implements UnitOfMeasurementDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addUnitOfMeasurement(UnitOfMeasurement unitofmeasurement) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(unitofmeasurement);

	}

	// @Override
	public void updateUnitOfMeasurement(UnitOfMeasurement unitofmeasurement) {
		Session session = sessionFactory.getCurrentSession();
		session.update(unitofmeasurement);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<UnitOfMeasurement> listUnitOfMeasurements() {
		Session session = sessionFactory.getCurrentSession();
		List<UnitOfMeasurement> UnitOfMeasurementsList = session.createQuery("from UnitOfMeasurement").list();

		return UnitOfMeasurementsList;
	}

	// @Override
	public UnitOfMeasurement getUnitOfMeasurementById(int id) {
		Session session = sessionFactory.getCurrentSession();
		UnitOfMeasurement unitofmeasurement = (UnitOfMeasurement) session.get(UnitOfMeasurement.class, new Integer(id));
		return unitofmeasurement;
	}

	// @Override
	public void removeUnitOfMeasurement(int id) {
		Session session = sessionFactory.getCurrentSession();
		UnitOfMeasurement unitofmeasurement = (UnitOfMeasurement) session.get(UnitOfMeasurement.class, new Integer(id));

		// De-activate the flag
		unitofmeasurement.setActiveFlag(0);

		if (null != unitofmeasurement) {
			// session.delete(unitofmeasurement);

			session.update(unitofmeasurement);
		}
	}
}
