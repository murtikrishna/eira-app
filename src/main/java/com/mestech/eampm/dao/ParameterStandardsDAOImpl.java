
/******************************************************
 * 
 *    	Filename	: ParameterStandardsDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Standard parameters related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.MparameterIntegratedStandards;
import com.mestech.eampm.model.ParameterStandard;

@Repository
public class ParameterStandardsDAOImpl implements ParameterStandardsDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addParameterStandards(ParameterStandard parameterstandards) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(parameterstandards);

	}

	// @Override
	public void updateParameterStandards(ParameterStandard parameterstandards) {
		Session session = sessionFactory.getCurrentSession();
		session.update(parameterstandards);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<ParameterStandard> listParameterStandards() {
		Session session = sessionFactory.getCurrentSession();
		List<ParameterStandard> ParameteStandardsList = session
				.createQuery("from ParameterStandard order by StandardId Asc").list();

		return ParameteStandardsList;
	}

	// @Override
	public ParameterStandard getParameterStandardsById(int id) {
		Session session = sessionFactory.getCurrentSession();
		ParameterStandard parameterstandards = (ParameterStandard) session.get(ParameterStandard.class,
				new Integer(id));
		return parameterstandards;
	}

	// @Override
	public void removeParameterStandards(int id) {
		Session session = sessionFactory.getCurrentSession();
		ParameterStandard parameterstandards = (ParameterStandard) session.get(ParameterStandard.class,
				new Integer(id));

		// De-activate the flag
		parameterstandards.setActiveFlag(0);

		if (null != parameterstandards) {
			session.update(parameterstandards);
			// session.delete(activity);
		}
	}

	public ParameterStandard getParameterStandardByMax(String MaxColumnName) {
		Session session = sessionFactory.getCurrentSession();
		ParameterStandard parameterstandard = null;
		// Common for Oracle & PostgreSQL
		try {
			parameterstandard = (ParameterStandard) session
					.createQuery("from ParameterStandard Order By " + MaxColumnName + " desc").setMaxResults(1)
					.getSingleResult();
		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return parameterstandard;
	}

	public List<ParameterStandard> listParameterStandards(int standardid) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<ParameterStandard> ParameterStandardList = session
				.createQuery("from ParameterStandard where StandardId not in('" + standardid + "')").list();
		return ParameterStandardList;
	}

	@Override
	public List<MparameterIntegratedStandards> getallStandards() {
		try {
			Session session = sessionFactory.getCurrentSession();
			String qry = "FROM MparameterIntegratedStandards";
			List<MparameterIntegratedStandards> integratedStandards = session.createQuery(qry).list();
			return integratedStandards;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public List<String> getallStandardParameterNames() {
		return sessionFactory.getCurrentSession()
				.createSQLQuery("Select standardparametername from mparameterstandards").list();
	}

	public void saveParameterIntegratedStandards(MparameterIntegratedStandards integratedStandards) {
		sessionFactory.getCurrentSession().save(integratedStandards);
	}
}
