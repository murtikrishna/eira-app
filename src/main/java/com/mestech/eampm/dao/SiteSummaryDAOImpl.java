
/******************************************************
 * 
 *    	Filename	: SiteSummaryDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for site summary operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.bean.SiteSummaryBean;
import com.mestech.eampm.model.SiteSummary;
import com.mestech.eampm.utility.UtilityCommonDAO;

@Repository
public class SiteSummaryDAOImpl implements SiteSummaryDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();

	// @Override
	public void addSiteSummary(SiteSummary SiteSummary) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(SiteSummary);

	}

	// @Override
	public void updateSiteSummary(SiteSummary SiteSummary) {
		Session session = sessionFactory.getCurrentSession();
		session.update(SiteSummary);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<SiteSummary> listSiteSummarys() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<SiteSummary> SiteSummarysList = session.createQuery("from SiteSummary").list();

		return SiteSummarysList;
	}

	@SuppressWarnings("unchecked")
	public List<SiteSummary> listSiteSummarysByFilter1(int equipmentId, int siteId, int lastNDays) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "from SiteSummary where EquipmentId='" + equipmentId + "' and SiteId='" + siteId
				+ "' and  trunc(timestamp)=trunc(sysdate-" + lastNDays + ") order by Timestamp desc";
		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "from SiteSummary where EquipmentId='" + equipmentId + "' and SiteId='" + siteId
					+ "' and  date_trunc('day',timestamp)=date_trunc('day',now()) - (" + lastNDays
					+ " * interval '1 day') order by Timestamp desc";
		}
		List<SiteSummary> SiteSummarysList = session.createQuery(Query).list();
		return SiteSummarysList;
	}

	public List<SiteSummary> getSiteSummaryListByUserId(int userId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from SiteSummary where SiteID in (Select SiteId from Site where ActiveFlag='1' and CustomerID in (Select CustomerID from CustomerMap where UserID='"
				+ userId
				+ "') or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
				+ userId + "'))";
		List<SiteSummary> SiteSummarysList = session.createQuery(Query).list();

		return SiteSummarysList;
	}

	public List<SiteSummary> getSiteSummaryListByCustomerId(int customerId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from SiteSummary where SiteID in (Select SiteId from Site where ActiveFlag='1' and CustomerID='"
				+ customerId + "')";
		List<SiteSummary> SiteSummarysList = session.createQuery(Query).list();

		return SiteSummarysList;
	}

	public List<SiteSummary> getSiteSummaryListBySiteId(int SiteId) {
		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "from SiteSummary where SiteID = '" + SiteId
				+ "' and TimeStamp <= trunc(sysdate+1) and  TimeStamp > trunc(to_date('01-01-2015','DD-MM-YYYY')) order by TimeStamp";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "from SiteSummary where SiteID = '" + SiteId
					+ "' and TimeStamp <= (date_trunc('day',now()) + (1 * interval '1 day')) and  TimeStamp > date_trunc('day',to_timestamp('01-01-2015','DD-MM-YYYY')) order by TimeStamp";
			Query = "from SiteSummary where SiteID = '" + SiteId
					+ "' and TimeStamp > date_trunc('day',to_timestamp('01-01-2015','DD-MM-YYYY')) order by TimeStamp";

		}
		List<SiteSummary> SiteSummarysList = session.createQuery(Query).list();

		return SiteSummarysList;
	}

	public List<SiteSummary> getSiteSummaryListBySiteIdWithDate(String SiteId, String FromDate, String ToDate) {
		Session session = sessionFactory.getCurrentSession();

		// Oracle
		String BarChartQuery = "Select SiteID as SiteID, SiteName,Timestamp,"
				+ "(case when TodayEnergyFlag=1 then TodayEnergy else (DailyEnergy - Lag(DailyEnergy, 1, 0) OVER (ORDER BY  SiteID,Timestamp)) end) as TodayEnergy "
				+ "from "
				+ "(Select t1.SiteID as SiteID,(t3.SiteName || ' (' || t3.CustomerReference || ')' ) as SiteName,Timestamp,"
				+ "t3.TodayEnergyFlag,TotalEnergy,TodayEnergy,(NVL(TotalEnergy,0) - NVL(YesterdayEnergy,0)) as DailyEnergy "
				+ "from "
				+ "(Select SiteID,Timestamp,NVL(Sum(NVL(TotalEnergy,0)),0) as TotalEnergy,NVL(Sum(NVL(TodayEnergy,0)),0) as TodayEnergy "
				+ "from (Select SiteID,Trunc(Timestamp) as Timestamp,EquipmentID,Max(NVL(TotalEnergy,0)) as TotalEnergy,Max(NVL(TodayEnergy,0)) as TodayEnergy "
				+ "from tDataTransaction where SiteID in (" + SiteId + ")  and Timestamp > to_date('" + FromDate
				+ "','DD/MM/YYYY HH24:MI:SS') and " + "Timestamp <=  to_date('" + ToDate
				+ "','DD/MM/YYYY HH24:MI:SS') group by SiteID,Trunc(Timestamp),EquipmentID) tbl1  "
				+ "group by SiteID,Timestamp) t1 " + "left outer join (Select SiteID, YesterdayEnergy from "
				+ "(Select SiteID,Timestamp,ROW_NUMBER() over(order by Timestamp desc) as Sno,NVL(Sum(NVL(TotalEnergy,0)),0) as YesterdayEnergy "
				+ "from (Select SiteID,Trunc(Timestamp) as Timestamp,EquipmentID,Max(NVL(TotalEnergy,0)) as TotalEnergy,Max(NVL(TodayEnergy,0)) as TodayEnergy "
				+ "from tDataTransaction where SiteID in (" + SiteId + ")  and Timestamp < trunc(to_date('" + FromDate
				+ "','DD/MM/YYYY HH24:MI:SS')) "
				+ "group by SiteID,Trunc(Timestamp),EquipmentID) tbl1 group by SiteID,Timestamp) tbl1 where Sno=1) t2 on t2.SiteID=t1.SiteID "
				+ "left outer join mSite t3 on t3.SiteID=t1.SiteID) tbl order by SiteID,Timestamp";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			BarChartQuery = "Select SiteID as SiteID, SiteName,Timestamp,"
					+ "(case when TodayEnergyFlag=1 then TodayEnergy else (DailyEnergy - Lag(DailyEnergy) OVER (ORDER BY  SiteID,Timestamp)) end) as TodayEnergy "
					+ "from "
					+ "(Select t1.SiteID as SiteID,(t3.SiteName || ' (' || t3.CustomerReference || ')' ) as SiteName,Timestamp,"
					+ "t3.TodayEnergyFlag,TotalEnergy,TodayEnergy,(coalesce(TotalEnergy,0) - coalesce(YesterdayEnergy,0)) as DailyEnergy "
					+ "from "
					+ "(Select SiteID,Timestamp,coalesce(Sum(coalesce(TotalEnergy,0)),0) as TotalEnergy,coalesce(Sum(coalesce(TodayEnergy,0)),0) as TodayEnergy "
					+ "from (Select SiteID,date_trunc('day',Timestamp) as Timestamp,EquipmentID,Max(coalesce(TotalEnergy,0)) as TotalEnergy,Max(coalesce(TodayEnergy,0)) as TodayEnergy "
					+ "from tDataTransaction where SiteID in (" + SiteId + ")  and Timestamp > to_timestamp('"
					+ FromDate + "','DD/MM/YYYY HH24:MI:SS') and " + "Timestamp <=  to_timestamp('" + ToDate
					+ "','DD/MM/YYYY HH24:MI:SS') group by SiteID,date_trunc('day',Timestamp),EquipmentID) tbl1  "
					+ "group by SiteID,Timestamp) t1 " + "left outer join (Select SiteID, YesterdayEnergy from "
					+ "(Select SiteID,Timestamp,ROW_NUMBER() over(order by Timestamp desc) as Sno,coalesce(Sum(coalesce(TotalEnergy,0)),0) as YesterdayEnergy "
					+ "from (Select SiteID,date_trunc('day',Timestamp) as Timestamp,EquipmentID,Max(coalesce(TotalEnergy,0)) as TotalEnergy,Max(coalesce(TodayEnergy,0)) as TodayEnergy "
					+ "from tDataTransaction where SiteID in (" + SiteId
					+ ")  and Timestamp < date_trunc('day',to_timestamp('" + FromDate + "','DD/MM/YYYY HH24:MI:SS')) "
					+ "group by SiteID,date_trunc('day',Timestamp),EquipmentID) tbl1 group by SiteID,Timestamp) tbl1 where Sno=1) t2 on t2.SiteID=t1.SiteID "
					+ "left outer join mSite t3 on t3.SiteID=t1.SiteID) tbl order by SiteID,Timestamp";

			String TimezoneOffset = "";
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				BarChartQuery = "Select SiteID as SiteID, SiteName,Timestamp,"
						+ "(case when TodayEnergyFlag=1 then TodayEnergy else (DailyEnergy - Lag(DailyEnergy) OVER (ORDER BY  SiteID,Timestamp)) end) as TodayEnergy "
						+ "from "
						+ "(Select t1.SiteID as SiteID,(t3.SiteName || ' (' || t3.CustomerReference || ')' ) as SiteName,Timestamp,"
						+ "t3.TodayEnergyFlag,TotalEnergy,TodayEnergy,(coalesce(TotalEnergy,0) - coalesce(YesterdayEnergy,0)) as DailyEnergy "
						+ "from "
						+ "(Select SiteID,Timestamp,coalesce(Sum(coalesce(TotalEnergy,0)),0) as TotalEnergy,coalesce(Sum(coalesce(TodayEnergy,0)),0) as TodayEnergy "
						+ "from (Select SiteID,date_trunc('day',Timestamp) as Timestamp,EquipmentID,Max(coalesce(TotalEnergy,0)) as TotalEnergy,Max(coalesce(TodayEnergy,0)) as TodayEnergy "
						+ "from tDataTransaction where SiteID in (" + SiteId + ")  and Timestamp > to_timestamp('"
						+ FromDate + "','DD/MM/YYYY HH24:MI:SS') and " + "Timestamp <=  to_timestamp('" + ToDate
						+ "','DD/MM/YYYY HH24:MI:SS') group by SiteID,date_trunc('day',Timestamp),EquipmentID) tbl1  "
						+ "group by SiteID,Timestamp) t1 " + "left outer join (Select SiteID, YesterdayEnergy from "
						+ "(Select SiteID,Timestamp,ROW_NUMBER() over(order by Timestamp desc) as Sno,coalesce(Sum(coalesce(TotalEnergy,0)),0) as YesterdayEnergy "
						+ "from (Select SiteID,date_trunc('day',Timestamp) as Timestamp,EquipmentID,Max(coalesce(TotalEnergy,0)) as TotalEnergy,Max(coalesce(TodayEnergy,0)) as TodayEnergy "
						+ "from tDataTransaction where SiteID in (" + SiteId
						+ ")  and Timestamp < date_trunc('day',to_timestamp('" + FromDate
						+ "','DD/MM/YYYY HH24:MI:SS')) "
						+ "group by SiteID,date_trunc('day',Timestamp),EquipmentID) tbl1 group by SiteID,Timestamp) tbl1 where Sno=1) t2 on t2.SiteID=t1.SiteID "
						+ "left outer join mSite t3 on t3.SiteID=t1.SiteID) tbl order by SiteID,Timestamp";

			}

		}

		List<SiteSummary> SiteSummarysList = session.createSQLQuery(BarChartQuery).list();

		return SiteSummarysList;
	}

	public List<SiteSummary> getSiteSummaryListBySiteIdsGroupWithDate(String siteIds, String FromDate, String ToDate) {
		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "Select '1' as SummaryID,'0' as SiteID,Timestamp,SUM(TodayEnergy) as TodayEnergy from SiteSummary where SiteID in ("
				+ siteIds + ") and Timestamp <= to_date('" + ToDate
				+ "','DD/MM/YYYY HH24:MI:SS') and  Timestamp >= to_date('" + FromDate
				+ "','DD/MM/YYYY HH24:MI:SS')   group by Timestamp order by Timestamp";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select '1' as SummaryID,'0' as SiteID,Timestamp,SUM(TodayEnergy) as TodayEnergy from SiteSummary where SiteID in ("
					+ siteIds + ") and Timestamp <= to_timestamp('" + ToDate
					+ "','DD/MM/YYYY HH24:MI:SS') and  Timestamp >= to_timestamp('" + FromDate
					+ "','DD/MM/YYYY HH24:MI:SS')   group by Timestamp order by Timestamp";
		}
		List<SiteSummary> SiteSummarysList = session.createQuery(Query).list();
		return SiteSummarysList;
	}

	public List<SiteSummary> getSiteSummaryListBySiteIdsGroup(String siteIds) {
		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "Select '1' as SummaryID,'0' as SiteID,Timestamp,SUM(TodayEnergy) as TodayEnergy from SiteSummary where SiteID in ("
				+ siteIds
				+ ") and Timestamp <= trunc(sysdate+1) and  Timestamp > trunc(to_date('01-01-2017','DD-MM-YYYY'))  group by Timestamp order by Timestamp";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select '1' as SummaryID,'0' as SiteID,Timestamp,SUM(TodayEnergy) as TodayEnergy from SiteSummary where SiteID in ("
					+ siteIds
					+ ") and Timestamp <= (date_trunc('day',now()) + (1 * interval '1 day')) and  Timestamp > date_trunc('day',to_timestamp('01-01-2017','DD-MM-YYYY'))  group by Timestamp order by Timestamp";
		}
		List<SiteSummary> SiteSummarysList = session.createQuery(Query).list();
		return SiteSummarysList;
	}

	public List<SiteSummary> getSiteSummaryListByEquipmentId(int equipmentId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from SiteSummary order by TimeStamp";
		List<SiteSummary> SiteSummarysList = session.createQuery(Query).list();

		return SiteSummarysList;
	}

	// @Override
	public SiteSummary getSiteSummaryById(int id) {
		Session session = sessionFactory.getCurrentSession();
		SiteSummary SiteSummary = (SiteSummary) session.get(SiteSummary.class, new Integer(id));
		return SiteSummary;
	}

	public SiteSummary getSiteSummaryByMax(String MaxColumnName) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from SiteSummary Order By " + MaxColumnName + " desc";
		SiteSummary SiteSummary = (SiteSummary) session.createQuery(Query).setMaxResults(1).getSingleResult();
		return SiteSummary;
	}

	// @Override
	public void removeSiteSummary(int id) {
		Session session = sessionFactory.getCurrentSession();
		SiteSummary SiteSummary = (SiteSummary) session.get(SiteSummary.class, new Integer(id));

		// De-activate the flag
		SiteSummary.setActiveFlag(0);

		if (null != SiteSummary) {
			// session.delete(SiteSummary);

			session.update(SiteSummary);
		}
	}
}
