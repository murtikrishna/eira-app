
/******************************************************
 * 
 *    	Filename	: ParameterListDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Parameter list related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.ParameterList;

public interface ParameterListDAO {

	public void addParameterList(ParameterList parameterlist);

	public void updateParameterList(ParameterList parameterlist);

	public ParameterList getParameterListById(int id);

	public void removeParameterList(int id);

	public List<ParameterList> listParameterLists();
}
