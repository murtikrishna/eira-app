
/******************************************************
 * 
 *    	Filename	: EquipmentParameterDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Equipment parameter operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.EquipmentParameter;

@Repository
public class EquipmentParameterDAOImpl implements EquipmentParameterDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addEquipmentParameter(EquipmentParameter equipmentparameter) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(equipmentparameter);

	}

	// @Override
	public void updateEquipmentParameter(EquipmentParameter equipmentparameter) {
		Session session = sessionFactory.getCurrentSession();
		session.update(equipmentparameter);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<EquipmentParameter> listEquipmentParameters() {
		Session session = sessionFactory.getCurrentSession();
		List<EquipmentParameter> EquipmentParametersList = session.createQuery("from EquipmentParameter").list();

		return EquipmentParametersList;
	}

	// @Override
	public EquipmentParameter getEquipmentParameterById(int id) {
		Session session = sessionFactory.getCurrentSession();
		EquipmentParameter equipmentparameter = (EquipmentParameter) session.get(EquipmentParameter.class,
				new Integer(id));
		return equipmentparameter;
	}

	// @Override
	public void removeEquipmentParameter(int id) {
		Session session = sessionFactory.getCurrentSession();
		EquipmentParameter equipmentparameter = (EquipmentParameter) session.get(EquipmentParameter.class,
				new Integer(id));
		if (null != equipmentparameter) {
			session.delete(equipmentparameter);
		}
	}
}
