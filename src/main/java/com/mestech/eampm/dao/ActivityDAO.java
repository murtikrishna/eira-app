
/******************************************************
 * 
 *    	Filename	: AccessListBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Activity operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.Activity;

public interface ActivityDAO {

	public void addActivity(Activity activity);

	public void updateActivity(Activity activity);

	public Activity getActivityById(int id);

	public void removeActivity(int id);

	public List<Activity> listActivities();

	public Activity getActivityName(int Id);
}
