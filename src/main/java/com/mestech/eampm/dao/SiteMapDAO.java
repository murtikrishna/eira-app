
/******************************************************
 * 
 *    	Filename	: SiteMapDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for site map operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.SiteMap;
import com.mestech.eampm.utility.Response;
import com.mestech.eampm.utility.SiteMapPojo;

public interface SiteMapDAO {

	public void addSiteMap(SiteMap sitemap);

	public void updateSiteMap(SiteMap sitemap);

	public SiteMap getSiteMapById(int id);

	public void removeSiteMap(int id);

	public List<SiteMap> listSiteMaps();

	public List<SiteMap> listSiteMaps(int mapid);

	public Response updateSiteMapping(SiteMapPojo siteMap);

	public Response getSiteMapping();
}
