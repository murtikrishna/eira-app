
/******************************************************
 * 
 *    	Filename	: CustomerViewDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Customer View related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

public interface CustomerViewDAO {

}
