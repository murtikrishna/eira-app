
/******************************************************
 * 
 *    	Filename	: CustomerTypeDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Customer Type related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.CustomerType;

public interface CustomerTypeDAO {

	public void addCustomerType(CustomerType customertype);

	public void updateCustomerType(CustomerType customertype);

	public CustomerType getCustomerTypeById(int id);

	public void removeCustomerType(int id);

	public List<CustomerType> listCustomerTypes();
}
