
/******************************************************
 * 
 *    	Filename	: CountryDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Country related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.Country;

public interface CountryDAO {

	public void addCountry(Country country);

	public void updateCountry(Country country);

	public Country getCountryById(int id);

	public void removeCountry(int id);

	public List<Country> listCountries();
}
