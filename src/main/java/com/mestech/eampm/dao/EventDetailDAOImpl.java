
/******************************************************
 * 
 *    	Filename	: EventDetailDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Event detail related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.hibernate.query.*;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.bean.OpenStateTicketsCount;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.DataTransaction;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.TicketTransaction;
import com.mestech.eampm.utility.UtilityCommon;
import com.mestech.eampm.utility.UtilityCommonDAO;
import com.mestech.eampm.model.EventDetail;

@Repository
public class EventDetailDAOImpl implements EventDetailDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();
	private UtilityCommon utilityCommon = new UtilityCommon();

	// @Override
	public void addEventDetail(EventDetail eventdetail) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(eventdetail);

	}

	// @Override
	public void updateEventDetail(EventDetail eventdetail) {
		Session session = sessionFactory.getCurrentSession();
		session.update(eventdetail);
	}

	// @Override
	public void removeEvent(int id) {
		Session session = sessionFactory.getCurrentSession();
		EventDetail eventdetail = (EventDetail) session.get(EventDetail.class, new Integer(id));

		// De-activate the flag
		eventdetail.setActiveFlag(0);

		if (null != eventdetail) {
			session.update(eventdetail);
			// session.delete(activity);
		}
	}

	// @Override
	public void removeEventDetail(int id) {
		Session session = sessionFactory.getCurrentSession();
		EventDetail eventdetail = (EventDetail) session.get(EventDetail.class, new Integer(id));

		// De-activate the flag
		eventdetail.setActiveFlag(0);

		if (null != eventdetail) {
			// session.delete(site);
			session.update(eventdetail);
		}
	}

	private List<EventDetail> GetCastedEventDetailList(List<EventDetail> lstEventDetail) {
		List<EventDetail> lstEventDetailCasted = new ArrayList<EventDetail>();

		if (lstEventDetail.size() > 0) {

			Integer transactionid = 0;
			Integer eventid = 0;
			Integer errorid = 0;
			Integer equipmentid = 0;
			Integer siteid = 0;
			Integer severity = 0;
			Integer priority = 0;
			String remarks = "";
			Date eventtimestamp = null;
			Date closedtimestamp = null;
			Integer eventoccurrence = 0;
			Integer eventstatus = 0;
			Integer activeflag = 0;
			Date creationdate = null;
			Integer createdby = 0;
			Date lastupdateddate = null;
			Integer lastupdatedby = 0;
			Date lasteventtimestamp = null;

			for (Object object : lstEventDetail) {
				Object[] obj = (Object[]) object;

				transactionid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[0]);
				eventid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[1]);
				errorid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[2]);
				equipmentid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[3]);
				siteid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[4]);
				severity = utilityCommon.IntegerFromShortObject(obj[5]);
				if (obj[6] != null) {

					priority = utilityCommon.IntegerFromShortObject(obj[6]);
				}
				remarks = utilityCommon.StringFromStringObject(obj[7]);
				eventtimestamp = utilityCommon.DateFromDateObject(obj[8]);
				closedtimestamp = utilityCommon.DateFromDateObject(obj[9]);
				eventoccurrence = utilityCommon.IntegerFromShortObject(obj[10]);
				eventstatus = utilityCommon.IntegerFromShortObject(obj[11]);
				activeflag = utilityCommon.IntegerFromShortObject(obj[12]);
				creationdate = utilityCommon.DateFromDateObject(obj[13]);
				createdby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[14]);
				lastupdateddate = utilityCommon.DateFromDateObject(obj[15]);
				lastupdatedby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[16]);
				lasteventtimestamp = utilityCommon.DateFromDateObject(obj[17]);

				EventDetail objEventDetailCasted = new EventDetail();
				objEventDetailCasted.setTransactionId(transactionid);
				objEventDetailCasted.setEventId(eventid);
				objEventDetailCasted.setErrorId(errorid);
				objEventDetailCasted.setEquipmentId(equipmentid);
				objEventDetailCasted.setSiteID(siteid);
				objEventDetailCasted.setSeverity(severity);
				objEventDetailCasted.setPriority(priority);
				objEventDetailCasted.setRemarks(remarks);
				objEventDetailCasted.setEventTimestamp(eventtimestamp);
				objEventDetailCasted.setClosedTimestamp(closedtimestamp);
				objEventDetailCasted.setEventOccurrence(eventoccurrence);
				objEventDetailCasted.setEventStatus(eventstatus);
				objEventDetailCasted.setActiveFlag(activeflag);
				objEventDetailCasted.setCreationDate(creationdate);
				objEventDetailCasted.setCreatedBy(createdby);
				objEventDetailCasted.setLastUpdatedDate(lastupdateddate);
				objEventDetailCasted.setLastUpdatedBy(lastupdatedby);
				objEventDetailCasted.setLastEventTimestamp(lasteventtimestamp);

				lstEventDetailCasted.add(objEventDetailCasted);

			}
		}

		return lstEventDetailCasted;
	}

	private List<EventDetail> GetCastedEventDetailJoinList(List<EventDetail> lstEventDetail) {
		List<EventDetail> lstEventDetailCasted = new ArrayList<EventDetail>();

		if (lstEventDetail.size() > 0) {

			Integer transactionid = 0;
			Integer eventid = 0;
			Integer errorid = 0;
			Integer equipmentid = 0;
			Integer siteid = 0;
			Integer severity = 0;
			Integer priority = 0;
			String remarks = "";
			Date eventtimestamp = null;
			Date closedtimestamp = null;
			Integer eventoccurrence = 0;
			Integer eventstatus = 0;
			Integer activeflag = 0;
			Date creationdate = null;
			Integer createdby = 0;
			Date lastupdateddate = null;
			Integer lastupdatedby = 0;
			Date lasteventtimestamp = null;

			String SiteName = "";
			String EquipmentName = "";
			String ErrorCode = "";
			String ErrorMessage = "";
			String EquipmentType = "";
			String ErrorDescription = "";
			String EventTimestampText = "";
			String ClosedTimestampText = "";
			String LastEventTimestampText = "";
			String CustomerNaming = "";
			Double Capacity = 0.0;

			for (Object object : lstEventDetail) {
				Object[] obj = (Object[]) object;

				transactionid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[0]);
				eventid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[1]);
				errorid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[2]);
				equipmentid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[3]);
				siteid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[4]);
				severity = utilityCommon.IntegerFromShortObject(obj[5]);
				if (obj[6] != null) {

					priority = utilityCommon.IntegerFromShortObject(obj[6]);
				}

				remarks = utilityCommon.StringFromStringObject(obj[7]);
				eventtimestamp = utilityCommon.DateFromDateObject(obj[8]);
				closedtimestamp = utilityCommon.DateFromDateObject(obj[9]);
				eventoccurrence = utilityCommon.IntegerFromShortObject(obj[10]);
				eventstatus = utilityCommon.IntegerFromShortObject(obj[11]);
				activeflag = utilityCommon.IntegerFromShortObject(obj[12]);
				creationdate = utilityCommon.DateFromDateObject(obj[13]);
				createdby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[14]);
				lastupdateddate = utilityCommon.DateFromDateObject(obj[15]);
				lastupdatedby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[16]);
				lasteventtimestamp = utilityCommon.DateFromDateObject(obj[17]);
				SiteName = utilityCommon.StringFromStringObject(obj[18]);
				EquipmentName = utilityCommon.StringFromStringObject(obj[19]);
				ErrorCode = utilityCommon.StringFromStringObject(obj[20]);
				ErrorMessage = utilityCommon.StringFromStringObject(obj[21]);
				EquipmentType = utilityCommon.StringFromStringObject(obj[22]);
				ErrorDescription = utilityCommon.StringFromStringObject(obj[23]);
				EventTimestampText = utilityCommon.StringFromStringObject(obj[24]);
				ClosedTimestampText = utilityCommon.StringFromStringObject(obj[25]);
				LastEventTimestampText = utilityCommon.StringFromStringObject(obj[26]);
				CustomerNaming = utilityCommon.StringFromStringObject(obj[27]);
				Capacity = utilityCommon.DoubleFromBigDecimalObject(obj[28]);

				EventDetail objEventDetailCasted = new EventDetail();
				objEventDetailCasted.setTransactionId(transactionid);
				objEventDetailCasted.setEventId(eventid);
				objEventDetailCasted.setErrorId(errorid);
				objEventDetailCasted.setEquipmentId(equipmentid);
				objEventDetailCasted.setSiteID(siteid);
				objEventDetailCasted.setSeverity(severity);
				objEventDetailCasted.setPriority(priority);
				objEventDetailCasted.setRemarks(remarks);
				objEventDetailCasted.setEventTimestamp(eventtimestamp);
				objEventDetailCasted.setClosedTimestamp(closedtimestamp);
				objEventDetailCasted.setEventOccurrence(eventoccurrence);
				objEventDetailCasted.setEventStatus(eventstatus);
				objEventDetailCasted.setActiveFlag(activeflag);
				objEventDetailCasted.setCreationDate(creationdate);
				objEventDetailCasted.setCreatedBy(createdby);
				objEventDetailCasted.setLastUpdatedDate(lastupdateddate);
				objEventDetailCasted.setLastUpdatedBy(lastupdatedby);
				objEventDetailCasted.setLastEventTimestamp(lasteventtimestamp);

				objEventDetailCasted.setSiteName(SiteName);
				objEventDetailCasted.setEquipmentName(EquipmentName);
				objEventDetailCasted.setErrorCode(ErrorCode);
				objEventDetailCasted.setErrorMessage(ErrorMessage);
				objEventDetailCasted.setEquipmentType(EquipmentType);
				objEventDetailCasted.setErrorDescription(ErrorDescription);
				objEventDetailCasted.setEventTimestampText(EventTimestampText);
				objEventDetailCasted.setClosedTimestampText(ClosedTimestampText);
				objEventDetailCasted.setLastEventTimestampText(LastEventTimestampText);

				objEventDetailCasted.setCustomerNaming(CustomerNaming);
				objEventDetailCasted.setCapacity(String.format("%.1f", Capacity));

				lstEventDetailCasted.add(objEventDetailCasted);

			}
		}

		return lstEventDetailCasted;
	}

	private EventDetail GetCastedEventDetail(Object objEventDetail) {
		EventDetail objEventDetailCasted = new EventDetail();
		System.out.println("mohan::1");
		if (objEventDetail != null) {

			Integer transactionid = 0;
			Integer eventid = 0;
			Integer errorid = 0;
			Integer equipmentid = 0;
			Integer siteid = 0;
			Integer severity = 0;
			Integer priority = 0;
			String remarks = "";
			Date eventtimestamp = null;
			Date closedtimestamp = null;
			Integer eventoccurrence = 0;
			Integer eventstatus = 0;
			Integer activeflag = 0;
			Date creationdate = null;
			Integer createdby = 0;
			Date lastupdateddate = null;
			Integer lastupdatedby = 0;
			Date lasteventtimestamp = null;

			Object[] obj = (Object[]) objEventDetail;

			transactionid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[0]);
			eventid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[1]);
			errorid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[2]);
			equipmentid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[3]);
			siteid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[4]);
			severity = utilityCommon.IntegerFromShortObject(obj[5]);
			if (obj[6] != null) {

				priority = utilityCommon.IntegerFromShortObject(obj[6]);
			}

			remarks = utilityCommon.StringFromStringObject(obj[7]);
			eventtimestamp = utilityCommon.DateFromDateObject(obj[8]);
			closedtimestamp = utilityCommon.DateFromDateObject(obj[9]);
			eventoccurrence = utilityCommon.IntegerFromShortObject(obj[10]);
			eventstatus = utilityCommon.IntegerFromShortObject(obj[11]);
			activeflag = utilityCommon.IntegerFromShortObject(obj[12]);
			creationdate = utilityCommon.DateFromDateObject(obj[13]);
			createdby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[14]);
			lastupdateddate = utilityCommon.DateFromDateObject(obj[15]);
			lastupdatedby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[16]);
			lasteventtimestamp = utilityCommon.DateFromDateObject(obj[17]);

			objEventDetailCasted.setTransactionId(transactionid);
			objEventDetailCasted.setEventId(eventid);
			objEventDetailCasted.setErrorId(errorid);
			objEventDetailCasted.setEquipmentId(equipmentid);
			objEventDetailCasted.setSiteID(siteid);
			objEventDetailCasted.setSeverity(severity);
			objEventDetailCasted.setPriority(priority);
			objEventDetailCasted.setRemarks(remarks);
			objEventDetailCasted.setEventTimestamp(eventtimestamp);
			objEventDetailCasted.setClosedTimestamp(closedtimestamp);
			objEventDetailCasted.setEventOccurrence(eventoccurrence);
			objEventDetailCasted.setEventStatus(eventstatus);
			objEventDetailCasted.setActiveFlag(activeflag);
			objEventDetailCasted.setCreationDate(creationdate);
			objEventDetailCasted.setCreatedBy(createdby);
			objEventDetailCasted.setLastUpdatedDate(lastupdateddate);
			objEventDetailCasted.setLastUpdatedBy(lastupdatedby);
			objEventDetailCasted.setLastEventTimestamp(lasteventtimestamp);

		}

		return objEventDetailCasted;
	}

	public String GetSystemTimeWithOffsetByMins(String TimezoneOffset) {
		SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date AppServerDate = new Date();

		Calendar cal = new GregorianCalendar();
		cal.setTime(AppServerDate);
		Integer OffsetInMins = -1 * Integer.valueOf(TimezoneOffset);
		cal.add(Calendar.MINUTE, OffsetInMins);
		return sdfdate.format(cal.getTime());
	}

	public String GetSystemTimeWithOffsetByDate(Integer OffsetInMins) {
		SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date AppServerDate = new Date();

		Calendar cal = new GregorianCalendar();
		cal.setTime(AppServerDate);
		cal.add(Calendar.DATE, OffsetInMins);
		return sdfdate.format(cal.getTime());
	}

	@Override
	public EventDetail getEventDetailById(int id, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		EventDetail eventdetail = null;

		try {
			// Oracle
			String tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , eventtimestamp , closedtimestamp,eventoccurrence ,eventstatus , activeflag , creationdate,createdby, lastupdateddate ,lastupdatedby , lasteventtimestamp";
			String Query = "Select " + tablecolumnnames + " from tEventDetail where TransactionID='" + id
					+ "' and rownum=1";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , (eventtimestamp"
							+ TimeConversionFromUTC + ") as  eventtimestamp ,(closedtimestamp" + TimeConversionFromUTC
							+ ") as  closedtimestamp,eventoccurrence ,eventstatus , activeflag ,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby ,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lasteventtimestamp";

				}

				Query = "Select " + tablecolumnnames + " from tEventDetail where TransactionID='" + id + "' limit 1";
			}
			Object EventDetailObject = session.createSQLQuery(Query).list().get(0);
			eventdetail = GetCastedEventDetail(EventDetailObject);

		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetail;

	}

	// @Override
	public EventDetail getEventDetailByErrorId(Integer ErrorId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		EventDetail eventdetail = null;

		try {
			// Oracle
			String tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , eventtimestamp , closedtimestamp,eventoccurrence ,eventstatus , activeflag , creationdate,createdby, lastupdateddate ,lastupdatedby , lasteventtimestamp";
			String Query = "Select " + tablecolumnnames + " from tEventDetail  where ErrorId='" + ErrorId
					+ "' and rownum=1";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , (eventtimestamp"
							+ TimeConversionFromUTC + ") as  eventtimestamp ,(closedtimestamp" + TimeConversionFromUTC
							+ ") as  closedtimestamp,eventoccurrence ,eventstatus , activeflag ,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby ,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lasteventtimestamp";

				}
				Query = "Select " + tablecolumnnames + " from tEventDetail  where ErrorId='" + ErrorId + "' limit 1";

			}
			Object EventDetailObject = session.createSQLQuery(Query).list().get(0);
			eventdetail = GetCastedEventDetail(EventDetailObject);
		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetail;

	}

	// @Override
	public EventDetail getEventDetailByMax(String MaxColumnName, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		EventDetail eventdetail = null;

		try {
			// Oracle
			String tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , eventtimestamp , closedtimestamp,eventoccurrence ,eventstatus , activeflag , creationdate,createdby, lastupdateddate ,lastupdatedby , lasteventtimestamp";
			String Query = "Select " + tablecolumnnames + " from tEventDetail  Order By " + MaxColumnName + " desc";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , (eventtimestamp"
							+ TimeConversionFromUTC + ") as  eventtimestamp ,(closedtimestamp" + TimeConversionFromUTC
							+ ") as  closedtimestamp,eventoccurrence ,eventstatus , activeflag ,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby ,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lasteventtimestamp";

				}
				Query = "Select " + tablecolumnnames + " from tEventDetail  Order By " + MaxColumnName
						+ " desc limit 1";

			}

			Object EventDetailObject = session.createSQLQuery(Query).list().get(0);
			eventdetail = GetCastedEventDetail(EventDetailObject);

		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetail;

	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<EventDetail> listEventDetails(String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> eventdetaillist = null;

		try {
			// Oracle
			String tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , eventtimestamp , closedtimestamp,eventoccurrence ,eventstatus , activeflag , creationdate,createdby, lastupdateddate ,lastupdatedby , lasteventtimestamp";
			String Query = "Select " + tablecolumnnames + " from tEventDetail";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , (eventtimestamp"
							+ TimeConversionFromUTC + ") as  eventtimestamp ,(closedtimestamp" + TimeConversionFromUTC
							+ ") as  closedtimestamp,eventoccurrence ,eventstatus , activeflag ,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby ,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lasteventtimestamp";

				}
				Query = "Select " + tablecolumnnames + " from tEventDetail";
			}
			List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
			eventdetaillist = GetCastedEventDetailList(EventDetailListObject);
		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetaillist;

	}

	public List<EventDetail> getEventDetailListByUserId_Old(int userId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> eventdetaillist = null;

		try {
			// Oracle
			String tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , eventtimestamp , closedtimestamp,eventoccurrence ,eventstatus , activeflag , creationdate,createdby, lastupdateddate ,lastupdatedby , lasteventtimestamp";
			String Query = "Select " + tablecolumnnames
					+ " from tEventDetail where  ActiveFlag='1' and eventTimestamp >= trunc(sysdate-30) and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
					+ userId + "')) order by eventTimestamp desc";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {

				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , (eventtimestamp"
							+ TimeConversionFromUTC + ") as  eventtimestamp ,(closedtimestamp" + TimeConversionFromUTC
							+ ") as  closedtimestamp,eventoccurrence ,eventstatus , activeflag ,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby ,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lasteventtimestamp";

				}

				Query = "Select " + tablecolumnnames
						+ " from tEventDetail where  ActiveFlag='1' and eventTimestamp >= date_trunc('day',now() - (30 * interval '1 day')) and SiteID in (Select SiteId from mSite where CustomerID in(Select CustomerID from mcustomer where customerid in (Select CustomerID from mCustomerMap where UserID='"
						+ userId
						+ "') or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
						+ userId + "'))) order by eventTimestamp desc";

			}

			List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
			eventdetaillist = GetCastedEventDetailList(EventDetailListObject);

		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetaillist;

	}

	public List<EventDetail> getEventDetailListByUserId(int userId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> eventdetaillist = null;

		try {
			// Oracle
			String Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription, to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= trunc(sysdate-30) and a.SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
					+ userId + "' and Activeflag=1)) order by a.eventTimestamp desc";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day', now() - (30 * interval '1 day')) and a.SiteID in (Select SiteId from mSiteMap where UserID='"
						+ userId + "' and ActiveFlag=1) order by a.eventTimestamp desc";

				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
							+ TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" + TimeConversionFromUTC
							+ ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
							+ TimeConversionFromUTC + ") as  creationdate,a.createdby, (a.lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp"
							+ TimeConversionFromUTC
							+ ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp "
							+ TimeConversionFromUTC + " >= date_trunc('day',now()" + TimeConversionFromUTC
							+ " - (30 * interval '1 day')) and a.SiteID in (Select SiteId from mSiteMap where UserID='"
							+ userId + "' and ActiveFlag=1) order by a.eventTimestamp desc";

				}

			}

			List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
			eventdetaillist = GetCastedEventDetailJoinList(EventDetailListObject);
		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetaillist;

	}

	public List<EventDetail> getTodayEventDetailListByUserId(int userId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> eventdetaillist = null;

		try {
			// Oracle
			String tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , eventtimestamp , closedtimestamp,eventoccurrence ,eventstatus , activeflag , creationdate,createdby, lastupdateddate ,lastupdatedby , lasteventtimestamp";
			String Query = "Select " + tablecolumnnames
					+ " from tEventDetail where trunc(EventTimestamp)=trunc(sysdate) and ActiveFlag='1' and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
					+ userId + "' and Activeflag=1))";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , (eventtimestamp"
							+ TimeConversionFromUTC + ") as  eventtimestamp ,(closedtimestamp" + TimeConversionFromUTC
							+ ") as  closedtimestamp,eventoccurrence ,eventstatus , activeflag ,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby ,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lasteventtimestamp";
				}

				Query = "Select " + tablecolumnnames
						+ " from tEventDetail where date_trunc('day',EventTimestamp)=date_trunc('day',now()) and ActiveFlag='1' and SiteID in (Select SiteId from mSiteMap where UserID='"
						+ userId + "' and ActiveFlag=1)";

			}

			List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
			eventdetaillist = GetCastedEventDetailList(EventDetailListObject);

		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetaillist;

	}

	public List<EventDetail> getTodayEventDetailListByCustomerId(int customerId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> eventdetaillist = null;

		try {
			// Oracle
			String tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , eventtimestamp , closedtimestamp,eventoccurrence ,eventstatus , activeflag , creationdate,createdby, lastupdateddate ,lastupdatedby , lasteventtimestamp";
			String Query = "Select " + tablecolumnnames
					+ " from tEventDetail where trunc(EventTimestamp)=trunc(sysdate) and ActiveFlag='1' and SiteID in (Select SiteId from mSite where CustomerID in ("
					+ customerId + "))";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , (eventtimestamp"
							+ TimeConversionFromUTC + ") as  eventtimestamp ,(closedtimestamp" + TimeConversionFromUTC
							+ ") as  closedtimestamp,eventoccurrence ,eventstatus , activeflag ,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby ,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lasteventtimestamp";

				}

				Query = "Select " + tablecolumnnames
						+ " from tEventDetail where date_trunc('day',EventTimestamp)=date_trunc('day',now()) and ActiveFlag='1' and SiteID in (Select SiteId from mSite where mCustomerID in ("
						+ customerId + "))";

			}

			List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
			eventdetaillist = GetCastedEventDetailList(EventDetailListObject);

		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetaillist;

	}

	public List<EventDetail> getEventDetailListBySiteId(int siteId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> eventdetaillist = null;

		try {
			// Oracle
			String Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription, to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= trunc(sysdate-30) and a.SiteID ='"
					+ siteId + "' order by a.eventTimestamp desc";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day', now() - (60 * interval '1 day')) and a.SiteID ='"
						+ siteId + "' order by a.eventTimestamp desc";

				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
							+ TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" + TimeConversionFromUTC
							+ ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
							+ TimeConversionFromUTC + ") as  creationdate,a.createdby, (a.lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp"
							+ TimeConversionFromUTC
							+ ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day',now()"
							+ TimeConversionFromUTC + " - (60 * interval '1 day')) and a.SiteID ='" + siteId
							+ "' order by a.eventTimestamp desc";

				}

			}

			List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
			eventdetaillist = GetCastedEventDetailJoinList(EventDetailListObject);
		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetaillist;

		/*
		 * Session session = sessionFactory.getCurrentSession(); List<EventDetail>
		 * eventdetaillist = null;
		 * 
		 * try { //Oracle String tablecolumnnames
		 * ="transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , eventtimestamp , closedtimestamp,eventoccurrence ,eventstatus , activeflag , creationdate,createdby, lastupdateddate ,lastupdatedby , lasteventtimestamp"
		 * ; String Query = "Select " + tablecolumnnames +
		 * " from tEventDetail where ActiveFlag='1' and SiteID='" + siteId + "'";
		 * 
		 * 
		 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
		 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
		 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
		 * tablecolumnnames
		 * ="transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , (eventtimestamp"
		 * + TimeConversionFromUTC + ") as  eventtimestamp ,(closedtimestamp" +
		 * TimeConversionFromUTC +
		 * ") as  closedtimestamp,eventoccurrence ,eventstatus , activeflag ,(creationdate"
		 * + TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" +
		 * TimeConversionFromUTC +
		 * ") as lastupdateddate ,lastupdatedby ,(lastupdateddate" +
		 * TimeConversionFromUTC + ") as lasteventtimestamp"; } Query = "Select " +
		 * tablecolumnnames + " from tEventDetail where ActiveFlag='1' and SiteID='" +
		 * siteId + "'";
		 * 
		 * }
		 * 
		 * 
		 * List<EventDetail> EventDetailListObject =
		 * session.createSQLQuery(Query).list(); eventdetaillist =
		 * GetCastedEventDetailList(EventDetailListObject); } catch(NoResultException
		 * ex) {
		 * 
		 * } catch(Exception ex) {
		 * 
		 * }
		 * 
		 * 
		 * return eventdetaillist;
		 */

	}

	public List<EventDetail> getTodayEventDetailListBySiteId(int siteId, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> eventdetaillist = null;

		try {
			// Oracle
			String tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , eventtimestamp , closedtimestamp,eventoccurrence ,eventstatus , activeflag , creationdate,createdby, lastupdateddate ,lastupdatedby , lasteventtimestamp";
			String Query = "Select " + tablecolumnnames
					+ " from tEventDetail where trunc(EventTimestamp)=trunc(sysdate) and ActiveFlag='1' and SiteID='"
					+ siteId + "'";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , (eventtimestamp"
							+ TimeConversionFromUTC + ") as  eventtimestamp ,(closedtimestamp" + TimeConversionFromUTC
							+ ") as  closedtimestamp,eventoccurrence ,eventstatus , activeflag ,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby ,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lasteventtimestamp";
				}

				Query = "Select " + tablecolumnnames
						+ " from tEventDetail where date_trunc('day',EventTimestamp)=date_trunc('day',now()) and ActiveFlag='1' and SiteID='"
						+ siteId + "'";

			}

			List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
			eventdetaillist = GetCastedEventDetailList(EventDetailListObject);
		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetaillist;

	}

	public List<EventDetail> getEventDetailListByCustomerId(int customerId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> eventdetaillist = null;

		try {
			// Oracle
			String Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription, to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= trunc(sysdate-30) and a.SiteID in (Select SiteId from mSite where CustomerID='"
					+ customerId + "') order by a.eventTimestamp desc";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day', now() - (30 * interval '1 day')) and a.SiteID in (Select SiteId from mSitemap where UserID ='"
						+ customerId + "') order by a.eventTimestamp desc";

				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
							+ TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" + TimeConversionFromUTC
							+ ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
							+ TimeConversionFromUTC + ") as  creationdate,a.createdby, (a.lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp"
							+ TimeConversionFromUTC
							+ ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day',now()"
							+ TimeConversionFromUTC
							+ " - (30 * interval '1 day')) and a.SiteID in (Select SiteId from mSitemap where UserID ='"
							+ customerId + "') order by a.eventTimestamp desc";

				}

			}

			List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
			eventdetaillist = GetCastedEventDetailJoinList(EventDetailListObject);
		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetaillist;

		/*
		 * Session session = sessionFactory.getCurrentSession(); List<EventDetail>
		 * eventdetaillist = null;
		 * 
		 * try { //Oracle String tablecolumnnames
		 * ="transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , eventtimestamp , closedtimestamp,eventoccurrence ,eventstatus , activeflag , creationdate,createdby, lastupdateddate ,lastupdatedby , lasteventtimestamp"
		 * ; String Query = "Select " + tablecolumnnames +
		 * " from tEventDetail where  ActiveFlag='1' and SiteID in (Select SiteId from mSite where CustomerID ='"
		 * + customerId + "')";
		 * 
		 * 
		 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
		 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
		 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
		 * tablecolumnnames
		 * ="transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , (eventtimestamp"
		 * + TimeConversionFromUTC + ") as  eventtimestamp ,(closedtimestamp" +
		 * TimeConversionFromUTC +
		 * ") as  closedtimestamp,eventoccurrence ,eventstatus , activeflag ,(creationdate"
		 * + TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" +
		 * TimeConversionFromUTC +
		 * ") as lastupdateddate ,lastupdatedby ,(lastupdateddate" +
		 * TimeConversionFromUTC + ") as lasteventtimestamp"; } Query = "Select " +
		 * tablecolumnnames +
		 * " from tEventDetail where  ActiveFlag='1' and SiteID in (Select SiteId from mSite where CustomerID ='"
		 * + customerId + "')";
		 * 
		 * }
		 * 
		 * List<EventDetail> EventDetailListObject =
		 * session.createSQLQuery(Query).list(); eventdetaillist =
		 * GetCastedEventDetailList(EventDetailListObject); } catch(NoResultException
		 * ex) {
		 * 
		 * } catch(Exception ex) {
		 * 
		 * }
		 * 
		 * 
		 * return eventdetaillist;
		 */

	}

// 03-02-19

	/*
	 * @SuppressWarnings("unchecked") public List<EventDetail>
	 * listEventDetailsForDisplay_Old(int userid,String siteId,String
	 * fromDate,String toDate,int eventcode, int errorid, String equipmentid, String
	 * priority, int severity,String TimezoneOffset) {
	 * 
	 * 
	 * Session session = sessionFactory.getCurrentSession(); List<EventDetail>
	 * eventdetaillist = null; String WhereCondition = ""; String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * 
	 * 
	 * if(siteId!=null && siteId!="" && siteId!="null") { WhereCondition =
	 * WhereCondition + " and SiteID ='" + siteId + "' "; } if(fromDate!=null &&
	 * toDate!=null && fromDate!="" && toDate!="") { //PostgreSQL
	 * if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * 
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
	 * WhereCondition = WhereCondition + " and  date_trunc('day',CreationDate" +
	 * TimeConversionFromUTC + ")>=date_trunc('day',to_timestamp('" + fromDate +
	 * "','dd/mm/yyyy')) and  date_trunc('day',CreationDate" + TimeConversionFromUTC
	 * + ")<=date_trunc('day',to_timestamp('" + toDate + "','dd/mm/yyyy')) "; } else
	 * { WhereCondition = WhereCondition +
	 * " and  date_trunc('day',CreationDate)>=date_trunc('day',to_timestamp('" +
	 * fromDate +
	 * "','dd/mm/yyyy')) and  date_trunc('day',CreationDate)<=date_trunc('day',to_timestamp('"
	 * + toDate + "','dd/mm/yyyy')) "; } } else { //Oracle WhereCondition =
	 * WhereCondition + " and  trunc(CreationDate)>=trunc(to_date('" + fromDate +
	 * "','dd/mm/yyyy')) and  trunc(CreationDate)<=trunc(to_date('" + toDate +
	 * "','dd/mm/yyyy')) "; } } if(eventcode!=0 && eventcode!=-1) { WhereCondition =
	 * WhereCondition + " and EventCode ='" + eventcode + "' "; } if(errorid!=0 &&
	 * errorid!=-1) { WhereCondition = WhereCondition + " and ErrorId ='" + errorid
	 * + "' "; } if(equipmentid!=null && equipmentid!="" && equipmentid!="null") {
	 * WhereCondition = WhereCondition + " and EquipmentId ='" + equipmentid + "' ";
	 * } if(priority!=null && priority!="" && priority!="-1" && priority!="0") {
	 * WhereCondition = WhereCondition + " and Priority ='" + priority + "' "; }
	 * 
	 * 
	 * 
	 * try { //Oracle String tablecolumnnames
	 * ="transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , eventtimestamp , closedtimestamp,eventoccurrence ,eventstatus , activeflag , creationdate,createdby, lastupdateddate ,lastupdatedby , lasteventtimestamp"
	 * ; String Query = "Select " + tablecolumnnames +
	 * " from tEventDetail where ActiveFlag='1'  " + WhereCondition +
	 * " and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid + "' and Activeflag=1)) order by TransactionId desc";
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
	 * tablecolumnnames
	 * ="transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , (eventtimestamp"
	 * + TimeConversionFromUTC + ") as  eventtimestamp ,(closedtimestamp" +
	 * TimeConversionFromUTC +
	 * ") as  closedtimestamp,eventoccurrence ,eventstatus , activeflag ,(creationdate"
	 * + TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" +
	 * TimeConversionFromUTC +
	 * ") as lastupdateddate ,lastupdatedby ,(lastupdateddate" +
	 * TimeConversionFromUTC + ") as lasteventtimestamp"; }
	 * 
	 * Query = "Select " + tablecolumnnames +
	 * " from tEventDetail where ActiveFlag='1'  " + WhereCondition +
	 * " and SiteID in (Select SiteId from mSite where CustomerID in(Select CustomerID from mCustomer where CustomerId in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid +
	 * "' and Activeflag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid + "'))) order by TransactionId desc";
	 * 
	 * } List<EventDetail> EventDetailListObject =
	 * session.createSQLQuery(Query).list(); eventdetaillist =
	 * GetCastedEventDetailList(EventDetailListObject);
	 * 
	 * } catch(NoResultException ex) {
	 * 
	 * } catch(Exception ex) {
	 * 
	 * }
	 * 
	 * 
	 * return eventdetaillist;
	 * 
	 * 
	 * 
	 * 
	 * }
	 */

	@SuppressWarnings("unchecked")
	public List<EventDetail> listEventDetailsForDisplay_Old(int userid, String siteId, String fromDate, String toDate,
			int eventcode, int errorid, String equipmentid, String priority, int severity, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> eventdetaillist = null;
		String WhereCondition = "";
		String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";

		if (siteId != null && siteId != "" && siteId != "null") {
			WhereCondition = WhereCondition + " and SiteID ='" + siteId + "' ";
		}
		if (fromDate != null && toDate != null && fromDate != "" && toDate != "") {
			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {

				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					WhereCondition = WhereCondition + " and  date_trunc('day',CreationDate" + TimeConversionFromUTC
							+ ")>=date_trunc('day',to_timestamp('" + fromDate
							+ "','dd/mm/yyyy')) and  date_trunc('day',CreationDate" + TimeConversionFromUTC
							+ ")<=date_trunc('day',to_timestamp('" + toDate + "','dd/mm/yyyy')) ";
				} else {
					WhereCondition = WhereCondition
							+ " and  date_trunc('day',CreationDate)>=date_trunc('day',to_timestamp('" + fromDate
							+ "','dd/mm/yyyy')) and  date_trunc('day',CreationDate)<=date_trunc('day',to_timestamp('"
							+ toDate + "','dd/mm/yyyy')) ";
				}
			} else { // Oracle
				WhereCondition = WhereCondition + " and  trunc(CreationDate)>=trunc(to_date('" + fromDate
						+ "','dd/mm/yyyy')) and  trunc(CreationDate)<=trunc(to_date('" + toDate + "','dd/mm/yyyy')) ";
			}
		}
		if (eventcode != 0 && eventcode != -1) {
			WhereCondition = WhereCondition + " and EventCode ='" + eventcode + "' ";
		}
		if (errorid != 0 && errorid != -1) {
			WhereCondition = WhereCondition + " and ErrorId ='" + errorid + "' ";
		}
		if (equipmentid != null && equipmentid != "" && equipmentid != "null") {
			WhereCondition = WhereCondition + " and EquipmentId ='" + equipmentid + "' ";
		}
		if (priority != null && priority != "" && priority != "-1" && priority != "0") {
			WhereCondition = WhereCondition + " and Priority ='" + priority + "' ";
		}

		try {
			// Oracle
			String tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , eventtimestamp , closedtimestamp,eventoccurrence ,eventstatus , activeflag , creationdate,createdby, lastupdateddate ,lastupdatedby , lasteventtimestamp";
			String Query = "Select " + tablecolumnnames + " from tEventDetail where ActiveFlag='1'  " + WhereCondition
					+ " and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
					+ userid + "' and Activeflag=1)) order by TransactionId desc";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , (eventtimestamp"
							+ TimeConversionFromUTC + ") as  eventtimestamp ,(closedtimestamp" + TimeConversionFromUTC
							+ ") as  closedtimestamp,eventoccurrence ,eventstatus , activeflag ,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby ,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lasteventtimestamp";
				}

				Query = "Select " + tablecolumnnames + " from tEventDetail where ActiveFlag='1'  " + WhereCondition
						+ " and SiteID in (Select SiteId from mSiteMap where UserID='" + userid
						+ "' and ActiveFlag=1) order by TransactionId desc";

			}
			List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
			eventdetaillist = GetCastedEventDetailList(EventDetailListObject);

		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetaillist;

	}

	@SuppressWarnings("unchecked")
	public List<EventDetail> listCustomerEventDetailsForDisplay_Old(int customerid, String siteId, String fromDate,
			String toDate, int eventcode, int errorid, String equipmentid, String priority, int severity,
			String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> eventdetaillist = null;
		String WhereCondition = "";
		String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";

		if (siteId != null && siteId != "" && siteId != "null") {
			WhereCondition = WhereCondition + " and SiteID ='" + siteId + "' ";
		}
		if (fromDate != null && toDate != null && fromDate != "" && toDate != "") {
			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {

				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					WhereCondition = WhereCondition + " and  date_trunc('day',CreationDate" + TimeConversionFromUTC
							+ ")>=date_trunc('day',to_timestamp('" + fromDate
							+ "','dd/mm/yyyy')) and  date_trunc('day',CreationDate" + TimeConversionFromUTC
							+ ")<=date_trunc('day',to_timestamp('" + toDate + "','dd/mm/yyyy')) ";
				} else {
					WhereCondition = WhereCondition
							+ " and  date_trunc('day',CreationDate)>=date_trunc('day',to_timestamp('" + fromDate
							+ "','dd/mm/yyyy')) and  date_trunc('day',CreationDate)<=date_trunc('day',to_timestamp('"
							+ toDate + "','dd/mm/yyyy')) ";

				}
			} else { // Oracle
				WhereCondition = WhereCondition + " and  trunc(CreationDate)>=trunc(to_date('" + fromDate
						+ "','dd/mm/yyyy')) and  trunc(CreationDate)<=trunc(to_date('" + toDate + "','dd/mm/yyyy')) ";
			}
		}
		if (eventcode != 0 && eventcode != -1) {
			WhereCondition = WhereCondition + " and EventCode ='" + eventcode + "' ";
		}
		if (errorid != 0 && errorid != -1) {
			WhereCondition = WhereCondition + " and ErrorId ='" + errorid + "' ";
		}
		if (equipmentid != null && equipmentid != "" && equipmentid != "null") {
			WhereCondition = WhereCondition + " and EquipmentId ='" + equipmentid + "' ";
		}
		if (priority != null && priority != "" && priority != "-1" && priority != "0") {
			WhereCondition = WhereCondition + " and Priority ='" + priority + "' ";
		}

		try {
			// Oracle
			String tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , eventtimestamp , closedtimestamp,eventoccurrence ,eventstatus , activeflag , creationdate,createdby, lastupdateddate ,lastupdatedby , lasteventtimestamp";
			String Query = "Select " + tablecolumnnames + " from tEventDetail where ActiveFlag='1'  " + WhereCondition
					+ " and SiteID in (Select SiteId from mSite where CustomerID ='" + customerid
					+ "') order by TransactionId desc";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , (eventtimestamp"
							+ TimeConversionFromUTC + ") as  eventtimestamp ,(closedtimestamp" + TimeConversionFromUTC
							+ ") as  closedtimestamp,eventoccurrence ,eventstatus , activeflag ,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby ,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lasteventtimestamp";
				}
				Query = "Select " + tablecolumnnames + " from tEventDetail where ActiveFlag='1'  " + WhereCondition
						+ " and SiteID in (Select SiteId from mSite where CustomerID ='" + customerid
						+ "') order by TransactionId desc";

			}

			List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
			eventdetaillist = GetCastedEventDetailList(EventDetailListObject);

		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetaillist;

	}

	@Override
	public List<EventDetail> getEventDetailListByErrorId(int errorId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> eventdetaillist = null;

		try {
			// Oracle
			String tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , eventtimestamp , closedtimestamp,eventoccurrence ,eventstatus , activeflag , creationdate,createdby, lastupdateddate ,lastupdatedby , lasteventtimestamp";
			String Query = "Select " + tablecolumnnames + " from tEventDetail where ActiveFlag='1' and ErrorId='"
					+ errorId + "' order by TransactionId";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					tablecolumnnames = "transactionid,eventid, errorid,equipmentid ,siteid,severity ,priority ,remarks , (eventtimestamp"
							+ TimeConversionFromUTC + ") as  eventtimestamp ,(closedtimestamp" + TimeConversionFromUTC
							+ ") as  closedtimestamp,eventoccurrence ,eventstatus , activeflag ,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,lastupdatedby ,(lastupdateddate"
							+ TimeConversionFromUTC + ") as lasteventtimestamp";
				}

				Query = "Select " + tablecolumnnames + " from tEventDetail where ActiveFlag='1' and ErrorId='" + errorId
						+ "' order by TransactionId";

			}
			List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
			eventdetaillist = GetCastedEventDetailList(EventDetailListObject);

		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetaillist;

	}

// 03-02-19
	/*
	 * @SuppressWarnings("unchecked") public List<EventDetail>
	 * listEventDetailsForDisplay(int userId,String siteId,String fromDate,String
	 * toDate,int eventcode, int errorid, String equipmentid, String priority, int
	 * severity,String TimezoneOffset) {
	 * 
	 * 
	 * Session session = sessionFactory.getCurrentSession(); List<EventDetail>
	 * eventdetaillist = null; String WhereCondition = ""; String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * 
	 * 
	 * if(siteId!=null && siteId!="" && siteId!="null") { WhereCondition =
	 * WhereCondition + " and a.SiteID ='" + siteId + "' "; } if(fromDate!=null &&
	 * toDate!=null && fromDate!="" && toDate!="") { //PostgreSQL
	 * if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * 
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
	 * //WhereCondition = WhereCondition + " and  date_trunc('day',a.CreationDate" +
	 * TimeConversionFromUTC + ")>=date_trunc('day',to_timestamp('" + fromDate +
	 * "','dd/mm/yyyy')) and  date_trunc('day',a.CreationDate" +
	 * TimeConversionFromUTC + ")<=date_trunc('day',to_timestamp('" + toDate +
	 * "','dd/mm/yyyy')) "; WhereCondition = WhereCondition +
	 * " and  date_trunc('day',a.eventTimestamp" + TimeConversionFromUTC +
	 * ")>=date_trunc('day',to_timestamp('" + fromDate +
	 * "','dd/mm/yyyy')) and  date_trunc('day',a.eventTimestamp" +
	 * TimeConversionFromUTC + ")<=date_trunc('day',to_timestamp('" + toDate +
	 * "','dd/mm/yyyy')) "; } else { //WhereCondition = WhereCondition +
	 * " and  date_trunc('day',a.CreationDate)>=date_trunc('day',to_timestamp('" +
	 * fromDate +
	 * "','dd/mm/yyyy')) and  date_trunc('day',a.CreationDate)<=date_trunc('day',to_timestamp('"
	 * + toDate + "','dd/mm/yyyy')) "; WhereCondition = WhereCondition +
	 * " and  date_trunc('day',a.eventTimestamp)>=date_trunc('day',to_timestamp('" +
	 * fromDate +
	 * "','dd/mm/yyyy')) and  date_trunc('day',a.eventTimestamp)<=date_trunc('day',to_timestamp('"
	 * + toDate + "','dd/mm/yyyy')) "; } } else { //Oracle //WhereCondition =
	 * WhereCondition + " and  trunc(a.CreationDate)>=trunc(to_date('" + fromDate +
	 * "','dd/mm/yyyy')) and  trunc(a.CreationDate)<=trunc(to_date('" + toDate +
	 * "','dd/mm/yyyy')) "; WhereCondition = WhereCondition +
	 * " and  trunc(a.eventTimestamp)>=trunc(to_date('" + fromDate +
	 * "','dd/mm/yyyy')) and  trunc(a.eventTimestamp)<=trunc(to_date('" + toDate +
	 * "','dd/mm/yyyy')) "; } } if(eventcode!=0 && eventcode!=-1) { WhereCondition =
	 * WhereCondition + " and a.EventCode ='" + eventcode + "' "; } if(errorid!=0 &&
	 * errorid!=-1) { WhereCondition = WhereCondition + " and a.ErrorId ='" +
	 * errorid + "' "; } if(equipmentid!=null && equipmentid!="" &&
	 * equipmentid!="null") { WhereCondition = WhereCondition +
	 * " and a.EquipmentId ='" + equipmentid + "' "; } if(priority!=null &&
	 * priority!="" && priority!="-1" && priority!="0") { WhereCondition =
	 * WhereCondition + " and a.Priority ='" + priority + "' "; }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * try { //Oracle String Query =
	 * "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription, to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId  where a.ActiveFlag='1'  "
	 * + WhereCondition +
	 * "  and a.SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userId + "' and Activeflag=1)) order by a.eventTimestamp desc";
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * Query =
	 * "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' "
	 * + WhereCondition +
	 * "  and a.SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userId + "' and Activeflag=1)) order by a.eventTimestamp desc";
	 * 
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { Query =
	 * "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
	 * + TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" +
	 * TimeConversionFromUTC +
	 * ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
	 * + TimeConversionFromUTC +
	 * ") as  creationdate,a.createdby, (a.lastupdateddate" + TimeConversionFromUTC
	 * + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp" +
	 * TimeConversionFromUTC +
	 * ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
	 * + TimeConversionFromUTC +
	 * ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp" +
	 * TimeConversionFromUTC +
	 * ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
	 * + TimeConversionFromUTC +
	 * ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' "
	 * + WhereCondition +
	 * "  and a.SiteID in (Select SiteId from mSite where CustomerID in(Select CustomerID from mCustomer where CustomerId in (Select CustomerID from mCustomerMap where UserID='"
	 * + userId +
	 * "' and Activeflag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userId + "'))) order by a.eventTimestamp desc";
	 * 
	 * }
	 * 
	 * }
	 * 
	 * List<EventDetail> EventDetailListObject =
	 * session.createSQLQuery(Query).list(); eventdetaillist =
	 * GetCastedEventDetailJoinList(EventDetailListObject); }
	 * catch(NoResultException ex) {
	 * 
	 * } catch(Exception ex) {
	 * 
	 * }
	 * 
	 * 
	 * return eventdetaillist;
	 * 
	 * 
	 * 
	 * 
	 * 
	 * }
	 */

	@SuppressWarnings("unchecked")
	public List<EventDetail> listEventDetailsForDisplay(int userId, String siteId, String fromDate, String toDate,
			int eventcode, int errorid, String equipmentid, String priority, int severity, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> eventdetaillist = null;
		String WhereCondition = "";
		String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";

		if (siteId != null && siteId != "" && siteId != "null") {
			WhereCondition = WhereCondition + " and a.SiteID ='" + siteId + "' ";
		}
		if (fromDate != null && toDate != null && fromDate != "" && toDate != "") {
			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {

				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					// WhereCondition = WhereCondition + " and date_trunc('day',a.CreationDate" +
					// TimeConversionFromUTC + ")>=date_trunc('day',to_timestamp('" + fromDate +
					// "','dd/mm/yyyy')) and date_trunc('day',a.CreationDate" +
					// TimeConversionFromUTC + ")<=date_trunc('day',to_timestamp('" + toDate +
					// "','dd/mm/yyyy')) ";
					WhereCondition = WhereCondition + " and  date_trunc('day',a.eventTimestamp" + TimeConversionFromUTC
							+ ")>=date_trunc('day',to_timestamp('" + fromDate
							+ "','dd/mm/yyyy')) and  date_trunc('day',a.eventTimestamp" + TimeConversionFromUTC
							+ ")<=date_trunc('day',to_timestamp('" + toDate + "','dd/mm/yyyy')) ";
				} else {
					// WhereCondition = WhereCondition + " and
					// date_trunc('day',a.CreationDate)>=date_trunc('day',to_timestamp('" + fromDate
					// + "','dd/mm/yyyy')) and
					// date_trunc('day',a.CreationDate)<=date_trunc('day',to_timestamp('" + toDate +
					// "','dd/mm/yyyy')) ";
					WhereCondition = WhereCondition
							+ " and  date_trunc('day',a.eventTimestamp)>=date_trunc('day',to_timestamp('" + fromDate
							+ "','dd/mm/yyyy')) and  date_trunc('day',a.eventTimestamp)<=date_trunc('day',to_timestamp('"
							+ toDate + "','dd/mm/yyyy')) ";
				}
			} else { // Oracle
						// WhereCondition = WhereCondition + " and
						// trunc(a.CreationDate)>=trunc(to_date('" + fromDate + "','dd/mm/yyyy')) and
						// trunc(a.CreationDate)<=trunc(to_date('" + toDate + "','dd/mm/yyyy')) ";
				WhereCondition = WhereCondition + " and  trunc(a.eventTimestamp)>=trunc(to_date('" + fromDate
						+ "','dd/mm/yyyy')) and  trunc(a.eventTimestamp)<=trunc(to_date('" + toDate
						+ "','dd/mm/yyyy')) ";
			}
		}
		if (eventcode != 0 && eventcode != -1) {
			WhereCondition = WhereCondition + " and a.EventCode ='" + eventcode + "' ";
		}
		if (errorid != 0 && errorid != -1) {
			WhereCondition = WhereCondition + " and a.ErrorId ='" + errorid + "' ";
		}
		if (equipmentid != null && equipmentid != "" && equipmentid != "null") {
			WhereCondition = WhereCondition + " and a.EquipmentId ='" + equipmentid + "' ";
		}
		if (priority != null && priority != "" && priority != "-1" && priority != "0") {
			WhereCondition = WhereCondition + " and a.Priority ='" + priority + "' ";
		}

		try {
			// Oracle
			String Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription, to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId  where a.ActiveFlag='1'  "
					+ WhereCondition
					+ "  and a.SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
					+ userId + "' and Activeflag=1)) order by a.eventTimestamp desc";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' "
						+ WhereCondition + "  and a.SiteID in (Select SiteId from mSiteMap where UserID='" + userId
						+ "' and ActiveFlag=1) order by a.eventTimestamp desc";

				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
							+ TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" + TimeConversionFromUTC
							+ ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
							+ TimeConversionFromUTC + ") as  creationdate,a.createdby, (a.lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp"
							+ TimeConversionFromUTC
							+ ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' "
							+ WhereCondition + "  and a.SiteID in (Select SiteId from mSiteMap where UserID='" + userId
							+ "' and ActiveFlag=1) order by a.eventTimestamp desc";

				}

			}

			List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
			eventdetaillist = GetCastedEventDetailJoinList(EventDetailListObject);
		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetaillist;

	}

	@SuppressWarnings("unchecked")
	public List<EventDetail> listCustomerEventDetailsForDisplay(int customerid, String siteId, String fromDate,
			String toDate, int eventcode, int errorid, String equipmentid, String priority, int severity,
			String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> eventdetaillist = null;
		String WhereCondition = "";
		String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";

		if (siteId != null && siteId != "" && siteId != "null") {
			WhereCondition = WhereCondition + " and a.SiteID ='" + siteId + "' ";
		}
		if (fromDate != null && toDate != null && fromDate != "" && toDate != "") {
			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {

				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					// WhereCondition = WhereCondition + " and date_trunc('day',a.CreationDate" +
					// TimeConversionFromUTC + ")>=date_trunc('day',to_timestamp('" + fromDate +
					// "','dd/mm/yyyy')) and date_trunc('day',a.CreationDate" +
					// TimeConversionFromUTC + ")<=date_trunc('day',to_timestamp('" + toDate +
					// "','dd/mm/yyyy')) ";
					WhereCondition = WhereCondition + " and  date_trunc('day',a.eventTimestamp" + TimeConversionFromUTC
							+ ")>=date_trunc('day',to_timestamp('" + fromDate
							+ "','dd/mm/yyyy')) and  date_trunc('day',a.eventTimestamp" + TimeConversionFromUTC
							+ ")<=date_trunc('day',to_timestamp('" + toDate + "','dd/mm/yyyy')) ";
				} else {
					// WhereCondition = WhereCondition + " and
					// date_trunc('day',a.CreationDate)>=date_trunc('day',to_timestamp('" + fromDate
					// + "','dd/mm/yyyy')) and
					// date_trunc('day',a.CreationDate)<=date_trunc('day',to_timestamp('" + toDate +
					// "','dd/mm/yyyy')) ";
					WhereCondition = WhereCondition
							+ " and  date_trunc('day',a.eventTimestamp)>=date_trunc('day',to_timestamp('" + fromDate
							+ "','dd/mm/yyyy')) and  date_trunc('day',a.eventTimestamp)<=date_trunc('day',to_timestamp('"
							+ toDate + "','dd/mm/yyyy')) ";

				}
			} else { // Oracle
						// WhereCondition = WhereCondition + " and
						// trunc(a.CreationDate)>=trunc(to_date('" + fromDate + "','dd/mm/yyyy')) and
						// trunc(a.CreationDate)<=trunc(to_date('" + toDate + "','dd/mm/yyyy')) ";
				WhereCondition = WhereCondition + " and  trunc(a.eventTimestamp)>=trunc(to_date('" + fromDate
						+ "','dd/mm/yyyy')) and  trunc(a.eventTimestamp)<=trunc(to_date('" + toDate
						+ "','dd/mm/yyyy')) ";
			}
		}
		if (eventcode != 0 && eventcode != -1) {
			WhereCondition = WhereCondition + " and a.EventCode ='" + eventcode + "' ";
		}
		if (errorid != 0 && errorid != -1) {
			WhereCondition = WhereCondition + " and a.ErrorId ='" + errorid + "' ";
		}
		if (equipmentid != null && equipmentid != "" && equipmentid != "null") {
			WhereCondition = WhereCondition + " and a.EquipmentId ='" + equipmentid + "' ";
		}
		if (priority != null && priority != "" && priority != "-1" && priority != "0") {
			WhereCondition = WhereCondition + " and a.Priority ='" + priority + "' ";
		}

		try {
			// Oracle
			String Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription, to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1'  "
					+ WhereCondition + " and a.SiteID in (Select SiteId from mSite where CustomerID ='" + customerid
					+ "') order by a.TransactionId desc";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1'  "
						+ WhereCondition + " and a.SiteID in (Select SiteId from mSitemap where UserID ='" + customerid
						+ "') order by a.TransactionId desc";

				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
							+ TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" + TimeConversionFromUTC
							+ ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
							+ TimeConversionFromUTC + ") as  creationdate,a.createdby, (a.lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp"
							+ TimeConversionFromUTC
							+ ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1'  "
							+ WhereCondition + " and a.SiteID in (Select SiteId from mSitemap where UserID ='"
							+ customerid + "') order by a.TransactionId desc";

				}

			}

			List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
			eventdetaillist = GetCastedEventDetailJoinList(EventDetailListObject);
		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetaillist;

	}

	// 03-02-19
	/*
	 * public List<OpenStateTicketsCount> EventsCountByErrorMessage(int userid,
	 * String TimezoneOffset){
	 * 
	 * Session session = sessionFactory.getCurrentSession(); String Query="";
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
	 * // Query =
	 * "select tbl3.errormessage as errormessage,coalesce(errorid,0) as count,tbl3.color as color from(select errormessage,StatusSeq,color from (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#64b5b8' as color union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as color  union select 'Communication Loss' as errormessage,3 as StatusSeq,'#192438' as color union select 'SPD Failure' as errormessage,4 as StatusSeq,'#192438' as color) tbl1)tbl3 left outer join (select errorid,errormessage from(select coalesce (count(errorid),0) as errorid,coalesce(case when errormessage='Insulation Fault' then 'Insulation Fault' else case when errormessage='Hardware Failure' then 'Hardware Failure' else case when errormessage='Communication Loss' then 'Communication Loss' else case when errormessage='SPD Failure' then 'SPD Failure' end end end end ) as Errormessage from merrorcode where errorid in (select errorid from teventdetail where  eventTimestamp"
	 * + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC +
	 * " - interval '60 day') and  SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid
	 * +"' and ActiveFlag=1))) and errormessage in('Insulation Fault','Hardware Failure','Cmmunication Losss','SPD Failure')  group by errormessage) tbl )tbl4 on tbl3.errormessage=tbl4.errormessage  order by tbl3.StatusSeq"
	 * ; //
	 * Query="select tbl3.errormessage as errormessage,coalesce(transactionid,0) as count,tbl3.color as color from(select errormessage,StatusSeq,color from (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#ff3399' as color union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as color  union select 'Communication Loss' as errormessage,3 as StatusSeq,'#862d86' as color union select 'SPD Failure' as errormessage,4 as StatusSeq,'#660033' as color) tbl1)tbl3 left outer join (select transactionid,errormessage from(select coalesce (transactionid,0) as transactionid,coalesce(case when errormessage='Insulation Fault' then 'Insulation Fault' else case when errormessage='Hardware Failure' then 'Hardware Failure' else case when errormessage='Communication Loss' then 'Communication Loss' else case when errormessage='SPD Failure' then 'SPD Failure' end end end end ) as ErrorMessage from(select count(a.transactionid) as transactionid,coalesce(case when d.errormessage='Insulation Fault' then 'Insulation Fault' else case when d.errormessage='Hardware Failure' then 'Hardware Failure' else case when d.errormessage='Communication Loss' then 'Communication Loss' else case when d.errormessage='SPD Failure' then 'SPD Failure' end end end end )  as errormessage from teventdetail a left outer join mErrorCode d on a.ErrorId=d.ErrorId where d.errormessage in('Insulation Fault','Hardware Failure','Cmmunication Losss','SPD Failure')  and eventTimestamp"
	 * + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC +
	 * " - interval '60 day') and  SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid +
	 * "' and ActiveFlag=1)) group by d.errormessage)tbl2 group by transactionid,ErrorMessage)tbl4 group by transactionid,errormessage)tbl4 on tbl3.errormessage=tbl4.errormessage  order by tbl3.StatusSeq"
	 * ;
	 * Query="select errormessage as errormessage,coalesce(transactionid,0) as count from (select transactionid,errormessage from (select count (transactionid) as transactionid,errormessage as ErrorMessage from(select count(a.transactionid) as transactionid,d.errormessage as errormessage from teventdetail a left outer join mErrorCode d on a.ErrorId=d.ErrorId where eventTimestamp"
	 * + TimeConversionFromUTC + " >(now()" + TimeConversionFromUTC +
	 * "  - interval '60 day') and  SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * + userid +
	 * "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid +
	 * "')))  group by a.transactionid,a.eventoccurrence,errormessage order by eventoccurrence desc )tbl2 group by transactionid,ErrorMessage order by transactionid desc limit 5)tbl4 group by transactionid,errormessage order by transactionid desc limit 5)tbl4"
	 * ;
	 * 
	 * } else {
	 * 
	 * Query =
	 * "select tbl3.errormessage as errormessage,coalesce(errorid,0) as count,tbl3.color as color from(select errormessage,StatusSeq,color from (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#64b5b8' as color union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as color  union select 'Communication Loss' as errormessage,3 as StatusSeq,'#192438' as color union select 'SPD Failure' as errormessage,4 as StatusSeq,'#192438' as color) tbl1)tbl3 left outer join (select errorid,errormessage from(select coalesce (count(errorid),0) as errorid,coalesce(case when errormessage='Insulation Fault' then 'Insulation Fault' else case when errormessage='Hardware Failure' then 'Hardware Failure' else case when errormessage='Communication Loss' then 'Communication Loss' else case when errormessage='SPD Failure' then 'SPD Failure' end end end end ) as Errormessage from merrorcode where errorid in (select errorid from teventdetail where eventTimestamp>(now()- interval '60 day') and  SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid
	 * +"' and ActiveFlag=1))) and errormessage in('Insulation Fault','Hardware Failure','Cmmunication Losss','SPD Failure') group by errormessage) tbl )tbl4 on tbl3.errormessage=tbl4.errormessage  order by tbl3.StatusSeq"
	 * ;
	 * 
	 * 
	 * Query="select tbl3.errormessage as errormessage,coalesce(transactionid,0) as count,tbl3.color as color from(select errormessage,StatusSeq,color from (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#ff3399' as color union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as color  union select 'Communication Loss' as errormessage,3 as StatusSeq,'#862d86' as color union select 'SPD Failure' as errormessage,4 as StatusSeq,'#660033' as color) tbl1)tbl3 left outer join (select transactionid,errormessage from(select coalesce (transactionid,0) as transactionid,coalesce(case when errormessage='Insulation Fault' then 'Insulation Fault' else case when errormessage='Hardware Failure' then 'Hardware Failure' else case when errormessage='Communication Loss' then 'Communication Loss' else case when errormessage='SPD Failure' then 'SPD Failure' end end end end ) as ErrorMessage from(select count(a.transactionid) as transactionid,coalesce(case when d.errormessage='Insulation Fault' then 'Insulation Fault' else case when d.errormessage='Hardware Failure' then 'Hardware Failure' else case when d.errormessage='Communication Loss' then 'Communication Loss' else case when d.errormessage='SPD Failure' then 'SPD Failure' end end end end )  as errormessage from teventdetail a left outer join mErrorCode d on a.ErrorId=d.ErrorId where d.errormessage in('Insulation Fault','Hardware Failure','Cmmunication Losss','SPD Failure')  and eventTimestamp >(now()  - interval '60 day') and  SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid +
	 * "' and ActiveFlag=1)) group by d.errormessage)tbl2 group by transactionid,ErrorMessage)tbl4 group by transactionid,errormessage)tbl4 on tbl3.errormessage=tbl4.errormessage  order by tbl3.StatusSeq"
	 * ;
	 * 
	 * Query="select errormessage as errormessage,coalesce(transactionid,0) as count from (select transactionid,errormessage from (select count (transactionid) as transactionid,errormessage as ErrorMessage from(select count(a.transactionid) as transactionid,d.errormessage as errormessage from teventdetail a left outer join mErrorCode d on a.ErrorId=d.ErrorId where eventTimestamp >(now()  - interval '60 day') and  SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * + userid +
	 * "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid +
	 * "')))  group by a.transactionid,a.eventoccurrence,errormessage order by eventoccurrence desc )tbl2 group by transactionid,ErrorMessage order by transactionid desc limit 5)tbl4 group by transactionid,errormessage order by transactionid desc limit 5)tbl4"
	 * ; } }
	 * 
	 * List<OpenStateTicketsCount> TicketDetailList =
	 * session.createSQLQuery(Query).list(); return TicketDetailList;
	 * 
	 * }
	 */

	public List<OpenStateTicketsCount> EventsCountByErrorMessage(int userid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		String Query = "";
		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				// Query = "select tbl3.errormessage as errormessage,coalesce(errorid,0) as
				// count,tbl3.color as color from(select errormessage,StatusSeq,color from
				// (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#64b5b8' as color
				// union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as
				// color union select 'Communication Loss' as errormessage,3 as
				// StatusSeq,'#192438' as color union select 'SPD Failure' as errormessage,4 as
				// StatusSeq,'#192438' as color) tbl1)tbl3 left outer join (select
				// errorid,errormessage from(select coalesce (count(errorid),0) as
				// errorid,coalesce(case when errormessage='Insulation Fault' then 'Insulation
				// Fault' else case when errormessage='Hardware Failure' then 'Hardware Failure'
				// else case when errormessage='Communication Loss' then 'Communication Loss'
				// else case when errormessage='SPD Failure' then 'SPD Failure' end end end end
				// ) as Errormessage from merrorcode where errorid in (select errorid from
				// teventdetail where eventTimestamp" + TimeConversionFromUTC + ">(now()" +
				// TimeConversionFromUTC + " - interval '60 day') and SiteID in (Select SiteId
				// from mSite where CustomerID in (Select CustomerID from mCustomerMap where
				// UserID='"+ userid +"' and ActiveFlag=1))) and errormessage in('Insulation
				// Fault','Hardware Failure','Cmmunication Losss','SPD Failure') group by
				// errormessage) tbl )tbl4 on tbl3.errormessage=tbl4.errormessage order by
				// tbl3.StatusSeq";
				// Query="select tbl3.errormessage as errormessage,coalesce(transactionid,0) as
				// count,tbl3.color as color from(select errormessage,StatusSeq,color from
				// (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#ff3399' as color
				// union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as
				// color union select 'Communication Loss' as errormessage,3 as
				// StatusSeq,'#862d86' as color union select 'SPD Failure' as errormessage,4 as
				// StatusSeq,'#660033' as color) tbl1)tbl3 left outer join (select
				// transactionid,errormessage from(select coalesce (transactionid,0) as
				// transactionid,coalesce(case when errormessage='Insulation Fault' then
				// 'Insulation Fault' else case when errormessage='Hardware Failure' then
				// 'Hardware Failure' else case when errormessage='Communication Loss' then
				// 'Communication Loss' else case when errormessage='SPD Failure' then 'SPD
				// Failure' end end end end ) as ErrorMessage from(select count(a.transactionid)
				// as transactionid,coalesce(case when d.errormessage='Insulation Fault' then
				// 'Insulation Fault' else case when d.errormessage='Hardware Failure' then
				// 'Hardware Failure' else case when d.errormessage='Communication Loss' then
				// 'Communication Loss' else case when d.errormessage='SPD Failure' then 'SPD
				// Failure' end end end end ) as errormessage from teventdetail a left outer
				// join mErrorCode d on a.ErrorId=d.ErrorId where d.errormessage in('Insulation
				// Fault','Hardware Failure','Cmmunication Losss','SPD Failure') and
				// eventTimestamp" + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC +
				// " - interval '60 day') and SiteID in (Select SiteId from mSite where
				// CustomerID in (Select CustomerID from mCustomerMap where UserID='"+ userid +
				// "' and ActiveFlag=1)) group by d.errormessage)tbl2 group by
				// transactionid,ErrorMessage)tbl4 group by transactionid,errormessage)tbl4 on
				// tbl3.errormessage=tbl4.errormessage order by tbl3.StatusSeq";
				Query = "select errormessage as errormessage,coalesce(transactionid,0) as count from (select transactionid,errormessage from (select count (transactionid) as transactionid,errormessage as ErrorMessage from(select count(a.transactionid) as transactionid,d.errormessage as errormessage from teventdetail a left outer join mErrorCode d on a.ErrorId=d.ErrorId where eventTimestamp"
						+ TimeConversionFromUTC + " >(now()" + TimeConversionFromUTC
						+ "  - interval '60 day') and  SiteID in (Select SiteId from mSiteMap where UserID='" + userid
						+ "' and ActiveFlag=1)  group by a.transactionid,a.eventoccurrence,errormessage order by eventoccurrence desc )tbl2 group by transactionid,ErrorMessage order by transactionid desc limit 5)tbl4 group by transactionid,errormessage order by transactionid desc limit 5)tbl4";

			} else {

				/*
				 * Query =
				 * "select tbl3.errormessage as errormessage,coalesce(errorid,0) as count,tbl3.color as color from(select errormessage,StatusSeq,color from (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#64b5b8' as color union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as color  union select 'Communication Loss' as errormessage,3 as StatusSeq,'#192438' as color union select 'SPD Failure' as errormessage,4 as StatusSeq,'#192438' as color) tbl1)tbl3 left outer join (select errorid,errormessage from(select coalesce (count(errorid),0) as errorid,coalesce(case when errormessage='Insulation Fault' then 'Insulation Fault' else case when errormessage='Hardware Failure' then 'Hardware Failure' else case when errormessage='Communication Loss' then 'Communication Loss' else case when errormessage='SPD Failure' then 'SPD Failure' end end end end ) as Errormessage from merrorcode where errorid in (select errorid from teventdetail where eventTimestamp>(now()- interval '60 day') and  SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				 * + userid
				 * +"' and ActiveFlag=1))) and errormessage in('Insulation Fault','Hardware Failure','Cmmunication Losss','SPD Failure') group by errormessage) tbl )tbl4 on tbl3.errormessage=tbl4.errormessage  order by tbl3.StatusSeq"
				 * ;
				 */
				/*
				 * Query="select tbl3.errormessage as errormessage,coalesce(transactionid,0) as count,tbl3.color as color from(select errormessage,StatusSeq,color from (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#ff3399' as color union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as color  union select 'Communication Loss' as errormessage,3 as StatusSeq,'#862d86' as color union select 'SPD Failure' as errormessage,4 as StatusSeq,'#660033' as color) tbl1)tbl3 left outer join (select transactionid,errormessage from(select coalesce (transactionid,0) as transactionid,coalesce(case when errormessage='Insulation Fault' then 'Insulation Fault' else case when errormessage='Hardware Failure' then 'Hardware Failure' else case when errormessage='Communication Loss' then 'Communication Loss' else case when errormessage='SPD Failure' then 'SPD Failure' end end end end ) as ErrorMessage from(select count(a.transactionid) as transactionid,coalesce(case when d.errormessage='Insulation Fault' then 'Insulation Fault' else case when d.errormessage='Hardware Failure' then 'Hardware Failure' else case when d.errormessage='Communication Loss' then 'Communication Loss' else case when d.errormessage='SPD Failure' then 'SPD Failure' end end end end )  as errormessage from teventdetail a left outer join mErrorCode d on a.ErrorId=d.ErrorId where d.errormessage in('Insulation Fault','Hardware Failure','Cmmunication Losss','SPD Failure')  and eventTimestamp >(now()  - interval '60 day') and  SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				 * + userid +
				 * "' and ActiveFlag=1)) group by d.errormessage)tbl2 group by transactionid,ErrorMessage)tbl4 group by transactionid,errormessage)tbl4 on tbl3.errormessage=tbl4.errormessage  order by tbl3.StatusSeq"
				 * ;
				 */
				Query = "select errormessage as errormessage,coalesce(transactionid,0) as count from (select transactionid,errormessage from (select count (transactionid) as transactionid,errormessage as ErrorMessage from(select count(a.transactionid) as transactionid,d.errormessage as errormessage from teventdetail a left outer join mErrorCode d on a.ErrorId=d.ErrorId where eventTimestamp >(now()  - interval '60 day') and  SiteID in (Select SiteId from mSiteMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1)  group by a.transactionid,a.eventoccurrence,errormessage order by eventoccurrence desc )tbl2 group by transactionid,ErrorMessage order by transactionid desc limit 5)tbl4 group by transactionid,errormessage order by transactionid desc limit 5)tbl4";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

// 03-02-19	
	/*
	 * public List<OpenStateTicketsCount> getEventDetailBasedOnDays(int userid,
	 * String TimezoneOffset){
	 * 
	 * Session session = sessionFactory.getCurrentSession(); String Query="";
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
	 * Query =
	 * "select count(transactionid) as ticketcount,b.sitename as sitename,b.customerreference as siteref from teventdetail a left outer join msite b on a.siteid=b.siteid  where a.siteid in(Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID="
	 * + userid +
	 * " and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid + "'))) and a.eventTimestamp " + TimeConversionFromUTC +
	 * ">=date_trunc('day',now()" + TimeConversionFromUTC +
	 * " - interval '60 day') group by a.siteid,b.sitename,b.customerreference order by ticketcount desc limit 5"
	 * ;
	 * 
	 * } else { Query =
	 * "select count(transactionid) as ticketcount,b.sitename as sitename,b.customerreference as siteref from teventdetail a left outer join msite b on a.siteid=b.siteid  where a.siteid in(Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID="
	 * + userid +
	 * " and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid +
	 * "'))) and a.eventTimestamp >= date_trunc('day', now() - (60 * interval '1 day')) group by a.siteid,b.sitename,b.customerreference order by ticketcount desc limit 5"
	 * ;
	 * 
	 * } }
	 * 
	 * List<OpenStateTicketsCount> TicketDetailList =
	 * session.createSQLQuery(Query).list(); return TicketDetailList;
	 * 
	 * }
	 */

	public List<OpenStateTicketsCount> getEventDetailBasedOnDays(int userid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		String Query = "";
		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select count(transactionid) as ticketcount,b.sitename as sitename,b.customerreference as siteref from teventdetail a left outer join msite b on a.siteid=b.siteid  where a.siteid in (Select SiteId from mSiteMap where UserID='"
						+ userid + "' and ActiveFlag=1) and a.eventTimestamp " + TimeConversionFromUTC
						+ ">=date_trunc('day',now()" + TimeConversionFromUTC
						+ " - interval '60 day') group by a.siteid,b.sitename,b.customerreference order by ticketcount desc limit 5";

			} else {
				Query = "select count(transactionid) as ticketcount,b.sitename as sitename,b.customerreference as siteref from teventdetail a left outer join msite b on a.siteid=b.siteid  where a.siteid in (Select SiteId from mSiteMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1) and a.eventTimestamp >= date_trunc('day', now() - (60 * interval '1 day')) group by a.siteid,b.sitename,b.customerreference order by ticketcount desc limit 5";

			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<EventDetail> getEventDetailBasedOnSite(String siteId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> EventDetailList = new ArrayList<EventDetail>();
		String Query = "";
		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
						+ TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
						+ TimeConversionFromUTC + ") as  creationdate,a.createdby, (a.lastupdateddate"
						+ TimeConversionFromUTC + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp"
						+ TimeConversionFromUTC
						+ ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day',now()"
						+ TimeConversionFromUTC
						+ " - (60 * interval '1 day')) and a.SiteID in (Select SiteId from mSite where SiteName='"
						+ siteId + "') order by a.eventTimestamp desc";

			} else {
				Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day', now() - (60 * interval '1 day')) and a.SiteID in (Select SiteId from mSite where SiteName='"
						+ siteId + "') order by a.eventTimestamp desc";

			}
		}

		List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
		EventDetailList = GetCastedEventDetailJoinList(EventDetailListObject);
		return EventDetailList;

	}

// 03-02-19
	/*
	 * public List<EventDetail> getEventDetailListByUserIdLimit(int userId, String
	 * TimezoneOffset) { Session session = sessionFactory.getCurrentSession();
	 * List<EventDetail> EventDetailList = new ArrayList<EventDetail>();
	 * 
	 * String Query = "";
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * Query =
	 * "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day', now() - (60 * interval '1 day')) and a.SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * + userId +
	 * "' and Activeflag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userId + "'))) order by a.eventtimestamp desc";
	 * 
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * Query =
	 * "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
	 * + TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" +
	 * TimeConversionFromUTC +
	 * ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
	 * + TimeConversionFromUTC +
	 * ") as  creationdate,a.createdby, (a.lastupdateddate" + TimeConversionFromUTC
	 * + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp" +
	 * TimeConversionFromUTC +
	 * ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
	 * + TimeConversionFromUTC +
	 * ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp" +
	 * TimeConversionFromUTC +
	 * ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
	 * + TimeConversionFromUTC +
	 * ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day',now()"
	 * + TimeConversionFromUTC +
	 * " - (60 * interval '1 day')) and a.SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * + userId +
	 * "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userId + "'))) order by a.eventtimestamp desc";
	 * 
	 * }
	 * 
	 * 
	 * }
	 * 
	 * List<EventDetail> EventDetailListObject =
	 * session.createSQLQuery(Query).list(); EventDetailList =
	 * GetCastedEventDetailJoinList(EventDetailListObject); return EventDetailList;
	 * }
	 */

	public List<EventDetail> getEventDetailListByUserIdLimit(int userId, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> EventDetailList = new ArrayList<EventDetail>();

		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day', now() - (60 * interval '1 day')) and a.SiteID in  (Select SiteId from mSiteMap where UserID='"
					+ userId + "' and ActiveFlag=1) order by a.eventtimestamp desc";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
						+ TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
						+ TimeConversionFromUTC + ") as  creationdate,a.createdby, (a.lastupdateddate"
						+ TimeConversionFromUTC + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp"
						+ TimeConversionFromUTC
						+ ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day',now()"
						+ TimeConversionFromUTC
						+ " - (60 * interval '1 day')) and a.SiteID in (Select SiteId from mSiteMap where UserID='"
						+ userId + "' and ActiveFlag=1) order by a.eventtimestamp desc";

			}

		}

		List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
		EventDetailList = GetCastedEventDetailJoinList(EventDetailListObject);
		return EventDetailList;
	}

	public List<OpenStateTicketsCount> getEventDetailBasedOnDaysBySite(int siteId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		String Query = "";
		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select count(transactionid) as ticketcount,b.sitename as sitename,b.customerreference as siteref from teventdetail a left outer join msite b on a.siteid=b.siteid where a.siteid in('"
						+ siteId + "') and a.eventTimestamp " + TimeConversionFromUTC + ">=date_trunc('day',now()"
						+ TimeConversionFromUTC
						+ " -interval '60 day') group by a.siteid,b.sitename,b.customerreference";

			} else {
				Query = "select count(transactionid) as ticketcount,b.sitename as sitename,b.customerreference as siteref from teventdetail a left outer join msite b on a.siteid=b.siteid where a.siteid in('"
						+ siteId
						+ "') and a.eventTimestamp >= date_trunc('day', now() - (60 * interval '1 day')) group by a.siteid,b.sitename,b.customerreference";

			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<EventDetail> getEventDetailListBySiteIdLimit(int siteId, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> EventDetailList = new ArrayList<EventDetail>();

		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day', now() - (60 * interval '1 day')) and a.SiteID in (Select SiteId from mSite where SiteID in ('"
					+ siteId + "'))) order by a.eventtimestamp desc";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
						+ TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
						+ TimeConversionFromUTC + ") as  creationdate,a.createdby, (a.lastupdateddate"
						+ TimeConversionFromUTC + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp"
						+ TimeConversionFromUTC
						+ ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day',now()"
						+ TimeConversionFromUTC
						+ " - (60 * interval '1 day')) and a.SiteID in (Select SiteId from mSite where SiteID in ('"
						+ siteId + "')) order by a.eventtimestamp desc";

			}

		}

		List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
		EventDetailList = GetCastedEventDetailJoinList(EventDetailListObject);
		return EventDetailList;
	}

	public List<OpenStateTicketsCount> EventsCountByErrorMessageBySite(int siteId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		String Query = "";
		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			/*
			 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
			 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
			 * Query =
			 * "select tbl3.errormessage as errormessage,coalesce(errorid,0) as count,tbl3.color as color from(select errormessage,StatusSeq,color from (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#64b5b8' as color union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as color  union select 'Communication Loss' as errormessage,3 as StatusSeq,'#192438' as color union select 'SPD Failure' as errormessage,4 as StatusSeq,'#192438' as color) tbl1)tbl3 left outer join (select errorid,errormessage from(select coalesce (count(errorid),0) as errorid,coalesce(case when errormessage='Insulation Fault' then 'Insulation Fault' else case when errormessage='Hardware Failure' then 'Hardware Failure' else case when errormessage='Communication Loss' then 'Communication Loss' else case when errormessage='SPD Failure' then 'SPD Failure' end end end end ) as Errormessage from merrorcode where errorid in (select errorid from teventdetail where  creationdate"
			 * + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC +
			 * " -interval '60 day') and  SiteID in (Select SiteId from mSite where SiteID in ('"
			 * + siteId
			 * +"'))) and errormessage in('Insulation Fault','Hardware Failure','Cmmunication Losss','SPD Failure')  group by errormessage) tbl )tbl4 on tbl3.errormessage=tbl4.errormessage  order by tbl3.StatusSeq"
			 * ;
			 * 
			 * } else {
			 * 
			 * Query =
			 * "select tbl3.errormessage as errormessage,coalesce(errorid,0) as count,tbl3.color as color from(select errormessage,StatusSeq,color from (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#64b5b8' as color union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as color  union select 'Communication Loss' as errormessage,3 as StatusSeq,'#192438' as color union select 'SPD Failure' as errormessage,4 as StatusSeq,'#192438' as color) tbl1)tbl3 left outer join (select errorid,errormessage from(select coalesce (count(errorid),0) as errorid,coalesce(case when errormessage='Insulation Fault' then 'Insulation Fault' else case when errormessage='Hardware Failure' then 'Hardware Failure' else case when errormessage='Communication Loss' then 'Communication Loss' else case when errormessage='SPD Failure' then 'SPD Failure' end end end end ) as Errormessage from merrorcode where errorid in (select errorid from teventdetail where creationdate>(now()-interval '60 day') and  SiteID in (Select SiteId from mSite where SiteID in ('"
			 * + siteId
			 * +"'))) and errormessage in('Insulation Fault','Hardware Failure','Cmmunication Losss','SPD Failure') group by errormessage) tbl )tbl4 on tbl3.errormessage=tbl4.errormessage  order by tbl3.StatusSeq"
			 * ;
			 * 
			 * }
			 * 
			 */

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				// Query = "select tbl3.errormessage as errormessage,coalesce(errorid,0) as
				// count,tbl3.color as color from(select errormessage,StatusSeq,color from
				// (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#64b5b8' as color
				// union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as
				// color union select 'Communication Loss' as errormessage,3 as
				// StatusSeq,'#192438' as color union select 'SPD Failure' as errormessage,4 as
				// StatusSeq,'#192438' as color) tbl1)tbl3 left outer join (select
				// errorid,errormessage from(select coalesce (count(errorid),0) as
				// errorid,coalesce(case when errormessage='Insulation Fault' then 'Insulation
				// Fault' else case when errormessage='Hardware Failure' then 'Hardware Failure'
				// else case when errormessage='Communication Loss' then 'Communication Loss'
				// else case when errormessage='SPD Failure' then 'SPD Failure' end end end end
				// ) as Errormessage from merrorcode where errorid in (select errorid from
				// teventdetail where eventTimestamp" + TimeConversionFromUTC + ">(now()" +
				// TimeConversionFromUTC + " - interval '60 day') and SiteID in (Select SiteId
				// from mSite where CustomerID in (Select CustomerID from mCustomerMap where
				// UserID='"+ userid +"' and ActiveFlag=1))) and errormessage in('Insulation
				// Fault','Hardware Failure','Cmmunication Losss','SPD Failure') group by
				// errormessage) tbl )tbl4 on tbl3.errormessage=tbl4.errormessage order by
				// tbl3.StatusSeq";
				// Query="select tbl3.errormessage as errormessage,coalesce(transactionid,0) as
				// count,tbl3.color as color from(select errormessage,StatusSeq,color from
				// (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#ff3399' as color
				// union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as
				// color union select 'Communication Loss' as errormessage,3 as
				// StatusSeq,'#862d86' as color union select 'SPD Failure' as errormessage,4 as
				// StatusSeq,'#660033' as color) tbl1)tbl3 left outer join (select
				// transactionid,errormessage from(select coalesce (transactionid,0) as
				// transactionid,coalesce(case when errormessage='Insulation Fault' then
				// 'Insulation Fault' else case when errormessage='Hardware Failure' then
				// 'Hardware Failure' else case when errormessage='Communication Loss' then
				// 'Communication Loss' else case when errormessage='SPD Failure' then 'SPD
				// Failure' end end end end ) as ErrorMessage from(select count(a.transactionid)
				// as transactionid,coalesce(case when d.errormessage='Insulation Fault' then
				// 'Insulation Fault' else case when d.errormessage='Hardware Failure' then
				// 'Hardware Failure' else case when d.errormessage='Communication Loss' then
				// 'Communication Loss' else case when d.errormessage='SPD Failure' then 'SPD
				// Failure' end end end end ) as errormessage from teventdetail a left outer
				// join mErrorCode d on a.ErrorId=d.ErrorId where d.errormessage in('Insulation
				// Fault','Hardware Failure','Cmmunication Losss','SPD Failure') and
				// eventTimestamp" + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC +
				// " - interval '60 day') and SiteID ='"+ siteId +"' group by
				// d.errormessage)tbl2 group by transactionid,ErrorMessage)tbl4 group by
				// transactionid,errormessage)tbl4 on tbl3.errormessage=tbl4.errormessage order
				// by tbl3.StatusSeq";
				Query = "select errormessage as errormessage,coalesce(transactionid,0) as count from (select transactionid,errormessage from (select count (transactionid) as transactionid,errormessage as ErrorMessage from(select count(a.transactionid) as transactionid,d.errormessage as errormessage from teventdetail a left outer join mErrorCode d on a.ErrorId=d.ErrorId where eventTimestamp"
						+ TimeConversionFromUTC + " >(now()" + TimeConversionFromUTC
						+ "  - interval '60 day') and  SiteID ='" + siteId
						+ "'  group by a.transactionid,a.eventoccurrence,errormessage order by eventoccurrence desc)tbl2 group by transactionid,ErrorMessage order by transactionid desc limit 5)tbl4 group by transactionid,errormessage order by transactionid desc limit 5)tbl4";

			} else {

				// Query = "select tbl3.errormessage as errormessage,coalesce(errorid,0) as
				// count,tbl3.color as color from(select errormessage,StatusSeq,color from
				// (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#64b5b8' as color
				// union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as
				// color union select 'Communication Loss' as errormessage,3 as
				// StatusSeq,'#192438' as color union select 'SPD Failure' as errormessage,4 as
				// StatusSeq,'#192438' as color) tbl1)tbl3 left outer join (select
				// errorid,errormessage from(select coalesce (count(errorid),0) as
				// errorid,coalesce(case when errormessage='Insulation Fault' then 'Insulation
				// Fault' else case when errormessage='Hardware Failure' then 'Hardware Failure'
				// else case when errormessage='Communication Loss' then 'Communication Loss'
				// else case when errormessage='SPD Failure' then 'SPD Failure' end end end end
				// ) as Errormessage from merrorcode where errorid in (select errorid from
				// teventdetail where eventTimestamp>(now()- interval '60 day') and SiteID in
				// (Select SiteId from mSite where CustomerID in (Select CustomerID from
				// mCustomerMap where UserID='"+ userid +"' and ActiveFlag=1))) and errormessage
				// in('Insulation Fault','Hardware Failure','Cmmunication Losss','SPD Failure')
				// group by errormessage) tbl )tbl4 on tbl3.errormessage=tbl4.errormessage order
				// by tbl3.StatusSeq";
				// Query="select tbl3.errormessage as errormessage,coalesce(transactionid,0) as
				// count,tbl3.color as color from(select errormessage,StatusSeq,color from
				// (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#ff3399' as color
				// union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as
				// color union select 'Communication Loss' as errormessage,3 as
				// StatusSeq,'#862d86' as color union select 'SPD Failure' as errormessage,4 as
				// StatusSeq,'#660033' as color) tbl1)tbl3 left outer join (select
				// transactionid,errormessage from(select coalesce (transactionid,0) as
				// transactionid,coalesce(case when errormessage='Insulation Fault' then
				// 'Insulation Fault' else case when errormessage='Hardware Failure' then
				// 'Hardware Failure' else case when errormessage='Communication Loss' then
				// 'Communication Loss' else case when errormessage='SPD Failure' then 'SPD
				// Failure' end end end end ) as ErrorMessage from(select count(a.transactionid)
				// as transactionid,coalesce(case when d.errormessage='Insulation Fault' then
				// 'Insulation Fault' else case when d.errormessage='Hardware Failure' then
				// 'Hardware Failure' else case when d.errormessage='Communication Loss' then
				// 'Communication Loss' else case when d.errormessage='SPD Failure' then 'SPD
				// Failure' end end end end ) as errormessage from teventdetail a left outer
				// join mErrorCode d on a.ErrorId=d.ErrorId where d.errormessage in('Insulation
				// Fault','Hardware Failure','Cmmunication Losss','SPD Failure') and
				// eventTimestamp >(now() - interval '60 day') and SiteID ='"+ siteId + " group
				// by d.errormessage)tbl2 group by transactionid,ErrorMessage)tbl4 group by
				// transactionid,errormessage)tbl4 on tbl3.errormessage=tbl4.errormessage order
				// by tbl3.StatusSeq";
				Query = "select errormessage as errormessage,coalesce(transactionid,0) as count from (select transactionid,errormessage from (select count (transactionid) as transactionid,errormessage as ErrorMessage from(select count(a.transactionid) as transactionid,d.errormessage as errormessage from teventdetail a left outer join mErrorCode d on a.ErrorId=d.ErrorId where eventTimestamp >(now()  - interval '60 day') and  SiteID ='"
						+ siteId
						+ "'  group by a.transactionid,a.eventoccurrence,errormessage order by eventoccurrence desc)tbl2 group by transactionid,ErrorMessage order by transactionid desc limit 5)tbl4 group by transactionid,errormessage order by transactionid desc limit 5)tbl4";

			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> getEventDetailBasedOnDaysByCustomerId(int customerId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		String Query = "";
		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select count(transactionid) as ticketcount,b.sitename as sitename,b.customerreference as siteref from teventdetail a left outer join msite b on a.siteid=b.siteid where a.siteid in (Select SiteId from msite where CustomerId in ('"
						+ customerId + "')) and a.eventTimestamp " + TimeConversionFromUTC + ">=date_trunc('day',now()"
						+ TimeConversionFromUTC
						+ " -interval '60 day') group by a.siteid,b.sitename,b.customerreference order by ticketcount desc limit 5";

			} else {
				Query = "select count(transactionid) as ticketcount,b.sitename as sitename,b.customerreference as siteref from teventdetail a left outer join msite b on a.siteid=b.siteid where a.siteid in (Select SiteId from msite where CustomerId in ('"
						+ customerId
						+ "')) and a.eventTimestamp >= date_trunc('day', now() - (60 * interval '1 day')) group by a.siteid,b.sitename,b.customerreference order by ticketcount desc limit 5";

			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<EventDetail> getEventDetailListByCustomerIdLimit(int customerId, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> EventDetailList = new ArrayList<EventDetail>();

		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day', now() - (60 * interval '1 day')) and a.SiteID in (Select SiteId from msite where CustomerId in ('"
					+ customerId + "'))) order by a.eventtimestamp desc";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
						+ TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
						+ TimeConversionFromUTC + ") as  creationdate,a.createdby, (a.lastupdateddate"
						+ TimeConversionFromUTC + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp"
						+ TimeConversionFromUTC
						+ ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day',now()"
						+ TimeConversionFromUTC
						+ " - (60 * interval '1 day')) and a.SiteID in (Select SiteId from msite where CustomerId in ("
						+ customerId + ")) order by a.eventtimestamp desc";

			}

		}

		List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
		EventDetailList = GetCastedEventDetailJoinList(EventDetailListObject);
		return EventDetailList;
	}

	public List<OpenStateTicketsCount> EventsCountByErrorMessageByCustomerId(int customerId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		String Query = "";
		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			/*
			 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
			 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
			 * Query =
			 * "select tbl3.errormessage as errormessage,coalesce(errorid,0) as count,tbl3.color as color from(select errormessage,StatusSeq,color from (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#64b5b8' as color union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as color  union select 'Communication Loss' as errormessage,3 as StatusSeq,'#192438' as color union select 'SPD Failure' as errormessage,4 as StatusSeq,'#192438' as color) tbl1)tbl3 left outer join (select errorid,errormessage from(select coalesce (count(errorid),0) as errorid,coalesce(case when errormessage='Insulation Fault' then 'Insulation Fault' else case when errormessage='Hardware Failure' then 'Hardware Failure' else case when errormessage='Communication Loss' then 'Communication Loss' else case when errormessage='SPD Failure' then 'SPD Failure' end end end end ) as Errormessage from merrorcode where errorid in (select errorid from teventdetail where  creationdate"
			 * + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC +
			 * " -interval '60 day') and  SiteID in (Select SiteId from mSite where  CustomerID in ("
			 * + customerId +
			 * "))) and errormessage in('Insulation Fault','Hardware Failure','Cmmunication Losss','SPD Failure')  group by errormessage) tbl )tbl4 on tbl3.errormessage=tbl4.errormessage  order by tbl3.StatusSeq"
			 * ;
			 * 
			 * } else {
			 * 
			 * Query =
			 * "select tbl3.errormessage as errormessage,coalesce(errorid,0) as count,tbl3.color as color from(select errormessage,StatusSeq,color from (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#64b5b8' as color union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as color  union select 'Communication Loss' as errormessage,3 as StatusSeq,'#192438' as color union select 'SPD Failure' as errormessage,4 as StatusSeq,'#192438' as color) tbl1)tbl3 left outer join (select errorid,errormessage from(select coalesce (count(errorid),0) as errorid,coalesce(case when errormessage='Insulation Fault' then 'Insulation Fault' else case when errormessage='Hardware Failure' then 'Hardware Failure' else case when errormessage='Communication Loss' then 'Communication Loss' else case when errormessage='SPD Failure' then 'SPD Failure' end end end end ) as Errormessage from merrorcode where errorid in (select errorid from teventdetail where creationdate>(now()-interval '60 day') and  SiteID in (Select SiteId from mSite where CustomerID in ("
			 * + customerId +
			 * "))) and errormessage in('Insulation Fault','Hardware Failure','Cmmunication Losss','SPD Failure') group by errormessage) tbl )tbl4 on tbl3.errormessage=tbl4.errormessage  order by tbl3.StatusSeq"
			 * ;
			 * 
			 * }
			 */

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				// Query = "select tbl3.errormessage as errormessage,coalesce(errorid,0) as
				// count,tbl3.color as color from(select errormessage,StatusSeq,color from
				// (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#64b5b8' as color
				// union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as
				// color union select 'Communication Loss' as errormessage,3 as
				// StatusSeq,'#192438' as color union select 'SPD Failure' as errormessage,4 as
				// StatusSeq,'#192438' as color) tbl1)tbl3 left outer join (select
				// errorid,errormessage from(select coalesce (count(errorid),0) as
				// errorid,coalesce(case when errormessage='Insulation Fault' then 'Insulation
				// Fault' else case when errormessage='Hardware Failure' then 'Hardware Failure'
				// else case when errormessage='Communication Loss' then 'Communication Loss'
				// else case when errormessage='SPD Failure' then 'SPD Failure' end end end end
				// ) as Errormessage from merrorcode where errorid in (select errorid from
				// teventdetail where eventTimestamp" + TimeConversionFromUTC + ">(now()" +
				// TimeConversionFromUTC + " - interval '60 day') and SiteID in (Select SiteId
				// from mSite where CustomerID in (Select CustomerID from mCustomerMap where
				// UserID='"+ userid +"' and ActiveFlag=1))) and errormessage in('Insulation
				// Fault','Hardware Failure','Cmmunication Losss','SPD Failure') group by
				// errormessage) tbl )tbl4 on tbl3.errormessage=tbl4.errormessage order by
				// tbl3.StatusSeq";
				// Query="select tbl3.errormessage as errormessage,coalesce(transactionid,0) as
				// count,tbl3.color as color from(select 'Insulation Fault' as errormessage, 1
				// as StatusSeq,'#ff3399' as color union select 'Hardware Failure' as
				// errormessage,2 as StatusSeq,'#64b5b8' as color union select 'Communication
				// Loss' as errormessage,3 as StatusSeq,'#862d86' as color union select 'SPD
				// Failure' as errormessage,4 as StatusSeq,'#660033' as color) tbl1)tbl3 left
				// outer join (select transactionid,errormessage from(select coalesce
				// (transactionid,0) as transactionid,coalesce(case when
				// errormessage='Insulation Fault' then 'Insulation Fault' else case when
				// errormessage='Hardware Failure' then 'Hardware Failure' else case when
				// errormessage='Communication Loss' then 'Communication Loss' else case when
				// errormessage='SPD Failure' then 'SPD Failure' end end end end ) as
				// ErrorMessage from(select count(a.transactionid) as
				// transactionid,coalesce(case when d.errormessage='Insulation Fault' then
				// 'Insulation Fault' else case when d.errormessage='Hardware Failure' then
				// 'Hardware Failure' else case when d.errormessage='Communication Loss' then
				// 'Communication Loss' else case when d.errormessage='SPD Failure' then 'SPD
				// Failure' end end end end ) as errormessage from teventdetail a left outer
				// join mErrorCode d on a.ErrorId=d.ErrorId where d.errormessage in('Insulation
				// Fault','Hardware Failure','Cmmunication Losss','SPD Failure') and
				// eventTimestamp" + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC +
				// " - interval '60 day') and SiteID in (Select SiteId from mSite where
				// CustomerID in (" + customerId + ")) group by d.errormessage)tbl2 group by
				// transactionid,ErrorMessage)tbl4 group by transactionid,errormessage)tbl4 on
				// tbl3.errormessage=tbl4.errormessage order by tbl3.StatusSeq";
				Query = "select errormessage as errormessage,coalesce(transactionid,0) as count from (select transactionid,errormessage from (select count (transactionid) as transactionid,errormessage as ErrorMessage from(select count(a.transactionid) as transactionid,d.errormessage as errormessage from teventdetail a left outer join mErrorCode d on a.ErrorId=d.ErrorId where eventTimestamp"
						+ TimeConversionFromUTC + " >(now()" + TimeConversionFromUTC
						+ "  - interval '60 day') and  SiteID in (Select SiteId from msite where  CustomerId in ("
						+ customerId
						+ "))  group by a.transactionid,a.eventoccurrence,errormessage order by eventoccurrence desc)tbl2 group by transactionid,ErrorMessage order by transactionid desc limit 5)tbl4 group by transactionid,errormessage order by transactionid desc limit 5)tbl4";

			} else {

				// Query = "select tbl3.errormessage as errormessage,coalesce(errorid,0) as
				// count,tbl3.color as color from(select errormessage,StatusSeq,color from
				// (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#64b5b8' as color
				// union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as
				// color union select 'Communication Loss' as errormessage,3 as
				// StatusSeq,'#192438' as color union select 'SPD Failure' as errormessage,4 as
				// StatusSeq,'#192438' as color) tbl1)tbl3 left outer join (select
				// errorid,errormessage from(select coalesce (count(errorid),0) as
				// errorid,coalesce(case when errormessage='Insulation Fault' then 'Insulation
				// Fault' else case when errormessage='Hardware Failure' then 'Hardware Failure'
				// else case when errormessage='Communication Loss' then 'Communication Loss'
				// else case when errormessage='SPD Failure' then 'SPD Failure' end end end end
				// ) as Errormessage from merrorcode where errorid in (select errorid from
				// teventdetail where eventTimestamp>(now()- interval '60 day') and SiteID in
				// (Select SiteId from mSite where CustomerID in (Select CustomerID from
				// mCustomerMap where UserID='"+ userid +"' and ActiveFlag=1))) and errormessage
				// in('Insulation Fault','Hardware Failure','Cmmunication Losss','SPD Failure')
				// group by errormessage) tbl )tbl4 on tbl3.errormessage=tbl4.errormessage order
				// by tbl3.StatusSeq";
				// Query="select tbl3.errormessage as errormessage,coalesce(transactionid,0) as
				// count,tbl3.color as color from(select errormessage,StatusSeq,color from
				// (select 'Insulation Fault' as errormessage, 1 as StatusSeq,'#ff3399' as color
				// union select 'Hardware Failure' as errormessage,2 as StatusSeq,'#64b5b8' as
				// color union select 'Communication Loss' as errormessage,3 as
				// StatusSeq,'#862d86' as color union select 'SPD Failure' as errormessage,4 as
				// StatusSeq,'#660033' as color) tbl1)tbl3 left outer join (select
				// transactionid,errormessage from(select coalesce (transactionid,0) as
				// transactionid,coalesce(case when errormessage='Insulation Fault' then
				// 'Insulation Fault' else case when errormessage='Hardware Failure' then
				// 'Hardware Failure' else case when errormessage='Communication Loss' then
				// 'Communication Loss' else case when errormessage='SPD Failure' then 'SPD
				// Failure' end end end end ) as ErrorMessage from(select count(a.transactionid)
				// as transactionid,coalesce(case when d.errormessage='Insulation Fault' then
				// 'Insulation Fault' else case when d.errormessage='Hardware Failure' then
				// 'Hardware Failure' else case when d.errormessage='Communication Loss' then
				// 'Communication Loss' else case when d.errormessage='SPD Failure' then 'SPD
				// Failure' end end end end ) as errormessage from teventdetail a left outer
				// join mErrorCode d on a.ErrorId=d.ErrorId where d.errormessage in('Insulation
				// Fault','Hardware Failure','Cmmunication Losss','SPD Failure') and
				// eventTimestamp >(now() - interval '60 day') and SiteID in (Select SiteId from
				// mSite where CustomerID in (" + customerId + ")) group by d.errormessage)tbl2
				// group by transactionid,ErrorMessage)tbl4 group by
				// transactionid,errormessage)tbl4 on tbl3.errormessage=tbl4.errormessage order
				// by tbl3.StatusSeq";
				Query = "select errormessage as errormessage,coalesce(transactionid,0) as count from (select transactionid,errormessage from (select count (transactionid) as transactionid,errormessage as ErrorMessage from(select count(a.transactionid) as transactionid,d.errormessage as errormessage from teventdetail a left outer join mErrorCode d on a.ErrorId=d.ErrorId where eventTimestamp >(now()  - interval '60 day') and  SiteID in (Select SiteId from mSite where CustomerID in (Select SiteId from msite where  CustomerId in ("
						+ customerId
						+ "))  group by a.transactionid,a.eventoccurrence,errormessage order by eventoccurrence desc)tbl2 group by transactionid,ErrorMessage order by transactionid desc limit 5)tbl4 group by transactionid,errormessage order by transactionid desc limit 5)tbl4";

			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

// 03-02-19
	/*
	 * public List<EventDetail> ClickByErrorMessage(String ErrorMessage,int userid,
	 * String TimezoneOffset){
	 * 
	 * Session session = sessionFactory.getCurrentSession(); List<EventDetail>
	 * EventDetailList = new ArrayList<EventDetail>();
	 * 
	 * String Query = "";
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * Query =
	 * "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and d.errormessage in('"
	 * + ErrorMessage +
	 * "') and a.eventTimestamp >= date_trunc('day', now() - (60 * interval '1 day')) and a.SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid +
	 * "' and Activeflag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid + "')) order by a.transactionid desc";
	 * 
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * Query =
	 * "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
	 * + TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" +
	 * TimeConversionFromUTC +
	 * ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
	 * + TimeConversionFromUTC +
	 * ") as  creationdate,a.createdby, (a.lastupdateddate" + TimeConversionFromUTC
	 * + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp" +
	 * TimeConversionFromUTC +
	 * ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
	 * + TimeConversionFromUTC +
	 * ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp" +
	 * TimeConversionFromUTC +
	 * ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
	 * + TimeConversionFromUTC +
	 * ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and d.errormessage in('"
	 * + ErrorMessage +"') and a.eventTimestamp >= date_trunc('day',now()" +
	 * TimeConversionFromUTC +
	 * " - (60 * interval '1 day')) and a.SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid +
	 * "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid + "')) order by a.transactionid desc";
	 * 
	 * }
	 * 
	 * 
	 * }
	 * 
	 * List<EventDetail> EventDetailListObject =
	 * session.createSQLQuery(Query).list(); EventDetailList =
	 * GetCastedEventDetailJoinList(EventDetailListObject); return EventDetailList;
	 * 
	 * }
	 */

	public List<EventDetail> ClickByErrorMessage(String ErrorMessage, int userid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> EventDetailList = new ArrayList<EventDetail>();

		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and d.errormessage in('"
					+ ErrorMessage
					+ "') and a.eventTimestamp >= date_trunc('day', now() - (60 * interval '1 day')) and a.SiteID in  (Select SiteId from mSiteMap where UserID='"
					+ userid + "' and ActiveFlag=1) order by a.transactionid desc";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
						+ TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
						+ TimeConversionFromUTC + ") as  creationdate,a.createdby, (a.lastupdateddate"
						+ TimeConversionFromUTC + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp"
						+ TimeConversionFromUTC
						+ ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and d.errormessage in('"
						+ ErrorMessage + "') and a.eventTimestamp >= date_trunc('day',now()" + TimeConversionFromUTC
						+ " - (60 * interval '1 day')) and a.SiteID in  (Select SiteId from mSiteMap where UserID='"
						+ userid + "' and ActiveFlag=1) order by a.transactionid desc";

			}

		}

		List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
		EventDetailList = GetCastedEventDetailJoinList(EventDetailListObject);
		return EventDetailList;

	}

	public List<EventDetail> ClickByErrorMessageBySite(String ErrorMessage, int siteId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> EventDetailList = new ArrayList<EventDetail>();

		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and d.errormessage in('"
					+ ErrorMessage
					+ "') and a.eventTimestamp >= date_trunc('day', now() - (60 * interval '1 day')) and a.SiteID='"
					+ siteId + "' order by a.transactionid desc";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
						+ TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
						+ TimeConversionFromUTC + ") as  creationdate,a.createdby, (a.lastupdateddate"
						+ TimeConversionFromUTC + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp"
						+ TimeConversionFromUTC
						+ ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and d.errormessage in('"
						+ ErrorMessage + "') and a.eventTimestamp >= date_trunc('day',now()" + TimeConversionFromUTC
						+ " - (60 * interval '1 day')) and a.SiteID='" + siteId + "' order by a.transactionid desc";

			}

		}

		List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
		EventDetailList = GetCastedEventDetailJoinList(EventDetailListObject);
		return EventDetailList;

	}

	public List<EventDetail> ClickByErrorMessageByCustomer(String ErrorMessage, int customerId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> EventDetailList = new ArrayList<EventDetail>();

		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and d.errormessage in('"
					+ ErrorMessage
					+ "') and a.eventTimestamp >= date_trunc('day', now() - (60 * interval '1 day')) and a.SiteID in (Select SiteId from mSitemap where UserID='"
					+ customerId + "') order by a.transactionid desc";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
						+ TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
						+ TimeConversionFromUTC + ") as  creationdate,a.createdby, (a.lastupdateddate"
						+ TimeConversionFromUTC + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp"
						+ TimeConversionFromUTC
						+ ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
						+ TimeConversionFromUTC
						+ ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and d.errormessage in('"
						+ ErrorMessage + "') and a.eventTimestamp >= date_trunc('day',now()" + TimeConversionFromUTC
						+ " - (60 * interval '1 day')) and a.SiteID in (Select SiteId from mSitemap where UserID='"
						+ customerId + "') order by a.transactionid desc";

			}

		}

		List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
		EventDetailList = GetCastedEventDetailJoinList(EventDetailListObject);
		return EventDetailList;

	}

//@Override
	public List<EventDetail> findByEquipmentId(int equipmentId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<EventDetail> eventdetaillist = null;

		try {
			// Oracle
			String Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription, to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= trunc(sysdate-36) and a.EquipmentID ='"
					+ equipmentId + "' order by a.eventTimestamp desc";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  a.eventtimestamp ,  a.closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,a.creationdate,a.createdby,a.lastupdateddate ,a.lastupdatedby , a.lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp,'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp,'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp,'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day', now() - (60 * interval '1 day')) and a.EquipmentID ='"
						+ equipmentId + "' order by a.eventTimestamp desc";

				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					Query = "Select a.transactionid,a.eventid, a.errorid as errorid,a.equipmentid ,a.siteid,a.severity ,a.priority ,a.remarks ,  (a.eventtimestamp"
							+ TimeConversionFromUTC + ") as eventtimestamp,  (a.closedtimestamp" + TimeConversionFromUTC
							+ ") as closedtimestamp,a.eventoccurrence ,a.eventstatus , a.activeflag ,(a.creationdate"
							+ TimeConversionFromUTC + ") as  creationdate,a.createdby, (a.lastupdateddate"
							+ TimeConversionFromUTC + ") as lastupdateddate ,a.lastupdatedby , (a.lasteventtimestamp"
							+ TimeConversionFromUTC
							+ ") as  lasteventtimestamp , SiteName, PrimarySerialNumber as EquipmentName,ErrorCode, ErrorMessage, EquipmentType, ErrorDescription,to_char(a.EventTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as EventTimestampText,to_char(a.ClosedTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as ClosedTimestampText,to_char(a.LastEventTimestamp"
							+ TimeConversionFromUTC
							+ ",'DD-MM-YYYY HH24:MI') as LastEventTimestampText,c.CustomerNaming,c.Capacity from tEventDetail a left outer join mSite b on a.SiteId=b.SiteId left outer join mEquipment c on a.EquipmentId=c.EquipmentId left outer join mErrorCode d on a.ErrorId=d.ErrorId where a.ActiveFlag='1' and a.eventTimestamp >= date_trunc('day',now()"
							+ TimeConversionFromUTC + " - (60 * interval '1 day')) and a.EquipmentID ='" + equipmentId
							+ "' order by a.eventTimestamp desc";

				}

			}

			List<EventDetail> EventDetailListObject = session.createSQLQuery(Query).list();
			eventdetaillist = GetCastedEventDetailJoinList(EventDetailListObject);
		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return eventdetaillist;
		/*
		 * Date todate = new Date(); Calendar calendar = Calendar.getInstance();
		 * calendar.add(Calendar.MONTH, -2); Date fromDate = calendar.getTime(); Session
		 * session = sessionFactory.getCurrentSession(); String Query =
		 * "FROM EventDetail WHERE EquipmentId=:EquipmentId and EventTimestamp BETWEEN :fromDate AND :todate"
		 * ; Query query2 = session.createQuery(Query);
		 * query2.setParameter("EquipmentId", equipmentId);
		 * query2.setParameter("fromDate", fromDate); query2.setParameter("todate",
		 * todate); List<EventDetail> eventDetail = query2.list(); return eventDetail;
		 */
	}

}
