
/******************************************************
 * 
 *    	Filename	: ParameterListDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Parameter list related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.dao.ParameterListDAO;
import com.mestech.eampm.model.ParameterList;

@Repository
public class ParameterListDAOImpl implements ParameterListDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addParameterList(ParameterList parameterlist) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(parameterlist);

	}

	// @Override
	public void updateParameterList(ParameterList parameterlist) {
		Session session = sessionFactory.getCurrentSession();
		session.update(parameterlist);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<ParameterList> listParameterLists() {
		Session session = sessionFactory.getCurrentSession();
		List<ParameterList> ParameterListsList = session.createQuery("from ParameterList").list();

		return ParameterListsList;
	}

	// @Override
	public ParameterList getParameterListById(int id) {
		Session session = sessionFactory.getCurrentSession();
		ParameterList parameterlist = (ParameterList) session.get(ParameterList.class, new Integer(id));
		return parameterlist;
	}

	// @Override
	public void removeParameterList(int id) {
		Session session = sessionFactory.getCurrentSession();
		ParameterList parameterlist = (ParameterList) session.get(ParameterList.class, new Integer(id));
		if (null != parameterlist) {
			session.delete(parameterlist);
		}
	}
}
