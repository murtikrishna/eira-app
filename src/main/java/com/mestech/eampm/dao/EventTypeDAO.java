
/******************************************************
 * 
 *    	Filename	: EventTypeDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Event Type related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.EventType;

public interface EventTypeDAO {

	public void addEventType(EventType eventtype);

	public void updateEventType(EventType eventtype);

	public EventType getEventTypeById(int id);

	public void removeEventType(int id);

	public List<EventType> listEventTypes();
}
