
/******************************************************
 * 
 *    	Filename	: TicketTransactionDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for ticket transaction operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.TicketTransaction;

public interface TicketTransactionDAO {

	public void addTicketTransaction(TicketTransaction ticketdetail);

	public void updateTicketTransaction(TicketTransaction ticketdetail);

	public void removeTicketTransaction(int id);

	public TicketTransaction getTicketTransactionById(int id, String TimezoneOffset);

	public List<TicketTransaction> getTicketTransactionListByUserId(int userId, String TimezoneOffset);

	public List<TicketTransaction> getTicketTransactionListBySiteId(int siteId, String TimezoneOffset);

	public List<TicketTransaction> getTicketTransactionListByTicketId(int ticketId, String TimezoneOffset);

	public TicketTransaction getTicketTransactionByMax(String MaxColumnName, String TimezoneOffset);

	public List<TicketTransaction> listTicketTransactions(String TimezoneOffset);
}
