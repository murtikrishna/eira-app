
/******************************************************
 * 
 *    	Filename	: EventDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for event related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.Event;

@Repository
public class EventDAOImpl implements EventDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addEvent(Event event) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(event);

		System.out.println("DAO Impl Completed");
	}

	// @Override
	public void updateEvent(Event event) {
		Session session = sessionFactory.getCurrentSession();
		session.update(event);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Event> listEvents() {
		Session session = sessionFactory.getCurrentSession();
		List<Event> EventsList = session.createQuery("from Event").list();

		return EventsList;
	}

	// @Override
	public Event getEventById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Event event = (Event) session.get(Event.class, new Integer(id));
		return event;
	}

	// @Override
	public void removeEvent(int id) {
		Session session = sessionFactory.getCurrentSession();
		Event event = (Event) session.get(Event.class, new Integer(id));
		if (null != event) {
			session.delete(event);
		}
	}
}
