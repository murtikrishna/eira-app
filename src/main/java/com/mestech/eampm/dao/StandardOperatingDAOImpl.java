
/******************************************************
 * 
 *    	Filename	: StandardOperatingDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for sop operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.StandardOperatingProcedure;
import com.mestech.eampm.utility.SOPRequest;
import com.mestech.eampm.utility.SOPSub;

@Repository
public class StandardOperatingDAOImpl implements StandardOperatingDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void saveOrUpdate(SOPRequest sopRequest) {
		Map<Integer, StandardOperatingProcedure> map = new HashMap<>();

		Session session = sessionFactory.getCurrentSession();
		if (sopRequest.getCategory() != null && sopRequest.getType() != null) {
			List<StandardOperatingProcedure> operatingProcedures = getSop(sopRequest.getCategory(),
					sopRequest.getType());
			if (operatingProcedures != null && operatingProcedures.size() > 0) {
				for (StandardOperatingProcedure procedure : operatingProcedures) {
					map.put(procedure.getSteps(), procedure);

				}
				List<SOPSub> sopSubs = sopRequest.getSopSubs();
				if (sopSubs != null && !sopSubs.isEmpty()) {
					for (SOPSub sopSub : sopSubs) {
						if (map.get(sopSub.getSteps()) == null) {
							StandardOperatingProcedure operatingProcedure = new StandardOperatingProcedure(
									sopSub.getSteps(), sopRequest.getType(), sopRequest.getCategory(),
									sopSub.getOption1(), sopSub.getOption2(), sopSub.getOption3(), sopSub.getOption4(),
									sopSub.getWorkInstruction(), sopSub.getWorkInstructionInfo(), sopSub.getRemarks());
							session.save(operatingProcedure);
						} else if (map.get(sopSub.getSteps()) != null) {
							StandardOperatingProcedure procedure = map.get(sopSub.getSteps());
							procedure.setSteps(sopSub.getSteps());
							procedure.setWorkType(sopRequest.getType());
							procedure.setCategory(sopRequest.getCategory());
							procedure.setOption1(sopSub.getOption1());
							procedure.setOption2(sopSub.getOption2());
							procedure.setOption3(sopSub.getOption3());
							procedure.setOption4(sopSub.getOption4());
							procedure.setWorkInstruction(sopSub.getWorkInstruction());
							procedure.setWorkInstructionInfo(sopSub.getWorkInstructionInfo());
							procedure.setRemarks(sopSub.getRemarks());
							session.update(procedure);
							map.remove(sopSub.getSteps());
						}

					}
				}
				if (map != null && map.size() > 0) {
					for (Map.Entry<Integer, StandardOperatingProcedure> entry : map.entrySet()) {
						delete(entry.getValue());
					}
				}
			} else {
				List<SOPSub> sopSubs = sopRequest.getSopSubs();
				if (sopSubs != null && !sopSubs.isEmpty()) {
					for (SOPSub sopSub : sopSubs) {
						StandardOperatingProcedure operatingProcedure = new StandardOperatingProcedure(
								sopSub.getSteps(), sopRequest.getType(), sopRequest.getCategory(), sopSub.getOption1(),
								sopSub.getOption2(), sopSub.getOption3(), sopSub.getOption4(),
								sopSub.getWorkInstruction(), sopSub.getWorkInstructionInfo(), sopSub.getRemarks());
						session.save(operatingProcedure);
					}
				}
			}
		}
	}

	@Override
	public List<StandardOperatingProcedure> getSop(String category, String type) {
		Session session = sessionFactory.getCurrentSession();

		Query query = session.createQuery(
				"FROM StandardOperatingProcedure WHERE category=:category AND workType=:workType ORDER BY steps ASC");
		query.setParameter("workType", type);
		query.setParameter("category", category);
		return query.list();
	}

	@Override
	public void delete(StandardOperatingProcedure operatingProcedure) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.delete(operatingProcedure);
	}

}
