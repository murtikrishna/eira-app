
/******************************************************
 * 
 *    	Filename	: MDataloggerDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for datalogger related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.math.BigInteger;
import java.util.List;

import com.mestech.eampm.model.MDatalogger;

public interface MDataloggerDAO {

	public MDatalogger save(MDatalogger datalogger);

	public MDatalogger update(MDatalogger datalogger);

	public void delete(BigInteger id);

	public MDatalogger edit(BigInteger id);

	public List<MDatalogger> listAll();

}
