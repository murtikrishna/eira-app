
/******************************************************
 * 
 *    	Filename	: EquipmentCategoryDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Equipment category related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentCategory;
import com.mestech.eampm.model.SiteMap;

public interface EquipmentCategoryDAO {

	public void addEquipmentCategory(EquipmentCategory equipmentcategory);

	public void updateEquipmentCategory(EquipmentCategory equipmentcategory);

	public EquipmentCategory getEquipmentCategoryById(int id);

	public void removeEquipmentCategory(int id);

	public List<EquipmentCategory> listEquipmentCategories();

	public EquipmentCategory getEquipmentCategoryByMax(String MaxColumnName);

	public List<EquipmentCategory> listEquipmentCategorys(int categoryid);
}
