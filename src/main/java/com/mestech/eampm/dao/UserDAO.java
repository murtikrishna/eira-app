
/******************************************************
 * 
 *    	Filename	: UserDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for User related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.User;

public interface UserDAO {

	public void addUser(User user);

	public void updateUser(User user);

	public User getUserById(int id);

	public User getUserByName(String userName);

	public void removeUser(int id);

	public User getUserByMax(String MaxColumnName);

	public List<User> listUsers();

	public List<User> listFieldUsers();

	public List<User> listAppUsers();

	public List<User> listUsers(int userid);

}
