

/******************************************************
 * 
 *    	Filename	: CustomerDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Customer related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.bean.CustomerListBean;
import com.mestech.eampm.bean.DashboardSiteListBean;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.CustomerMap;
import com.mestech.eampm.model.EquipmentType;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;
import com.mestech.eampm.utility.UtilityCommonDAO;

@Repository
public class CustomerDAOImpl implements CustomerDAO {

	@Autowired
    private SessionFactory sessionFactory;
   

	
	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();
	
	
   //@Override
   public void addCustomer(Customer customer) {
	   
	   
	   Session session = sessionFactory.openSession(); //create the session object
       session.beginTransaction();//create the transaction from the session object
       
       
       String CustomerCode = customer.getCustomerCode();
       
       CustomerMap customerMap = new CustomerMap();
       customerMap.setUserID(1);
       customerMap.setActiveFlag(customer.getActiveFlag());
       customerMap.setCreatedBy(customer.getCreatedBy());
       customerMap.setCreationDate(customer.getCreationDate());
       
       
       
       
       
       session.save(customer); 
       
       Customer customer1 = (Customer) session.createQuery("from Customer where CustomerCode='" + CustomerCode + "'").uniqueResult();
       customerMap.setCustomerID(customer1.getCustomerId());
       
       session.save(customerMap);
       
       session.getTransaction().commit(); //close the transaction
       session.close(); //close the session
       
         
       
   }

   //@Override
   public void updateCustomer(Customer customer) {
       Session session = sessionFactory.getCurrentSession();
       session.update(customer);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<Customer> listCustomers() {
       Session session = sessionFactory.getCurrentSession();
       //Common for Oracle & PostgreSQL
       List<Customer> CustomersList = session.createQuery("from Customer where ActiveFlag=1").list();
      
       return CustomersList;
   }
   
   
   @SuppressWarnings("unchecked")
   //@Override
   public List<Customer> listConfigCustomers() {
       Session session = sessionFactory.getCurrentSession();
       //Common for Oracle & PostgreSQL
       List<Customer> CustomersList = session.createQuery("from Customer").list();
      
       return CustomersList;
   }
   
   public List<Customer> listprimarycustomers()
   {
	   Session session = sessionFactory.getCurrentSession();
       //Common for Oracle & PostgreSQL
       List<Customer> CustomersList = session.createQuery("from Customer where CustomerGroup='Primary' ").list();
      
       return CustomersList;
   }

   // 03-02-19
  /* public List<Customer> getCustomerListByUserId(int userId)
	{
	   Session session = sessionFactory.getCurrentSession();
	   //Common for Oracle & PostgreSQL
	   String Query = "from Customer where  ActiveFlag='1' and CustomerID in (Select CustomerID from CustomerMap where UserID='" + userId + "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from CustomerMap where ActiveFlag=1 and UserID='"+ userId +"')";
	   List<Customer> CustomerList = session.createQuery(Query).list();

	   return CustomerList;
	}*/
   
   
   
   public List<Customer> getCustomerListByUserId(int userId)
	{
	   Session session = sessionFactory.getCurrentSession();
	   //Common for Oracle & PostgreSQL
	   String Query = "from Customer where  ActiveFlag='1' and CustomerID in (select CustomerID from Site Where SiteId in (Select SiteId from SiteMap where UserID='" + userId + "' and ActiveFlag=1) and ActiveFlag=1) ";
	  List<Customer> CustomerList = session.createQuery(Query).list();

	   return CustomerList;
	}
	
   
   //@Override
   public Customer getCustomerById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       Customer customer = (Customer) session.get(Customer.class, new Integer(id));
       return customer;
   }
   
   public Customer getCustomerByMax(String MaxColumnName) {
       Session session = sessionFactory.getCurrentSession();  
       Customer customer = null;
       //Common for Oracle & PostgreSQL
       try
       {
    	   customer = (Customer) session.createQuery("from Customer Order By " + MaxColumnName +  " desc")
   		    .setMaxResults(1).getSingleResult();
       }
       catch(NoResultException ex)
       {
    	   
       }
       catch(Exception ex)
       {
    	   
       }
       
       return customer;
   }
   
   
   public Customer getPrimaryCustomerByMax(String MaxColumnName) {
       Session session = sessionFactory.getCurrentSession();  
       Customer customer = null;
       //Common for Oracle & PostgreSQL
       try
       {
    	   customer = (Customer) session.createQuery("from Customer where PrimaryCustomer is not null and primarycustomercode is not null order by " + MaxColumnName +  " desc")
   		    .setMaxResults(1).getSingleResult();
       }
       catch(NoResultException ex)
       {
    	   
       }
       catch(Exception ex)
       {
    	   
       }
       
       return customer;
   }
   
   

   //@Override
   public void removeCustomer(int id) {
       Session session = sessionFactory.getCurrentSession();
       Customer customer = (Customer) session.get(Customer.class, new Integer(id));
       
       //De-activate the flag
       customer.setActiveFlag(0);
       
       if(null != customer){
           //session.delete(customer);
           session.update(customer);
       }
   }
   
   
   // 03-02-19
 /*  public List<CustomerListBean> getAjaxCustomerListByUser(String UserID) {
		Session session = sessionFactory.getCurrentSession();
		
		if(UserID == null)
		{ UserID = "0";}
		else if(UserID.equals(""))
		{ UserID = "0";}
		
		 
		//Oracle
		String Query = "Select nvl(a.CustomerId,0) as CustomerId,nvl(a.CustomerCode,' ') as CustomerCode,nvl(a.CustomerReference,' ') as CustomerReference,nvl(a.CustomerName,' ') as CustomerName,nvl(a.Address,' ') as Address,nvl(a.ContactPerson,' ') as ContactPerson,nvl(a.Mobile,' ') as Mobile, nvl(Count(b.SiteId),0) as TotalSites from mCustomer a left outer join mSite b on a.CustomerId=b.CustomerID where  a.ActiveFlag='1' and b.ActiveFlag='1' and a.CustomerID in (Select CustomerID from mCustomerMap where UserID='" + UserID + "' and ActiveFlag=1) Group by a.CustomerId,a.CustomerCode,a.CustomerReference,a.CustomerName,a.Address,a.ContactPerson,a.Mobile Order by CustomerCode";

		
		//PostgreSQL
		if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB))
		{
			Query = "Select coalesce(a.CustomerId,0) as CustomerId,coalesce(a.CustomerCode,' ') as CustomerCode,coalesce(a.CustomerReference,' ') as CustomerReference,coalesce(a.CustomerName,' ') as CustomerName,coalesce(a.Address,' ') as Address,coalesce(a.ContactPerson,' ') as ContactPerson,coalesce(a.Mobile,' ') as Mobile, coalesce(Count(b.SiteId),0) as TotalSites,coalesce(sum(b.Installationcapacity),0) as Totalcapacity,coalesce(a.CustomerGroup,' ') as CustomerGroup,coalesce(a.Primarycustomer,0) as Primarycustomer,coalesce(a.PrimaryCustomerCode,' ') as PrimaryCustomerCode from mCustomer a left outer join mSite b on a.CustomerId=b.CustomerID where  a.ActiveFlag='1' and b.ActiveFlag='1' and a.CustomerID in (Select CustomerID from mCustomerMap where UserID='" + UserID + "' and ActiveFlag=1) or a.PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"+ UserID +"') Group by a.CustomerId,a.CustomerCode,a.CustomerReference,a.CustomerName,a.Address,a.ContactPerson,a.Mobile,a.CustomerGroup,a.PrimaryCustomer,a.PrimaryCustomerCode Order by CustomerID";
		
		}

		System.out.println(Query);		
		List<CustomerListBean> CustomerList = session.createSQLQuery(Query).list();
		
	    return CustomerList;
 }*/
   
   
   
   
   
   public List<CustomerListBean> getAjaxCustomerListByUser(String UserID) {
		Session session = sessionFactory.getCurrentSession();
		
		if(UserID == null)
		{ UserID = "0";}
		else if(UserID.equals(""))
		{ UserID = "0";}
		
		 
		//Oracle
		String Query = "Select nvl(a.CustomerId,0) as CustomerId,nvl(a.CustomerCode,' ') as CustomerCode,nvl(a.CustomerReference,' ') as CustomerReference,nvl(a.CustomerName,' ') as CustomerName,nvl(a.Address,' ') as Address,nvl(a.ContactPerson,' ') as ContactPerson,nvl(a.Mobile,' ') as Mobile, nvl(Count(b.SiteId),0) as TotalSites from mCustomer a left outer join mSite b on a.CustomerId=b.CustomerID where  a.ActiveFlag='1' and b.ActiveFlag='1' and a.CustomerID in (Select CustomerID from mCustomerMap where UserID='" + UserID + "' and ActiveFlag=1) Group by a.CustomerId,a.CustomerCode,a.CustomerReference,a.CustomerName,a.Address,a.ContactPerson,a.Mobile Order by CustomerCode";

		
		//PostgreSQL
		if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB))
		{
			Query = "Select coalesce(a.CustomerId,0) as CustomerId,coalesce(a.CustomerCode,' ') as CustomerCode,coalesce(a.CustomerReference,' ') as CustomerReference,coalesce(a.CustomerName,' ') as CustomerName,coalesce(a.Address,' ') as Address,coalesce(a.ContactPerson,' ') as ContactPerson,coalesce(a.Mobile,' ') as Mobile, coalesce(Count(b.SiteId),0) as TotalSites,coalesce(sum(b.Installationcapacity),0) as Totalcapacity,coalesce(a.CustomerGroup,' ') as CustomerGroup,coalesce(a.Primarycustomer,0) as Primarycustomer,coalesce(a.PrimaryCustomerCode,' ') as PrimaryCustomerCode from mCustomer a left outer join mSite b on a.CustomerId=b.CustomerID where  a.ActiveFlag='1' and b.ActiveFlag='1' and a.CustomerID in (select CustomerID from mSite Where SiteId in (Select SiteId from mSiteMap where UserID='" + UserID + "' and ActiveFlag=1) and ActiveFlag=1) Group by a.CustomerId,a.CustomerCode,a.CustomerReference,a.CustomerName,a.Address,a.ContactPerson,a.Mobile,a.CustomerGroup,a.PrimaryCustomer,a.PrimaryCustomerCode Order by CustomerID";
		
		}

		System.out.println(Query);		
		List<CustomerListBean> CustomerList = session.createSQLQuery(Query).list();
		
	    return CustomerList;
}
   
   
   public List<CustomerListBean> getAjaxCustomerListByCustomer(String CustomerID) {
		Session session = sessionFactory.getCurrentSession();
		
		if(CustomerID == null)
		{ CustomerID = "0";}
		else if(CustomerID.equals(""))
		{ CustomerID = "0";}
		
		 
		//Oracle
		String Query = "Select nvl(a.CustomerId,0) as CustomerId,nvl(a.CustomerCode,' ') as CustomerCode,nvl(a.CustomerReference,' ') as CustomerReference,nvl(a.CustomerName,' ') as CustomerName,nvl(a.Address,' ') as Address,nvl(a.ContactPerson,' ') as ContactPerson,nvl(a.Mobile,' ') as Mobile, nvl(Count(b.SiteId),0) as TotalSites from mCustomer a left outer join mSite b on a.CustomerId=b.CustomerID where  a.ActiveFlag='1' and b.ActiveFlag='1' and a.CustomerID ='" + CustomerID + "' Group by a.CustomerId,a.CustomerCode,a.CustomerReference,a.CustomerName,a.Address,a.ContactPerson,a.Mobile Order by CustomerCode";

		//PostgreSQL
		if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB))
		{
			Query = "Select coalesce(a.CustomerId,0) as CustomerId,coalesce(a.CustomerCode,' ') as CustomerCode,coalesce(a.CustomerReference,' ') as CustomerReference,coalesce(a.CustomerName,' ') as CustomerName,coalesce(a.Address,' ') as Address,coalesce(a.ContactPerson,' ') as ContactPerson,coalesce(a.Mobile,' ') as Mobile, coalesce(Count(b.SiteId),0) as TotalSites from mCustomer a left outer join mSite b on a.CustomerId=b.CustomerID where  a.ActiveFlag='1' and b.ActiveFlag='1' and a.CustomerID ='" + CustomerID + "' Group by a.CustomerId,a.CustomerCode,a.CustomerReference,a.CustomerName,a.Address,a.ContactPerson,a.Mobile Order by CustomerCode";
		}
		System.out.println(Query);		
		List<CustomerListBean> CustomerList = session.createSQLQuery(Query).list();
		
	    return CustomerList;
 }
   
   public List<Customer> listallcustomers(int customerid) {
       Session session = sessionFactory.getCurrentSession();
       
       String Query = "from Customer where CustomerID not in('" + customerid + "')";
       List<Customer> CustomerList = session.createQuery(Query).list();
       
       
       
    return CustomerList;
}
   
   
   public  List<Customer> getPrimaryCustomerById(String id) {
	   
 Session session = sessionFactory.getCurrentSession();
       
       String Query = "from Customer where CustomerId in('" + id + "')";
       List<Customer> CustomerList =  session.createQuery(Query).list();
       
       
       
    return CustomerList;
	   
   }

   
   
}
