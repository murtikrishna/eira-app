
/******************************************************
 * 
 *    	Filename	: HistoricalUploadDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Historical upload operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.HistoricalUpload;

@Repository
public class HistoricalUploadDAOImpl implements HistoricalUploadDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public HistoricalUpload save(HistoricalUpload historicalUpload) {
		Session session = sessionFactory.getCurrentSession();
		Long id = (Long) session.save(historicalUpload);
		historicalUpload.setId(id);
		return historicalUpload;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<HistoricalUpload> list() {
		Session session = sessionFactory.getCurrentSession();
		List<HistoricalUpload> historicalUploads = session.createQuery("FROM HistoricalUpload").list();
		return historicalUploads;
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<HistoricalUpload> getHistory(String siteCode, String directory) {
		Session session = sessionFactory.getCurrentSession();
		@SuppressWarnings("rawtypes")
		Query query = session.createQuery("FROM HistoricalUpload WHERE siteCode=:siteCode AND directory=:directory");
		query.setParameter("siteCode", siteCode);
		query.setParameter("directory", directory);
		return query.list();
	}

}
