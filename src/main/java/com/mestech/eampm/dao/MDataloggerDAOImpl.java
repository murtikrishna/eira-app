
/******************************************************
 * 
 *    	Filename	: MDataloggerDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for datalogger related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.math.BigInteger;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.MDatalogger;

@Repository
public class MDataloggerDAOImpl implements MDataloggerDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public MDatalogger save(MDatalogger datalogger) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(datalogger);
		return datalogger;
	}

	@Override
	public MDatalogger update(MDatalogger datalogger) {
		Session session = sessionFactory.getCurrentSession();
		session.update(datalogger);
		return datalogger;
	}

	@Override
	public void delete(BigInteger id) {
		Session session = sessionFactory.getCurrentSession();
		MDatalogger mDatalogger = edit(id);
		session.delete(mDatalogger);
	}

	@Override
	public MDatalogger edit(BigInteger id) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM MDatalogger WHERE dataloggerid=:dataloggerid");
		query.setParameter("dataloggerid", id);
		return (MDatalogger) query.getSingleResult();
	}

	@Override
	public List<MDatalogger> listAll() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM MDatalogger WHERE activeflag=1");
		return query.list();
	}

}
