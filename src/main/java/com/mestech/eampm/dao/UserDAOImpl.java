
/******************************************************
 * 
 *    	Filename	: UserDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for User related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.config.CacheHandler;
import com.mestech.eampm.model.User;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addUser(User user) {

		Session session = sessionFactory.getCurrentSession();
		int userId = (int) session.save(user);
		user.setUserId(userId);
		Cache userCache = CacheHandler.getCache("userCache");
		userCache.put(new Element(userId, user));
	}

	// @Override
	public void updateUser(User user) {
		Session session = sessionFactory.getCurrentSession();
		session.update(user);
		Cache userCache = CacheHandler.getCache("userCache");
		userCache.put(new Element(user.getUserId(), user));

	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<User> listUsers() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<User> UsersList = session.createQuery("from User").list();

		return UsersList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<User> listFieldUsers() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<User> UsersList = session.createQuery(
				"from User where ActiveFlag=1 and RoleID in (Select RoleId from UserRole where RoleName='FieldUser')")
				.list();

		return UsersList;
	}

	// @Override
	public User getUserById(int id) {
		Session session = sessionFactory.getCurrentSession();
		User user = (User) session.get(User.class, new Integer(id));
		return user;
	}

	public User getUserByName(String userName) {

		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<User> UsersList = session.createQuery("from User where userName=:userName and ActiveFlag=1")
				.setParameter("userName", userName).list();
		if (UsersList != null && UsersList.size() > 0) {
			return UsersList.get(0);
		}
		return null;
	}

	public User getUserByMax(String MaxColumnName) {
		Session session = sessionFactory.getCurrentSession();
		User user = null;
		// Common for Oracle & PostgreSQL
		try {
			user = (User) session.createQuery("from User Order By " + MaxColumnName + " desc").setMaxResults(1)
					.getSingleResult();
		} catch (NoResultException ex) {
		} catch (Exception ex) {
		}

		return user;
	}

	// @Override
	public void removeUser(int id) {
		Session session = sessionFactory.getCurrentSession();
		User user = (User) session.get(User.class, new Integer(id));
		if (null != user) {
			session.delete(user);
		}
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<User> listAppUsers() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<User> UsersList = session.createQuery("from User where RoleID not in ('5')").list();

		return UsersList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<User> listUsers(int userid) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<User> UsersList = session.createQuery("from User where UserID not in('" + userid + "')").list();

		return UsersList;
	}

}
