
/******************************************************
 * 
 *    	Filename	: SiteDocumentDaoImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for site document operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.SiteDocumentation;

@SuppressWarnings("deprecation")
@Repository
public class SiteDocumentDaoImpl implements SiteDocumentationDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public SiteDocumentation save(SiteDocumentation siteDocumentation) {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			session.save(siteDocumentation);
			return siteDocumentation;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public SiteDocumentation update(SiteDocumentation siteDocumentation) {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			session.update(siteDocumentation);
			return siteDocumentation;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public void delete(Long id) {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			SiteDocumentation siteDocumentation = edit(id);
			session.delete(siteDocumentation);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public List<SiteDocumentation> listAll() {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			return session.createQuery("FROM SiteDocumentation WHERE activeflag=1").list();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	public SiteDocumentation edit(Long id) {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			Query query = session.createQuery("FROM SiteDocumentation WHERE id=:id");
			query.setParameter("id", id);
			return (SiteDocumentation) query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SiteDocumentation> findBySiteId(int id) {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			@SuppressWarnings("rawtypes")
			Query query = session.createQuery("FROM SiteDocumentation WHERE siteId=:id AND activeflag=1");
			query.setParameter("id", id);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void deleteDocuments(String siteCode, String docCategory, String docName) {
		Session session = null;

		session = sessionFactory.getCurrentSession();
		@SuppressWarnings("rawtypes")
		Query query = session.createQuery(
				"FROM SiteDocumentation WHERE siteCode=:siteCode AND docCategory=:docCategory AND fileName=:fileName");
		query.setParameter("siteCode", siteCode);
		query.setParameter("docCategory", docCategory);
		query.setParameter("fileName", docName);
		List<SiteDocumentation> documentations = query.list();
		if (documentations != null && !documentations.isEmpty()) {
			for (SiteDocumentation documentation : documentations) {
				documentation.setActiveflag(new Integer(0));
				update(documentation);
			}
		}
	}

	@Override
	public List<SiteDocumentation> findBySiteId(int id, String category) {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			@SuppressWarnings("rawtypes")
			Query query = session.createQuery("FROM SiteDocumentation WHERE siteId='" + id + "' and docCategory='"
					+ category + "' and activeflag=1");

			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
