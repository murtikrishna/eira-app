
/******************************************************
 * 
 *    	Filename	: SiteDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for site related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.bean.AnnualYeild;
import com.mestech.eampm.bean.DashboardEnergySummaryBean;
import com.mestech.eampm.bean.DashboardOandMBean;
import com.mestech.eampm.bean.DashboardSiteBean;
import com.mestech.eampm.bean.DashboardSiteListBean;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.utility.UtilityCommonDAO;

@Repository
public class SiteDAOImpl implements SiteDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();

	// @Override
	public Site addSite(Site site) {

		Session session = sessionFactory.getCurrentSession();
		Integer siteId = (Integer) session.save(site);
		site.setSiteId(siteId);
		return site;
	}

	// @Override
	public void updateSite(Site site) {

		Session session = sessionFactory.getCurrentSession();
		session.update(site);

	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Site> listSiteById(int id) {
		Session session1 = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Site where SiteId='" + id + "'";
		List<Site> SitesList = session1.createQuery(Query).list();

		return SitesList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Site> listSites() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Site where ActiveFlag=1";
		List<Site> SitesList = session.createQuery(Query).list();

		return SitesList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Site> listConfigSites() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Site";
		List<Site> SitesList = session.createQuery(Query).list();

		return SitesList;
	}

	// @Override
	public Site getSiteById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Site site = (Site) session.get(Site.class, new Integer(id));
		return site;
	}

	public Site getSiteByName(String siteName) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Site where ActiveFlag='1' and SiteName='" + siteName + "'";
		Site site = (Site) session.createQuery(Query).uniqueResult();

		return site;
	}

	public List<Site> getSiteListByCustomerId(int customerId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Site where ActiveFlag='1' and CustomerID='" + customerId + "'";
		List<Site> SitesList = session.createQuery(Query).list();

		return SitesList;
	}

	// 03-02-19
	/*
	 * public List<Site> getSiteListByUserId(int userId) { Session session =
	 * sessionFactory.getCurrentSession(); //Common for Oracle & PostgreSQL String
	 * Query =
	 * "from Site where  ActiveFlag='1' and CustomerID in (select CustomerId from Customer where CustomerId in (Select CustomerID from CustomerMap where UserID='"
	 * + userId +
	 * "' and ActiveFlag=1) or PrimaryCustomer in(Select CustomerID from CustomerMap where UserID='"
	 * + userId + "' and ActiveFlag=1))order by SiteId asc"; List<Site> SiteList =
	 * session.createQuery(Query).list();
	 * 
	 * return SiteList; }
	 */

	public List<Site> getSiteListByUserId(int userId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Site where  ActiveFlag='1' and SiteId in (Select SiteId from SiteMap where UserID='"
				+ userId + "' and ActiveFlag=1)  order by SiteId asc";
		List<Site> SiteList = session.createQuery(Query).list();

		return SiteList;
	}
	
	
	// 03-02-19
	/*
	 * public List<Site> getProductionSiteListByUserId(int userId) { Session session
	 * = sessionFactory.getCurrentSession(); //Common for Oracle & PostgreSQL String
	 * Query =
	 * "from Site where  ActiveFlag='1' and ProdFlag=1 and CustomerID in (Select CustomerID from CustomerMap where UserID='"
	 * + userId + "' and ActiveFlag=1)order by SiteId asc"; List<Site> SiteList =
	 * session.createQuery(Query).list();
	 * 
	 * return SiteList; }
	 */

	public List<Site> getProductionSiteListByUserId(int userId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Site where  ActiveFlag='1' and ProdFlag=1 and SiteId in  (Select SiteId from SiteMap where UserID='"
				+ userId + "' and ActiveFlag=1) order by SiteId asc";
		List<Site> SiteList = session.createQuery(Query).list();

		return SiteList;
	}

	public Site getSiteByMax(String MaxColumnName) {
		Session session = sessionFactory.getCurrentSession();
		Site site = null;
		// Common for Oracle & PostgreSQL
		String Query = "from Site Order By " + MaxColumnName + " desc";
		try {
			site = (Site) session.createQuery(Query).setMaxResults(1).getSingleResult();
		} catch (NoResultException ex) {
		} catch (Exception ex) {
		}

		return site;
	}

	// @Override
	public void removeSite(int id) {
		Session session = sessionFactory.getCurrentSession();
		Site site = (Site) session.get(Site.class, new Integer(id));

		// De-activate the flag
		site.setActiveFlag(0);

		if (null != site) {
			// session.delete(site);
			session.update(site);
		}
	}

	// 03-02-19
	/*
	 * public List<DashboardSiteBean> getDashboardSiteDetails(String UserID, String
	 * TimezoneOffset) { Session session = sessionFactory.getCurrentSession();
	 * 
	 * if(UserID == null) { UserID = "0";} else if(UserID.equals("")) { UserID =
	 * "0";}
	 * 
	 * //Oracle String Query = "Select nvl(Count(a.SiteID),0) as TotalSites, " +
	 * " nvl(Sum(case when SiteTypeID=1 then 1 else null end),0) as RoofTopSites, "
	 * +
	 * " nvl(Sum(case when SiteTypeID=2 then 1 else null end),0) as UtilitySites, "
	 * +
	 * " nvl(Sum(case when SiteStatus=0 then 1 else null end),0) as OfflineStatus, "
	 * +
	 * " nvl(Sum(case when SiteStatus=1 then 1 else null end),0) as ActiveStatus, "
	 * +
	 * " nvl(Sum(case when SiteStatus=2 then 1 else null end),0) as WarningStatus, "
	 * + " nvl(Sum(case when SiteStatus=3 then 1 else null end),0) as DownStatus " +
	 * " from mSite a,tSiteStatus b where a.SiteID=b.SiteID and a.ActiveFlag=1 and b.ActiveFlag=1 and a.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID + "'))"; //PostgreSQL
	 * if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) { Query =
	 * "Select coalesce(Count(a.SiteID),0) as TotalSites, " +
	 * " coalesce(Sum(case when SiteTypeID='1' then 1 else null end),0) as RoofTopSites, "
	 * +
	 * " coalesce(Sum(case when SiteTypeID='2' then 1 else null end),0) as UtilitySites, "
	 * +
	 * " coalesce(Sum(case when SiteStatus='0' then 1 else null end),0) as OfflineStatus, "
	 * +
	 * " coalesce(Sum(case when SiteStatus='1' then 1 else null end),0) as ActiveStatus, "
	 * +
	 * " coalesce(Sum(case when SiteStatus='2' then 1 else null end),0) as WarningStatus, "
	 * +
	 * " coalesce(Sum(case when SiteStatus='3' then 1 else null end),0) as DownStatus "
	 * +
	 * " from mSite as a,tSiteStatus as b where a.SiteID=b.SiteID and a.ActiveFlag=1 and b.ActiveFlag=1 and a.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID +
	 * "') or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID + "'))";
	 * 
	 * }
	 * 
	 * 
	 * System.out.println(Query); List<DashboardSiteBean> DashboardSiteDetailsList =
	 * session.createSQLQuery(Query).list();
	 * 
	 * return DashboardSiteDetailsList; }
	 */

	public List<DashboardSiteBean> getDashboardSiteDetails(String UserID, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		if (UserID == null) {
			UserID = "0";
		} else if (UserID.equals("")) {
			UserID = "0";
		}

		// Oracle
		String Query = "Select nvl(Count(a.SiteID),0) as TotalSites, "
				+ " nvl(Sum(case when SiteTypeID=1 then 1 else null end),0) as RoofTopSites, "
				+ " nvl(Sum(case when SiteTypeID=2 then 1 else null end),0) as UtilitySites, "
				+ " nvl(Sum(case when SiteStatus=0 then 1 else null end),0) as OfflineStatus, "
				+ " nvl(Sum(case when SiteStatus=1 then 1 else null end),0) as ActiveStatus, "
				+ " nvl(Sum(case when SiteStatus=2 then 1 else null end),0) as WarningStatus, "
				+ " nvl(Sum(case when SiteStatus=3 then 1 else null end),0) as DownStatus "
				+ " from mSite a,tSiteStatus b where a.SiteID=b.SiteID and a.ActiveFlag=1 and b.ActiveFlag=1 and a.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
				+ UserID + "'))";
		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select coalesce(Count(a.SiteID),0) as TotalSites, "
					+ " coalesce(Sum(case when SiteTypeID='1' then 1 else null end),0) as RoofTopSites, "
					+ " coalesce(Sum(case when SiteTypeID='2' then 1 else null end),0) as UtilitySites, "
					+ " coalesce(Sum(case when SiteStatus='0' then 1 else null end),0) as OfflineStatus, "
					+ " coalesce(Sum(case when SiteStatus='1' then 1 else null end),0) as ActiveStatus, "
					+ " coalesce(Sum(case when SiteStatus='2' then 1 else null end),0) as WarningStatus, "
					+ " coalesce(Sum(case when SiteStatus='3' then 1 else null end),0) as DownStatus "
					+ " from mSite as a,tSiteStatus as b where a.SiteID=b.SiteID and a.ActiveFlag=1 and b.ActiveFlag=1 and a.SiteID in (select SiteId from mSiteMap where UserID='"
					+ UserID + "' and ActiveFlag=1)";

		}

		System.out.println(Query);
		List<DashboardSiteBean> DashboardSiteDetailsList = session.createSQLQuery(Query).list();

		return DashboardSiteDetailsList;
	}

	// version 2.0 for Inverters

	/*
	 * public List<DashboardEnergySummaryBean>
	 * getDashboardEnergySummaryDetails(String UserID, String TimezoneOffset) {
	 * Session session = sessionFactory.getCurrentSession();
	 * 
	 * if(UserID == null) { UserID = "0";} else if(UserID.equals("")) { UserID =
	 * "0";}
	 * 
	 * //Oracle String Query =
	 * "Select nvl(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, nvl(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked, nvl(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, nvl(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime, nvl(Sum(TotalEnergy),0) as TotalEnergy, "
	 * +
	 * "  nvl(Sum(TodayEnergy),0) as TodayEnergy, nvl(Max(TodayHoursOn),0) as TodayHoursOn "
	 * +
	 * " from (Select a.SiteID,Max(a.Timestamp) as LastChecked,Max(LastDataReceived) as LastDataReceived,Sum(nvl(TotalEnergy,0)) as TotalEnergy, "
	 * +
	 * " (case when c.TodayEnergyFlag=1 then Sum(nvl(TodayEnergy,0)) else (Sum(nvl(TotalEnergy,0)) - Sum(nvl(YesterdayTotalEnergy,0))) end ) as TodayEnergy, "
	 * +
	 * " Max(LastDownTime) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from tDataSource a,mEquipment b, mSite c "
	 * +
	 * " where a.SiteID=b.SiteID and a.SiteID=c.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1 and c.ActiveFlag=1 and "
	 * +
	 * " c.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and "
	 * +
	 * " CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID + "')) and " +
	 * " EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where " +
	 * " CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID,c.TodayEnergyFlag)"
	 * ;
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * Query =
	 * "Select coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked, coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy, "
	 * +
	 * "  coalesce(Sum(TodayEnergy),0) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 "
	 * +
	 * " from (Select a.SiteID,Max(a.Timestamp) as LastChecked,Max(LastDataReceived) as LastDataReceived,Sum(coalesce(TotalEnergy,0)) as TotalEnergy, "
	 * +
	 * " (case when c.TodayEnergyFlag=1 then Sum(coalesce(TodayEnergy,0)) else (Sum(coalesce(TotalEnergy,0)) - Sum(coalesce(YesterdayTotalEnergy,0))) end ) as TodayEnergy, "
	 * +
	 * " Max(LastDownTime) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from tDataSource as a,mEquipment as b, mSite as c "
	 * +
	 * " where a.SiteID=b.SiteID and a.SiteID=c.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1 and c.ActiveFlag=1 and "
	 * +
	 * " c.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and "
	 * +
	 * " CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID + "')) and " +
	 * " EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where " +
	 * " CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID,c.TodayEnergyFlag) as tbl"
	 * ;
	 * 
	 * 
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset +
	 * " * interval '1 minute') ";
	 * 
	 * Query =
	 * "Select coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked, coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy, "
	 * +
	 * "  coalesce(Sum(TodayEnergy),0) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 "
	 * + " from (Select a.SiteID,Max(a.Timestamp" + TimeConversionFromUTC +
	 * ") as LastChecked,Max(LastDataReceived" + TimeConversionFromUTC +
	 * ") as LastDataReceived,Sum(coalesce(TotalEnergy,0)) as TotalEnergy, " +
	 * " (case when c.TodayEnergyFlag=1 then Sum(coalesce(TodayEnergy,0)) else (Sum(coalesce(TotalEnergy,0)) - Sum(coalesce(YesterdayTotalEnergy,0))) end ) as TodayEnergy, "
	 * + " Max(LastDownTime" + TimeConversionFromUTC +
	 * ") as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from tDataSource as a,mEquipment as b, mSite as c "
	 * +
	 * " where a.SiteID=b.SiteID and a.SiteID=c.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1 and c.ActiveFlag=1 and "
	 * +
	 * " c.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and "
	 * +
	 * " CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID + "')) and " +
	 * " EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where " +
	 * " CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID,c.TodayEnergyFlag) as tbl"
	 * ;
	 * 
	 * 
	 * 
	 * } } System.out.println(Query); List<DashboardEnergySummaryBean>
	 * DashboardEnergySummaryDetailsList = session.createSQLQuery(Query).list();
	 * 
	 * return DashboardEnergySummaryDetailsList; }
	 */

	// Common for Inverters and Energy Meters 03-02-19
	/*
	 * public List<DashboardEnergySummaryBean>
	 * getDashboardEnergySummaryDetails(String UserID, String TimezoneOffset) {
	 * Session session = sessionFactory.getCurrentSession();
	 * 
	 * if(UserID == null) { UserID = "0";} else if(UserID.equals("")) { UserID =
	 * "0";}
	 * 
	 * //Oracle String Query =
	 * "Select nvl(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, nvl(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked, nvl(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, nvl(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime, nvl(Sum(TotalEnergy),0) as TotalEnergy, "
	 * +
	 * "  nvl(Sum(TodayEnergy),0) as TodayEnergy, nvl(Max(TodayHoursOn),0) as TodayHoursOn "
	 * +
	 * " from (Select a.SiteID,Max(a.Timestamp) as LastChecked,Max(LastDataReceived) as LastDataReceived,Sum(nvl(TotalEnergy,0)) as TotalEnergy, "
	 * +
	 * " (case when c.TodayEnergyFlag=1 then Sum(nvl(TodayEnergy,0)) else (Sum(nvl(TotalEnergy,0)) - Sum(nvl(YesterdayTotalEnergy,0))) end ) as TodayEnergy, "
	 * +
	 * " Max(LastDownTime) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from tDataSource a,mEquipment b, mSite c "
	 * +
	 * " where a.SiteID=b.SiteID and a.SiteID=c.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1 and c.ActiveFlag=1 and "
	 * +
	 * " c.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and "
	 * +
	 * " CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID + "')) and " +
	 * " EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where " +
	 * " CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID,c.TodayEnergyFlag)"
	 * ;
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * 
	 * 
	 * Query="Select coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked, coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy, coalesce(Sum(TodayEnergy),0)  as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(Select (Max(LastChecked)) as ProductionOn, Max(LastChecked) as LastChecked, Max(LastDataReceived) as LastDataReceived, Max(LastDownTime) as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy,   (case when TodayEnergyFlag=1 then coalesce(Sum(TodayEnergy),0)  else coalesce(Sum(TotalEnergy)-Sum(YesterdayTotalEnergy),0) end) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(select e.TodayEnergyFlag,c.categorygroup,d.SiteID,Max(Timestamp + (330 * interval '1 minute') ) as LastChecked,Max(LastDataReceived + (330 * interval '1 minute') ) as LastDataReceived,(case when e.equipmentflag=1 and c.categorygroup='EM' then Sum(coalesce(TotalEnergy,0)) else  case when e.equipmentflag=0 and c.categorygroup='INV' then Sum(coalesce(TotalEnergy,0)) else 0 end end) as TotalEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(TodayEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(TotalEnergy,0))) else 0 end end) as TodayEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(YesterdayTotalEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(YesterdayTotalEnergy,0))) else 0 end end) as YesterdayTotalEnergy,Max(LastDownTime + (330 * interval '1 minute') ) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from tdatasource d left outer join mequipment a on d.equipmentid=a.equipmentid left outer join msite e on  e.siteid=a.siteid left outer join mequipmenttype b on a.equipmenttypeid=b.equipmenttypeid left outer join mequipmentcategory c on c.categoryid=b.categoryid where d.siteid=a.siteid and d.siteid=e.siteid and d.activeflag=1 and e.activeflag=1 and a.activeflag=1 and e.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID
	 * +"') or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID
	 * +"')) and c.categorygroup in('INV','EM') group by d.siteid,e.TodayEnergyFlag,e.equipmentflag,e.todayenergyflag,c.categorygroup ) as tbl group by todayenergyflag) tbl2"
	 * ;
	 * 
	 * 
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset +
	 * " * interval '1 minute') ";
	 * 
	 * Query="Select coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked, coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy, coalesce(Sum(TodayEnergy),0)  as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(Select (Max(LastChecked)) as ProductionOn, Max(LastChecked) as LastChecked, Max(LastDataReceived) as LastDataReceived, Max(LastDownTime) as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy,   (case when TodayEnergyFlag=1 then coalesce(Sum(TodayEnergy),0)  else coalesce(Sum(TotalEnergy)-Sum(YesterdayTotalEnergy),0) end) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(select e.TodayEnergyFlag,c.categorygroup,d.SiteID,Max(Timestamp "
	 * +TimeConversionFromUTC+" ) as LastChecked,Max(LastDataReceived "
	 * +TimeConversionFromUTC+" ) as LastDataReceived,(case when e.equipmentflag=1 and c.categorygroup='EM' then Sum(coalesce(TotalEnergy,0)) else  case when e.equipmentflag=0 and c.categorygroup='INV' then Sum(coalesce(TotalEnergy,0)) else 0 end end) as TotalEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(TodayEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(TotalEnergy,0))) else 0 end end) as TodayEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(YesterdayTotalEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(YesterdayTotalEnergy,0))) else 0 end end) as YesterdayTotalEnergy,Max(LastDownTime "
	 * +TimeConversionFromUTC+" ) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from tdatasource d left outer join mequipment a on d.equipmentid=a.equipmentid left outer join msite e on  e.siteid=a.siteid left outer join mequipmenttype b on a.equipmenttypeid=b.equipmenttypeid left outer join mequipmentcategory c on c.categoryid=b.categoryid where d.siteid=a.siteid and d.siteid=e.siteid and d.activeflag=1 and e.activeflag=1 and a.activeflag=1 and e.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID
	 * +"') or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID
	 * +"') or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID
	 * +"') ) and c.categorygroup in('INV','EM') group by d.siteid,e.TodayEnergyFlag,e.equipmentflag,e.todayenergyflag,c.categorygroup ) as tbl group by todayenergyflag) tbl2"
	 * ;
	 * 
	 * 
	 * 
	 * } } System.out.println(Query); List<DashboardEnergySummaryBean>
	 * DashboardEnergySummaryDetailsList = session.createSQLQuery(Query).list();
	 * 
	 * return DashboardEnergySummaryDetailsList; }
	 */

	public List<DashboardEnergySummaryBean> getDashboardEnergySummaryDetails(String UserID, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		if (UserID == null) {
			UserID = "0";
		} else if (UserID.equals("")) {
			UserID = "0";
		}

		// Oracle
		String Query = "Select nvl(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, nvl(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked, nvl(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, nvl(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime, nvl(Sum(TotalEnergy),0) as TotalEnergy, "
				+ "  nvl(Sum(TodayEnergy),0) as TodayEnergy, nvl(Max(TodayHoursOn),0) as TodayHoursOn "
				+ " from (Select a.SiteID,Max(a.Timestamp) as LastChecked,Max(LastDataReceived) as LastDataReceived,Sum(nvl(TotalEnergy,0)) as TotalEnergy, "
				+ " (case when c.TodayEnergyFlag=1 then Sum(nvl(TodayEnergy,0)) else (Sum(nvl(TotalEnergy,0)) - Sum(nvl(YesterdayTotalEnergy,0))) end ) as TodayEnergy, "
				+ " Max(LastDownTime) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from tDataSource a,mEquipment b, mSite c "
				+ " where a.SiteID=b.SiteID and a.SiteID=c.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1 and c.ActiveFlag=1 and "
				+ " c.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and "
				+ " CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='" + UserID
				+ "')) and " + " EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where "
				+ " CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID,c.TodayEnergyFlag)";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {

			Query = "Select coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked, coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy, coalesce(Sum(TodayEnergy),0)  as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(Select (Max(LastChecked)) as ProductionOn, Max(LastChecked) as LastChecked, Max(LastDataReceived) as LastDataReceived, Max(LastDownTime) as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy,   (case when TodayEnergyFlag=1 then coalesce(Sum(TodayEnergy),0)  else coalesce(Sum(TotalEnergy)-Sum(YesterdayTotalEnergy),0) end) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(select e.TodayEnergyFlag,c.categorygroup,d.SiteID,Max(Timestamp + (330 * interval '1 minute') ) as LastChecked,Max(LastDataReceived + (330 * interval '1 minute') ) as LastDataReceived,(case when e.equipmentflag=1 and c.categorygroup='EM' then Sum(coalesce(TotalEnergy,0)) else  case when e.equipmentflag=0 and c.categorygroup='INV' then Sum(coalesce(TotalEnergy,0)) else 0 end end) as TotalEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(TodayEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(TotalEnergy,0))) else 0 end end) as TodayEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(YesterdayTotalEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(YesterdayTotalEnergy,0))) else 0 end end) as YesterdayTotalEnergy,Max(LastDownTime + (330 * interval '1 minute') ) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from tdatasource d left outer join mequipment a on d.equipmentid=a.equipmentid left outer join msite e on  e.siteid=a.siteid left outer join mequipmenttype b on a.equipmenttypeid=b.equipmenttypeid left outer join mequipmentcategory c on c.categoryid=b.categoryid where d.siteid=a.siteid and d.siteid=e.siteid and d.activeflag=1 and e.activeflag=1 and a.activeflag=1 and e.SiteID in (Select SiteId from mSiteMap where UserID='"
					+ UserID
					+ "' and ActiveFlag=1) and c.categorygroup in('INV','EM') group by d.siteid,e.TodayEnergyFlag,e.equipmentflag,e.todayenergyflag,c.categorygroup ) as tbl group by todayenergyflag) tbl2";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "Select coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked, coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy, coalesce(Sum(TodayEnergy),0)  as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(Select (Max(LastChecked)) as ProductionOn, Max(LastChecked) as LastChecked, Max(LastDataReceived) as LastDataReceived, Max(LastDownTime) as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy,   (case when TodayEnergyFlag=1 then coalesce(Sum(TodayEnergy),0)  else coalesce(Sum(TotalEnergy)-Sum(YesterdayTotalEnergy),0) end) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(select e.TodayEnergyFlag,c.categorygroup,d.SiteID,Max(Timestamp "
						+ TimeConversionFromUTC + " ) as LastChecked,Max(LastDataReceived " + TimeConversionFromUTC
						+ " ) as LastDataReceived,(case when e.equipmentflag=1 and c.categorygroup='EM' then Sum(coalesce(TotalEnergy,0)) else  case when e.equipmentflag=0 and c.categorygroup='INV' then Sum(coalesce(TotalEnergy,0)) else 0 end end) as TotalEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(TodayEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(TotalEnergy,0))) else 0 end end) as TodayEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(YesterdayTotalEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(YesterdayTotalEnergy,0))) else 0 end end) as YesterdayTotalEnergy,Max(LastDownTime "
						+ TimeConversionFromUTC
						+ " ) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from tdatasource d left outer join mequipment a on d.equipmentid=a.equipmentid left outer join msite e on  e.siteid=a.siteid left outer join mequipmenttype b on a.equipmenttypeid=b.equipmenttypeid left outer join mequipmentcategory c on c.categoryid=b.categoryid where d.siteid=a.siteid and d.siteid=e.siteid and d.activeflag=1 and e.activeflag=1 and a.activeflag=1 and e.SiteID in (Select SiteId from mSiteMap where UserID='"
						+ UserID
						+ "' and ActiveFlag=1) and  c.categorygroup in('INV','EM') group by d.siteid,e.TodayEnergyFlag,e.equipmentflag,e.todayenergyflag,c.categorygroup ) as tbl group by todayenergyflag) tbl2";

			}
		}
		System.out.println(Query);
		List<DashboardEnergySummaryBean> DashboardEnergySummaryDetailsList = session.createSQLQuery(Query).list();

		return DashboardEnergySummaryDetailsList;
	}

	// 03-02-19
	/*
	 * public List<DashboardOandMBean> getDashboardEventAndTicketDetails(String
	 * UserID, String TimezoneOffset) { Session session =
	 * sessionFactory.getCurrentSession();
	 * 
	 * if(UserID == null) { UserID = "0";} else if(UserID.equals("")) { UserID =
	 * "0";}
	 * 
	 * //Oracle String Query =
	 * "Select TotalTicket,OpenTicket,ClosedTicket,TotalEvent,TodayEvent from " +
	 * " (Select 'OM' as OM, nvl(Count(TicketID),0) as TotalTicket, " +
	 * " nvl(Sum(case when State=1 then 1 else null end),0) as OpenTicket, " +
	 * " nvl(Sum(case when State=2 then 1 else null end),0) as ClosedTicket " +
	 * " from tTicketDetail where ActiveFlag=1  and  SiteID in (Select SiteId from mSite where ActiveFlag=1  and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID + "')))) a, " +
	 * " (Select 'OM' as OM, nvl(Count(TransactionId),0) as TotalEvent, " +
	 * " nvl(Sum(case when Trunc(EventTimestamp)=Trunc(sysdate) then 1 else null end),0) as TodayEvent "
	 * +
	 * " from tEventDetail where ActiveFlag=1  and SiteID in (Select SiteId from mSite where ActiveFlag=1  and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID + "')))) b where a.OM=b.OM ";
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * Query =
	 * "Select TotalTicket,OpenTicket,ClosedTicket,TotalEvent,TodayEvent from " +
	 * " (Select character varying(2) 'OM' as OM, coalesce(Count(TicketID),0) as TotalTicket, "
	 * + " coalesce(Sum(case when State=1 then 1 else null end),0) as OpenTicket, "
	 * + " coalesce(Sum(case when State=2 then 1 else null end),0) as ClosedTicket "
	 * +
	 * " from tTicketDetail where ActiveFlag=1  and SiteID in (Select SiteId from mSite where ActiveFlag=1  and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID +
	 * "') or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID +"')))) as a, " +
	 * " (Select character varying(2) 'OM' as OM, coalesce(Count(TransactionId),0) as TotalEvent, "
	 * +
	 * " coalesce(Sum(case when DATE_TRUNC('day',EventTimestamp)=DATE_TRUNC('day', now()) then 1 else null end),0) as TodayEvent "
	 * +
	 * " from tEventDetail where ActiveFlag=1  and SiteID in (Select SiteId from mSite where ActiveFlag=1  and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID +
	 * "') or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID +"')))) as b where a.OM=b.OM ";
	 * 
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset +
	 * " * interval '1 minute') ";
	 * 
	 * Query =
	 * "Select TotalTicket,OpenTicket,ClosedTicket,TotalEvent,TodayEvent from " +
	 * " (Select character varying(2) 'OM' as OM, coalesce(Count(TicketID),0) as TotalTicket, "
	 * + " coalesce(Sum(case when State=1 then 1 else null end),0) as OpenTicket, "
	 * + " coalesce(Sum(case when State=2 then 1 else null end),0) as ClosedTicket "
	 * +
	 * " from tTicketDetail where ActiveFlag=1  and SiteID in (Select SiteId from mSite where ActiveFlag=1  and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID +
	 * "') or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID +"')))) as a, " +
	 * " (Select character varying(2) 'OM' as OM, coalesce(Count(TransactionId),0) as TotalEvent, "
	 * + " coalesce(Sum(case when DATE_TRUNC('day',EventTimestamp" +
	 * TimeConversionFromUTC + ")=DATE_TRUNC('day', now()" + TimeConversionFromUTC +
	 * ") then 1 else null end),0) as TodayEvent " +
	 * " from tEventDetail where ActiveFlag=1  and SiteID in (Select SiteId from mSite where ActiveFlag=1  and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID +
	 * "') or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID +"')))) as b where a.OM=b.OM ";
	 * 
	 * } }
	 * 
	 * System.out.println(Query); List<DashboardOandMBean>
	 * DashboardEventAndTicketDetailsList = session.createSQLQuery(Query).list();
	 * 
	 * return DashboardEventAndTicketDetailsList; }
	 */

	public List<DashboardOandMBean> getDashboardEventAndTicketDetails(String UserID, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		if (UserID == null) {
			UserID = "0";
		} else if (UserID.equals("")) {
			UserID = "0";
		}

		// Oracle
		String Query = "Select TotalTicket,OpenTicket,ClosedTicket,TotalEvent,TodayEvent from "
				+ " (Select 'OM' as OM, nvl(Count(TicketID),0) as TotalTicket, "
				+ " nvl(Sum(case when State=1 then 1 else null end),0) as OpenTicket, "
				+ " nvl(Sum(case when State=2 then 1 else null end),0) as ClosedTicket "
				+ " from tTicketDetail where ActiveFlag=1  and  SiteID in (Select SiteId from mSite where ActiveFlag=1  and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
				+ UserID + "')))) a, " + " (Select 'OM' as OM, nvl(Count(TransactionId),0) as TotalEvent, "
				+ " nvl(Sum(case when Trunc(EventTimestamp)=Trunc(sysdate) then 1 else null end),0) as TodayEvent "
				+ " from tEventDetail where ActiveFlag=1  and SiteID in (Select SiteId from mSite where ActiveFlag=1  and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
				+ UserID + "')))) b where a.OM=b.OM ";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select TotalTicket,OpenTicket,ClosedTicket,TotalEvent,TodayEvent from "
					+ " (Select character varying(2) 'OM' as OM, coalesce(Count(TicketID),0) as TotalTicket, "
					+ " coalesce(Sum(case when State=1 then 1 else null end),0) as OpenTicket, "
					+ " coalesce(Sum(case when State=2 then 1 else null end),0) as ClosedTicket "
					+ " from tTicketDetail where ActiveFlag=1 and ticketstatus not in(6,7) and SiteID in (select SiteId from msitemap where UserID='"
					+ UserID + "' and Activeflag=1)) as a, "
					+ " (Select character varying(2) 'OM' as OM, coalesce(Count(TransactionId),0) as TotalEvent, "
					+ " coalesce(Sum(case when DATE_TRUNC('day',EventTimestamp)=DATE_TRUNC('day', now()) then 1 else null end),0) as TodayEvent "
					+ " from tEventDetail where ActiveFlag=1  and SiteID in (Select SiteId from mSiteMap where UserID='"
					+ UserID + "' and ActiveFlag=1)) as b where a.OM=b.OM ";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "Select TotalTicket,OpenTicket,ClosedTicket,TotalEvent,TodayEvent from "
						+ " (Select character varying(2) 'OM' as OM, coalesce(Count(TicketID),0) as TotalTicket, "
						+ " coalesce(Sum(case when State=1 then 1 else null end),0) as OpenTicket, "
						+ " coalesce(Sum(case when State=2 then 1 else null end),0) as ClosedTicket "
						+ " from tTicketDetail where ActiveFlag=1 and ticketstatus not in(6,7) and SiteID in (select SiteId from msitemap where UserID='"
						+ UserID + "' and Activeflag=1)) as a, "
						+ " (Select character varying(2) 'OM' as OM, coalesce(Count(TransactionId),0) as TotalEvent, "
						+ " coalesce(Sum(case when DATE_TRUNC('day',EventTimestamp" + TimeConversionFromUTC
						+ ")=DATE_TRUNC('day', now()" + TimeConversionFromUTC
						+ ") then 1 else null end),0) as TodayEvent "
						+ " from tEventDetail where ActiveFlag=1  and SiteID in (Select SiteId from mSiteMap where UserID='"
						+ UserID + "' and ActiveFlag=1)) as b where a.OM=b.OM ";

			}
		}

		System.out.println(Query);
		List<DashboardOandMBean> DashboardEventAndTicketDetailsList = session.createSQLQuery(Query).list();

		return DashboardEventAndTicketDetailsList;
	}

	/*
	 * public List<DashboardSiteListBean> getDashboardSiteList(String UserID, String
	 * TimezoneOffset) { Session session = sessionFactory.getCurrentSession();
	 * 
	 * if(UserID == null) { UserID = "0";} else if(UserID.equals("")) { UserID =
	 * "0";}
	 * 
	 * //Oracle String Query =
	 * "Select  nvl(tbl1.SiteId,0) as SiteId,nvl(case when Sitetypeid=1 then 'Rooftop' else case when Sitetypeid=2 then 'Utility' end end) as sitetype,nvl(SiteCode,' ') as SiteCode,nvl(SiteReference,' ') as SiteReference,nvl(SiteName,' ') as SiteName,nvl(Latitude,' ') as Latitude,nvl(Longitude,' ') as Longitude,nvl(Address,' ') as Address,nvl(City,' ') as City,nvl(State,' ')  as State,nvl(Country,' ') as Country,nvl(PostalCode,' ') as PostalCode,nvl(ActiveInverterCount,0) as ActiveInverterCount,nvl(InverterCount,0) as InverterCount,nvl(LastChecked,' ') as LastChecked,nvl(TotalEnergy,0) as TotalEnergy,(case when TodayEnergyFlag=1 then nvl(TodayEnergy,0) else (nvl(TotalEnergy,0)-nvl(YesterdayTotalEnergy,0)) end) as TodayEnergy,nvl(LastDownTime,' ' ) as LastDownTime,nvl(LastDataReceived,' ') as LastDataReceived,nvl(TodayHoursOn,0) as TodayHoursOn,nvl((case when Status=-1 then 'Active' else case when Status=2 then 'Warning' else case when Status=3 then 'Down' else 'Offline' end end end),'Offline') as Status,nvl(InstallationCapacity,0) as InstallationCapacity,nvl(Mobile,' ') as Mobile,nvl(Telephone,' ') as Telephone,nvl(LocationUrl,' ') as LocationUrl,SitePoDate from  "
	 * +
	 * " (Select SiteId,SiteCode,TodayEnergyFlag,CustomerReference as SiteReference,SiteName,Latitude,Longitude,Address,City,StateName as State,CountryName as Country,PostalCode,InstallationCapacity,Mobile,Telephone,SiteImage as LocationUrl,SitePoDate from mSite a,mState b,mCountry c where a.StateID=b.StateId and a.CountryID=c.CountryId and a.ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID + "'))) tbl1 left outer join " +
	 * " (Select a.SiteID,Count(case when a.Status=1 and  b.DismandalFlag=0  then 1 else null end) as ActiveInverterCount,Count(distinct (case when b.DismandalFlag=0 then b.EquipmentID else null end)) as InverterCount,to_char(Max(a.Timestamp),'DD-MM-YYYY HH24:MI') as LastChecked,Sum(nvl(TodayEnergy,0)) as TodayEnergy,Sum(nvl(TotalEnergy,0)) as TotalEnergy,Max(nvl((case when Status=1 then -1 else Status end),0)) as Status,Sum(nvl(YesterdayTotalEnergy,0)) as YesterdayTotalEnergy,to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI') as LastDownTime,to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI') as LastDataReceived,Max(TodayHoursOn) as TodayHoursOn from tDataSource a,mEquipment b where a.SiteID=b.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1  and a.SiteID in (Select SiteId from mSite where ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID +
	 * "'))) and EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID) tbl2 on tbl1.SiteID=tbl2.SiteID order by SiteCode "
	 * ;
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * Query =
	 * "Select  coalesce(tbl1.SiteId,0) as SiteId,coalesce((case when tbl1.Sitetypeid=1 then 'Rooftop' else case when tbl1.Sitetypeid=2 then 'Utility' end end),' ') as Sitetype,coalesce(SiteCode,' ') as SiteCode,coalesce(SiteReference,' ') as SiteReference,coalesce(SiteName,' ') as SiteName,coalesce(Latitude,' ') as Latitude,coalesce(Longitude,' ') as Longitude,coalesce(Address,' ') as Address,coalesce(City,' ') as City,coalesce(State,' ')  as State,coalesce(Country,' ') as Country,coalesce(PostalCode,' ') as PostalCode,coalesce(ActiveInverterCount,0) as ActiveInverterCount,coalesce(InverterCount,0) as InverterCount,coalesce(LastChecked,' ') as LastChecked,coalesce(TotalEnergy,0) as TotalEnergy,(case when TodayEnergyFlag=1 then coalesce(TodayEnergy,0) else (coalesce(TotalEnergy,0)-coalesce(YesterdayTotalEnergy,0)) end) as TodayEnergy,coalesce(LastDownTime,' ' ) as LastDownTime,coalesce(LastDataReceived,' ') as LastDataReceived,coalesce(TodayHoursOn,0) as TodayHoursOn,coalesce((case when Status=-1 then 'Active' else case when Status=2 then 'Warning' else case when Status=3 then 'Down' else 'Offline' end end end),'Offline') as Status,coalesce(InstallationCapacity,0) as InstallationCapacity,coalesce(Mobile,' ') as Mobile,coalesce(Telephone,' ') as Telephone,coalesce(LocationUrl,' ') as LocationUrl,coalesce(SitePoDate,' ') as SitePoDate from  "
	 * +
	 * " (Select SiteId,Sitetypeid,SiteCode,TodayEnergyFlag,CustomerReference as SiteReference,SiteName,Latitude,Longitude,Address,City,StateName as State,CountryName as Country,PostalCode,InstallationCapacity,Mobile,Telephone,SiteImage as LocationUrl,to_char(SitePoDate + (330 * interval '1 minute') ,'DD-MM-YYYY HH24:MI') as SitePoDate from mSite as a,mState as b,mCountry as c where a.StateID=b.StateId and a.CountryID=c.CountryId and a.ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID + "'))) tbl1 left outer join " +
	 * " (Select a.SiteID,Count(case when a.Status=1 and  b.DismandalFlag=0  then 1 else null end) as ActiveInverterCount,Count(distinct (case when b.DismandalFlag=0 then b.EquipmentID else null end)) as InverterCount,to_char(Max(a.Timestamp),'DD-MM-YYYY HH24:MI') as LastChecked,Sum(coalesce(TodayEnergy,0)) as TodayEnergy,Sum(coalesce(TotalEnergy,0)) as TotalEnergy,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,Sum(coalesce(YesterdayTotalEnergy,0)) as YesterdayTotalEnergy,to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI') as LastDownTime,to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI') as LastDataReceived,Max(TodayHoursOn) as TodayHoursOn from tDataSource as a,mEquipment as b where a.SiteID=b.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1  and a.SiteID in (Select SiteId from mSite where ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID +
	 * "'))) and EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID) as tbl2 on tbl1.SiteID=tbl2.SiteID order by Status "
	 * ;
	 * 
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset +
	 * " * interval '1 minute') ";
	 * 
	 * Query =
	 * "Select  coalesce(tbl1.SiteId,0) as SiteId,coalesce((case when tbl1.Sitetypeid=1 then 'Rooftop' else case when tbl1.Sitetypeid=2 then 'Utility' end end),' ') as Sitetype,coalesce(SiteCode,' ') as SiteCode,coalesce(SiteReference,' ') as SiteReference,coalesce(SiteName,' ') as SiteName,coalesce(Latitude,' ') as Latitude,coalesce(Longitude,' ') as Longitude,coalesce(Address,' ') as Address,coalesce(City,' ') as City,coalesce(State,' ')  as State,coalesce(Country,' ') as Country,coalesce(PostalCode,' ') as PostalCode,coalesce(ActiveInverterCount,0) as ActiveInverterCount,coalesce(InverterCount,0) as InverterCount,coalesce(LastChecked,' ') as LastChecked,coalesce(TotalEnergy,0) as TotalEnergy,(case when TodayEnergyFlag=1 then coalesce(TodayEnergy,0) else (coalesce(TotalEnergy,0)-coalesce(YesterdayTotalEnergy,0)) end) as TodayEnergy,coalesce(LastDownTime,' ' ) as LastDownTime,coalesce(LastDataReceived,' ') as LastDataReceived,coalesce(TodayHoursOn,0) as TodayHoursOn,coalesce((case when Status=-1 then 'Active' else case when Status=2 then 'Warning' else case when Status=3 then 'Down' else 'Offline' end end end),'Offline') as Status,coalesce(InstallationCapacity,0) as InstallationCapacity,coalesce(Mobile,' ') as Mobile,coalesce(Telephone,' ') as Telephone,coalesce(LocationUrl,' ') as LocationUrl,coalesce(SitePoDate,' ') as SitePoDate from  "
	 * +
	 * " (Select SiteId,SiteTypeID,SiteCode,TodayEnergyFlag,CustomerReference as SiteReference,SiteName,Latitude,Longitude,Address,City,StateName as State,CountryName as Country,PostalCode,InstallationCapacity,Mobile,Telephone,SiteImage as LocationUrl,to_char(SitePoDate + (330 * interval '1 minute') ,'DD-MM-YYYY HH24:MI') as SitePoDate from mSite as a,mState as b,mCountry as c where a.StateID=b.StateId and a.CountryID=c.CountryId and a.ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID + "'))) tbl1 left outer join " +
	 * " (Select a.SiteID,Count(case when a.Status=1 and  b.DismandalFlag=0  then 1 else null end) as ActiveInverterCount,Count(distinct (case when b.DismandalFlag=0 then b.EquipmentID else null end)) as InverterCount,to_char(Max(a.Timestamp"
	 * + TimeConversionFromUTC +
	 * "),'DD-MM-YYYY HH24:MI') as LastChecked,Sum(coalesce(TodayEnergy,0)) as TodayEnergy,Sum(coalesce(TotalEnergy,0)) as TotalEnergy,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,Sum(coalesce(YesterdayTotalEnergy,0)) as YesterdayTotalEnergy,to_char(Max(LastDownTime"
	 * + TimeConversionFromUTC +
	 * "),'DD-MM-YYYY HH24:MI') as LastDownTime,to_char(Max(LastDataReceived" +
	 * TimeConversionFromUTC +
	 * "),'DD-MM-YYYY HH24:MI') as LastDataReceived,Max(TodayHoursOn) as TodayHoursOn from tDataSource as a,mEquipment as b where a.SiteID=b.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1  and a.SiteID in (Select SiteId from mSite where ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID +
	 * "'))) and EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID) as tbl2 on tbl1.SiteID=tbl2.SiteID order by Status "
	 * ;
	 * 
	 * 
	 * } }
	 * 
	 * System.out.println(Query); List<DashboardSiteListBean> DashboardSiteList =
	 * session.createSQLQuery(Query).list();
	 * 
	 * return DashboardSiteList; }
	 */

	// 03-02-19
	// New query Common for EM And Inverter
	/*
	 * public List<DashboardSiteListBean> getDashboardSiteList(String UserID, String
	 * TimezoneOffset) { Session session = sessionFactory.getCurrentSession();
	 * 
	 * if(UserID == null) { UserID = "0";} else if(UserID.equals("")) { UserID =
	 * "0";}
	 * 
	 * //Oracle String Query =
	 * "Select  nvl(tbl1.SiteId,0) as SiteId,nvl(case when Sitetypeid=1 then 'Rooftop' else case when Sitetypeid=2 then 'Utility' end end) as sitetype,nvl(SiteCode,' ') as SiteCode,nvl(SiteReference,' ') as SiteReference,nvl(SiteName,' ') as SiteName,nvl(Latitude,' ') as Latitude,nvl(Longitude,' ') as Longitude,nvl(Address,' ') as Address,nvl(City,' ') as City,nvl(State,' ')  as State,nvl(Country,' ') as Country,nvl(PostalCode,' ') as PostalCode,nvl(ActiveInverterCount,0) as ActiveInverterCount,nvl(InverterCount,0) as InverterCount,nvl(LastChecked,' ') as LastChecked,nvl(TotalEnergy,0) as TotalEnergy,(case when TodayEnergyFlag=1 then nvl(TodayEnergy,0) else (nvl(TotalEnergy,0)-nvl(YesterdayTotalEnergy,0)) end) as TodayEnergy,nvl(LastDownTime,' ' ) as LastDownTime,nvl(LastDataReceived,' ') as LastDataReceived,nvl(TodayHoursOn,0) as TodayHoursOn,nvl((case when Status=-1 then 'Active' else case when Status=2 then 'Warning' else case when Status=3 then 'Down' else 'Offline' end end end),'Offline') as Status,nvl(InstallationCapacity,0) as InstallationCapacity,nvl(Mobile,' ') as Mobile,nvl(Telephone,' ') as Telephone,nvl(LocationUrl,' ') as LocationUrl,SitePoDate,prodflag from  "
	 * +
	 * " (Select SiteId,SiteCode,TodayEnergyFlag,CustomerReference as SiteReference,SiteName,Latitude,Longitude,Address,City,StateName as State,CountryName as Country,PostalCode,InstallationCapacity,Mobile,Telephone,SiteImage as LocationUrl,SitePoDate,ProdFlag from mSite a,mState b,mCountry c where a.StateID=b.StateId and a.CountryID=c.CountryId and a.ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID + "'))) tbl1 left outer join " +
	 * " (Select a.SiteID,Count(case when a.Status=1 and  b.DismandalFlag=0  then 1 else null end) as ActiveInverterCount,Count(distinct (case when b.DismandalFlag=0 then b.EquipmentID else null end)) as InverterCount,to_char(Max(a.Timestamp),'DD-MM-YYYY HH24:MI') as LastChecked,Sum(nvl(TodayEnergy,0)) as TodayEnergy,Sum(nvl(TotalEnergy,0)) as TotalEnergy,Max(nvl((case when Status=1 then -1 else Status end),0)) as Status,Sum(nvl(YesterdayTotalEnergy,0)) as YesterdayTotalEnergy,to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI') as LastDownTime,to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI') as LastDataReceived,Max(TodayHoursOn) as TodayHoursOn from tDataSource a,mEquipment b where a.SiteID=b.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1  and a.SiteID in (Select SiteId from mSite where ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID +
	 * "'))) and EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID) tbl2 on tbl1.SiteID=tbl2.SiteID order by SiteCode "
	 * ;
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * 
	 * Query="Select SiteId,Sitetype,SiteCode,SiteReference,SiteName,Latitude,Longitude,Address,City,State,Country,PostalCode,ActiveInverterCount,InverterCount,LastChecked,TotalEnergy,TodayEnergy,LastDownTime,LastDataReceived,TodayHoursOn,coalesce((case when Status=-1 then 'Active' else case when Status=2 then 'Warning' else case when Status=3 then 'Down' else 'Offline' end end end),'Offline') as Status,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,ActiveEMCount,EMCount,ProductionOn,Prodflag from(Select coalesce(SiteId,0) as SiteId,coalesce((case when Sitetypeid=1 then 'Rooftop' else case when Sitetypeid=2 then 'Utility' end end),' ') as Sitetype,coalesce(SiteCode,' ') as SiteCode,coalesce(SiteReference,' ') as SiteReference,coalesce(SiteName,' ') as SiteName,coalesce(Latitude,' ') as Latitude,coalesce(Longitude,' ') as Longitude,coalesce(Address,' ') as Address,coalesce(City,' ') as City,coalesce(State,' ')  as State,coalesce(Country,' ') as Country,coalesce(PostalCode,' ') as PostalCode,coalesce(Sum(ActiveInverterCount),0) as ActiveInverterCount,coalesce(Sum(InverterCount),0) as InverterCount,coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked,coalesce(Sum(TotalEnergy),0) as TotalEnergy, coalesce(Sum(TodayEnergy),0)  as TodayEnergy,coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime,coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,coalesce(InstallationCapacity,0) as InstallationCapacity,coalesce(Mobile,' ') as Mobile,coalesce(Telephone,' ') as Telephone,coalesce(LocationUrl,' ') as LocationUrl,coalesce(Cod,' ') as Cod,coalesce(Prodflag,0) as Prodflag,coalesce(Sum(ActiveEMCount),0) as ActiveEMCount,coalesce(Sum(EMCount),0) as EMCount,coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(Select SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Latitude,Longitude,Address,City,State,Country,PostalCode,Sum(ActiveInverterCount) as ActiveInverterCount,Sum(InverterCount) as InverterCount,Sum(ActiveEMCount) as ActiveEMCount,Sum(EMCount) as EMCount,(Max(LastChecked)) as ProductionOn, Max(LastChecked) as LastChecked, Max(LastDataReceived) as LastDataReceived, Max(LastDownTime) as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy,   (case when TodayEnergyFlag=1 then coalesce(Sum(TodayEnergy),0)  else coalesce(Sum(TotalEnergy)-Sum(YesterdayTotalEnergy),0) end) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 ,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,Prodflag from(select e.sitetypeid,e.SiteCode,e.CustomerReference as SiteReference,e.sitename,e.Latitude,e.longitude,e.address,e.city,g.StateName as State,h.CountryName as Country,e.postalcode,Max(coalesce((case when d.Status=1 then -1 else Status end),0)) as Status,Count(case when d.Status=1 and  a.DismandalFlag=0 and c.categorygroup='INV' then 1 else null end) as ActiveInverterCount,Count(distinct (case when a.DismandalFlag=0 and c.categorygroup='INV' then a.EquipmentID else null end)) as InverterCount,Count(case when d.Status=1 and  a.DismandalFlag=0 and c.categorygroup='EM' then 1 else null end) as ActiveEMCount,Count(distinct (case when a.DismandalFlag=0 and c.categorygroup='EM' then a.EquipmentID else null end)) as EMCount,e.InstallationCapacity,e.Mobile,e.Telephone,e.SiteImage as LocationUrl,to_char(e.Cod ,'DD-MM-YYYY') as Cod,e.Prodflag,e.TodayEnergyFlag,c.categorygroup,e.SiteID,Max(Timestamp + (330 * interval '1 minute') ) as LastChecked,Max(LastDataReceived + (330 * interval '1 minute') ) as LastDataReceived,(case when e.equipmentflag=1 and c.categorygroup='EM' then Sum(coalesce(TotalEnergy,0)) else  case when e.equipmentflag=0 and c.categorygroup='INV' then Sum(coalesce(TotalEnergy,0)) else 0 end end) as TotalEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(TodayEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(TotalEnergy,0))) else 0 end end) as TodayEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(YesterdayTotalEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(YesterdayTotalEnergy,0))) else 0 end end) as YesterdayTotalEnergy,Max(LastDownTime + (330 * interval '1 minute') ) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from msite e left outer join mState g on e.StateID=g.StateId  left outer join mCountry h on e.CountryID=h.CountryID left outer join tdatasource d on e.siteid=d.siteid  and d.activeflag=1 left outer join mequipment a on d.equipmentid=a.equipmentid  and a.activeflag=1 left outer join   mequipmenttype b on a.equipmenttypeid=b.equipmenttypeid left outer join mequipmentcategory c on c.categoryid=b.categoryid and c.categorygroup in('INV','EM')where  e.activeflag=1 and e.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID +
	 * "') or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID
	 * +"')) group by d.siteid,e.siteid,e.sitetypeid,e.sitename,e.sitecode,e.customerreference,e.address,e.Latitude,e.longitude,e.city,g.StateName,h.CountryName,e.PostalCode,d.Status,a.DismandalFlag,a.EquipmentID,e.TodayEnergyFlag,e.equipmentflag,e.todayenergyflag,c.categorygroup,e.InstallationCapacity,e.Mobile,e.Telephone,e.SiteImage,e.Cod,e.Prodflag) as tbl group by SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Latitude,longitude,address,city,State,Country,Postalcode,todayenergyflag,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,Prodflag) tbl2 group by SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Address,city,Latitude,longitude,State,Country,postalcode,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,Prodflag) tbl3 order by status"
	 * ;
	 * 
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset +
	 * " * interval '1 minute') ";
	 * 
	 * Query="Select SiteId,Sitetype,SiteCode,SiteReference,SiteName,Latitude,Longitude,Address,City,State,Country,PostalCode,ActiveInverterCount,InverterCount,LastChecked,TotalEnergy,TodayEnergy,LastDownTime,LastDataReceived,TodayHoursOn,coalesce((case when Status=-1 then 'Active' else case when Status=2 then 'Warning' else case when Status=3 then 'Down' else 'Offline' end end end),'Offline') as Status,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,ActiveEMCount,EMCount,ProductionOn,Prodflag from(Select coalesce(SiteId,0) as SiteId,coalesce((case when Sitetypeid=1 then 'Rooftop' else case when Sitetypeid=2 then 'Utility' end end),' ') as Sitetype,coalesce(SiteCode,' ') as SiteCode,coalesce(SiteReference,' ') as SiteReference,coalesce(SiteName,' ') as SiteName,coalesce(Latitude,' ') as Latitude,coalesce(Longitude,' ') as Longitude,coalesce(Address,' ') as Address,coalesce(City,' ') as City,coalesce(State,' ')  as State,coalesce(Country,' ') as Country,coalesce(PostalCode,' ') as PostalCode,coalesce(Sum(ActiveInverterCount),0) as ActiveInverterCount,coalesce(Sum(InverterCount),0) as InverterCount,coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked,coalesce(Sum(TotalEnergy),0) as TotalEnergy, coalesce(Sum(TodayEnergy),0)  as TodayEnergy,coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime,coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,coalesce(InstallationCapacity,0) as InstallationCapacity,coalesce(Mobile,' ') as Mobile,coalesce(Telephone,' ') as Telephone,coalesce(LocationUrl,' ') as LocationUrl,coalesce(Cod,' ') as Cod,coalesce(Prodflag,0) as Prodflag,coalesce(Sum(ActiveEMCount),0) as ActiveEMCount,coalesce(Sum(EMCount),0) as EMCount,coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(Select SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Latitude,Longitude,Address,City,State,Country,PostalCode,Sum(ActiveInverterCount) as ActiveInverterCount,Sum(InverterCount) as InverterCount,Sum(ActiveEMCount) as ActiveEMCount,Sum(EMCount) as EMCount,(Max(LastChecked)) as ProductionOn, Max(LastChecked) as LastChecked, Max(LastDataReceived) as LastDataReceived, Max(LastDownTime) as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy,   (case when TodayEnergyFlag=1 then coalesce(Sum(TodayEnergy),0)  else coalesce(Sum(TotalEnergy)-Sum(YesterdayTotalEnergy),0) end) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 ,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,Prodflag from(select e.sitetypeid,e.SiteCode,e.CustomerReference as SiteReference,e.sitename,e.Latitude,e.longitude,e.address,e.city,g.StateName as State,h.CountryName as Country,e.postalcode,Max(coalesce((case when d.Status=1 then -1 else Status end),0)) as Status,Count(case when d.Status=1 and  a.DismandalFlag=0 and c.categorygroup='INV' then 1 else null end) as ActiveInverterCount,Count(distinct (case when a.DismandalFlag=0 and c.categorygroup='INV' then a.EquipmentID else null end)) as InverterCount,Count(case when d.Status=1 and  a.DismandalFlag=0 and c.categorygroup='EM' then 1 else null end) as ActiveEMCount,Count(distinct (case when a.DismandalFlag=0 and c.categorygroup='EM' then a.EquipmentID else null end)) as EMCount,e.InstallationCapacity,e.Mobile,e.Telephone,e.SiteImage as LocationUrl,to_char(e.Cod "
	 * + TimeConversionFromUTC +
	 * " ,'DD-MM-YYYY') as Cod,e.Prodflag,e.TodayEnergyFlag,c.categorygroup,e.SiteID,Max(Timestamp "
	 * + TimeConversionFromUTC + " ) as LastChecked,Max(LastDataReceived " +
	 * TimeConversionFromUTC +
	 * " ) as LastDataReceived,(case when e.equipmentflag=1 and c.categorygroup='EM' then Sum(coalesce(TotalEnergy,0)) else  case when e.equipmentflag=0 and c.categorygroup='INV' then Sum(coalesce(TotalEnergy,0)) else 0 end end) as TotalEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(TodayEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(TotalEnergy,0))) else 0 end end) as TodayEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(YesterdayTotalEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(YesterdayTotalEnergy,0))) else 0 end end) as YesterdayTotalEnergy,Max(LastDownTime + (330 * interval '1 minute') ) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from msite e left outer join mState g on e.StateID=g.StateId  left outer join mCountry h on e.CountryID=h.CountryID left outer join tdatasource d on e.siteid=d.siteid  and d.activeflag=1 left outer join mequipment a on d.equipmentid=a.equipmentid  and a.activeflag=1 left outer join   mequipmenttype b on a.equipmenttypeid=b.equipmenttypeid left outer join mequipmentcategory c on c.categoryid=b.categoryid and c.categorygroup in('INV','EM')where  e.activeflag=1 and e.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID +
	 * "') or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + UserID
	 * +"')) group by d.siteid,e.siteid,e.sitetypeid,e.sitename,e.sitecode,e.customerreference,e.address,e.Latitude,e.longitude,e.city,g.StateName,h.CountryName,e.PostalCode,d.Status,a.DismandalFlag,a.EquipmentID,e.TodayEnergyFlag,e.equipmentflag,e.todayenergyflag,c.categorygroup,e.InstallationCapacity,e.Mobile,e.Telephone,e.SiteImage,e.Cod,e.Prodflag) as tbl group by SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Latitude,longitude,address,city,State,Country,Postalcode,todayenergyflag,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,Prodflag) tbl2 group by SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Address,city,Latitude,longitude,State,Country,postalcode,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,Prodflag) tbl3 order by status"
	 * ;
	 * 
	 * 
	 * 
	 * } }
	 * 
	 * System.out.println(Query); List<DashboardSiteListBean> DashboardSiteList =
	 * session.createSQLQuery(Query).list();
	 * 
	 * return DashboardSiteList; }
	 */

	public List<DashboardSiteListBean> getDashboardSiteList(String UserID, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		if (UserID == null) {
			UserID = "0";
		} else if (UserID.equals("")) {
			UserID = "0";
		}

		// Oracle
		String Query = "Select  nvl(tbl1.SiteId,0) as SiteId,nvl(case when Sitetypeid=1 then 'Rooftop' else case when Sitetypeid=2 then 'Utility' end end) as sitetype,nvl(SiteCode,' ') as SiteCode,nvl(SiteReference,' ') as SiteReference,nvl(SiteName,' ') as SiteName,nvl(Latitude,' ') as Latitude,nvl(Longitude,' ') as Longitude,nvl(Address,' ') as Address,nvl(City,' ') as City,nvl(State,' ')  as State,nvl(Country,' ') as Country,nvl(PostalCode,' ') as PostalCode,nvl(ActiveInverterCount,0) as ActiveInverterCount,nvl(InverterCount,0) as InverterCount,nvl(LastChecked,' ') as LastChecked,nvl(TotalEnergy,0) as TotalEnergy,(case when TodayEnergyFlag=1 then nvl(TodayEnergy,0) else (nvl(TotalEnergy,0)-nvl(YesterdayTotalEnergy,0)) end) as TodayEnergy,nvl(LastDownTime,' ' ) as LastDownTime,nvl(LastDataReceived,' ') as LastDataReceived,nvl(TodayHoursOn,0) as TodayHoursOn,nvl((case when Status=-1 then 'Active' else case when Status=2 then 'Warning' else case when Status=3 then 'Down' else 'Offline' end end end),'Offline') as Status,nvl(InstallationCapacity,0) as InstallationCapacity,nvl(Mobile,' ') as Mobile,nvl(Telephone,' ') as Telephone,nvl(LocationUrl,' ') as LocationUrl,SitePoDate,prodflag from  "
				+ " (Select SiteId,SiteCode,TodayEnergyFlag,CustomerReference as SiteReference,SiteName,Latitude,Longitude,Address,City,StateName as State,CountryName as Country,PostalCode,InstallationCapacity,Mobile,Telephone,SiteImage as LocationUrl,SitePoDate,ProdFlag from mSite a,mState b,mCountry c where a.StateID=b.StateId and a.CountryID=c.CountryId and a.ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
				+ UserID + "'))) tbl1 left outer join "
				+ " (Select a.SiteID,Count(case when a.Status=1 and  b.DismandalFlag=0  then 1 else null end) as ActiveInverterCount,Count(distinct (case when b.DismandalFlag=0 then b.EquipmentID else null end)) as InverterCount,to_char(Max(a.Timestamp),'DD-MM-YYYY HH24:MI') as LastChecked,Sum(nvl(TodayEnergy,0)) as TodayEnergy,Sum(nvl(TotalEnergy,0)) as TotalEnergy,Max(nvl((case when Status=1 then -1 else Status end),0)) as Status,Sum(nvl(YesterdayTotalEnergy,0)) as YesterdayTotalEnergy,to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI') as LastDownTime,to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI') as LastDataReceived,Max(TodayHoursOn) as TodayHoursOn from tDataSource a,mEquipment b where a.SiteID=b.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1  and a.SiteID in (Select SiteId from mSite where ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
				+ UserID
				+ "'))) and EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID) tbl2 on tbl1.SiteID=tbl2.SiteID order by SiteCode ";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {

			Query = "Select SiteId,Sitetype,SiteCode,SiteReference,SiteName,Latitude,Longitude,Address,City,State,Country,PostalCode,ActiveInverterCount,InverterCount,LastChecked,TotalEnergy,TodayEnergy,LastDownTime,LastDataReceived,TodayHoursOn,coalesce((case when Status=-1 then 'Active' else case when Status=2 then 'Warning' else case when Status=3 then 'Down' else 'Offline' end end end),'Offline') as Status,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,ActiveEMCount,EMCount,ProductionOn,Prodflag from(Select coalesce(SiteId,0) as SiteId,coalesce((case when Sitetypeid=1 then 'Rooftop' else case when Sitetypeid=2 then 'Utility' end end),' ') as Sitetype,coalesce(SiteCode,' ') as SiteCode,coalesce(SiteReference,' ') as SiteReference,coalesce(SiteName,' ') as SiteName,coalesce(Latitude,' ') as Latitude,coalesce(Longitude,' ') as Longitude,coalesce(Address,' ') as Address,coalesce(City,' ') as City,coalesce(State,' ')  as State,coalesce(Country,' ') as Country,coalesce(PostalCode,' ') as PostalCode,coalesce(Sum(ActiveInverterCount),0) as ActiveInverterCount,coalesce(Sum(InverterCount),0) as InverterCount,coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked,coalesce(Sum(TotalEnergy),0) as TotalEnergy, coalesce(Sum(TodayEnergy),0)  as TodayEnergy,coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime,coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,coalesce(InstallationCapacity,0) as InstallationCapacity,coalesce(Mobile,' ') as Mobile,coalesce(Telephone,' ') as Telephone,coalesce(LocationUrl,' ') as LocationUrl,coalesce(Cod,' ') as Cod,coalesce(Prodflag,0) as Prodflag,coalesce(Sum(ActiveEMCount),0) as ActiveEMCount,coalesce(Sum(EMCount),0) as EMCount,coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(Select SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Latitude,Longitude,Address,City,State,Country,PostalCode,Sum(ActiveInverterCount) as ActiveInverterCount,Sum(InverterCount) as InverterCount,Sum(ActiveEMCount) as ActiveEMCount,Sum(EMCount) as EMCount,(Max(LastChecked)) as ProductionOn, Max(LastChecked) as LastChecked, Max(LastDataReceived) as LastDataReceived, Max(LastDownTime) as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy,   (case when TodayEnergyFlag=1 then coalesce(Sum(TodayEnergy),0)  else coalesce(Sum(TotalEnergy)-Sum(YesterdayTotalEnergy),0) end) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 ,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,Prodflag from(select e.sitetypeid,e.SiteCode,e.CustomerReference as SiteReference,e.sitename,e.Latitude,e.longitude,e.address,e.city,g.StateName as State,h.CountryName as Country,e.postalcode,Max(coalesce((case when d.Status=1 then -1 else Status end),0)) as Status,Count(case when d.Status=1 and  a.DismandalFlag=0 and c.categorygroup='INV' then 1 else null end) as ActiveInverterCount,Count(distinct (case when a.DismandalFlag=0 and c.categorygroup='INV' then a.EquipmentID else null end)) as InverterCount,Count(case when d.Status=1 and  a.DismandalFlag=0 and c.categorygroup='EM' then 1 else null end) as ActiveEMCount,Count(distinct (case when a.DismandalFlag=0 and c.categorygroup='EM' then a.EquipmentID else null end)) as EMCount,e.InstallationCapacity,e.Mobile,e.Telephone,e.SiteImage as LocationUrl,to_char(e.Cod ,'DD-MM-YYYY') as Cod,e.Prodflag,e.TodayEnergyFlag,c.categorygroup,e.SiteID,Max(Timestamp + (330 * interval '1 minute') ) as LastChecked,Max(LastDataReceived + (330 * interval '1 minute') ) as LastDataReceived,(case when e.equipmentflag=1 and c.categorygroup='EM' then Sum(coalesce(TotalEnergy,0)) else  case when e.equipmentflag=0 and c.categorygroup='INV' then Sum(coalesce(TotalEnergy,0)) else 0 end end) as TotalEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(TodayEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(TotalEnergy,0))) else 0 end end) as TodayEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(YesterdayTotalEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(YesterdayTotalEnergy,0))) else 0 end end) as YesterdayTotalEnergy,Max(LastDownTime + (330 * interval '1 minute') ) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from msite e left outer join mState g on e.StateID=g.StateId  left outer join mCountry h on e.CountryID=h.CountryID left outer join tdatasource d on e.siteid=d.siteid  and d.activeflag=1 left outer join mequipment a on d.equipmentid=a.equipmentid  and a.activeflag=1 left outer join   mequipmenttype b on a.equipmenttypeid=b.equipmenttypeid left outer join mequipmentcategory c on c.categoryid=b.categoryid and c.categorygroup in('INV','EM')where  e.activeflag=1 and e.SiteID in (Select SiteId from mSiteMap where UserID='"
					+ UserID
					+ "' and ActiveFlag=1) group by d.siteid,e.siteid,e.sitetypeid,e.sitename,e.sitecode,e.customerreference,e.address,e.Latitude,e.longitude,e.city,g.StateName,h.CountryName,e.PostalCode,d.Status,a.DismandalFlag,a.EquipmentID,e.TodayEnergyFlag,e.equipmentflag,e.todayenergyflag,c.categorygroup,e.InstallationCapacity,e.Mobile,e.Telephone,e.SiteImage,e.Cod,e.Prodflag) as tbl group by SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Latitude,longitude,address,city,State,Country,Postalcode,todayenergyflag,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,Prodflag) tbl2 group by SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Address,city,Latitude,longitude,State,Country,postalcode,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,Prodflag) tbl3 order by status";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "Select SiteId,Sitetype,SiteCode,SiteReference,SiteName,Latitude,Longitude,Address,City,State,Country,PostalCode,ActiveInverterCount,InverterCount,LastChecked,TotalEnergy,TodayEnergy,LastDownTime,LastDataReceived,TodayHoursOn,coalesce((case when Status=-1 then 'Active' else case when Status=2 then 'Warning' else case when Status=3 then 'Down' else 'Offline' end end end),'Offline') as Status,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,ActiveEMCount,EMCount,ProductionOn,Prodflag from(Select coalesce(SiteId,0) as SiteId,coalesce((case when Sitetypeid=1 then 'Rooftop' else case when Sitetypeid=2 then 'Utility' end end),' ') as Sitetype,coalesce(SiteCode,' ') as SiteCode,coalesce(SiteReference,' ') as SiteReference,coalesce(SiteName,' ') as SiteName,coalesce(Latitude,' ') as Latitude,coalesce(Longitude,' ') as Longitude,coalesce(Address,' ') as Address,coalesce(City,' ') as City,coalesce(State,' ')  as State,coalesce(Country,' ') as Country,coalesce(PostalCode,' ') as PostalCode,coalesce(Sum(ActiveInverterCount),0) as ActiveInverterCount,coalesce(Sum(InverterCount),0) as InverterCount,coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked,coalesce(Sum(TotalEnergy),0) as TotalEnergy, coalesce(Sum(TodayEnergy),0)  as TodayEnergy,coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime,coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,coalesce(InstallationCapacity,0) as InstallationCapacity,coalesce(Mobile,' ') as Mobile,coalesce(Telephone,' ') as Telephone,coalesce(LocationUrl,' ') as LocationUrl,coalesce(Cod,' ') as Cod,coalesce(Prodflag,0) as Prodflag,coalesce(Sum(ActiveEMCount),0) as ActiveEMCount,coalesce(Sum(EMCount),0) as EMCount,coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(Select SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Latitude,Longitude,Address,City,State,Country,PostalCode,Sum(ActiveInverterCount) as ActiveInverterCount,Sum(InverterCount) as InverterCount,Sum(ActiveEMCount) as ActiveEMCount,Sum(EMCount) as EMCount,(Max(LastChecked)) as ProductionOn, Max(LastChecked) as LastChecked, Max(LastDataReceived) as LastDataReceived, Max(LastDownTime) as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy,   (case when TodayEnergyFlag=1 then coalesce(Sum(TodayEnergy),0)  else coalesce(Sum(TotalEnergy)-Sum(YesterdayTotalEnergy),0) end) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 ,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,Prodflag from(select e.sitetypeid,e.SiteCode,e.CustomerReference as SiteReference,e.sitename,e.Latitude,e.longitude,e.address,e.city,g.StateName as State,h.CountryName as Country,e.postalcode,Max(coalesce((case when d.Status=1 then -1 else Status end),0)) as Status,Count(case when d.Status=1 and  a.DismandalFlag=0 and c.categorygroup='INV' then 1 else null end) as ActiveInverterCount,Count(distinct (case when a.DismandalFlag=0 and c.categorygroup='INV' then a.EquipmentID else null end)) as InverterCount,Count(case when d.Status=1 and  a.DismandalFlag=0 and c.categorygroup='EM' then 1 else null end) as ActiveEMCount,Count(distinct (case when a.DismandalFlag=0 and c.categorygroup='EM' then a.EquipmentID else null end)) as EMCount,e.InstallationCapacity,e.Mobile,e.Telephone,e.SiteImage as LocationUrl,to_char(e.Cod "
						+ TimeConversionFromUTC
						+ " ,'DD-MM-YYYY') as Cod,e.Prodflag,e.TodayEnergyFlag,c.categorygroup,e.SiteID,Max(Timestamp "
						+ TimeConversionFromUTC + " ) as LastChecked,Max(LastDataReceived " + TimeConversionFromUTC
						+ " ) as LastDataReceived,(case when e.equipmentflag=1 and c.categorygroup='EM' then Sum(coalesce(TotalEnergy,0)) else  case when e.equipmentflag=0 and c.categorygroup='INV' then Sum(coalesce(TotalEnergy,0)) else 0 end end) as TotalEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(TodayEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(TotalEnergy,0))) else 0 end end) as TodayEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(YesterdayTotalEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(YesterdayTotalEnergy,0))) else 0 end end) as YesterdayTotalEnergy,Max(LastDownTime + (330 * interval '1 minute') ) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from msite e left outer join mState g on e.StateID=g.StateId  left outer join mCountry h on e.CountryID=h.CountryID left outer join tdatasource d on e.siteid=d.siteid  and d.activeflag=1 left outer join mequipment a on d.equipmentid=a.equipmentid  and a.activeflag=1 left outer join   mequipmenttype b on a.equipmenttypeid=b.equipmenttypeid left outer join mequipmentcategory c on c.categoryid=b.categoryid and c.categorygroup in('INV','EM')where  e.activeflag=1 and e.SiteID in (Select SiteId from mSiteMap where UserID='"
						+ UserID
						+ "' and ActiveFlag=1) group by d.siteid,e.siteid,e.sitetypeid,e.sitename,e.sitecode,e.customerreference,e.address,e.Latitude,e.longitude,e.city,g.StateName,h.CountryName,e.PostalCode,d.Status,a.DismandalFlag,a.EquipmentID,e.TodayEnergyFlag,e.equipmentflag,e.todayenergyflag,c.categorygroup,e.InstallationCapacity,e.Mobile,e.Telephone,e.SiteImage,e.Cod,e.Prodflag) as tbl group by SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Latitude,longitude,address,city,State,Country,Postalcode,todayenergyflag,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,Prodflag) tbl2 group by SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Address,city,Latitude,longitude,State,Country,postalcode,InstallationCapacity,Mobile,Telephone,LocationUrl,Cod,Prodflag) tbl3 order by status";

			}
		}

		System.out.println(Query);
		List<DashboardSiteListBean> DashboardSiteList = session.createSQLQuery(Query).list();

		return DashboardSiteList;
	}

	public List<DashboardSiteBean> getDashboardSiteDetailsByCustomer(String CustomerID, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		if (CustomerID == null) {
			CustomerID = "0";
		} else if (CustomerID.equals("")) {
			CustomerID = "0";
		}

		// Oracle
		String Query = "Select nvl(Count(a.SiteID),0) as TotalSites, "
				+ " nvl(Sum(case when SiteTypeID=1 then 1 else null end),0) as RoofTopSites, "
				+ " nvl(Sum(case when SiteTypeID=2 then 1 else null end),0) as UtilitySites, "
				+ " nvl(Sum(case when SiteStatus=0 then 1 else null end),0) as OfflineStatus, "
				+ " nvl(Sum(case when SiteStatus=1 then 1 else null end),0) as ActiveStatus, "
				+ " nvl(Sum(case when SiteStatus=2 then 1 else null end),0) as WarningStatus, "
				+ " nvl(Sum(case when SiteStatus=3 then 1 else null end),0) as DownStatus "
				+ " from mSite a,tSiteStatus b where a.SiteID=b.SiteID and a.ActiveFlag=1 and b.ActiveFlag=1 and a.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID ='"
				+ CustomerID + "')";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select coalesce(Count(a.SiteID),0) as TotalSites, "
					+ " coalesce(Sum(case when SiteTypeID='1' then 1 else null end),0) as RoofTopSites, "
					+ " coalesce(Sum(case when SiteTypeID='2' then 1 else null end),0) as UtilitySites, "
					+ " coalesce(Sum(case when SiteStatus='0' then 1 else null end),0) as OfflineStatus, "
					+ " coalesce(Sum(case when SiteStatus='1' then 1 else null end),0) as ActiveStatus, "
					+ " coalesce(Sum(case when SiteStatus='2' then 1 else null end),0) as WarningStatus, "
					+ " coalesce(Sum(case when SiteStatus='3' then 1 else null end),0) as DownStatus "
					+ " from mSite as a,tSiteStatus as b where a.SiteID=b.SiteID and a.ActiveFlag=1 and b.ActiveFlag=1 and a.CustomerID ='"
					+ CustomerID + "'";

		}

		System.out.println(Query);
		List<DashboardSiteBean> DashboardSiteDetailsList = session.createSQLQuery(Query).list();

		return DashboardSiteDetailsList;
	}

	// Only For Inverter
	/*
	 * public List<DashboardEnergySummaryBean>
	 * getDashboardEnergySummaryDetailsByCustomer(String CustomerID, String
	 * TimezoneOffset) { Session session = sessionFactory.getCurrentSession();
	 * 
	 * if(CustomerID == null) { CustomerID = "0";} else if(CustomerID.equals("")) {
	 * CustomerID = "0";}
	 * 
	 * //Oracle String Query =
	 * "Select  nvl(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, nvl(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked, nvl(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, nvl(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime, nvl(Sum(TotalEnergy),0) as TotalEnergy, "
	 * +
	 * "  nvl(Sum(TodayEnergy),0) as TodayEnergy, nvl(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 "
	 * +
	 * " from (Select a.SiteID,Max(a.Timestamp) as LastChecked,Max(LastDataReceived) as LastDataReceived,Sum(nvl(TotalEnergy,0)) as TotalEnergy, "
	 * +
	 * " (case when c.TodayEnergyFlag=1 then Sum(nvl(TodayEnergy,0)) else (Sum(nvl(TotalEnergy,0)) - Sum(nvl(YesterdayTotalEnergy,0))) end ) as TodayEnergy, "
	 * +
	 * " Max(LastDownTime) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from tDataSource a,mEquipment b, mSite c "
	 * +
	 * " where a.SiteID=b.SiteID and a.SiteID=c.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1 and c.ActiveFlag=1 and "
	 * +
	 * " c.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and "
	 * + " CustomerID ='" + CustomerID + "') and " +
	 * " EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where " +
	 * " CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID,c.TodayEnergyFlag)"
	 * ;
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * Query =
	 * "Select  coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked, coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy, "
	 * +
	 * "  coalesce(Sum(TodayEnergy),0) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 "
	 * +
	 * " from (Select a.SiteID,Max(a.Timestamp) as LastChecked,Max(LastDataReceived) as LastDataReceived,Sum(coalesce(TotalEnergy,0)) as TotalEnergy, "
	 * +
	 * " (case when c.TodayEnergyFlag=1 then Sum(coalesce(TodayEnergy,0)) else (Sum(coalesce(TotalEnergy,0)) - Sum(coalesce(YesterdayTotalEnergy,0))) end ) as TodayEnergy, "
	 * +
	 * " Max(LastDownTime) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from tDataSource as a,mEquipment as b, mSite as c "
	 * +
	 * " where a.SiteID=b.SiteID and a.SiteID=c.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1 and c.ActiveFlag=1 and "
	 * +
	 * " c.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and "
	 * + " CustomerID ='" + CustomerID + "') and " +
	 * " EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where " +
	 * " CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID,c.TodayEnergyFlag) as tbl"
	 * ;
	 * 
	 * 
	 * 
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset +
	 * " * interval '1 minute') ";
	 * 
	 * Query =
	 * "Select  coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked, coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy, "
	 * +
	 * "  coalesce(Sum(TodayEnergy),0) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 "
	 * + " from (Select a.SiteID,Max(a.Timestamp" + TimeConversionFromUTC +
	 * ") as LastChecked,Max(LastDataReceived" + TimeConversionFromUTC +
	 * ") as LastDataReceived,Sum(coalesce(TotalEnergy,0)) as TotalEnergy, " +
	 * " (case when c.TodayEnergyFlag=1 then Sum(coalesce(TodayEnergy,0)) else (Sum(coalesce(TotalEnergy,0)) - Sum(coalesce(YesterdayTotalEnergy,0))) end ) as TodayEnergy, "
	 * + " Max(LastDownTime" + TimeConversionFromUTC +
	 * ") as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from tDataSource as a,mEquipment as b, mSite as c "
	 * +
	 * " where a.SiteID=b.SiteID and a.SiteID=c.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1 and c.ActiveFlag=1 and "
	 * +
	 * " c.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and "
	 * + " CustomerID ='" + CustomerID + "') and " +
	 * " EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where " +
	 * " CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID,c.TodayEnergyFlag) as tbl"
	 * ;
	 * 
	 * 
	 * 
	 * } }
	 * 
	 * System.out.println(Query); List<DashboardEnergySummaryBean>
	 * DashboardEnergySummaryDetailsList = session.createSQLQuery(Query).list();
	 * 
	 * return DashboardEnergySummaryDetailsList; }
	 */

	// Both Inveretr and Energy Meter
	public List<DashboardEnergySummaryBean> getDashboardEnergySummaryDetailsByCustomer(String CustomerID,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		if (CustomerID == null) {
			CustomerID = "0";
		} else if (CustomerID.equals("")) {
			CustomerID = "0";
		}

		// Oracle
		String Query = "Select  nvl(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, nvl(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked, nvl(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, nvl(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime, nvl(Sum(TotalEnergy),0) as TotalEnergy, "
				+ "  nvl(Sum(TodayEnergy),0) as TodayEnergy, nvl(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 "
				+ " from (Select a.SiteID,Max(a.Timestamp) as LastChecked,Max(LastDataReceived) as LastDataReceived,Sum(nvl(TotalEnergy,0)) as TotalEnergy, "
				+ " (case when c.TodayEnergyFlag=1 then Sum(nvl(TodayEnergy,0)) else (Sum(nvl(TotalEnergy,0)) - Sum(nvl(YesterdayTotalEnergy,0))) end ) as TodayEnergy, "
				+ " Max(LastDownTime) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from tDataSource a,mEquipment b, mSite c "
				+ " where a.SiteID=b.SiteID and a.SiteID=c.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1 and c.ActiveFlag=1 and "
				+ " c.CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and " + " CustomerID ='"
				+ CustomerID + "') and " + " EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where "
				+ " CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID,c.TodayEnergyFlag)";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked, coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy, coalesce(Sum(TodayEnergy),0)  as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(Select (Max(LastChecked)) as ProductionOn, Max(LastChecked) as LastChecked, Max(LastDataReceived) as LastDataReceived, Max(LastDownTime) as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy,   (case when TodayEnergyFlag=1 then coalesce(Sum(TodayEnergy),0)  else coalesce(Sum(TotalEnergy)-Sum(YesterdayTotalEnergy),0) end) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(select e.TodayEnergyFlag,c.categorygroup,d.SiteID,Max(Timestamp + (330 * interval '1 minute') ) as LastChecked,Max(LastDataReceived + (330 * interval '1 minute') ) as LastDataReceived,(case when e.equipmentflag=1 and c.categorygroup='EM' then Sum(coalesce(TotalEnergy,0)) else  case when e.equipmentflag=0 and c.categorygroup='INV' then Sum(coalesce(TotalEnergy,0)) else 0 end end) as TotalEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(TodayEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(TotalEnergy,0))) else 0 end end) as TodayEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(YesterdayTotalEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(YesterdayTotalEnergy,0))) else 0 end end) as YesterdayTotalEnergy,Max(LastDownTime + (330 * interval '1 minute') ) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from tdatasource d left outer join mequipment a on d.equipmentid=a.equipmentid left outer join msite e on  e.siteid=a.siteid left outer join mequipmenttype b on a.equipmenttypeid=b.equipmenttypeid left outer join mequipmentcategory c on c.categoryid=b.categoryid where d.siteid=a.siteid and d.siteid=e.siteid and d.activeflag=1 and e.activeflag=1 and a.activeflag=1 and e.SiteID in (Select SiteID from msitemap where ActiveFlag=1 and CustomerID ='"
					+ CustomerID
					+ "') and c.categorygroup in('INV','EM') group by d.siteid,e.TodayEnergyFlag,e.equipmentflag,e.todayenergyflag,c.categorygroup ) as tbl group by todayenergyflag) tbl2";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "Select coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked, coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy, coalesce(Sum(TodayEnergy),0)  as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(Select (Max(LastChecked)) as ProductionOn, Max(LastChecked) as LastChecked, Max(LastDataReceived) as LastDataReceived, Max(LastDownTime) as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy,   (case when TodayEnergyFlag=1 then coalesce(Sum(TodayEnergy),0)  else coalesce(Sum(TotalEnergy)-Sum(YesterdayTotalEnergy),0) end) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(select e.TodayEnergyFlag,c.categorygroup,d.SiteID,Max(Timestamp "
						+ TimeConversionFromUTC + " ) as LastChecked,Max(LastDataReceived " + TimeConversionFromUTC
						+ " ) as LastDataReceived,(case when e.equipmentflag=1 and c.categorygroup='EM' then Sum(coalesce(TotalEnergy,0)) else  case when e.equipmentflag=0 and c.categorygroup='INV' then Sum(coalesce(TotalEnergy,0)) else 0 end end) as TotalEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(TodayEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(TotalEnergy,0))) else 0 end end) as TodayEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(YesterdayTotalEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(YesterdayTotalEnergy,0))) else 0 end end) as YesterdayTotalEnergy,Max(LastDownTime "
						+ TimeConversionFromUTC
						+ " ) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from tdatasource d left outer join mequipment a on d.equipmentid=a.equipmentid left outer join msite e on  e.siteid=a.siteid left outer join mequipmenttype b on a.equipmenttypeid=b.equipmenttypeid left outer join mequipmentcategory c on c.categoryid=b.categoryid where d.siteid=a.siteid and d.siteid=e.siteid and d.activeflag=1 and e.activeflag=1 and a.activeflag=1 and e.SiteID in (Select SiteID from msitemap where ActiveFlag=1 and CustomerID ='"
						+ CustomerID
						+ "') and c.categorygroup in('INV','EM') group by d.siteid,e.TodayEnergyFlag,e.equipmentflag,e.todayenergyflag,c.categorygroup ) as tbl group by todayenergyflag) tbl2";

			}
		}

		System.out.println(Query);
		List<DashboardEnergySummaryBean> DashboardEnergySummaryDetailsList = session.createSQLQuery(Query).list();

		return DashboardEnergySummaryDetailsList;
	}

	public List<DashboardOandMBean> getDashboardEventAndTicketDetailsByCustomer(String CustomerID,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		if (CustomerID == null) {
			CustomerID = "0";
		} else if (CustomerID.equals("")) {
			CustomerID = "0";
		}

		// Oracle
		String Query = "Select TotalTicket,OpenTicket,ClosedTicket,TotalEvent,TodayEvent from "
				+ " (Select 'OM' as OM, nvl(Count(TicketID),0) as TotalTicket, "
				+ " nvl(Sum(case when State=1 then 1 else null end),0) as OpenTicket, "
				+ " nvl(Sum(case when State=2 then 1 else null end),0) as ClosedTicket "
				+ " from tTicketDetail where ActiveFlag=1  and SiteID in (select SiteId from msitemap where UserID='"
				+ CustomerID + "')) as a, " + " (Select 'OM' as OM, nvl(Count(TransactionId),0) as TotalEvent, "
				+ " nvl(Sum(case when Trunc(EventTimestamp)=Trunc(sysdate) then 1 else null end),0) as TodayEvent "
				+ " from tEventDetail where ActiveFlag=1  and SiteID in (Select SiteId from mSite where ActiveFlag=1  and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID ='"
				+ CustomerID + "'))) b where a.OM=b.OM ";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select TotalTicket,OpenTicket,ClosedTicket,TotalEvent,TodayEvent from "
					+ " (Select character varying(2) 'OM' as OM, coalesce(Count(TicketID),0) as TotalTicket, "
					+ " coalesce(Sum(case when State=1 then 1 else null end),0) as OpenTicket, "
					+ " coalesce(Sum(case when State=2 then 1 else null end),0) as ClosedTicket "
					+ " from tTicketDetail where ActiveFlag=1  and SiteID in (select SiteId from msitemap where customerid='"
					+ CustomerID + "')) as a, "
					+ " (Select character varying(2) 'OM' as OM, coalesce(Count(TransactionId),0) as TotalEvent, "
					+ " coalesce(Sum(case when DATE_TRUNC('day',EventTimestamp)=DATE_TRUNC('day', now()) then 1 else null end),0) as TodayEvent "
					+ " from tEventDetail where ActiveFlag=1  and SiteID in (Select SiteId from msitemap where ActiveFlag=1 and UserID ='"
					+ CustomerID + "')) as b where a.OM=b.OM ";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "Select TotalTicket,OpenTicket,ClosedTicket,TotalEvent,TodayEvent from "
						+ " (Select character varying(2) 'OM' as OM, coalesce(Count(TicketID),0) as TotalTicket, "
						+ " coalesce(Sum(case when State=1 then 1 else null end),0) as OpenTicket, "
						+ " coalesce(Sum(case when State=2 then 1 else null end),0) as ClosedTicket "
						+ " from tTicketDetail where ActiveFlag=1  and SiteID in (select siteid from msitemap where ActiveFlag=1 and customerid ='"
						+ CustomerID + "')) as a, "
						+ " (Select character varying(2) 'OM' as OM, coalesce(Count(TransactionId),0) as TotalEvent, "
						+ " coalesce(Sum(case when DATE_TRUNC('day',EventTimestamp" + TimeConversionFromUTC
						+ ")=DATE_TRUNC('day', now()" + TimeConversionFromUTC
						+ ") then 1 else null end),0) as TodayEvent "
						+ " from tEventDetail where ActiveFlag=1  and SiteID in (Select SiteId from msitemap where ActiveFlag=1 and UserID ='"
						+ CustomerID + "')) as b where a.OM=b.OM ";

			}
		}

		System.out.println(Query);
		List<DashboardOandMBean> DashboardEventAndTicketDetailsList = session.createSQLQuery(Query).list();

		return DashboardEventAndTicketDetailsList;
	}

	// Only For Inverter
	/*
	 * public List<DashboardSiteListBean> getDashboardSiteListByCustomer(String
	 * CustomerID, String TimezoneOffset) { Session session =
	 * sessionFactory.getCurrentSession();
	 * 
	 * if(CustomerID == null) { CustomerID = "0";} else if(CustomerID.equals("")) {
	 * CustomerID = "0";}
	 * 
	 * 
	 * //Oracle String Query =
	 * "Select  nvl(tbl1.SiteId,0) as SiteId,nvl(SiteCode,' ') as SiteCode,nvl(SiteReference,' ') as SiteReference,nvl(SiteName,' ') as SiteName,nvl(Latitude,' ') as Latitude,nvl(Longitude,' ') as Longitude,nvl(Address,' ') as Address,nvl(City,' ') as City,nvl(State,' ')  as State,nvl(Country,' ') as Country,nvl(PostalCode,' ') as PostalCode,nvl(ActiveInverterCount,0) as ActiveInverterCount,nvl(InverterCount,0) as InverterCount,nvl(LastChecked,' ') as LastChecked,nvl(TotalEnergy,0) as TotalEnergy,(case when TodayEnergyFlag=1 then nvl(TodayEnergy,0) else (nvl(TotalEnergy,0)-nvl(YesterdayTotalEnergy,0)) end) as TodayEnergy,nvl(LastDownTime,' ' ) as LastDownTime,nvl(LastDataReceived,' ') as LastDataReceived,nvl(TodayHoursOn,0) as TodayHoursOn,nvl((case when Status=-1 then 'Active' else case when Status=2 then 'Warning' else case when Status=3 then 'Down' else 'Offline' end end end),'Offline') as Status,nvl(InstallationCapacity,0) as InstallationCapacity,nvl(Mobile,' ') as Mobile,nvl(Telephone,' ') as Telephone,nvl(LocationUrl,' ') as LocationUrl from  "
	 * +
	 * " (Select SiteId,SiteCode,TodayEnergyFlag,CustomerReference as SiteReference,SiteName,Latitude,Longitude,Address,City,StateName as State,CountryName as Country,PostalCode,InstallationCapacity,Mobile,Telephone,SiteImage as LocationUrl from mSite a,mState b,mCountry c where a.StateID=b.StateId and a.CountryID=c.CountryId and a.ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID ='"
	 * + CustomerID + "')) tbl1 left outer join " +
	 * " (Select a.SiteID,Count(case when a.Status=1 and  b.DismandalFlag=0  then 1 else null end) as ActiveInverterCount,Count(distinct (case when b.DismandalFlag=0 then b.EquipmentID else null end)) as InverterCount,to_char(Max(a.Timestamp),'DD-MM-YYYY HH24:MI') as LastChecked,Sum(nvl(TodayEnergy,0)) as TodayEnergy,Sum(nvl(TotalEnergy,0)) as TotalEnergy,Max(nvl((case when Status=1 then -1 else Status end),0)) as Status,Sum(nvl(YesterdayTotalEnergy,0)) as YesterdayTotalEnergy,to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI') as LastDownTime,to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI') as LastDataReceived,Max(TodayHoursOn) as TodayHoursOn from tDataSource a,mEquipment b where a.SiteID=b.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1 and a.SiteID in (Select SiteId from mSite where ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID ='"
	 * + CustomerID +
	 * "')) and EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID) tbl2 on tbl1.SiteID=tbl2.SiteID order by SiteCode "
	 * ;
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * Query =
	 * "Select  coalesce(tbl1.SiteId,0) as SiteId,coalesce((case when tbl1.Sitetypeid=1 then 'Rooftop' else case when tbl1.Sitetypeid=2 then 'Utility' end end),' ') as Sitetype,coalesce(SiteCode,' ') as SiteCode,coalesce(SiteReference,' ') as SiteReference,coalesce(SiteName,' ') as SiteName,coalesce(Latitude,' ') as Latitude,coalesce(Longitude,' ') as Longitude,coalesce(Address,' ') as Address,coalesce(City,' ') as City,coalesce(State,' ')  as State,coalesce(Country,' ') as Country,coalesce(PostalCode,' ') as PostalCode,coalesce(ActiveInverterCount,0) as ActiveInverterCount,coalesce(InverterCount,0) as InverterCount,coalesce(LastChecked,' ') as LastChecked,coalesce(TotalEnergy,0) as TotalEnergy,(case when TodayEnergyFlag=1 then coalesce(TodayEnergy,0) else (coalesce(TotalEnergy,0)-coalesce(YesterdayTotalEnergy,0)) end) as TodayEnergy,coalesce(LastDownTime,' ' ) as LastDownTime,coalesce(LastDataReceived,' ') as LastDataReceived,coalesce(TodayHoursOn,0) as TodayHoursOn,coalesce((case when Status=-1 then 'Active' else case when Status=2 then 'Warning' else case when Status=3 then 'Down' else 'Offline' end end end),'Offline') as Status,coalesce(InstallationCapacity,0) as InstallationCapacity,coalesce(Mobile,' ') as Mobile,coalesce(Telephone,' ') as Telephone,coalesce(LocationUrl,' ') as LocationUrl,coalesce(SitePoDate,' ') as SitePoDate from  "
	 * +
	 * " (Select SiteId,Sitetypeid,SiteCode,TodayEnergyFlag,CustomerReference as SiteReference,SiteName,Latitude,Longitude,Address,City,StateName as State,CountryName as Country,PostalCode,InstallationCapacity,Mobile,Telephone,SiteImage as LocationUrl,to_char(SitePoDate + (330 * interval '1 minute') ,'DD-MM-YYYY HH24:MI') as SitePoDate from mSite as a,mState as b,mCountry as c where a.StateID=b.StateId and a.CountryID=c.CountryId and a.ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID ='"
	 * + CustomerID + "')) tbl1 left outer join " +
	 * " (Select a.SiteID,Count(case when a.Status=1 and  b.DismandalFlag=0  then 1 else null end) as ActiveInverterCount,Count(distinct (case when b.DismandalFlag=0 then b.EquipmentID else null end)) as InverterCount,to_char(Max(a.Timestamp),'DD-MM-YYYY HH24:MI') as LastChecked,Sum(coalesce(TodayEnergy,0)) as TodayEnergy,Sum(coalesce(TotalEnergy,0)) as TotalEnergy,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,Sum(coalesce(YesterdayTotalEnergy,0)) as YesterdayTotalEnergy,to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI') as LastDownTime,to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI') as LastDataReceived,Max(TodayHoursOn) as TodayHoursOn from tDataSource as a,mEquipment as b where a.SiteID=b.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1 and a.SiteID in (Select SiteId from mSite where ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID ='"
	 * + CustomerID +
	 * "')) and EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID) as tbl2 on tbl1.SiteID=tbl2.SiteID order by SiteCode "
	 * ;
	 * 
	 * 
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset +
	 * " * interval '1 minute') ";
	 * 
	 * 
	 * Query =
	 * "Select  coalesce(tbl1.SiteId,0) as SiteId,coalesce((case when tbl1.Sitetypeid=1 then 'Rooftop' else case when tbl1.Sitetypeid=2 then 'Utility' end end),' ') as Sitetype,coalesce(SiteCode,' ') as SiteCode,coalesce(SiteReference,' ') as SiteReference,coalesce(SiteName,' ') as SiteName,coalesce(Latitude,' ') as Latitude,coalesce(Longitude,' ') as Longitude,coalesce(Address,' ') as Address,coalesce(City,' ') as City,coalesce(State,' ')  as State,coalesce(Country,' ') as Country,coalesce(PostalCode,' ') as PostalCode,coalesce(ActiveInverterCount,0) as ActiveInverterCount,coalesce(InverterCount,0) as InverterCount,coalesce(LastChecked,' ') as LastChecked,coalesce(TotalEnergy,0) as TotalEnergy,(case when TodayEnergyFlag=1 then coalesce(TodayEnergy,0) else (coalesce(TotalEnergy,0)-coalesce(YesterdayTotalEnergy,0)) end) as TodayEnergy,coalesce(LastDownTime,' ' ) as LastDownTime,coalesce(LastDataReceived,' ') as LastDataReceived,coalesce(TodayHoursOn,0) as TodayHoursOn,coalesce((case when Status=-1 then 'Active' else case when Status=2 then 'Warning' else case when Status=3 then 'Down' else 'Offline' end end end),'Offline') as Status,coalesce(InstallationCapacity,0) as InstallationCapacity,coalesce(Mobile,' ') as Mobile,coalesce(Telephone,' ') as Telephone,coalesce(LocationUrl,' ') as LocationUrl,coalesce(SitePoDate,' ') as SitePoDate from  "
	 * +
	 * " (Select SiteId,Sitetypeid,SiteCode,TodayEnergyFlag,CustomerReference as SiteReference,SiteName,Latitude,Longitude,Address,City,StateName as State,CountryName as Country,PostalCode,InstallationCapacity,Mobile,Telephone,SiteImage as LocationUrl,to_char(SitePoDate + (330 * interval '1 minute') ,'DD-MM-YYYY HH24:MI') as SitePoDate from mSite as a,mState as b,mCountry as c where a.StateID=b.StateId and a.CountryID=c.CountryId and a.ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID ='"
	 * + CustomerID + "')) tbl1 left outer join " +
	 * " (Select a.SiteID,Count(case when a.Status=1 and  b.DismandalFlag=0  then 1 else null end) as ActiveInverterCount,Count(distinct (case when b.DismandalFlag=0 then b.EquipmentID else null end)) as InverterCount,to_char(Max(a.Timestamp"
	 * + TimeConversionFromUTC +
	 * "),'DD-MM-YYYY HH24:MI') as LastChecked,Sum(coalesce(TodayEnergy,0)) as TodayEnergy,Sum(coalesce(TotalEnergy,0)) as TotalEnergy,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,Sum(coalesce(YesterdayTotalEnergy,0)) as YesterdayTotalEnergy,to_char(Max(LastDownTime"
	 * + TimeConversionFromUTC +
	 * "),'DD-MM-YYYY HH24:MI') as LastDownTime,to_char(Max(LastDataReceived" +
	 * TimeConversionFromUTC +
	 * "),'DD-MM-YYYY HH24:MI') as LastDataReceived,Max(TodayHoursOn) as TodayHoursOn from tDataSource as a,mEquipment as b where a.SiteID=b.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1 and a.SiteID in (Select SiteId from mSite where ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID ='"
	 * + CustomerID +
	 * "')) and EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID) as tbl2 on tbl1.SiteID=tbl2.SiteID order by SiteCode "
	 * ;
	 * 
	 * 
	 * 
	 * } } System.out.println(Query); List<DashboardSiteListBean> DashboardSiteList
	 * = session.createSQLQuery(Query).list();
	 * 
	 * return DashboardSiteList; }
	 */

	// Both Inv and Energy Meter
	public List<DashboardSiteListBean> getDashboardSiteListByCustomer(String CustomerID, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		if (CustomerID == null) {
			CustomerID = "0";
		} else if (CustomerID.equals("")) {
			CustomerID = "0";
		}

		// Oracle
		String Query = "Select  nvl(tbl1.SiteId,0) as SiteId,nvl(SiteCode,' ') as SiteCode,nvl(SiteReference,' ') as SiteReference,nvl(SiteName,' ') as SiteName,nvl(Latitude,' ') as Latitude,nvl(Longitude,' ') as Longitude,nvl(Address,' ') as Address,nvl(City,' ') as City,nvl(State,' ')  as State,nvl(Country,' ') as Country,nvl(PostalCode,' ') as PostalCode,nvl(ActiveInverterCount,0) as ActiveInverterCount,nvl(InverterCount,0) as InverterCount,nvl(LastChecked,' ') as LastChecked,nvl(TotalEnergy,0) as TotalEnergy,(case when TodayEnergyFlag=1 then nvl(TodayEnergy,0) else (nvl(TotalEnergy,0)-nvl(YesterdayTotalEnergy,0)) end) as TodayEnergy,nvl(LastDownTime,' ' ) as LastDownTime,nvl(LastDataReceived,' ') as LastDataReceived,nvl(TodayHoursOn,0) as TodayHoursOn,nvl((case when Status=-1 then 'Active' else case when Status=2 then 'Warning' else case when Status=3 then 'Down' else 'Offline' end end end),'Offline') as Status,nvl(InstallationCapacity,0) as InstallationCapacity,nvl(Mobile,' ') as Mobile,nvl(Telephone,' ') as Telephone,nvl(LocationUrl,' ') as LocationUrl from  "
				+ " (Select SiteId,SiteCode,TodayEnergyFlag,CustomerReference as SiteReference,SiteName,Latitude,Longitude,Address,City,StateName as State,CountryName as Country,PostalCode,InstallationCapacity,Mobile,Telephone,SiteImage as LocationUrl from mSite a,mState b,mCountry c where a.StateID=b.StateId and a.CountryID=c.CountryId and a.ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID ='"
				+ CustomerID + "')) tbl1 left outer join "
				+ " (Select a.SiteID,Count(case when a.Status=1 and  b.DismandalFlag=0  then 1 else null end) as ActiveInverterCount,Count(distinct (case when b.DismandalFlag=0 then b.EquipmentID else null end)) as InverterCount,to_char(Max(a.Timestamp),'DD-MM-YYYY HH24:MI') as LastChecked,Sum(nvl(TodayEnergy,0)) as TodayEnergy,Sum(nvl(TotalEnergy,0)) as TotalEnergy,Max(nvl((case when Status=1 then -1 else Status end),0)) as Status,Sum(nvl(YesterdayTotalEnergy,0)) as YesterdayTotalEnergy,to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI') as LastDownTime,to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI') as LastDataReceived,Max(TodayHoursOn) as TodayHoursOn from tDataSource a,mEquipment b where a.SiteID=b.SiteID and a.EquipmentID=b.EquipmentID and a.ActiveFlag=1 and b.ActiveFlag=1 and a.SiteID in (Select SiteId from mSite where ActiveFlag=1 and CustomerID in (Select CustomerId from mCustomer where ActiveFlag=1 and CustomerID ='"
				+ CustomerID
				+ "')) and EquipmentTypeID in (select EquipmentTypeId from mEquipmentType where CategoryID in (select CategoryId from mEquipmentCategory where CategoryGroup in ('INV'))) group by a.SiteID) tbl2 on tbl1.SiteID=tbl2.SiteID order by SiteCode ";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {

			Query = "Select SiteId,Sitetype,SiteCode,SiteReference,SiteName,Latitude,Longitude,Address,City,State,Country,PostalCode,ActiveInverterCount,InverterCount,LastChecked,TotalEnergy,TodayEnergy,LastDownTime,LastDataReceived,TodayHoursOn,coalesce((case when Status=-1 then 'Active' else case when Status=2 then 'Warning' else case when Status=3 then 'Down' else 'Offline' end end end),'Offline') as Status,InstallationCapacity,Mobile,Telephone,LocationUrl,SitePoDate,ActiveEMCount,EMCount,ProductionOn from(Select coalesce(SiteId,0) as SiteId,coalesce((case when Sitetypeid=1 then 'Rooftop' else case when Sitetypeid=2 then 'Utility' end end),' ') as Sitetype,coalesce(SiteCode,' ') as SiteCode,coalesce(SiteReference,' ') as SiteReference,coalesce(SiteName,' ') as SiteName,coalesce(Latitude,' ') as Latitude,coalesce(Longitude,' ') as Longitude,coalesce(Address,' ') as Address,coalesce(City,' ') as City,coalesce(State,' ')  as State,coalesce(Country,' ') as Country,coalesce(PostalCode,' ') as PostalCode,coalesce(Sum(ActiveInverterCount),0) as ActiveInverterCount,coalesce(Sum(InverterCount),0) as InverterCount,coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked,coalesce(Sum(TotalEnergy),0) as TotalEnergy, coalesce(Sum(TodayEnergy),0)  as TodayEnergy,coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime,coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,coalesce(InstallationCapacity,0) as InstallationCapacity,coalesce(Mobile,' ') as Mobile,coalesce(Telephone,' ') as Telephone,coalesce(LocationUrl,' ') as LocationUrl,coalesce(SitePoDate,' ') as SitePoDate,coalesce(Sum(ActiveEMCount),0) as ActiveEMCount,coalesce(Sum(EMCount),0) as EMCount,coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(Select SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Latitude,Longitude,Address,City,State,Country,PostalCode,Sum(ActiveInverterCount) as ActiveInverterCount,Sum(InverterCount) as InverterCount,Sum(ActiveEMCount) as ActiveEMCount,Sum(EMCount) as EMCount,(Max(LastChecked)) as ProductionOn, Max(LastChecked) as LastChecked, Max(LastDataReceived) as LastDataReceived, Max(LastDownTime) as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy,   (case when TodayEnergyFlag=1 then coalesce(Sum(TodayEnergy),0)  else coalesce(Sum(TotalEnergy)-Sum(YesterdayTotalEnergy),0) end) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 ,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,InstallationCapacity,Mobile,Telephone,LocationUrl,SitePoDate from(select e.sitetypeid,e.SiteCode,e.CustomerReference as SiteReference,e.sitename,e.Latitude,e.longitude,e.address,e.city,g.StateName as State,h.CountryName as Country,e.postalcode,Max(coalesce((case when d.Status=1 then -1 else Status end),0)) as Status,Count(case when d.Status=1 and  a.DismandalFlag=0 and c.categorygroup='INV' then 1 else null end) as ActiveInverterCount,Count(distinct (case when a.DismandalFlag=0 and c.categorygroup='INV' then a.EquipmentID else null end)) as InverterCount,Count(case when d.Status=1 and  a.DismandalFlag=0 and c.categorygroup='EM' then 1 else null end) as ActiveEMCount,Count(distinct (case when a.DismandalFlag=0 and c.categorygroup='EM' then a.EquipmentID else null end)) as EMCount,e.InstallationCapacity,e.Mobile,e.Telephone,e.SiteImage as LocationUrl,to_char(e.SitePoDate,'DD-MM-YYYY HH24:MI') as SitePoDate,e.TodayEnergyFlag,c.categorygroup,e.SiteID,Max(Timestamp) as LastChecked,Max(LastDataReceived) as LastDataReceived,(case when e.equipmentflag=1 and c.categorygroup='EM' then Sum(coalesce(TotalEnergy,0)) else  case when e.equipmentflag=0 and c.categorygroup='INV' then Sum(coalesce(TotalEnergy,0)) else 0 end end) as TotalEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(TodayEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(TotalEnergy,0))) else 0 end end) as TodayEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(YesterdayTotalEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(YesterdayTotalEnergy,0))) else 0 end end) as YesterdayTotalEnergy,Max(LastDownTime) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from msite e left outer join mState g on e.StateID=g.StateId  left outer join mCountry h on e.CountryID=h.CountryID left outer join tdatasource d on e.siteid=d.siteid  and d.activeflag=1 left outer join mequipment a on d.equipmentid=a.equipmentid  and a.activeflag=1 left outer join   mequipmenttype b on a.equipmenttypeid=b.equipmenttypeid left outer join mequipmentcategory c on c.categoryid=b.categoryid and c.categorygroup in('INV','EM')where  e.activeflag=1 and e.CustomerID='"
					+ CustomerID
					+ "' group by d.siteid,e.siteid,e.sitetypeid,e.sitename,e.sitecode,e.customerreference,e.address,e.Latitude,e.longitude,e.city,g.StateName,h.CountryName,e.PostalCode,d.Status,a.DismandalFlag,a.EquipmentID,e.TodayEnergyFlag,e.equipmentflag,e.todayenergyflag,c.categorygroup,e.InstallationCapacity,e.Mobile,e.Telephone,e.SiteImage,e.SitePoDate) as tbl group by SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Latitude,longitude,address,city,State,Country,Postalcode,todayenergyflag,InstallationCapacity,Mobile,Telephone,LocationUrl,SitePoDate) tbl2 group by SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Address,city,Latitude,longitude,State,Country,postalcode,InstallationCapacity,Mobile,Telephone,LocationUrl,SitePoDate) tbl3 order by status";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "Select SiteId,Sitetype,SiteCode,SiteReference,SiteName,Latitude,Longitude,Address,City,State,Country,PostalCode,ActiveInverterCount,InverterCount,LastChecked,TotalEnergy,TodayEnergy,LastDownTime,LastDataReceived,TodayHoursOn,coalesce((case when Status=-1 then 'Active' else case when Status=2 then 'Warning' else case when Status=3 then 'Down' else 'Offline' end end end),'Offline') as Status,InstallationCapacity,Mobile,Telephone,LocationUrl,SitePoDate,ActiveEMCount,EMCount,ProductionOn from(Select coalesce(SiteId,0) as SiteId,coalesce((case when Sitetypeid=1 then 'Rooftop' else case when Sitetypeid=2 then 'Utility' end end),' ') as Sitetype,coalesce(SiteCode,' ') as SiteCode,coalesce(SiteReference,' ') as SiteReference,coalesce(SiteName,' ') as SiteName,coalesce(Latitude,' ') as Latitude,coalesce(Longitude,' ') as Longitude,coalesce(Address,' ') as Address,coalesce(City,' ') as City,coalesce(State,' ')  as State,coalesce(Country,' ') as Country,coalesce(PostalCode,' ') as PostalCode,coalesce(Sum(ActiveInverterCount),0) as ActiveInverterCount,coalesce(Sum(InverterCount),0) as InverterCount,coalesce(to_char(Max(LastChecked),'DD-MM-YYYY HH24:MI'),' ') as LastChecked,coalesce(Sum(TotalEnergy),0) as TotalEnergy, coalesce(Sum(TodayEnergy),0)  as TodayEnergy,coalesce(to_char(Max(LastDownTime),'DD-MM-YYYY HH24:MI'),' ') as LastDownTime,coalesce(to_char(Max(LastDataReceived),'DD-MM-YYYY HH24:MI'),' ') as LastDataReceived, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,coalesce(InstallationCapacity,0) as InstallationCapacity,coalesce(Mobile,' ') as Mobile,coalesce(Telephone,' ') as Telephone,coalesce(LocationUrl,' ') as LocationUrl,coalesce(SitePoDate,' ') as SitePoDate,coalesce(Sum(ActiveEMCount),0) as ActiveEMCount,coalesce(Sum(EMCount),0) as EMCount,coalesce(to_char(Max(LastChecked),'DD-MM-YYYY'),' ') as ProductionOn, coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 from(Select SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Latitude,Longitude,Address,City,State,Country,PostalCode,Sum(ActiveInverterCount) as ActiveInverterCount,Sum(InverterCount) as InverterCount,Sum(ActiveEMCount) as ActiveEMCount,Sum(EMCount) as EMCount,(Max(LastChecked)) as ProductionOn, Max(LastChecked) as LastChecked, Max(LastDataReceived) as LastDataReceived, Max(LastDownTime) as LastDownTime, coalesce(Sum(TotalEnergy),0) as TotalEnergy,   (case when TodayEnergyFlag=1 then coalesce(Sum(TodayEnergy),0)  else coalesce(Sum(TotalEnergy)-Sum(YesterdayTotalEnergy),0) end) as TodayEnergy, coalesce(Max(TodayHoursOn),0) as TodayHoursOn,coalesce(sum((0.00067*TodayEnergy*1000)),0) as Co2,coalesce(sum((0.00067*TotalEnergy*1000)),0) as TotalCo2 ,Max(coalesce((case when Status=1 then -1 else Status end),0)) as Status,InstallationCapacity,Mobile,Telephone,LocationUrl,SitePoDate from(select e.sitetypeid,e.SiteCode,e.CustomerReference as SiteReference,e.sitename,e.Latitude,e.longitude,e.address,e.city,g.StateName as State,h.CountryName as Country,e.postalcode,Max(coalesce((case when d.Status=1 then -1 else Status end),0)) as Status,Count(case when d.Status=1 and  a.DismandalFlag=0 and c.categorygroup='INV' then 1 else null end) as ActiveInverterCount,Count(distinct (case when a.DismandalFlag=0 and c.categorygroup='INV' then a.EquipmentID else null end)) as InverterCount,Count(case when d.Status=1 and  a.DismandalFlag=0 and c.categorygroup='EM' then 1 else null end) as ActiveEMCount,Count(distinct (case when a.DismandalFlag=0 and c.categorygroup='EM' then a.EquipmentID else null end)) as EMCount,e.InstallationCapacity,e.Mobile,e.Telephone,e.SiteImage as LocationUrl,to_char(e.SitePoDate "
						+ TimeConversionFromUTC
						+ " ,'DD-MM-YYYY HH24:MI') as SitePoDate,e.TodayEnergyFlag,c.categorygroup,e.SiteID,Max(Timestamp "
						+ TimeConversionFromUTC + " ) as LastChecked,Max(LastDataReceived " + TimeConversionFromUTC
						+ " ) as LastDataReceived,(case when e.equipmentflag=1 and c.categorygroup='EM' then Sum(coalesce(TotalEnergy,0)) else  case when e.equipmentflag=0 and c.categorygroup='INV' then Sum(coalesce(TotalEnergy,0)) else 0 end end) as TotalEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(TodayEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(TotalEnergy,0))) else 0 end end) as TodayEnergy,(case when e.TodayEnergyFlag=1 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV')  then Sum(coalesce(YesterdayTotalEnergy,0)) else case when e.TodayEnergyFlag=0 and (e.equipmentflag=1 and c.categorygroup='EM') or (e.equipmentflag=0 and c.categorygroup='INV') then (Sum(coalesce(YesterdayTotalEnergy,0))) else 0 end end) as YesterdayTotalEnergy,Max(LastDownTime "
						+ TimeConversionFromUTC
						+ " ) as LastDownTime,Max(TodayHoursOn) as TodayHoursOn from msite e left outer join mState g on e.StateID=g.StateId  left outer join mCountry h on e.CountryID=h.CountryID left outer join tdatasource d on e.siteid=d.siteid  and d.activeflag=1 left outer join mequipment a on d.equipmentid=a.equipmentid  and a.activeflag=1 left outer join   mequipmenttype b on a.equipmenttypeid=b.equipmenttypeid left outer join mequipmentcategory c on c.categoryid=b.categoryid and c.categorygroup in('INV','EM')where  e.activeflag=1 and e.CustomerID='"
						+ CustomerID
						+ "' group by d.siteid,e.siteid,e.sitetypeid,e.sitename,e.sitecode,e.customerreference,e.address,e.Latitude,e.longitude,e.city,g.StateName,h.CountryName,e.PostalCode,d.Status,a.DismandalFlag,a.EquipmentID,e.TodayEnergyFlag,e.equipmentflag,e.todayenergyflag,c.categorygroup,e.InstallationCapacity,e.Mobile,e.Telephone,e.SiteImage,e.SitePoDate) as tbl group by SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Latitude,longitude,address,city,State,Country,Postalcode,todayenergyflag,InstallationCapacity,Mobile,Telephone,LocationUrl,SitePoDate) tbl2 group by SiteID,SiteTypeID,SiteCode,SiteReference,SiteName,Address,city,Latitude,longitude,State,Country,postalcode,InstallationCapacity,Mobile,Telephone,LocationUrl,SitePoDate) tbl3 order by status";

			}
		}
		System.out.println(Query);
		List<DashboardSiteListBean> DashboardSiteList = session.createSQLQuery(Query).list();

		return DashboardSiteList;
	}

	public List<Site> listallsites(int siteid) {
		Session session = sessionFactory.getCurrentSession();

		String Query = "from Site where  SiteID not in('" + siteid + "')";
		List<Site> SiteList = session.createQuery(Query).list();

		return SiteList;
	}

	public List<AnnualYeild> getAnnualyeildbySite(int siteid) {
		Session session = sessionFactory.getCurrentSession();
		String Query = "Select SiteID,sum(coalesce(TodayEnergy,0)) as annualyeildvalue from (Select SiteID as SiteID, SiteName, Timestamp, (case when TodayEnergyFlag=1 then TodayEnergy else (DailyEnergy - Lag(DailyEnergy) OVER ( ORDER BY SiteID, Timestamp)) end) as TodayEnergy from (Select t1.SiteID as SiteID, (t3.SiteName) as SiteName, Timestamp, t3.TodayEnergyFlag, TotalEnergy, TodayEnergy, (coalesce(TotalEnergy, 0) - coalesce(YesterdayEnergy, 0)) as DailyEnergy from (Select SiteID, Timestamp, coalesce(Sum(coalesce(TotalEnergy, 0)), 0) as TotalEnergy, coalesce(Sum(coalesce(TodayEnergy, 0)), 0) as TodayEnergy from (Select SiteID, date_trunc('day', Timestamp) as Timestamp, EquipmentID, Max(coalesce(TotalEnergy, 0)) as TotalEnergy, Max(coalesce(TodayEnergy, 0)) as TodayEnergy from tDataTransaction where SiteID in ('"
				+ siteid
				+ "') and EquipmentID in (Select EquipmentId from mEquipment where ActiveFlag=1 and SiteId in('"
				+ siteid
				+ "') and EquipmentTypeId in (Select EquipmentTypeId from mEquipmentType where ActiveFlag=1 and CategoryId in (Select CategoryId from mEquipmentCategory where ActiveFlag=1 and CategoryGroup in (case when (select EquipmentFlag from mSite where SiteId in('"
				+ siteid
				+ "')) in (0) then 'INV' else 'EM' end)))) and date_part('year', Timestamp+ interval '1 day') = date_part('year', CURRENT_DATE) group by SiteID, date_trunc('day', Timestamp), EquipmentID) tbl1 group by SiteID, Timestamp) t1 left outer join ( Select SiteID, YesterdayEnergy from (Select SiteID, Timestamp, ROW_NUMBER() over( order by Timestamp desc) as Sno, coalesce(Sum(coalesce(TotalEnergy, 0)), 0) as YesterdayEnergy from (Select SiteID, date_trunc('day', Timestamp) as Timestamp, EquipmentID, Max(coalesce(TotalEnergy, 0)) as TotalEnergy, Max(coalesce(TodayEnergy, 0)) as TodayEnergy from tDataTransaction where SiteID in ('"
				+ siteid
				+ "') and EquipmentID in (Select EquipmentId from mEquipment where ActiveFlag=1 and SiteId=3 and EquipmentTypeId in (Select EquipmentTypeId from mEquipmentType where ActiveFlag=1 and CategoryId in (Select CategoryId from mEquipmentCategory where ActiveFlag=1 and CategoryGroup in (case when (select EquipmentFlag from mSite where SiteId in('"
				+ siteid
				+ "')) in (0) then 'INV' else 'EM' end)))) and Timestamp < date_trunc('day',to_timestamp('01-01-' || date_part('year', CURRENT_DATE) || ' 00:00:00','DD/MM/YYYY HH24:MI:SS')) group by SiteID, date_trunc('day', Timestamp), EquipmentID) tbl1 group by SiteID, Timestamp) tbl1 where Sno=1) t2 on t2.SiteID=t1.SiteID left outer join mSite t3 on t3.SiteID=t1.SiteID ) tbl ) tblfinal group by SiteID";
		System.out.println(Query);
		List<AnnualYeild> Annaulyeild = session.createSQLQuery(Query).list();
		return Annaulyeild;

	}

	@Override
	public List<Site> listSitesAgainstCollection() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Site where ActiveFlag=1 ";
		List<Site> SitesList = session.createQuery(Query).list();

		return SitesList;
	}

	public void updateSiteConfig(Site site) {
		Session session = sessionFactory.getCurrentSession();
		session.update(site);

	}

}
