
/******************************************************
 * 
 *    	Filename	: EquipmentTypeDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Equipment Type operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentType;

public interface EquipmentTypeDAO {

	public void addEquipmentType(EquipmentType equipmenttype);

	public void updateEquipmentType(EquipmentType equipmenttype);

	public EquipmentType getEquipmentTypeById(int id);

	public EquipmentType getEquipmentTypeByMax(String MaxColumnName);

	public void removeEquipmentType(int id);

	public List<EquipmentType> listEquipmentTypes();

	public List<EquipmentType> listallequipments(int equipmentid);

	public List<EquipmentType> listFilterEquipmentsByTypeId(int typeId);
}
