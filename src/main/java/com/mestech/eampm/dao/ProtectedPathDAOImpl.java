
/******************************************************
 * 
 *    	Filename	: ProtectedPathDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for URl operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.ProtectedUrl;

@Repository
public class ProtectedPathDAOImpl implements ProtectedPathDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<ProtectedUrl> getAll() {
		Session session = sessionFactory.getCurrentSession();
		return session.createQuery("FROM ProtectedUrl").list();
	}

}
