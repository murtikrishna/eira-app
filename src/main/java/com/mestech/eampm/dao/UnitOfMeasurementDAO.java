
/******************************************************
 * 
 *    	Filename	: UnitOfMeasurementDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for UOM operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.UnitOfMeasurement;

public interface UnitOfMeasurementDAO {

	public void addUnitOfMeasurement(UnitOfMeasurement unitofmeasurement);

	public void updateUnitOfMeasurement(UnitOfMeasurement unitofmeasurement);

	public UnitOfMeasurement getUnitOfMeasurementById(int id);

	public void removeUnitOfMeasurement(int id);

	public List<UnitOfMeasurement> listUnitOfMeasurements();
}
