
/******************************************************
 * 
 *    	Filename	: DataTransactionDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Data Source related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.bean.EnergyPerformanceBean;
import com.mestech.eampm.bean.EquipmentWithParameterListBean;
import com.mestech.eampm.bean.EquipmentWithoutParameterListBean;
import com.mestech.eampm.bean.GraphDataBean;
import com.mestech.eampm.model.DataTransaction;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteSummary;
import com.mestech.eampm.utility.UtilityCommonDAO;

@Repository
public class DataTransactionDAOImpl implements DataTransactionDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();

	// @Override
	public void addDataTransaction(DataTransaction dataTransaction) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(dataTransaction);

	}

	// @Override
	public void updateDataTransaction(DataTransaction dataTransaction) {
		Session session = sessionFactory.getCurrentSession();
		session.update(dataTransaction);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<DataTransaction> listDataTransactions() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<DataTransaction> DataTransactionsList = session.createQuery("from DataTransaction").list();

		return DataTransactionsList;
	}

	public List<DataTransaction> listDataTransactionsForDataDownload(int siteId, String fromDate, String toDate,
			int equipmentId, String TimezoneOffset) {

		List<DataTransaction> DataTransactionsList = new ArrayList<DataTransaction>();
		String EquipmentCondition = "";

		if (equipmentId != 0 && equipmentId != -1) {
			EquipmentCondition = " and EquipmentID ='" + equipmentId + "' ";
		}

		try {
			Session session = sessionFactory.getCurrentSession();

			// Oracle
			String Query = "from DataTransaction where SiteId='" + siteId + "' and  trunc(timestamp)>=trunc(to_date('"
					+ fromDate + "','dd-mm-yyyy')) and  trunc(timestamp)<=trunc(to_date('" + toDate
					+ "','dd-mm-yyyy')) " + EquipmentCondition + "  order by EquipmentId,Timestamp";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				Query = "from DataTransaction where SiteId='" + siteId
						+ "' and  date_trunc('day',timestamp)>=date_trunc('day',to_timestamp('" + fromDate
						+ "','dd-mm-yyyy')) and  date_trunc('day',timestamp)<=date_trunc('day',to_timestamp('" + toDate
						+ "','dd-mm-yyyy')) " + EquipmentCondition + "  order by EquipmentId,Timestamp";

				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {

					Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(fromDate);

					System.out.println(date1);
					Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(toDate);
					System.out.println(date2);

					SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

					Calendar c = Calendar.getInstance();
					Calendar c1 = Calendar.getInstance();
					c.setTime(date1);
					c1.setTime(date2);

					c.add(Calendar.MINUTE, Integer.valueOf(TimezoneOffset));
					c1.add(Calendar.MINUTE, Integer.valueOf(TimezoneOffset));

					// convert calendar to date
					Date FromDate = c.getTime();
					Date ToDate = c1.getTime();

					String FromDate1 = df.format(FromDate);
					String ToDate1 = df.format(ToDate);

					System.out.println("FromDate " + df.format(FromDate));
					System.out.println("ToDate " + df.format(ToDate));

					// Query = "from DataTransaction where SiteId='" + siteId + "' and
					// date_trunc('day',timestamp" + TimeConversionFromUTC +
					// ")>=date_trunc('day',to_timestamp('" + fromDate + "','dd-mm-yyyy')) and
					// date_trunc('day',timestamp" + TimeConversionFromUTC +
					// ")<=date_trunc('day',to_timestamp('" + toDate + "','dd-mm-yyyy')) " +
					// EquipmentCondition + " order by EquipmentId,Timestamp";
					Query = "from DataTransaction where SiteId='" + siteId
							+ "' and  date_trunc('day',timestamp)>=date_trunc('day',to_timestamp('" + FromDate1
							+ "','dd-mm-yyyy')) and  date_trunc('day',timestamp)<=date_trunc('day',to_timestamp('"
							+ ToDate1 + "','dd-mm-yyyy')) " + EquipmentCondition + "  order by EquipmentId,Timestamp";

				}
			}
			System.out.println("DataExport Query" + Query);

			DataTransactionsList = session.createQuery(Query).list();
			System.out.println("SIZE " + DataTransactionsList.size());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return DataTransactionsList;
	}

// 03-02-19
	/*
	 * public List<DataTransaction> getDataTransactionListByUserId(int userId,String
	 * TimezoneOffset) { Session session = sessionFactory.getCurrentSession();
	 * //Common for Oracle & PostgreSQL String Query =
	 * "from DataTransaction where SiteID in (Select SiteId from Site where ActiveFlag='1' and CustomerID in (Select CustomerID from CustomerMap where UserID='"
	 * + userId +
	 * "') or PrimaryCustomer in (Select CustomerID from CustomerMap where ActiveFlag=1 and UserID='"
	 * + userId + "'))"; List<DataTransaction> DataTransactionsList =
	 * session.createQuery(Query).list();
	 * 
	 * return DataTransactionsList; }
	 */

	public List<DataTransaction> getDataTransactionListByUserId(int userId, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from DataTransaction where SiteID in (Select SiteId from SiteMap where UserID='" + userId
				+ "' and ActiveFlag=1) and ActiveFlag=1";
		List<DataTransaction> DataTransactionsList = session.createQuery(Query).list();

		return DataTransactionsList;
	}

	public List<DataTransaction> getDataTransactionListByCustomerId(int customerId, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from DataTransaction where SiteID in (Select SiteId from Site where ActiveFlag='1' and CustomerID='"
				+ customerId + "')";
		List<DataTransaction> DataTransactionsList = session.createQuery(Query).list();

		return DataTransactionsList;
	}

	public List<DataTransaction> getDataTransactionListBySiteId(int SiteId, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from DataTransaction where SiteID = '" + SiteId + "'";
		List<DataTransaction> DataTransactionsList = session.createQuery(Query).list();

		return DataTransactionsList;
	}

	public List<DataTransaction> getDataTransactionListByEquipmentId(int equipmentId, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "from DataTransaction where EquipmentID = '" + equipmentId
				+ "'  and Timestamp <= trunc(sysdate+1) and  Timestamp > trunc(to_date('01-01-2015','DD-MM-YYYY')) order by TimeStamp";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "from DataTransaction where EquipmentID = '" + equipmentId
					+ "'  and Timestamp <= date_trunc('day',now()) + (1 * interval '1 day') and  Timestamp > date_trunc('day',to_timestamp('01-01-2015','DD-MM-YYYY')) order by TimeStamp";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "from DataTransaction where EquipmentID = '" + equipmentId + "'  and Timestamp"
						+ TimeConversionFromUTC + " <= date_trunc('day',now()" + TimeConversionFromUTC
						+ ") + (1 * interval '1 day') and  Timestamp" + TimeConversionFromUTC
						+ " > date_trunc('day',to_timestamp('01-01-2015','DD-MM-YYYY')) order by TimeStamp";

			}
		}

		System.out.println(Query);
		List<DataTransaction> DataTransactionsList = session.createQuery(Query).list();

		return DataTransactionsList;
	}

	public List<DataTransaction> getDataTransactionListByEquipmentIdWithDate(int equipmentId, String FromDate,
			String ToDate, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "from DataTransaction where EquipmentID = '" + equipmentId + "'  and Timestamp <= to_date('"
				+ ToDate + "','DD/MM/YYYY HH24:MI:SS') and  Timestamp > to_date('" + FromDate
				+ "','DD/MM/YYYY HH24:MI:SS') order by TimeStamp";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "from DataTransaction where EquipmentID = '" + equipmentId + "'  and Timestamp <= to_timestamp('"
					+ ToDate + "','DD/MM/YYYY HH24:MI:SS') and  Timestamp > to_timestamp('" + FromDate
					+ "','DD/MM/YYYY HH24:MI:SS') order by TimeStamp";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "from DataTransaction where EquipmentID = '" + equipmentId + "'  and Timestamp"
						+ TimeConversionFromUTC + " <= to_timestamp('" + ToDate
						+ "','DD/MM/YYYY HH24:MI:SS') and  Timestamp" + TimeConversionFromUTC + " > to_timestamp('"
						+ FromDate + "','DD/MM/YYYY HH24:MI:SS') order by TimeStamp";

			}
		}
		System.out.println(Query);
		List<DataTransaction> DataTransactionsList = session.createQuery(Query).list();

		return DataTransactionsList;
	}

	public List<GraphDataBean> getDataTransactionListByEquipmentIdsWithDate(String equipmentIds, String FromDate,
			String ToDate, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "Select Timestamp,SUM(TotalEnergy) as Value from DataTransaction where EquipmentID in ("
				+ equipmentIds + ")  and Timestamp <=  to_date('" + ToDate
				+ "','DD/MM/YYYY HH24:MI:SS') and  Timestamp > to_date('" + FromDate
				+ "','DD/MM/YYYY HH24:MI:SS') group by TimeStamp order by TimeStamp";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select Timestamp,SUM(TotalEnergy) as Value from DataTransaction where EquipmentID in ("
					+ equipmentIds + ")  and Timestamp <=  to_timestamp('" + ToDate
					+ "','DD/MM/YYYY HH24:MI:SS') and  Timestamp > to_timestamp('" + FromDate
					+ "','DD/MM/YYYY HH24:MI:SS') group by TimeStamp order by TimeStamp";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "Select (Timestamp" + TimeConversionFromUTC
						+ ") as Timestamp,SUM(TotalEnergy) as Value from DataTransaction where EquipmentID in ("
						+ equipmentIds + ")  and Timestamp" + TimeConversionFromUTC + " <=  to_timestamp('" + ToDate
						+ "','DD/MM/YYYY HH24:MI:SS') and  Timestamp" + TimeConversionFromUTC + " > to_timestamp('"
						+ FromDate + "','DD/MM/YYYY HH24:MI:SS') group by TimeStamp order by TimeStamp";

			}
		}

		System.out.println(Query);
		List<GraphDataBean> DataTransactionsList = session.createQuery(Query).list();

		return DataTransactionsList;
	}

	public List<GraphDataBean> getParameterValuesByEquipmentIdsWithDate(String equipmentIds, String Parameter,
			String FromDate, String ToDate, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "Select Timestamp,max(nvl(" + Parameter
				+ ",0)) as Value from tDataTransaction where EquipmentID in (" + equipmentIds
				+ ")  and Timestamp <=  to_date('" + ToDate + "','DD/MM/YYYY HH24:MI:SS') and  Timestamp > to_date('"
				+ FromDate + "','DD/MM/YYYY HH24:MI:SS') group by TimeStamp order by TimeStamp";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select Timestamp,max(coalesce(" + Parameter
					+ ",null)) as Value from tDataTransaction where EquipmentID in (" + equipmentIds + ")  and  "
					+ Parameter + " is not null and Timestamp <=  to_timestamp('" + ToDate
					+ "','DD/MM/YYYY HH24:MI:SS') and  Timestamp > to_timestamp('" + FromDate
					+ "','DD/MM/YYYY HH24:MI:SS') group by TimeStamp order by TimeStamp";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "Select  (Timestamp" + TimeConversionFromUTC + ") as Timestamp,max(coalesce(" + Parameter
						+ ",null)) as Value from tDataTransaction where EquipmentID in (" + equipmentIds + ")  and "
						+ Parameter + " is not null and Timestamp" + TimeConversionFromUTC + " <=  to_timestamp('"
						+ ToDate + "','DD/MM/YYYY HH24:MI:SS') and  Timestamp" + TimeConversionFromUTC
						+ " > to_timestamp('" + FromDate
						+ "','DD/MM/YYYY HH24:MI:SS') group by TimeStamp order by TimeStamp";

			}

		}

		System.out.println("PC Query::: " + Query);

		List<GraphDataBean> DataTransactionsList = session.createSQLQuery(Query).list();

		return DataTransactionsList;
	}

	public String getParameterComparisionDateWithDate(String equipmentIds, String Parameters, String FromDate,
			String ToDate, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "Select Timestamp," + Parameters + " from DataTransaction where EquipmentID in (" + equipmentIds
				+ ")  and Timestamp <=  to_date('" + ToDate + "','DD/MM/YYYY HH24:MI:SS') and  Timestamp > to_date('"
				+ FromDate + "','DD/MM/YYYY HH24:MI:SS') order by TimeStamp";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select Timestamp," + Parameters + " from DataTransaction where EquipmentID in (" + equipmentIds
					+ ")  and Timestamp <=  to_timestamp('" + ToDate
					+ "','DD/MM/YYYY HH24:MI:SS') and  Timestamp > to_timestamp('" + FromDate
					+ "','DD/MM/YYYY HH24:MI:SS') order by TimeStamp";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "Select (Timestamp" + TimeConversionFromUTC + ") as Timestamp," + Parameters
						+ " from DataTransaction where EquipmentID in (" + equipmentIds + ")  and Timestamp"
						+ TimeConversionFromUTC + " <=  to_timestamp('" + ToDate
						+ "','DD/MM/YYYY HH24:MI:SS') and  Timestamp" + TimeConversionFromUTC + " > to_timestamp('"
						+ FromDate + "','DD/MM/YYYY HH24:MI:SS') order by TimeStamp";

			}
		}

		System.out.println(Query);

		List<GraphDataBean> DataTransactionsList = session.createSQLQuery(Query).list();
		String ParameterChartData = "";

		/*
		 * if(DataTransactionsList.size()>0) { Integer a=0; for(Object
		 * object:DataTransactionsList) { Object []obj=(Object[]) object; GraphDataBean
		 * objGraphDataBean =new GraphDataBean(); objGraphDataBean.setTimestamp((Date)
		 * obj[0]);
		 * 
		 * if(obj[1]==null) { objGraphDataBean.setValue(0.0); } else {
		 * objGraphDataBean.setValue((Double) obj[1]); }
		 * 
		 * 
		 * 
		 * 
		 * if(objGraphDataBean.getTimestamp() !=null) {
		 * 
		 * try {
		 * 
		 * SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 * SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
		 * 
		 * String varChartDate =
		 * sdfchart.format(objGraphDataBean.getTimestamp()).toString();
		 * 
		 * Calendar cal = Calendar. getInstance();
		 * cal.setTime(objGraphDataBean.getTimestamp()); cal. add(Calendar.HOUR, 5);
		 * cal. add(Calendar.MINUTE, 30);
		 * 
		 * 
		 * 
		 * //long millis = lstDataTransaction.get(a).getTimestamp().getTime(); long
		 * millis = cal.getTime().getTime(); varChartDate = String.valueOf(millis);
		 * 
		 * Double dbValue = 0.0; dbValue = GetParameterValueInUnit( Parameter,
		 * objGraphDataBean.getValue());
		 * 
		 * String varChartValue = String.format("%.2f", dbValue);
		 * 
		 * 
		 * if(a==0) { blnIsRecordExist =true; ParameterChartData = "[" + varChartDate +
		 * "," + varChartValue + "]"; } else { blnIsRecordExist =true;
		 * ParameterChartData = ParameterChartData + ",[" + varChartDate + "," +
		 * varChartValue + "]";
		 * 
		 * }
		 * 
		 * 
		 * } catch (Exception e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 * 
		 * 
		 * }
		 * 
		 * 
		 * a = a + 1; }
		 * 
		 * }
		 */

		return "";
	}

	public List<GraphDataBean> getDaywiseIrradiationValuesBySitesWithDate(String siteId, String FromDate, String ToDate,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		// Oracle
		String IrradiationQuery = "Select Timestamp,NVL(Round(Sum(NVL(Irradiation,0)),2),0) as Value from "
				+ "(Select Trunc(Timestamp) as Timestamp,EquipmentID,Max(NVL(Irradiation,0)) as Irradiation from tDataTransaction where "
				+ "EquipmentID in (Select e.EquipmentID from mEquipment e,mEquipmentType et, mEquipmentCategory ec "
				+ "where e.EquipmentTypeID=et.EquipmentTypeID and et.CategoryID=ec.CategoryID and ec.EquipmentCategory ='WEATHRSTION' and e.SiteID in ("
				+ siteId + ")) " + "and Timestamp > to_date('" + FromDate
				+ "','DD/MM/YYYY HH24:MI:SS') and Timestamp <=  to_date('" + ToDate + "','DD/MM/YYYY HH24:MI:SS') "
				+ "group by Trunc(Timestamp),EquipmentID) tbl1  group by Timestamp order by Timestamp ";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			IrradiationQuery = "Select Timestamp,coalesce(Round(Sum(coalesce(Irradiation,0)),2),0) as Value from "
					+ "(Select date_trunc('day',Timestamp) as Timestamp,EquipmentID,Max(coalesce(Irradiation,0)) as Irradiation from tDataTransaction where "
					+ "EquipmentID in (Select e.EquipmentID from mEquipment e,mEquipmentType et, mEquipmentCategory ec "
					+ "where e.EquipmentTypeID=et.EquipmentTypeID and et.CategoryID=ec.CategoryID and ec.EquipmentCategory ='WEATHRSTION' and e.SiteID in ("
					+ siteId + ")) " + "and Timestamp > to_timestamp('" + FromDate
					+ "','DD/MM/YYYY HH24:MI:SS') and Timestamp <=  to_timestamp('" + ToDate
					+ "','DD/MM/YYYY HH24:MI:SS') "
					+ "group by date_trunc('day',Timestamp),EquipmentID) tbl1  group by Timestamp order by Timestamp ";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				IrradiationQuery = "Select Timestamp,coalesce(Round(Sum(coalesce(Irradiation,0)),2),0) as Value from "
						+ "(Select date_trunc('day',Timestamp" + TimeConversionFromUTC
						+ ") as Timestamp,EquipmentID,Max(coalesce(Irradiation,0)) as Irradiation from tDataTransaction where "
						+ "EquipmentID in (Select e.EquipmentID from mEquipment e,mEquipmentType et, mEquipmentCategory ec "
						+ "where e.EquipmentTypeID=et.EquipmentTypeID and et.CategoryID=ec.CategoryID and ec.EquipmentCategory ='WEATHRSTION' and e.SiteID in ("
						+ siteId + ")) " + "and Timestamp" + TimeConversionFromUTC + " > to_timestamp('" + FromDate
						+ "','DD/MM/YYYY HH24:MI:SS') and Timestamp" + TimeConversionFromUTC + " <=  to_timestamp('"
						+ ToDate + "','DD/MM/YYYY HH24:MI:SS') " + "group by date_trunc('day',Timestamp"
						+ TimeConversionFromUTC + "),EquipmentID) tbl1  group by Timestamp order by Timestamp ";
			}
		}

		System.out.println(IrradiationQuery);
		List<GraphDataBean> DataTransactionsList = session.createSQLQuery(IrradiationQuery).list();

		return DataTransactionsList;
	}

	public List<GraphDataBean> getIrradiationValuesBySitesWithDate(String siteId, String FromDate, String ToDate,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		// Oracle
		String IrradiationQuery = "Select Timestamp,NVL(Round(NVL(Irradiation,0),2),0) as Value from "
				+ "(Select Timestamp,NVL(Irradiation,0) as Irradiation from tDataTransaction where "
				+ "EquipmentID in (Select e.EquipmentID from mEquipment e,mEquipmentType et, mEquipmentCategory ec "
				+ "where e.EquipmentTypeID=et.EquipmentTypeID and et.CategoryID=ec.CategoryID and ec.EquipmentCategory ='WEATHRSTION' and e.SiteID in ("
				+ siteId + ")) " + "and Timestamp > to_date('" + FromDate
				+ "','DD/MM/YYYY HH24:MI:SS') and Timestamp <=  to_date('" + ToDate + "','DD/MM/YYYY HH24:MI:SS') "
				+ ") tbl1  order by Timestamp ";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			IrradiationQuery = "Select Timestamp,coalesce(Round(coalesce(Irradiation,0),2),0) as Value from "
					+ "(Select Timestamp,coalesce(Irradiation,0) as Irradiation from tDataTransaction where "
					+ "EquipmentID in (Select e.EquipmentID from mEquipment e,mEquipmentType et, mEquipmentCategory ec "
					+ "where e.EquipmentTypeID=et.EquipmentTypeID and et.CategoryID=ec.CategoryID and ec.EquipmentCategory ='WEATHRSTION' and e.SiteID in ("
					+ siteId + ")) " + "and Timestamp > to_timestamp('" + FromDate
					+ "','DD/MM/YYYY HH24:MI:SS') and Timestamp <=  to_timestamp('" + ToDate
					+ "','DD/MM/YYYY HH24:MI:SS') " + ") tbl1  order by Timestamp ";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				IrradiationQuery = "Select Timestamp,coalesce(Round(coalesce(Irradiation,0),2),0) as Value from "
						+ "(Select (Timestamp" + TimeConversionFromUTC
						+ ") as Timestamp,coalesce(Irradiation,0) as Irradiation from tDataTransaction where "
						+ "EquipmentID in (Select e.EquipmentID from mEquipment e,mEquipmentType et, mEquipmentCategory ec "
						+ "where e.EquipmentTypeID=et.EquipmentTypeID and et.CategoryID=ec.CategoryID and ec.EquipmentCategory ='WEATHRSTION' and e.SiteID in ("
						+ siteId + ")) " + "and Timestamp" + TimeConversionFromUTC + " > to_timestamp('" + FromDate
						+ "','DD/MM/YYYY HH24:MI:SS') and Timestamp" + TimeConversionFromUTC + " <=  to_timestamp('"
						+ ToDate + "','DD/MM/YYYY HH24:MI:SS') " + ") tbl1  order by Timestamp ";
			}

		}
		System.out.println(IrradiationQuery);
		System.out.println("Testing  start: " + new Date().toString());
		List<GraphDataBean> DataTransactionsList = session.createSQLQuery(IrradiationQuery).list();
		System.out.println("Testing end : " + new Date().toString());
		
		return DataTransactionsList;
	}

	public List<EnergyPerformanceBean> getEnergyDataByEquipmentIdsWithDate(String equipmentIds, String FromDate,
			String ToDate, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		// Oracle

		String Query = "Select Timestamp,Sum(TotalEnergy) as TotalEnergy,Sum(TodayEnergy) as TodayEnergy "
				+ "from  (Select distinct Timestamp,EquipmentID,NVL((case when TotalEnergy is null then (max(TotalEnergy) over (partition by EquipmentID order by EquipmentID,Timestamp)) else TotalEnergy end),0) as TotalEnergy, "
				+ "NVL((case when TodayEnergy is null then (max(TodayEnergy) over (partition by EquipmentID,Trunc(Timestamp) order by EquipmentID,Timestamp)) else TodayEnergy end),0) as TodayEnergy "
				+ "from  (select tbl1.Timestamp,tbl1.EquipmentID,tbl2.TotalEnergy,tbl2.todayenergy from (select EquipmentID,Timestamp from  (select distinct TRUNC(Timestamp, 'MI') - MOD(TO_CHAR(Timestamp, 'MI'), 5) / (24 * 60) as Timestamp ,'1' as mapcol from tDataTransaction where  EquipmentID in ("
				+ equipmentIds + ") and TotalEnergy is not null and todayenergy is not null and Timestamp <= to_date('"
				+ ToDate + "','DD-MM-YYYY HH24:MI:SS') and Timestamp > to_date('" + FromDate
				+ "','DD-MM-YYYY HH24:MI:SS')) a, "
				+ "(select distinct EquipmentID,'1' as mapcol from mEquipment where  EquipmentID in (" + equipmentIds
				+ ")) b where a.mapcol=b.mapcol union select distinct EquipmentID,to_date('" + FromDate
				+ "','DD-MM-YYYY HH24:MI:SS') as Timestamp  from mEquipment where  EquipmentID in (" + equipmentIds
				+ ")) tbl1 left outer join (select EquipmentID,Timestamp,TotalEnergy,todayenergy from "
				+ "(Select distinct EquipmentID,TRUNC(Timestamp, 'MI') - MOD(TO_CHAR(Timestamp, 'MI'), 5) / (24 * 60) as Timestamp,TotalEnergy,todayenergy from tDataTransaction where ActiveFlag=1 and  EquipmentID in ("
				+ equipmentIds + ")  and Timestamp <= to_date('" + ToDate
				+ "','DD-MM-YYYY HH24:MI:SS') and Timestamp > to_date('" + FromDate + "','DD-MM-YYYY HH24:MI:SS')) c "
				+ "union select distinct EquipmentID,to_date('" + FromDate
				+ "','DD-MM-YYYY HH24:MI:SS') as Timestamp,0 as TotalEnergy,0 as todayenergy  from mEquipment where  EquipmentID in ("
				+ equipmentIds + ")) tbl2 on tbl1.Timestamp=tbl2.Timestamp and tbl1.EquipmentID=tbl2.EquipmentID )) "
				+ "group by Timestamp order by Timestamp";

		// PostgreSQL
		// date_trunc('minute', now() - ((mod(date_part('minute',
		// date_trunc('minute',now())::timestamp -
		// date_TRUNC('hour',now())::timestamp)::integer,5)) * interval '1 minute'))
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {

			/*
			 * Query =
			 * "Select Timestamp,Sum(TotalEnergy) as TotalEnergy,Sum(TodayEnergy) as TodayEnergy "
			 * +
			 * "from  (Select distinct Timestamp,EquipmentID,coalesce((case when TotalEnergy is null then (max(TotalEnergy) over (partition by EquipmentID order by EquipmentID,Timestamp)) else TotalEnergy end),0) as TotalEnergy, "
			 * +
			 * "coalesce((case when TodayEnergy is null then (max(TodayEnergy) over (partition by EquipmentID,date_trunc('day',Timestamp) order by EquipmentID,Timestamp)) else TodayEnergy end),0) as TodayEnergy "
			 * +
			 * "from  (select tbl1.Timestamp,tbl1.EquipmentID,tbl2.TotalEnergy,tbl2.todayenergy from (select EquipmentID,Timestamp from  (select distinct (date_trunc('minute', Timestamp - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp) as timestamp) - cast(date_trunc('hour',Timestamp) as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp ,'1' as mapcol from tDataTransaction where  EquipmentID in ("
			 * + equipmentIds + ") and Timestamp <= to_timestamp('" + ToDate +
			 * "','DD-MM-YYYY HH24:MI:SS') and Timestamp > to_timestamp('" + FromDate +
			 * "','DD-MM-YYYY HH24:MI:SS')) a, " +
			 * "(select distinct EquipmentID,'1' as mapcol from mEquipment where  EquipmentID in ("
			 * + equipmentIds +
			 * ")) b where a.mapcol=b.mapcol ) tbl1 left outer join (select EquipmentID,Timestamp,TotalEnergy,todayenergy from "
			 * +
			 * "(Select distinct EquipmentID,(date_trunc('minute', Timestamp - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp) as timestamp) - cast(date_trunc('hour',Timestamp) as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp,TotalEnergy,todayenergy from tDataTransaction where ActiveFlag=1 and  EquipmentID in ("
			 * + equipmentIds + ")  and Timestamp <= to_timestamp('" + ToDate +
			 * "','DD-MM-YYYY HH24:MI:SS') and Timestamp > to_timestamp('" + FromDate +
			 * "','DD-MM-YYYY HH24:MI:SS')) c " +
			 * ") tbl2 on tbl1.Timestamp=tbl2.Timestamp and tbl1.EquipmentID=tbl2.EquipmentID ) tbla) tblb "
			 * + "group by Timestamp order by Timestamp";
			 */

			String Query1 = "from Site where SiteId in(select SiteID from Equipment where EquipmentId in("
					+ equipmentIds + ")) and TodayEnergyFlag=1";
			String ConditionOnlyForTodayEnergySite = " ";

			List<Site> sitelist = session.createQuery(Query1).list();

			if (sitelist.size() > 0) {
				ConditionOnlyForTodayEnergySite = " and todayenergy is not null ";
			} else {
				ConditionOnlyForTodayEnergySite = " and totalenergy is not null ";
			}

			Query = "Select Timestamp,sum(TotalEnergy) as TotalEnergy,sum(TodayEnergy) as TodayEnergy from "
					+ "(select EquipmentID,Timestamp, "
					+ "first_value(TotalEnergy) over (partition by TotalEnergySeq order by EquipmentID,Timestamp) as TotalEnergy, "
					+ "first_value(TodayEnergy) over (partition by TodayEnergySeq order by EquipmentID,Timestamp) as TodayEnergy from "
					+ "(select EquipmentID,Timestamp,TotalEnergy,TodayEnergy, "
					+ "sum(case when TotalEnergy is null or TotalEnergy = 0  then 0 else 1 end) over (order by EquipmentID,Timestamp) as TotalEnergySeq, "
					+ "sum(case when TodayEnergy is null then 0 else 1 end) over (order by EquipmentID,Timestamp) as TodayEnergySeq "
					+ " from " + "(select tbl1.Timestamp,tbl1.EquipmentID,tbl2.TotalEnergy,tbl2.TodayEnergy from "
					+ "(select EquipmentID,Timestamp from  "
					+ "(select distinct (date_trunc('minute', Timestamp  - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp ) as timestamp) - cast(date_trunc('hour',Timestamp ) as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp ,'1' as mapcol from tDataTransaction where  EquipmentID in ("
					+ equipmentIds + ") " + ConditionOnlyForTodayEnergySite + "  and Timestamp  <= to_timestamp('"
					+ ToDate + "','DD-MM-YYYY HH24:MI:SS') and Timestamp  > to_timestamp('" + FromDate
					+ "','DD-MM-YYYY HH24:MI:SS')) a, (select distinct EquipmentID,'1' as mapcol from mEquipment where  EquipmentID in ("
					+ equipmentIds + ")) b where a.mapcol=b.mapcol ) tbl1 " + "left outer join " + "("
					+ "select EquipmentID,Timestamp,TotalEnergy,todayenergy from " + "("
					+ "select distinct EquipmentID,(date_trunc('minute', Timestamp  - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp ) as timestamp) - cast(date_trunc('hour',Timestamp ) as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp,TotalEnergy,todayenergy from tDataTransaction where ActiveFlag=1 and  EquipmentID in ("
					+ equipmentIds + ") " + ConditionOnlyForTodayEnergySite + "  and Timestamp  <= to_timestamp('"
					+ ToDate + "','DD-MM-YYYY HH24:MI:SS') and Timestamp  > to_timestamp('" + FromDate
					+ "','DD-MM-YYYY HH24:MI:SS') " + ") c " + ") tbl2 "
					+ "on tbl1.Timestamp=tbl2.Timestamp and tbl1.EquipmentID=tbl2.EquipmentID " + "Union "
					+ "select distinct (to_timestamp('" + FromDate
					+ "','DD-MM-YYYY HH24:MI:SS')-60 * interval '1 minute') as Timestamp,EquipmentID,TotalEnergy, 0 as TodayEnergy from tDataTransaction where (EquipmentID,Timestamp) in "
					+ "(select distinct EquipmentID,Max(Timestamp ) as Timestamp from "
					+ "tDataTransaction where ActiveFlag=1 and TotalEnergy<>0 and EquipmentID in (" + equipmentIds
					+ ") " + ConditionOnlyForTodayEnergySite + " and "
					+ "date_trunc('day',Timestamp ) <= date_trunc('day',to_timestamp('" + FromDate
					+ "','DD-MM-YYYY HH24:MI:SS'))-(1 * interval '1 day') "
					+ "Group by EquipmentID)) tblrset ) as tblpreview) as tblfinal where tblfinal.Timestamp >= to_timestamp('"
					+ FromDate + "','DD-MM-YYYY HH24:MI:SS') " + "Group by Timestamp Order by Timestamp";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				/*
				 * Query =
				 * "Select Timestamp,Sum(TotalEnergy) as TotalEnergy,Sum(TodayEnergy) as TodayEnergy "
				 * +
				 * "from  (Select distinct Timestamp,EquipmentID,coalesce((case when TotalEnergy is null then (max(TotalEnergy) over (partition by EquipmentID order by EquipmentID,Timestamp)) else TotalEnergy end),0) as TotalEnergy, "
				 * +
				 * "coalesce((case when TodayEnergy is null then (max(TodayEnergy) over (partition by EquipmentID,date_trunc('day',Timestamp) order by EquipmentID,Timestamp)) else TodayEnergy end),0) as TodayEnergy "
				 * +
				 * "from  (select tbl1.Timestamp,tbl1.EquipmentID,tbl2.TotalEnergy,tbl2.todayenergy from (select EquipmentID,Timestamp from  (select distinct (date_trunc('minute', Timestamp"
				 * + TimeConversionFromUTC +
				 * " - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp" +
				 * TimeConversionFromUTC + ") as timestamp) - cast(date_trunc('hour',Timestamp"
				 * + TimeConversionFromUTC +
				 * ") as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp ,'1' as mapcol from tDataTransaction where  EquipmentID in ("
				 * + equipmentIds + ") and Timestamp" + TimeConversionFromUTC +
				 * " <= to_timestamp('" + ToDate + "','DD-MM-YYYY HH24:MI:SS') and Timestamp" +
				 * TimeConversionFromUTC + " > to_timestamp('" + FromDate +
				 * "','DD-MM-YYYY HH24:MI:SS')) a, " +
				 * "(select distinct EquipmentID,'1' as mapcol from mEquipment where  EquipmentID in ("
				 * + equipmentIds +
				 * ")) b where a.mapcol=b.mapcol) tbl1 left outer join (select EquipmentID,Timestamp,TotalEnergy,todayenergy from "
				 * + "(Select distinct EquipmentID,(date_trunc('minute', Timestamp" +
				 * TimeConversionFromUTC +
				 * " - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp" +
				 * TimeConversionFromUTC + ") as timestamp) - cast(date_trunc('hour',Timestamp"
				 * + TimeConversionFromUTC +
				 * ") as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp,TotalEnergy,todayenergy from tDataTransaction where ActiveFlag=1 and  EquipmentID in ("
				 * + equipmentIds + ")  and Timestamp" + TimeConversionFromUTC +
				 * " <= to_timestamp('" + ToDate + "','DD-MM-YYYY HH24:MI:SS') and Timestamp" +
				 * TimeConversionFromUTC + " > to_timestamp('" + FromDate +
				 * "','DD-MM-YYYY HH24:MI:SS')) c " +
				 * ") tbl2 on tbl1.Timestamp=tbl2.Timestamp and tbl1.EquipmentID=tbl2.EquipmentID ) tbla) tblb "
				 * + "group by Timestamp order by Timestamp";
				 */

				Query = "Select Timestamp,sum(TotalEnergy) as TotalEnergy,sum(TodayEnergy) as TodayEnergy from "
						+ "(select EquipmentID,Timestamp, "
						+ "first_value(TotalEnergy) over (partition by TotalEnergySeq order by EquipmentID,Timestamp) as TotalEnergy, "
						+ "first_value(TodayEnergy) over (partition by TodayEnergySeq order by EquipmentID,Timestamp) as TodayEnergy from "
						+ "(select EquipmentID,Timestamp,TotalEnergy,TodayEnergy, "
						+ "sum(case when TotalEnergy is null or TotalEnergy = 0  then 0 else 1 end) over (order by EquipmentID,Timestamp) as TotalEnergySeq, "
						+ "sum(case when TodayEnergy is null then 0 else 1 end) over (order by EquipmentID,Timestamp) as TodayEnergySeq "
						+ " from " + "(select tbl1.Timestamp,tbl1.EquipmentID,tbl2.TotalEnergy,tbl2.TodayEnergy from "
						+ "(select EquipmentID,Timestamp from  " + "(select distinct (date_trunc('minute', Timestamp"
						+ TimeConversionFromUTC
						+ "  - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp"
						+ TimeConversionFromUTC + " ) as timestamp) - cast(date_trunc('hour',Timestamp"
						+ TimeConversionFromUTC
						+ " ) as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp ,'1' as mapcol from tDataTransaction where  EquipmentID in ("
						+ equipmentIds + ")  " + ConditionOnlyForTodayEnergySite + "  and Timestamp"
						+ TimeConversionFromUTC + "  <= to_timestamp('" + ToDate
						+ "','DD-MM-YYYY HH24:MI:SS') and Timestamp" + TimeConversionFromUTC + "  > to_timestamp('"
						+ FromDate
						+ "','DD-MM-YYYY HH24:MI:SS')) a, (select distinct EquipmentID,'1' as mapcol from mEquipment where  EquipmentID in ("
						+ equipmentIds + ")) b where a.mapcol=b.mapcol ) tbl1 " + "left outer join " + "("
						+ "select EquipmentID,Timestamp,TotalEnergy,todayenergy from " + "("
						+ "select distinct EquipmentID,(date_trunc('minute', Timestamp" + TimeConversionFromUTC
						+ "  - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp"
						+ TimeConversionFromUTC + " ) as timestamp) - cast(date_trunc('hour',Timestamp"
						+ TimeConversionFromUTC
						+ " ) as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp,TotalEnergy,todayenergy from tDataTransaction where ActiveFlag=1 and  EquipmentID in ("
						+ equipmentIds + ") " + ConditionOnlyForTodayEnergySite + " and Timestamp"
						+ TimeConversionFromUTC + "  <= to_timestamp('" + ToDate
						+ "','DD-MM-YYYY HH24:MI:SS') and Timestamp" + TimeConversionFromUTC + "  > to_timestamp('"
						+ FromDate + "','DD-MM-YYYY HH24:MI:SS') " + ") c " + ") tbl2 "
						+ "on tbl1.Timestamp=tbl2.Timestamp and tbl1.EquipmentID=tbl2.EquipmentID " + "Union "
						+ "select distinct (to_timestamp('" + FromDate
						+ "','DD-MM-YYYY HH24:MI:SS')-60 * interval '1 minute') as Timestamp,EquipmentID,TotalEnergy, 0 as TodayEnergy from tDataTransaction where (EquipmentID,Timestamp"
						+ TimeConversionFromUTC + ") in " + "(select distinct EquipmentID,Max(Timestamp"
						+ TimeConversionFromUTC + " ) as Timestamp from "
						+ "tDataTransaction where ActiveFlag=1 and TotalEnergy<>0 and EquipmentID in (" + equipmentIds
						+ ") " + ConditionOnlyForTodayEnergySite + "  and " + "date_trunc('day',Timestamp"
						+ TimeConversionFromUTC + " ) <= date_trunc('day',to_timestamp('" + FromDate
						+ "','DD-MM-YYYY HH24:MI:SS'))-(1 * interval '1 day') "
						+ "Group by EquipmentID)) tblrset ) as tblpreview) as tblfinal where tblfinal.Timestamp >= to_timestamp('"
						+ FromDate + "','DD-MM-YYYY HH24:MI:SS') " + "Group by Timestamp Order by Timestamp";

			}

		}
		System.out.println("Query : " + Query);

		List<EnergyPerformanceBean> DataTransactionsList = session.createSQLQuery(Query).list();

		return DataTransactionsList;
	}

	public String getYesterdayEnergyByEquipmentIdsWithDate(String equipmentIds, String FromDate, String ToDate,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		// Oracle
		String Query = "Select nvl(Sum(nvl(YesterdayEnergy,0)),0) as YesterdayEnergy from (Select distinct EquipmentID,Max(TotalEnergy) as YesterdayEnergy from tDataTransaction where  (EquipmentId,Timestamp) in (Select distinct EquipmentID,Max(Timestamp) as Timestamp from tDataTransaction where ActiveFlag=1 and TotalEnergy <>0 and  EquipmentID in ("
				+ equipmentIds + ")  and trunc(Timestamp) <= trunc(to_date('" + FromDate
				+ "','DD/MM/YYYY HH24:MI:SS'))-1 Group by EquipmentID) Group by EquipmentID)";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select coalesce(Sum(coalesce(YesterdayEnergy,0)),0) as YesterdayEnergy from (Select distinct EquipmentID,Max(TotalEnergy) as YesterdayEnergy from tDataTransaction where (EquipmentId,Timestamp) in (Select distinct EquipmentID,Max(Timestamp) as Timestamp from tDataTransaction where ActiveFlag=1 and TotalEnergy <>0 and EquipmentID in ("
					+ equipmentIds + ")  and date_trunc('day',Timestamp) <= date_trunc('day',to_timestamp('" + FromDate
					+ "','DD/MM/YYYY HH24:MI:SS'))-(1 * interval '1 day') Group by EquipmentID) Group by EquipmentID) tbla";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "Select coalesce(Sum(coalesce(YesterdayEnergy,0)),0) as YesterdayEnergy from (Select distinct EquipmentID,Max(TotalEnergy) as YesterdayEnergy from tDataTransaction where (EquipmentId,Timestamp"
						+ TimeConversionFromUTC + ") in (Select distinct EquipmentID,Max(Timestamp"
						+ TimeConversionFromUTC
						+ ") as Timestamp from tDataTransaction where ActiveFlag=1 and TotalEnergy <>0 and EquipmentID in ("
						+ equipmentIds + ")  and date_trunc('day',Timestamp" + TimeConversionFromUTC
						+ ") <= date_trunc('day',to_timestamp('" + FromDate
						+ "','DD/MM/YYYY HH24:MI:SS'))-(1 * interval '1 day') Group by EquipmentID) Group by EquipmentID) tbla";

			}
		}

		System.out.println(Query);

		String ScalarResult = (String) session.createSQLQuery(Query).list().get(0).toString();

		return ScalarResult;
	}

	public String getTodayFlagStatus(String equipmentIds, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		// Oracle
		String Query = "Select nvl(min(nvl(TodayEnergyFlag,0)),0) as TodayEnergyFlag  from mSite Where SiteId in (Select distinct SiteID from mEquipment where EquipmentID in ("
				+ equipmentIds + "))";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select coalesce(min(coalesce(TodayEnergyFlag,0)),0) as TodayEnergyFlag  from mSite Where SiteId in (Select distinct SiteID from mEquipment where EquipmentID in ("
					+ equipmentIds + "))";

		}
		System.out.println(Query);
		String ScalarResult = (String) session.createSQLQuery(Query).list().get(0).toString();

		return ScalarResult;
	}

	public List<EnergyPerformanceBean> getEnergyDataBySiteIdsWithDate(String siteIds, String FromDate, String ToDate,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		// Oracle

		
		String Query = "Select Timestamp,Sum(TotalEnergy) as TotalEnergy,Sum(TodayEnergy) as TodayEnergy "
				+ "from  (Select distinct Timestamp,EquipmentID,NVL((case when TotalEnergy is null then (max(TotalEnergy) over (partition by EquipmentID order by EquipmentID,Timestamp)) else TotalEnergy end),0) as TotalEnergy, "
				+ "NVL((case when TodayEnergy is null then (max(TodayEnergy) over (partition by EquipmentID,Trunc(Timestamp) order by EquipmentID,Timestamp)) else TodayEnergy end),0) as TodayEnergy "
				+ "from  (select tbl1.Timestamp,tbl1.EquipmentID,tbl2.TotalEnergy,tbl2.todayenergy from (select EquipmentID,Timestamp from  (select distinct TRUNC(Timestamp, 'MI') - MOD(TO_CHAR(Timestamp, 'MI'), 5) / (24 * 60) as Timestamp ,'1' as mapcol from tDataTransaction where  SiteID in ("
				+ siteIds + ") and Timestamp <= to_date('" + ToDate
				+ "','DD-MM-YYYY HH24:MI:SS') and Timestamp > to_date('" + FromDate + "','DD-MM-YYYY HH24:MI:SS')) a, "
				+ "(select distinct EquipmentID,'1' as mapcol from mEquipment where  SiteID in (" + siteIds
				+ ")) b where a.mapcol=b.mapcol ) tbl1 left outer join (select EquipmentID,Timestamp,TotalEnergy,todayenergy from "
				+ "(Select distinct EquipmentID,TRUNC(Timestamp, 'MI') - MOD(TO_CHAR(Timestamp, 'MI'), 5) / (24 * 60) as Timestamp,TotalEnergy,todayenergy from tDataTransaction where ActiveFlag=1 and  SiteID in ("
				+ siteIds + ")  and Timestamp <= to_date('" + ToDate
				+ "','DD-MM-YYYY HH24:MI:SS') and Timestamp > to_date('" + FromDate + "','DD-MM-YYYY HH24:MI:SS')) c "
				+ ") tbl2 on tbl1.Timestamp=tbl2.Timestamp and tbl1.EquipmentID=tbl2.EquipmentID )) "
				+ "group by Timestamp order by Timestamp";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {

			/*
			 * Query =
			 * "Select Timestamp,Sum(TotalEnergy) as TotalEnergy,Sum(TodayEnergy) as TodayEnergy "
			 * +
			 * "from  (Select distinct Timestamp,EquipmentID,coalesce((case when TotalEnergy is null then (max(TotalEnergy) over (partition by EquipmentID order by EquipmentID,Timestamp)) else TotalEnergy end),0) as TotalEnergy, "
			 * +
			 * "coalesce((case when TodayEnergy is null then (max(TodayEnergy) over (partition by EquipmentID,date_trunc('day',Timestamp) order by EquipmentID,Timestamp)) else TodayEnergy end),0) as TodayEnergy "
			 * +
			 * "from  (select tbl1.Timestamp,tbl1.EquipmentID,tbl2.TotalEnergy,tbl2.todayenergy from (select EquipmentID,Timestamp from  (select distinct (date_trunc('minute', Timestamp - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp) as timestamp) - cast(date_trunc('hour',Timestamp) as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp ,'1' as mapcol from tDataTransaction where  SiteID in ("
			 * + siteIds + ") and Timestamp <= to_timestamp('" + ToDate +
			 * "','DD-MM-YYYY HH24:MI:SS') and Timestamp > to_timestamp('" + FromDate +
			 * "','DD-MM-YYYY HH24:MI:SS')) a, " +
			 * "(select distinct EquipmentID,'1' as mapcol from mEquipment where  SiteID in ("
			 * + siteIds +
			 * ")) b where a.mapcol=b.mapcol ) tbl1 left outer join (select EquipmentID,Timestamp,TotalEnergy,todayenergy from "
			 * +
			 * "(Select distinct EquipmentID,(date_trunc('minute', Timestamp - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp) as timestamp) - cast(date_trunc('hour',Timestamp) as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp,TotalEnergy,todayenergy from tDataTransaction where ActiveFlag=1 and  SiteID in ("
			 * + siteIds + ")  and Timestamp <= to_timestamp('" + ToDate +
			 * "','DD-MM-YYYY HH24:MI:SS') and Timestamp > to_timestamp('" + FromDate +
			 * "','DD-MM-YYYY HH24:MI:SS')) c " +
			 * ") tbl2 on tbl1.Timestamp=tbl2.Timestamp and tbl1.EquipmentID=tbl2.EquipmentID ) tbla) tblb "
			 * + "group by Timestamp order by Timestamp";
			 */
			
//			String Query2 = "from Site where SiteId =" + siteIds ;
//			Site site=(Site) session.createQuery(Query2).getSingleResult();
//			String transTable="tDataTransaction";
//			if(site!=null && site.getOperationMode()==2) {
//				transTable="tDataTransactionhistory";
//				System.out.println(site.getOperationMode());
//				
//			}
			
			
			String Query1 = "from Site where SiteId in(" + siteIds + ") and TodayEnergyFlag=1";
			String ConditionOnlyForTodayEnergySite = " ";

			List<Site> sitelist = session.createQuery(Query1).list();

			if (sitelist.size() > 0) {
				ConditionOnlyForTodayEnergySite = " and todayenergy is not null ";
			} else {
				ConditionOnlyForTodayEnergySite = " and totalenergy is not null ";
			}

			Query = "Select Timestamp,sum(TotalEnergy) as TotalEnergy,sum(TodayEnergy) as TodayEnergy from "
					+ "(select EquipmentID,Timestamp, "
					+ "first_value(TotalEnergy) over (partition by TotalEnergySeq order by EquipmentID,Timestamp) as TotalEnergy, "
					+ "first_value(TodayEnergy) over (partition by TodayEnergySeq order by EquipmentID,Timestamp) as TodayEnergy from "
					+ "(select EquipmentID,Timestamp,TotalEnergy,TodayEnergy, "
					+ "sum(case when TotalEnergy is null or TotalEnergy = 0  then 0 else 1 end) over (order by EquipmentID,Timestamp) as TotalEnergySeq, "
					+ "sum(case when TodayEnergy is null then 0 else 1 end) over (order by EquipmentID,Timestamp) as TodayEnergySeq "
					+ " from " + "(select tbl1.Timestamp,tbl1.EquipmentID,tbl2.TotalEnergy,tbl2.TodayEnergy from "
					+ "(select EquipmentID,Timestamp from  "
					+ "(select distinct (date_trunc('minute', Timestamp  - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp ) as timestamp) - cast(date_trunc('hour',Timestamp ) as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp ,'1' as mapcol from tDataTransaction where  SiteID in ("
					+ siteIds + ") " + ConditionOnlyForTodayEnergySite + " and Timestamp  <= to_timestamp('" + ToDate
					+ "','DD-MM-YYYY HH24:MI:SS') and Timestamp  > to_timestamp('" + FromDate
					+ "','DD-MM-YYYY HH24:MI:SS')) a, (select distinct EquipmentID,'1' as mapcol from mEquipment where  SiteID in ("
					+ siteIds + ")) b where a.mapcol=b.mapcol ) tbl1 " + "left outer join " + "("
					+ "select EquipmentID,Timestamp,TotalEnergy,todayenergy from " + "("
					+ "select distinct EquipmentID,(date_trunc('minute', Timestamp  - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp ) as timestamp) - cast(date_trunc('hour',Timestamp ) as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp,TotalEnergy,todayenergy from tDataTransaction where ActiveFlag=1 and  SiteID in ("
					+ siteIds + ")  " + ConditionOnlyForTodayEnergySite + " and Timestamp  <= to_timestamp('" + ToDate
					+ "','DD-MM-YYYY HH24:MI:SS') and Timestamp  > to_timestamp('" + FromDate
					+ "','DD-MM-YYYY HH24:MI:SS') " + ") c " + ") tbl2 "
					+ "on tbl1.Timestamp=tbl2.Timestamp and tbl1.EquipmentID=tbl2.EquipmentID " + "Union "
					+ "select distinct (to_timestamp('" + FromDate
					+ "','DD-MM-YYYY HH24:MI:SS')-60 * interval '1 minute') as Timestamp,EquipmentID,TotalEnergy, 0 as TodayEnergy from tDataTransaction where (EquipmentID,Timestamp) in "
					+ "(select distinct EquipmentID,Max(Timestamp ) as Timestamp from "
					+ "tDataTransaction where ActiveFlag=1 " + ConditionOnlyForTodayEnergySite
					+ " and TotalEnergy<>0 and EquipmentID in (Select EquipmentID from mEquipment where SiteID in ("
					+ siteIds + "))  and " + "date_trunc('day',Timestamp ) <= date_trunc('day',to_timestamp('"
					+ FromDate + "','DD-MM-YYYY HH24:MI:SS'))-(1 * interval '1 day') "
					+ "Group by EquipmentID)) tblrset ) as tblpreview) as tblfinal where tblfinal.Timestamp >= to_timestamp('"
					+ FromDate + "','DD-MM-YYYY HH24:MI:SS') " + "Group by Timestamp Order by Timestamp";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				/*
				 * Query =
				 * "Select Timestamp,Sum(TotalEnergy) as TotalEnergy,Sum(TodayEnergy) as TodayEnergy "
				 * +
				 * "from  (Select distinct Timestamp,EquipmentID,coalesce((case when TotalEnergy is null then (max(TotalEnergy) over (partition by EquipmentID order by EquipmentID,Timestamp)) else TotalEnergy end),0) as TotalEnergy, "
				 * +
				 * "coalesce((case when TodayEnergy is null then (max(TodayEnergy) over (partition by EquipmentID,date_trunc('day',Timestamp) order by EquipmentID,Timestamp)) else TodayEnergy end),0) as TodayEnergy "
				 * +
				 * "from  (select tbl1.Timestamp,tbl1.EquipmentID,tbl2.TotalEnergy,tbl2.todayenergy from (select EquipmentID,Timestamp from  (select distinct (date_trunc('minute', Timestamp"
				 * + TimeConversionFromUTC +
				 * " - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp" +
				 * TimeConversionFromUTC + ") as timestamp) - cast(date_trunc('hour',Timestamp"
				 * + TimeConversionFromUTC +
				 * ") as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp ,'1' as mapcol from tDataTransaction where  SiteID in ("
				 * + siteIds + ") and Timestamp" + TimeConversionFromUTC + " <= to_timestamp('"
				 * + ToDate + "','DD-MM-YYYY HH24:MI:SS') and Timestamp" + TimeConversionFromUTC
				 * + " > to_timestamp('" + FromDate + "','DD-MM-YYYY HH24:MI:SS')) a, " +
				 * "(select distinct EquipmentID,'1' as mapcol from mEquipment where  SiteID in ("
				 * + siteIds +
				 * ")) b where a.mapcol=b.mapcol ) tbl1 left outer join (select EquipmentID,Timestamp,TotalEnergy,todayenergy from "
				 * + "(Select distinct EquipmentID,(date_trunc('minute', Timestamp" +
				 * TimeConversionFromUTC +
				 * " - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp" +
				 * TimeConversionFromUTC + ") as timestamp) - cast(date_trunc('hour',Timestamp"
				 * + TimeConversionFromUTC +
				 * ") as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp,TotalEnergy,todayenergy from tDataTransaction where ActiveFlag=1 and  SiteID in ("
				 * + siteIds + ")  and Timestamp" + TimeConversionFromUTC + " <= to_timestamp('"
				 * + ToDate + "','DD-MM-YYYY HH24:MI:SS') and Timestamp" + TimeConversionFromUTC
				 * + " > to_timestamp('" + FromDate + "','DD-MM-YYYY HH24:MI:SS')) c " +
				 * ") tbl2 on tbl1.Timestamp=tbl2.Timestamp and tbl1.EquipmentID=tbl2.EquipmentID ) tbla) tblb "
				 * + "group by Timestamp order by Timestamp";
				 */

				Query = "Select Timestamp,sum(TotalEnergy) as TotalEnergy,sum(TodayEnergy) as TodayEnergy from "
						+ "(select EquipmentID,Timestamp, "
						+ "first_value(TotalEnergy) over (partition by TotalEnergySeq order by EquipmentID,Timestamp) as TotalEnergy, "
						+ "first_value(TodayEnergy) over (partition by TodayEnergySeq order by EquipmentID,Timestamp) as TodayEnergy from "
						+ "(select EquipmentID,Timestamp,TotalEnergy,TodayEnergy, "
						+ "sum(case when TotalEnergy is null or TotalEnergy = 0  then 0 else 1 end) over (order by EquipmentID,Timestamp) as TotalEnergySeq, "
						+ "sum(case when TodayEnergy is null then 0 else 1 end) over (order by EquipmentID,Timestamp) as TodayEnergySeq "
						+ " from " + "(select tbl1.Timestamp,tbl1.EquipmentID,tbl2.TotalEnergy,tbl2.TodayEnergy from "
						+ "(select EquipmentID,Timestamp from  " + "(select distinct (date_trunc('minute', Timestamp"
						+ TimeConversionFromUTC
						+ "  - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp"
						+ TimeConversionFromUTC + " ) as timestamp) - cast(date_trunc('hour',Timestamp"
						+ TimeConversionFromUTC
						+ " ) as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp ,'1' as mapcol from tDataTransaction where  SiteID in ("
						+ siteIds + ") " + ConditionOnlyForTodayEnergySite + " and Timestamp" + TimeConversionFromUTC
						+ "  <= to_timestamp('" + ToDate + "','DD-MM-YYYY HH24:MI:SS') and Timestamp"
						+ TimeConversionFromUTC + "  > to_timestamp('" + FromDate
						+ "','DD-MM-YYYY HH24:MI:SS')) a, (select distinct EquipmentID,'1' as mapcol from mEquipment where  SiteID in ("
						+ siteIds + ")) b where a.mapcol=b.mapcol ) tbl1 " + "left outer join " + "("
						+ "select EquipmentID,Timestamp,TotalEnergy,todayenergy from " + "("
						+ "select distinct EquipmentID,(date_trunc('minute', Timestamp" + TimeConversionFromUTC
						+ "  - ((mod(cast(date_part('minute', cast(date_trunc('minute',Timestamp"
						+ TimeConversionFromUTC + " ) as timestamp) - cast(date_trunc('hour',Timestamp"
						+ TimeConversionFromUTC
						+ " ) as timestamp)) as integer),5)) * interval '1 minute'))) as Timestamp,TotalEnergy,todayenergy from tDataTransaction where ActiveFlag=1 and  SiteID in ("
						+ siteIds + ") " + ConditionOnlyForTodayEnergySite + " and Timestamp" + TimeConversionFromUTC
						+ "  <= to_timestamp('" + ToDate + "','DD-MM-YYYY HH24:MI:SS') and Timestamp"
						+ TimeConversionFromUTC + "  > to_timestamp('" + FromDate + "','DD-MM-YYYY HH24:MI:SS') "
						+ ") c " + ") tbl2 " + "on tbl1.Timestamp=tbl2.Timestamp and tbl1.EquipmentID=tbl2.EquipmentID "
						+ "Union " + "select distinct (to_timestamp('" + FromDate
						+ "','DD-MM-YYYY HH24:MI:SS')-60 * interval '1 minute') as Timestamp,EquipmentID,TotalEnergy, 0 as TodayEnergy from tDataTransaction where (EquipmentID,Timestamp"
						+ TimeConversionFromUTC + ") in " + "(select distinct EquipmentID,Max(Timestamp"
						+ TimeConversionFromUTC + " ) as Timestamp from " + "tDataTransaction where ActiveFlag=1 "
						+ ConditionOnlyForTodayEnergySite
						+ " and TotalEnergy<>0 and EquipmentID in (Select EquipmentID from mEquipment where SiteID in ("
						+ siteIds + "))  and " + "date_trunc('day',Timestamp" + TimeConversionFromUTC
						+ " ) <= date_trunc('day',to_timestamp('" + FromDate
						+ "','DD-MM-YYYY HH24:MI:SS'))-(1 * interval '1 day') "
						+ "Group by EquipmentID)) tblrset ) as tblpreview) as tblfinal where tblfinal.Timestamp >= to_timestamp('"
						+ FromDate + "','DD-MM-YYYY HH24:MI:SS') " + "Group by Timestamp Order by Timestamp";

			}
		}

		System.out.println("Query : " + Query);

		System.out.println("Testing start : " + new Date().toString());
		List<EnergyPerformanceBean> DataTransactionsList = session.createSQLQuery(Query).list();
		System.out.println("Testing end: " + new Date().toString());
		
		return DataTransactionsList;
	}

	public String getYesterdayEnergyBySiteIdsWithDate(String siteId, String FromDate, String ToDate,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		// Oracle
		String Query = "Select nvl(Sum(nvl(YesterdayEnergy,0)),0) as YesterdayEnergy from (Select distinct EquipmentID,Max(TotalEnergy) as YesterdayEnergy from tDataTransaction where (EquipmentId,Timestamp) in (Select distinct EquipmentID,Max(Timestamp) as Timestamp from tDataTransaction where ActiveFlag=1 and TotalEnergy <>0 and EquipmentID in (Select EquipmentID from mEquipment where SiteID in ("
				+ siteId + "))  and trunc(Timestamp) <= trunc(to_date('" + FromDate
				+ "','DD/MM/YYYY HH24:MI:SS'))-1 Group by EquipmentID) Group by EquipmentID)";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select coalesce(Sum(coalesce(YesterdayEnergy,0)),0) as YesterdayEnergy from (Select distinct EquipmentID,Max(TotalEnergy) as YesterdayEnergy from tDataTransaction where (EquipmentId,Timestamp) in (Select distinct EquipmentID,Max(Timestamp) as Timestamp from tDataTransaction where ActiveFlag=1 and TotalEnergy <>0 and EquipmentID in (Select EquipmentID from mEquipment where SiteID in ("
					+ siteId + "))  and date_trunc('day',Timestamp) <= date_trunc('day',to_timestamp('" + FromDate
					+ "','DD/MM/YYYY HH24:MI:SS'))-(1 * interval '1 day') Group by EquipmentID) Group by EquipmentID) tbla";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "Select coalesce(Sum(coalesce(YesterdayEnergy,0)),0) as YesterdayEnergy from (Select distinct EquipmentID,Max(TotalEnergy) as YesterdayEnergy from tDataTransaction where (EquipmentId,Timestamp"
						+ TimeConversionFromUTC + ") in (Select distinct EquipmentID,Max(Timestamp"
						+ TimeConversionFromUTC
						+ ") as Timestamp from tDataTransaction where ActiveFlag=1 and TotalEnergy <>0 and EquipmentID in (Select EquipmentID from mEquipment where SiteID in ("
						+ siteId + "))  and date_trunc('day',Timestamp" + TimeConversionFromUTC
						+ ") <= date_trunc('day',to_timestamp('" + FromDate
						+ "','DD/MM/YYYY HH24:MI:SS'))-(1 * interval '1 day') Group by EquipmentID) Group by EquipmentID) tbla";

			}
		}
		System.out.println(Query);

		String ScalarResult = (String) session.createSQLQuery(Query).list().get(0).toString();

		return ScalarResult;
	}

	public List<GraphDataBean> getParameterValuesBySiteIdsWithDate(String siteIds, String Parameter, String FromDate,
			String ToDate, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();

		// Oracle
		String Query = "Select Timestamp,SUM(NVL(" + Parameter + ",0)) as Value from DataTransaction where SiteID in ("
				+ siteIds + ")  and Timestamp <=  to_date('" + ToDate
				+ "','DD/MM/YYYY HH24:MI:SS') and  Timestamp > to_date('" + FromDate
				+ "','DD/MM/YYYY HH24:MI:SS') group by TimeStamp order by TimeStamp";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select Timestamp,SUM(coalesce(" + Parameter
					+ ",0)) as Value from DataTransaction where SiteID in (" + siteIds
					+ ")  and Timestamp <=  to_timestamp('" + ToDate
					+ "','DD/MM/YYYY HH24:MI:SS') and  Timestamp > to_timestamp('" + FromDate
					+ "','DD/MM/YYYY HH24:MI:SS') group by TimeStamp order by TimeStamp";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "Select (Timestamp" + TimeConversionFromUTC + ") as Timestamp,SUM(coalesce(" + Parameter
						+ ",0)) as Value from DataTransaction where SiteID in (" + siteIds + ")  and Timestamp"
						+ TimeConversionFromUTC + " <=  to_timestamp('" + ToDate
						+ "','DD/MM/YYYY HH24:MI:SS') and  Timestamp" + TimeConversionFromUTC + " > to_timestamp('"
						+ FromDate + "','DD/MM/YYYY HH24:MI:SS') group by TimeStamp order by TimeStamp";

			}
		}
		System.out.println(Query);
		List<GraphDataBean> DataTransactionsList = session.createQuery(Query).list();

		return DataTransactionsList;
	}

	// @Override
	public DataTransaction getDataTransactionById(int id) {
		Session session = sessionFactory.getCurrentSession();
		DataTransaction dataTransaction = (DataTransaction) session.get(DataTransaction.class, new Integer(id));
		return dataTransaction;
	}

	public DataTransaction getDataTransactionByMax(String MaxColumnName) {
		Session session = sessionFactory.getCurrentSession();

		// Common for Oracle & PostgreSQL
		String Query = "from DataTransaction Order By " + MaxColumnName + " desc";
		DataTransaction dataTransaction = (DataTransaction) session.createQuery(Query).setMaxResults(1)
				.getSingleResult();
		return dataTransaction;
	}

	// @Override
	public void removeDataTransaction(int id) {
		Session session = sessionFactory.getCurrentSession();
		DataTransaction dataTransaction = (DataTransaction) session.get(DataTransaction.class, new Integer(id));

		// De-activate the flag
		dataTransaction.setActiveFlag(0);

		if (null != dataTransaction) {
			// session.delete(dataTransaction);

			session.update(dataTransaction);
		}
	}

	public List<EquipmentWithoutParameterListBean> getEquipmentList(String siteId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		// String Query = "Select e.SiteID as SiteID,e.EquipmentID as
		// EquipmentID,e.EquipmentCode as EquipmentCode,(case when
		// ec.EquipmentCategory='WEATHRSTION' then 'Sensor' else 'Others' end end) as
		// EquipmentName,e.CustomerNaming as EquipmentReference from mEquipment
		// e,mEquipmentType et,mEquipmentCategory ec where e.ActiveFlag=1 and
		// e.SiteID='" + siteId + "' and e.EquipmentTypeID=et.EquipmentTypeID and
		// et.CategoryID=ec.CategoryID order by EquipmentName,e.CustomerNaming";
// Sarath
		String Query = "Select e.SiteID as SiteID,e.EquipmentID as EquipmentID,e.EquipmentCode as EquipmentCode,(case when ec.EquipmentCategory='WEATHRSTION' then 'Sensor' else case when ec.EquipmentCategory in('STRINGINVRTR','CENTRLINVRTR') then 'Inverter' else case when ec.EquipmentCategory in('Primary Energy Meter','Secondary Energy Meter') then 'Energymeter' else case when ec.EquipmentCategory='STRINGCOMBINER' then 'SMB' else case when ec.EquipmentCategory='Tracker Sensor' then 'Tracker' else  'Others'  end end end end  end) as EquipmentName,e.CustomerNaming as EquipmentReference from mEquipment e,mEquipmentType et,mEquipmentCategory ec where e.ActiveFlag=1 and e.SiteID='"
				+ siteId
				+ "' and e.EquipmentTypeID=et.EquipmentTypeID and et.CategoryID=ec.CategoryID order by EquipmentName,e.CustomerNaming";

		System.out.println("Query : " + Query);
		List<EquipmentWithoutParameterListBean> DataTransactionsList = session.createSQLQuery(Query).list();

		return DataTransactionsList;
	}

	public List<EquipmentWithParameterListBean> getEquipmentWithParameterList(String siteId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "Select e.SiteId as SiteID,e.EquipmentId as EquipmentID,ec.EquipmentCategory as EquipmentCategory,e.CustomerNaming as CustomerNaming,StandardId,StandardParameterName,ParameterDescription,StandardParameterUOM from mEquipment e, mEquipmentType et, mEquipmentCategory ec,mSite s,mParameterStandards ps,mParameterIntegratedStandards pis where e.SiteId='"
				+ siteId
				+ "' and et.EquipmentTypeID=e.EquipmentTypeID and ec.CategoryID=et.CategoryID and  s.SiteId=e.SiteID and pis.DataLoggerID=(case when ec.EquipmentCategory='WEATHRSTION' then s.DataLoggerID_Sensor else s.DataLoggerID end) and ps.StandardParameterName=pis.StandardName and pis.StandardName is not null and ps.CoreParameterFlag=1 and e.ActiveFlag=1 and et.ActiveFlag=1 and ec.ActiveFlag=1 and s.ActiveFlag=1 and ps.ActiveFlag=1 and pis.ActiveFlag=1 order by e.SiteId,e.EquipmentId,e.CustomerNaming,StandardId";

		System.out.println("Query : " + Query);
		List<EquipmentWithParameterListBean> DataTransactionsList = session.createSQLQuery(Query).list();

		return DataTransactionsList;
	}

	public List<GraphDataBean> getEquipmentviewForSmb(String equipmentIds, String FromDate, String ToDate,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		String Query = "";
		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select Timestamp,sum(coalesce(Inputpower_01,null)) as Value from tDataTransaction where EquipmentID in ("
					+ equipmentIds + ")  and  Inputpower_01 is not null and Timestamp <=  to_timestamp('" + ToDate
					+ "','DD/MM/YYYY HH24:MI:SS') and  Timestamp > to_timestamp('" + FromDate
					+ "','DD/MM/YYYY HH24:MI:SS') group by TimeStamp order by TimeStamp";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "Select  (Timestamp" + TimeConversionFromUTC
						+ ") as Timestamp,sum(coalesce(Inputpower_01,null)) as Value from tDataTransaction where EquipmentID in ("
						+ equipmentIds + ")  and Inputpower_01 is not null and Timestamp" + TimeConversionFromUTC
						+ " <=  to_timestamp('" + ToDate + "','DD/MM/YYYY HH24:MI:SS') and  Timestamp"
						+ TimeConversionFromUTC + " > to_timestamp('" + FromDate
						+ "','DD/MM/YYYY HH24:MI:SS') group by TimeStamp order by TimeStamp";

			}

		}

		System.out.println("PC Query::: " + Query);

		List<GraphDataBean> DataTransactionsList = session.createSQLQuery(Query).list();

		return DataTransactionsList;
	}

	public List<GraphDataBean> getEquipmentviewForTracker(String equipmentIds, String FromDate, String ToDate,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		String Query = "";
		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			Query = "Select Timestamp,sum(coalesce(Sunangle,null)) as Value from tDataTransaction where EquipmentID in ("
					+ equipmentIds + ")  and  Sunangle is not null and Timestamp <=  to_timestamp('" + ToDate
					+ "','DD/MM/YYYY HH24:MI:SS') and  Timestamp > to_timestamp('" + FromDate
					+ "','DD/MM/YYYY HH24:MI:SS') group by TimeStamp order by TimeStamp";

			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				String WhereTimeConversionFromUTC = " + (-1 * " + TimezoneOffset + " * interval '1 minute') ";

				Query = "Select  (Timestamp" + TimeConversionFromUTC
						+ ") as Timestamp,sum(coalesce(Sunangle,null)) as Value from tDataTransaction where EquipmentID in ("
						+ equipmentIds + ")  and Sunangle is not null and Timestamp" + TimeConversionFromUTC
						+ " <=  to_timestamp('" + ToDate + "','DD/MM/YYYY HH24:MI:SS') and  Timestamp"
						+ TimeConversionFromUTC + " > to_timestamp('" + FromDate
						+ "','DD/MM/YYYY HH24:MI:SS') group by TimeStamp order by TimeStamp";

			}

		}

		System.out.println("PC Query::: " + Query);

		List<GraphDataBean> DataTransactionsList = session.createSQLQuery(Query).list();

		return DataTransactionsList;
	}

}
