
/******************************************************
 * 
 *    	Filename	: StateDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for state operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.State;

@Repository
public class StateDAOImpl implements StateDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addState(State state) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(state);
	}

	// @Override
	public void updateState(State state) {
		Session session = sessionFactory.getCurrentSession();
		session.update(state);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<State> listStates() {
		Session session = sessionFactory.getCurrentSession();
		List<State> StatesList = session.createQuery("from State").list();

		return StatesList;
	}

	public List<State> listAllStatesByCountryID(int id) {
		Session session = sessionFactory.getCurrentSession();
		List<State> StatesList = session.createQuery("from State where CountryID='" + id + "'").list();

		return StatesList;
	}

	// @Override
	public State getStateById(int id) {
		Session session = sessionFactory.getCurrentSession();
		State state = (State) session.get(State.class, new Integer(id));
		return state;
	}

	// @Override
	public void removeState(int id) {
		Session session = sessionFactory.getCurrentSession();
		State state = (State) session.get(State.class, new Integer(id));

		// De-activate the flag
		state.setActiveFlag(0);

		if (null != state) {
			// session.delete(state);
			session.update(state);
		}
	}
}
