
/******************************************************
 * 
 *    	Filename	: ErrorCodeDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Error Code related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;


import java.util.List;

import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.ErrorCode;
import com.mestech.eampm.model.ParameterStandard;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;

public interface ErrorCodeDAO {
	 
	public void addErrorCode(ErrorCode errorcode);
	    
	public void updateErrorCode(ErrorCode errorcode);
	    
	public ErrorCode getErrorCodeById(int id);
	    
	public void removeErrorCode(int id);
	    
	public List<ErrorCode> listErrorCodes(int TypeID);
	public List<ErrorCode> listErrorCodes();
	
	public ErrorCode getErrorCodeByMax(String MaxColumnName);
	
	public List<ErrorCode> getErrorCodeListByUserId(int userId);
	
	public List<ErrorCode> getErrorCodeListBySiteId(int siteId);
	
	//public List<ErrorCode> listErrorCodesForDisplay(int siteId, String fromDate,String toDate,String category, String type, String priority );
	  
	public List<ErrorCode> getErrorCodeListByCustomerId(int customerId);
	
	public List<ErrorCode> listexitsErrorCodes(int equipmenttypeid,int errorid);
	
	
}
