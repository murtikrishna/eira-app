
/******************************************************
 * 
 *    	Filename	: SiteDocumentationDao.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for site document operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.SiteDocumentation;

public interface SiteDocumentationDao {
	public SiteDocumentation save(SiteDocumentation siteDocumentation);

	public SiteDocumentation update(SiteDocumentation siteDocumentation);

	public void delete(Long id);

	public SiteDocumentation edit(Long id);

	public List<SiteDocumentation> findBySiteId(int id);

	public List<SiteDocumentation> listAll();

	public void deleteDocuments(String siteCode, String docCategory, String docName);

	public List<SiteDocumentation> findBySiteId(int id, String category);
}
