
/******************************************************
 * 
 *    	Filename	: CustomerTypeDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Customer Type related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.CustomerType;

@Repository
public class CustomerTypeDAOImpl implements CustomerTypeDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addCustomerType(CustomerType customertype) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(customertype);

	}

	// @Override
	public void updateCustomerType(CustomerType customertype) {
		Session session = sessionFactory.getCurrentSession();
		session.update(customertype);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<CustomerType> listCustomerTypes() {
		Session session = sessionFactory.getCurrentSession();
		List<CustomerType> CustomerTypesList = session.createQuery("from CustomerType").list();

		return CustomerTypesList;
	}

	// @Override
	public CustomerType getCustomerTypeById(int id) {
		Session session = sessionFactory.getCurrentSession();
		CustomerType customertype = (CustomerType) session.get(CustomerType.class, new Integer(id));
		return customertype;
	}

	/*
	 * public CustomerType getCustomerTypeByMax(String MaxColumnName) { Session
	 * session = sessionFactory.getCurrentSession(); CustomerType customertype =
	 * (CustomerType) session.createQuery("from CustomerType ORDER BY " +
	 * MaxColumnName + " DESC") .setMaxResults(1).getSingleResult(); return
	 * customertype; }
	 */
	// @Override
	public void removeCustomerType(int id) {
		Session session = sessionFactory.getCurrentSession();
		CustomerType customertype = (CustomerType) session.get(CustomerType.class, new Integer(id));

		// De-activate the flag
		customertype.setActiveFlag(0);

		if (null != customertype) {
			// session.delete(customertype);

			session.update(customertype);
		}
	}
}
