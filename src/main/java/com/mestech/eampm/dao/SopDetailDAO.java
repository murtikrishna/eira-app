
/******************************************************
 * 
 *    	Filename	: SopDetailDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for site detail related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.SopDetail;

public interface SopDetailDAO {

	public void addSopDetail(SopDetail sopdetail);

	public void updateSopDetail(SopDetail sopdetail);

	public SopDetail getSopDetailById(int id);

	public void removeSopDetail(int id);

	public List<SopDetail> listSopDetails();
}
