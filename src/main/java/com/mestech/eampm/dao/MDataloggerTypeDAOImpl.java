
/******************************************************
 * 
 *    	Filename	: MDataloggerTypeDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for datalogger type related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.math.BigInteger;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.MDatalogger;
import com.mestech.eampm.model.MDataloggerType;

@Repository
public class MDataloggerTypeDAOImpl implements MDataloggerTypeDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void addDatalogger(MDataloggerType datalogger) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(datalogger);
	}

	public void maddDatalogger(MDatalogger datalogger) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate("dataloggerid", datalogger);
	}

	@Override
	public MDataloggerType save(MDataloggerType mDataloggerType) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(mDataloggerType);
		return mDataloggerType;
	}

	@Override
	public MDataloggerType update(MDataloggerType dataloggerType) {
		Session session = sessionFactory.getCurrentSession();
		session.update(dataloggerType);
		return dataloggerType;
	}

	@Override
	public void delete(int id) {
		Session session = sessionFactory.getCurrentSession();
		MDataloggerType dataloggerType = edit(id);
		session.delete(dataloggerType);
	}

	@Override
	public MDataloggerType edit(int id) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM MDataloggerType WHERE dataloggertypeid=:dataloggertypeid");
		query.setParameter("dataloggertypeid", id);
		return (MDataloggerType) query.getSingleResult();
	}

	@Override
	public List<MDataloggerType> listAll() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM MDataloggerType WHERE activeflag=1");
		return query.list();
	}

	@Override
	public List<MDatalogger> listAllDatalogger(int flag) {
		Session session = sessionFactory.getCurrentSession();
		String qry = "FROM MDatalogger WHERE activeflag=1";
		if (flag == 0) {
			qry = "FROM MDatalogger";
		}
		Query query = session.createQuery(qry);
		return query.list();
	}

	@Override
	public List<BigInteger> listDataloggerIdFromStandards() {
		Session session = sessionFactory.getCurrentSession();
		String qry = "select dataloggerid from mparameterintegratedstandards Group by dataloggerid order by dataloggerid";
		Query query = session.createSQLQuery(qry);
		List<BigInteger> obj = query.list();
		return obj;
	}

}
