
/******************************************************
 * 
 *    	Filename	: CountryRegionDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Country region operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.CountryRegion;

@Repository
public class CountryRegionDAOImpl implements CountryRegionDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addCountryRegion(CountryRegion countryregion) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(countryregion);

	}

	// @Override
	public void updateCountryRegion(CountryRegion countryregion) {
		Session session = sessionFactory.getCurrentSession();
		session.update(countryregion);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<CountryRegion> listCountryRegions() {
		Session session = sessionFactory.getCurrentSession();
		List<CountryRegion> CountryRegionsList = session.createQuery("from CountryRegion").list();

		return CountryRegionsList;
	}

	// @Override
	public CountryRegion getCountryRegionById(int id) {
		Session session = sessionFactory.getCurrentSession();
		CountryRegion countryregion = (CountryRegion) session.get(CountryRegion.class, new Integer(id));
		return countryregion;
	}

	// @Override
	public void removeCountryRegion(int id) {
		Session session = sessionFactory.getCurrentSession();
		CountryRegion countryregion = (CountryRegion) session.get(CountryRegion.class, new Integer(id));

		// De-activate the flag
		countryregion.setActiveFlag(0);

		if (null != countryregion) {
			// session.delete(countryregion);
			session.update(countryregion);
		}
	}
}
