

/******************************************************
 * 
 *    	Filename	: MServiceCodeDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Service code related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.math.BigInteger;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.MServiceCode;

@Repository
public class MServiceCodeDAOImpl implements MServiceCodeDAO {
	@Autowired
	private SessionFactory sessionfactory;

	@Override
	public MServiceCode save(MServiceCode mServiceCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MServiceCode update(MServiceCode mServiceCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public MServiceCode edit(BigInteger id) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MServiceCode> listAll() {
		Session session = null;
		try {
			session = sessionfactory.getCurrentSession();
			@SuppressWarnings("deprecation")
			Query<MServiceCode> query = session.createQuery("FROM MServiceCode WHERE activeflag=1");
			@SuppressWarnings("deprecation")
			List<MServiceCode> mServiceCodes = query.list();
			return mServiceCodes;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

}
