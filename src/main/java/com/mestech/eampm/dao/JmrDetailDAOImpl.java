
/******************************************************
 * 
 *    	Filename	: JmrDetailDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Jmt report related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.JmrDetail;
import com.mestech.eampm.model.Site;

@Repository
public class JmrDetailDAOImpl implements JmrDetailDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addJmrDetail(JmrDetail jmrdetail) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(jmrdetail);

	}

	// @Override
	public void updateJmrDetail(JmrDetail jmrdetail) {
		Session session = sessionFactory.getCurrentSession();
		session.update(jmrdetail);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<JmrDetail> listJmrDetails() {
		Session session = sessionFactory.getCurrentSession();
		List<JmrDetail> JmrDetailsList = session.createQuery("from JmrDetail").list();

		return JmrDetailsList;
	}

	// @Override
	public JmrDetail getJmrDetailById(int id) {
		Session session = sessionFactory.getCurrentSession();
		JmrDetail jmrdetail = (JmrDetail) session.get(JmrDetail.class, new Integer(id));
		return jmrdetail;
	}

	// @Override
	public void removeJmrDetail(int id) {
		Session session = sessionFactory.getCurrentSession();
		JmrDetail jmrdetail = (JmrDetail) session.get(JmrDetail.class, new Integer(id));

		// De-activate the flag
		jmrdetail.setActiveFlag(0);

		if (null != jmrdetail) {
			session.update(jmrdetail);
			// session.delete(activity);
		}
	}

	@Override
	public JmrDetail getticketID(Integer ticketid) {
		Session session = sessionFactory.getCurrentSession();
		JmrDetail jmrdetail = (JmrDetail) session
				.createQuery("from JmrDetail where ActiveFlag='1' and TicketID='" + ticketid + "'").uniqueResult();

		return jmrdetail;
	}

	public JmrDetail getJmrDetailByticketId(int ticketid) {
		Session session = sessionFactory.getCurrentSession();
		JmrDetail jmrdetail = (JmrDetail) session.get(JmrDetail.class, new Integer(ticketid));
		return jmrdetail;
	}
}
