
/******************************************************
 * 
 *    	Filename	: ActivityDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Activity operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.Activity;

@Repository
public class ActivityDAOImpl implements ActivityDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addActivity(Activity activity) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(activity);

	}

	// @Override
	public void updateActivity(Activity activity) {
		Session session = sessionFactory.getCurrentSession();
		session.update(activity);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Activity> listActivities() {
		Session session = sessionFactory.getCurrentSession();
		List<Activity> ActivitiesList = session.createQuery("from Activity").list();

		return ActivitiesList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public Activity getActivityName(int id) {
		Session session = sessionFactory.getCurrentSession();
		Activity ActivitiesList = (Activity) session
				.createQuery("from Activity where ActivityId='" + id + "' and ActiveFlag=1").uniqueResult();

		return ActivitiesList;
	}

	// @Override
	public Activity getActivityById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Activity activity = (Activity) session.get(Activity.class, new Integer(id));
		return activity;
	}

	// @Override
	public void removeActivity(int id) {
		Session session = sessionFactory.getCurrentSession();
		Activity activity = (Activity) session.get(Activity.class, new Integer(id));

		// De-activate the flag
		activity.setActiveFlag(0);

		if (null != activity) {
			session.update(activity);
			// session.delete(activity);
		}
	}
}
