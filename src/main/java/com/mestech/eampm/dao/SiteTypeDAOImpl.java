
/******************************************************
 * 
 *    	Filename	: SiteTypeDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for site type operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.SiteType;

@Repository
public class SiteTypeDAOImpl implements SiteTypeDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addSiteType(SiteType sitetype) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(sitetype);

	}

	// @Override
	public void updateSiteType(SiteType sitetype) {
		Session session = sessionFactory.getCurrentSession();
		session.update(sitetype);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<SiteType> listSiteTypes() {
		Session session = sessionFactory.getCurrentSession();
		List<SiteType> SiteTypesList = session.createQuery("from SiteType").list();

		return SiteTypesList;
	}

	// @Override
	public SiteType getSiteTypeById(int id) {
		Session session = sessionFactory.getCurrentSession();
		SiteType sitetype = (SiteType) session.get(SiteType.class, new Integer(id));
		return sitetype;
	}

	// @Override
	public void removeSiteType(int id) {
		Session session = sessionFactory.getCurrentSession();
		SiteType sitetype = (SiteType) session.get(SiteType.class, new Integer(id));

		// De-activate the flag
		sitetype.setActiveFlag(0);

		if (null != sitetype) {
			// session.delete(sitetype);

			session.update(sitetype);
		}
	}
}
