
/******************************************************
 * 
 *    	Filename	: TicketDetailDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for ticket details operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.bean.EquipmentCategoryBean;
import com.mestech.eampm.bean.OpenStateTicketsCount;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.DataTransaction;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.utility.UtilityCommon;
import com.mestech.eampm.utility.UtilityCommonDAO;

@Repository
public class TicketDetailDAOImpl implements TicketDetailDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();
	private UtilityCommon utilityCommon = new UtilityCommon();

	// @Override
	public void addTicketDetail(TicketDetail ticketdetail) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(ticketdetail);

	}

	// @Override
	public void updateTicketDetail(TicketDetail ticketdetail) {
		Session session = sessionFactory.getCurrentSession();
		session.update(ticketdetail);
	}

	// @Override
	public void removeTicketDetail(int id) {
		Session session = sessionFactory.getCurrentSession();
		TicketDetail ticketdetail = (TicketDetail) session.get(TicketDetail.class, new Integer(id));

		// De-activate the flag
		ticketdetail.setActiveFlag(0);

		if (null != ticketdetail) {
			session.update(ticketdetail);
			// session.delete(activity);
		}
	}

	private List<TicketDetail> GetCastedTicketDetailList(List<TicketDetail> lstTicketDetail) {
		List<TicketDetail> lstTicketDetailCasted = new ArrayList<TicketDetail>();

		if (lstTicketDetail.size() > 0) {

			Integer ticketid = 0;
			String ticketcode = "";
			Integer eventtransactionid = 0;
			Integer siteid = 0;
			Integer severity = 0;
			String description = "";
			Integer assignedto = 0;
			Date startedtimestamp = null;
			Date closedtimestamp = null;
			String remarks = "";
			Integer ticketstatus = 0;
			Integer refticketid = 0;
			Integer activeflag = 0;
			Date creationdate = null;
			Integer createdby = 0;
			Date lastupdateddate = null;
			Integer lastupdatedby = 0;
			Integer priority = 0;
			Integer state = 0;
			String ticketmode = "";
			String tickettype = "";
			String ticketcategory = "";
			String ticketdetail = "";
			Integer equipmentid = 0;
			Integer notificationstatus = 0;
			String longitude = "";
			String lattitude = "";
			String processtype = "";
			Date scheduledon = null;
			Integer closedby = 0;
			Date completedtimestamp = null;
			Date completedon = null;
			String longitude_completed = "";
			String lattitude_completed = "";
			Integer daycycle = 0;
			String ticketgroup = "";
			Integer uploadflag = 0;

			for (Object object : lstTicketDetail) {
				Object[] obj = (Object[]) object;

				ticketid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[0]);
				ticketcode = utilityCommon.StringFromStringObject(obj[1]);
				eventtransactionid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[2]);
				siteid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[3]);
				severity = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[4]);
				description = utilityCommon.StringFromStringObject(obj[5]);
				assignedto = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[6]);
				startedtimestamp = utilityCommon.DateFromDateObject(obj[7]);
				closedtimestamp = utilityCommon.DateFromDateObject(obj[8]);
				remarks = utilityCommon.StringFromStringObject(obj[9]);
				ticketstatus = utilityCommon.IntegerFromShortObject(obj[10]);
				refticketid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[11]);
				activeflag = utilityCommon.IntegerFromShortObject(obj[12]);
				creationdate = utilityCommon.DateFromDateObject(obj[13]);
				createdby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[14]);
				lastupdateddate = utilityCommon.DateFromDateObject(obj[15]);
				lastupdatedby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[16]);
				priority = utilityCommon.IntegerFromShortObject(obj[17]);
				state = utilityCommon.IntegerFromShortObject(obj[18]);
				ticketmode = utilityCommon.StringFromStringObject(obj[19]);
				tickettype = utilityCommon.StringFromStringObject(obj[20]);
				ticketcategory = utilityCommon.StringFromStringObject(obj[21]);
				ticketdetail = utilityCommon.StringFromStringObject(obj[22]);
				equipmentid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[23]);
				notificationstatus = utilityCommon.IntegerFromShortObject(obj[24]);
				longitude = utilityCommon.StringFromStringObject(obj[25]);
				lattitude = utilityCommon.StringFromStringObject(obj[26]);
				processtype = utilityCommon.StringFromStringObject(obj[27]);
				scheduledon = utilityCommon.DateFromDateObject(obj[28]);
				closedby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[29]);
				completedtimestamp = utilityCommon.DateFromDateObject(obj[30]);
				completedon = utilityCommon.DateFromDateObject(obj[31]);
				longitude_completed = utilityCommon.StringFromStringObject(obj[32]);
				lattitude_completed = utilityCommon.StringFromStringObject(obj[33]);
				daycycle = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[34]);
				ticketgroup = utilityCommon.StringFromStringObject(obj[35]);
				uploadflag = utilityCommon.IntegerFromShortObject(obj[36]);
				/*
				 * startedtimestampUTC = utilityCommon.DateFromDateObject(obj[37]);
				 * closedtimestamp = utilityCommon.DateFromDateObject(obj[38]); creationdate =
				 * utilityCommon.DateFromDateObject(obj[39]); lastupdateddate =
				 * utilityCommon.DateFromDateObject(obj[40]); scheduledon =
				 * utilityCommon.DateFromDateObject(obj[41]); completedtimestamp =
				 * utilityCommon.DateFromDateObject(obj[42]); completedon =
				 * utilityCommon.DateFromDateObject(obj[43]);
				 */

				TicketDetail objTicketDetailCasted = new TicketDetail();
				objTicketDetailCasted.setTicketID(ticketid);
				objTicketDetailCasted.setTicketCode(ticketcode);
				objTicketDetailCasted.setEventTransactionID(eventtransactionid);
				objTicketDetailCasted.setSiteID(siteid);
				objTicketDetailCasted.setSeverity(severity);
				objTicketDetailCasted.setDescription(description);
				objTicketDetailCasted.setAssignedTo(assignedto);
				objTicketDetailCasted.setStartedTimeStamp(startedtimestamp);
				objTicketDetailCasted.setClosedTimeStamp(closedtimestamp);
				objTicketDetailCasted.setRemarks(remarks);
				objTicketDetailCasted.setTicketStatus(ticketstatus);
				objTicketDetailCasted.setRefTicketID(refticketid);
				objTicketDetailCasted.setActiveFlag(activeflag);
				objTicketDetailCasted.setCreationDate(creationdate);
				objTicketDetailCasted.setCreatedBy(createdby);
				objTicketDetailCasted.setLastUpdatedDate(lastupdateddate);
				objTicketDetailCasted.setLastUpdatedBy(lastupdatedby);
				objTicketDetailCasted.setPriority(priority);
				objTicketDetailCasted.setState(state);
				objTicketDetailCasted.setTicketMode(ticketmode);
				objTicketDetailCasted.setTicketType(tickettype);
				objTicketDetailCasted.setTicketCategory(ticketcategory);
				objTicketDetailCasted.setTicketDetail(ticketdetail);
				objTicketDetailCasted.setEquipmentID(equipmentid);
				objTicketDetailCasted.setNotificationStatus(notificationstatus);
				objTicketDetailCasted.setLongitude(longitude);
				objTicketDetailCasted.setLattitude(lattitude);
				objTicketDetailCasted.setProcessType(processtype);
				objTicketDetailCasted.setScheduledOn(scheduledon);
				objTicketDetailCasted.setClosedBy(closedby);
				objTicketDetailCasted.setCompletedTimeStamp(completedtimestamp);
				objTicketDetailCasted.setCompletedOn(completedon);
				objTicketDetailCasted.setLongitude(longitude_completed);
				objTicketDetailCasted.setLattitude(lattitude_completed);
				objTicketDetailCasted.setDayCycle(daycycle);
				objTicketDetailCasted.setTicketGroup(ticketgroup);
				objTicketDetailCasted.setUploadFlag(uploadflag);

				lstTicketDetailCasted.add(objTicketDetailCasted);

			}
		}

		return lstTicketDetailCasted;
	}

	private TicketDetail GetCastedTicketDetail(Object objTicketDetail) {
		TicketDetail objTicketDetailCasted = new TicketDetail();
		if (objTicketDetail != null) {

			Integer ticketid = 0;
			String ticketcode = "";
			Integer eventtransactionid = 0;
			Integer siteid = 0;
			Integer severity = 0;
			String description = "";
			Integer assignedto = 0;
			Date startedtimestamp = null;
			Date closedtimestamp = null;
			String remarks = "";
			Integer ticketstatus = 0;
			Integer refticketid = 0;
			Integer activeflag = 0;
			Date creationdate = null;
			Integer createdby = 0;
			Date lastupdateddate = null;
			Integer lastupdatedby = 0;
			Integer priority = 0;
			Integer state = 0;
			String ticketmode = "";
			String tickettype = "";
			String ticketcategory = "";
			String ticketdetail = "";
			Integer equipmentid = 0;
			Integer notificationstatus = 0;
			String longitude = "";
			String lattitude = "";
			String processtype = "";
			Date scheduledon = null;
			Integer closedby = 0;
			Date completedtimestamp = null;
			Date completedon = null;
			String longitude_completed = "";
			String lattitude_completed = "";
			Integer daycycle = 0;
			String ticketgroup = "";
			Integer uploadflag = 0;

			Object[] obj = (Object[]) objTicketDetail;

			ticketid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[0]);
			ticketcode = utilityCommon.StringFromStringObject(obj[1]);
			eventtransactionid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[2]);
			siteid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[3]);
			severity = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[4]);
			description = utilityCommon.StringFromStringObject(obj[5]);
			assignedto = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[6]);
			startedtimestamp = utilityCommon.DateFromDateObject(obj[7]);
			closedtimestamp = utilityCommon.DateFromDateObject(obj[8]);
			remarks = utilityCommon.StringFromStringObject(obj[9]);
			ticketstatus = utilityCommon.IntegerFromShortObject(obj[10]);
			refticketid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[11]);
			activeflag = utilityCommon.IntegerFromShortObject(obj[12]);
			creationdate = utilityCommon.DateFromDateObject(obj[13]);
			createdby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[14]);
			lastupdateddate = utilityCommon.DateFromDateObject(obj[15]);
			lastupdatedby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[16]);
			priority = utilityCommon.IntegerFromShortObject(obj[17]);
			state = utilityCommon.IntegerFromShortObject(obj[18]);
			ticketmode = utilityCommon.StringFromStringObject(obj[19]);
			tickettype = utilityCommon.StringFromStringObject(obj[20]);
			ticketcategory = utilityCommon.StringFromStringObject(obj[21]);
			ticketdetail = utilityCommon.StringFromStringObject(obj[22]);
			equipmentid = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[23]);
			notificationstatus = utilityCommon.IntegerFromShortObject(obj[24]);
			longitude = utilityCommon.StringFromStringObject(obj[25]);
			lattitude = utilityCommon.StringFromStringObject(obj[26]);
			processtype = utilityCommon.StringFromStringObject(obj[27]);
			scheduledon = utilityCommon.DateFromDateObject(obj[28]);
			closedby = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[29]);
			completedtimestamp = utilityCommon.DateFromDateObject(obj[30]);
			completedon = utilityCommon.DateFromDateObject(obj[31]);
			longitude_completed = utilityCommon.StringFromStringObject(obj[32]);
			lattitude_completed = utilityCommon.StringFromStringObject(obj[33]);
			daycycle = utilityCommon.IntegerFromBigDecimalOrIntegerObject(obj[34]);
			ticketgroup = utilityCommon.StringFromStringObject(obj[35]);
			uploadflag = utilityCommon.IntegerFromShortObject(obj[36]);

			objTicketDetailCasted.setTicketID(ticketid);
			objTicketDetailCasted.setTicketCode(ticketcode);
			objTicketDetailCasted.setEventTransactionID(eventtransactionid);
			objTicketDetailCasted.setSiteID(siteid);
			objTicketDetailCasted.setSeverity(severity);
			objTicketDetailCasted.setDescription(description);
			objTicketDetailCasted.setAssignedTo(assignedto);
			objTicketDetailCasted.setStartedTimeStamp(startedtimestamp);
			objTicketDetailCasted.setClosedTimeStamp(closedtimestamp);
			objTicketDetailCasted.setRemarks(remarks);
			objTicketDetailCasted.setTicketStatus(ticketstatus);
			objTicketDetailCasted.setRefTicketID(refticketid);
			objTicketDetailCasted.setActiveFlag(activeflag);
			objTicketDetailCasted.setCreationDate(creationdate);
			objTicketDetailCasted.setCreatedBy(createdby);
			objTicketDetailCasted.setLastUpdatedDate(lastupdateddate);
			objTicketDetailCasted.setLastUpdatedBy(lastupdatedby);
			objTicketDetailCasted.setPriority(priority);
			objTicketDetailCasted.setState(state);
			objTicketDetailCasted.setTicketMode(ticketmode);
			objTicketDetailCasted.setTicketType(tickettype);
			objTicketDetailCasted.setTicketCategory(ticketcategory);
			objTicketDetailCasted.setTicketDetail(ticketdetail);
			objTicketDetailCasted.setEquipmentID(equipmentid);
			objTicketDetailCasted.setNotificationStatus(notificationstatus);
			objTicketDetailCasted.setLongitude(longitude);
			objTicketDetailCasted.setLattitude(lattitude);
			objTicketDetailCasted.setProcessType(processtype);
			objTicketDetailCasted.setScheduledOn(scheduledon);
			objTicketDetailCasted.setClosedBy(closedby);
			objTicketDetailCasted.setCompletedTimeStamp(completedtimestamp);
			objTicketDetailCasted.setCompletedOn(completedon);
			objTicketDetailCasted.setLongitude(longitude_completed);
			objTicketDetailCasted.setLattitude(lattitude_completed);
			objTicketDetailCasted.setDayCycle(daycycle);
			objTicketDetailCasted.setTicketGroup(ticketgroup);
			objTicketDetailCasted.setUploadFlag(uploadflag);

		}

		return objTicketDetailCasted;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<TicketDetail> listTicketDetails(String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";

			}
		}

		List<TicketDetail> TicketDetailListObject = session
				.createSQLQuery("Select " + tablecolumnnames + " from tTicketDetail").list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;
	}

	// @Override
	public TicketDetail getTicketDetailById(int id, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		TicketDetail ticketdetail = null;

		try {
			// Oracle
			String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
			String Query = "Select " + tablecolumnnames + " from tTicketDetail where TicketID='" + id
					+ "' and rownum=1";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				Query = "Select " + tablecolumnnames + " from tTicketDetail where TicketID='" + id + "' limit 1";
			}

			Object TicketDetailObject = session.createSQLQuery(Query).list().get(0);
			ticketdetail = GetCastedTicketDetail(TicketDetailObject);
		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return ticketdetail;

	}

	// @Override
	public TicketDetail getTicketDetailByIdOnUTC(int id, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		TicketDetail ticketdetail = null;

		try {
			// Oracle
			String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
			String Query = "Select " + tablecolumnnames + " from tTicketDetail where TicketID='" + id
					+ "' and rownum=1";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
							+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
							+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC
							+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
							+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
							+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
							+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";

				}
				Query = "Select " + tablecolumnnames + " from tTicketDetail where TicketID='" + id + "' limit 1";
			}

			Object TicketDetailObject = session.createSQLQuery(Query).list().get(0);
			ticketdetail = GetCastedTicketDetail(TicketDetailObject);
		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return ticketdetail;

	}

	public TicketDetail getTicketDetailByTicketCode(String TicketCode, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		TicketDetail ticketdetail = null;

		try {
			// Oracle
			String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
			String Query = "Select " + tablecolumnnames + " from tTicketDetail where TicketCode='" + TicketCode
					+ "' and rownum=1";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
							+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
							+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC
							+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
							+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
							+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
							+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";

				}
				Query = "Select " + tablecolumnnames + " from tTicketDetail where TicketCode='" + TicketCode
						+ "' limit 1";

			}

			Object TicketDetailObject = session.createSQLQuery(Query).list().get(0);
			ticketdetail = GetCastedTicketDetail(TicketDetailObject);

		} catch (NoResultException ex) {

		} catch (Exception ex) {

		}

		return ticketdetail;

	}

	public TicketDetail getTicketDetailByMax(String MaxColumnName, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		TicketDetail ticketdetail = null;

		try {
			// Oracle
			String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
			String Query = "Select " + tablecolumnnames + " from tTicketDetail  Order By " + MaxColumnName + " desc";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
							+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
							+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC
							+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
							+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
							+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
							+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";

				}

				Query = "Select " + tablecolumnnames + " from tTicketDetail  Order By " + MaxColumnName
						+ " desc limit 1";
			}
			System.out.println("Sarath" + Query);
			Object TicketDetailObject = session.createSQLQuery(Query).list().get(0);
			ticketdetail = GetCastedTicketDetail(TicketDetailObject);

		} catch (NoResultException ex) {
			System.out.println("Mohanraj ::: " + ex.getMessage());
		} catch (Exception ex) {
			System.out.println("Mohanraj ::: " + ex.getMessage());
		}

		return ticketdetail;

	}

	// 03-02-19
	/*
	 * public List<TicketDetail> listReportTicketDetailsForDisplay(int userid,String
	 * siteId, String fromDate,String toDate,String category, String type, String
	 * priority,String TimezoneOffset ) { Session session =
	 * sessionFactory.getCurrentSession(); List<TicketDetail> TicketDetailList = new
	 * ArrayList<TicketDetail>(); String WhereCondition = "";
	 * 
	 * if(siteId!=null && siteId!="" && siteId!="null") { WhereCondition =
	 * WhereCondition + " and SiteID ='" + siteId + "' "; } if(fromDate!=null &&
	 * toDate!=null && fromDate!="" && toDate!="") {
	 * if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) { //PostgreSQL
	 * 
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * WhereCondition = WhereCondition + " and  date_trunc('day',CreationDate" +
	 * TimeConversionFromUTC + ")>= date_trunc('day',to_date('" + fromDate +
	 * "','dd/mm/yyyy')) and   date_trunc('day',CreationDate" +
	 * TimeConversionFromUTC + ")<= date_trunc('day',to_date('" + toDate +
	 * "','dd/mm/yyyy')) ";
	 * 
	 * } else { WhereCondition = WhereCondition +
	 * " and  date_trunc('day',CreationDate)>= date_trunc('day',to_date('" +
	 * fromDate +
	 * "','dd/mm/yyyy')) and   date_trunc('day',CreationDate)<= date_trunc('day',to_date('"
	 * + toDate + "','dd/mm/yyyy')) ";
	 * 
	 * } } else { //Oracle WhereCondition = WhereCondition +
	 * " and  trunc(CreationDate)>=trunc(to_date('" + fromDate +
	 * "','dd/mm/yyyy')) and  trunc(CreationDate)<=trunc(to_date('" + toDate +
	 * "','dd/mm/yyyy')) "; } } if(category!=null && category!="") { WhereCondition
	 * = WhereCondition + " and TicketCategory ='" + category + "' "; }
	 * 
	 * 
	 * String tablecolumnnames =
	 * "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag"
	 * ; String Query = "Select " + tablecolumnnames +
	 * " from tTicketDetail where ActiveFlag='1'  " + WhereCondition +
	 * " and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid + "' and ActiveFlag=1)) order by TicketID desc";
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * tablecolumnnames =
	 * "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
	 * + TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" +
	 * TimeConversionFromUTC +
	 * ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
	 * + TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" +
	 * TimeConversionFromUTC +
	 * ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
	 * + TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp" +
	 * TimeConversionFromUTC + ") as completedtimestamp,(completedon" +
	 * TimeConversionFromUTC +
	 * ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag"
	 * ;
	 * 
	 * } Query = "Select " + tablecolumnnames +
	 * " from tTicketDetail where ActiveFlag='1'  " + WhereCondition +
	 * " and (TicketStatus in(4,5) or State in(2)) and SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mCustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * + userid +
	 * "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid + "'))) order by TicketID desc";
	 * 
	 * }
	 * 
	 * List<TicketDetail> TicketDetailListObject =
	 * session.createSQLQuery(Query).list(); TicketDetailList =
	 * GetCastedTicketDetailList(TicketDetailListObject);
	 * 
	 * return TicketDetailList;
	 * 
	 * 
	 * }
	 */

	public List<TicketDetail> listReportTicketDetailsForDisplay(int userid, String siteId, String fromDate,
			String toDate, String category, String type, String priority, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();
		String WhereCondition = "";

		if (siteId != null && siteId != "" && siteId != "null") {
			WhereCondition = WhereCondition + " and SiteID ='" + siteId + "' ";
		}
		if (fromDate != null && toDate != null && fromDate != "" && toDate != "") {
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) { // PostgreSQL

				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					WhereCondition = WhereCondition + " and  date_trunc('day',CreationDate" + TimeConversionFromUTC
							+ ")>= date_trunc('day',to_date('" + fromDate
							+ "','dd/mm/yyyy')) and   date_trunc('day',CreationDate" + TimeConversionFromUTC
							+ ")<= date_trunc('day',to_date('" + toDate + "','dd/mm/yyyy')) ";

				} else {
					WhereCondition = WhereCondition
							+ " and  date_trunc('day',CreationDate)>= date_trunc('day',to_date('" + fromDate
							+ "','dd/mm/yyyy')) and   date_trunc('day',CreationDate)<= date_trunc('day',to_date('"
							+ toDate + "','dd/mm/yyyy')) ";

				}
			} else { // Oracle
				WhereCondition = WhereCondition + " and  trunc(CreationDate)>=trunc(to_date('" + fromDate
						+ "','dd/mm/yyyy')) and  trunc(CreationDate)<=trunc(to_date('" + toDate + "','dd/mm/yyyy')) ";
			}
		}
		if (category != null && category != "") {
			WhereCondition = WhereCondition + " and TicketCategory ='" + category + "' ";
		}
		if (type != null && type != "") {
			WhereCondition = WhereCondition + " and TicketType ='" + type + "' ";
		}

		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where ActiveFlag='1'  " + WhereCondition
				+ " and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid + "' and ActiveFlag=1)) order by TicketID desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";

			}
			Query = "Select " + tablecolumnnames + " from tTicketDetail where ActiveFlag='1'  " + WhereCondition
					+ " and (TicketStatus in(4,5) or State in(2)) and SiteID in (Select SiteId from mSiteMap where UserID='"
					+ userid + "' and ActiveFlag=1) order by TicketID desc";

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

// 03-02-19
	/*
	 * @SuppressWarnings("unchecked") public List<TicketDetail>
	 * listTicketDetailsForDisplay(int userid,String siteId, String fromDate,String
	 * toDate,String category, String state, String priority , String
	 * TimezoneOffset) {
	 * 
	 * Session session = sessionFactory.getCurrentSession(); List<TicketDetail>
	 * TicketDetailList = new ArrayList<TicketDetail>(); String WhereCondition = "";
	 * 
	 * if(siteId!=null && siteId!="" && siteId!="null") { WhereCondition =
	 * WhereCondition + " and SiteID ='" + siteId + "' "; } if(fromDate!=null &&
	 * toDate!=null && fromDate!="" && toDate!="") {
	 * if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) { //PostgreSQL
	 * 
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * WhereCondition = WhereCondition + " and  date_trunc('day',CreationDate" +
	 * TimeConversionFromUTC + ")>= date_trunc('day',to_date('" + fromDate +
	 * "','dd/mm/yyyy')) and   date_trunc('day',CreationDate" +
	 * TimeConversionFromUTC + ")<= date_trunc('day',to_date('" + toDate +
	 * "','dd/mm/yyyy')) ";
	 * 
	 * } else { WhereCondition = WhereCondition +
	 * " and  date_trunc('day',CreationDate)>= date_trunc('day',to_date('" +
	 * fromDate +
	 * "','dd/mm/yyyy')) and   date_trunc('day',CreationDate)<= date_trunc('day',to_date('"
	 * + toDate + "','dd/mm/yyyy')) ";
	 * 
	 * } } else { //Oracle WhereCondition = WhereCondition +
	 * " and  trunc(CreationDate)>=trunc(to_date('" + fromDate +
	 * "','dd/mm/yyyy')) and  trunc(CreationDate)<=trunc(to_date('" + toDate +
	 * "','dd/mm/yyyy')) "; } } if(category!=null && category!="") { WhereCondition
	 * = WhereCondition + " and TicketCategory ='" + category + "' "; }
	 * if(state!=null && state!="") { WhereCondition = WhereCondition +
	 * " and State ='" + state + "' "; } if(priority!=null && priority!="" &&
	 * priority!="-1" && priority!="0") { WhereCondition = WhereCondition +
	 * " and Priority ='" + priority + "' "; }
	 * 
	 * 
	 * //Oracle String tablecolumnnames =
	 * "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag"
	 * ; String Query = "Select " + tablecolumnnames +
	 * " from tTicketDetail where ActiveFlag='1'  " + WhereCondition +
	 * " and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid + "' and ActiveFlag=1)) order by TicketID desc";
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * tablecolumnnames =
	 * "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
	 * + TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" +
	 * TimeConversionFromUTC +
	 * ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
	 * + TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" +
	 * TimeConversionFromUTC +
	 * ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
	 * + TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp" +
	 * TimeConversionFromUTC + ") as completedtimestamp,(completedon" +
	 * TimeConversionFromUTC +
	 * ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag"
	 * ;
	 * 
	 * } Query = "Select " + tablecolumnnames +
	 * " from tTicketDetail where ActiveFlag='1'  " + WhereCondition +
	 * " and SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mCustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * + userid +
	 * "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid + "'))) order by TicketID desc";
	 * 
	 * }
	 * 
	 * List<TicketDetail> TicketDetailListObject =
	 * session.createSQLQuery(Query).list(); TicketDetailList =
	 * GetCastedTicketDetailList(TicketDetailListObject);
	 * 
	 * return TicketDetailList;
	 * 
	 * 
	 * 
	 * 
	 * 
	 * }
	 */

	@SuppressWarnings("unchecked")
	public List<TicketDetail> listTicketDetailsForDisplay(int userid, String siteId, String fromDate, String toDate,
			String category, String state, String priority, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();
		String WhereCondition = "";

		if (siteId != null && siteId != "" && siteId != "null") {
			WhereCondition = WhereCondition + " and SiteID ='" + siteId + "' ";
		}
		if (fromDate != null && toDate != null && fromDate != "" && toDate != "") {
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) { // PostgreSQL

				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					WhereCondition = WhereCondition + " and  date_trunc('day',CreationDate" + TimeConversionFromUTC
							+ ")>= date_trunc('day',to_date('" + fromDate
							+ "','dd/mm/yyyy')) and   date_trunc('day',CreationDate" + TimeConversionFromUTC
							+ ")<= date_trunc('day',to_date('" + toDate + "','dd/mm/yyyy')) ";

				} else {
					WhereCondition = WhereCondition
							+ " and  date_trunc('day',CreationDate)>= date_trunc('day',to_date('" + fromDate
							+ "','dd/mm/yyyy')) and   date_trunc('day',CreationDate)<= date_trunc('day',to_date('"
							+ toDate + "','dd/mm/yyyy')) ";

				}
			} else { // Oracle
				WhereCondition = WhereCondition + " and  trunc(CreationDate)>=trunc(to_date('" + fromDate
						+ "','dd/mm/yyyy')) and  trunc(CreationDate)<=trunc(to_date('" + toDate + "','dd/mm/yyyy')) ";
			}
		}
		if (category != null && category != "") {
			WhereCondition = WhereCondition + " and TicketCategory ='" + category + "' ";
		}
		if (state != null && state != "") {
			WhereCondition = WhereCondition + " and State ='" + state + "' ";
		}
		if (priority != null && priority != "" && priority != "-1" && priority != "0") {
			WhereCondition = WhereCondition + " and Priority ='" + priority + "' ";
		}

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where ActiveFlag='1'  " + WhereCondition
				+ " and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from msitemap where UserID='"
				+ userid + "' and ActiveFlag=1)) order by TicketID desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";

			}
			Query = "Select " + tablecolumnnames
					+ " from tTicketDetail where ActiveFlag='1' and ticketstatus not in(6,7) " + WhereCondition
					+ " and SiteID in (Select SiteId from msitemap where UserID='" + userid
					+ "' and ActiveFlag=1) order by TicketID desc";

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	@SuppressWarnings("unchecked")
	public List<TicketDetail> listCustomerTicketDetailsForDisplay(int customerId, String siteId, String fromDate,
			String toDate, String category, String state, String priority, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();
		String WhereCondition = "";

		if (siteId != null && siteId != "" && siteId != "null") {
			WhereCondition = WhereCondition + " and SiteID ='" + siteId + "' ";
		}
		if (fromDate != null && toDate != null && fromDate != "" && toDate != "") {

			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) { // PostgreSQL

				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					WhereCondition = WhereCondition + " and   date_trunc('day',CreationDate" + TimeConversionFromUTC
							+ ")>= date_trunc('day',to_date('" + fromDate
							+ "','dd/mm/yyyy')) and   date_trunc('day',CreationDate" + TimeConversionFromUTC
							+ ")<= date_trunc('day',to_date('" + toDate + "','dd/mm/yyyy')) ";

				} else {
					WhereCondition = WhereCondition
							+ " and   date_trunc('day',CreationDate)>= date_trunc('day',to_date('" + fromDate
							+ "','dd/mm/yyyy')) and   date_trunc('day',CreationDate)<= date_trunc('day',to_date('"
							+ toDate + "','dd/mm/yyyy')) ";

				}
			} else { // Oracle
				WhereCondition = WhereCondition + " and  trunc(CreationDate)>=trunc(to_date('" + fromDate
						+ "','dd/mm/yyyy')) and  trunc(CreationDate)<=trunc(to_date('" + toDate + "','dd/mm/yyyy')) ";
			}

		}
		if (category != null && category != "") {
			WhereCondition = WhereCondition + " and TicketCategory ='" + category + "' ";
		}
		if (state != null && state != "") {
			WhereCondition = WhereCondition + " and State ='" + state + "' ";
		}
		if (priority != null && priority != "" && priority != "-1" && priority != "0") {
			WhereCondition = WhereCondition + " and Priority ='" + priority + "' ";
		}

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where ActiveFlag='1'  " + WhereCondition
				+ " and SiteID in (Select SiteId from mSitemap where userId ='" + customerId
				+ "') order by TicketID desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";

			}
			Query = "Select " + tablecolumnnames + " from tTicketDetail where ActiveFlag='1'  " + WhereCondition
					+ " and SiteID in (Select SiteId from mSitemap where UserID ='" + customerId
					+ "') order by TicketID desc";

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	public List<TicketDetail> getTicketDetailListByUserId(int userId, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames
				+ " from tTicketDetail where  ActiveFlag='1' and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userId + "' and ActiveFlag=1))";
		String TimeConversionFromUTC = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";

			}

			Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1' and creationdate"
					+ TimeConversionFromUTC + " >= date_trunc('day',now()" + TimeConversionFromUTC
					+ " - (30 * interval '1 day')) and SiteID in (Select SiteId from mSiteMap where UserID='" + userId
					+ "' and ActiveFlag=1)";

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	public List<TicketDetail> getTicketDetailListBySiteId(int siteId, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where ActiveFlag='1' and SiteID='" + siteId
				+ "'";
		String TimeConversionFromUTC = "";
		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";

			}

			Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1' and creationdate"
					+ TimeConversionFromUTC + " >= date_trunc('day',now()" + TimeConversionFromUTC
					+ " - (60 * interval '1 day')) and SiteID='" + siteId + "'";

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	public List<TicketDetail> getTicketDetailListByCustomerId(int customerId, String TimezoneOffset) {
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();
		try {

			Session session = sessionFactory.getCurrentSession();
			// Oracle
			String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
			String Query = "Select " + tablecolumnnames
					+ " from tTicketDetail where  ActiveFlag='1' and SiteID in (Select SiteId from mSite where CustomerID ='"
					+ customerId + "')";
			String TimeConversionFromUTC = "";

			// PostgreSQL
			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
					tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
							+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
							+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
							+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate"
							+ TimeConversionFromUTC
							+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
							+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
							+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
							+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";

				}

//				Query = "Select " + tablecolumnnames
//						+ " from tTicketDetail where  ActiveFlag='1' and ticketstatus not in(6,7) and creationdate"
//						+ TimeConversionFromUTC + " >= date_trunc('day',now()" + TimeConversionFromUTC
//						+ " - (30 * interval '1 day')) and SiteID in (Select SiteId from mSitemap where UserID ='"
//						+ customerId + "' and activeflag=1)";
				Query = "Select " + tablecolumnnames
						+ " from tTicketDetail where  ActiveFlag='1' and ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + " >= date_trunc('day',now()" + TimeConversionFromUTC
						+ " - (30 * interval '1 day')) and SiteID in (Select SiteId from mSite where CustomerID ='"
						+ customerId + "' and activeflag=1)";

			}

			List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
			TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return TicketDetailList;
	}

// 03-02-19
	/*
	 * public List<TicketDetail> getTicketDetailListByUserIdLimit(int userId, String
	 * TimezoneOffset) { Session session = sessionFactory.getCurrentSession();
	 * List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();
	 * 
	 * //Oracle String tablecolumnnames =
	 * "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag"
	 * ; String Query = "Select " + tablecolumnnames +
	 * " from tTicketDetail where ActiveFlag='1' and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userId +"' and ActiveFlag=1)) order by ticketcode desc";
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * tablecolumnnames =
	 * "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
	 * + TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" +
	 * TimeConversionFromUTC +
	 * ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
	 * + TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" +
	 * TimeConversionFromUTC +
	 * ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
	 * + TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp" +
	 * TimeConversionFromUTC + ") as completedtimestamp,(completedon" +
	 * TimeConversionFromUTC +
	 * ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag"
	 * ;
	 * 
	 * }
	 * 
	 * Query = "Select " + tablecolumnnames +
	 * " from tTicketDetail where ActiveFlag='1' and SiteID in (Select SiteId from mSite where CustomerID in(Select CustomerID from mcustomer where customerid in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * + userId
	 * +"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userId + "')))) order by ticketcode desc limit 10";
	 * 
	 * }
	 * 
	 * List<TicketDetail> TicketDetailListObject =
	 * session.createSQLQuery(Query).list(); TicketDetailList =
	 * GetCastedTicketDetailList(TicketDetailListObject); return TicketDetailList; }
	 */

	public List<TicketDetail> getTicketDetailListByUserIdLimit(int userId, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames
				+ " from tTicketDetail where ActiveFlag='1' and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userId + "' and ActiveFlag=1)) order by ticketcode desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";

			}

			Query = "Select " + tablecolumnnames
					+ " from tTicketDetail where ActiveFlag='1' and SiteID in (Select SiteId from mSiteMap where UserID='"
					+ userId + "' and ActiveFlag=1) order by ticketid desc limit 10";

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);
		return TicketDetailList;
	}

	public List<OpenStateTicketsCount> listOpenticketstatecountsByUser(int userid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,nvl(ticketcount,0) as ticketcount,tbl3.color as color from  (select Ticketstatus,StatusSeq,DaysSeq,days,color from  (select 'Open-Created' as Ticketstatus,1 as StatusSeq from dual union select 'Open-Assigned' as Ticketstatus,2 as StatusSeq  from dual union select 'Open-Inprogress' as Ticketstatus, 3 as StatusSeq  from dual union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq  from dual union select 'Open-Finished' as Ticketstatus, 5 as StatusSeq  from dual union select 'Closed-Created' as Ticketstatus,6 as StatusSeq from dual union select 'Closed-Assigned' as Ticketstatus,7 as StatusSeq  from dual union select 'Closed-Inprogress' as Ticketstatus, 8 as StatusSeq  from dual union select 'Closed-Unfinished' as Ticketstatus, 9 as StatusSeq  from dual union select 'Closed-Finished' as Ticketstatus, 10 as StatusSeq  from dual union select 'Hold-Created' as Ticketstatus,11 as StatusSeq from dual union select 'Hold-Assigned' as Ticketstatus,12 as StatusSeq  from dual union select 'Hold-Inprogress' as Ticketstatus, 13 as StatusSeq  from dual union select 'Hold-Unfinished' as Ticketstatus, 14 as StatusSeq  from dual union select 'Hold-Finished' as Ticketstatus, 15 as StatusSeq  from dual) tbl1, (select '0-5 days' as days, 1 as DaysSeq,'#CFD8DC' as color from dual union select '6-15 days' as days, 2 as DaysSeq,'#78909C' as color from dual union select '16-30 days' as days, 3 as DaysSeq,'#455A64' as color from dual union select '31-60 days' as days, 4 as DaysSeq,'#263238' as color from dual) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from(Select TicketID,State,TicketStatus,round(trunc(sysdate)-trunc(CreationDate)) as  day  from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid
				+ "' and ActiveFlag=1)))) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from  (select Ticketstatus,StatusSeq,DaysSeq,days,color from  (select 'Open-Created' as Ticketstatus,1 as StatusSeq union select 'Open-Assigned' as Ticketstatus,2 as StatusSeq union select 'Open-Inprogress' as Ticketstatus, 3 as StatusSeq  union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq union select 'Open-Finished' as Ticketstatus, 5 as StatusSeq  union select 'Closed-Created' as Ticketstatus,6 as StatusSeq union select 'Closed-Assigned' as Ticketstatus,7 as StatusSeq union select 'Closed-Inprogress' as Ticketstatus, 8 as StatusSeq union select 'Closed-Unfinished' as Ticketstatus, 9 as StatusSeq union select 'Closed-Finished' as Ticketstatus, 10 as StatusSeq union select 'Hold-Created' as Ticketstatus,11 as StatusSeq union select 'Hold-Assigned' as Ticketstatus,12 as StatusSeq union select 'Hold-Inprogress' as Ticketstatus, 13 as StatusSeq  union select 'Hold-Unfinished' as Ticketstatus, 14 as StatusSeq  union select 'Hold-Finished' as Ticketstatus, 15 as StatusSeq) tbl1, (select '0-5 days' as days, 1 as DaysSeq,'#CFD8DC' as color  union select '6-15 days' as days, 2 as DaysSeq,'#78909C' as color union select '16-30 days' as days, 3 as DaysSeq,'#455A64' as color union select '31-60 days' as days, 4 as DaysSeq,'#263238' as color) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from(Select TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now()"
						+ TimeConversionFromUTC + ")-date_trunc('day',CreationDate" + TimeConversionFromUTC
						+ ")) as  day  from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
						+ userid
						+ "'))) tbla) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq desc";
			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from  (select Ticketstatus,StatusSeq,DaysSeq,days,color from  (select 'Open-Created' as Ticketstatus,1 as StatusSeq union select 'Open-Assigned' as Ticketstatus,2 as StatusSeq union  select 'Open-Inprogress' as Ticketstatus, 3 as StatusSeq  union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq union select 'Open-Finished' as Ticketstatus, 5 as StatusSeq  union select 'Closed-Created' as Ticketstatus,6 as StatusSeq union select 'Closed-Assigned' as Ticketstatus,7 as StatusSeq union select 'Closed-Inprogress' as Ticketstatus, 8 as StatusSeq union select 'Closed-Unfinished' as Ticketstatus, 9 as StatusSeq union select 'Closed-Finished' as Ticketstatus, 10 as StatusSeq union select 'Hold-Created' as Ticketstatus,11 as StatusSeq union select 'Hold-Assigned' as Ticketstatus,12 as StatusSeq union select 'Hold-Inprogress' as Ticketstatus, 13 as StatusSeq  union select 'Hold-Unfinished' as Ticketstatus, 14 as StatusSeq  union select 'Hold-Finished' as Ticketstatus, 15 as StatusSeq) tbl1, (select '0-5 days' as days, 1 as DaysSeq,'#CFD8DC' as color  union select '6-15 days' as days, 2 as DaysSeq,'#78909C' as color union select '16-30 days' as days, 3 as DaysSeq,'#455A64' as color union select '31-60 days' as days, 4 as DaysSeq,'#263238' as color) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from(Select TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now())-date_trunc('day',CreationDate)) as  day  from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
						+ userid
						+ "'))) tbla) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq desc";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> listOpenticketstatecountsByAssignedByUser(int userid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,nvl(ticketcount,0) as ticketcount from (select Ticketstatus,StatusSeq,DaysSeq,days from (select 'Open-Assigned' as Ticketstatus,1 as StatusSeq from dual union select 'Open-Inprogress' as Ticketstatus,2 as StatusSeq  from dual union select 'Open-Finished' as Ticketstatus, 3 as StatusSeq  from dual union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq  from dual union select 'Closed-Closed' as Ticketstatus, 5 as StatusSeq  from dual union select 'Closed-Finished' as Ticketstatus, 6 as StatusSeq  from dual union select 'Closed-Unfinished' as Ticketstatus, 7 as StatusSeq  from dual union select 'Hold-Hold' as Ticketstatus, 8 as StatusSeq  from dual) tbl1, (select '0-5 days' as days, 1 as DaysSeq from dual union select '6-15 days' as days, 2 as DaysSeq from dual union select '16-30 days' as days, 3 as DaysSeq from dual union select '31-60 days' as days, 4 as DaysSeq from dual) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'   end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from (Select TicketID,State,TicketStatus,round(trunc(sysdate)-trunc(Scheduledon)) as  day  from tTicketDetail where ActiveFlag='1' and state=1 and ticketstatus<>1  and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid
				+ "' and ActiveFlag=1)))) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq ";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,coalesce(ticketcount,0) as ticketcount from (select Ticketstatus,StatusSeq,DaysSeq,days from (select 'Open-Assigned' as Ticketstatus,1 as StatusSeq  union select 'Open-Inprogress' as Ticketstatus,2 as StatusSeq   union select 'Open-Finished' as Ticketstatus, 3 as StatusSeq   union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq   union select 'Closed-Closed' as Ticketstatus, 5 as StatusSeq   union select 'Closed-Finished' as Ticketstatus, 6 as StatusSeq   union select 'Closed-Unfinished' as Ticketstatus, 7 as StatusSeq   union select 'Hold-Hold' as Ticketstatus, 8 as StatusSeq  ) tbl1, (select '0-5 days' as days, 1 as DaysSeq  union select '6-15 days' as days, 2 as DaysSeq  union select '16-30 days' as days, 3 as DaysSeq  union select '31-60 days' as days, 4 as DaysSeq ) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'   end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from (Select TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now()"
						+ TimeConversionFromUTC + ")-date_trunc('day',Scheduledon" + TimeConversionFromUTC
						+ ")) as  day  from tTicketDetail where ActiveFlag='1' and state=1 and ticketstatus<>1  and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
						+ userid
						+ "'))) tbla) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq ";

			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,coalesce(ticketcount,0) as ticketcount from (select Ticketstatus,StatusSeq,DaysSeq,days from (select 'Open-Assigned' as Ticketstatus,1 as StatusSeq  union select 'Open-Inprogress' as Ticketstatus,2 as StatusSeq   union select 'Open-Finished' as Ticketstatus, 3 as StatusSeq   union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq   union select 'Closed-Closed' as Ticketstatus, 5 as StatusSeq   union select 'Closed-Finished' as Ticketstatus, 6 as StatusSeq   union select 'Closed-Unfinished' as Ticketstatus, 7 as StatusSeq   union select 'Hold-Hold' as Ticketstatus, 8 as StatusSeq  ) tbl1, (select '0-5 days' as days, 1 as DaysSeq  union select '6-15 days' as days, 2 as DaysSeq  union select '16-30 days' as days, 3 as DaysSeq  union select '31-60 days' as days, 4 as DaysSeq ) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'   end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from (Select TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now())-date_trunc('day',Scheduledon)) as  day  from tTicketDetail where ActiveFlag='1' and state=1 and ticketstatus<>1  and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
						+ userid
						+ "'))) tbla) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq ";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> listOpenticketstatecountsByAssignedBySite(int siteid, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,nvl(ticketcount,0) as ticketcount from (select Ticketstatus,StatusSeq,DaysSeq,days from (select 'Open-Assigned' as Ticketstatus,1 as StatusSeq from dual union select 'Open-Inprogress' as Ticketstatus,2 as StatusSeq  from dual union select 'Open-Finished' as Ticketstatus, 3 as StatusSeq  from dual union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq  from dual union select 'Closed-Closed' as Ticketstatus, 5 as StatusSeq  from dual union select 'Closed-Finished' as Ticketstatus, 6 as StatusSeq  from dual union select 'Closed-Unfinished' as Ticketstatus, 7 as StatusSeq  from dual union select 'Hold-Hold' as Ticketstatus, 8 as StatusSeq  from dual) tbl1, (select '0-5 days' as days, 1 as DaysSeq from dual union select '6-15 days' as days, 2 as DaysSeq from dual union select '16-30 days' as days, 3 as DaysSeq from dual union select '31-60 days' as days, 4 as DaysSeq from dual) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'   end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from(Select TicketID,State,TicketStatus,round(trunc(sysdate)-trunc(Scheduledon)) as  day  from tTicketDetail where ActiveFlag='1' and state=1 and ticketstatus<>1  and SiteID='"
				+ siteid
				+ "')) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq ";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,coalesce(ticketcount,0) as ticketcount from (select Ticketstatus,StatusSeq,DaysSeq,days from (select 'Open-Assigned' as Ticketstatus,1 as StatusSeq  union select 'Open-Inprogress' as Ticketstatus,2 as StatusSeq   union select 'Open-Finished' as Ticketstatus, 3 as StatusSeq   union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq   union select 'Closed-Closed' as Ticketstatus, 5 as StatusSeq   union select 'Closed-Finished' as Ticketstatus, 6 as StatusSeq   union select 'Closed-Unfinished' as Ticketstatus, 7 as StatusSeq   union select 'Hold-Hold' as Ticketstatus, 8 as StatusSeq  ) tbl1, (select '0-5 days' as days, 1 as DaysSeq  union select '6-15 days' as days, 2 as DaysSeq  union select '16-30 days' as days, 3 as DaysSeq  union select '31-60 days' as days, 4 as DaysSeq ) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from (select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'   end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from (Select TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now()"
						+ TimeConversionFromUTC + ")-date_trunc('day',Scheduledon" + TimeConversionFromUTC
						+ ")) as  day  from tTicketDetail where ActiveFlag='1' and state=1 and ticketstatus<>1  and SiteID='"
						+ siteid
						+ "') tbla) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq ";

			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,coalesce(ticketcount,0) as ticketcount from (select Ticketstatus,StatusSeq,DaysSeq,days from (select 'Open-Assigned' as Ticketstatus,1 as StatusSeq  union select 'Open-Inprogress' as Ticketstatus,2 as StatusSeq   union select 'Open-Finished' as Ticketstatus, 3 as StatusSeq   union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq   union select 'Closed-Closed' as Ticketstatus, 5 as StatusSeq   union select 'Closed-Finished' as Ticketstatus, 6 as StatusSeq   union select 'Closed-Unfinished' as Ticketstatus, 7 as StatusSeq   union select 'Hold-Hold' as Ticketstatus, 8 as StatusSeq  ) tbl1, (select '0-5 days' as days, 1 as DaysSeq  union select '6-15 days' as days, 2 as DaysSeq  union select '16-30 days' as days, 3 as DaysSeq  union select '31-60 days' as days, 4 as DaysSeq ) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from (select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'   end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from (Select TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now())-date_trunc('day',Scheduledon)) as  day  from tTicketDetail where ActiveFlag='1' and state=1 and ticketstatus<>1  and SiteID='"
						+ siteid
						+ "') tbla) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq ";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> listOpenticketstatecountsByAssignedByCustomer(int customerid,
			String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,nvl(ticketcount,0) as ticketcount from (select Ticketstatus,StatusSeq,DaysSeq,days from (select 'Open-Assigned' as Ticketstatus,1 as StatusSeq from dual union select 'Open-Inprogress' as Ticketstatus,2 as StatusSeq  from dual union select 'Open-Finished' as Ticketstatus, 3 as StatusSeq  from dual union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq  from dual union select 'Closed-Closed' as Ticketstatus, 5 as StatusSeq  from dual union select 'Closed-Finished' as Ticketstatus, 6 as StatusSeq  from dual union select 'Closed-Unfinished' as Ticketstatus, 7 as StatusSeq  from dual union select 'Hold-Hold' as Ticketstatus, 8 as StatusSeq  from dual) tbl1, (select '0-5 days' as days, 1 as DaysSeq from dual union select '6-15 days' as days, 2 as DaysSeq from dual union select '16-30 days' as days, 3 as DaysSeq from dual union select '31-60 days' as days, 4 as DaysSeq from dual) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'   end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from(Select TicketID,State,TicketStatus,round(trunc(sysdate)-trunc(Scheduledon)) as  day  from tTicketDetail where ActiveFlag='1' and state=1 and ticketstatus<>1  and SiteID  in (Select SiteId from mSite where CustomerID ='"
				+ customerid
				+ "'))) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq ";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,coalesce(ticketcount,0) as ticketcount from (select Ticketstatus,StatusSeq,DaysSeq,days from (select 'Open-Assigned' as Ticketstatus,1 as StatusSeq  union select 'Open-Inprogress' as Ticketstatus,2 as StatusSeq   union select 'Open-Finished' as Ticketstatus, 3 as StatusSeq   union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq   union select 'Closed-Closed' as Ticketstatus, 5 as StatusSeq   union select 'Closed-Finished' as Ticketstatus, 6 as StatusSeq   union select 'Closed-Unfinished' as Ticketstatus, 7 as StatusSeq   union select 'Hold-Hold' as Ticketstatus, 8 as StatusSeq  ) tbl1, (select '0-5 days' as days, 1 as DaysSeq  union select '6-15 days' as days, 2 as DaysSeq  union select '16-30 days' as days, 3 as DaysSeq  union select '31-60 days' as days, 4 as DaysSeq ) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'   end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from(Select TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now()"
						+ TimeConversionFromUTC + ")-date_trunc('day',Scheduledon" + TimeConversionFromUTC
						+ ")) as  day  from tTicketDetail where ActiveFlag='1' and state=1 and ticketstatus<>1  and SiteID  in (Select SiteId from mSite where CustomerID ='"
						+ customerid
						+ "')) tbla) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq ";

			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,coalesce(ticketcount,0) as ticketcount from (select Ticketstatus,StatusSeq,DaysSeq,days from (select 'Open-Assigned' as Ticketstatus,1 as StatusSeq  union select 'Open-Inprogress' as Ticketstatus,2 as StatusSeq   union select 'Open-Finished' as Ticketstatus, 3 as StatusSeq   union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq   union select 'Closed-Closed' as Ticketstatus, 5 as StatusSeq   union select 'Closed-Finished' as Ticketstatus, 6 as StatusSeq   union select 'Closed-Unfinished' as Ticketstatus, 7 as StatusSeq   union select 'Hold-Hold' as Ticketstatus, 8 as StatusSeq  ) tbl1, (select '0-5 days' as days, 1 as DaysSeq  union select '6-15 days' as days, 2 as DaysSeq  union select '16-30 days' as days, 3 as DaysSeq  union select '31-60 days' as days, 4 as DaysSeq ) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'   end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from(Select TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now())-date_trunc('day',Scheduledon)) as  day  from tTicketDetail where ActiveFlag='1' and state=1 and ticketstatus<>1  and SiteID  in (Select SiteId from mSite where CustomerID ='"
						+ customerid
						+ "')) tbla) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq ";

			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> listOpenticketstatecountsBySite(int siteid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();

		// Oracle
		String Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,nvl(ticketcount,0) as ticketcount,tbl3.color as color from  (select Ticketstatus,StatusSeq,DaysSeq,days,color from  (select 'Open-Created' as Ticketstatus,1 as StatusSeq from dual union select 'Open-Assigned' as Ticketstatus,2 as StatusSeq  from dual union select 'Open-Inprogress' as Ticketstatus, 3 as StatusSeq  from dual union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq  from dual union select 'Open-Finished' as Ticketstatus, 5 as StatusSeq  from dual union select 'Closed-Created' as Ticketstatus,6 as StatusSeq from dual union select 'Closed-Assigned' as Ticketstatus,7 as StatusSeq  from dual union select 'Closed-Inprogress' as Ticketstatus, 8 as StatusSeq  from dual union select 'Closed-Unfinished' as Ticketstatus, 9 as StatusSeq  from dual union select 'Closed-Finished' as Ticketstatus, 10 as StatusSeq  from dual union select 'Hold-Created' as Ticketstatus,11 as StatusSeq from dual union select 'Hold-Assigned' as Ticketstatus,12 as StatusSeq  from dual union select 'Hold-Inprogress' as Ticketstatus, 13 as StatusSeq  from dual union select 'Hold-Unfinished' as Ticketstatus, 14 as StatusSeq  from dual union select 'Hold-Finished' as Ticketstatus, 15 as StatusSeq  from dual) tbl1, (select '0-5 days' as days, 1 as DaysSeq,'#CFD8DC' as color from dual union select '6-15 days' as days, 2 as DaysSeq,'#78909C' as color from dual union select '16-30 days' as days, 3 as DaysSeq,'#455A64' as color from dual union select '31-60 days' as days, 4 as DaysSeq,'#263238' as color from dual) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from(Select TicketID,State,TicketStatus,round(trunc(sysdate)-trunc(CreationDate)) as  day  from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and  ticketstatus not in(6,7) and SiteID ='"
				+ siteid
				+ "')) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from  (select Ticketstatus,StatusSeq,DaysSeq,days,color from  (select 'Open-Created' as Ticketstatus,1 as StatusSeq union select 'Open-Assigned' as Ticketstatus,2 as StatusSeq union select 'Open-Inprogress' as Ticketstatus, 3 as StatusSeq  union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq union select 'Open-Finished' as Ticketstatus, 5 as StatusSeq  union select 'Closed-Created' as Ticketstatus,6 as StatusSeq union select 'Closed-Assigned' as Ticketstatus,7 as StatusSeq union select 'Closed-Inprogress' as Ticketstatus, 8 as StatusSeq union select 'Closed-Unfinished' as Ticketstatus, 9 as StatusSeq union select 'Closed-Finished' as Ticketstatus, 10 as StatusSeq union select 'Hold-Created' as Ticketstatus,11 as StatusSeq union select 'Hold-Assigned' as Ticketstatus,12 as StatusSeq union select 'Hold-Inprogress' as Ticketstatus, 13 as StatusSeq  union select 'Hold-Unfinished' as Ticketstatus, 14 as StatusSeq  union select 'Hold-Finished' as Ticketstatus, 15 as StatusSeq) tbl1, (select '0-5 days' as days, 1 as DaysSeq,'#CFD8DC' as color  union select '6-15 days' as days, 2 as DaysSeq,'#78909C' as color union select '16-30 days' as days, 3 as DaysSeq,'#455A64' as color union select '31-60 days' as days, 4 as DaysSeq,'#263238' as color) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from(Select TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now()"
						+ TimeConversionFromUTC + ")-date_trunc('day',CreationDate" + TimeConversionFromUTC
						+ ")) as  day  from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and SiteID ='"
						+ siteid
						+ "') tbla) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq desc";
			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from  (select Ticketstatus,StatusSeq,DaysSeq,days,color from  (select 'Open-Created' as Ticketstatus,1 as StatusSeq union select 'Open-Assigned' as Ticketstatus,2 as StatusSeq union select 'Open-Inprogress' as Ticketstatus, 3 as StatusSeq  union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq union select 'Open-Finished' as Ticketstatus, 5 as StatusSeq  union select 'Closed-Created' as Ticketstatus,6 as StatusSeq union select 'Closed-Assigned' as Ticketstatus,7 as StatusSeq union select 'Closed-Inprogress' as Ticketstatus, 8 as StatusSeq union select 'Closed-Unfinished' as Ticketstatus, 9 as StatusSeq union select 'Closed-Finished' as Ticketstatus, 10 as StatusSeq union select 'Hold-Created' as Ticketstatus,11 as StatusSeq union select 'Hold-Assigned' as Ticketstatus,12 as StatusSeq union select 'Hold-Inprogress' as Ticketstatus, 13 as StatusSeq  union select 'Hold-Unfinished' as Ticketstatus, 14 as StatusSeq  union select 'Hold-Finished' as Ticketstatus, 15 as StatusSeq) tbl1, (select '0-5 days' as days, 1 as DaysSeq,'#CFD8DC' as color  union select '6-15 days' as days, 2 as DaysSeq,'#78909C' as color union select '16-30 days' as days, 3 as DaysSeq,'#455A64' as color union select '31-60 days' as days, 4 as DaysSeq,'#263238' as color) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from(Select TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now())-date_trunc('day',CreationDate)) as  day  from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and SiteID='"
						+ siteid
						+ "') tbla) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq desc";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> listOpenticketstatecountsByCustomer(int customerid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();

		// Oracle
		String Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,nvl(ticketcount,0) as ticketcount,tbl3.color as color from  (select Ticketstatus,StatusSeq,DaysSeq,days,color from  (select 'Open-Created' as Ticketstatus,1 as StatusSeq from dual union select 'Open-Assigned' as Ticketstatus,2 as StatusSeq  from dual union select 'Open-Inprogress' as Ticketstatus, 3 as StatusSeq  from dual union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq  from dual union select 'Open-Finished' as Ticketstatus, 5 as StatusSeq  from dual union select 'Closed-Created' as Ticketstatus,6 as StatusSeq from dual union select 'Closed-Assigned' as Ticketstatus,7 as StatusSeq  from dual union select 'Closed-Inprogress' as Ticketstatus, 8 as StatusSeq  from dual union select 'Closed-Unfinished' as Ticketstatus, 9 as StatusSeq  from dual union select 'Closed-Finished' as Ticketstatus, 10 as StatusSeq  from dual union select 'Hold-Created' as Ticketstatus,11 as StatusSeq from dual union select 'Hold-Assigned' as Ticketstatus,12 as StatusSeq  from dual union select 'Hold-Inprogress' as Ticketstatus, 13 as StatusSeq  from dual union select 'Hold-Unfinished' as Ticketstatus, 14 as StatusSeq  from dual union select 'Hold-Finished' as Ticketstatus, 15 as StatusSeq  from dual) tbl1, (select '0-5 days' as days, 1 as DaysSeq,'#CFD8DC' as color from dual union select '6-15 days' as days, 2 as DaysSeq,'#78909C' as color from dual union select '16-30 days' as days, 3 as DaysSeq,'#455A64' as color from dual union select '31-60 days' as days, 4 as DaysSeq,'#263238' as color from dual) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from(Select TicketID,State,TicketStatus,round(trunc(sysdate)-trunc(CreationDate)) as  day  from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID ='"
				+ customerid
				+ "'))) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from  (select Ticketstatus,StatusSeq,DaysSeq,days,color from  (select 'Open-Created' as Ticketstatus,1 as StatusSeq union select 'Open-Assigned' as Ticketstatus,2 as StatusSeq union select 'Open-Inprogress' as Ticketstatus, 3 as StatusSeq  union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq union select 'Open-Finished' as Ticketstatus, 5 as StatusSeq  union select 'Closed-Created' as Ticketstatus,6 as StatusSeq union select 'Closed-Assigned' as Ticketstatus,7 as StatusSeq union select 'Closed-Inprogress' as Ticketstatus, 8 as StatusSeq union select 'Closed-Unfinished' as Ticketstatus, 9 as StatusSeq union select 'Closed-Finished' as Ticketstatus, 10 as StatusSeq union select 'Hold-Created' as Ticketstatus,11 as StatusSeq union select 'Hold-Assigned' as Ticketstatus,12 as StatusSeq union select 'Hold-Inprogress' as Ticketstatus, 13 as StatusSeq  union select 'Hold-Unfinished' as Ticketstatus, 14 as StatusSeq  union select 'Hold-Finished' as Ticketstatus, 15 as StatusSeq) tbl1, (select '0-5 days' as days, 1 as DaysSeq,'#CFD8DC' as color  union select '6-15 days' as days, 2 as DaysSeq,'#78909C' as color union select '16-30 days' as days, 3 as DaysSeq,'#455A64' as color union select '31-60 days' as days, 4 as DaysSeq,'#263238' as color) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from(Select TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now()"
						+ TimeConversionFromUTC + ")-date_trunc('day',CreationDate" + TimeConversionFromUTC
						+ ")) as  day  from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID ='"
						+ customerid
						+ "')) tbla) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq desc";
			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,tbl3.days as days,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from  (select Ticketstatus,StatusSeq,DaysSeq,days,color from  (select 'Open-Created' as Ticketstatus,1 as StatusSeq union select 'Open-Assigned' as Ticketstatus,2 as StatusSeq union select 'Open-Inprogress' as Ticketstatus, 3 as StatusSeq  union select 'Open-Unfinished' as Ticketstatus, 4 as StatusSeq union select 'Open-Finished' as Ticketstatus, 5 as StatusSeq  union select 'Closed-Created' as Ticketstatus,6 as StatusSeq union select 'Closed-Assigned' as Ticketstatus,7 as StatusSeq union select 'Closed-Inprogress' as Ticketstatus, 8 as StatusSeq union select 'Closed-Unfinished' as Ticketstatus, 9 as StatusSeq union select 'Closed-Finished' as Ticketstatus, 10 as StatusSeq union select 'Hold-Created' as Ticketstatus,11 as StatusSeq union select 'Hold-Assigned' as Ticketstatus,12 as StatusSeq union select 'Hold-Inprogress' as Ticketstatus, 13 as StatusSeq  union select 'Hold-Unfinished' as Ticketstatus, 14 as StatusSeq  union select 'Hold-Finished' as Ticketstatus, 15 as StatusSeq) tbl1, (select '0-5 days' as days, 1 as DaysSeq,'#CFD8DC' as color  union select '6-15 days' as days, 2 as DaysSeq,'#78909C' as color union select '16-30 days' as days, 3 as DaysSeq,'#455A64' as color union select '31-60 days' as days, 4 as DaysSeq,'#263238' as color) tbl2) tbl3 left outer join (select state || '-' || ticketstatus as Ticketstatus,days,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus,(case when day BETWEEN 0 AND 5 then '0-5 days' else case when day BETWEEN 6 AND 15 then '6-15 days' else case when day BETWEEN 16 AND 30 then '16-30 days' else case when day BETWEEN 31 AND 60 then '31-60 days' end end end end ) as days from(Select TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now())-date_trunc('day',CreationDate)) as  day  from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID ='"
						+ customerid
						+ "')) tbla) tbl group by state,ticketstatus,days order by state,ticketstatus,days) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus and tbl3.days=tbl4.days order by tbl3.StatusSeq,tbl3.DaysSeq desc";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> listOpenticketstatecountspiechartBySite(int siteid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color from dual union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color from dual union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color from dual ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>sysdate-60 and SiteID='"
				+ siteid
				+ "' )) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color  ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID='" + siteid
						+ "' ) tbla) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";
			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color  ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID='"
						+ siteid
						+ "' ) tbla) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> listOpenticketstatecountspiechartByUser(int userid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color from dual union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color from dual union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color from dual ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>sysdate-60 and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid
				+ "' and ActiveFlag=1)))) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color  ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
						+ userid
						+ "')) ) tbla) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";
			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color  ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
						+ userid
						+ "'))) tbla) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> listOpenticketstatecountspiechartByCustomer(int customerid,
			String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color from dual union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color from dual union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color from dual ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and creationdate>sysdate-60 and ticketstatus not in(6,7) and  SiteID in (Select SiteId from mSite where CustomerID='"
				+ customerid
				+ "') )) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color  ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and ticketstatus not in (6,7) and  SiteID in (Select SiteId from mSite where CustomerID='"
						+ customerid
						+ "')) tbla) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";
			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color  ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and creationdate>(now()-interval '60 day') and ticketstatus not in (6,7) and  SiteID in (Select SiteId from mSite where CustomerID='"
						+ customerid
						+ "')) tbla) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	@Override
	public List<OpenStateTicketsCount> listAllticketstatecountsByUser(int userid, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.state as state,tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,state,StateSeq,color from (select 'Created' as Ticketstatus,1 as StatusSeq,'#BDBDBD' as color from dual union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#80DEEA' as color from dual union select 'Inprogress' as Ticketstatus,3 as StatusSeq,'#7986CB' as color  from dual union select 'Finished' as Ticketstatus, 4 as StatusSeq,'#69F0AE' as color  from dual union select 'Unfinished' as Ticketstatus, 5 as StatusSeq,'#424242' as color  from dual) tbl2,(select 'Open' as state,1 as StateSeq from dual union select 'Closed' as state,2 as StateSeq from dual union select 'Hold' as state,3 as StateSeq  from dual )tbl5)tbl3 left outer join(select state , ticketstatus ,COUNT(TicketID) as ticketcount from(select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished'  end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and  ticketstatus not in(6,7) and creationdate>sysdate-60  and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid
				+ "' and ActiveFlag=1)))) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.state=tbl4.state and tbl3.Ticketstatus=tbl4.Ticketstatus  order by tbl3.StateSeq,tbl3.StatusSeq";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "select tbl3.state as state,tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,state,StateSeq,color from (select 'Created' as Ticketstatus,1 as StatusSeq,'#BDBDBD' as color  union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#80DEEA' as color  union select 'Inprogress' as Ticketstatus,3 as StatusSeq,'#7986CB' as color   union select 'Finished' as Ticketstatus, 4 as StatusSeq,'#69F0AE' as color   union select 'Unfinished' as Ticketstatus, 5 as StatusSeq,'#424242' as color  ) tbl2,(select 'Open' as state,1 as StateSeq  union select 'Closed' as state,2 as StateSeq  union select 'Hold' as state,3 as StateSeq   )tbl5)tbl3 left outer join(select state , ticketstatus ,COUNT(TicketID) as ticketcount from (select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished'  end end end end end) as TicketStatus from (Select TicketID,State,TicketStatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and  ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day')  and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
						+ userid
						+ "'))) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.state=tbl4.state and tbl3.Ticketstatus=tbl4.Ticketstatus  order by tbl3.StateSeq,tbl3.StatusSeq";
			} else {
				Query = "select tbl3.state as state,tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,state,StateSeq,color from (select 'Created' as Ticketstatus,1 as StatusSeq,'#BDBDBD' as color  union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#80DEEA' as color  union select 'Inprogress' as Ticketstatus,3 as StatusSeq,'#7986CB' as color   union select 'Finished' as Ticketstatus, 4 as StatusSeq,'#69F0AE' as color   union select 'Unfinished' as Ticketstatus, 5 as StatusSeq,'#424242' as color  ) tbl2,(select 'Open' as state,1 as StateSeq  union select 'Closed' as state,2 as StateSeq  union select 'Hold' as state,3 as StateSeq   )tbl5)tbl3 left outer join(select state , ticketstatus ,COUNT(TicketID) as ticketcount from (select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished'  end end end end end) as TicketStatus from (Select TicketID,State,TicketStatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and  ticketstatus not in(6,7) and creationdate >(now() -interval '60 day')  and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
						+ userid
						+ "'))) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.state=tbl4.state and tbl3.Ticketstatus=tbl4.Ticketstatus  order by tbl3.StateSeq,tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	@Override
	public List<OpenStateTicketsCount> listAllicketstatecountsBySite(int siteid, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.state as state,tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,state,StateSeq,color from (select 'Created' as Ticketstatus,1 as StatusSeq,'#BDBDBD' as color from dual union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#80DEEA' as color from dual union select 'Inprogress' as Ticketstatus,3 as StatusSeq,'#7986CB' as color  from dual union select 'Finished' as Ticketstatus, 4 as StatusSeq,'#69F0AE' as color  from dual union select 'Unfinished' as Ticketstatus, 5 as StatusSeq,'#424242' as color  from dual) tbl2,(select 'Open' as state,1 as StateSeq from dual union select 'Closed' as state,2 as StateSeq from dual union select 'Hold' as state,3 as StateSeq  from dual )tbl5)tbl3 left outer join(select state , ticketstatus ,COUNT(TicketID) as ticketcount from(select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished'  end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and  ticketstatus not in(6,7) and creationdate>sysdate-60 and SiteID='"
				+ siteid
				+ "' )) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.state=tbl4.state and tbl3.Ticketstatus=tbl4.Ticketstatus  order by tbl3.StateSeq,tbl3.StatusSeq";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "select tbl3.state as state,tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,state,StateSeq,color from (select 'Created' as Ticketstatus,1 as StatusSeq,'#BDBDBD' as color  union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#80DEEA' as color  union select 'Inprogress' as Ticketstatus,3 as StatusSeq,'#7986CB' as color   union select 'Finished' as Ticketstatus, 4 as StatusSeq,'#69F0AE' as color   union select 'Unfinished' as Ticketstatus, 5 as StatusSeq,'#424242' as color  ) tbl2,(select 'Open' as state,1 as StateSeq  union select 'Closed' as state,2 as StateSeq  union select 'Hold' as state,3 as StateSeq   )tbl5)tbl3 left outer join(select state , ticketstatus ,COUNT(TicketID) as ticketcount from(select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished'  end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and  ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + " > (now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID='" + siteid
						+ "' ) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.state=tbl4.state and tbl3.Ticketstatus=tbl4.Ticketstatus  order by tbl3.StateSeq,tbl3.StatusSeq";
			} else {
				Query = "select tbl3.state as state,tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,state,StateSeq,color from (select 'Created' as Ticketstatus,1 as StatusSeq,'#BDBDBD' as color  union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#80DEEA' as color  union select 'Inprogress' as Ticketstatus,3 as StatusSeq,'#7986CB' as color   union select 'Finished' as Ticketstatus, 4 as StatusSeq,'#69F0AE' as color   union select 'Unfinished' as Ticketstatus, 5 as StatusSeq,'#424242' as color  ) tbl2,(select 'Open' as state,1 as StateSeq  union select 'Closed' as state,2 as StateSeq  union select 'Hold' as state,3 as StateSeq   )tbl5)tbl3 left outer join(select state , ticketstatus ,COUNT(TicketID) as ticketcount from(select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished'  end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and  ticketstatus not in(6,7) and creationdate >(now() -interval '60 day') and SiteID='"
						+ siteid
						+ "' ) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.state=tbl4.state and tbl3.Ticketstatus=tbl4.Ticketstatus  order by tbl3.StateSeq,tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> listAllicketstatecountsByCustomer(int customerid, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.state as state,tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,state,StateSeq,color from (select 'Created' as Ticketstatus,1 as StatusSeq,'#BDBDBD' as color from dual union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#80DEEA' as color from dual union select 'Inprogress' as Ticketstatus,3 as StatusSeq,'#7986CB' as color  from dual union select 'Finished' as Ticketstatus, 4 as StatusSeq,'#69F0AE' as color  from dual union select 'Unfinished' as Ticketstatus, 5 as StatusSeq,'#424242' as color  from dual) tbl2,(select 'Open' as state,1 as StateSeq from dual union select 'Closed' as state,2 as StateSeq from dual union select 'Hold' as state,3 as StateSeq  from dual )tbl5)tbl3 left outer join(select state , ticketstatus ,COUNT(TicketID) as ticketcount from(select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished'  end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and  ticketstatus not in(6,7) and creationdate>sysdate-60  and SiteID in (Select SiteId from mSite where CustomerID ='"
				+ customerid
				+ "'))) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.state=tbl4.state and tbl3.Ticketstatus=tbl4.Ticketstatus  order by tbl3.StateSeq,tbl3.StatusSeq";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "select tbl3.state as state,tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,state,StateSeq,color from (select 'Created' as Ticketstatus,1 as StatusSeq,'#BDBDBD' as color  union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#80DEEA' as color  union select 'Inprogress' as Ticketstatus,3 as StatusSeq,'#7986CB' as color   union select 'Finished' as Ticketstatus, 4 as StatusSeq,'#69F0AE' as color   union select 'Unfinished' as Ticketstatus, 5 as StatusSeq,'#424242' as color  ) tbl2,(select 'Open' as state,1 as StateSeq  union select 'Closed' as state,2 as StateSeq  union select 'Hold' as state,3 as StateSeq   )tbl5)tbl3 left outer join(select state , ticketstatus ,COUNT(TicketID) as ticketcount from(select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished'  end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and  ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + " > (now()" + TimeConversionFromUTC
						+ " -interval '60 day')  and SiteID in (Select SiteId from mSite where CustomerID ='"
						+ customerid
						+ "')) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.state=tbl4.state and tbl3.Ticketstatus=tbl4.Ticketstatus  order by tbl3.StateSeq,tbl3.StatusSeq";
			} else {
				Query = "select tbl3.state as state,tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,state,StateSeq,color from (select 'Created' as Ticketstatus,1 as StatusSeq,'#BDBDBD' as color  union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#80DEEA' as color  union select 'Inprogress' as Ticketstatus,3 as StatusSeq,'#7986CB' as color   union select 'Finished' as Ticketstatus, 4 as StatusSeq,'#69F0AE' as color   union select 'Unfinished' as Ticketstatus, 5 as StatusSeq,'#424242' as color  ) tbl2,(select 'Open' as state,1 as StateSeq  union select 'Closed' as state,2 as StateSeq  union select 'Hold' as state,3 as StateSeq   )tbl5)tbl3 left outer join(select state , ticketstatus ,COUNT(TicketID) as ticketcount from(select TicketID,(case when  state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end  end) as State,( case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished'  end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and  ticketstatus not in(6,7) and creationdate > (now() -interval '60 day')  and SiteID in (Select SiteId from mSite where CustomerID ='"
						+ customerid
						+ "')) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.state=tbl4.state and tbl3.Ticketstatus=tbl4.Ticketstatus  order by tbl3.StateSeq,tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<TicketDetail> listDaylapsedsincecreationfilterTickets(int state, int status, int fromday, int today,
			int userid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,nvl(assignedto,'0') as assignedto,scheduledon,TicketID,State,ticketstatus from(Select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,assignedto,scheduledon,TicketID,State,TicketStatus,round(trunc(sysdate)-trunc(CreationDate)) as  day  from tTicketDetail where ActiveFlag='1' and state='"
				+ state + "' and ticketstatus='" + status
				+ "' and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid + "' and ActiveFlag=1))) tbl where tbl.day between '" + fromday + "' and '" + today + "' ";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,coalesce(assignedto,'0') as assignedto,scheduledon,TicketID,State,ticketstatus from(Select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,assignedto,scheduledon,TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now()"
						+ TimeConversionFromUTC + ")-date_trunc('day',CreationDate" + TimeConversionFromUTC
						+ ")) as  day  from tTicketDetail where ActiveFlag='1' and state='" + state
						+ "' and ticketstatus='" + status
						+ "' and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
						+ userid + "'))) tbl where tbl.day between '" + fromday + "' and '" + today + "' ";
			} else {
				Query = "select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,coalesce(assignedto,'0') as assignedto,scheduledon,TicketID,State,ticketstatus from(Select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,assignedto,scheduledon,TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now())-date_trunc('day',CreationDate)) as  day  from tTicketDetail where ActiveFlag='1' and state='"
						+ state + "' and ticketstatus='" + status
						+ "' and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
						+ userid + "'))) tbl where tbl.day between '" + fromday + "' and '" + today + "' ";
			}
		}

		List<TicketDetail> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<TicketDetail> listDaylapsedsincecreationfilterTicketsBySite(int state, int status, int fromday,
			int today, int siteid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,nvl(assignedto,'0') as assignedto,scheduledon,TicketID,State,ticketstatus from(Select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,assignedto,scheduledon,TicketID,State,TicketStatus,round(trunc(sysdate)-trunc(CreationDate)) as  day  from tTicketDetail where ActiveFlag='1' and state='"
				+ state + "' and ticketstatus='" + status + "' and SiteID='" + siteid + "') tbl where tbl.day between '"
				+ fromday + "' and '" + today + "'";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,coalesce(assignedto,'0') as assignedto,scheduledon,TicketID,State,ticketstatus from(Select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,assignedto,scheduledon,TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now()"
						+ TimeConversionFromUTC + ")-date_trunc('day',CreationDate" + TimeConversionFromUTC
						+ ")) as  day  from tTicketDetail where ActiveFlag='1' and state='" + state
						+ "' and ticketstatus='" + status + "' and SiteID='" + siteid + "') tbl where tbl.day between '"
						+ fromday + "' and '" + today + "'";
			} else {
				Query = "select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,coalesce(assignedto,'0') as assignedto,scheduledon,TicketID,State,ticketstatus from(Select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,assignedto,scheduledon,TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now())-date_trunc('day',CreationDate)) as  day  from tTicketDetail where ActiveFlag='1' and state='"
						+ state + "' and ticketstatus='" + status + "' and SiteID='" + siteid
						+ "') tbl where tbl.day between '" + fromday + "' and '" + today + "'";
			}
		}

		List<TicketDetail> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<TicketDetail> listDaylapsedsincecreationfilterTicketsByCustomer(int state, int status, int fromday,
			int today, int customerid, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,nvl(assignedto,'0') as assignedto,scheduledon,TicketID,State,ticketstatus from(Select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,assignedto,scheduledon,TicketID,State,TicketStatus,round(trunc(sysdate)-trunc(CreationDate)) as  day  from tTicketDetail where ActiveFlag='1' and state='"
				+ state + "' and ticketstatus='" + status
				+ "' and SiteID in (Select SiteId from mSite where CustomerID ='" + customerid
				+ "')) tbl where tbl.day between '" + fromday + "' and '" + today + "'";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				Query = "select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,coalesce(assignedto,'0') as assignedto,scheduledon,TicketID,State,ticketstatus from(Select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,assignedto,scheduledon,TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now()"
						+ TimeConversionFromUTC + ")-date_trunc('day',CreationDate" + TimeConversionFromUTC
						+ ")) as  day  from tTicketDetail where ActiveFlag='1' and state='" + state
						+ "' and ticketstatus='" + status
						+ "' and SiteID in (Select SiteId from mSite where CustomerID ='" + customerid
						+ "')) tbl where tbl.day between '" + fromday + "' and '" + today + "'";
			} else {
				Query = "select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,coalesce(assignedto,'0') as assignedto,scheduledon,TicketID,State,ticketstatus from(Select siteid,ticketcode,priority,creationdate,ticketcategory,ticketdetail,assignedto,scheduledon,TicketID,State,TicketStatus,DATE_PART('day',date_trunc('day',now())-date_trunc('day',CreationDate)) as  day  from tTicketDetail where ActiveFlag='1' and state='"
						+ state + "' and ticketstatus='" + status
						+ "' and SiteID in (Select SiteId from mSite where CustomerID ='" + customerid
						+ "')) tbl where tbl.day between '" + fromday + "' and '" + today + "'";
			}
		}

		List<TicketDetail> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<TicketDetail> getTicketDetailListByCustomerLimit(int customerid, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames
				+ " from tTicketDetail where  ActiveFlag='1'  and SiteID  in (Select SiteId from mSite where CustomerID ='"
				+ customerid + "') order by ticketcode desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";

			}

			Query = "Select " + tablecolumnnames
					+ " from tTicketDetail where  ActiveFlag='1'  and SiteID  in (Select SiteId from mSitemap where UserID ='"
					+ customerid + "') order by ticketid desc limit 10";

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	public List<TicketDetail> getTicketDetailListBySiteLimit(int siteid, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and SiteID='" + siteid
				+ "' order by ticketcode desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";

			}

			Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and SiteID='" + siteid
					+ "' order by ticketid desc limit 10";

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	public List<TicketDetail> listStatefilterTickets(String state, int userid, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + state
				+ "' and  creationdate>sysdate-60 and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid + "' and ActiveFlag=1)) order by ticketcode desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and " + state
						+ " and  creationdate" + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSitemap where UserID='"
						+ userid + "' and ActiveFlag=1) order by ticketcode desc";
			} else {
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and " + state
						+ " and  creationdate>(now() -interval '60 day') and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSitemap where UserID='"
						+ userid + "' and ActiveFlag=1) order by ticketcode desc";
			}

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	public List<TicketDetail> listStatefilterTicketsBySite(String state, int siteid, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + state
				+ "' and  creationdate>sysdate-60 and ticketstatus not in(6,7) and SiteID='" + siteid
				+ "' order by ticketcode desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and " + state
						+ " and  creationdate" + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and ticketstatus not in(6,7) and SiteID='" + siteid
						+ "' order by ticketcode desc";
			} else {
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and " + state
						+ " and  creationdate>(now() -interval '60 day') and ticketstatus not in(6,7) and SiteID='"
						+ siteid + "' order by ticketcode desc";
			}

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	public List<TicketDetail> listStatefilterTicketsByCustomer(String state, int customerid, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + state
				+ "' and  creationdate>sysdate-60 and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID ='"
				+ customerid + "') order by ticketcode desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and " + state
						+ " and   creationdate" + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSitemap where UserID ='"
						+ customerid + "') order by ticketcode desc";
			} else {
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and " + state
						+ " and   creationdate>(now() -interval '60 day') and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSitemap where UserID ='"
						+ customerid + "') order by ticketcode desc";
			}

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	public List<TicketDetail> listStateandStatusfilterTickets(int state, int status, int userid,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + state
				+ "' and Ticketstatus='" + status
				+ "' and  creationdate>sysdate-60 and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid + "' and ActiveFlag=1)) order by ticketcode desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + state
						+ "' and Ticketstatus='" + status + "' and  creationdate" + TimeConversionFromUTC + ">(now()"
						+ TimeConversionFromUTC
						+ " -interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
						+ userid + "')) order by ticketcode desc";
			} else {
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + state
						+ "' and Ticketstatus='" + status
						+ "' and  creationdate>(now() -interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
						+ userid + "')) order by ticketcode desc";
			}

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	public List<TicketDetail> listStateandStatusfilterTicketsBySite(int state, int status, int siteid,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + state
				+ "' and Ticketstatus='" + status + "' and SiteID='" + siteid
				+ "' and creationdate>sysdate-60 order by ticketcode desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + state
						+ "' and Ticketstatus='" + status + "' and SiteID='" + siteid + "' and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') order by ticketcode desc";
			} else {
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + state
						+ "' and Ticketstatus='" + status + "' and SiteID='" + siteid
						+ "' and creationdate>(now() -interval '60 day') order by ticketcode desc";
			}

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	public List<TicketDetail> listStateandStatusfilterTicketsByCustomer(int state, int status, int customerid,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + state
				+ "' and Ticketstatus='" + status
				+ "' and creationdate>sysdate-60 and SiteID in (Select SiteId from mSite where CustomerID ='"
				+ customerid + "') order by ticketcode desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + state
						+ "' and Ticketstatus='" + status + "' and creationdate" + TimeConversionFromUTC + ">(now()"
						+ TimeConversionFromUTC
						+ " -interval '60 day') and SiteID in (Select SiteId from mSitemap where UserID ='" + customerid
						+ "') order by ticketcode desc";
			} else {
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + state
						+ "' and Ticketstatus='" + status
						+ "' and creationdate>(now() -interval '60 day') and SiteID in (Select SiteId from mSitemap where UserID ='"
						+ customerid + "') order by ticketcode desc";
			}

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	@Override
	public List<TicketDetail> findByEquipmentId(int siteId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where ActiveFlag='1' SiteID='" + siteId
				+ "'";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1' and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and EquipmentID='" + siteId + "' order by ticketcode desc";
			} else {
				Query = "Select " + tablecolumnnames
						+ " from tTicketDetail where  ActiveFlag='1' and creationdate>(now() -interval '60 day') and EquipmentID='"
						+ siteId + "' order by ticketcode desc";
			}
		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;
		/*
		 * Date todate = new Date(); Calendar calendar = Calendar.getInstance();
		 * calendar.add(Calendar.MONTH, -2); Date fromDate = calendar.getTime(); //
		 * DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //
		 * String fromDateInString = dateFormat.format(fromDate); // String
		 * toDateInString = dateFormat.format(todate); Session session =
		 * sessionFactory.getCurrentSession(); String Query =
		 * "FROM TicketDetail WHERE EquipmentID=:EquipmentID AND CreationDate BETWEEN :fromDate AND :todate"
		 * ; Query query = session.createQuery(Query); query.setParameter("EquipmentID",
		 * equipmentId); query.setParameter("fromDate", fromDate);
		 * query.setParameter("todate", todate); return query.list();
		 */
	}

// 03-02-19
	/*
	 * public List<OpenStateTicketsCount> Operationallstatepiechart(int userid,
	 * String TimezoneOffset){
	 * 
	 * Session session = sessionFactory.getCurrentSession(); //Oracle String Query =
	 * "select tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color from dual union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color from dual union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color from dual ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>sysdate-60 and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * +
	 * userid+"' and ActiveFlag=1)))) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq"
	 * ;
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
	 * Query =
	 * "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and tickettype='Operation' and  ticketstatus not in(6,7) and creationdate"
	 * + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC +
	 * " -interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in (Select CustomerID from mCustomerMap where UserID='"
	 * +
	 * userid+"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid +
	 * "')))) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq"
	 * ; } else { Query =
	 * "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and tickettype='Operation' and  ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * +
	 * userid+"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid +
	 * "')))) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq"
	 * ;
	 * 
	 * } }
	 * 
	 * List<OpenStateTicketsCount> TicketDetailList =
	 * session.createSQLQuery(Query).list(); return TicketDetailList;
	 * 
	 * }
	 */

	public List<OpenStateTicketsCount> Operationallstatepiechart(int userid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color from dual union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color from dual union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color from dual ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>sysdate-60 and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid
				+ "' and ActiveFlag=1)))) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and tickettype='Operation' and  ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID in (Select SiteId from mSiteMap where UserID='" + userid
						+ "' and ActiveFlag=1)) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";
			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and tickettype='Operation' and  ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in (Select SiteId from mSiteMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1)) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}
// 03-02-19
	/*
	 * public List<OpenStateTicketsCount> Maintenanceallstatepiechart(int userid,
	 * String TimezoneOffset){
	 * 
	 * Session session = sessionFactory.getCurrentSession(); //Oracle String Query =
	 * "select tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color from dual union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color from dual union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color from dual ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>sysdate-60 and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * +
	 * userid+"' and ActiveFlag=1)))) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq"
	 * ;
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
	 * Query =
	 * "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and tickettype='Maintenance'  and  ticketstatus not in(6,7) and creationdate"
	 * + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC +
	 * " -interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * +
	 * userid+"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid +
	 * "')))) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq"
	 * ;
	 * 
	 * } else { Query =
	 * "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and tickettype='Maintenance' and  ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * +
	 * userid+"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid +
	 * "')))) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq"
	 * ;
	 * 
	 * } }
	 * 
	 * List<OpenStateTicketsCount> TicketDetailList =
	 * session.createSQLQuery(Query).list(); return TicketDetailList;
	 * 
	 * }
	 */

	public List<OpenStateTicketsCount> Maintenanceallstatepiechart(int userid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color from dual union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color from dual union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color from dual ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>sysdate-60 and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid
				+ "' and ActiveFlag=1)))) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and tickettype='Maintenance'  and  ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID in (Select SiteId from mSiteMap where UserID='" + userid
						+ "' and ActiveFlag=1)) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and tickettype='Maintenance' and  ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in (Select SiteId from mSiteMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1)) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}
// 03-02-19
	/*
	 * public List<OpenStateTicketsCount> Allstatepiechart(int userid, String
	 * TimezoneOffset){
	 * 
	 * Session session = sessionFactory.getCurrentSession(); //Oracle String Query =
	 * "select tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color from dual union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color from dual union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color from dual ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>sysdate-60 and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * +
	 * userid+"' and ActiveFlag=1)))) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq"
	 * ;
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
	 * Query =
	 * "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and  ticketstatus not in(6,7) and creationdate"
	 * + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC +
	 * " -interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * +
	 * userid+"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid +
	 * "')))) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq"
	 * ; } else {
	 * 
	 * Query =
	 * "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and  ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * +
	 * userid+"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid +
	 * "')))) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq"
	 * ; } }
	 * 
	 * List<OpenStateTicketsCount> TicketDetailList =
	 * session.createSQLQuery(Query).list(); return TicketDetailList;
	 * 
	 * }
	 */

	public List<OpenStateTicketsCount> Allstatepiechart(int userid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color from dual union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color from dual union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color from dual ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>sysdate-60 and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid
				+ "' and ActiveFlag=1)))) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and  ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID in  (Select SiteId from mSiteMap where UserID='" + userid
						+ "' and ActiveFlag=1)) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";
			} else {

				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and  ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in (Select SiteId from mSiteMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1)) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

// 03-02-19
	/*
	 * public List<OpenStateTicketsCount> Openstateandstatustikcets(int userid,
	 * String TimezoneOffset){
	 * 
	 * Session session = sessionFactory.getCurrentSession(); //Oracle String Query =
	 * "select tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color from dual union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color from dual union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color from dual ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>sysdate-60 and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * +
	 * userid+"' and ActiveFlag=1)))) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq"
	 * ;
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
	 * Query="select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#FF0000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#FF0000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#FF0000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#FF0000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#FF0000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(1) and ticketstatus not in(6,7) and creationdate"
	 * + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC +
	 * " -interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * +userid+"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid +
	 * "')))) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq"
	 * ;
	 * 
	 * } else {
	 * Query="select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#FF0000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#FF0000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#FF0000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#FF0000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#FF0000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(1) and ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * +userid+"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid +
	 * "')))) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq"
	 * ; } }
	 * 
	 * List<OpenStateTicketsCount> TicketDetailList =
	 * session.createSQLQuery(Query).list(); return TicketDetailList;
	 * 
	 * }
	 */

	public List<OpenStateTicketsCount> Openstateandstatustikcets(int userid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color from dual union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color from dual union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color from dual ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>sysdate-60 and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid
				+ "' and ActiveFlag=1)))) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#FF0000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#FF0000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#FF0000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#FF0000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#FF0000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(1) and ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID in (Select SiteId from mSiteMap where UserID='" + userid
						+ "' and ActiveFlag=1)) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";

			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#FF0000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#FF0000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#FF0000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#FF0000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#FF0000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(1) and ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in (Select SiteId from mSiteMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1)) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

// 03-02-19
	/*
	 * public List<OpenStateTicketsCount> Closestateandstatustikcets(int userid,
	 * String TimezoneOffset){
	 * 
	 * Session session = sessionFactory.getCurrentSession(); //Oracle String Query =
	 * "select tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color from dual union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color from dual union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color from dual ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>sysdate-60 and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * +
	 * userid+"' and ActiveFlag=1)))) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq"
	 * ;
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
	 * Query="select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#008000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#008000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#008000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#008000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#008000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=2 then 'Close' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(2) and ticketstatus not in(6,7) and creationdate"
	 * + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC +
	 * " -interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * +userid+"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid +
	 * "')))) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq"
	 * ;
	 * 
	 * } else {
	 * Query="select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#008000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#008000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#008000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#008000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#008000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=2 then 'Close' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(2) and ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * +userid+"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid +
	 * "')))) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq"
	 * ; } }
	 * 
	 * List<OpenStateTicketsCount> TicketDetailList =
	 * session.createSQLQuery(Query).list(); return TicketDetailList;
	 * 
	 * }
	 */

	public List<OpenStateTicketsCount> Closestateandstatustikcets(int userid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color from dual union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color from dual union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color from dual ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>sysdate-60 and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid
				+ "' and ActiveFlag=1)))) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#008000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#008000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#008000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#008000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#008000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=2 then 'Close' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(2) and ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID in (Select SiteId from mSiteMap where UserID='" + userid
						+ "' and ActiveFlag=1)) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";

			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#008000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#008000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#008000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#008000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#008000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=2 then 'Close' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(2) and ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in (Select SiteId from mSiteMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1)) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

// 03-02-19
	/*
	 * public List<OpenStateTicketsCount> Holdstateandstatustikcets(int userid,
	 * String TimezoneOffset){
	 * 
	 * Session session = sessionFactory.getCurrentSession(); //Oracle String Query =
	 * "select tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color from dual union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color from dual union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color from dual ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>sysdate-60 and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * +
	 * userid+"' and ActiveFlag=1)))) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq"
	 * ;
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
	 * Query="select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#808080' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#808080' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#808080' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#808080' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#808080' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=3 then 'Hold' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(3) and ticketstatus not in(6,7) and creationdate"
	 * + TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC +
	 * " -interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * +userid+"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid +
	 * "')))) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq"
	 * ;
	 * 
	 * } else {
	 * Query="select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#808080' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#808080' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#808080' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#808080' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#808080' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=3 then 'Hold' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(3) and ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in (Select SiteId from mSite where CustomerID in (select CustomerID from mcustomer where customerid in(Select CustomerID from mCustomerMap where UserID='"
	 * +userid+"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid +
	 * "')))) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq"
	 * ; } }
	 * 
	 * List<OpenStateTicketsCount> TicketDetailList =
	 * session.createSQLQuery(Query).list(); return TicketDetailList;
	 * 
	 * }
	 */

	public List<OpenStateTicketsCount> Holdstateandstatustikcets(int userid, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "select tbl3.Ticketstatus as Ticketstatus,nvl(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#64b5b8' as color from dual union select 'Hold' as Ticketstatus,2 as StatusSeq,'#192438' as color from dual union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#7DE789' as color from dual ) tbl1) tbl3 left outer join (select state ,COUNT(TicketID) as ticketcount from (select TicketID,(case when state=1 then 'Open' else case when state=2 then 'Closed' else case when state=3 then 'Hold' end end end ) as State from(Select TicketID,State from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and ticketstatus not in(6,7) and creationdate>sysdate-60 and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid
				+ "' and ActiveFlag=1)))) tbl group by state order by state) tbl4 on tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#808080' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#808080' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#808080' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#808080' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#808080' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=3 then 'Hold' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(3) and ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID in (Select SiteId from mSiteMap where UserID='" + userid
						+ "' and ActiveFlag=1)) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";

			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#808080' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#808080' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#808080' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#808080' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#808080' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=3 then 'Hold' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(3) and ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in (Select SiteId from mSiteMap where UserID='"
						+ userid
						+ "' and ActiveFlag=1)) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> AllstatepiechartBySite(int siteId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and  ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID='" + siteId
						+ "') tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";
			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and  ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID='"
						+ siteId
						+ "') tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> OperationallstatepiechartBySite(int siteId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and tickettype='Operation' and  ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID='" + siteId
						+ "') tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";
			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and tickettype='Operation' and  ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID='"
						+ siteId
						+ "') tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> MaintenanceallstatepiechartBySite(int siteId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "";
		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and tickettype='Maintenance'  and  ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID='" + siteId
						+ "') tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and tickettype='Maintenance' and  ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID='"
						+ siteId
						+ "') tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> OpenstateandstatustikcetsBySite(int siteId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#FF0000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#FF0000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#FF0000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#FF0000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#FF0000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(1) and ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID='" + siteId
						+ "') tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";

			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#FF0000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#FF0000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#FF0000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#FF0000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#FF0000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(1) and ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID='"
						+ siteId
						+ "') tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> ClosestateandstatustikcetsBySite(int siteId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#008000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#008000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#008000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#008000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#008000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=2 then 'Close' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(2) and ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID='" + siteId
						+ "') tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";

			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#008000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#008000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#008000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#008000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#008000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=2 then 'Close' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(2) and ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID='"
						+ siteId
						+ "') tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> HoldstateandstatustikcetsBySite(int siteId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#808080' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#808080' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#808080' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#808080' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#808080' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=3 then 'Hold' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(3) and ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID='" + siteId
						+ "') tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";

			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#808080' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#808080' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#808080' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#808080' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#808080' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=3 then 'Hold' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(3) and ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID='"
						+ siteId
						+ "') tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> AllstatepiechartByCustomer(int customerId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and  ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID in(select siteid from msitemap where userid='" + customerId
						+ "')) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and  ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in(select siteid from msitemap where userid='"
						+ customerId
						+ "')) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> OperationallstatepiechartByCustomer(int customerId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and tickettype='Operation' and  ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID in (select siteid from msitemap where userid='" + customerId
						+ "')) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";
			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3)  and tickettype='Operation' and  ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in (select siteid from msitemap where userid='"
						+ customerId
						+ "')) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> MaintenanceallstatepiechartByCustomer(int customerId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "";
		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and tickettype='Maintenance'  and  ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID in (select siteid from msitemap where userid='" + customerId
						+ "')) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";
			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from (select 'Open' as Ticketstatus,1 as StatusSeq,'#FF0000' as color  union select 'Hold' as Ticketstatus,2 as StatusSeq,'#808080' as color  union select 'Closed' as Ticketstatus, 3 as StatusSeq,'#008000' as color union select 'Inprogress' as Ticketstatus, 4 as StatusSeq,'#FFFF00' as color  ) tbl1) tbl3 left outer join (select state,state1,COUNT(TicketID) as ticketcount from (select TicketID,(case when (state=1 and ticketstatus not in(3)) then 'Open' else case when (state=2 and ticketstatus not in(3)) then 'Closed' else case when (state=3 and ticketstatus not in(3)) then 'Hold' end end end ) as state,(case when ticketstatus=3 then 'Inprogress' end) as state1 from(Select TicketID,State,ticketstatus from tTicketDetail where ActiveFlag='1' and state in(1,2,3) and tickettype='Maintenance' and  ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in (select siteid from msitemap where userid='"
						+ customerId
						+ "')) tbla) tbl group by state,state1 order by state) tbl4 on tbl3.Ticketstatus=tbl4.state1 or tbl3.Ticketstatus=tbl4.state  order by tbl3.StatusSeq";

			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> OpenstateandstatustikcetsByCustomer(int customerId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#FF0000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#FF0000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#FF0000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#FF0000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#FF0000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(1) and ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID in(select siteid from msitemap where userid='" + customerId
						+ "')) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";

			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#FF0000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#FF0000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#FF0000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#FF0000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#FF0000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=1 then 'Open' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(1) and ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in(select siteid from msitemap where userid='"
						+ customerId
						+ "')) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> ClosestateandstatustikcetsByCustomer(int customerId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#008000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#008000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#008000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#008000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#008000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=2 then 'Close' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(2) and ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID in(select siteid from msitemap where userid='" + customerId
						+ "')) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";

			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#008000' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#008000' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#008000' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#008000' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#008000' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=2 then 'Close' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(2) and ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in(select siteid from msitemap where userid='"
						+ customerId
						+ "')) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

	public List<OpenStateTicketsCount> HoldstateandstatustikcetsByCustomer(int customerId, String TimezoneOffset) {

		Session session = sessionFactory.getCurrentSession();
		// Oracle
		String Query = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute')";
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#808080' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#808080' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#808080' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#808080' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#808080' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=3 then 'Hold' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(3) and ticketstatus not in(6,7) and creationdate"
						+ TimeConversionFromUTC + ">(now()" + TimeConversionFromUTC
						+ " -interval '60 day') and SiteID in(select siteid from msitemap where userid='" + customerId
						+ "')) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";

			} else {
				Query = "select tbl3.Ticketstatus as Ticketstatus,coalesce(ticketcount,0) as ticketcount,tbl3.color as color from (select Ticketstatus,StatusSeq,color from  (select 'Created' as Ticketstatus,1 as StatusSeq,'#808080' as color union select 'Assigned' as Ticketstatus,2 as StatusSeq,'#808080' as color union  select 'Inprogress' as Ticketstatus, 3 as StatusSeq,'#808080' as color  union select 'Unfinished' as Ticketstatus, 4 as StatusSeq,'#808080' as color union select 'Finished' as Ticketstatus, 5 as StatusSeq,'#808080' as color) tbl1)tbl3   left outer join (select ticketstatus as Ticketstatus,COUNT(TicketID) as ticketcount from(select TicketID,(case when state=3 then 'Hold' end ) as State,(case when ticketstatus=1 then 'Created' else case when ticketstatus=2 then 'Assigned' else case when ticketstatus=3 then 'Inprogress' else case when ticketstatus=4 then 'Unfinished' else case when ticketstatus=5 then 'Finished' else case when ticketstatus=6 then 'Closed' else case when ticketstatus=7 then 'Hold'  end end end end end end end) as TicketStatus from(Select TicketID,State,TicketStatus  from tTicketDetail where ActiveFlag='1' and state in(3) and ticketstatus not in(6,7) and creationdate>(now()-interval '60 day') and SiteID in(select siteid from msitemap where userid='"
						+ customerId
						+ "')) tbla) tbl group by state,ticketstatus order by state,ticketstatus) tbl4 on tbl3.Ticketstatus=tbl4.Ticketstatus order by tbl3.StatusSeq";
			}
		}

		List<OpenStateTicketsCount> TicketDetailList = session.createSQLQuery(Query).list();
		return TicketDetailList;

	}

// 03-02-19
	/*
	 * public List<TicketDetail> listStatefilterTickets(String type,String state,int
	 * userid, String TimezoneOffset){ Session session =
	 * sessionFactory.getCurrentSession(); List<TicketDetail> TicketDetailList = new
	 * ArrayList<TicketDetail>();
	 * 
	 * //Oracle String tablecolumnnames =
	 * "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag"
	 * ; String Query = "Select " + tablecolumnnames +
	 * " from tTicketDetail where  ActiveFlag='1'  and State='"+ state
	 * +"' and  creationdate>sysdate-60 and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid +"' and ActiveFlag=1)) order by ticketcode desc";
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * tablecolumnnames =
	 * "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
	 * + TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" +
	 * TimeConversionFromUTC +
	 * ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
	 * + TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" +
	 * TimeConversionFromUTC +
	 * ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
	 * + TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp" +
	 * TimeConversionFromUTC + ") as completedtimestamp,(completedon" +
	 * TimeConversionFromUTC +
	 * ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag"
	 * ; Query = "Select " + tablecolumnnames +
	 * " from tTicketDetail where  ActiveFlag='1'  and tickettype='"+ type
	 * +"' and "+ state +" and  creationdate" + TimeConversionFromUTC + ">(now()" +
	 * TimeConversionFromUTC +
	 * " -interval '60 day') and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID in(Select CustomerID from mCustomer where CustomerId in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid
	 * +"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid + "'))) order by ticketcode desc"; } else { Query = "Select " +
	 * tablecolumnnames +
	 * " from tTicketDetail where  ActiveFlag='1'  and tickettype='"+ type +
	 * "' and "+ state
	 * +" and  creationdate>(now() -interval '60 day') and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID in(Select CustomerID from mCustomer where CustomerId in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid
	 * +"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid + "'))) order by ticketcode desc"; }
	 * 
	 * }
	 * 
	 * List<TicketDetail> TicketDetailListObject =
	 * session.createSQLQuery(Query).list(); TicketDetailList =
	 * GetCastedTicketDetailList(TicketDetailListObject);
	 * 
	 * 
	 * return TicketDetailList;
	 * 
	 * }
	 */

	public List<TicketDetail> listStatefilterTickets(String type, String state, int userid, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + state
				+ "' and  creationdate>sysdate-60 and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid + "' and ActiveFlag=1)) order by ticketcode desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and tickettype='"
						+ type + "' and " + state + " and  creationdate" + TimeConversionFromUTC + ">(now()"
						+ TimeConversionFromUTC
						+ " -interval '60 day') and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSiteMap where UserID='"
						+ userid + "' and ActiveFlag=1) order by ticketcode desc";
			} else {
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and tickettype='"
						+ type + "' and " + state
						+ " and  creationdate>(now() -interval '60 day') and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSiteMap where UserID='"
						+ userid + "' and ActiveFlag=1) order by ticketcode desc";
			}

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

// 03-02-19
	/*
	 * public List<TicketDetail> AllStateandStatusfilterTickets(String type,int
	 * status,int userid, String TimezoneOffset){ Session session =
	 * sessionFactory.getCurrentSession(); List<TicketDetail> TicketDetailList = new
	 * ArrayList<TicketDetail>();
	 * 
	 * //Oracle String tablecolumnnames =
	 * "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag"
	 * ; String Query = "Select " + tablecolumnnames +
	 * " from tTicketDetail where  ActiveFlag='1'  and State='"+ type
	 * +"' and  creationdate>sysdate-60 and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid +"' and ActiveFlag=1)) order by ticketcode desc";
	 * 
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) { String
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * tablecolumnnames =
	 * "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
	 * + TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" +
	 * TimeConversionFromUTC +
	 * ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
	 * + TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" +
	 * TimeConversionFromUTC +
	 * ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
	 * + TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp" +
	 * TimeConversionFromUTC + ") as completedtimestamp,(completedon" +
	 * TimeConversionFromUTC +
	 * ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag"
	 * ; Query = "Select " + tablecolumnnames +
	 * " from tTicketDetail where  ActiveFlag='1'  and ticketstatus='"+ status
	 * +"' and State='"+ type +"' and  creationdate" + TimeConversionFromUTC +
	 * ">(now()" + TimeConversionFromUTC +
	 * " -interval '60 day') and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID in(Select CustomerID from mCustomer where CustomerId in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid
	 * +"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid + "'))) order by ticketcode desc"; } else { Query = "Select " +
	 * tablecolumnnames +
	 * " from tTicketDetail where  ActiveFlag='1'  and ticketstatus='"+ status +
	 * "' and State='"+ type
	 * +"' and  creationdate>(now() -interval '60 day') and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID in(Select CustomerID from mCustomer where CustomerId in (Select CustomerID from mCustomerMap where UserID='"
	 * + userid
	 * +"' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userid + "'))) order by ticketcode desc"; }
	 * 
	 * }
	 * 
	 * List<TicketDetail> TicketDetailListObject =
	 * session.createSQLQuery(Query).list(); TicketDetailList =
	 * GetCastedTicketDetailList(TicketDetailListObject);
	 * 
	 * 
	 * return TicketDetailList;
	 * 
	 * }
	 */

	public List<TicketDetail> AllStateandStatusfilterTickets(String type, int status, int userid,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + type
				+ "' and  creationdate>sysdate-60 and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userid + "' and ActiveFlag=1)) order by ticketcode desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and ticketstatus='"
						+ status + "' and State='" + type + "' and  creationdate" + TimeConversionFromUTC + ">(now()"
						+ TimeConversionFromUTC
						+ " -interval '60 day') and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSiteMap where UserID='"
						+ userid + "' and ActiveFlag=1) order by ticketcode desc";
			} else {
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and ticketstatus='"
						+ status + "' and State='" + type
						+ "' and  creationdate>(now() -interval '60 day') and ticketstatus not in(6,7) and SiteID in (Select SiteId from mSiteMap where UserID='"
						+ userid + "' and ActiveFlag=1) order by ticketcode desc";
			}

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	public List<TicketDetail> AllStateandStatusfilterTicketsBySite(String type, int status, int siteId,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + type
				+ "' and  creationdate>sysdate-60 and ticketstatus not in(6,7) and SiteID='" + siteId
				+ "' order by ticketcode desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and ticketstatus='"
						+ status + "' and State='" + type + "' and  creationdate" + TimeConversionFromUTC + ">(now()"
						+ TimeConversionFromUTC + " -interval '60 day') and ticketstatus not in(6,7) and SiteID='"
						+ siteId + "' order by ticketcode desc";
			} else {
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and ticketstatus='"
						+ status + "' and State='" + type
						+ "' and  creationdate>(now() -interval '60 day') and ticketstatus not in(6,7) and SiteID='"
						+ siteId + "' order by ticketcode desc";
			}

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	public List<TicketDetail> listStatefilterTicketsBySite(String type, String state, int siteId,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + state
				+ "' and  creationdate>sysdate-60 and ticketstatus not in(6,7) and SiteID='" + siteId
				+ "' order by ticketcode desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and tickettype='"
						+ type + "' and " + state + " and  creationdate" + TimeConversionFromUTC + ">(now()"
						+ TimeConversionFromUTC + " -interval '60 day') and ticketstatus not in(6,7) and SiteID='"
						+ siteId + "' order by ticketcode desc";
			} else {
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and tickettype='"
						+ type + "' and " + state
						+ " and  creationdate>(now() -interval '60 day') and ticketstatus not in(6,7) and SiteID='"
						+ siteId + "' order by ticketcode desc";
			}

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	public List<TicketDetail> AllStateandStatusfilterTicketsByCustomer(String type, int status, int customerId,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + type
				+ "' and  creationdate>sysdate-60 and ticketstatus not in(6,7) and SiteID in(select siteid from msite where customerid='"
				+ customerId + "') order by ticketcode desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and ticketstatus='"
						+ status + "' and State='" + type + "' and  creationdate" + TimeConversionFromUTC + ">(now()"
						+ TimeConversionFromUTC
						+ " -interval '60 day') and ticketstatus not in(6,7) and SiteID in(select siteid from msitemap where userid='"
						+ customerId + "') order by ticketcode desc";
			} else {
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and ticketstatus='"
						+ status + "' and State='" + type
						+ "' and  creationdate>(now() -interval '60 day') and ticketstatus not in(6,7) and SiteID in(select siteid from msitemap where userid='"
						+ customerId + "') order by ticketcode desc";
			}

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

	public List<TicketDetail> listTypefilterTicketsByCustomer(String type, String state, int customerId,
			String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and State='" + state
				+ "' and  creationdate>sysdate-60 and ticketstatus not in(6,7) and SiteID in(select siteid from msitemap where userid='"
				+ customerId + "') order by ticketcode desc";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				String TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and tickettype='"
						+ type + "' and " + state + " and  creationdate" + TimeConversionFromUTC + ">(now()"
						+ TimeConversionFromUTC
						+ " -interval '60 day') and ticketstatus not in(6,7) and SiteID in(select siteid from msite where customerid='"
						+ customerId + "') order by ticketcode desc";
			} else {
				Query = "Select " + tablecolumnnames + " from tTicketDetail where  ActiveFlag='1'  and tickettype='"
						+ type + "' and " + state
						+ " and  creationdate>(now() -interval '60 day') and ticketstatus not in(6,7) and SiteID in(select siteid from msitemap where userid='"
						+ customerId + "') order by ticketcode desc";
			}

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

// 03-02-19
	/*
	 * public List<TicketDetail> getReportTicketDetailListByUserId(int userId,
	 * String TimezoneOffset) { Session session =
	 * sessionFactory.getCurrentSession(); List<TicketDetail> TicketDetailList = new
	 * ArrayList<TicketDetail>();
	 * 
	 * //Oracle String tablecolumnnames =
	 * "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag"
	 * ; String Query = "Select " + tablecolumnnames +
	 * " from tTicketDetail where  ActiveFlag='1' and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
	 * + userId + "' and ActiveFlag=1))"; String TimeConversionFromUTC="";
	 * 
	 * //PostgreSQL if(utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
	 * if(utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
	 * TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
	 * tablecolumnnames =
	 * "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
	 * + TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" +
	 * TimeConversionFromUTC +
	 * ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
	 * + TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" +
	 * TimeConversionFromUTC +
	 * ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
	 * + TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp" +
	 * TimeConversionFromUTC + ") as completedtimestamp,(completedon" +
	 * TimeConversionFromUTC +
	 * ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag"
	 * ;
	 * 
	 * }
	 * 
	 * Query = "Select " + tablecolumnnames +
	 * " from tTicketDetail where  ActiveFlag='1' and (TicketStatus in(4,5) or State in(2)) and creationdate"
	 * + TimeConversionFromUTC + " >= date_trunc('day',now()" +
	 * TimeConversionFromUTC +
	 * " - (30 * interval '1 day')) and SiteID in (Select SiteId from mSite where CustomerID in(Select CustomerID from mCustomer where customerid in (Select CustomerID from mCustomerMap where UserID='"
	 * + userId +
	 * "' and ActiveFlag=1) or PrimaryCustomer in (Select CustomerID from mCustomerMap where ActiveFlag=1 and UserID='"
	 * + userId + "')))";
	 * 
	 * }
	 * 
	 * List<TicketDetail> TicketDetailListObject =
	 * session.createSQLQuery(Query).list(); TicketDetailList =
	 * GetCastedTicketDetailList(TicketDetailListObject);
	 * 
	 * 
	 * return TicketDetailList;
	 * 
	 * 
	 * }
	 */

	public List<TicketDetail> getReportTicketDetailListByUserId(int userId, String TimezoneOffset) {
		Session session = sessionFactory.getCurrentSession();
		List<TicketDetail> TicketDetailList = new ArrayList<TicketDetail>();

		// Oracle
		String tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto, startedtimestamp,closedtimestamp,remarks,ticketstatus,refticketid,activeflag, creationdate,createdby, lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,scheduledon,closedby,completedtimestamp, completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";
		String Query = "Select " + tablecolumnnames
				+ " from tTicketDetail where  ActiveFlag='1' and SiteID in (Select SiteId from mSite where CustomerID in (Select CustomerID from mCustomerMap where UserID='"
				+ userId + "' and ActiveFlag=1))";
		String TimeConversionFromUTC = "";

		// PostgreSQL
		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
			if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
				TimeConversionFromUTC = " + (" + TimezoneOffset + " * interval '1 minute') ";
				tablecolumnnames = "ticketid,ticketcode,eventtransactionid,siteid,severity,description,assignedto,(startedtimestamp"
						+ TimeConversionFromUTC + ") as startedtimestamp,(closedtimestamp" + TimeConversionFromUTC
						+ ") as closedtimestamp,remarks,ticketstatus,refticketid,activeflag,(creationdate"
						+ TimeConversionFromUTC + ") as creationdate,createdby,(lastupdateddate" + TimeConversionFromUTC
						+ ") as lastupdateddate ,lastupdatedby,priority,state,ticketmode,tickettype,ticketcategory,ticketdetail,equipmentid,notificationstatus,longitude,lattitude,processtype,(scheduledon"
						+ TimeConversionFromUTC + ") as scheduledon,closedby,(completedtimestamp"
						+ TimeConversionFromUTC + ") as completedtimestamp,(completedon" + TimeConversionFromUTC
						+ ") as completedon,longitude_completed,lattitude_completed,daycycle,ticketgroup,uploadflag";

			}

			Query = "Select " + tablecolumnnames
					+ " from tTicketDetail where  ActiveFlag='1' and (TicketStatus in(4,5) or State in(2)) and creationdate"
					+ TimeConversionFromUTC + " >= date_trunc('day',now()" + TimeConversionFromUTC
					+ " - (30 * interval '1 day')) and SiteID in (Select SiteId from mSiteMap where UserID='" + userId
					+ "' and ActiveFlag=1)";

		}

		List<TicketDetail> TicketDetailListObject = session.createSQLQuery(Query).list();
		TicketDetailList = GetCastedTicketDetailList(TicketDetailListObject);

		return TicketDetailList;

	}

}
