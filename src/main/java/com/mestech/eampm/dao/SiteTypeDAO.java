
/******************************************************
 * 
 *    	Filename	: SiteTypeDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for site type operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.SiteType;

public interface SiteTypeDAO {

	public void addSiteType(SiteType sitetype);

	public void updateSiteType(SiteType sitetype);

	public SiteType getSiteTypeById(int id);

	public void removeSiteType(int id);

	public List<SiteType> listSiteTypes();
}
