
/******************************************************
 * 
 *    	Filename	: UserLogDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Log operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.JmrDetail;
import com.mestech.eampm.model.UserLog;

@Repository
public class UserLogDAOImpl implements UserLogDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addUserlog(UserLog userlog) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(userlog);

		System.out.println("DAO Impl Completed");
	}

	// @Override
	public void updateUserlog(UserLog userlog) {
		Session session = sessionFactory.getCurrentSession();
		session.update(userlog);
	}

	@Override
	public UserLog getUserLogById(String sessionId) {
		Session session = sessionFactory.getCurrentSession();
		UserLog userlog = (UserLog) session.createQuery("from UserLog where SessionId='" + sessionId + "'")
				.uniqueResult();

		System.out.println("Userlog Session" + userlog);

		return userlog;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<UserLog> listUsers() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<UserLog> UserlogsList = session.createQuery("from UserLog").list();

		return UserlogsList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<UserLog> listFieldUsers() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<UserLog> UserlogsList = session
				.createQuery("from User where RoleID in (Select RoleId from UserRole where RoleName='FieldUser')")
				.list();

		return UserlogsList;
	}

	@Override
	public void updateUserlogDetails(String SessionID, String IpAddress) {
		Session session = sessionFactory.getCurrentSession();
		Object Query = session.createSQLQuery(
				"update tuserlog  set LocalIpAddress='" + IpAddress + "' where SessionID='" + SessionID + "'");

		session.update(Query);
	}

	// @Override
	public void removeUserlog(int id) {
		Session session = sessionFactory.getCurrentSession();
		UserLog userlog = (UserLog) session.get(UserLog.class, new Integer(id));
		if (null != userlog) {
			session.delete(userlog);
		}
	}

	@Override
	public List<UserLog> listUserlogs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserLog> listFieldUserlogs() {
		// TODO Auto-generated method stub
		return null;
	}
}
