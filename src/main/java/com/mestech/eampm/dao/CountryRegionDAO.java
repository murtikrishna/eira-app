

/******************************************************
 * 
 *    	Filename	: CountryRegionDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Country region operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;


import java.util.List;
 
import com.mestech.eampm.model.CountryRegion;

public interface CountryRegionDAO {
	 
	public void addCountryRegion(CountryRegion countryregion);
	    
	public void updateCountryRegion(CountryRegion countryregion);
	    
	public CountryRegion getCountryRegionById(int id);
	    
	public void removeCountryRegion(int id);
	    
	public List<CountryRegion> listCountryRegions();
}
