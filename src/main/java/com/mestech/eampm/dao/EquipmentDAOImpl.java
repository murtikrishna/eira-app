
/******************************************************
 * 
 *    	Filename	: EquipmentDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Equipment related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.bean.EnergyPerformanceBean;
import com.mestech.eampm.bean.EquipmentCategoryBean;
import com.mestech.eampm.bean.OpenStateTicketsCount;
import com.mestech.eampm.bean.SiteCommonBean;
import com.mestech.eampm.bean.SiteDataloggersBean;
import com.mestech.eampm.bean.SiteViewBean;
import com.mestech.eampm.bean.StandardParameterBean;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentCategory;
import com.mestech.eampm.model.JmrDetail;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.utility.UtilityCommon;
import com.mestech.eampm.utility.UtilityCommonDAO;

@Repository
public class EquipmentDAOImpl implements EquipmentDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();
	private UtilityCommon utilityCommon = new UtilityCommon();

	private List<EquipmentCategoryBean> GetCastedEquipmentCategory(List<EquipmentCategoryBean> lstTicketDetail) {
		List<EquipmentCategoryBean> lstTicketDetailCasted = new ArrayList<EquipmentCategoryBean>();

		if (lstTicketDetail.size() > 0) {

			String Category = "";

			for (Object object : lstTicketDetail) {
				Object[] obj = (Object[]) object;

				System.out.println(obj[0].toString());
				System.out.println("3");
				EquipmentCategoryBean objTicketDetailCasted = new EquipmentCategoryBean();
				System.out.println("4");
				objTicketDetailCasted.setEquipmentCategory(obj[0].toString());
				System.out.println("5");

				lstTicketDetailCasted.add(objTicketDetailCasted);

			}
		}

		return lstTicketDetailCasted;
	}

	// @Override
	public void addEquipment(Equipment equipment) {

		try {
			Session session = sessionFactory.getCurrentSession();
			session.persist(equipment);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// @Override
	public void updateEquipment(Equipment equipment) {
		Session session = sessionFactory.getCurrentSession();
		session.update(equipment);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listEquipments() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<Equipment> EquipmentsList = session.createQuery("from Equipment").list();

		return EquipmentsList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listEquipmentsBySiteId(int siteId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<Equipment> EquipmentsList = session
				.createQuery("from Equipment where SiteId='" + siteId + "' and ActiveFlag=1").list();

		return EquipmentsList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listEquipmentsByIds(String Ids) {
		Session session = sessionFactory.getCurrentSession();
		List<Equipment> EquipmentsList = session.createQuery("from Equipment where EquipmentId in (" + Ids + ")")
				.list();

		return EquipmentsList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	// 03-02-19
	/*
	 * public List<Equipment> listEquipmentsByUserId(int userId){ Session session =
	 * sessionFactory.getCurrentSession(); //Common for Oracle & PostgreSQL String
	 * Query =
	 * "from Equipment where ActiveFlag='1' and SiteId in (Select SiteId from Site where CustomerID in (Select CustomerId from Customer where CustomerId in (Select CustomerID from CustomerMap where UserID='"
	 * + userId +
	 * "') or PrimaryCustomer in (Select CustomerID from CustomerMap where ActiveFlag=1 and UserID='"
	 * + userId + "')))"; List<Equipment> EquipmentsList =
	 * session.createQuery(Query).list();
	 * 
	 * 
	 * return EquipmentsList; }
	 */

	public List<Equipment> listEquipmentsByUserId(int userId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Equipment where ActiveFlag='1' and SiteId in (Select SiteId from SiteMap where UserID='"
				+ userId + "' and ActiveFlag=1)";
		List<Equipment> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

	// @Override
	public Equipment getEquipmentById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Equipment equipment = (Equipment) session.get(Equipment.class, new Integer(id));
		return equipment;
	}

	/*
	 * public Equipment getEquipmentByMax(String MaxColumnName) { Session session =
	 * sessionFactory.getCurrentSession(); Equipment equipment = null; //Common for
	 * Oracle & PostgreSQL String Query = "from Equipment Order By " + MaxColumnName
	 * + " desc"; try { equipment = (Equipment) session.createQuery(Query)
	 * .setMaxResults(1).getSingleResult(); } catch(NoResultException ex) {}
	 * catch(Exception ex) {}
	 * 
	 * 
	 * return equipment; }
	 */

	public Equipment getEquipmentByMax(String MaxColumnName) {
		Session session = sessionFactory.getCurrentSession();

		Equipment equipment = null;
		String Query = "from Equipment where SiteID='" + MaxColumnName + "' Order By EquipmentId desc";
		try {
			equipment = (Equipment) session.createQuery(Query).setMaxResults(1).getSingleResult();
		} catch (NoResultException ex) {
		} catch (Exception ex) {
		}

		return equipment;
	}

	// @Override
	public void removeEquipment(int id) {
		Session session = sessionFactory.getCurrentSession();
		Equipment equipment = (Equipment) session.get(Equipment.class, new Integer(id));

		// De-activate the flag
		equipment.setActiveFlag(0);

		if (null != equipment) {
			// session.delete(equipment);

			session.update(equipment);
		}
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listInverters() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Equipment where ActiveFlag='1' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR')))";
		List<Equipment> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listEnergymeters() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Equipment where ActiveFlag='1' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where ActiveFlag='1' and EquipmentCategory in ('Primary Energy Meter','Secondary Energy Meter')))";
		List<Equipment> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listScbs() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Equipment where ActiveFlag='1' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where ActiveFlag='1' and EquipmentCategory in ('STRINGCOMBINER')))";
		List<Equipment> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listTrackers(int siteid) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Equipment where ActiveFlag='1' and SiteID='" + siteid
				+ "' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where ActiveFlag='1' and EquipmentCategory in ('Tracker Sensor')))";
		List<Equipment> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listInvertersBySiteId(int siteId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Equipment where ActiveFlag='1' and SiteId='" + siteId
				+ "' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR')))";
		List<Equipment> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listFilterInvertersBySiteId(int siteId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Equipment where ActiveFlag='1' and DismandalFlag=0 and SiteId='" + siteId
				+ "' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR')))";
		List<Equipment> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

	// 03-02-19
	/*
	 * @SuppressWarnings("unchecked") //@Override public List<Equipment>
	 * listFilterInvertersByUserId(int userId){ Session session =
	 * sessionFactory.getCurrentSession(); //Common for Oracle & PostgreSQL String
	 * Query =
	 * "from Equipment where ActiveFlag='1' and DismandalFlag=0 and SiteId in (Select SiteId from Site where  ActiveFlag='1' and CustomerID in (Select CustomerId from Customer where CustomerId in(Select CustomerID from CustomerMap where UserID='"
	 * + userId +
	 * "') or PrimaryCustomer in (Select CustomerID from CustomerMap where ActiveFlag=1 and UserID='"
	 * + userId +
	 * "'))) and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR')))"
	 * ; List<Equipment> EquipmentsList = session.createQuery(Query).list();
	 * 
	 * 
	 * return EquipmentsList; }
	 */

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listFilterInvertersByUserId(int userId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Equipment where ActiveFlag='1' and DismandalFlag=0 and SiteId in (Select SiteId from SiteMap where UserID='"
				+ userId
				+ "' and ActiveFlag=1) and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR')))";
		List<Equipment> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

	// 03-02-19
	/*
	 * @SuppressWarnings("unchecked") //@Override public List<Equipment>
	 * listInvertersByUserId(int userId){ Session session =
	 * sessionFactory.getCurrentSession(); //Common for Oracle & PostgreSQL String
	 * Query =
	 * "from Equipment where ActiveFlag='1' and SiteId in (Select SiteId from Site where  ActiveFlag='1' and CustomerID in (Select CustomerId from Customer where CustomerId in(Select CustomerID from CustomerMap where UserID='"
	 * + userId +
	 * "') or PrimaryCustomer in (Select CustomerID from CustomerMap where ActiveFlag=1 and UserID='"
	 * + userId +
	 * "'))) and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR')))"
	 * ; List<Equipment> EquipmentsList = session.createQuery(Query).list();
	 * 
	 * 
	 * return EquipmentsList; }
	 */

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listInvertersByUserId(int userId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Equipment where ActiveFlag='1' and SiteId in (select SiteId from SiteMap where UserID='"
				+ userId
				+ "' and ActiveFlag=1) and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR')))";
		List<Equipment> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

	// 03-02-19
	/*
	 * @SuppressWarnings("unchecked") //@Override public List<Equipment>
	 * listEnergymeterByUserId(int userId){ Session session =
	 * sessionFactory.getCurrentSession(); //Common for Oracle & PostgreSQL String
	 * Query =
	 * "from Equipment where ActiveFlag='1' and SiteId in (Select SiteId from Site where  ActiveFlag='1' and CustomerID in (Select CustomerId from Customer where CustomerId in(Select CustomerID from CustomerMap where UserID='"
	 * + userId +
	 * "') or PrimaryCustomer in (Select CustomerID from CustomerMap where ActiveFlag=1 and UserID='"
	 * + userId +
	 * "'))) and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('Primary Energy Meter')))"
	 * ; List<Equipment> EquipmentsList = session.createQuery(Query).list();
	 * 
	 * 
	 * return EquipmentsList; }
	 */

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listEnergymeterByUserId(int userId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Equipment where ActiveFlag='1' and SiteId in (Select SiteId from SiteMap where UserID='"
				+ userId
				+ "' and ActiveFlag=1) and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('Primary Energy Meter')))";
		List<Equipment> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

	// 03-02-19
	/*
	 * @SuppressWarnings("unchecked") //@Override public List<Equipment>
	 * listScbByUserId(int userId){ Session session =
	 * sessionFactory.getCurrentSession(); //Common for Oracle & PostgreSQL String
	 * Query =
	 * "from Equipment where ActiveFlag='1' and SiteId in (Select SiteId from Site where  ActiveFlag='1' and CustomerID in (Select CustomerId from Customer where CustomerId in(Select CustomerID from CustomerMap where UserID='"
	 * + userId +
	 * "') or PrimaryCustomer in (Select CustomerID from CustomerMap where ActiveFlag=1 and UserID='"
	 * + userId +
	 * "'))) and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('STRINGCOMBINER')))"
	 * ; List<Equipment> EquipmentsList = session.createQuery(Query).list();
	 * 
	 * 
	 * return EquipmentsList; }
	 */

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listScbByUserId(int userId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Equipment where ActiveFlag='1' and SiteId in (Select SiteId from SiteMap where UserID='"
				+ userId
				+ "' and ActiveFlag=1) and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('STRINGCOMBINER')))";
		List<Equipment> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listSensors() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Equipment where ActiveFlag='1' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where ActiveFlag='1' and EquipmentCategory in ('WEATHRSTION')))";
		List<Equipment> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listSensorsBySiteId(int siteId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Equipment where ActiveFlag='1' and SiteId='" + siteId
				+ "' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('WEATHRSTION')))";
		List<Equipment> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

	// 03-02-19
	/*
	 * @SuppressWarnings("unchecked") //@Override public List<Equipment>
	 * listSensorsByUserId(int userId){ Session session =
	 * sessionFactory.getCurrentSession(); //Common for Oracle & PostgreSQL String
	 * Query =
	 * "from Equipment where ActiveFlag='1' and SiteId in (Select SiteId from Site where  ActiveFlag='1' and CustomerID in (Select CustomerId from Customer where CustomerId in(Select CustomerID from CustomerMap where UserID='"
	 * + userId +
	 * "') or PrimaryCustomer in (Select CustomerID from CustomerMap where ActiveFlag=1 and UserID='"
	 * + userId +
	 * "'))) and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('WEATHRSTION')))"
	 * ; List<Equipment> EquipmentsList = session.createQuery(Query).list();
	 * 
	 * 
	 * return EquipmentsList; }
	 */

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listSensorsByUserId(int userId) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Equipment where ActiveFlag='1' and SiteId in (Select SiteId from SiteMap where UserID='"
				+ userId
				+ "' and ActiveFlag=1) and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('WEATHRSTION')))";
		List<Equipment> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

	public List<EquipmentCategory> getCategoryByEquipmentIds(int EquipmentsIDs) {
		Session session = sessionFactory.getCurrentSession();
		String Query1 = " from EquipmentCategory where CategoryID in(select CategoryID from EquipmentType where EquipmentTypeID in(select EquipmentTypeID from Equipment where EquipmentID in("
				+ EquipmentsIDs + ")))";

		List<EquipmentCategory> EquipmentCategoryBean = session.createQuery(Query1).list();

		return EquipmentCategoryBean;
	}

	public List<StandardParameterBean> getStandardParameterByEquipmentIds(String equipmentIds, String EquipmentsIDs) {

		EquipmentsIDs = EquipmentsIDs.replaceAll("Energymeter,", "");
		EquipmentsIDs = EquipmentsIDs.replaceAll("Inverter,", "");
		EquipmentsIDs = EquipmentsIDs.replaceAll("SMB,", "");
		EquipmentsIDs = EquipmentsIDs.replaceAll("Sensor,", "");
		EquipmentsIDs = EquipmentsIDs.replaceAll("Tracker,", "");
		EquipmentsIDs = EquipmentsIDs.replaceAll("Tracker,", "");

		System.out.println(EquipmentsIDs);

		Session session = sessionFactory.getCurrentSession();
		List<EquipmentCategoryBean> CastedCategoryBean = new ArrayList<EquipmentCategoryBean>();
		if (equipmentIds == null) {
			equipmentIds = "0";
		} else if (equipmentIds.equals("")) {
			equipmentIds = "0";
		}

		String Query2 = "Select coalesce(Dataloggerid_scb,0) as Dataloggerid_scb,coalesce(Dataloggerid_sensor,0) as Dataloggerid_sensor,coalesce(Dataloggerid_energymeter,0) as Dataloggerid_energymeter,coalesce(Dataloggerid_inverter,0) as Dataloggerid_inverter,coalesce(Dataloggerid_modbus,0) as Dataloggerid_modbus,coalesce(Dataloggerid_tracker,0) as Dataloggerid_tracker from mSite where SiteId in ("
				+ equipmentIds + ")";

		List<SiteDataloggersBean> DataloggerList = session.createSQLQuery(Query2).list();

		SiteDataloggersBean o = new SiteDataloggersBean();
		for (Object obj : DataloggerList) {
			Object[] str = (Object[]) obj;

			o.setDataloggerid_scb(Integer.valueOf(str[0].toString()));

			o.setDataloggerid_sensor(Integer.valueOf(str[1].toString()));

			o.setDataloggerid_energymeter(Integer.valueOf(str[2].toString()));

			o.setDataloggerid_inverter(Integer.valueOf(str[3].toString()));

			o.setDataloggerid_modbus(Integer.valueOf(str[4].toString()));

			o.setDataloggerid_tracker(Integer.valueOf(str[5].toString()));
		}

		// String Query1="select equipmentcategory as category from mequipmentcategory
		// where categoryid in(select categoryid from mequipmenttype where
		// equipmenttypeid in(select equipmenttypeid from mequipment where equipmentid
		// in("+ EquipmentsIDs +")))";
		String Query1 = " from EquipmentCategory where CategoryID in(select CategoryID from EquipmentType where EquipmentTypeID in(select EquipmentTypeID from Equipment where EquipmentID in("
				+ EquipmentsIDs + ")))";

		List<EquipmentCategory> CategoryBean = session.createQuery(Query1).list();
		System.out.println("1");

		EquipmentCategoryBean o1 = new EquipmentCategoryBean();
		System.out.println("1-1-1");

		int dataloggers1 = 0;
		int dataloggers2 = 0;
		int dataloggers3 = 0;
		int dataloggers4 = 0;
		int dataloggers5 = 0;
		int dataloggers6 = 0;
		System.out.println(CategoryBean.size());
		for (int i = 0; i < CategoryBean.size(); i++) {
			if (CategoryBean.get(i).getEquipmentCategory().equals("CENTRLINVRTR")
					|| CategoryBean.get(i).getEquipmentCategory().equals("STRINGINVRTR")) {

				dataloggers1 = o.getDataloggerid_inverter();
				dataloggers2 = o.getDataloggerid_modbus();
			} else if (CategoryBean.get(i).getEquipmentCategory().equals("Primary Energy Meter")
					|| CategoryBean.get(i).getEquipmentCategory().equals("Secondary Energy Meter")) {

				dataloggers3 = o.getDataloggerid_energymeter();

			} else if (CategoryBean.get(i).getEquipmentCategory().equals("STRINGCOMBINER")) {

				dataloggers4 = o.getDataloggerid_scb();
			} else if (CategoryBean.get(i).getEquipmentCategory().equals("WEATHRSTION")) {

				dataloggers5 = o.getDataloggerid_sensor();
			} else if (CategoryBean.get(i).getEquipmentCategory().equals("Tracker Sensor")) {

				dataloggers6 = o.getDataloggerid_tracker();
			}
		}

		String dataloggers = dataloggers1 + "," + dataloggers2 + "," + dataloggers3 + "," + dataloggers4 + ","
				+ dataloggers5 + "," + dataloggers6;

		// Common for Oracle & PostgreSQL
		String Query = "Select StandardId,StandardParameterName,ParameterDescription,StandardParameterUOM from mParameterStandards where StandardParameterName in ( "
				+ " Select distinct StandardName from mParameterIntegratedStandards where DataLoggerID in ("
				+ dataloggers + ") and StandardName is not null) and CoreParameterFlag=1 order by SequenceId ";

		System.out.println(Query);
		List<StandardParameterBean> StandardParameterList = session.createSQLQuery(Query).list();

		return StandardParameterList;
	}

	public List<StandardParameterBean> getStandardParameterBySiteIds(String siteIds) {
		Session session = sessionFactory.getCurrentSession();

		if (siteIds == null) {
			siteIds = "0";
		} else if (siteIds.equals("")) {
			siteIds = "0";
		}

		String Query1 = "Select coalesce(Dataloggerid_scb,0) as Dataloggerid_scb,coalesce(Dataloggerid_sensor,0) as Dataloggerid_sensor,coalesce(Dataloggerid_energymeter,0) as Dataloggerid_energymeter,coalesce(Dataloggerid_inverter,0) as Dataloggerid_inverter,coalesce(Dataloggerid_modbus,0) as Dataloggerid_modbus,coalesce(Dataloggerid_tracker,0) as Dataloggerid_tracker from mSite where SiteId in ("
				+ siteIds + ")";

		List<SiteDataloggersBean> DataloggerList = session.createSQLQuery(Query1).list();
		Integer val1 = null;
		Integer val2 = null;
		Integer val3 = null;
		Integer val4 = null;
		Integer val5 = null;

		SiteDataloggersBean o = new SiteDataloggersBean();
		for (Object obj : DataloggerList) {
			Object[] str = (Object[]) obj;

			o.setDataloggerid_scb(Integer.valueOf(str[0].toString()));

			o.setDataloggerid_sensor(Integer.valueOf(str[1].toString()));

			o.setDataloggerid_energymeter(Integer.valueOf(str[2].toString()));

			o.setDataloggerid_inverter(Integer.valueOf(str[3].toString()));

			o.setDataloggerid_modbus(Integer.valueOf(str[4].toString()));
			o.setDataloggerid_tracker(Integer.valueOf(str[5].toString()));

		}

		String dataloggers = o.getDataloggerid_scb() + "," + o.getDataloggerid_sensor() + ","
				+ o.getDataloggerid_energymeter() + "," + o.getDataloggerid_inverter() + ","
				+ o.getDataloggerid_modbus() + "," + o.getDataloggerid_tracker();

		System.out.println(dataloggers);
		// Common for Oracle & PostgreSQL
		/*
		 * String Query =
		 * "Select StandardId,StandardParameterName,ParameterDescription,StandardParameterUOM from mParameterStandards where StandardParameterName in ( "
		 * +
		 * " Select distinct StandardName from mParameterIntegratedStandards where DataLoggerID in ( "
		 * + " Select Dataloggerid_ID from mSite where SiteId in (" + siteIds +
		 * ")) and StandardName is not null) and CoreParameterFlag=1 order by StandardId "
		 * ;
		 */ String Query = "Select StandardId,StandardParameterName,ParameterDescription,StandardParameterUOM from mParameterStandards where StandardParameterName in ((Select distinct StandardName from mParameterIntegratedStandards where DataLoggerID in("
				+ dataloggers + ") and StandardName is not null)) and CoreParameterFlag=1 order by StandardId";

		System.out.println(Query);
		List<StandardParameterBean> StandardParameterList = session.createSQLQuery(Query).list();

		return StandardParameterList;
	}

	public List<Equipment> listallequipments(int equipmentid) {
		Session session = sessionFactory.getCurrentSession();

		String Query = "from Equipment where EquipmentID not in('" + equipmentid + "')";
		List<Equipment> EquipmentList = session.createQuery(Query).list();

		return EquipmentList;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Equipment> listConfigEquipments() {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<Equipment> EquipmentsList = session.createQuery("from Equipment").list();

		return EquipmentsList;
	}

	// @Override
	public List<SiteCommonBean> listInverterDetails(int siteid) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "select count(equipmentid) as EnergymeterCount,coalesce(sum(capacity),0) as Equipmentscapacity from mequipment where dismandalflag=0 and siteid='"
				+ siteid
				+ "' and equipmenttypeid in(select equipmenttypeid from mequipmenttype where categoryid in(select categoryid from mequipmentcategory where equipmentcategory in('Primary Energy Meter')))";
		List<SiteCommonBean> EquipmentsList = session.createSQLQuery(Query).list();

		return EquipmentsList;
	}

	// @Override
	public List<Equipment> listEquipmentComunicationCount(int siteid) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		String Query = "from Equipment where DismandalFlag=0 and Activeflag=1 and ProdFlag=1 and SiteId='" + siteid
				+ "' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR','Primary Energy Meter','Secondary Energy Meter','WEATHRSTION','STRINGCOMBINER','Tracker Sensor')))";

		List<Equipment> EquipmentsList = session.createQuery(Query).list();

		return EquipmentsList;
	}

}
