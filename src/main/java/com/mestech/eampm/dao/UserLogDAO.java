
/******************************************************
 * 
 *    	Filename	: UserLogDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for user Log operations.
 *      
 *         
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.UserLog;

public interface UserLogDAO {

	public void addUserlog(UserLog userlog);

	public void updateUserlog(UserLog userlog);

	public void updateUserlogDetails(String SessionID, String IpAddress);

	public UserLog getUserLogById(String sessionId);

	public void removeUserlog(int id);

	public List<UserLog> listUserlogs();

	public List<UserLog> listFieldUserlogs();

}
