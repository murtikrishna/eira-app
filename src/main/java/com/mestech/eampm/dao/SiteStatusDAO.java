
/******************************************************
 * 
 *    	Filename	: SiteStatusDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for site status related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.SiteStatus;

public interface SiteStatusDAO {

	public List<SiteStatus> getSiteStatusListByCustomerId(int customerId);

	public List<SiteStatus> listSiteStatus();

	public List<SiteStatus> getSiteStatusListByUserId(int userId);

	public SiteStatus getSiteStatusBySiteId(int siteId);

	public SiteStatus save(SiteStatus siteStatus);
}
