
/******************************************************
 * 
 *    	Filename	: BatteryLimitsDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Battery Limits operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.BatteryLimits;

@Repository
public class BatteryLimitsDAOImpl implements BatteryLimitsDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addBatteryLimits(BatteryLimits batterylimits) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(batterylimits);

	}

	// @Override
	public void updateBatteryLimits(BatteryLimits batterylimits) {
		Session session = sessionFactory.getCurrentSession();
		session.update(batterylimits);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<BatteryLimits> listBatteryLimits() {
		Session session = sessionFactory.getCurrentSession();
		List<BatteryLimits> BatteryLimitsList = session.createQuery("from BatteryLimits where ActiveFlag=1").list();

		return BatteryLimitsList;
	}

	// @Override
	public BatteryLimits getBatteryLimitsById(int id) {
		Session session = sessionFactory.getCurrentSession();
		BatteryLimits batterylimits = (BatteryLimits) session.get(BatteryLimits.class, new Integer(id));
		return batterylimits;
	}

	// @Override
	public void removeBatteryLimits(int id) {
		Session session = sessionFactory.getCurrentSession();
		BatteryLimits batterylimits = (BatteryLimits) session.get(BatteryLimits.class, new Integer(id));

		// De-activate the flag
		batterylimits.setActiveFlag(0);

		if (null != batterylimits) {
			session.update(batterylimits);
			// session.delete(activity);
		}
	}

}
