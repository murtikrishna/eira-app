
/******************************************************
 * 
 *    	Filename	: MDataloggerTypeDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for datalogger type related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.math.BigInteger;
import java.util.List;

import com.mestech.eampm.model.JmrDetail;
import com.mestech.eampm.model.MDatalogger;
import com.mestech.eampm.model.MDataloggerType;

public interface MDataloggerTypeDAO {

	public void addDatalogger(MDataloggerType datalogger);

	public MDataloggerType save(MDataloggerType datalogger);

	public MDataloggerType update(MDataloggerType datalogger);

	public void delete(int id);

	public MDataloggerType edit(int id);

	public List<MDataloggerType> listAll();

	public List<MDatalogger> listAllDatalogger(int flag);

	public void maddDatalogger(MDatalogger datalogger);

	List<BigInteger> listDataloggerIdFromStandards();
}
