

/******************************************************
 * 
 *    	Filename	: CurrencyDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Currency related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
 
import com.mestech.eampm.model.Currency;

@Repository
public class CurrencyDAOImpl implements CurrencyDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addCurrency(Currency currency) {
	   
       Session session = sessionFactory.getCurrentSession();
       session.persist(currency);
       
   }

   //@Override
   public void updateCurrency(Currency currency) {
       Session session = sessionFactory.getCurrentSession();
       session.update(currency);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<Currency> listCurrencies() {
       Session session = sessionFactory.getCurrentSession();
       List<Currency> CurrenciesList = session.createQuery("from Currency").list();
       
       return CurrenciesList;
   }

   //@Override
   public Currency getCurrencyById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       Currency currency = (Currency) session.get(Currency.class, new Integer(id));
       return currency;
   }

   //@Override
   public void removeCurrency(int id) {
       Session session = sessionFactory.getCurrentSession();
       Currency currency = (Currency) session.get(Currency.class, new Integer(id));
      
       //De-activate the flag
       currency.setActiveFlag(0);
       
       
       if(null != currency){
           //session.delete(currency);
    	   session.update(currency);
       }
   }
}
