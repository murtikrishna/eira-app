
/******************************************************
 * 
 *    	Filename	: CustomerDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Customer related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.bean.CustomerBean;
import com.mestech.eampm.bean.CustomerListBean;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.EquipmentType;
import com.mestech.eampm.model.SiteStatus;

public interface CustomerDAO {

	public void addCustomer(Customer customer);

	public void updateCustomer(Customer customer);

	public Customer getCustomerById(int id);

	public List<Customer> getPrimaryCustomerById(String id);

	public Customer getCustomerByMax(String MaxColumnName);

	public Customer getPrimaryCustomerByMax(String MaxColumnName);

	public void removeCustomer(int id);

	public List<Customer> listCustomers();

	public List<Customer> listConfigCustomers();

	public List<Customer> getCustomerListByUserId(int userId);

	public List<CustomerListBean> getAjaxCustomerListByUser(String UserID);

	public List<CustomerListBean> getAjaxCustomerListByCustomer(String CustomerID);

	public List<Customer> listallcustomers(int customerid);

	public List<Customer> listprimarycustomers();
}
