
/******************************************************
 * 
 *    	Filename	: HistoricalUploadDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Historical upload operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.HistoricalUpload;

public interface HistoricalUploadDAO {

	public HistoricalUpload save(HistoricalUpload historicalUpload);

	public List<HistoricalUpload> list();

	public List<HistoricalUpload> getHistory(String siteCode, String directory);

}
