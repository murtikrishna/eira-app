
/******************************************************
 * 
 *    	Filename	: ParameterStandardsDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Standard parameters related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.EquipmentCategory;
import com.mestech.eampm.model.MparameterIntegratedStandards;
import com.mestech.eampm.model.ParameterStandard;

public interface ParameterStandardsDAO {

	public void addParameterStandards(ParameterStandard parameterstandards);

	public void updateParameterStandards(ParameterStandard parameterstandards);

	public ParameterStandard getParameterStandardsById(int id);

	public void removeParameterStandards(int id);

	public ParameterStandard getParameterStandardByMax(String MaxColumnName);

	public List<ParameterStandard> listParameterStandards();

	public List<ParameterStandard> listParameterStandards(int standardid);

	public List<MparameterIntegratedStandards> getallStandards();

	public List<String> getallStandardParameterNames();

	public void saveParameterIntegratedStandards(MparameterIntegratedStandards integratedStandards);

}
