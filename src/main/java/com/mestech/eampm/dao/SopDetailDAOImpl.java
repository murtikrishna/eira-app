
/******************************************************
 * 
 *    	Filename	: SopDetailDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for site detail related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.SopDetail;

@Repository
public class SopDetailDAOImpl implements SopDetailDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addSopDetail(SopDetail sopdetail) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(sopdetail);
	}

	// @Override
	public void updateSopDetail(SopDetail sopdetail) {
		Session session = sessionFactory.getCurrentSession();
		session.update(sopdetail);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<SopDetail> listSopDetails() {
		Session session = sessionFactory.getCurrentSession();
		List<SopDetail> SopDetailsList = session.createQuery("from SopDetail").list();

		return SopDetailsList;
	}

	// @Override
	public SopDetail getSopDetailById(int id) {
		Session session = sessionFactory.getCurrentSession();
		SopDetail sopdetail = (SopDetail) session.get(SopDetail.class, new Integer(id));
		return sopdetail;
	}

	// @Override
	public void removeSopDetail(int id) {
		Session session = sessionFactory.getCurrentSession();
		SopDetail sopdetail = (SopDetail) session.get(SopDetail.class, new Integer(id));
		if (null != sopdetail) {
			session.delete(sopdetail);
		}
	}
}
