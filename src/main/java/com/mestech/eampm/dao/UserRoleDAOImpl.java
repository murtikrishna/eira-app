
/******************************************************
 * 
 *    	Filename	: UserRoleDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for user log operations.
 *      
 *         
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.UserRole;

@Repository
public class UserRoleDAOImpl implements UserRoleDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addUserRole(UserRole userrole) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(userrole);

		System.out.println("DAO Impl Completed");
	}

	// @Override
	public void updateUserRole(UserRole userrole) {
		Session session = sessionFactory.getCurrentSession();
		session.update(userrole);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<UserRole> listUserRoles() {
		Session session = sessionFactory.getCurrentSession();
		List<UserRole> UserRolesList = session.createQuery("from UserRole").list();

		return UserRolesList;
	}

	// @Override
	public UserRole getUserRoleById(int id) {
		Session session = sessionFactory.getCurrentSession();
		UserRole userrole = (UserRole) session.get(UserRole.class, new Integer(id));
		return userrole;
	}

	// @Override
	public void removeUserRole(int id) {
		Session session = sessionFactory.getCurrentSession();
		UserRole userrole = (UserRole) session.get(UserRole.class, new Integer(id));
		if (null != userrole) {
			session.delete(userrole);
		}
	}

	public List<UserRole> listRoles(int roleid) {
		Session session = sessionFactory.getCurrentSession();
		List<UserRole> UserRolesList = session.createQuery("from UserRole Where RoleId not in ('" + roleid + "')")
				.list();

		return UserRolesList;
	}
}
