
/******************************************************
 * 
 *    	Filename	: EquipmentCategoryDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Equipment category related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentCategory;
import com.mestech.eampm.model.SiteMap;

@Repository
public class EquipmentCategoryDAOImpl implements EquipmentCategoryDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addEquipmentCategory(EquipmentCategory equipmentcategory) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(equipmentcategory);

	}

	// @Override
	public void updateEquipmentCategory(EquipmentCategory equipmentcategory) {
		Session session = sessionFactory.getCurrentSession();
		session.update(equipmentcategory);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<EquipmentCategory> listEquipmentCategories() {
		Session session = sessionFactory.getCurrentSession();
		List<EquipmentCategory> EquipmentCategoriesList = session.createQuery("from EquipmentCategory").list();

		return EquipmentCategoriesList;
	}

	// @Override
	public EquipmentCategory getEquipmentCategoryById(int id) {
		Session session = sessionFactory.getCurrentSession();
		EquipmentCategory equipmentcategory = (EquipmentCategory) session.get(EquipmentCategory.class, new Integer(id));
		return equipmentcategory;
	}

	// @Override
	public void removeEquipmentCategory(int id) {
		Session session = sessionFactory.getCurrentSession();
		EquipmentCategory equipmentcategory = (EquipmentCategory) session.get(EquipmentCategory.class, new Integer(id));

		// De-activate the flag
		equipmentcategory.setActiveFlag(0);

		if (null != equipmentcategory) {
			// session.delete(equipmentcategory);
			session.update(equipmentcategory);
		}
	}

	public EquipmentCategory getEquipmentCategoryByMax(String MaxColumnName) {
		Session session = sessionFactory.getCurrentSession();
		EquipmentCategory equipmentcategory = null;
		// Common for Oracle & PostgreSQL
		String Query = "from EquipmentCategory Order By " + MaxColumnName + " desc";
		try {
			equipmentcategory = (EquipmentCategory) session.createQuery(Query).setMaxResults(1).getSingleResult();
		} catch (NoResultException ex) {
		} catch (Exception ex) {
		}

		return equipmentcategory;
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<EquipmentCategory> listEquipmentCategorys(int categoryid) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<EquipmentCategory> EquipmentCategoryList = session
				.createQuery("from EquipmentCategory where CategoryID not in('" + categoryid + "')").list();

		return EquipmentCategoryList;
	}
}
