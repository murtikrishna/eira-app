
/******************************************************
 * 
 *    	Filename	: MServiceCodeDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Service code related operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.math.BigInteger;
import java.util.List;

import com.mestech.eampm.model.MServiceCode;

public interface MServiceCodeDAO {
	public MServiceCode save(MServiceCode mServiceCode);

	public MServiceCode update(MServiceCode mServiceCode);

	public void delete(Long id);

	public MServiceCode edit(BigInteger id);

	public List<MServiceCode> listAll();

}
