
/******************************************************
 * 
 *    	Filename	: SiteMapDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for site map operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteMap;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.SiteMapService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.Customers;
import com.mestech.eampm.utility.Response;
import com.mestech.eampm.utility.SiteDetails;
import com.mestech.eampm.utility.SiteMapGrouping;
import com.mestech.eampm.utility.SiteMapPojo;
import com.mestech.eampm.utility.SiteMapResponse;

@Repository
public class SiteMapDAOImpl implements SiteMapDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private UserService userService;

	@Autowired
	private SiteMapService sitemapService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private CustomerService customerService;

	// @Override
	public void addSiteMap(SiteMap sitemap) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(sitemap);

	}

	// @Override
	public void updateSiteMap(SiteMap sitemap) {
		Session session = sessionFactory.getCurrentSession();
		session.update(sitemap);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<SiteMap> listSiteMaps() {
		Session session = sessionFactory.getCurrentSession();
		List<SiteMap> CustomerMapsList = session.createQuery("from SiteMap").list();

		return CustomerMapsList;
	}

	// @Override
	public SiteMap getSiteMapById(int id) {
		Session session = sessionFactory.getCurrentSession();
		SiteMap customermap = (SiteMap) session.get(SiteMap.class, new Integer(id));
		return customermap;
	}

	// @Override
	public void removeSiteMap(int id) {
		Session session = sessionFactory.getCurrentSession();
		SiteMap sitemap = (SiteMap) session.get(SiteMap.class, new Integer(id));

		// De-activate the flag
		sitemap.setActiveFlag(0);

		if (null != sitemap) {
			session.update(sitemap);
			// session.delete(activity);
		}
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<SiteMap> listSiteMaps(int mapid) {
		Session session = sessionFactory.getCurrentSession();
		// Common for Oracle & PostgreSQL
		List<SiteMap> MapList = session.createQuery("from SiteMap where MapID not in('" + mapid + "')").list();

		return MapList;
	}

	@Override
	public Response updateSiteMapping(SiteMapPojo siteMap) {
		Response response = new Response();
		try {
			Session session = sessionFactory.getCurrentSession();

			for (Integer userId : siteMap.getUserId()) {
				Query query = session
						.createQuery("FROM SiteMap WHERE UserID=:UserID AND CustomerID=:CustomerID AND ActiveFlag=1");
				query.setParameter("UserID", userId);
				query.setParameter("CustomerID", siteMap.getCustomerId());
				List<SiteMap> siteMaps = query.list();
				Map<Integer, SiteMap> maps = new HashMap<>();
				if (siteMaps != null && !siteMaps.isEmpty()) {
					for (SiteMap map : siteMaps) {
						map.setActiveFlag(new Integer(0));
						if (siteMap.getSiteId().contains(map.getSiteId())) {
							map.setActiveFlag(new Integer(1));
							maps.put(map.getSiteId(), map);
						} else {
							session.update(map);
						}
					}
					for (Integer siteId : siteMap.getSiteId()) {
						if (maps.get(siteId) != null) {
							session.update(maps.get(siteId));
						} else {
							SiteMap map = new SiteMap(userId, siteId, siteMap.getCustomerId(), new Integer(1),
									new Date());
							session.save(map);
						}
					}
				} else {
					if (siteMap.getSiteId() != null && !siteMap.getSiteId().isEmpty()) {
						for (Integer siteId : siteMap.getSiteId()) {
							SiteMap map = new SiteMap(userId, siteId, siteMap.getCustomerId(), new Integer(1),
									new Date());
							session.save(map);
						}
					}
				}
			}
			response.setStatus("success");
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus("failed");
		}
		return response;
	}

	@Override
	public Response getSiteMapping() {
		Response response = new Response();
		Map<Integer, List<SiteMap>> groupByUserId = new HashMap<>();
		try {
			List<SiteMapResponse> mapResponses = new ArrayList<>();
			List<SiteMapGrouping> siteMapGroupings = new ArrayList<>();
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery("FROM SiteMap WHERE ActiveFlag=1");
			List<SiteMap> siteMaps = query.list();
			if (siteMaps != null && !siteMaps.isEmpty()) {
				for (SiteMap map : siteMaps) {
					if (groupByUserId.get(map.getUserID()) != null && !groupByUserId.get(map.getUserID()).isEmpty()) {
						List<SiteMap> list = groupByUserId.get(map.getUserID());
						list.add(map);
						groupByUserId.put(map.getUserID(), list);
					} else {
						List<SiteMap> list = new ArrayList<>();
						list.add(map);
						groupByUserId.put(map.getUserID(), list);
					}
				}
				if (groupByUserId != null && !groupByUserId.isEmpty()) {

					for (Map.Entry<Integer, List<SiteMap>> entry : groupByUserId.entrySet()) {
						SiteMapGrouping siteMapGrouping = new SiteMapGrouping();
						User user = userService.getUserById(entry.getKey());
						if (user != null) {
							siteMapGrouping.setUserId(user.getUserId());
							siteMapGrouping.setUserName(user.getUserName());
						}
						List<SiteMap> maps = entry.getValue();
						if (maps != null && !maps.isEmpty()) {
							Map<Integer, List<Integer>> groupMapping = new HashMap<>();
							List<Customers> customersList = new ArrayList<>();
							for (SiteMap map : maps) {
								if (groupMapping.get(map.getCustomerID()) != null
										&& !groupMapping.get(map.getCustomerID()).isEmpty()) {
									List<Integer> siteIds = groupMapping.get(map.getCustomerID());
									siteIds.add(map.getSiteId());
									groupMapping.put(map.getCustomerID(), siteIds);
								} else {
									List<Integer> siteIds = new ArrayList<>();
									siteIds.add(map.getSiteId());
									groupMapping.put(map.getCustomerID(), siteIds);
								}

							}
							if (groupMapping != null && !groupMapping.isEmpty()) {
								for (Map.Entry<Integer, List<Integer>> entry2 : groupMapping.entrySet()) {
									Customer customer = customerService.getCustomerById(entry2.getKey());
									if (customer == null)
										continue;
									List<Integer> siteIds = entry2.getValue();
									if (siteIds != null && !siteIds.isEmpty()) {
										List<SiteDetails> details = new ArrayList<>();
										for (Integer siteId : siteIds) {
											Site site = siteService.getSiteById(siteId);
											if (site != null) {
												String siteName = site.getSiteName().replaceAll(",", "").trim();
												SiteDetails siteDetails = new SiteDetails(site.getSiteId(), siteName);
												if (siteDetails != null) {
													details.add(siteDetails);
												}
											}
										}
										Customers customers = new Customers(customer.getCustomerId(),
												customer.getCustomerName(), details);
										customersList.add(customers);
									}

								}
								siteMapGrouping.setCustomers(customersList);
							}
							siteMapGroupings.add(siteMapGrouping);
						}

					}

				}
				if (siteMapGroupings != null && !siteMapGroupings.isEmpty()) {
					for (SiteMapGrouping mapGrouping : siteMapGroupings) {
						SiteMapResponse siteMapResponse = new SiteMapResponse();
						List<Customers> customers = mapGrouping.getCustomers();
						if (customers != null && !customers.isEmpty()) {
							List<Customers> customers2 = new ArrayList<>();
							for (Customers customer : customers) {
								siteMapResponse.setUserId(mapGrouping.getUserId());
								siteMapResponse.setUserName(mapGrouping.getUserName());
								StringJoiner stringJoiner = new StringJoiner(",");
								if (null != customer.getSiteDetails() && !customer.getSiteDetails().isEmpty()) {
									for (SiteDetails siteDetails : customer.getSiteDetails()) {
										if (siteDetails.getSiteName() == null)
											continue;
										stringJoiner.add(siteDetails.getSiteName());

									}
								}
								if (stringJoiner != null) {
									customer.setSiteNames(stringJoiner.toString());
									customers2.add(customer);
								}
							}
							if (customers2 == null || customers2.isEmpty())
								continue;
							siteMapResponse.setCustomers(customers2);
							mapResponses.add(siteMapResponse);
						}
					}
				}
			}
			response.setData(mapResponses);
			response.setStatus("success");
		} catch (Exception e) {
			e.printStackTrace();
			response.setData(null);
			response.setStatus("failed");
		}
		// TODO Auto-generated method stub
		return response;
	}

}
