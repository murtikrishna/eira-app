
/******************************************************
 * 
 *    	Filename	: FilterDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for filtering operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.Filter;

public interface FilterDAO {

	public void addFilter(Filter filter);

	public void updateFilter(Filter filter);

	public Filter getFilterById(int id);

	public void removeFilter(int id);

	public List<Filter> listFilters();
}
