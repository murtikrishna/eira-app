
/******************************************************
 * 
 *    	Filename	: EquipmentParameterDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Equipment parameter operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.EquipmentParameter;

public interface EquipmentParameterDAO {

	public void addEquipmentParameter(EquipmentParameter equipmentparameter);

	public void updateEquipmentParameter(EquipmentParameter equipmentparameter);

	public EquipmentParameter getEquipmentParameterById(int id);

	public void removeEquipmentParameter(int id);

	public List<EquipmentParameter> listEquipmentParameters();
}
