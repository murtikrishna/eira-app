
/******************************************************
 * 
 *    	Filename	: RoleActivityDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used for Role operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.RoleActivity;

public interface RoleActivityDAO {

	public void addRoleActivity(RoleActivity roleactivity);

	public void updateRoleActivity(RoleActivity roleactivity);

	public RoleActivity getRoleActivityById(int id);

	public void removeRoleActivity(int id);

	public List<RoleActivity> listRoleActivities();

	public List<RoleActivity> getRoleActivityByRoleId(int roleId);

	public void deleteRoleActivity(RoleActivity roleActivity);
}
