
/******************************************************
 * 
 *    	Filename	: FilterDAOImpl.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for filtering operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.Filter;

@Repository
public class FilterDAOImpl implements FilterDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	public void addFilter(Filter filter) {

		Session session = sessionFactory.getCurrentSession();
		session.persist(filter);

	}

	// @Override
	public void updateFilter(Filter filter) {
		Session session = sessionFactory.getCurrentSession();
		session.update(filter);
	}

	@SuppressWarnings("unchecked")
	// @Override
	public List<Filter> listFilters() {
		Session session = sessionFactory.getCurrentSession();
		List<Filter> FiltersList = session.createQuery("from Filter").list();

		return FiltersList;
	}

	// @Override
	public Filter getFilterById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Filter filter = (Filter) session.get(Filter.class, new Integer(id));
		return filter;
	}

	// @Override
	public void removeFilter(int id) {
		Session session = sessionFactory.getCurrentSession();
		Filter filter = (Filter) session.get(Filter.class, new Integer(id));

		// De-activate the flag
		filter.setActiveFlag(0);

		if (null != filter) {
			session.update(filter);
			// session.delete(filter);
		}
	}
}