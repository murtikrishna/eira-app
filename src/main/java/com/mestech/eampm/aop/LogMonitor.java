
/******************************************************
 * 
 *    	Filename	: LogMonitor.java
 *      
 *      Author		: Johnson Edwinraj
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is parent for log.
 *      
 *      
 *******************************************************/

package com.mestech.eampm.aop;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.stereotype.Component;

@Component
public class LogMonitor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5226734247412583575L;

	public static final LogMonitor INSTANCE = new LogMonitor();

	@PersistenceUnit
	private EntityManagerFactory emf;

	@Inject
	private InsertEvent insert;

	@Inject
	private UpdateEvent update;

	@PostConstruct
	protected void init() {
		SessionFactoryImpl sF = emf.unwrap(SessionFactoryImpl.class);
		EventListenerRegistry registry = sF.getServiceRegistry().getService(EventListenerRegistry.class);
		registry.getEventListenerGroup(EventType.POST_INSERT).appendListener(insert);
		registry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(update);
	}
	// @Override
	// public void onPostUpdate(PostUpdateEvent event) {
	// // TODO Auto-generated method stub
	// System.out.println("_____________________________________________________________________________");
	// }

}
