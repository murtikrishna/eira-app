
/******************************************************
 * 
 *    	Filename	: UpdateEvent.java
 *      
 *      Author		: Johnson Edwinraj
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is log db updates.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.aop;

import org.apache.log4j.Logger;
import org.hibernate.event.spi.PostUpdateEvent;
import org.hibernate.event.spi.PostUpdateEventListener;
import org.hibernate.persister.entity.EntityPersister;
import org.springframework.stereotype.Component;

@Component
public class UpdateEvent implements PostUpdateEventListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2221429602808144271L;

	@Override
	public void onPostUpdate(PostUpdateEvent event) {
		// TODO Auto-generated method stub
		Logger logger = Logger.getLogger(event.getEntity().getClass());
		logger.info("_____________________________________________________________________________");
		logger.info(event.getEntity().getClass().getName() + " " + "Updated Successfully");
	}

	@Override
	public boolean requiresPostCommitHanding(EntityPersister persister) {
		// TODO Auto-generated method stub
		return false;
	}

}
