
/******************************************************
 * 
 *    	Filename	: Aspects.java
 *      
 *      Author		: Johnson Edwinraj
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to configure logs.
 *      
 *      
 *******************************************************/

package com.mestech.eampm.aop;

import org.apache.log4j.Logger;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class Aspects {

	@AfterThrowing(pointcut = "execution(* com.mestech.eampm..*.*(..))", throwing = "ex")
	public void loggingForExceptions(Exception ex) throws Throwable {
		Logger logger = Logger.getLogger(Exception.class);
		logger.error("Exception Occured....\n", ex);
	}
}
