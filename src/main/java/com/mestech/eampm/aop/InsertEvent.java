
/******************************************************
 * 
 *    	Filename	: InsertEvent.java
 *      
 *      Author		: Johnson Edwinraj
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to log db inserts.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.aop;

import org.apache.log4j.Logger;
import org.hibernate.event.spi.PostInsertEvent;
import org.hibernate.event.spi.PostInsertEventListener;
import org.hibernate.persister.entity.EntityPersister;
import org.springframework.stereotype.Component;

@Component
public class InsertEvent implements PostInsertEventListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3298556491676572175L;

	@Override
	public void onPostInsert(PostInsertEvent event) {
		// TODO Auto-generated method stub
		Logger logger = Logger.getLogger(event.getEntity().getClass());
		logger.info("_____________________________________________________________________________");
		logger.info(event.getEntity().getClass().getName() + " " + "Saved Successfully");
	}

	@Override
	public boolean requiresPostCommitHanding(EntityPersister persister) {
		// TODO Auto-generated method stub
		return false;
	}
}
