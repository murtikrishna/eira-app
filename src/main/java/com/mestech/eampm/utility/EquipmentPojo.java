
/******************************************************
 * 
 *    	Filename	: EquipmentPojo.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Equipment related data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

public class EquipmentPojo {

	private String equipmentCode, capacity, customerRefNumber, equipmentCategory, serialNumber, equipementType;

	private String components, disconnectRating, disconnectType;

	public EquipmentPojo() {
		super();
	}

	public EquipmentPojo(String equipmentCode, String capacity, String customerRefNumber, String equipmentCategory,
			String serialNumber, String equipementType, String components, String disconnectRating,
			String disconnectType) {
		super();
		this.equipmentCode = equipmentCode;
		this.capacity = capacity;

		this.customerRefNumber = customerRefNumber;
		this.equipmentCategory = equipmentCategory;

		this.serialNumber = serialNumber;
		this.equipementType = equipementType;
		this.components = components;
		this.disconnectRating = disconnectRating;
		this.disconnectType = disconnectType;

	}

	public String getComponents() {
		return components;
	}

	public void setComponents(String components) {
		components = components;
	}

	public String getDisconnectRating() {
		return disconnectRating;
	}

	public void setDisconnectRating(String disconnectRating) {
		disconnectRating = disconnectRating;
	}

	public String getDisconnectType() {
		return disconnectType;
	}

	public void setDisconnectType(String disconnectType) {
		disconnectType = disconnectType;
	}

	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getCustomerRefNumber() {
		return customerRefNumber;
	}

	public void setCustomerRefNumber(String customerRefNumber) {
		this.customerRefNumber = customerRefNumber;
	}

	public String getEquipmentCategory() {
		return equipmentCategory;
	}

	public void setEquipmentCategory(String equipmentCategory) {
		this.equipmentCategory = equipmentCategory;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getEquipementType() {
		return equipementType;
	}

	public void setEquipementType(String equipementType) {
		this.equipementType = equipementType;
	}

}
