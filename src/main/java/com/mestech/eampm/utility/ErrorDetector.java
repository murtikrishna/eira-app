
/******************************************************
 * 
 *    	Filename	: ErrorDetector.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Error data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

import java.util.List;

public class ErrorDetector {

	private String fieldName;

	private List<String> errors;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

}
