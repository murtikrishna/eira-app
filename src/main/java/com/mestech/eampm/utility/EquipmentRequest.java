package com.mestech.eampm.utility;

public class EquipmentRequest {

	private Integer row;

	private String siteName;

	private String categoryName;

	private String equipmentTypeName;

	private String primarySerialNumber;

	private String description;

	private String remarks;

	private String customerReference;

	private String customerNaming;

	private String capacity;

	private Integer activeFlag;

	private String installationDate;

	private String warrantyDate;

	private String equipmentSelection;

	private String components;

	private String disconnectRating;

	private String disconnectType;
	
	
	
	

	public EquipmentRequest() {
		super();
	}

	public EquipmentRequest(Integer row, String siteName, String categoryName, String equipmentTypeName,
			String primarySerialNumber, String description, String remarks, String customerReference,
			String customerNaming, String capacity, Integer activeFlag, String installationDate, String warrantyDate,
			String equipmentSelection, String components, String disconnectRating, String disconnectType) {
		super();
		this.row = row;
		this.siteName = siteName;
		this.categoryName = categoryName;
		this.equipmentTypeName = equipmentTypeName;
		this.primarySerialNumber = primarySerialNumber;
		this.description = description;
		this.remarks = remarks;
		this.customerReference = customerReference;
		this.customerNaming = customerNaming;
		this.capacity = capacity;
		this.activeFlag = activeFlag;
		this.installationDate = installationDate;
		this.warrantyDate = warrantyDate;
		this.equipmentSelection = equipmentSelection;
		this.components = components;
		this.disconnectRating = disconnectRating;
		this.disconnectType = disconnectType;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getEquipmentTypeName() {
		return equipmentTypeName;
	}

	public void setEquipmentTypeName(String equipmentTypeName) {
		this.equipmentTypeName = equipmentTypeName;
	}

	public String getPrimarySerialNumber() {
		return primarySerialNumber;
	}

	public void setPrimarySerialNumber(String primarySerialNumber) {
		this.primarySerialNumber = primarySerialNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCustomerReference() {
		return customerReference;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public String getCustomerNaming() {
		return customerNaming;
	}

	public void setCustomerNaming(String customerNaming) {
		this.customerNaming = customerNaming;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public Integer getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getInstallationDate() {
		return installationDate;
	}

	public void setInstallationDate(String installationDate) {
		this.installationDate = installationDate;
	}

	public String getWarrantyDate() {
		return warrantyDate;
	}

	public void setWarrantyDate(String warrantyDate) {
		this.warrantyDate = warrantyDate;
	}

	public String getEquipmentSelection() {
		return equipmentSelection;
	}

	public void setEquipmentSelection(String equipmentSelection) {
		this.equipmentSelection = equipmentSelection;
	}

	public String getComponents() {
		return components;
	}

	public void setComponents(String components) {
		this.components = components;
	}

	public String getDisconnectRating() {
		return disconnectRating;
	}

	public void setDisconnectRating(String disconnectRating) {
		this.disconnectRating = disconnectRating;
	}

	public String getDisconnectType() {
		return disconnectType;
	}

	public void setDisconnectType(String disconnectType) {
		this.disconnectType = disconnectType;
	}

}
