
/******************************************************
 * 
 *    	Filename	: MasterSheetType.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This enum is used to handle Master Sheet type data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

public enum MasterSheetType {

	ACTIVITY("ActivityColumn"), COUNTRY("CountryColumn"), COUNTRYREGION("CountryRegionColumn"),
	CURRENCY("CurrencyColumn"), CUSTOMER("CustomerColumn"), CUSTOMER_TYPE("CustomerTypeColumn"), EQUIPMENT("Equipment"),
	EQUIPMENTCATEGORY("EquipementCateColumn"), EQUIPMENTPARAMETER("EquipemetParaColumn"),
	EQUIPMENT_TYPE("EquipemetTypeColumn"), EVENTS("EventColumn"), EVENTS_TYPE("EventTypeColumn"),
	PARAMETER_LIST("ParameterListColumn"), PART_COLUMN("PartColumn"), ROLE_MAP("RoleMappingColumn"),
	SITES("SiteColumn"), SITE_TYPE("SiteTypeColumn"), SOPD_DETAIL("SOPDetailColumn"), STATE("StateColumn"),
	STATUS("StatusColumn"), TIMEZONE("TimezoneColumn"), UOM("UOMColumn"), USERS("UserColumn"),
	USER_ROLE("UserRoleColumn"), VERSION("VersionColumn"), PARAMETER("Parameters");

	public String type;

	MasterSheetType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
