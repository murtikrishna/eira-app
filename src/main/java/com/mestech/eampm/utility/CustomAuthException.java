
/******************************************************
 * 
 *    	Filename	: CustomAuthException.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Custom exception.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.core.AuthenticationException;

public class CustomAuthException extends AuthenticationException {

	private String errMsg;

	public CustomAuthException(String msg) {
		super(msg);
		errMsg = msg;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

}
