
/******************************************************
 * 
 *    	Filename	: RoleMapRequest.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Class is used to handle Role Map Request.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

import java.util.List;

public class RoleMapRequest {

	private Integer roleId;

	private List<Integer> activityId;;

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public List<Integer> getActivityId() {
		return activityId;
	}

	public void setActivityId(List<Integer> activityId) {
		this.activityId = activityId;
	}

}
