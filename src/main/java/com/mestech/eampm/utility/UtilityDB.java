
/******************************************************
 * 
 *    	Filename	: UtilityDB.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Class is used to handle Common data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mestech.eampm.model.CountryRegion;
import com.mestech.eampm.service.CountryRegionService;
import com.mestech.eampm.service.CountryService;

public class UtilityDB {
	private CountryService countryService;

	private CountryRegionService countryRegionService;

	public Map<String, String> getDropdownList(String DropdownName) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "CountryRegion") {
			List<CountryRegion> ddlList = countryRegionService.listCountryRegions();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getCountryRegionId().toString(), ddlList.get(i).getCountryRegionName());

			}

		}

		return ddlMap;
	}

}
