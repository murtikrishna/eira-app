
/******************************************************
 * 
 *    	Filename	: DataCollection.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Data collection.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

import java.math.BigInteger;

public class DataCollection {

	private Integer siteId;

	private String collectionType;

	private Integer operationMode;

	private Integer equipmentFlag;

	private String timezone;
	private Integer serviceFlag2, serviceFlag3, serviceFlag4;

	public Integer getServiceFlag2() {
		return serviceFlag2;
	}

	public void setServiceFlag2(Integer serviceFlag2) {
		this.serviceFlag2 = serviceFlag2;
	}

	public Integer getServiceFlag3() {
		return serviceFlag3;
	}

	public void setServiceFlag3(Integer serviceFlag3) {
		this.serviceFlag3 = serviceFlag3;
	}

	public Integer getServiceFlag4() {
		return serviceFlag4;
	}

	public void setServiceFlag4(Integer serviceFlag4) {
		this.serviceFlag4 = serviceFlag4;
	}

	private Integer todayEnergyFlag;

	private String fileType;

	private String localFtpHomeDirectory;

	private Integer prodFlag;

	private String localFtpUserName;

	private String localFtpPassword;

	private String localFtpDirectoryPath_Inverter;

	private String localFtpDirectory_Inverter;

	private String localFtpDirectoryPath_Modbus;

	private String localFtpDirectory_Modbus;

	private String localFtpDirectoryPath_Sensor;

	private String localFtpDirectory_Sensor;

	private Integer dataLoggerIdInverter;

	private Integer dataLoggerIdModBus;

	private BigInteger dataLoggerIdSensor;

	private Integer serviceCodeInverter;

	private Integer serviceCodeModeBus;

	private Integer serviceCodeSensor;

	private BigInteger apiScheduleInterval;

	private Integer apiDataInterval;

	private String apiUrl;

	private String apiKey;

	private String sensorUrl;
	private String sensorkey;
	private String energyMeterUrl;

	private String energyMeterkey;
	private String scbUrl;

	private String scbkey;
	private String trackerUrl;

	private String trackerkey;

	private Integer dataLoggerIdEnergyMeter;

	private Integer serviceCodeEnergyMeter;

	private Integer dataLoggerIdScb;

	private Integer serviceCodeScb;

	private Integer dataLoggerIdTracker;

	private Integer serviceCodeTracker;
	private BigInteger dataloggertypeid;

	public Integer getProdFlag() {
		return prodFlag;
	}

	public void setProdFlag(Integer prodFlag) {
		this.prodFlag = prodFlag;
	}

	public BigInteger getDataloggertypeid() {
		return dataloggertypeid;
	}

	public void setDataloggertypeid(BigInteger dataloggertypeid) {
		this.dataloggertypeid = dataloggertypeid;
	}

	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	public String getCollectionType() {
		return collectionType;
	}

	public void setCollectionType(String collectionType) {
		this.collectionType = collectionType;
	}

	public Integer getOperationMode() {
		return operationMode;
	}

	public void setOperationMode(Integer operationMode) {
		this.operationMode = operationMode;
	}

	public Integer getEquipmentFlag() {
		return equipmentFlag;
	}

	public void setEquipmentFlag(Integer equipmentFlag) {
		this.equipmentFlag = equipmentFlag;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public Integer getTodayEnergyFlag() {
		return todayEnergyFlag;
	}

	public void setTodayEnergyFlag(Integer todayEnergyFlag) {
		this.todayEnergyFlag = todayEnergyFlag;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getLocalFtpHomeDirectory() {
		return localFtpHomeDirectory;
	}

	public void setLocalFtpHomeDirectory(String localFtpHomeDirectory) {
		this.localFtpHomeDirectory = localFtpHomeDirectory;
	}

	public String getLocalFtpUserName() {
		return localFtpUserName;
	}

	public void setLocalFtpUserName(String localFtpUserName) {
		this.localFtpUserName = localFtpUserName;
	}

	public String getLocalFtpPassword() {
		return localFtpPassword;
	}

	public void setLocalFtpPassword(String localFtpPassword) {
		this.localFtpPassword = localFtpPassword;
	}

	public String getLocalFtpDirectoryPath_Inverter() {
		return localFtpDirectoryPath_Inverter;
	}

	public void setLocalFtpDirectoryPath_Inverter(String localFtpDirectoryPath_Inverter) {
		this.localFtpDirectoryPath_Inverter = localFtpDirectoryPath_Inverter;
	}

	public String getLocalFtpDirectory_Inverter() {
		return localFtpDirectory_Inverter;
	}

	public void setLocalFtpDirectory_Inverter(String localFtpDirectory_Inverter) {
		this.localFtpDirectory_Inverter = localFtpDirectory_Inverter;
	}

	public String getLocalFtpDirectoryPath_Modbus() {
		return localFtpDirectoryPath_Modbus;
	}

	public void setLocalFtpDirectoryPath_Modbus(String localFtpDirectoryPath_Modbus) {
		this.localFtpDirectoryPath_Modbus = localFtpDirectoryPath_Modbus;
	}

	public String getLocalFtpDirectory_Modbus() {
		return localFtpDirectory_Modbus;
	}

	public void setLocalFtpDirectory_Modbus(String localFtpDirectory_Modbus) {
		this.localFtpDirectory_Modbus = localFtpDirectory_Modbus;
	}

	public String getLocalFtpDirectoryPath_Sensor() {
		return localFtpDirectoryPath_Sensor;
	}

	public void setLocalFtpDirectoryPath_Sensor(String localFtpDirectoryPath_Sensor) {
		this.localFtpDirectoryPath_Sensor = localFtpDirectoryPath_Sensor;
	}

	public String getLocalFtpDirectory_Sensor() {
		return localFtpDirectory_Sensor;
	}

	public void setLocalFtpDirectory_Sensor(String localFtpDirectory_Sensor) {
		this.localFtpDirectory_Sensor = localFtpDirectory_Sensor;
	}

	public Integer getDataLoggerIdInverter() {
		return dataLoggerIdInverter;
	}

	public void setDataLoggerIdInverter(Integer dataLoggerIdInverter) {
		this.dataLoggerIdInverter = dataLoggerIdInverter;
	}

	public Integer getDataLoggerIdModBus() {
		return dataLoggerIdModBus;
	}

	public void setDataLoggerIdModBus(Integer dataLoggerIdModBus) {
		this.dataLoggerIdModBus = dataLoggerIdModBus;
	}

	public BigInteger getDataLoggerIdSensor() {
		return dataLoggerIdSensor;
	}

	public void setDataLoggerIdSensor(BigInteger dataLoggerIdSensor) {
		this.dataLoggerIdSensor = dataLoggerIdSensor;
	}

	public Integer getServiceCodeInverter() {
		return serviceCodeInverter;
	}

	public void setServiceCodeInverter(Integer serviceCodeInverter) {
		this.serviceCodeInverter = serviceCodeInverter;
	}

	public Integer getServiceCodeModeBus() {
		return serviceCodeModeBus;
	}

	public void setServiceCodeModeBus(Integer serviceCodeModeBus) {
		this.serviceCodeModeBus = serviceCodeModeBus;
	}

	public Integer getServiceCodeSensor() {
		return serviceCodeSensor;
	}

	public void setServiceCodeSensor(Integer serviceCodeSensor) {
		this.serviceCodeSensor = serviceCodeSensor;
	}

	public BigInteger getApiScheduleInterval() {
		return apiScheduleInterval;
	}

	public void setApiScheduleInterval(BigInteger apiScheduleInterval) {
		this.apiScheduleInterval = apiScheduleInterval;
	}

	public Integer getApiDataInterval() {
		return apiDataInterval;
	}

	public void setApiDataInterval(Integer apiDataInterval) {
		this.apiDataInterval = apiDataInterval;
	}

	public String getApiUrl() {
		return apiUrl;
	}

	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getSensorUrl() {
		return sensorUrl;
	}

	public void setSensorUrl(String sensorUrl) {
		this.sensorUrl = sensorUrl;
	}

	public String getSensorkey() {
		return sensorkey;
	}

	public void setSensorkey(String sensorkey) {
		this.sensorkey = sensorkey;
	}

	public String getEnergyMeterUrl() {
		return energyMeterUrl;
	}

	public void setEnergyMeterUrl(String energyMeterUrl) {
		this.energyMeterUrl = energyMeterUrl;
	}

	public String getEnergyMeterkey() {
		return energyMeterkey;
	}

	public void setEnergyMeterkey(String energyMeterkey) {
		this.energyMeterkey = energyMeterkey;
	}

	public String getScbUrl() {
		return scbUrl;
	}

	public void setScbUrl(String scbUrl) {
		this.scbUrl = scbUrl;
	}

	public String getScbkey() {
		return scbkey;
	}

	public void setScbkey(String scbkey) {
		this.scbkey = scbkey;
	}

	public String getTrackerUrl() {
		return trackerUrl;
	}

	public void setTrackerUrl(String trackerUrl) {
		this.trackerUrl = trackerUrl;
	}

	public String getTrackerkey() {
		return trackerkey;
	}

	public void setTrackerkey(String trackerkey) {
		this.trackerkey = trackerkey;
	}

	public Integer getDataLoggerIdEnergyMeter() {
		return dataLoggerIdEnergyMeter;
	}

	public void setDataLoggerIdEnergyMeter(Integer dataLoggerIdEnergyMeter) {
		this.dataLoggerIdEnergyMeter = dataLoggerIdEnergyMeter;
	}

	public Integer getServiceCodeEnergyMeter() {
		return serviceCodeEnergyMeter;
	}

	public void setServiceCodeEnergyMeter(Integer serviceCodeEnergyMeter) {
		this.serviceCodeEnergyMeter = serviceCodeEnergyMeter;
	}

	public Integer getDataLoggerIdScb() {
		return dataLoggerIdScb;
	}

	public void setDataLoggerIdScb(Integer dataLoggerIdScb) {
		this.dataLoggerIdScb = dataLoggerIdScb;
	}

	public Integer getServiceCodeScb() {
		return serviceCodeScb;
	}

	public void setServiceCodeScb(Integer serviceCodeScb) {
		this.serviceCodeScb = serviceCodeScb;
	}

	public Integer getDataLoggerIdTracker() {
		return dataLoggerIdTracker;
	}

	public void setDataLoggerIdTracker(Integer dataLoggerIdTracker) {
		this.dataLoggerIdTracker = dataLoggerIdTracker;
	}

	public Integer getServiceCodeTracker() {
		return serviceCodeTracker;
	}

	public void setServiceCodeTracker(Integer serviceCodeTracker) {
		this.serviceCodeTracker = serviceCodeTracker;
	}

}
