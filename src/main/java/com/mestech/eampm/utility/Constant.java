
/******************************************************
 * 
 *    	Filename	: Constant.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle constants.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

import java.io.BufferedReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.mestech.eampm.bean.EquipmentListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.bean.SiteViewBean;
import com.mestech.eampm.model.DataSource;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EventDetail;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.SiteStatusService;
import com.mestech.eampm.model.EquipmentCategory;

@Component
@PropertySource("classpath:database.properties")
public class Constant {

	@Autowired
	private EquipmentService equipmentService;

	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();

	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

	@Value("${file.filepath}")
	public String DestFile;

	@Value("${file.filename}")
	public String download_url;

	public String getDestFile() {
		return DestFile;
	}

	public void setDestFile(String destFile) {
		DestFile = destFile;
	}

	public String getDownload_url() {
		return download_url;
	}

	public void setDownload_url(String download_url) {
		this.download_url = download_url;
	}

	public List<String> WeatherData(InputStream is) {

		List<String> lstWeatherData = new ArrayList<String>();

		try {

			try {
				BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
				StringBuilder sb = new StringBuilder();
				int cp;
				while ((cp = rd.read()) != -1) {
					sb.append((char) cp);
				}

				String jsonText = sb.toString();
				;
				JSONObject json = new JSONObject(jsonText);

				String name = "main";
				String Temp = "";
				String Weather = "";
				String Windspeed = "";
				if (json.get("main").toString().contains("\"temp\":") == true) {
					Temp = json.getJSONObject("main").getString("temp");
				}

				if (json.get("wind").toString().contains("\"speed\":") == true) {
					Windspeed = json.getJSONObject("wind").getString("speed");
				}
				if (json.get("weather").toString().contains("\"main\":") == true) {
					Weather = json.getJSONArray("weather").getJSONObject(0).getString("main");
				}
				lstWeatherData.add(Temp);
				lstWeatherData.add(Weather);
				lstWeatherData.add(Windspeed);

			} finally {
				is.close();
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return lstWeatherData;
	}

	public String FutureWeatherData(InputStream is, Authentication authentication) {

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		List<String> lstWeatherData = new ArrayList<String>();
		Map<String, String> map = new HashMap<String, String>();
		String FutureTempValue = "";
		try {

			try {
				BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
				StringBuilder sb = new StringBuilder();
				int cp;
				while ((cp = rd.read()) != -1) {
					sb.append((char) cp);
				}

				String jsonText = sb.toString();
				;
				JSONObject json = new JSONObject(jsonText);

				String Temp1 = "";
				JSONObject data = null;
				JSONArray jsonarry = (JSONArray) json.get("list");

				for (int i = 0; i < jsonarry.length(); i++) {
					data = jsonarry.getJSONObject(i);

					if (data.get("main").toString().contains("\"temp\":") == true) {
						Temp1 = data.getJSONObject("main").getString("temp");

					}

					map.put(data.get("dt_txt").toString(), Temp1);

				}

				/*
				 * Date date = Calendar.getInstance().getTime(); Calendar c =
				 * Calendar.getInstance(); c.setTime(date); c.add(Calendar.DATE, 1);
				 * c.set(Calendar.HOUR, 0); c.set(Calendar.MINUTE, 0); c.set(Calendar.SECOND,
				 * 0);
				 * 
				 * Date dt1 = c.getTime(); DateFormat dateFormat = new
				 * SimpleDateFormat("YYYY-MM-dd hh:mm:ss"); String strDate =
				 * dateFormat.format(dt1); String FinalValue = strDate.replaceAll("12:", "06:");
				 */

				Date date = Calendar.getInstance().getTime();
				Calendar c = Calendar.getInstance();
				c.setTime(date);
				c.add(Calendar.DATE, 1);
				c.set(Calendar.HOUR, 6);
				c.set(Calendar.MINUTE, 00);
				c.set(Calendar.SECOND, 0);

				Date dt1 = c.getTime();
				DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss");

				String strDate = dateFormat.format(dt1);
				System.out.println(strDate);

				FutureTempValue = (String) map.get(strDate);

			} finally {
				is.close();
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return FutureTempValue;
	}

	public List<EquipmentListBean> ForInverter(SiteViewBean objSiteViewBean, List<DataSource> lstDataSource,
			Site objSite, SiteStatus objSiteStatus, String timezonevalue, List<Equipment> lstEquipmentAll, int id,
			List<DataSource> lstDataSourceSesnor) {

		Double dblYesterdayTotalEnergy = 0.0;
		Double dblTotalEnergy = 0.0;
		Double dblTodayEnergy = 0.0;
		Double dblTodayHoursOn = 0.0;
		Double dblCVTodayHoursOn = 0.0;
		Date LstUpdatedDate = null;
		Double Efficiency = null;
		List<EquipmentListBean> lstEquipmentList = new ArrayList<EquipmentListBean>();
		if (objSiteStatus != null) {
			Integer InverterCount = lstDataSource.stream().filter(p -> p.getDismandalFlag() == 0)
					.collect(Collectors.toList()).size();

			for (int j = 0; j < InverterCount; j++) {

				if (lstDataSource.get(j).getTotalEnergy() != null) {
					dblTotalEnergy += lstDataSource.get(j).getTotalEnergy();
				}

				if (lstDataSource.get(j).getYesterdayTotalEnergy() != null) {
					dblYesterdayTotalEnergy += lstDataSource.get(j).getYesterdayTotalEnergy();
				}

				if (lstDataSource.get(j).getTodayHoursOn() != null) {
					dblTodayHoursOn = lstDataSource.get(j).getTodayHoursOn();

					if (dblCVTodayHoursOn < dblTodayHoursOn) {
						dblCVTodayHoursOn = dblTodayHoursOn;
					}

				}

				if (lstDataSource.get(j).getTimestamp() != null) {
					if (LstUpdatedDate == null) {
						LstUpdatedDate = lstDataSource.get(j).getTimestamp();

					} else if (LstUpdatedDate.compareTo(lstDataSource.get(j).getTimestamp()) < 0) {
						LstUpdatedDate = lstDataSource.get(j).getTimestamp();
					}

				}

			}

			dblTodayEnergy = dblTotalEnergy - dblYesterdayTotalEnergy;
			dblTotalEnergy = dblTotalEnergy / 1000;
			dblYesterdayTotalEnergy = dblYesterdayTotalEnergy / 1000;

			objSiteViewBean.setSiteCode(objSite.getCustomerReference());// HAUT1021

			if (objSite.getInstallationCapacity() >= 1000) {
				objSiteViewBean.setCapacity(String.format("%.2f", objSite.getInstallationCapacity() / 1000) + " MW");
			} else {
				objSiteViewBean.setCapacity(String.format("%.2f", objSite.getInstallationCapacity()) + " kW");
			}

			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			String strDate = "";
			if (objSite.getCod() != null) {
				strDate = formatter.format(objSite.getCod());
			}

			objSiteViewBean.setCommisionningDate(strDate);// 20-07-2017
			objSiteViewBean.setSiteStatus(objSiteStatus.getSiteStatus());// Active

			if (dblTodayEnergy >= 1000) {
				Double dblTemp0 = dblTodayEnergy / 1000;
				objSiteViewBean.setTodayEnergy(String.format("%.2f", dblTemp0) + " MWh");

				Double co2avioded = 0.00067 * dblTemp0 * 1000;
				objSiteViewBean.setCo(String.format("%.2f", co2avioded) + " T");

			} else {
				objSiteViewBean.setTodayEnergy(String.format("%.2f", dblTodayEnergy) + " kWh");
				Double co2avioded = 0.00067 * dblTodayEnergy * 1000;
				objSiteViewBean.setCo(String.format("%.2f", co2avioded) + " T");

			}

			objSiteViewBean.setTotalEnergy(String.format("%.2f", dblTotalEnergy) + " MWh");
			Double Totalco2avioded = 0.00067 * dblTotalEnergy * 1000;
			objSiteViewBean.setTotalCo2(String.format("%.2f", Totalco2avioded) + " T");

			Double loadfactor = ((dblYesterdayTotalEnergy) / (objSite.getInstallationCapacity() * 24)) * 100;
			objSiteViewBean.setLoadFactor(String.format("%.2f", loadfactor) + " %");
			if (loadfactor >= 35.0) {
				objSiteViewBean.setLoadFactor("-");
			}

			if (dblTodayEnergy >= 1000) {
				Double dblTemp = dblTodayEnergy / 1000;
				objSiteViewBean.setTodayProductionYeild(String.format("%.2f", dblTemp) + " MWh");// 725.3 kWh
			} else {
				objSiteViewBean.setTodayProductionYeild(String.format("%.2f", dblTodayEnergy) + " kWh");// 725.3 kWh
			}

			// Integer varIntTotalCompletedTicket = lstTicketDetail.stream().filter(p ->
			// p.getState().equals(4)).collect(Collectors.toList()).size();

			objSiteViewBean.setTotalProductionYeild(String.format("%.2f", dblTotalEnergy) + " MWh");// 857 kWh
			objSiteViewBean.setYesterdayProductionYeild(String.format("%.2f", dblYesterdayTotalEnergy) + " MWh");// 185.5
																													// kWh

			if (LstUpdatedDate != null) {

				// GetSystemTimeWithOffsetByMins(LstUpdatedDate,timezonevalue,"dd-MM-yyyy
				// HH:mm");

				// PostgreSQL
				if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
					if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
						LstUpdatedDate = GetZoneTimeOffsetByMins(LstUpdatedDate, timezonevalue);
					}

				}

				objSiteViewBean.setProductionDate(sdfdate.format(LstUpdatedDate));// Production Date : 17-07-2017

				Date CurrentDate = new Date();
				long duration = CurrentDate.getTime() - LstUpdatedDate.getTime();

				long diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(duration);
				long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
				long diffInHours = TimeUnit.MILLISECONDS.toHours(duration);
				long diffInDays = TimeUnit.MILLISECONDS.toDays(duration);

				String EnergyLastUpdate = "";
				if (diffInSeconds < 60) {
					if (diffInSeconds > 0) {
						EnergyLastUpdate = diffInSeconds + " Secs ago..";
					} else {
						EnergyLastUpdate = "Just now updated..";
					}
				} else if (diffInMinutes < 60) {
					EnergyLastUpdate = diffInMinutes + " Mins ago..";
				} else if (diffInHours < 24) {
					EnergyLastUpdate = diffInHours + " Hrs ago..";
				} else if (diffInDays < 1) {
					EnergyLastUpdate = diffInDays + " Days ago..";
				}

				objSiteViewBean.setEnergyLastUpdate(EnergyLastUpdate);// 12 Mins ago..
			}

			Double dblSY = (((dblTodayEnergy) / (objSite.getInstallationCapacity())));

			if (dblSY >= 7.0) {
				objSiteViewBean.setTodayPerformanceRatio("-");
			} else {
				objSiteViewBean.setTodayPerformanceRatio(String.format("%.2f", dblSY) + "");
			}

			objSiteViewBean.setTotalPerformanceRatio("-");// 98.3 %
			objSiteViewBean.setYesterdayPerformanceRatio("-");// 83.5%

			objSiteViewBean.setLocationUrl(objSite.getSiteImage());// https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d446095.8251204708!2d76.76081242324295!3d29.13154359029048!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390dc621d575b2af%3A0x8997e32432dcf12d!2sNestle+Samalkha+Factory!5e0!3m2!1sen!2sin!4v1500908406317

			if (objSite.getSiteName().toString().length() > 30) {
				objSiteViewBean.setLocationName(objSite.getSiteName().substring(0, 30));// SiteName
			} else {
				objSiteViewBean.setLocationName(objSite.getSiteName());// SiteName
			}

			String varIrradiation = "";

			if (objSiteStatus.getIrradiation() == null) {
				objSiteViewBean.setIrradiation("-");//
			} else if (objSiteStatus.getIrradiation() == "") {
				objSiteViewBean.setIrradiation("-");//
			} else {
				varIrradiation = String.format("%.2f", Double.valueOf(objSiteStatus.getIrradiation()));
				objSiteViewBean.setIrradiation(varIrradiation);//
			}

			if (lstDataSourceSesnor.size() == 0) {
				objSiteViewBean.setModuleTemp(" ");// 31�C

			} else {
				if (lstDataSourceSesnor.get(0).getModuleTemperature() != null) {
					String varModuleTemp = String.format("%.1f",
							Double.valueOf(lstDataSourceSesnor.get(0).getModuleTemperature()));
					objSiteViewBean.setModuleTemp(varModuleTemp + " �C");// 31�C
				}
				objSiteViewBean.setModuleTemp(" ");// 31�C
			}

			objSiteViewBean.setIrradiationLastUpdate("-");// 6 days ago...

			objSiteViewBean.setTodayScheduledCleaningJobs("-");// 5
			objSiteViewBean.setTodayScheduledPMJobs("-");// 1
			objSiteViewBean.setTodayScheduledJobs("-");// 4
			objSiteViewBean.setTotalScheduledJobs("-");// 1185

			Integer ErrorInverter = lstDataSource.stream().filter(p -> p.getStatus() == 3 && p.getDismandalFlag() == 0)
					.collect(Collectors.toList()).size();
			// Integer ActiveInverter = InverterCount - ErrorInverter;
			Integer ActiveInverter = lstDataSource.stream().filter(p -> p.getStatus() == 1 && p.getDismandalFlag() == 0)
					.collect(Collectors.toList()).size();

			objSiteViewBean.setTotalInverters(InverterCount.toString());// 8 - 7/8
			objSiteViewBean.setActiveInverters(ActiveInverter.toString());// 7
			objSiteViewBean.setErrorInverters(ErrorInverter.toString());// 1
			objSiteViewBean.setModules("-");// 589

			/*
			 * objSiteViewBean.setTodayCo2Avoided("-");//121.4 kg
			 * objSiteViewBean.setTotalCo2("-");
			 */
			// 97.3 t

			List<Equipment> lstEquipment = lstEquipmentAll.stream().filter(p -> p.getSiteID() == id)
					.collect(Collectors.toList());

			for (int k = 0; k < lstEquipment.size(); k++) {
				EquipmentListBean objEquipmentListBean = new EquipmentListBean();

				if (lstEquipment.get(k).getSiteID() != null) {
					objEquipmentListBean.setSiteID(lstEquipment.get(k).getSiteID().toString());

				}

				if (lstEquipment.get(k).getEquipmentId() != null) {
					objEquipmentListBean.setEquipmentID(lstEquipment.get(k).getEquipmentId().toString());

					Integer CurrentEquipmentId = lstEquipment.get(k).getEquipmentId();

					List<DataSource> myLstDataSource = lstDataSource.stream()
							.filter(p -> p.getEquipmentID().equals(CurrentEquipmentId)).collect(Collectors.toList());

					if (myLstDataSource.size() != 0) {
						DataSource myObjDataSource = myLstDataSource.get(0);

						Double dbYesterdayTotalEnergy = 0.0;
						Double dbTotalEnergy = 0.0;
						Double dbTodayEnergy = 0.0;
						Double dbTodayHoursOn = 0.0;

						Date LastUpdatedDate = null;

						if (myObjDataSource.getTotalEnergy() != null) {
							dbTotalEnergy += myObjDataSource.getTotalEnergy();

						}

						if (myObjDataSource.getYesterdayTotalEnergy() != null) {
							dbYesterdayTotalEnergy += myObjDataSource.getYesterdayTotalEnergy();

						}

						if (myObjDataSource.getTodayHoursOn() != null) {
							dbTodayHoursOn += myObjDataSource.getTodayHoursOn();

						}

						if (myObjDataSource.getTimestamp() != null) {
							LastUpdatedDate = myObjDataSource.getTimestamp();

						}

						if (myObjDataSource.getStatus() != null) {
							objEquipmentListBean.setNetworkStatus(myObjDataSource.getStatus().toString());
						}

						if (myObjDataSource.getApparentPower() != null && myObjDataSource.getInputPower_01() != null) {
							Efficiency = myObjDataSource.getApparentPower() / myObjDataSource.getInputPower_01();

							objEquipmentListBean.setInverterEfficiency(String.format("%.2f", Efficiency));
						} else {
							objEquipmentListBean.setInverterEfficiency("-");
						}
						if (objEquipmentListBean.getInverterEfficiency().equals("Infinity")) {
							objEquipmentListBean.setInverterEfficiency("-");
						}

						dbTodayEnergy = dbTotalEnergy - dbYesterdayTotalEnergy;

						dbTotalEnergy = dbTotalEnergy / 1000;

						objEquipmentListBean.setTodayEnergy(String.format("%.2f", dbTodayEnergy));
						objEquipmentListBean.setTotalEnergy(String.format("%.2f", dbTotalEnergy));

						System.out.println("Equ ID::: " + objEquipmentListBean.getEquipmentID());
						System.out.println("Equipment Name::: " + objEquipmentListBean.getEquipmentName());
						System.out.println("Today E::" + objEquipmentListBean.getTodayEnergy());
						System.out.println("Total E ::" + objEquipmentListBean.getTotalEnergy());

						if (lstEquipment.get(k).getCapacity() != null) {

							if ((dbTodayEnergy) / lstEquipment.get(k).getCapacity() >= 7.0) {
								objEquipmentListBean.setPerformanceRatio("-");
							} else {
								objEquipmentListBean.setPerformanceRatio(
										String.format("%.2f", ((dbTodayEnergy) / lstEquipment.get(k).getCapacity())));

							}

						} else {
							objEquipmentListBean.setPerformanceRatio("-");
						}

						if (LastUpdatedDate == null) {
							objEquipmentListBean.setLastUpdate("-");
						} else {
							// PostgreSQL
							if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
								if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
									LastUpdatedDate = GetZoneTimeOffsetByMins(LastUpdatedDate, timezonevalue);
								}

							}

							objEquipmentListBean.setLastUpdate(sdf.format(LastUpdatedDate));
						}

					}

				}

				if (lstEquipment.get(k).getEquipmentCode() != null) {
					objEquipmentListBean.setEquipmentCode(lstEquipment.get(k).getEquipmentCode());
				}

				if (lstEquipment.get(k).getCustomerReference() != null) {
					objEquipmentListBean.setEquipmentReference(lstEquipment.get(k).getCustomerReference());
					// objEquipmentListBean.setEquipmentReference(lstEquipment.get(k).getPrimarySerialNumber());
				}

				if (lstEquipment.get(k).getDisplayName() != null) {
					objEquipmentListBean.setEquipmentName(lstEquipment.get(k).getDisplayName());
				}

				if (lstEquipment.get(k).getEquipmentTypeName() != null) {
					objEquipmentListBean.setEquipmentType(lstEquipment.get(k).getEquipmentTypeName());
				}

				if (lstEquipment.get(k).getCustomerNaming() != null) {
					objEquipmentListBean.setCustomerNaming(lstEquipment.get(k).getCustomerNaming());
				}

				if (lstEquipment.get(k).getDismandalFlag().equals(0)) {
					lstEquipmentList.add(objEquipmentListBean);
				}

			}
		}

		return lstEquipmentList;
	}

	public List<EquipmentListBean> ForEnergymeter(SiteViewBean objSiteViewBean, List<DataSource> lstDataSource,
			Site objSite, SiteStatus objSiteStatus, String timezonevalue, List<Equipment> lstEquipmentAll, int id,
			List<DataSource> lstDataSourceSesnor) {

		Double dblYesterdayTotalEnergy = 0.0;
		Double dblTotalEnergy = 0.0;
		Double dblTodayEnergy = 0.0;
		Double dblTodayHoursOn = 0.0;
		Double dblCVTodayHoursOn = 0.0;
		Date LstUpdatedDate = null;
		Double Efficiency = null;
		Integer EnergymeterCount = 0;

		Integer InverterCount = lstDataSource.stream().filter(p -> p.getDismandalFlag() == 0)
				.collect(Collectors.toList()).size();

		for (int j = 0; j < InverterCount; j++) {

			List<EquipmentCategory> category = equipmentService
					.getCategoryByEquipmentIds(lstDataSource.get(j).getEquipmentID());

			if (category.get(0).getEquipmentCategory().equals("Primary Energy Meter")) {
				EnergymeterCount += 1;

				if (lstDataSource.get(j).getTotalEnergy() != null) {
					dblTotalEnergy += lstDataSource.get(j).getTotalEnergy();
				}

				if (lstDataSource.get(j).getYesterdayTotalEnergy() != null) {
					dblYesterdayTotalEnergy += lstDataSource.get(j).getYesterdayTotalEnergy();
				}

				if (lstDataSource.get(j).getTodayHoursOn() != null) {
					dblTodayHoursOn = lstDataSource.get(j).getTodayHoursOn();

					if (dblCVTodayHoursOn < dblTodayHoursOn) {
						dblCVTodayHoursOn = dblTodayHoursOn;
					}

				}

				if (lstDataSource.get(j).getTimestamp() != null) {
					if (LstUpdatedDate == null) {
						LstUpdatedDate = lstDataSource.get(j).getTimestamp();

					} else if (LstUpdatedDate.compareTo(lstDataSource.get(j).getTimestamp()) < 0) {
						LstUpdatedDate = lstDataSource.get(j).getTimestamp();
					}

				}

			} else {

			}
		}

		dblTodayEnergy = dblTotalEnergy - dblYesterdayTotalEnergy;
		// dblTotalEnergy = dblTotalEnergy/1000;
		dblYesterdayTotalEnergy = dblYesterdayTotalEnergy / 1000;

		objSiteViewBean.setSiteCode(objSite.getCustomerReference());// HAUT1021

		if (objSite.getInstallationCapacity() >= 1000) {
			objSiteViewBean.setCapacity(String.format("%.2f", objSite.getInstallationCapacity() / 1000) + " MW");
		} else {
			objSiteViewBean.setCapacity(String.format("%.2f", objSite.getInstallationCapacity()) + " kW");
		}

		String strDate = "";
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		if (objSite.getCod() != null) {
			strDate = formatter.format(objSite.getCod());
		}
		objSiteViewBean.setCommisionningDate(strDate);// 20-07-2017
		objSiteViewBean.setSiteStatus(objSiteStatus.getSiteStatus());// Active

		if (dblTodayEnergy >= 1000) {
			Double dblTemp0 = dblTodayEnergy / 1000;
			objSiteViewBean.setTodayEnergy(String.format("%.2f", dblTemp0) + " MWh");

			Double co2avioded = 0.00067 * dblTodayEnergy * 1000;
			objSiteViewBean.setCo(String.format("%.2f", co2avioded) + " T");

		} else {
			objSiteViewBean.setTodayEnergy(String.format("%.2f", dblTodayEnergy) + " kWh");
			Double co2avioded = 0.00067 * dblTodayEnergy * 1000;
			objSiteViewBean.setCo(String.format("%.2f", co2avioded) + " T");

		}

		objSiteViewBean.setTotalEnergy(String.format("%.2f", dblTotalEnergy) + " MWh");

		Double loadfactor = ((dblYesterdayTotalEnergy) / (objSite.getInstallationCapacity() * 24)) * 100;
		objSiteViewBean.setLoadFactor(String.format("%.2f", loadfactor) + " %");
		if (loadfactor >= 35.0) {
			objSiteViewBean.setLoadFactor("-");
		}

		if (dblTodayEnergy >= 1000) {
			Double dblTemp = dblTodayEnergy / 1000;
			objSiteViewBean.setTodayProductionYeild(String.format("%.2f", dblTemp) + " MWh");// 725.3 kWh
		} else {
			objSiteViewBean.setTodayProductionYeild(String.format("%.2f", dblTodayEnergy) + " kWh");// 725.3 kWh
		}
		if (dblTotalEnergy >= 1000000) {
			Double dblTemp = dblTotalEnergy / 1000;
			objSiteViewBean.setTotalProductionYeild(String.format("%.2f", dblTemp) + " MWh");// 857 kWh
			Double Totalco2avioded = 0.00067 * dblTotalEnergy * 1000;
			objSiteViewBean.setTotalCo2(String.format("%.2f", Totalco2avioded) + " T");
		} else {
			objSiteViewBean.setTotalProductionYeild(String.format("%.2f", dblTotalEnergy) + " MWh");// 857 kWh
			Double Totalco2avioded = 0.00067 * dblTotalEnergy * 1000;
			objSiteViewBean.setTotalCo2(String.format("%.2f", Totalco2avioded) + " T");
		}

		objSiteViewBean.setYesterdayProductionYeild(String.format("%.2f", dblYesterdayTotalEnergy) + " MWh");// 185.5
																												// kWh

		if (LstUpdatedDate != null) {

			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
					LstUpdatedDate = GetZoneTimeOffsetByMins(LstUpdatedDate, timezonevalue);
				}

			}

			objSiteViewBean.setProductionDate(sdfdate.format(LstUpdatedDate));// Production Date : 17-07-2017

			Date CurrentDate = new Date();
			long duration = CurrentDate.getTime() - LstUpdatedDate.getTime();

			long diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(duration);
			long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
			long diffInHours = TimeUnit.MILLISECONDS.toHours(duration);
			long diffInDays = TimeUnit.MILLISECONDS.toDays(duration);

			String EnergyLastUpdate = "";
			if (diffInSeconds < 60) {
				if (diffInSeconds > 0) {
					EnergyLastUpdate = diffInSeconds + " Secs ago..";
				} else {
					EnergyLastUpdate = "Just now updated..";
				}
			} else if (diffInMinutes < 60) {
				EnergyLastUpdate = diffInMinutes + " Mins ago..";
			} else if (diffInHours < 24) {
				EnergyLastUpdate = diffInHours + " Hrs ago..";
			} else if (diffInDays < 1) {
				EnergyLastUpdate = diffInDays + " Days ago..";
			}

			objSiteViewBean.setEnergyLastUpdate(EnergyLastUpdate);// 12 Mins ago..
		}

		Double dblSY = (((dblTodayEnergy) / (objSite.getInstallationCapacity())));
		;

		if (dblSY >= 7.0) {
			objSiteViewBean.setTodayPerformanceRatio("-");
		} else {
			objSiteViewBean.setTodayPerformanceRatio(String.format("%.2f", dblSY) + "");
		}

		objSiteViewBean.setTotalPerformanceRatio("-");// 98.3 %
		objSiteViewBean.setYesterdayPerformanceRatio("-");// 83.5%

		objSiteViewBean.setLocationUrl(objSite.getSiteImage());// https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d446095.8251204708!2d76.76081242324295!3d29.13154359029048!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390dc621d575b2af%3A0x8997e32432dcf12d!2sNestle+Samalkha+Factory!5e0!3m2!1sen!2sin!4v1500908406317

		if (objSite.getSiteName().toString().length() > 30) {
			objSiteViewBean.setLocationName(objSite.getSiteName().substring(0, 30));// SiteName
		} else {
			objSiteViewBean.setLocationName(objSite.getSiteName());// SiteName
		}

		String varIrradiation = "";

		if (objSiteStatus.getIrradiation() == null) {
			objSiteViewBean.setIrradiation("-");//
		} else if (objSiteStatus.getIrradiation() == "") {
			objSiteViewBean.setIrradiation("-");//
		} else {
			varIrradiation = String.format("%.2f", Double.valueOf(objSiteStatus.getIrradiation()));
			objSiteViewBean.setIrradiation(varIrradiation);//
		}

		if (lstDataSourceSesnor.size() == 0) {
			objSiteViewBean.setModuleTemp("-");// 31�C
		} else {
			if (lstDataSourceSesnor.get(0).getModuleTemperature() != null) {

				String varModuleTemp = String.format("%.1f",
						Double.valueOf(lstDataSourceSesnor.get(0).getModuleTemperature()));
				objSiteViewBean.setModuleTemp(varModuleTemp + " �C");// 31�C
			} else {
				objSiteViewBean.setModuleTemp("-");// 31�C
			}

		}

		objSiteViewBean.setTodayScheduledCleaningJobs("-");// 5
		objSiteViewBean.setTodayScheduledPMJobs("-");// 1
		objSiteViewBean.setTodayScheduledJobs("-");// 4
		objSiteViewBean.setTotalScheduledJobs("-");// 1185

		Integer ErrorInverter = lstDataSource.stream().filter(p -> p.getStatus() == 3 && p.getDismandalFlag() == 0)
				.collect(Collectors.toList()).size();
		// Integer ActiveInverter = InverterCount - ErrorInverter;
		Integer ActiveInverter = lstDataSource.stream().filter(p -> p.getStatus() == 1 && p.getDismandalFlag() == 0)
				.collect(Collectors.toList()).size();

		objSiteViewBean.setEnergymeterCount(EnergymeterCount);
		objSiteViewBean.setActiveEms(ActiveInverter.toString());
		objSiteViewBean.setErrorEms(ErrorInverter.toString());

		objSiteViewBean.setModules("-");// 589

		List<EquipmentListBean> lstEquipmentList = new ArrayList<EquipmentListBean>();

		List<Equipment> lstEquipment = lstEquipmentAll.stream().filter(p -> p.getSiteID() == id)
				.collect(Collectors.toList());

		for (int k = 0; k < lstEquipment.size(); k++) {
			EquipmentListBean objEquipmentListBean = new EquipmentListBean();

			if (lstEquipment.get(k).getSiteID() != null) {
				objEquipmentListBean.setSiteID(lstEquipment.get(k).getSiteID().toString());

			}

			if (lstEquipment.get(k).getEquipmentId() != null) {
				objEquipmentListBean.setEquipmentID(lstEquipment.get(k).getEquipmentId().toString());

				Integer CurrentEquipmentId = lstEquipment.get(k).getEquipmentId();

				List<DataSource> myLstDataSource = lstDataSource.stream()
						.filter(p -> p.getEquipmentID().equals(CurrentEquipmentId)).collect(Collectors.toList());

				if (myLstDataSource.size() != 0) {
					DataSource myObjDataSource = myLstDataSource.get(0);

					Double dbYesterdayTotalEnergy = 0.0;
					Double dbTotalEnergy = 0.0;
					Double dbTodayEnergy = 0.0;
					Double dbTodayHoursOn = 0.0;
					Double dbActivePower = 0.0;
					Double dbReActivePower = 0.0;
					Double dbPowerFactor = 0.0;

					Date LastUpdatedDate = null;

					if (myObjDataSource.getTotalEnergy() != null) {
						dbTotalEnergy += myObjDataSource.getTotalEnergy();

					}

					if (myObjDataSource.getActivePower() != null) {
						dbActivePower = myObjDataSource.getActivePower();

					}
					if (myObjDataSource.getReactivePower() != null) {
						dbReActivePower = myObjDataSource.getReactivePower();

					}
					if (myObjDataSource.getPowerFactor() != null) {
						dbPowerFactor = myObjDataSource.getPowerFactor();

					}

					if (myObjDataSource.getYesterdayTotalEnergy() != null) {
						dbYesterdayTotalEnergy += myObjDataSource.getYesterdayTotalEnergy();

					}

					if (myObjDataSource.getTodayHoursOn() != null) {
						dbTodayHoursOn += myObjDataSource.getTodayHoursOn();

					}

					if (myObjDataSource.getTimestamp() != null) {
						LastUpdatedDate = myObjDataSource.getTimestamp();

					}

					if (myObjDataSource.getStatus() != null) {
						objEquipmentListBean.setNetworkStatus(myObjDataSource.getStatus().toString());
					}

					dbTodayEnergy = dbTotalEnergy - dbYesterdayTotalEnergy;

					dbTotalEnergy = dbTotalEnergy / 1000;

					objEquipmentListBean.setTodayEnergy(String.format("%.2f", dbTodayEnergy));
					objEquipmentListBean.setTotalEnergy(String.format("%.2f", dbTotalEnergy));

					objEquipmentListBean.setActivePower(String.format("%.2f", dbActivePower));
					objEquipmentListBean.setReactivePower(String.format("%.2f", dbReActivePower));
					objEquipmentListBean.setPowerFactor(String.format("%.2f", dbPowerFactor));

					System.out.println("Equ ID::: " + objEquipmentListBean.getEquipmentID());
					System.out.println("Equipment Name::: " + objEquipmentListBean.getEquipmentName());
					System.out.println("Today E::" + objEquipmentListBean.getTodayEnergy());
					System.out.println("Total E ::" + objEquipmentListBean.getTotalEnergy());

					if (lstEquipment.get(k).getCapacity() != null) {

						if ((dbTodayEnergy) / lstEquipment.get(k).getCapacity() >= 7.0) {
							objEquipmentListBean.setPerformanceRatio("-");
						} else {
							objEquipmentListBean.setPerformanceRatio(
									String.format("%.2f", ((dbTodayEnergy) / lstEquipment.get(k).getCapacity())));

						}

					} else {
						objEquipmentListBean.setPerformanceRatio("-");
					}

					if (LastUpdatedDate == null) {
						objEquipmentListBean.setLastUpdate("-");
					} else {
						// PostgreSQL
						if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
							if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
								LastUpdatedDate = GetZoneTimeOffsetByMins(LastUpdatedDate, timezonevalue);
							}

						}

						objEquipmentListBean.setLastUpdate(sdf.format(LastUpdatedDate));
					}

				}

			}

			if (lstEquipment.get(k).getEquipmentCode() != null) {
				objEquipmentListBean.setEquipmentCode(lstEquipment.get(k).getEquipmentCode());
			}

			if (lstEquipment.get(k).getCustomerReference() != null) {
				objEquipmentListBean.setEquipmentReference(lstEquipment.get(k).getCustomerReference());
				// objEquipmentListBean.setEquipmentReference(lstEquipment.get(k).getPrimarySerialNumber());
			}

			if (lstEquipment.get(k).getDisplayName() != null) {
				objEquipmentListBean.setEquipmentName(lstEquipment.get(k).getDisplayName());
			}

			if (lstEquipment.get(k).getEquipmentTypeName() != null) {
				objEquipmentListBean.setEquipmentType(lstEquipment.get(k).getEquipmentTypeName());
			}
			if (lstEquipment.get(k).getCustomerNaming() != null) {
				objEquipmentListBean.setCustomerNaming(lstEquipment.get(k).getCustomerNaming());
			}

			if (lstEquipment.get(k).getDismandalFlag().equals(0)) {
				lstEquipmentList.add(objEquipmentListBean);
			}

		}
		return lstEquipmentList;
	}

	public List<EquipmentListBean> ForInverterGrid(SiteViewBean objSiteViewBean, List<DataSource> lstDataSource,
			Site objSite, SiteStatus objSiteStatus, String timezonevalue, List<Equipment> lstEquipmentAll, int id) {

		Double dblYesterdayTotalEnergy = 0.0;
		Double dblTotalEnergy = 0.0;
		Double dblTodayEnergy = 0.0;
		Double dblTodayHoursOn = 0.0;
		Double dblCVTodayHoursOn = 0.0;
		Date LstUpdatedDate = null;
		Double Efficiency = null;

		Integer InverterCount = lstDataSource.stream().filter(p -> p.getDismandalFlag() == 0)
				.collect(Collectors.toList()).size();

		Integer ErrorInverter = lstDataSource.stream().filter(p -> p.getStatus() == 3 && p.getDismandalFlag() == 0)
				.collect(Collectors.toList()).size();
		// Integer ActiveInverter = InverterCount - ErrorInverter;
		Integer ActiveInverter = lstDataSource.stream().filter(p -> p.getStatus() == 1 && p.getDismandalFlag() == 0)
				.collect(Collectors.toList()).size();

		objSiteViewBean.setTotalInverters(InverterCount.toString());// 8 - 7/8
		objSiteViewBean.setActiveInverters(ActiveInverter.toString());// 7
		objSiteViewBean.setErrorInverters(ErrorInverter.toString());// 1
		objSiteViewBean.setModules("-");// 589

		List<EquipmentListBean> lstEquipmentList = new ArrayList<EquipmentListBean>();

		List<Equipment> lstEquipment = lstEquipmentAll.stream().filter(p -> p.getSiteID() == id)
				.collect(Collectors.toList());

		for (int k = 0; k < lstEquipment.size(); k++) {
			EquipmentListBean objEquipmentListBean = new EquipmentListBean();

			if (lstEquipment.get(k).getSiteID() != null) {
				objEquipmentListBean.setSiteID(lstEquipment.get(k).getSiteID().toString());

			}

			if (lstEquipment.get(k).getEquipmentId() != null) {
				objEquipmentListBean.setEquipmentID(lstEquipment.get(k).getEquipmentId().toString());

				Integer CurrentEquipmentId = lstEquipment.get(k).getEquipmentId();

				List<DataSource> myLstDataSource = lstDataSource.stream()
						.filter(p -> p.getEquipmentID().equals(CurrentEquipmentId)).collect(Collectors.toList());

				if (myLstDataSource.size() != 0) {
					DataSource myObjDataSource = myLstDataSource.get(0);

					Double dbYesterdayTotalEnergy = 0.0;
					Double dbTotalEnergy = 0.0;
					Double dbTodayEnergy = 0.0;
					Double dbTodayHoursOn = 0.0;

					Date LastUpdatedDate = null;

					if (myObjDataSource.getTotalEnergy() != null) {
						dbTotalEnergy += myObjDataSource.getTotalEnergy();

					}

					if (myObjDataSource.getYesterdayTotalEnergy() != null) {
						dbYesterdayTotalEnergy += myObjDataSource.getYesterdayTotalEnergy();

					}

					if (myObjDataSource.getTodayHoursOn() != null) {
						dbTodayHoursOn += myObjDataSource.getTodayHoursOn();

					}

					if (myObjDataSource.getTimestamp() != null) {
						LastUpdatedDate = myObjDataSource.getTimestamp();

					}

					if (myObjDataSource.getStatus() != null) {
						objEquipmentListBean.setNetworkStatus(myObjDataSource.getStatus().toString());
					}

					if (myObjDataSource.getApparentPower() != null && myObjDataSource.getInputPower_01() != null) {
						Efficiency = myObjDataSource.getApparentPower() / myObjDataSource.getInputPower_01();

						objEquipmentListBean.setInverterEfficiency(String.format("%.2f", Efficiency));
					} else {
						objEquipmentListBean.setInverterEfficiency("-");
					}
					if (objEquipmentListBean.getInverterEfficiency().equals("Infinity")) {
						objEquipmentListBean.setInverterEfficiency("-");
					}

					dbTodayEnergy = dbTotalEnergy - dbYesterdayTotalEnergy;

					dbTotalEnergy = dbTotalEnergy / 1000;

					objEquipmentListBean.setTodayEnergy(String.format("%.2f", dbTodayEnergy));
					objEquipmentListBean.setTotalEnergy(String.format("%.2f", dbTotalEnergy));

					System.out.println("Equ ID::: " + objEquipmentListBean.getEquipmentID());
					System.out.println("Equipment Name::: " + objEquipmentListBean.getEquipmentName());
					System.out.println("Today E::" + objEquipmentListBean.getTodayEnergy());
					System.out.println("Total E ::" + objEquipmentListBean.getTotalEnergy());

					if (lstEquipment.get(k).getCapacity() != null) {

						/*
						 * if(dbTodayHoursOn <=0) { objEquipmentListBean.setPerformanceRatio("-"); }
						 * else { objEquipmentListBean.setPerformanceRatio(String.format("%.2f",
						 * ((dbTodayEnergy/dbTodayHoursOn)/(Double.parseDouble(lstEquipment.get(k).
						 * getCapacity()))) * 100)); }
						 */

						if ((dbTodayEnergy) / lstEquipment.get(k).getCapacity() >= 7.0) {
							objEquipmentListBean.setPerformanceRatio("-");
						} else {
							objEquipmentListBean.setPerformanceRatio(
									String.format("%.2f", ((dbTodayEnergy) / lstEquipment.get(k).getCapacity())));

						}

					} else {
						objEquipmentListBean.setPerformanceRatio("-");
					}

					if (LastUpdatedDate == null) {
						objEquipmentListBean.setLastUpdate("-");
					} else {
						// PostgreSQL
						if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
							if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
								LastUpdatedDate = GetZoneTimeOffsetByMins(LastUpdatedDate, timezonevalue);
							}

						}

						objEquipmentListBean.setLastUpdate(sdf.format(LastUpdatedDate));
					}

				}

			}

			if (lstEquipment.get(k).getEquipmentCode() != null) {
				objEquipmentListBean.setEquipmentCode(lstEquipment.get(k).getEquipmentCode());
			}

			if (lstEquipment.get(k).getCustomerReference() != null) {
				objEquipmentListBean.setEquipmentReference(lstEquipment.get(k).getCustomerReference());
				// objEquipmentListBean.setEquipmentReference(lstEquipment.get(k).getPrimarySerialNumber());
			}

			if (lstEquipment.get(k).getDisplayName() != null) {
				objEquipmentListBean.setEquipmentName(lstEquipment.get(k).getDisplayName());
			}

			if (lstEquipment.get(k).getEquipmentTypeName() != null) {
				objEquipmentListBean.setEquipmentType(lstEquipment.get(k).getEquipmentTypeName());
			}
			if (lstEquipment.get(k).getCustomerNaming() != null) {
				objEquipmentListBean.setCustomerNaming(lstEquipment.get(k).getCustomerNaming());
			}

			if (lstEquipment.get(k).getDismandalFlag().equals(0)) {
				lstEquipmentList.add(objEquipmentListBean);
			}

		}
		return lstEquipmentList;
	}

	public List<EquipmentListBean> ForScbGrid(SiteViewBean objSiteViewBean, List<DataSource> lstDataSource,
			Site objSite, SiteStatus objSiteStatus, String timezonevalue, List<Equipment> lstEquipmentAll, int id) {

		List<EquipmentListBean> lstEquipmentList = new ArrayList<EquipmentListBean>();

		List<Equipment> lstEquipment = lstEquipmentAll.stream().filter(p -> p.getSiteID() == id)
				.collect(Collectors.toList());

		for (int k = 0; k < lstEquipment.size(); k++) {
			EquipmentListBean objEquipmentListBean = new EquipmentListBean();

			if (lstEquipment.get(k).getSiteID() != null) {
				objEquipmentListBean.setSiteID(lstEquipment.get(k).getSiteID().toString());

			}

			if (lstEquipment.get(k).getEquipmentId() != null) {
				objEquipmentListBean.setEquipmentID(lstEquipment.get(k).getEquipmentId().toString());

				Integer CurrentEquipmentId = lstEquipment.get(k).getEquipmentId();

				List<DataSource> myLstDataSource = lstDataSource.stream()
						.filter(p -> p.getEquipmentID().equals(CurrentEquipmentId)).collect(Collectors.toList());

				if (myLstDataSource.size() != 0) {
					DataSource myObjDataSource = myLstDataSource.get(0);

					Double dbPower = 0.0;
					Double dbVoltage = 0.0;
					Double dbCurrent = 0.0;
					Double dbTemp = 0.0;

					Date LastUpdatedDate = null;

					if (myObjDataSource.getTimestamp() != null) {
						LastUpdatedDate = myObjDataSource.getTimestamp();

					}

					if (myObjDataSource.getInputPower_01() != null) {
						dbPower = myObjDataSource.getInputPower_01();

					}

					if (myObjDataSource.getInputVoltage_01() != null) {
						dbVoltage = myObjDataSource.getInputVoltage_01();

					}

					if (myObjDataSource.getInputCurrent_01() != null) {
						dbCurrent = myObjDataSource.getInputCurrent_01();

					}
					if (myObjDataSource.getAmbientTemperature() != null) {
						dbTemp = myObjDataSource.getAmbientTemperature();

					}

					if (myObjDataSource.getStatus() != null) {
						objEquipmentListBean.setNetworkStatus(myObjDataSource.getStatus().toString());
					}

					objEquipmentListBean.setPower(String.format("%.2f", dbPower));
					objEquipmentListBean.setVoltage(String.format("%.2f", dbVoltage));
					objEquipmentListBean.setCurrent(String.format("%.2f", dbCurrent));
					objEquipmentListBean.setTemp(String.format("%.2f", dbTemp));

					if (LastUpdatedDate == null) {
						objEquipmentListBean.setLastUpdate("-");
					} else {
						// PostgreSQL
						if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
							if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
								LastUpdatedDate = GetZoneTimeOffsetByMins(LastUpdatedDate, timezonevalue);
							}

						}

						objEquipmentListBean.setLastUpdate(sdf.format(LastUpdatedDate));
					}

				}

			}

			if (lstEquipment.get(k).getEquipmentCode() != null) {
				objEquipmentListBean.setEquipmentCode(lstEquipment.get(k).getEquipmentCode());
			}

			if (lstEquipment.get(k).getCustomerReference() != null) {
				objEquipmentListBean.setEquipmentReference(lstEquipment.get(k).getCustomerReference());
				// objEquipmentListBean.setEquipmentReference(lstEquipment.get(k).getPrimarySerialNumber());
			}

			if (lstEquipment.get(k).getDisplayName() != null) {
				objEquipmentListBean.setEquipmentName(lstEquipment.get(k).getDisplayName());
			}

			if (lstEquipment.get(k).getEquipmentTypeName() != null) {
				objEquipmentListBean.setEquipmentType(lstEquipment.get(k).getEquipmentTypeName());
			}
			if (lstEquipment.get(k).getCustomerNaming() != null) {
				objEquipmentListBean.setCustomerNaming(lstEquipment.get(k).getCustomerNaming());
			}

			if (lstEquipment.get(k).getDismandalFlag().equals(0)) {
				lstEquipmentList.add(objEquipmentListBean);
			}

		}
		return lstEquipmentList;
	}

	public List<EquipmentListBean> ForTrackerGrid(SiteViewBean objSiteViewBean, List<DataSource> lstDataSource,
			Site objSite, SiteStatus objSiteStatus, String timezonevalue, List<Equipment> lstEquipment, int id) {

		List<EquipmentListBean> lstEquipmentList = new ArrayList<EquipmentListBean>();

		for (int k = 0; k < lstEquipment.size(); k++) {
			EquipmentListBean objEquipmentListBean = new EquipmentListBean();

			if (lstEquipment.get(k).getSiteID() != null) {
				objEquipmentListBean.setSiteID(lstEquipment.get(k).getSiteID().toString());

			}

			if (lstEquipment.get(k).getEquipmentId() != null) {
				objEquipmentListBean.setEquipmentID(lstEquipment.get(k).getEquipmentId().toString());

				Integer CurrentEquipmentId = lstEquipment.get(k).getEquipmentId();

				List<DataSource> myLstDataSource = lstDataSource.stream()
						.filter(p -> p.getEquipmentID().equals(CurrentEquipmentId)).collect(Collectors.toList());

				if (myLstDataSource.size() != 0) {
					DataSource myObjDataSource = myLstDataSource.get(0);

					Double Sunangle = 0.0;
					Double Trackerangle = 0.0;
					Double windspeed = 0.0;
					String Mode = "";

					Date LastUpdatedDate = null;

					if (myObjDataSource.getTimestamp() != null) {
						LastUpdatedDate = myObjDataSource.getTimestamp();

					}

					if (myObjDataSource.getSunAngle() != null) {
						Sunangle = myObjDataSource.getSunAngle();

					}

					if (myObjDataSource.getTrackerAngle() != null) {
						Trackerangle = myObjDataSource.getTrackerAngle();

					}

					if (myObjDataSource.getWindSpeed() != null) {
						windspeed = myObjDataSource.getWindSpeed();

					}

					if (myObjDataSource.getStatus() != null) {
						objEquipmentListBean.setNetworkStatus(myObjDataSource.getStatus().toString());
					}

					objEquipmentListBean.setSunangle(String.format("%.2f", Sunangle));
					objEquipmentListBean.setTrackerangle(String.format("%.2f", Trackerangle));
					objEquipmentListBean.setWindspeed(String.format("%.2f", windspeed));

					if (LastUpdatedDate == null) {
						objEquipmentListBean.setLastUpdate("-");
					} else {
						// PostgreSQL
						if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
							if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
								LastUpdatedDate = GetZoneTimeOffsetByMins(LastUpdatedDate, timezonevalue);
							}

						}

						objEquipmentListBean.setLastUpdate(sdf.format(LastUpdatedDate));
					}

				}

			}

			if (lstEquipment.get(k).getEquipmentCode() != null) {
				objEquipmentListBean.setEquipmentCode(lstEquipment.get(k).getEquipmentCode());
			}

			if (lstEquipment.get(k).getCustomerReference() != null) {
				objEquipmentListBean.setEquipmentReference(lstEquipment.get(k).getCustomerReference());
				// objEquipmentListBean.setEquipmentReference(lstEquipment.get(k).getPrimarySerialNumber());
			}

			if (lstEquipment.get(k).getDisplayName() != null) {
				objEquipmentListBean.setEquipmentName(lstEquipment.get(k).getDisplayName());
			}

			if (lstEquipment.get(k).getEquipmentTypeName() != null) {
				objEquipmentListBean.setEquipmentType(lstEquipment.get(k).getEquipmentTypeName());
			}

			if (lstEquipment.get(k).getCustomerNaming() != null) {
				objEquipmentListBean.setCustomerNaming(lstEquipment.get(k).getCustomerNaming());
			}

			if (lstEquipment.get(k).getDismandalFlag().equals(0)) {
				lstEquipmentList.add(objEquipmentListBean);
			}

		}
		return lstEquipmentList;
	}

	public Date GetZoneTimeOffsetByMins(Date AppServerDate, String TimezoneOffset) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(AppServerDate);
		Integer OffsetInMins = Integer.valueOf(TimezoneOffset);
		cal.add(Calendar.MINUTE, OffsetInMins);
		return cal.getTime();
	}

	public static void main(String[] args) {

		send("support@eira.io", "eira@ice4ever", "johnsonedwinraj@contractor.net", "Welcome Mail", "Hi");
		System.out.println("sent");
	}

	public static void send(String from, String password, String to, String sub, String msg) {
		// Get properties object
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		// get Session
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, password);
			}
		});
		// compose message
		try {
			MimeMessage message = new MimeMessage(session);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(sub);
			message.setText(msg);
			// send message
			Transport.send(message);
			System.out.println("message sent successfully");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}

}
