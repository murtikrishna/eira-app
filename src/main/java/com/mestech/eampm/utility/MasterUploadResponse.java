
/******************************************************
 * 
 *    	Filename	: MasterUploadResponse.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This interface is used to handle master upload response.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

import java.util.List;

public interface MasterUploadResponse<T> {

	public List<T> response(List<T> list);

}
