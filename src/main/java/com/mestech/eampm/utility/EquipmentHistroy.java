
/******************************************************
 * 
 *    	Filename	: EquipmentHistroy.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Equipment History.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

import java.util.Date;

public class EquipmentHistroy {

	private String isPrimary;

	private String installationDate;

	private String warantryExpire;

	private String description, remarks;

	private Integer replacementCount;

	private Date lastReplacement;

	public EquipmentHistroy() {
		super();
	}

	public EquipmentHistroy(String isPrimary, String installationDate, String warantryExpire, String description,
			String remarks, Integer replacementCount, Date lastReplacement) {
		super();
		this.isPrimary = isPrimary;
		this.installationDate = installationDate;
		this.warantryExpire = warantryExpire;
		this.description = description;
		this.remarks = remarks;
		this.replacementCount = replacementCount;
		this.lastReplacement = lastReplacement;
	}

	public String getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(String isPrimary) {
		this.isPrimary = isPrimary;
	}

	public String getInstallationDate() {
		return installationDate;
	}

	public void setInstallationDate(String installationDate) {
		this.installationDate = installationDate;
	}

	public String getWarantryExpire() {
		return warantryExpire;
	}

	public void setWarantryExpire(String warantryExpire) {
		this.warantryExpire = warantryExpire;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getReplacementCount() {
		return replacementCount;
	}

	public void setReplacementCount(Integer replacementCount) {
		this.replacementCount = replacementCount;
	}

	public Date getLastReplacement() {
		return lastReplacement;
	}

	public void setLastReplacement(Date lastReplacement) {
		this.lastReplacement = lastReplacement;
	}

}
