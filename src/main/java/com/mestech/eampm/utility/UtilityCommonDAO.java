
/******************************************************
 * 
 *    	Filename	: UtilityCommonDAO.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Class is used to handle common data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

public class UtilityCommonDAO {

	public static String DBType = "PostgreSQL"; // Oracle
	public static String TimezoneType = "UTC"; // IST

//	public static String DBType = "Oracle"; //PostgreSQL
//	public static String TimezoneType  = "IST"; // UTC

	public static String OracleDB = "Oracle";
	public static String PostgreDB = "PostgreSQL";
	public static String UTCZone = "UTC";
	public static String ISTZone = "IST";

	public UtilityCommonDAO() {
		// TODO Auto-generated constructor stub
	}
}
