
/******************************************************
 * 
 *    	Filename	: UtilityCommon.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Class is used to handle common data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

import java.math.BigDecimal;
import java.util.Date;

public class UtilityCommon {

	public static String DBType = "PostgreSQL"; // Oracle
	public static String TimezoneType = "UTC"; // IST

//	public static String DBType = "Oracle"; //PostgreSQL
//	public static String TimezoneType  = "IST"; // UTC

	public static String OracleDB = "Oracle";
	public static String PostgreDB = "PostgreSQL";

	public UtilityCommon() {
		// TODO Auto-generated constructor stub
	}

	public static String StringFromStringObject(Object obj) {
		String Value = "";
		if (obj != null) {
			Value = (String) obj;
		}
		return Value;
	}

	public static String StringFromBigDecimalObject(Object obj) {
		String Value = "";
		if (obj != null) {
			Value = ((java.math.BigDecimal) obj).toString();

		}
		return Value;
	}

	public static String StringFromBigIntegerObject(Object obj) {
		String Value = "";
		if (obj != null) {
			Value = ((java.math.BigInteger) obj).toString();

		}
		return Value;
	}

	public static String StringFromBigDecimalOrIntegerObject(Object obj) {
		String Value = "";
		if (obj != null) {
			if (DBType.equals(OracleDB)) {
				Value = ((java.math.BigDecimal) obj).toString();
			} else if (DBType.equals(PostgreDB)) {
				Value = ((java.math.BigInteger) obj).toString();
			}

		}
		return Value;
	}

	public static Double DoubleFromBigDecimalOrIntegerObject(Object obj) {
		Double Value = 0.0;
		if (obj != null) {
			if (DBType.equals(OracleDB)) {
				Value = ((java.math.BigDecimal) obj).doubleValue();
			} else if (DBType.equals(PostgreDB)) {
				Value = ((java.math.BigInteger) obj).doubleValue();
			}

		}
		return Value;
	}

	public static Double DoubleFromBigDecimalObject(Object obj) {
		Double Value = 0.0;
		if (obj != null) {
			Value = ((java.math.BigDecimal) obj).doubleValue();
		}
		return Value;
	}

	public static Double DoubleFromBigIntegerObject(Object obj) {
		Double Value = 0.0;
		if (obj != null) {
			Value = ((java.math.BigInteger) obj).doubleValue();
		}
		return Value;
	}

	public static Integer IntegerFromBigDecimalOrIntegerObject(Object obj) {
		Integer Value = 0;
		if (obj != null) {
			if (DBType.equals(OracleDB)) {
				Value = ((java.math.BigDecimal) obj).intValue();
			} else if (DBType.equals(PostgreDB)) {
				Value = ((java.math.BigInteger) obj).intValue();
			}

		}
		return Value;
	}

	public static Integer IntegerFromBigDecimalObject(Object obj) {
		Integer Value = 0;
		if (obj != null) {
			Value = ((java.math.BigDecimal) obj).intValue();
		}
		return Value;
	}

	public static Integer IntegerFromBigIntegerObject(Object obj) {
		Integer Value = 0;
		if (obj != null) {
			Value = ((java.math.BigInteger) obj).intValue();
		}
		return Value;
	}

	public static Integer IntegerFromShortObject(Object obj) {
		Integer Value = 0;
		if (obj != null) {
			if (DBType.equals(OracleDB)) {
				Value = ((java.math.BigDecimal) obj).intValue();
			} else if (DBType.equals(PostgreDB)) {
				Value = ((java.lang.Short) obj).intValue();
			}

		}
		return Value;
	}

	public static Date DateFromDateObject(Object obj) {
		Date Value = null;
		if (obj != null) {
			Value = ((Date) obj);
		}
		return Value;
	}

}
