package com.mestech.eampm.utility;

import java.util.List;

import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.MparameterIntegratedStandards;

public class MasterUploadRequest {

	private List<EquipmentRequest> equipment;

	private List<MparameterIntegratedStandards> integratedStandards;

	public List<EquipmentRequest> getEquipment() {
		return equipment;
	}

	public void setEquipment(List<EquipmentRequest> equipment) {
		this.equipment = equipment;
	}

	public List<MparameterIntegratedStandards> getIntegratedStandards() {
		return integratedStandards;
	}

	public void setIntegratedStandards(List<MparameterIntegratedStandards> integratedStandards) {
		this.integratedStandards = integratedStandards;
	}

}
