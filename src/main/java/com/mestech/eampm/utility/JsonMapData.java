package com.mestech.eampm.utility;

public class JsonMapData {

	private String content;

	private String latitude;

	private String longitude;

	private String title;

	private String icon;

	private Integer index;

	private String siteRef;

	public JsonMapData() {
		super();
	}

	public JsonMapData(String content, String latitude, String longitude, String title, String icon, Integer index,
			String siteRef) {
		super();
		this.content = content;
		this.latitude = latitude;
		this.longitude = longitude;
		this.title = title;
		this.icon = icon;
		this.index = index;
		this.siteRef = siteRef;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getSiteRef() {
		return siteRef;
	}

	public void setSiteRef(String siteRef) {
		this.siteRef = siteRef;
	}
}
