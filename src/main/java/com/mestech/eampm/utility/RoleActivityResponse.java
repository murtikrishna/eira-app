
/******************************************************
 * 
 *    	Filename	: RoleActivityResponse.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Class is used to handle Role activity.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

public class RoleActivityResponse {

	private String accessScreens;

	private String roleName;

	public RoleActivityResponse() {
		super();
	}

	public RoleActivityResponse(String accessScreens, String roleName) {
		super();
		this.accessScreens = accessScreens;
		this.roleName = roleName;
	}

	public String getAccessScreens() {
		return accessScreens;
	}

	public void setAccessScreens(String accessScreens) {
		this.accessScreens = accessScreens;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
