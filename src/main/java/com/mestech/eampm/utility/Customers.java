
/******************************************************
 * 
 *    	Filename	: Customers.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Customers data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

import java.util.List;

public class Customers {

	private Integer customerId;

	private String customerName;

	private String siteNames;

	private List<SiteDetails> siteDetails;

	public String getSiteNames() {
		return siteNames;
	}

	public void setSiteNames(String siteNames) {
		this.siteNames = siteNames;
	}

	public Customers(Integer customerId, String customerName, List<SiteDetails> siteDetails) {
		super();
		this.customerId = customerId;
		this.customerName = customerName;
		this.siteDetails = siteDetails;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public List<SiteDetails> getSiteDetails() {
		return siteDetails;
	}

	public void setSiteDetails(List<SiteDetails> siteDetails) {
		this.siteDetails = siteDetails;
	}

}
