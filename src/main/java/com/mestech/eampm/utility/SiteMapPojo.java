
/******************************************************
 * 
 *    	Filename	: SiteMapPojo.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Class is used to handle Site Map data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

import java.util.List;

public class SiteMapPojo {

	private List<Integer> userId;

	private Integer customerId, activeFlag;

	private List<Integer> siteId;

	public List<Integer> getUserId() {
		return userId;
	}

	public void setUserId(List<Integer> userId) {
		this.userId = userId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public List<Integer> getSiteId() {
		return siteId;
	}

	public void setSiteId(List<Integer> siteId) {
		this.siteId = siteId;
	}

	public Integer getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		this.activeFlag = activeFlag;
	}

}
