
/******************************************************
 * 
 *    	Filename	: OperationMode.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Enum is used to handle operation mode data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

public enum OperationMode {

	FTP("FTP"), API("API");

	public String type;

	OperationMode(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
