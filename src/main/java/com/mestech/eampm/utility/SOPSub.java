
/******************************************************
 * 
 *    	Filename	: SOPRequest.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Class is used to handle SOP data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

public class SOPSub {
	private Integer steps, option1, option2, option3, option4;

	private String workInstruction, workInstructionInfo, remarks;

	public Integer getSteps() {
		return steps;
	}

	public void setSteps(Integer steps) {
		this.steps = steps;
	}

	public Integer getOption1() {
		return option1;
	}

	public void setOption1(Integer option1) {
		this.option1 = option1;
	}

	public Integer getOption2() {
		return option2;
	}

	public void setOption2(Integer option2) {
		this.option2 = option2;
	}

	public Integer getOption3() {
		return option3;
	}

	public void setOption3(Integer option3) {
		this.option3 = option3;
	}

	public Integer getOption4() {
		return option4;
	}

	public void setOption4(Integer option4) {
		this.option4 = option4;
	}

	public String getWorkInstruction() {
		return workInstruction;
	}

	public void setWorkInstruction(String workInstruction) {
		this.workInstruction = workInstruction;
	}

	public String getWorkInstructionInfo() {
		return workInstructionInfo;
	}

	public void setWorkInstructionInfo(String workInstructionInfo) {
		this.workInstructionInfo = workInstructionInfo;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
