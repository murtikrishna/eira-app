
/******************************************************
 * 
 *    	Filename	: SiteDetails.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Class is used to handle site details.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

public class SiteDetails {

	private Integer siteId;

	private String siteName;

	public SiteDetails() {
		super();
	}

	public SiteDetails(Integer siteId, String siteName) {
		super();
		this.siteId = siteId;
		this.siteName = siteName;
	}

	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

}
