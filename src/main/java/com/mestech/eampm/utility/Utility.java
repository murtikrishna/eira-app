
/******************************************************
 * 
 *    	Filename	: Utility.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Class is used to handle dropdown values.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Utility {

	public static Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

}
