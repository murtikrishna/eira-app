
/******************************************************
 * 
 *    	Filename	: SOPRequest.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Class is used to handle Sop request data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

import java.util.List;

public class SOPRequest {

	private String type, category;

	private List<SOPSub> sopSubs;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<SOPSub> getSopSubs() {
		return sopSubs;
	}

	public void setSopSubs(List<SOPSub> sopSubs) {
		this.sopSubs = sopSubs;
	}

}
