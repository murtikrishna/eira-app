
/******************************************************
 * 
 *    	Filename	: SiteMapResponse.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Class is used to handle Site Map Response.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

import java.util.List;

public class SiteMapResponse {

	public Integer userId;

	public String userName;

	private List<Customers> customers;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<Customers> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customers> customers) {
		this.customers = customers;
	}

}
