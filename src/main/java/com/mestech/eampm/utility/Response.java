
/******************************************************
 * 
 *    	Filename	: Response.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Class is used to handle Responses.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.utility;

public class Response {

	private String status;

	private Object data;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
