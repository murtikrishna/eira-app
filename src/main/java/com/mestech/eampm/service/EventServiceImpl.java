package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.EventDAO;
import com.mestech.eampm.model.Event;

/******************************************************
 * 
 * Filename : EventServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from eventservice and its access the
 * 
 * information about eventdao.
 * 
 * 
 *******************************************************/
@Service

public class EventServiceImpl implements EventService {

	@Autowired
	private EventDAO eventDAO;

	/********************************************************************************************
	 * 
	 * Function Name :seteventDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the event dao .
	 * 
	 * Input Params :eventDAO.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void seteventDAO(EventDAO eventDAO) {
		this.eventDAO = eventDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addEvent
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the event.
	 * 
	 * Input Params :event.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addEvent(Event event) {
		eventDAO.addEvent(event);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateEvent
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the event.
	 * 
	 * Input Params :event.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateEvent(Event event) {
		eventDAO.updateEvent(event);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listEvents
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the events.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<Event>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<Event> listEvents() {
		return this.eventDAO.listEvents();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEventById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the events by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value :Event.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public Event getEventById(int id) {
		return eventDAO.getEventById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeEvent
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove events by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeEvent(int id) {
		eventDAO.removeEvent(id);
	}

}
