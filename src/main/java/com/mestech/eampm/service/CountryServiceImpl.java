package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.CountryDAO;
import com.mestech.eampm.model.Country;

/******************************************************
 * 
 * Filename : CountryServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description : this class is implements from countryservice and access
 * 
 * theinformation about countrydao.
 * 
 * 
 *******************************************************/

@Service

public class CountryServiceImpl implements CountryService {

	@Autowired
	private CountryDAO countryDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setcountryDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the value countrydao.
	 * 
	 * Input Params : countryDAO.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	public void setcountryDAO(CountryDAO countryDAO) {
		this.countryDAO = countryDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addCountry
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save country.
	 * 
	 * Input Params : country
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void addCountry(Country country) {
		countryDAO.addCountry(country);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateCountry
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update country.
	 * 
	 * Input Params : country
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void updateCountry(Country country) {
		countryDAO.updateCountry(country);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listCountries
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the countries.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<Country>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Country> listCountries() {
		return this.countryDAO.listCountries();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getCountryById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the country by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : Country
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public Country getCountryById(int id) {
		return countryDAO.getCountryById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeCountry
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the country by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeCountry(int id) {
		countryDAO.removeCountry(id);
	}

}
