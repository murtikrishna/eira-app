package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.bean.OpenStateTicketsCount;
import com.mestech.eampm.dao.EventDetailDAO;
import com.mestech.eampm.model.EventDetail;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.TicketTransaction;

/******************************************************
 * 
 * Filename :EventDetailServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from eventdetailsservice and its access
 * 
 * the information about eventdetaildao.
 * 
 * 
 *******************************************************/
@Service

public class EventDetailServiceImpl implements EventDetailService {

	@Autowired
	private EventDetailDAO eventDetailDAO;

	/********************************************************************************************
	 * 
	 * Function Name :seteventDetailDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the event detail dao .
	 * 
	 * Input Params :eventDetailDAO.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void seteventDetailDAO(EventDetailDAO eventDetailDAO) {
		this.eventDetailDAO = eventDetailDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addEventDetail
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the event detail.
	 * 
	 * Input Params :eventDetail.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addEventDetail(EventDetail eventDetail) {
		eventDetailDAO.addEventDetail(eventDetail);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateTicketDetail
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the ticket detail.
	 * 
	 * Input Params :eventDetail.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateTicketDetail(EventDetail eventDetail) {
		eventDetailDAO.updateEventDetail(eventDetail);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listEventDetails
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the event detail.
	 * 
	 * Input Params :TimezoneOffset.
	 * 
	 * Return Value :List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<EventDetail> listEventDetails(String TimezoneOffset) {
		return this.eventDetailDAO.listEventDetails(TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEventDetailByMax
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get event detail.
	 * 
	 * Input Params :MaxColumnName,TimezoneOffset.
	 * 
	 * Return Value : EventDetail
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public EventDetail getEventDetailByMax(String MaxColumnName, String TimezoneOffset) {
		return eventDetailDAO.getEventDetailByMax(MaxColumnName, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEventDetailListByErrorId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get event detail by using error id.
	 * 
	 * Input Params :errorId,TimezoneOffset.
	 * 
	 * Return Value : List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<EventDetail> getEventDetailListByErrorId(int errorId, String TimezoneOffset) {
		return this.eventDetailDAO.getEventDetailListByErrorId(errorId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEventDetailById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get event detail by using id.
	 * 
	 * Input Params :id,TimezoneOffset.
	 * 
	 * Return Value :EventDetail
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public EventDetail getEventDetailById(int id, String TimezoneOffset) {
		return eventDetailDAO.getEventDetailById(id, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEventDetailListByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all event detail by using user id.
	 * 
	 * Input Params :userId,TimezoneOffset.
	 * 
	 * Return Value : List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<EventDetail> getEventDetailListByUserId(int userId, String TimezoneOffset) {
		return this.eventDetailDAO.getEventDetailListByUserId(userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTodayEventDetailListByCustomerId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all today event detail by using
	 * customer id.
	 * 
	 * Input Params :customerId,TimezoneOffset.
	 * 
	 * Return Value : List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<EventDetail> getTodayEventDetailListByCustomerId(int customerId, String TimezoneOffset) {
		return this.eventDetailDAO.getTodayEventDetailListByCustomerId(customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTodayEventDetailListByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all today event detail by using
	 * user id.
	 * 
	 * Input Params :userId,TimezoneOffset.
	 * 
	 * Return Value : List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<EventDetail> getTodayEventDetailListByUserId(int userId, String TimezoneOffset) {
		return this.eventDetailDAO.getTodayEventDetailListByUserId(userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEventDetailListBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all event detail by using site id.
	 * 
	 * Input Params :siteId,TimezoneOffset.
	 * 
	 * Return Value : List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<EventDetail> getEventDetailListBySiteId(int siteId, String TimezoneOffset) {
		return this.eventDetailDAO.getEventDetailListBySiteId(siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTodayEventDetailListBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all event detail by using site id.
	 * 
	 * Input Params :siteId,TimezoneOffset.
	 * 
	 * Return Value : List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<EventDetail> getTodayEventDetailListBySiteId(int siteId, String TimezoneOffset) {
		return this.eventDetailDAO.getTodayEventDetailListBySiteId(siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEventDetailListByCustomerId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all event detail by using customer
	 * id.
	 * 
	 * Input Params :customerId,TimezoneOffset.
	 * 
	 * Return Value : List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<EventDetail> getEventDetailListByCustomerId(int customerId, String TimezoneOffset) {
		return this.eventDetailDAO.getEventDetailListByCustomerId(customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : removeEventDetail
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the event detail by using id.
	 * 
	 * Input Params :id.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeEventDetail(int id) {
		eventDetailDAO.removeEventDetail(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name : updateEventDetail
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the event detail by using event
	 * detail.
	 * 
	 * Input Params :eventdetail.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	public void updateEventDetail(EventDetail eventdetail) {
		// TODO Auto-generated method stub

	}

	/********************************************************************************************
	 * 
	 * Function Name : listEventDetailsForDisplay
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the event detail for display.
	 * 
	 * Input Params :userid,siteId,fromDate, toDate,eventcode,errorid,equipmentid,
	 * priority, severity,TimezoneOffset
	 * 
	 * Return Value : List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<EventDetail> listEventDetailsForDisplay(int userid, String siteId, String fromDate, String toDate,
			int eventcode, int errorid, String equipmentid, String priority, int severity, String TimezoneOffset) {
		return this.eventDetailDAO.listEventDetailsForDisplay(userid, siteId, fromDate, toDate, eventcode, errorid,
				equipmentid, priority, severity, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listCustomerEventDetailsForDisplay
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the customer event details for
	 * display.
	 * 
	 * Input Params :userid,siteId,fromDate, toDate,eventcode,errorid,equipmentid,
	 * priority, severity,TimezoneOffset
	 * 
	 * Return Value : List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<EventDetail> listCustomerEventDetailsForDisplay(int customerid, String siteId, String fromDate,
			String toDate, int eventcode, int errorid, String equipmentid, String priority, int severity,
			String TimezoneOffset) {
		return this.eventDetailDAO.listCustomerEventDetailsForDisplay(customerid, siteId, fromDate, toDate, eventcode,
				errorid, equipmentid, priority, severity, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :EventsCountByErrorMessage
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all events count error message by
	 * using userid.
	 * 
	 * Input Params :userId,TimezoneOffset
	 * 
	 * Return Value : List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<OpenStateTicketsCount> EventsCountByErrorMessage(int userId, String TimezoneOffset) {

		return this.eventDetailDAO.EventsCountByErrorMessage(userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEventDetailListByUserIdLimit
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all events by using userid.
	 * 
	 * Input Params :userId,TimezoneOffset.
	 * 
	 * Return Value : List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<EventDetail> getEventDetailListByUserIdLimit(int userId, String TimezoneOffset) {

		return this.eventDetailDAO.getEventDetailListByUserIdLimit(userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEventDetailBasedOnDays
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all event detail based on days by
	 * using user id.
	 * 
	 * Input Params :userId,TimezoneOffset.
	 * 
	 * Return Value : List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> getEventDetailBasedOnDays(int userId, String TimezoneOffset) {
		return this.eventDetailDAO.getEventDetailBasedOnDays(userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEventDetailBasedOnSite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all event detail based on site by
	 * using site id.
	 * 
	 * Input Params :siteId, TimezoneOffset.
	 * 
	 * Return Value :List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<EventDetail> getEventDetailBasedOnSite(String siteId, String TimezoneOffset) {
		return this.eventDetailDAO.getEventDetailBasedOnSite(siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEventDetailListBySiteIdLimit
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all event detail list by site id
	 * limits by using site id.
	 * 
	 * Input Params :siteId, TimezoneOffset.
	 * 
	 * Return Value :List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<EventDetail> getEventDetailListBySiteIdLimit(int siteId, String TimezoneOffset) {
		return this.eventDetailDAO.getEventDetailListBySiteIdLimit(siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEventDetailBasedOnDaysBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all event detail based on days by
	 * using site id.
	 * 
	 * Input Params :siteId, TimezoneOffset.
	 * 
	 * Return Value : List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> getEventDetailBasedOnDaysBySite(int siteId, String TimezoneOffset) {
		return this.eventDetailDAO.getEventDetailBasedOnDaysBySite(siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :EventsCountByErrorMessageBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all events count by error message
	 * by using site id.
	 * 
	 * Input Params :siteId, TimezoneOffset.
	 * 
	 * Return Value : List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<OpenStateTicketsCount> EventsCountByErrorMessageBySite(int siteId, String TimezoneOffset) {
		return this.eventDetailDAO.EventsCountByErrorMessageBySite(siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getEventDetailListByCustomerIdLimit
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all events count by customer id
	 * limits by using customer id.
	 * 
	 * Input Params :customerId, TimezoneOffset.
	 * 
	 * Return Value : List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<EventDetail> getEventDetailListByCustomerIdLimit(int customerId, String TimezoneOffset) {
		return this.eventDetailDAO.getEventDetailListByCustomerIdLimit(customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEventDetailBasedOnDaysByCustomerId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all event detail based on days by
	 * using customer id.
	 * 
	 * Input Params :customerId, TimezoneOffset.
	 * 
	 * Return Value : List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> getEventDetailBasedOnDaysByCustomerId(int customerId, String TimezoneOffset) {
		return this.eventDetailDAO.getEventDetailBasedOnDaysByCustomerId(customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :EventsCountByErrorMessageByCustomerId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all events count by error message
	 * by using customer id.
	 * 
	 * Input Params :customerId, TimezoneOffset.
	 * 
	 * Return Value : List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<OpenStateTicketsCount> EventsCountByErrorMessageByCustomerId(int customerId, String TimezoneOffset) {
		return this.eventDetailDAO.EventsCountByErrorMessageByCustomerId(customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :ClickByErrorMessage
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all click by error message by
	 * using user id.
	 * 
	 * Input Params :ErrorMessage,userId,TimezoneOffset.
	 * 
	 * Return Value : List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<EventDetail> ClickByErrorMessage(String ErrorMessage, int userId, String TimezoneOffset) {
		return this.eventDetailDAO.ClickByErrorMessage(ErrorMessage, userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : ClickByErrorMessageBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all click by error message by
	 * using site id.
	 * 
	 * Input Params :ErrorMessage,siteId,TimezoneOffset.
	 * 
	 * Return Value : List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<EventDetail> ClickByErrorMessageBySite(String ErrorMessage, int siteId, String TimezoneOffset) {
		return this.eventDetailDAO.ClickByErrorMessageBySite(ErrorMessage, siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : ClickByErrorMessageByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all click by error message by
	 * using customer id.
	 * 
	 * Input Params :ErrorMessage,customerId,TimezoneOffset.
	 * 
	 * Return Value : List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<EventDetail> ClickByErrorMessageByCustomer(String ErrorMessage, int customerId, String TimezoneOffset) {
		return this.eventDetailDAO.ClickByErrorMessageByCustomer(ErrorMessage, customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : findByEquipmentId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all event detail by using
	 * equipment id.
	 * 
	 * Input Params :equipmentId, TimezoneOffset.
	 * 
	 * Return Value : List<EventDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<EventDetail> findByEquipmentId(int equipmentId, String TimezoneOffset) {
		return this.eventDetailDAO.findByEquipmentId(equipmentId, TimezoneOffset);
	}
}
