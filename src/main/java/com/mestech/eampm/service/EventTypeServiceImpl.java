
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.EventTypeDAO;
import com.mestech.eampm.model.EventType;

/******************************************************
 * 
 * Filename : EventTypeServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from eventtypeservice and its access
 * 
 * the information about eventtypeservice.
 * 
 * 
 *******************************************************/
@Service

public class EventTypeServiceImpl implements EventTypeService {

	@Autowired
	private EventTypeDAO eventtypeDAO;

	/********************************************************************************************
	 * 
	 * Function Name :seteventtypeDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the event type dao .
	 * 
	 * Input Params :eventtypeDAO.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void seteventtypeDAO(EventTypeDAO eventtypeDAO) {
		this.eventtypeDAO = eventtypeDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addEventType
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the event type.
	 * 
	 * Input Params :eventtype.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addEventType(EventType eventtype) {
		eventtypeDAO.addEventType(eventtype);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateEventType
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the event type.
	 * 
	 * Input Params :eventtype.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateEventType(EventType eventtype) {
		eventtypeDAO.updateEventType(eventtype);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listEventTypes
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the event types.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<EventType>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<EventType> listEventTypes() {
		return this.eventtypeDAO.listEventTypes();
	}

	/********************************************************************************************
	 * 
	 * Function Name : getEventTypeById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get event type by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : EventType
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public EventType getEventTypeById(int id) {
		return eventtypeDAO.getEventTypeById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeEventType
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove event type by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeEventType(int id) {
		eventtypeDAO.removeEventType(id);
	}

}