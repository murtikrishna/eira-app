
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.bean.EnergyPerformanceBean;
import com.mestech.eampm.bean.EquipmentWithParameterListBean;
import com.mestech.eampm.bean.EquipmentWithoutParameterListBean;
import com.mestech.eampm.bean.GraphDataBean;
import com.mestech.eampm.dao.DataTransactionDAO;
import com.mestech.eampm.model.DataTransaction;

/******************************************************
 * 
 * Filename :DataTransactionServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from datatransectionservice and its
 * 
 * access the information about datatransectiondao.
 * 
 * 
 *******************************************************/
@Service
public class DataTransactionServiceImpl implements DataTransactionService {

	@Autowired
	private DataTransactionDAO dataTransactionDAO;

	/********************************************************************************************
	 * 
	 * Function Name : setdataTransactionDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set datatransection dao.
	 * 
	 * Input Params : dataTransactionDAO
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setdataTransactionDAO(DataTransactionDAO dataTransactionDAO) {
		this.dataTransactionDAO = dataTransactionDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name : addDataTransaction
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save data transection.
	 * 
	 * Input Params : dataTransaction
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void addDataTransaction(DataTransaction dataTransaction) {
		dataTransactionDAO.addDataTransaction(dataTransaction);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateDataTransaction
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the datatransection.
	 * 
	 * Input Params : dataTransaction
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void updateDataTransaction(DataTransaction dataTransaction) {
		dataTransactionDAO.updateDataTransaction(dataTransaction);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listDataTransactions
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the datatransection.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<DataTransaction>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<DataTransaction> listDataTransactions() {
		return this.dataTransactionDAO.listDataTransactions();
	}

	/********************************************************************************************
	 * 
	 * Function Name :listDataTransactionsForDataDownload
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data transection.
	 * 
	 * Input Params : siteId,fromDate,toDate, equipmentId,TimezoneOffset.
	 * 
	 * Return Value : List<DataTransaction>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<DataTransaction> listDataTransactionsForDataDownload(int siteId, String fromDate, String toDate,
			int equipmentId, String TimezoneOffset) {

		return this.dataTransactionDAO.listDataTransactionsForDataDownload(siteId, fromDate, toDate, equipmentId,
				TimezoneOffset);

	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataTransactionListByEquipmentIdWithDate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data transection by using
	 * eqiupment id with date.
	 * 
	 * Input Params : equipmentId, FromDate,ToDate,TimezoneOffset.
	 * 
	 * Return Value : List<DataTransaction>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataTransaction> getDataTransactionListByEquipmentIdWithDate(int equipmentId, String FromDate,
			String ToDate, String TimezoneOffset) {

		return this.dataTransactionDAO.getDataTransactionListByEquipmentIdWithDate(equipmentId, FromDate, ToDate,
				TimezoneOffset);

	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataTransactionListByEquipmentIdWithDate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data transection by using
	 * equipmentid with date.
	 * 
	 * Input Params : equipmentIds, FromDate,ToDate,TimezoneOffset.
	 * 
	 * Return Value : List<GraphDataBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<GraphDataBean> getDataTransactionListByEquipmentIdsWithDate(String equipmentIds, String FromDate,
			String ToDate, String TimezoneOffset) {

		return this.dataTransactionDAO.getDataTransactionListByEquipmentIdsWithDate(equipmentIds, FromDate, ToDate,
				TimezoneOffset);

	}

	/********************************************************************************************
	 * 
	 * Function Name :getParameterValuesByEquipmentIdsWithDate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get parameter values by using eqiupment
	 * ids with date.
	 * 
	 * Input Params : equipmentIds, Parameter,FromDate,ToDate,TimezoneOffset.
	 * 
	 * Return Value : List<GraphDataBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<GraphDataBean> getParameterValuesByEquipmentIdsWithDate(String equipmentIds, String Parameter,
			String FromDate, String ToDate, String TimezoneOffset) {

		return this.dataTransactionDAO.getParameterValuesByEquipmentIdsWithDate(equipmentIds, Parameter, FromDate,
				ToDate, TimezoneOffset);

	}

	/********************************************************************************************
	 * 
	 * Function Name : getParameterValuesBySiteIdsWithDate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data.
	 * 
	 * Input Params : siteIds,Parameter, FromDate, ToDate,TimezoneOffset.
	 * 
	 * Return Value : List<GraphDataBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<GraphDataBean> getParameterValuesBySiteIdsWithDate(String siteIds, String Parameter, String FromDate,
			String ToDate, String TimezoneOffset) {

		return this.dataTransactionDAO.getParameterValuesBySiteIdsWithDate(siteIds, Parameter, FromDate, ToDate,
				TimezoneOffset);

	}

	/********************************************************************************************
	 * 
	 * Function Name :getIrradiationValuesBySitesWithDate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data.
	 * 
	 * Input Params : siteId, FromDate, ToDate,TimezoneOffset.
	 * 
	 * Return Value : List<GraphDataBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<GraphDataBean> getIrradiationValuesBySitesWithDate(String siteId, String FromDate, String ToDate,
			String TimezoneOffset) {
		return this.dataTransactionDAO.getIrradiationValuesBySitesWithDate(siteId, FromDate, ToDate, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDaywiseIrradiationValuesBySitesWithDate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data.
	 * 
	 * Input Params : siteId, FromDate, ToDate,TimezoneOffset.
	 * 
	 * Return Value : List<GraphDataBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<GraphDataBean> getDaywiseIrradiationValuesBySitesWithDate(String siteId, String FromDate, String ToDate,
			String TimezoneOffset) {
		return this.dataTransactionDAO.getDaywiseIrradiationValuesBySitesWithDate(siteId, FromDate, ToDate,
				TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getEnergyDataByEquipmentIdsWithDate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data.
	 * 
	 * Input Params : equipmentIds, FromDate, ToDate,TimezoneOffset.
	 * 
	 * Return Value : List<EnergyPerformanceBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<EnergyPerformanceBean> getEnergyDataByEquipmentIdsWithDate(String equipmentIds, String FromDate,
			String ToDate, String TimezoneOffset) {
		return this.dataTransactionDAO.getEnergyDataByEquipmentIdsWithDate(equipmentIds, FromDate, ToDate,
				TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDataTransactionListByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data.
	 * 
	 * Input Params : userId,TimezoneOffset.
	 * 
	 * Return Value : List<DataTransaction>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataTransaction> getDataTransactionListByUserId(int userId, String TimezoneOffset) {
		return this.dataTransactionDAO.getDataTransactionListByUserId(userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDataTransactionListByCustomerId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data.
	 * 
	 * Input Params : customerId,TimezoneOffset.
	 * 
	 * Return Value : List<DataTransaction>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataTransaction> getDataTransactionListByCustomerId(int customerId, String TimezoneOffset) {
		return this.dataTransactionDAO.getDataTransactionListByCustomerId(customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataTransactionListBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data.
	 * 
	 * Input Params : siteId,TimezoneOffset.
	 * 
	 * Return Value : List<DataTransaction>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataTransaction> getDataTransactionListBySiteId(int siteId, String TimezoneOffset) {
		return this.dataTransactionDAO.getDataTransactionListBySiteId(siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataTransactionListByEquipmentId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data.
	 * 
	 * Input Params : equipmentId,TimezoneOffset.
	 * 
	 * Return Value : List<DataTransaction>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataTransaction> getDataTransactionListByEquipmentId(int equipmentId, String TimezoneOffset) {
		return this.dataTransactionDAO.getDataTransactionListByEquipmentId(equipmentId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataTransactionById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data.
	 * 
	 * Input Params : id.
	 * 
	 * Return Value : DataTransaction
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public DataTransaction getDataTransactionById(int id) {
		return dataTransactionDAO.getDataTransactionById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDataTransactionByMax
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data.
	 * 
	 * Input Params : MaxColumnName
	 * 
	 * Return Value : DataTransaction
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public DataTransaction getDataTransactionByMax(String MaxColumnName) {
		return dataTransactionDAO.getDataTransactionByMax(MaxColumnName);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeDataTransaction
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeDataTransaction(int id) {
		dataTransactionDAO.removeDataTransaction(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEnergyDataBySiteIdsWithDate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data.
	 * 
	 * Input Params : siteIds, FromDate, ToDate,TimezoneOffset.
	 * 
	 * Return Value : List<EnergyPerformanceBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<EnergyPerformanceBean> getEnergyDataBySiteIdsWithDate(String siteIds, String FromDate, String ToDate,
			String TimezoneOffset) {
		return dataTransactionDAO.getEnergyDataBySiteIdsWithDate(siteIds, FromDate, ToDate, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getYesterdayEnergyByEquipmentIdsWithDate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get yesterday energy by using
	 * equipmentid.
	 * 
	 * Input Params : equipmentIds, FromDate, ToDate,TimezoneOffset.
	 * 
	 * Return Value :String
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public String getYesterdayEnergyByEquipmentIdsWithDate(String equipmentIds, String FromDate, String ToDate,
			String TimezoneOffset) {
		return dataTransactionDAO.getYesterdayEnergyByEquipmentIdsWithDate(equipmentIds, FromDate, ToDate,
				TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getYesterdayEnergyBySiteIdsWithDate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get yesterday energy by using siteid.
	 * 
	 * Input Params : siteId, FromDate, ToDate,TimezoneOffset.
	 * 
	 * Return Value :String
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public String getYesterdayEnergyBySiteIdsWithDate(String siteId, String FromDate, String ToDate,
			String TimezoneOffset) {
		return dataTransactionDAO.getYesterdayEnergyBySiteIdsWithDate(siteId, FromDate, ToDate, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTodayFlagStatus
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get today flag status.
	 * 
	 * Input Params : equipmentIds,TimezoneOffset.
	 * 
	 * Return Value :String
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public String getTodayFlagStatus(String equipmentIds, String TimezoneOffset) {
		return dataTransactionDAO.getTodayFlagStatus(equipmentIds, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEquipmentList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all equipment.
	 * 
	 * Input Params :siteId.
	 * 
	 * Return Value : List<EquipmentWithoutParameterListBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<EquipmentWithoutParameterListBean> getEquipmentList(String siteId) {
		return dataTransactionDAO.getEquipmentList(siteId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEquipmentWithParameterList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all equipment with parameter.
	 * 
	 * Input Params :siteId.
	 * 
	 * Return Value : List<EquipmentWithParameterListBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<EquipmentWithParameterListBean> getEquipmentWithParameterList(String siteId) {
		return dataTransactionDAO.getEquipmentWithParameterList(siteId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getParameterComparisionDateWithDate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the parameter comparision date.
	 * 
	 * Input Params :equipmentIds, Parameters, FromDate, ToDate,TimezoneOffset.
	 * 
	 * Return Value : String
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public String getParameterComparisionDateWithDate(String equipmentIds, String Parameters, String FromDate,
			String ToDate, String TimezoneOffset) {
		return dataTransactionDAO.getParameterComparisionDateWithDate(equipmentIds, Parameters, FromDate, ToDate,
				TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEquipmentviewForSmb
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the equipment view and its for Smb.
	 * 
	 * Input Params :equipmentIds, FromDate, ToDate, TimezoneOffset.
	 * 
	 * Return Value :List<GraphDataBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<GraphDataBean> getEquipmentviewForSmb(String equipmentIds, String FromDate, String ToDate,
			String TimezoneOffset) {
		return dataTransactionDAO.getEquipmentviewForSmb(equipmentIds, FromDate, ToDate, TimezoneOffset);

	}

	/********************************************************************************************
	 * 
	 * Function Name : getEquipmentviewForTracker
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the Equipment view and its for
	 * Tracker.
	 * 
	 * Input Params :equipmentIds, FromDate, ToDate, TimezoneOffset.
	 * 
	 * Return Value :List<GraphDataBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<GraphDataBean> getEquipmentviewForTracker(String equipmentIds, String FromDate, String ToDate,
			String TimezoneOffset) {
		return dataTransactionDAO.getEquipmentviewForTracker(equipmentIds, FromDate, ToDate, TimezoneOffset);

	}
}
