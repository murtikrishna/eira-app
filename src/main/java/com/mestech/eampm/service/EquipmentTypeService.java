package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.EquipmentType;

public interface EquipmentTypeService {

	public void addEquipmentType(EquipmentType equipmenttype);
    
    public void updateEquipmentType(EquipmentType equipmenttype);
    
    public EquipmentType getEquipmentTypeById(int id);
    
    public EquipmentType getEquipmentTypeByMax(String MaxColumnName);
    
    public void removeEquipmentType(int id);
    
    public List<EquipmentType> listEquipmentTypes();	
    
    public List<EquipmentType> listallequipments(int equipmentid);
    
    
    public List<EquipmentType> listFilterEquipmentsByTypeId(int typeId);
}
