package com.mestech.eampm.service;


import java.util.List;

import com.mestech.eampm.model.MparameterIntegratedStandards;
import com.mestech.eampm.model.ParameterStandard;

public interface ParameterStandardsService {

	public void addParameterStandards(ParameterStandard parameterstandards);
	
	public void updateParameterStandards(ParameterStandard parameterstandards);
	    
	public ParameterStandard getParameterStandardsById(int id);
	
	    
	public void removeParameterStandards(int id);
	
	 public ParameterStandard getParameterStandardByMax(String MaxColumnName);
		
	    
	public List<ParameterStandard> listParameterStandards();
	
	public List<ParameterStandard> listParameterStandards(int standardid);
	
	public List<MparameterIntegratedStandards> getallintegretedStandards();
	
	public List<String> getallStandardParameterNames();
	
	public void saveParameterIntegratedStandards(MparameterIntegratedStandards integratedStandards);
}
