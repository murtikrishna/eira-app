package com.mestech.eampm.service;

import java.math.BigInteger;
import java.util.List;

import com.mestech.eampm.model.MDatalogger;

public interface MDataloggerService {

	public MDatalogger save(MDatalogger datalogger);

	public MDatalogger update(MDatalogger datalogger);

	public void delete(BigInteger id);

	public MDatalogger edit(BigInteger id);

	public List<MDatalogger> listAll();
}
