package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.BatteryLimits;

public interface BatteryLimitsService {

	public void addBatteryLimits(BatteryLimits batterylimits);
    
	public void updateBatteryLimits(BatteryLimits batterylimits);
	    
	public BatteryLimits getBatteryLimitsById(int id);
	    
	public void removeBatteryLimits(int id);
	    
	public List<BatteryLimits> listBatteryLimits();
    
   
}
