package com.mestech.eampm.service;

import java.math.BigInteger;
import java.util.List;

import com.mestech.eampm.model.MServiceCode;

public interface MServiceCodeService {
	public MServiceCode save(MServiceCode mServiceCode);

	public MServiceCode update(MServiceCode mServiceCode);

	public void delete(Long id);

	public MServiceCode edit(BigInteger id);

	public List<MServiceCode> listAll();

	
}
