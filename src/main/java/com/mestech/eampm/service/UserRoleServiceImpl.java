
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.UserRoleDAO;
import com.mestech.eampm.model.UserRole;

/******************************************************
 * 
 * Filename : UserRoleServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class implements from userroleservice and its access the
 * 
 * information about userroledao.
 * 
 * 
 *******************************************************/
@Service

public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	private UserRoleDAO userroleDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setuserroleDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the user role dao.
	 * 
	 * Input Params : userroleDAO
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setuserroleDAO(UserRoleDAO userroleDAO) {
		this.userroleDAO = userroleDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addUserRole
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the user role.
	 * 
	 * Input Params : userrole
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addUserRole(UserRole userrole) {
		userroleDAO.addUserRole(userrole);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateUserRole
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the user role.
	 * 
	 * Input Params : userrole
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateUserRole(UserRole userrole) {
		userroleDAO.updateUserRole(userrole);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listUserRoles
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the user roles.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<UserRole>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<UserRole> listUserRoles() {
		return this.userroleDAO.listUserRoles();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getUserRoleById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the user role by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : UserRole
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public UserRole getUserRoleById(int id) {
		return userroleDAO.getUserRoleById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeUserRole
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the user role by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeUserRole(int id) {
		userroleDAO.removeUserRole(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listRoles
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the roles by using role id.
	 * 
	 * Input Params : roleid
	 * 
	 * Return Value : List<UserRole>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<UserRole> listRoles(int roleid) {
		return userroleDAO.listRoles(roleid);
	}

}
