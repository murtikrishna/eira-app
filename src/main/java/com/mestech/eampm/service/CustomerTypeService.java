package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.CustomerType;

public interface CustomerTypeService {

	public void addCustomerType(CustomerType customertype);
    
    public void updateCustomerType(CustomerType customertype);
    
    public CustomerType getCustomerTypeById(int id);
    
    public void removeCustomerType(int id);
    
    public List<CustomerType> listCustomerTypes();	
}
