
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.StatusDAO;
import com.mestech.eampm.model.Status;

/******************************************************
 * 
 * Filename :StatusServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from statusservice and its access the
 * 
 * information about statusdao.
 * 
 * 
 *******************************************************/
@Service

public class StatusServiceImpl implements StatusService {

	@Autowired
	private StatusDAO statusDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setstatusDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the status dao.
	 * 
	 * Input Params :statusDAO
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setstatusDAO(StatusDAO statusDAO) {
		this.statusDAO = statusDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addStatus
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the status.
	 * 
	 * Input Params :status
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addStatus(Status status) {
		statusDAO.addStatus(status);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateStatus
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the status.
	 * 
	 * Input Params : status
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateStatus(Status status) {
		statusDAO.updateStatus(status);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listStatuses
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the statuses.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<Status>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<Status> listStatuses() {
		return this.statusDAO.listStatuses();
	}

	/********************************************************************************************
	 * 
	 * Function Name : getStatusById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the status by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value :Status
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public Status getStatusById(Integer id) {
		return statusDAO.getStatusById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeStatus
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the status by using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeStatus(int id) {
		statusDAO.removeStatus(id);
	}

}
