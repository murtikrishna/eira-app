package com.mestech.eampm.service;

import java.util.List;

import com.mestech.eampm.model.HistoricalUpload;

public interface HistoricalUploadService {
	public HistoricalUpload save(HistoricalUpload historicalUpload);

	public List<HistoricalUpload> list();

	public List<HistoricalUpload> getHistory(String siteCode, String directory);
}
