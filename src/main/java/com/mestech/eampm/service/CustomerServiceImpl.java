package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.bean.CustomerListBean;
import com.mestech.eampm.dao.CustomerDAO;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.EquipmentType;

/******************************************************
 * 
 * Filename : CustomerServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description : this class is implements from customerservice and its access
 * 
 * the information about customerdao.
 * 
 * 
 *******************************************************/
@Service

public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDAO customerDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setcustomerDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set customer dao.
	 * 
	 * Input Params : customerDAO
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	public void setcustomerDAO(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addcustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save customer.
	 * 
	 * Input Params : customer
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void addCustomer(Customer customer) {
		customerDAO.addCustomer(customer);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update customer.
	 * 
	 * Input Params : customer
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void updateCustomer(Customer customer) {
		customerDAO.updateCustomer(customer);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listCustomers
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all customer.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<Customer>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Customer> listCustomers() {
		return this.customerDAO.listCustomers();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getCustomerListByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the customer by using user id.
	 * 
	 * Input Params : userId
	 * 
	 * Return Value : List<Customer>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<Customer> getCustomerListByUserId(int userId) {
		return customerDAO.getCustomerListByUserId(userId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getCustomerListById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get customer by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : Customer
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public Customer getCustomerById(int id) {
		return customerDAO.getCustomerById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getCustomerByMax
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get customer by using maximum column
	 * name.
	 * 
	 * Input Params : MaxColumnName
	 * 
	 * Return Value : Customer
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public Customer getCustomerByMax(String MaxColumnName) {
		return customerDAO.getCustomerByMax(MaxColumnName);
	}

	/********************************************************************************************
	 * 
	 * Function Name : removeCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove customer by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeCustomer(int id) {
		customerDAO.removeCustomer(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getAjaxCustomerListByUser
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get ajaxcustomer by using user id.
	 * 
	 * Input Params : UserId
	 * 
	 * Return Value : List<CustomerListBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<CustomerListBean> getAjaxCustomerListByUser(String UserID) {
		return customerDAO.getAjaxCustomerListByUser(UserID);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getAjaxCustomerListByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get ajaxcustomer by using customerid.
	 * 
	 * Input Params : CustomerID
	 * 
	 * Return Value : List<CustomerListBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<CustomerListBean> getAjaxCustomerListByCustomer(String CustomerID) {
		return customerDAO.getAjaxCustomerListByCustomer(CustomerID);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listallcustomers
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the customer by using customer
	 * id.
	 * 
	 * Input Params :customerid
	 * 
	 * Return Value : List<Customer>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Customer> listallcustomers(int customerid) {
		return customerDAO.listallcustomers(customerid);

	}

	/********************************************************************************************
	 * 
	 * Function Name : listConfigCustomers
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the configcustomers.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<Customer>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Customer> listConfigCustomers() {
		return this.customerDAO.listConfigCustomers();
	}

	/********************************************************************************************
	 * 
	 * Function Name : getPrimaryCustomerById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the primarycustomer by using
	 * id
	 * 
	 * Input Params : id
	 * 
	 * Return Value : List<Customer>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Customer> getPrimaryCustomerById(String id) {
		return this.customerDAO.getPrimaryCustomerById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getPrimaryCustomerByMax
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to gat primary customer by using maximum
	 * column name.
	 * 
	 * Input Params : MaxColummnName
	 * 
	 * Return Value : Customer
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public Customer getPrimaryCustomerByMax(String MaxColumnName) {
		return this.customerDAO.getPrimaryCustomerByMax(MaxColumnName);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listprimarycustomers
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the primary customers.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<Customer>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Customer> listprimarycustomers() {
		return this.customerDAO.listprimarycustomers();
	}
}
