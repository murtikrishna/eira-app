

package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.EventType;

public interface EventTypeService {

	public void addEventType(EventType eventtype);
    
    public void updateEventType(EventType eventtype);
    
    public EventType getEventTypeById(int id);
    
    public void removeEventType(int id);
    
    public List<EventType> listEventTypes();	
}



