package com.mestech.eampm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.UserDAO;
import com.mestech.eampm.model.User;
import com.mestech.eampm.model.UserLog;
import com.mestech.eampm.model.UserRole;

/******************************************************
 * 
 * Filename : UserServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description : this class implements from userservice and userdetailsservice
 * then its used for access the information in user
 * dao,userroleservice,userlogservice.
 * 
 * 
 *******************************************************/
@Service("userService")
public class UserServiceImpl implements UserService, UserDetailsService {

	@Autowired
	private UserDAO userDAO;
	@Autowired
	private UserRoleService userRoleService;
	@Autowired
	private UserLogService userlogService;

	/********************************************************************************************
	 * 
	 * Function Name :setuserDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the user dao.
	 * 
	 * Input Params : userDAO
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setuserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addUser
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the user.
	 * 
	 * Input Params : user
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addUser(User user) {
		userDAO.addUser(user);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateUser
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the user.
	 * 
	 * Input Params : user
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateUser(User user) {
		userDAO.updateUser(user);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listUsers
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the users.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<User>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<User> listUsers() {
		return this.userDAO.listUsers();
	}

	/********************************************************************************************
	 * 
	 * Function Name :listFieldUsers
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the list field users.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<User>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<User> listFieldUsers() {
		return this.userDAO.listFieldUsers();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getUserById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the user by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value :User
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public User getUserById(int id) {
		return userDAO.getUserById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getUserByName
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the user by using user name.
	 * 
	 * Input Params : userName
	 * 
	 * Return Value :User
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public User getUserByName(String userName) {
		return userDAO.getUserByName(userName);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getUserByMax
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the user by using maximum column
	 * name.
	 * 
	 * Input Params : MaxColumnName
	 * 
	 * Return Value :User
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public User getUserByMax(String MaxColumnName) {
		return userDAO.getUserByMax(MaxColumnName);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeUser
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the user by using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeUser(int id) {
		userDAO.removeUser(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listAppUsers
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the app users.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<User>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<User> listAppUsers() {
		return this.userDAO.listAppUsers();
	}

	/********************************************************************************************
	 * 
	 * Function Name :listUsers
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the users.
	 * 
	 * Input Params :userid
	 * 
	 * Return Value :List<User>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<User> listUsers(int userid) {
		return this.userDAO.listUsers(userid);

	}

	/********************************************************************************************
	 * 
	 * Function Name :loadUserByUsername
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to load user by using user name.
	 * 
	 * Input Params :username
	 * 
	 * Return Value :UserDetails
	 * 
	 * Exceptions :UsernameNotFoundException.
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		if (null != userName) {
			String credentials[] = userName.split("johnEdwin333");
			if (credentials.length > 0 && credentials != null) {
				User user = getUserByName(credentials[0]);
				if (user != null) {
					if (user.getPassword().equals(credentials[1])) {
						UserLog userlog = new UserLog();
						Date date = new Date();
						userlog.setUserName(user.getUserName());
						userlog.setUserId(user.getUserId());
						userlog.setDlogDate(date);
						userlog.setActiveFlag(1);
						userlog.setSessionId(credentials[2]);
						userlog.setSlogDate(date);
						userlog.setCreationDate(date);
						userlogService.addUserlog(userlog);
						return new org.springframework.security.core.userdetails.User(user.getUserId().toString(),
								user.getPassword(), true, true, true, true, getGrantedAuthorities(user));
					}
				}
			}

		}
		return null;
	}

	/********************************************************************************************
	 * 
	 * Function Name :getGrantedAuthorities
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the granted authorities.
	 * 
	 * Input Params :List<GrantedAuthority>
	 * 
	 * Return Value :user
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	private List<GrantedAuthority> getGrantedAuthorities(User user) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		UserRole userRole = userRoleService.getUserRoleById(user.getRoleID());
		authorities.add(new SimpleGrantedAuthority("ROLE_" + userRole.getRoleName()));
		return authorities;
	}
}
