package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.Activity;

public interface ActivityService {

	public void addActivity(Activity activity);
    
    public void updateActivity(Activity activity);
    
    public Activity getActivityById(int id);
    
    public void removeActivity(int id);
    
    public List<Activity> listActivities();	
    
    public Activity getActivityName(int Id);
    
    public String getname();
}
