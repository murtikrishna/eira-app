package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.CustomerMapDAO;
import com.mestech.eampm.model.CustomerMap;

/******************************************************
 * 
 * Filename : CustomerMapServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description : this class is implements from customermapservice and its
 * 
 * access the information about customermapdao.
 * 
 * 
 *******************************************************/
@Service

public class CustomerMapServiceImpl implements CustomerMapService {

	@Autowired
	private CustomerMapDAO customermapDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setcustomermapDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this method used to set customermapdao.
	 * 
	 * Input Params : customermapDAO
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	public void setcustomermapDAO(CustomerMapDAO customermapDAO) {
		this.customermapDAO = customermapDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addcustomermap
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this method used to save customermap.
	 * 
	 * Input Params : customermapDAO
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void addCustomerMap(CustomerMap customermap) {
		customermapDAO.addCustomerMap(customermap);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateCustomerMap
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this method used to update customermap.
	 * 
	 * Input Params : customermap
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void updateCustomerMap(CustomerMap customermap) {
		customermapDAO.updateCustomerMap(customermap);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listCustomerMaps
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this method used to select all customermaps .
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<CustomerMap>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<CustomerMap> listCustomerMaps() {
		return this.customermapDAO.listCustomerMaps();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getCustomerMapById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this method used to get customermap bu using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : CustomerMap
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public CustomerMap getCustomerMapById(int id) {
		return customermapDAO.getCustomerMapById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeCustomerMap
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this method used to remove customermap by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeCustomerMap(int id) {
		customermapDAO.removeCustomerMap(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listCustomerMaps
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this method used to select all the customermaps.
	 * 
	 * Input Params : mapid
	 * 
	 * Return Value : List<CustomerMap>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<CustomerMap> listCustomerMaps(int mapid) {
		return this.customermapDAO.listCustomerMaps(mapid);
	}
}
