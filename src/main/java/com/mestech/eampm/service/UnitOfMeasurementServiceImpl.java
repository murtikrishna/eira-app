
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.UnitOfMeasurementDAO;
import com.mestech.eampm.model.UnitOfMeasurement;

/******************************************************
 * 
 * Filename :UnitOfMeasurementServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class implements from unit of measurement service and its
 * 
 * access the information about unit of measurementdao.
 * 
 * 
 *******************************************************/
@Service

public class UnitOfMeasurementServiceImpl implements UnitOfMeasurementService {

	@Autowired
	private UnitOfMeasurementDAO unitofmeasurementDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setunitofmeasurementDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the unit of measurement dao.
	 * 
	 * Input Params :unitofmeasurementDAO
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setunitofmeasurementDAO(UnitOfMeasurementDAO unitofmeasurementDAO) {
		this.unitofmeasurementDAO = unitofmeasurementDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addUnitOfMeasurement
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the unit of measurement.
	 * 
	 * Input Params :unitofmeasurement
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addUnitOfMeasurement(UnitOfMeasurement unitofmeasurement) {
		unitofmeasurementDAO.addUnitOfMeasurement(unitofmeasurement);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateUnitOfMeasurement
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the unit of measurement.
	 * 
	 * Input Params :unitofmeasurement
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateUnitOfMeasurement(UnitOfMeasurement unitofmeasurement) {
		unitofmeasurementDAO.updateUnitOfMeasurement(unitofmeasurement);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listUnitOfMeasurements
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the unit of measurement.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<UnitOfMeasurement>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<UnitOfMeasurement> listUnitOfMeasurements() {
		return this.unitofmeasurementDAO.listUnitOfMeasurements();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getUnitOfMeasurementById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the unit of measurement by using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value : UnitOfMeasurement
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public UnitOfMeasurement getUnitOfMeasurementById(int id) {
		return unitofmeasurementDAO.getUnitOfMeasurementById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeUnitOfMeasurement
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the unit of measurement.
	 * 
	 * Input Params :id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeUnitOfMeasurement(int id) {
		unitofmeasurementDAO.removeUnitOfMeasurement(id);
	}

}
