package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.User;

public interface UserService {

	public void addUser(User user);
    
    public void updateUser(User user);
    
    public User getUserById(int id);
    
    public User getUserByName(String userName);
    
    public void removeUser(int id);
    
    public User getUserByMax(String MaxColumnName);
    
    public List<User> listUsers();	
    
	public List<User> listFieldUsers();
	
	public List<User> listAppUsers();

	public List<User> listUsers(int userid);
}
