package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.ActivityDAO;
import com.mestech.eampm.dao.ParameterStandardsDAO;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.MparameterIntegratedStandards;
import com.mestech.eampm.model.ParameterStandard;

/******************************************************
 * 
 * Filename :ParameterStandardsServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from parameterstandardservice and its
 * 
 * access the information about parameterstandarddao.
 * 
 * 
 *******************************************************/
@Service

public class ParameterStandardsServiceImpl implements ParameterStandardsService {

	@Autowired
	private ParameterStandardsDAO parameterstandardsDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setparameterstandardsDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the parameter standards dao.
	 * 
	 * Input Params : parameterstandardsDAO.
	 * 
	 * Return Value : void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setparameterstandardsDAO(ParameterStandardsDAO parameterstandardsDAO) {
		this.parameterstandardsDAO = parameterstandardsDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addParameterStandards
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the parameter standards.
	 * 
	 * Input Params : parameterstandards.
	 * 
	 * Return Value : void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addParameterStandards(ParameterStandard parameterstandards) {
		parameterstandardsDAO.addParameterStandards(parameterstandards);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateParameterStandards
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the parameter standards.
	 * 
	 * Input Params : parameterstandards.
	 * 
	 * Return Value : void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateParameterStandards(ParameterStandard parameterstandards) {
		parameterstandardsDAO.updateParameterStandards(parameterstandards);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listParameterStandards
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to selecct all parameter standards.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<ParameterStandard>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<ParameterStandard> listParameterStandards() {
		return this.parameterstandardsDAO.listParameterStandards();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getParameterStandardsById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get parameter standard by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value :ParameterStandard.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public ParameterStandard getParameterStandardsById(int id) {
		return parameterstandardsDAO.getParameterStandardsById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name : removeParameterStandards
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove parameter standard by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeParameterStandards(int id) {
		parameterstandardsDAO.removeParameterStandards(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getParameterStandardByMax
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the parameter standard by using
	 * maximum column name.
	 * 
	 * Input Params : MaxColumnName
	 * 
	 * Return Value :ParameterStandard.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public ParameterStandard getParameterStandardByMax(String MaxColumnName) {
		return parameterstandardsDAO.getParameterStandardByMax(MaxColumnName);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listParameterStandards
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the parameter standards by
	 * using standard id.
	 * 
	 * Input Params :standardid.
	 * 
	 * Return Value :List<ParameterStandard> .
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<ParameterStandard> listParameterStandards(int standardid) {
		return parameterstandardsDAO.listParameterStandards(standardid);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getallintegretedStandards
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the integrated standards.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<MparameterIntegratedStandards>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<MparameterIntegratedStandards> getallintegretedStandards() {

		return parameterstandardsDAO.getallStandards();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getallStandardParameterNames
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get all standard parameter names.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<String>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<String> getallStandardParameterNames() {

		return parameterstandardsDAO.getallStandardParameterNames();
	}

	/********************************************************************************************
	 * 
	 * Function Name : saveParameterIntegratedStandards
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save parameter integrated standards.
	 * 
	 * Input Params :integratedStandards
	 * 
	 * Return Value : void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public void saveParameterIntegratedStandards(MparameterIntegratedStandards integratedStandards) {
		parameterstandardsDAO.saveParameterIntegratedStandards(integratedStandards);
	}
}
