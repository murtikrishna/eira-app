package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.CountryRegion;

public interface CountryRegionService {

	public void addCountryRegion(CountryRegion countryregion);
    
    public void updateCountryRegion(CountryRegion countryregion);
    
    public CountryRegion getCountryRegionById(int id);
    
    public void removeCountryRegion(int id);
    
    public List<CountryRegion> listCountryRegions();	
}
