package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.SopDetailDAO;
import com.mestech.eampm.model.SopDetail;

/******************************************************
 * 
 * Filename :SopDetailServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class implements from sopdetailservice and its access the
 * 
 * information about sopdetaildao.
 * 
 * 
 *******************************************************/
@Service

public class SopDetailServiceImpl implements SopDetailService {

	@Autowired
	private SopDetailDAO sopdetailDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setsopdetailDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the sop detail dao.
	 * 
	 * Input Params :sopdetailDAO
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setsopdetailDAO(SopDetailDAO sopdetailDAO) {
		this.sopdetailDAO = sopdetailDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name : addSopDetail
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the sop detail dao.
	 * 
	 * Input Params :sopdetail
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addSopDetail(SopDetail sopdetail) {
		sopdetailDAO.addSopDetail(sopdetail);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateSopDetail
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the sopdetail.
	 * 
	 * Input Params :sopdetail
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateSopDetail(SopDetail sopdetail) {
		sopdetailDAO.updateSopDetail(sopdetail);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listSopDetails
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the sop details.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<SopDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<SopDetail> listSopDetails() {
		return this.sopdetailDAO.listSopDetails();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getSopDetailById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get sop details by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value :SopDetail
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public SopDetail getSopDetailById(int id) {
		return sopdetailDAO.getSopDetailById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeSopDetail
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the sop detail by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeSopDetail(int id) {
		sopdetailDAO.removeSopDetail(id);
	}

}
