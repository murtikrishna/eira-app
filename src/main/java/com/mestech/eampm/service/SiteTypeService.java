package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.SiteType;

public interface SiteTypeService {

	public void addSiteType(SiteType sitetype);
    
    public void updateSiteType(SiteType sitetype);
    
    public SiteType getSiteTypeById(int id);
    
    public void removeSiteType(int id);
    
    public List<SiteType> listSiteTypes();	
}

