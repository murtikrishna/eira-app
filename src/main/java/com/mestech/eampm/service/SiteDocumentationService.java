package com.mestech.eampm.service;

import java.util.List;

import com.mestech.eampm.model.SiteDocumentation;

public interface SiteDocumentationService {

	public SiteDocumentation save(SiteDocumentation siteDocumentation);

	public SiteDocumentation update(SiteDocumentation siteDocumentation);

	public void delete(Long id);

	public SiteDocumentation edit(Long id);

	public void deleteDocuments(String siteCode, String docCategory, String docName);

	public List<SiteDocumentation> findBySiteId(int id);

	public List<SiteDocumentation> findBySiteId(int id, String category);

	public List<SiteDocumentation> listAll();
}
