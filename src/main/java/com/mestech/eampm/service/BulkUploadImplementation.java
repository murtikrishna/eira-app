package com.mestech.eampm.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/******************************************************
 * 
 * Filename : BulkUploadImplementation
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description : this class is implements from bulkupload and its access the
 * 
 * information about sessionfactory.
 * 
 * 
 *******************************************************/
@Service
public class BulkUploadImplementation implements BulkUpload {

	@Autowired
	private SessionFactory sessionFactory;

	/********************************************************************************************
	 * 
	 * Function Name :upload
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to upload the elements.
	 * 
	 * Input Params : elements
	 * 
	 * Return Value : <E>boolean
	 * 
	 * Exceptions : exception.
	 * 
	 * 
	 **********************************************************************************************/

	@Override

	public <E> boolean upload(List<E> elements) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			for (E element : elements) {
				session.persist(element);
			}
			transaction.commit();
			return true;
		} catch (Exception exception) {
			transaction.rollback();
			session.clear();
			return false;
		}

	}

}
