package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.User;
import com.mestech.eampm.model.UserLog;

public interface UserLogService {
	public void addUserlog(UserLog userlog);
	
    
	public void updateUserlog(UserLog userlog);
	    
	public UserLog getUserLogById(String sessionId);
	
	public void updateUserlogDetails(String SessionID,String IpAddress);
	 
	public void removeUserlog(int id);
	    
	public List<UserLog> listUserlogs();
	
	public List<UserLog> listFieldUserlogs();
}
