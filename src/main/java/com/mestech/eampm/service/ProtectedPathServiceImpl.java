package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.dao.ProtectedPathDAO;
import com.mestech.eampm.model.ProtectedUrl;

/******************************************************
 * 
 * Filename :ProtectedPathServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class implements from protectedpathservice and its access
 * 
 * the information about protectedpathdao.
 * 
 * 
 *******************************************************/
@Service
public class ProtectedPathServiceImpl implements ProtectedPathService {
	@Autowired
	private ProtectedPathDAO protectedPathDAO;

	/********************************************************************************************
	 * 
	 * Function Name :getAll
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the protected url.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<ProtectedUrl>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public List<ProtectedUrl> getAll() {
		// TODO Auto-generated method stub
		return protectedPathDAO.getAll();
	}

}
