package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.FilterDAO;
import com.mestech.eampm.model.Filter;

/******************************************************
 * 
 * Filename : FilterServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implpements from filterservice and its access the
 *
 * information about filterdao.
 * 
 * 
 *******************************************************/
@Service

public class FilterServiceImpl implements FilterService {

	@Autowired
	private FilterDAO filterDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setfilterDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the filter dao .
	 * 
	 * Input Params :filterDAO.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setfilterDAO(FilterDAO filterDAO) {
		this.filterDAO = filterDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addFilter
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the filter.
	 * 
	 * Input Params :filter.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addFilter(Filter filter) {
		filterDAO.addFilter(filter);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateFilter
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the filter.
	 * 
	 * Input Params :filter.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateFilter(Filter filter) {
		filterDAO.updateFilter(filter);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listFilters
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the filter.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<Filter>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<Filter> listFilters() {
		return this.filterDAO.listFilters();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getFilterById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to filter by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value :Filter
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public Filter getFilterById(int id) {
		return filterDAO.getFilterById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeFilter
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove filter by using id.
	 * 
	 * Input Params : id.
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeFilter(int id) {
		filterDAO.removeFilter(id);
	}

}
