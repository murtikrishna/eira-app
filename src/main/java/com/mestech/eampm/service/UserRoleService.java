
package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.UserRole;

public interface UserRoleService {

	public void addUserRole(UserRole userrole);
    
    public void updateUserRole(UserRole userrole);
    
    public UserRole getUserRoleById(int id);
    
    public void removeUserRole(int id);
    
    public List<UserRole> listUserRoles();	
    
	public List<UserRole> listRoles(int roleid);

}
