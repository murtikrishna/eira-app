package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.bean.SiteSummaryBean;
import com.mestech.eampm.dao.SiteSummaryDAO;
import com.mestech.eampm.model.SiteSummary;

/******************************************************
 * 
 * Filename :SiteSummaryServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from sitesummaryservice and its access
 *
 * the information about sitesummarydao.
 * 
 * 
 *******************************************************/

@Service
public class SiteSummaryServiceImpl implements SiteSummaryService {

	@Autowired
	private SiteSummaryDAO SiteSummaryDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setSiteSummaryDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the site summary dao.
	 * 
	 * Input Params :SiteSummaryDAO
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setSiteSummaryDAO(SiteSummaryDAO SiteSummaryDAO) {
		this.SiteSummaryDAO = SiteSummaryDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addSiteSummary
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the site summary.
	 * 
	 * Input Params :SiteSummary
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addSiteSummary(SiteSummary SiteSummary) {
		SiteSummaryDAO.addSiteSummary(SiteSummary);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateSiteSummary
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the site summary.
	 * 
	 * Input Params :SiteSummary
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateSiteSummary(SiteSummary SiteSummary) {
		SiteSummaryDAO.updateSiteSummary(SiteSummary);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listSiteSummarys
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site summaries.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<SiteSummary>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<SiteSummary> listSiteSummarys() {
		return this.SiteSummaryDAO.listSiteSummarys();
	}

	/********************************************************************************************
	 * 
	 * Function Name : listSiteSummarysByFilter1
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site summaries by using
	 * filter1.
	 * 
	 * Input Params : equipmentId, siteId, lastNDays
	 * 
	 * Return Value : List<SiteSummary>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<SiteSummary> listSiteSummarysByFilter1(int equipmentId, int siteId, int lastNDays) {
		return this.SiteSummaryDAO.listSiteSummarysByFilter1(equipmentId, siteId, lastNDays);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getSiteSummaryListByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site summaries by using
	 * user id.
	 * 
	 * Input Params :userId
	 * 
	 * Return Value : List<SiteSummary>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<SiteSummary> getSiteSummaryListByUserId(int userId) {
		return this.SiteSummaryDAO.getSiteSummaryListByUserId(userId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getSiteSummaryListByCustomerId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site summaries by using
	 * customer id.
	 * 
	 * Input Params :customerId
	 * 
	 * Return Value : List<SiteSummary>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<SiteSummary> getSiteSummaryListByCustomerId(int customerId) {
		return this.SiteSummaryDAO.getSiteSummaryListByCustomerId(customerId);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getSiteSummaryListBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site summaries by using
	 * site id.
	 * 
	 * Input Params :siteId
	 * 
	 * Return Value : List<SiteSummary>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<SiteSummary> getSiteSummaryListBySiteId(int siteId) {
		return this.SiteSummaryDAO.getSiteSummaryListBySiteId(siteId);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getSiteSummaryListBySiteIdWithDate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site summaries by using
	 * site id with date.
	 * 
	 * Input Params :siteId, FromDate, ToDate
	 * 
	 * Return Value : List<SiteSummary>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<SiteSummary> getSiteSummaryListBySiteIdWithDate(String siteId, String FromDate, String ToDate) {
		return this.SiteSummaryDAO.getSiteSummaryListBySiteIdWithDate(siteId, FromDate, ToDate);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getSiteSummaryListBySiteIdsGroupWithDate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site summaries by using
	 * site ids group with date.
	 * 
	 * Input Params :siteIds, FromDate, ToDate
	 * 
	 * Return Value : List<SiteSummary>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<SiteSummary> getSiteSummaryListBySiteIdsGroupWithDate(String siteIds, String FromDate, String ToDate) {
		return this.SiteSummaryDAO.getSiteSummaryListBySiteIdsGroupWithDate(siteIds, FromDate, ToDate);
	}

	/*
	 * @Transactional public List<SiteSummaryBean>
	 * getSiteSummaryListBySiteIdsGroup(String siteIds) { return
	 * this.SiteSummaryDAO.getSiteSummaryListBySiteIdsGroup(siteIds); }
	 */

	@Transactional
	public List<SiteSummary> getSiteSummaryListBySiteIdsGroup(String siteIds) {
		return this.SiteSummaryDAO.getSiteSummaryListBySiteIdsGroup(siteIds);
	}

	@Transactional
	public List<SiteSummary> getSiteSummaryListByEquipmentId(int equipmentId) {
		return this.SiteSummaryDAO.getSiteSummaryListByEquipmentId(equipmentId);
	}

	// @Override
	@Transactional
	public SiteSummary getSiteSummaryById(int id) {
		return SiteSummaryDAO.getSiteSummaryById(id);
	}

	@Transactional
	public SiteSummary getSiteSummaryByMax(String MaxColumnName) {
		return SiteSummaryDAO.getSiteSummaryByMax(MaxColumnName);
	}

	// @Override
	@Transactional
	public void removeSiteSummary(int id) {
		SiteSummaryDAO.removeSiteSummary(id);
	}

}
