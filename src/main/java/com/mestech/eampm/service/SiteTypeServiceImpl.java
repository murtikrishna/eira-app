package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.SiteTypeDAO;
import com.mestech.eampm.model.SiteType;

/******************************************************
 * 
 * Filename :SiteTypeServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class implements from sitetypeservice and its access the
 * 
 * information about sitetypeservice.
 * 
 * 
 *******************************************************/
@Service

public class SiteTypeServiceImpl implements SiteTypeService {

	@Autowired
	private SiteTypeDAO sitetypeDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setsitetypeDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the site type dao.
	 * 
	 * Input Params :sitetypeDAO
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setsitetypeDAO(SiteTypeDAO sitetypeDAO) {
		this.sitetypeDAO = sitetypeDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addSiteType
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the site type.
	 * 
	 * Input Params :sitetype
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addSiteType(SiteType sitetype) {
		sitetypeDAO.addSiteType(sitetype);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateSiteType
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the site type.
	 * 
	 * Input Params :sitetype
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateSiteType(SiteType sitetype) {
		sitetypeDAO.updateSiteType(sitetype);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listSiteTypes
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site types.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<SiteType>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<SiteType> listSiteTypes() {
		return this.sitetypeDAO.listSiteTypes();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getSiteTypeById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the site type by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : SiteType
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public SiteType getSiteTypeById(int id) {
		return sitetypeDAO.getSiteTypeById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name : removeSiteType
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the site type by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeSiteType(int id) {
		sitetypeDAO.removeSiteType(id);
	}

}
