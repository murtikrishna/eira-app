package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.bean.OpenStateTicketsCount;
import com.mestech.eampm.dao.TicketDetailDAO;
import com.mestech.eampm.model.TicketDetail;

/******************************************************
 * 
 * Filename :TicketDetailServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from ticketdetailservice and its access
 * 
 * the information about ticketdetaildao.
 * 
 * 
 *******************************************************/
@Service

public class TicketDetailServiceImpl implements TicketDetailService {

	@Autowired
	private TicketDetailDAO ticketDetailDAO;

	/********************************************************************************************
	 * 
	 * Function Name :addTicketDetail
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the ticket detail.
	 * 
	 * Input Params :ticketDetail
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void addTicketDetail(TicketDetail ticketDetail) {
		ticketDetailDAO.addTicketDetail(ticketDetail);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateTicketDetail
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the ticket detail.
	 * 
	 * Input Params :ticketDetail
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateTicketDetail(TicketDetail ticketDetail) {
		ticketDetailDAO.updateTicketDetail(ticketDetail);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeTicketDetail
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the ticket details by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeTicketDetail(int id) {
		ticketDetailDAO.removeTicketDetail(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTicketDetailById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the ticket details by using id.
	 * 
	 * Input Params :id,TimezoneOffset
	 * 
	 * Return Value :TicketDetail
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public TicketDetail getTicketDetailById(int id, String TimezoneOffset) {
		return ticketDetailDAO.getTicketDetailById(id, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTicketDetailByIdOnUTC
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the ticket datails by using id.
	 * 
	 * Input Params :id,TimezoneOffset
	 * 
	 * Return Value :TicketDetail
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public TicketDetail getTicketDetailByIdOnUTC(int id, String TimezoneOffset) {
		return ticketDetailDAO.getTicketDetailByIdOnUTC(id, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTicketDetailByTicketCode
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get ticket details by using ticket code.
	 * 
	 * Input Params :TicketCode,TimezoneOffset
	 * 
	 * Return Value :TicketDetail
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public TicketDetail getTicketDetailByTicketCode(String TicketCode, String TimezoneOffset) {
		return ticketDetailDAO.getTicketDetailByTicketCode(TicketCode, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTicketDetailByMax
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get ticket details by using maximum
	 * column name.
	 * 
	 * Input Params :MaxColumnName,TimezoneOffset
	 * 
	 * Return Value :TicketDetail
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public TicketDetail getTicketDetailByMax(String MaxColumnName, String TimezoneOffset) {
		return ticketDetailDAO.getTicketDetailByMax(MaxColumnName, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTicketDetailListByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the ticket details by using
	 * user id.
	 * 
	 * Input Params :userId,TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<TicketDetail> getTicketDetailListByUserId(int userId, String TimezoneOffset) {
		return this.ticketDetailDAO.getTicketDetailListByUserId(userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTicketDetailListBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the ticket details by using
	 * site id.
	 * 
	 * Input Params : siteId,TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<TicketDetail> getTicketDetailListBySiteId(int siteId, String TimezoneOffset) {
		return this.ticketDetailDAO.getTicketDetailListBySiteId(siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTicketDetailListByCustomerId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the ticket details by using
	 * customer id.
	 * 
	 * Input Params :customerId, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketDetail> getTicketDetailListByCustomerId(int customerId, String TimezoneOffset) {
		return this.ticketDetailDAO.getTicketDetailListByCustomerId(customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getTicketDetailListByUserIdLimit
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get all the ticket details by using user
	 * id.
	 * 
	 * Input Params :userId, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketDetail> getTicketDetailListByUserIdLimit(int userId, String TimezoneOffset) {
		return this.ticketDetailDAO.getTicketDetailListByUserIdLimit(userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getTicketDetailListBySiteLimit
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the ticket details list by
	 * using site id.
	 * 
	 * Input Params :siteid, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketDetail> getTicketDetailListBySiteLimit(int siteid, String TimezoneOffset) {
		return this.ticketDetailDAO.getTicketDetailListBySiteLimit(siteid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTicketDetailListByCustomerLimit
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the ticket details by using
	 * customer limit.
	 * 
	 * Input Params :customerid, TimezoneOffset.
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketDetail> getTicketDetailListByCustomerLimit(int customerid, String TimezoneOffset) {
		return this.ticketDetailDAO.getTicketDetailListByCustomerLimit(customerid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listTicketDetails
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the ticket details.
	 * 
	 * Input Params :TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketDetail> listTicketDetails(String TimezoneOffset) {
		return this.ticketDetailDAO.listTicketDetails(TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listTicketDetailsForDisplay
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the ticket details for
	 * display.
	 * 
	 * Input Params :userid, siteId, fromDate, toDate, category, type,priority,
	 * TimezoneOffset.
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketDetail> listTicketDetailsForDisplay(int userid, String siteId, String fromDate, String toDate,
			String category, String type, String priority, String TimezoneOffset) {
		return this.ticketDetailDAO.listTicketDetailsForDisplay(userid, siteId, fromDate, toDate, category, type,
				priority, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listCustomerTicketDetailsForDisplay
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the customer ticket details
	 * for display.
	 * 
	 * Input Params :customerid, siteId, fromDate, toDate, category,type, priority,
	 * TimezoneOffset.
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketDetail> listCustomerTicketDetailsForDisplay(int customerid, String siteId, String fromDate,
			String toDate, String category, String type, String priority, String TimezoneOffset) {
		return this.ticketDetailDAO.listCustomerTicketDetailsForDisplay(customerid, siteId, fromDate, toDate, category,
				type, priority, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listOpenticketstatecountsBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the open tickets state counts
	 * by using site id.
	 * 
	 * Input Params :siteid, TimezoneOffset.
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<OpenStateTicketsCount> listOpenticketstatecountsBySite(int siteid, String TimezoneOffset) {
		return this.ticketDetailDAO.listOpenticketstatecountsBySite(siteid, TimezoneOffset);
	}

	// @Override
	@Transactional
	public List<OpenStateTicketsCount> listOpenticketstatecountsByUser(int userid, String TimezoneOffset) {
		return this.ticketDetailDAO.listOpenticketstatecountsByUser(userid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listOpenticketstatecountsByAssignedByUser
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the open state ticket counts
	 * by using user id.
	 * 
	 * Input Params :userid, TimezoneOffset.
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<OpenStateTicketsCount> listOpenticketstatecountsByAssignedByUser(int userid, String TimezoneOffset) {
		return this.ticketDetailDAO.listOpenticketstatecountsByAssignedByUser(userid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listOpenticketstatecountsByAssignedBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the open tickets state counts
	 * by using user id.
	 * 
	 * Input Params :siteid, TimezoneOffset.
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<OpenStateTicketsCount> listOpenticketstatecountsByAssignedBySite(int siteid, String TimezoneOffset) {
		return this.ticketDetailDAO.listOpenticketstatecountsByAssignedBySite(siteid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listOpenticketstatecountspiechartBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the open tickets state counts
	 * pie chart by using site id.
	 * 
	 * Input Params :siteid, TimezoneOffset.
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<OpenStateTicketsCount> listOpenticketstatecountspiechartBySite(int siteid, String TimezoneOffset) {
		return this.ticketDetailDAO.listOpenticketstatecountspiechartBySite(siteid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listOpenticketstatecountspiechartByUser
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the open tickets state counts
	 * pie chart by using user id.
	 * 
	 * Input Params :userid, TimezoneOffset.
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<OpenStateTicketsCount> listOpenticketstatecountspiechartByUser(int userid, String TimezoneOffset) {
		return this.ticketDetailDAO.listOpenticketstatecountspiechartByUser(userid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listAllticketstatecountsByUser
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the ticket state counts by
	 * using user id.
	 * 
	 * Input Params :userid, TimezoneOffset.
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<OpenStateTicketsCount> listAllticketstatecountsByUser(int userid, String TimezoneOffset) {
		return this.ticketDetailDAO.listAllticketstatecountsByUser(userid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listAllicketstatecountsBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the ticket state counts by
	 * using site id.
	 * 
	 * Input Params :siteid, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<OpenStateTicketsCount> listAllicketstatecountsBySite(int siteid, String TimezoneOffset) {
		return this.ticketDetailDAO.listAllicketstatecountsBySite(siteid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listAllicketstatecountsByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the open state tickets count
	 * by using customer id.
	 * 
	 * Input Params :customerid, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<OpenStateTicketsCount> listAllicketstatecountsByCustomer(int customerid, String TimezoneOffset) {
		return this.ticketDetailDAO.listAllicketstatecountsByCustomer(customerid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listOpenticketstatecountspiechartByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the open state counts pie
	 * chart by using customer id.
	 * 
	 * Input Params :customerid, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<OpenStateTicketsCount> listOpenticketstatecountspiechartByCustomer(int customerid,
			String TimezoneOffset) {
		return this.ticketDetailDAO.listOpenticketstatecountspiechartByCustomer(customerid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listOpenticketstatecountsByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the open state ticket counts
	 * by using customer id.
	 * 
	 * Input Params :customerid, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<OpenStateTicketsCount> listOpenticketstatecountsByCustomer(int customerid, String TimezoneOffset) {
		return this.ticketDetailDAO.listOpenticketstatecountsByCustomer(customerid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listOpenticketstatecountsByAssignedByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the open state ticket counts
	 * by using customer id.
	 * 
	 * Input Params :customerid, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<OpenStateTicketsCount> listOpenticketstatecountsByAssignedByCustomer(int customerid,
			String TimezoneOffset) {
		return this.ticketDetailDAO.listOpenticketstatecountsByAssignedByCustomer(customerid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listStatefilterTickets
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the state filter tickets.
	 * 
	 * Input Params :state, userid, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketDetail> listStatefilterTickets(String state, int userid, String TimezoneOffset) {
		return this.ticketDetailDAO.listStatefilterTickets(state, userid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listStatefilterTicketsBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the state filter tickets by
	 * using site id.
	 * 
	 * Input Params :state, siteid, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketDetail> listStatefilterTicketsBySite(String state, int siteid, String TimezoneOffset) {
		return this.ticketDetailDAO.listStatefilterTicketsBySite(state, siteid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listStatefilterTicketsByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the state filter tickets by
	 * using customer id.
	 * 
	 * Input Params :state, customerid, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketDetail> listStatefilterTicketsByCustomer(String state, int customerid, String TimezoneOffset) {
		return this.ticketDetailDAO.listStatefilterTicketsByCustomer(state, customerid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listStateandStatusfilterTickets
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the state and status filter
	 * tickets.
	 * 
	 * Input Params :state, status, userid, TimezoneOffset
	 * 
	 * Return Value : List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketDetail> listStateandStatusfilterTickets(int state, int status, int userid,
			String TimezoneOffset) {
		return this.ticketDetailDAO.listStateandStatusfilterTickets(state, status, userid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listStateandStatusfilterTicketsBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the state and status filter
	 * tickets by using site id.
	 * 
	 * Input Params :state, status, siteid, TimezoneOffset
	 * 
	 * Return Value : List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketDetail> listStateandStatusfilterTicketsBySite(int state, int status, int siteid,
			String TimezoneOffset) {
		return this.ticketDetailDAO.listStateandStatusfilterTicketsBySite(state, status, siteid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listStateandStatusfilterTicketsByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the state and status filter
	 * tickets by using site id.
	 * 
	 * Input Params :state, status, customerid, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketDetail> listStateandStatusfilterTicketsByCustomer(int state, int status, int customerid,
			String TimezoneOffset) {
		return this.ticketDetailDAO.listStateandStatusfilterTicketsByCustomer(state, status, customerid,
				TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listDaylapsedsincecreationfilterTickets
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the day lapsed since creation
	 * filter tickets.
	 * 
	 * Input Params :state, status, fromday, today, userid, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketDetail> listDaylapsedsincecreationfilterTickets(int state, int status, int fromday, int today,
			int userid, String TimezoneOffset) {
		return this.ticketDetailDAO.listDaylapsedsincecreationfilterTickets(state, status, fromday, today, userid,
				TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listDaylapsedsincecreationfilterTicketsBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the day lapsed since creation
	 * filter tickets by using site id.
	 * 
	 * Input Params :state, status, fromday, today, siteid, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketDetail> listDaylapsedsincecreationfilterTicketsBySite(int state, int status, int fromday,
			int today, int siteid, String TimezoneOffset) {
		return this.ticketDetailDAO.listDaylapsedsincecreationfilterTicketsBySite(state, status, fromday, today, siteid,
				TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listDaylapsedsincecreationfilterTicketsByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the day lapsed since creation
	 * filter tickets by using customer id.
	 * 
	 * Input Params :state, status, fromday, today, customerid, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<TicketDetail> listDaylapsedsincecreationfilterTicketsByCustomer(int state, int status, int fromday,
			int today, int customerid, String TimezoneOffset) {
		return this.ticketDetailDAO.listDaylapsedsincecreationfilterTicketsByCustomer(state, status, fromday, today,
				customerid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :findByEquipmentId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the ticket details by using
	 * equipment id.
	 * 
	 * Input Params :siteId, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<TicketDetail> findByEquipmentId(int siteId, String TimezoneOffset) {
		return this.ticketDetailDAO.findByEquipmentId(siteId, TimezoneOffset);

	}

	/********************************************************************************************
	 * 
	 * Function Name :Allstatepiechart
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the state pie charts.
	 * 
	 * Input Params :userId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<OpenStateTicketsCount> Allstatepiechart(int userId, String TimezoneOffset) {
		return this.ticketDetailDAO.Allstatepiechart(userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :Operationallstatepiechart
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the operational state pie
	 * chart.
	 * 
	 * Input Params :userId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<OpenStateTicketsCount> Operationallstatepiechart(int userId, String TimezoneOffset) {
		return this.ticketDetailDAO.Operationallstatepiechart(userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :Maintenanceallstatepiechart
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the maintenance all state pie
	 * chart.
	 * 
	 * Input Params :userId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> Maintenanceallstatepiechart(int userId, String TimezoneOffset) {
		return this.ticketDetailDAO.Maintenanceallstatepiechart(userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :Openstateandstatustikcets
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the open state and status
	 * tickets.
	 * 
	 * Input Params :userId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> Openstateandstatustikcets(int userId, String TimezoneOffset) {
		return this.ticketDetailDAO.Openstateandstatustikcets(userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :Closestateandstatustikcets
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the close state and status
	 * tickets.
	 * 
	 * Input Params :userId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> Closestateandstatustikcets(int userId, String TimezoneOffset) {
		return this.ticketDetailDAO.Closestateandstatustikcets(userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :Holdstateandstatustikcets
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the Hold state and status
	 * tickets.
	 * 
	 * Input Params :userId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> Holdstateandstatustikcets(int userId, String TimezoneOffset) {
		return this.ticketDetailDAO.Holdstateandstatustikcets(userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :AllstatepiechartBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all state pie charts by site id.
	 * 
	 * Input Params :siteId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> AllstatepiechartBySite(int siteId, String TimezoneOffset) {
		return this.ticketDetailDAO.AllstatepiechartBySite(siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :OperationallstatepiechartBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all state pie charts by site id.
	 * 
	 * Input Params :siteId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> OperationallstatepiechartBySite(int siteId, String TimezoneOffset) {
		return this.ticketDetailDAO.OperationallstatepiechartBySite(siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :MaintenanceallstatepiechartBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to maintenance all state pie chart by using
	 * site id.
	 * 
	 * Input Params :siteId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> MaintenanceallstatepiechartBySite(int siteId, String TimezoneOffset) {
		return this.ticketDetailDAO.MaintenanceallstatepiechartBySite(siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :OpenstateandstatustikcetsBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the open state and status
	 * tickets by using site id.
	 * 
	 * Input Params :siteId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> OpenstateandstatustikcetsBySite(int siteId, String TimezoneOffset) {
		return this.ticketDetailDAO.OpenstateandstatustikcetsBySite(siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : ClosestateandstatustikcetsBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the close state and status
	 * tickets by using site id.
	 * 
	 * Input Params :siteId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> ClosestateandstatustikcetsBySite(int siteId, String TimezoneOffset) {
		return this.ticketDetailDAO.ClosestateandstatustikcetsBySite(siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : HoldstateandstatustikcetsBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the hold state and status
	 * tickets by using site id.
	 * 
	 * 
	 * Input Params :siteId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> HoldstateandstatustikcetsBySite(int siteId, String TimezoneOffset) {
		return this.ticketDetailDAO.HoldstateandstatustikcetsBySite(siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : AllstatepiechartByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the state pie chart by using
	 * customer id.
	 * 
	 * Input Params :customerId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> AllstatepiechartByCustomer(int customerId, String TimezoneOffset) {
		return this.ticketDetailDAO.AllstatepiechartByCustomer(customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : OperationallstatepiechartByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the operational state pie
	 * chart by using customer id.
	 * 
	 * Input Params :customerId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> OperationallstatepiechartByCustomer(int customerId, String TimezoneOffset) {
		return this.ticketDetailDAO.OperationallstatepiechartByCustomer(customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : MaintenanceallstatepiechartByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the maintenance all state pie
	 * chart by using customer id.
	 * 
	 * Input Params :customerId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> MaintenanceallstatepiechartByCustomer(int customerId, String TimezoneOffset) {
		return this.ticketDetailDAO.MaintenanceallstatepiechartByCustomer(customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : OpenstateandstatustikcetsByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the open state and status
	 * tickets by using customer id.
	 * 
	 * Input Params :customerId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> OpenstateandstatustikcetsByCustomer(int customerId, String TimezoneOffset) {
		return this.ticketDetailDAO.OpenstateandstatustikcetsByCustomer(customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : ClosestateandstatustikcetsByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the close state and status
	 * tickets by customer id.
	 * 
	 * Input Params :customerId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> ClosestateandstatustikcetsByCustomer(int customerId, String TimezoneOffset) {

		return this.ticketDetailDAO.ClosestateandstatustikcetsByCustomer(customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : HoldstateandstatustikcetsByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the hold state and status
	 * tickets by using customer.
	 * 
	 * Input Params :customerId, TimezoneOffset
	 * 
	 * Return Value :List<OpenStateTicketsCount>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<OpenStateTicketsCount> HoldstateandstatustikcetsByCustomer(int customerId, String TimezoneOffset) {
		return this.ticketDetailDAO.HoldstateandstatustikcetsByCustomer(customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listStatefilterTickets
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the state filter tickets.
	 * 
	 * Input Params :type, state, userid, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<TicketDetail> listStatefilterTickets(String type, String state, int userid, String TimezoneOffset) {
		return this.ticketDetailDAO.listStatefilterTickets(type, state, userid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :AllStateandStatusfilterTickets
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the state and status filter
	 * tickets.
	 * 
	 * Input Params :type, status, userid, TimezoneOffset
	 * 
	 * Return Value : List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<TicketDetail> AllStateandStatusfilterTickets(String type, int status, int userid,
			String TimezoneOffset) {
		return this.ticketDetailDAO.AllStateandStatusfilterTickets(type, status, userid, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listStatefilterTicketsBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the state filter tickets by
	 * using site id.
	 * 
	 * Input Params :type, state, siteId, TimezoneOffset
	 * 
	 * Return Value : List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<TicketDetail> listStatefilterTicketsBySite(String type, String state, int siteId,
			String TimezoneOffset) {
		return this.ticketDetailDAO.listStatefilterTicketsBySite(type, state, siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :AllStateandStatusfilterTicketsBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the state and status filter
	 * tickets by using site id.
	 * 
	 * Input Params :type, state, siteId, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<TicketDetail> AllStateandStatusfilterTicketsBySite(String type, int status, int siteId,
			String TimezoneOffset) {
		return this.ticketDetailDAO.AllStateandStatusfilterTicketsBySite(type, status, siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listTypefilterTicketsByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all type filter tickets by using
	 * customer id.
	 * 
	 * Input Params :type, state, customerId, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<TicketDetail> listTypefilterTicketsByCustomer(String type, String state, int customerId,
			String TimezoneOffset) {
		return this.ticketDetailDAO.listTypefilterTicketsByCustomer(type, state, customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :AllStateandStatusfilterTicketsByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all state and status filter
	 * tickets by customer id.
	 * 
	 * Input Params :type, state, customerId, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<TicketDetail> AllStateandStatusfilterTicketsByCustomer(String type, int status, int customerId,
			String TimezoneOffset) {
		return this.ticketDetailDAO.AllStateandStatusfilterTicketsByCustomer(type, status, customerId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listReportTicketDetailsForDisplay
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all report ticket details for
	 * display.
	 * 
	 * Input Params :userid, siteId, fromDate, toDate, category, type, priority,
	 * TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<TicketDetail> listReportTicketDetailsForDisplay(int userid, String siteId, String fromDate,
			String toDate, String category, String type, String priority, String TimezoneOffset) {
		return this.ticketDetailDAO.listReportTicketDetailsForDisplay(userid, siteId, fromDate, toDate, category, type,
				priority, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getReportTicketDetailListByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all report ticket details by using
	 * user id.
	 * 
	 * Input Params :userId, TimezoneOffset
	 * 
	 * Return Value :List<TicketDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<TicketDetail> getReportTicketDetailListByUserId(int userId, String TimezoneOffset) {
		return this.ticketDetailDAO.getReportTicketDetailListByUserId(userId, TimezoneOffset);
	}

}
