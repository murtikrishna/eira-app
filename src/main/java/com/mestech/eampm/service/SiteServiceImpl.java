
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.bean.AnnualYeild;
import com.mestech.eampm.bean.DashboardEnergySummaryBean;
import com.mestech.eampm.bean.DashboardOandMBean;
import com.mestech.eampm.bean.DashboardSiteBean;
import com.mestech.eampm.bean.DashboardSiteListBean;
import com.mestech.eampm.dao.SiteDAO;
import com.mestech.eampm.model.Site;

/******************************************************
 * 
 * Filename :SiteServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from siteservice and its access the
 * 
 * information about sitedao.
 * 
 * 
 *******************************************************/
@Service
public class SiteServiceImpl implements SiteService {

	@Autowired
	private SiteDAO siteDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setsiteDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the site dao.
	 * 
	 * Input Params :siteDAO.
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setsiteDAO(SiteDAO siteDAO) {
		this.siteDAO = siteDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addSite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the site in site dao.
	 * 
	 * Input Params :site.
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public Site addSite(Site site) {
		return siteDAO.addSite(site);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateSite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the site in site dao.
	 * 
	 * Input Params :site.
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateSite(Site site) {
		siteDAO.updateSite(site);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listSites
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the sites.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<Site>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<Site> listSites() {
		return this.siteDAO.listSites();
	}

	/********************************************************************************************
	 * 
	 * Function Name :listConfigSites
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the config sites.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<Site>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<Site> listConfigSites() {
		return this.siteDAO.listConfigSites();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getSiteById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the site by using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value :Site.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public Site getSiteById(int id) {
		return siteDAO.getSiteById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getSiteByName
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get site by using site name.
	 * 
	 * Input Params :siteName
	 * 
	 * Return Value :Site.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public Site getSiteByName(String siteName) {
		return siteDAO.getSiteByName(siteName);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getSiteListByCustomerId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get site by using customer id.
	 * 
	 * Input Params :customerId
	 * 
	 * Return Value :List<Site>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<Site> getSiteListByCustomerId(int customerId) {
		return siteDAO.getSiteListByCustomerId(customerId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getSiteListByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site by using user id.
	 * 
	 * Input Params :userId
	 * 
	 * Return Value :List<Site>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<Site> getSiteListByUserId(int userId) {
		return siteDAO.getSiteListByUserId(userId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getProductionSiteListByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the production site by using
	 * user id.
	 * 
	 * Input Params :userId
	 * 
	 * Return Value :List<Site>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<Site> getProductionSiteListByUserId(int userId) {
		return siteDAO.getProductionSiteListByUserId(userId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getSiteByMax
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the site by using maximum column
	 * name.
	 * 
	 * Input Params :MaxColumnName
	 * 
	 * Return Value :Site.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public Site getSiteByMax(String MaxColumnName) {
		return siteDAO.getSiteByMax(MaxColumnName);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeSite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove site by using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeSite(int id) {
		siteDAO.removeSite(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listSiteById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site by using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value :List<Site>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<Site> listSiteById(int id) {
		return siteDAO.listSiteById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDashboardSiteDetails
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the dash board site details.
	 * 
	 * Input Params :UserID, TimezoneOffset
	 * 
	 * Return Value :List<DashboardSiteBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<DashboardSiteBean> getDashboardSiteDetails(String UserID, String TimezoneOffset) {
		return siteDAO.getDashboardSiteDetails(UserID, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDashboardEnergySummaryDetails
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the dash board energy summary
	 * details.
	 * 
	 * Input Params :UserID, TimezoneOffset
	 * 
	 * Return Value :List<DashboardEnergySummaryBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<DashboardEnergySummaryBean> getDashboardEnergySummaryDetails(String UserID, String TimezoneOffset) {
		return siteDAO.getDashboardEnergySummaryDetails(UserID, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDashboardEventAndTicketDetails
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the dash board event and
	 * ticket details.
	 * 
	 * Input Params :UserID, TimezoneOffset
	 * 
	 * Return Value :List<DashboardOandMBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<DashboardOandMBean> getDashboardEventAndTicketDetails(String UserID, String TimezoneOffset) {
		return siteDAO.getDashboardEventAndTicketDetails(UserID, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDashboardSiteList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the dash board site.
	 * 
	 * Input Params :UserID, TimezoneOffset
	 * 
	 * Return Value :List<DashboardSiteListBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<DashboardSiteListBean> getDashboardSiteList(String UserID, String TimezoneOffset) {
		return siteDAO.getDashboardSiteList(UserID, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDashboardSiteDetailsByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the dash board site details by
	 * using customer id.
	 * 
	 * Input Params :CustomerID, TimezoneOffset
	 * 
	 * Return Value :List<DashboardSiteBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<DashboardSiteBean> getDashboardSiteDetailsByCustomer(String CustomerID, String TimezoneOffset) {
		return siteDAO.getDashboardSiteDetailsByCustomer(CustomerID, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDashboardEnergySummaryDetailsByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the dash board energy summary
	 * details by using customer id.
	 * 
	 * Input Params :CustomerID, TimezoneOffset
	 * 
	 * Return Value :List<DashboardEnergySummaryBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<DashboardEnergySummaryBean> getDashboardEnergySummaryDetailsByCustomer(String CustomerID,
			String TimezoneOffset) {
		return siteDAO.getDashboardEnergySummaryDetailsByCustomer(CustomerID, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDashboardEventAndTicketDetailsByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the dash board event and
	 * ticket detail by using customer id.
	 * 
	 * Input Params :CustomerID, TimezoneOffset
	 * 
	 * Return Value :List<DashboardOandMBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<DashboardOandMBean> getDashboardEventAndTicketDetailsByCustomer(String CustomerID,
			String TimezoneOffset) {
		return siteDAO.getDashboardEventAndTicketDetailsByCustomer(CustomerID, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDashboardSiteListByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the dash board site by using
	 * customer id.
	 * 
	 * Input Params :CustomerID, TimezoneOffset
	 * 
	 * Return Value :List<DashboardSiteListBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<DashboardSiteListBean> getDashboardSiteListByCustomer(String CustomerID, String TimezoneOffset) {
		return siteDAO.getDashboardSiteListByCustomer(CustomerID, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listallsites
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the sites by using site id.
	 * 
	 * Input Params :siteid
	 * 
	 * Return Value :List<Site>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Site> listallsites(int siteid) {
		return siteDAO.listallsites(siteid);

	}

	/********************************************************************************************
	 * 
	 * Function Name :getAnnualyeildbySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the annualyeild by using site
	 * id.
	 * 
	 * Input Params :siteid
	 * 
	 * Return Value :List<AnnualYeild>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<AnnualYeild> getAnnualyeildbySite(int siteid) {
		return siteDAO.getAnnualyeildbySite(siteid);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateSiteConfig
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the site config.
	 * 
	 * Input Params :site
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	@Transactional
	public void updateSiteConfig(Site site) {
		// TODO Auto-generated method stub
		siteDAO.updateSiteConfig(site);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listSitesAgainstCollection
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the sites against collection.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<Site>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public List<Site> listSitesAgainstCollection() {
		// TODO Auto-generated method stub
		return siteDAO.listSitesAgainstCollection();
	}

}
