package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.EquipmentCategoryDAO;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentCategory;

/******************************************************
 * 
 * Filename :EquipmentCategoryServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from equipementcategoryservice and its
 * 
 * access the information about equipmentcategory.
 * 
 * 
 *******************************************************/
@Service

public class EquipmentCategoryServiceImpl implements EquipmentCategoryService {

	@Autowired
	private EquipmentCategoryDAO equipmentcategoryDAO;

	/********************************************************************************************
	 * 
	 * Function Name : setequipmentcategoryDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the equipment category dao.
	 * 
	 * Input Params : equipmentcategoryDAO
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setequipmentcategoryDAO(EquipmentCategoryDAO equipmentcategoryDAO) {
		this.equipmentcategoryDAO = equipmentcategoryDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name : addEquipmentCategory
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the equipment category.
	 * 
	 * Input Params : equipmentcategory.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addEquipmentCategory(EquipmentCategory equipmentcategory) {
		equipmentcategoryDAO.addEquipmentCategory(equipmentcategory);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateEquipmentCategory
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the equipment category.
	 * 
	 * Input Params : equipmentcategory.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void updateEquipmentCategory(EquipmentCategory equipmentcategory) {
		equipmentcategoryDAO.updateEquipmentCategory(equipmentcategory);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listEquipmentCategories
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the equipment categories.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<EquipmentCategory>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<EquipmentCategory> listEquipmentCategories() {
		return this.equipmentcategoryDAO.listEquipmentCategories();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEquipmentCategoryById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get equipment category by using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value :EquipmentCategory
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public EquipmentCategory getEquipmentCategoryById(int id) {
		return equipmentcategoryDAO.getEquipmentCategoryById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeEquipmentCategory
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove equipment category by using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeEquipmentCategory(int id) {
		equipmentcategoryDAO.removeEquipmentCategory(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEquipmentCategoryByMax
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get equipment category by using Maximum
	 * column name.
	 * 
	 * Input Params :MaxColumnName
	 * 
	 * Return Value : EquipmentCategory
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public EquipmentCategory getEquipmentCategoryByMax(String MaxColumnName) {
		return equipmentcategoryDAO.getEquipmentCategoryByMax(MaxColumnName);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listEquipmentCategorys
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all equipment categories by using
	 * category id.
	 * 
	 * Input Params :categoryid
	 * 
	 * Return Value : List<EquipmentCategory>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<EquipmentCategory> listEquipmentCategorys(int categoryid) {
		return equipmentcategoryDAO.listEquipmentCategorys(categoryid);
	}

}
