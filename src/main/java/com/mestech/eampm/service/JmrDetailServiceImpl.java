package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.JmrDetailDAO;
import com.mestech.eampm.model.JmrDetail;

/******************************************************
 * 
 * Filename :JmrDetailServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from jmrdetailservice and its access
 * 
 * the information about jmrdetaildao.
 * 
 * 
 *******************************************************/
@Service

public class JmrDetailServiceImpl implements JmrDetailService {

	@Autowired
	private JmrDetailDAO jmrdetailDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setjmrdetailDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set jmr detail dao.
	 * 
	 * Input Params :jmrdetailDAO.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setjmrdetailDAO(JmrDetailDAO jmrdetailDAO) {
		this.jmrdetailDAO = jmrdetailDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addJmrDetail
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save jmr detail.
	 * 
	 * Input Params :jmrdetail.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addJmrDetail(JmrDetail jmrdetail) {
		jmrdetailDAO.addJmrDetail(jmrdetail);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateJmrDetail
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update jmr detail.
	 * 
	 * Input Params :jmrdetail.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateJmrDetail(JmrDetail jmrdetail) {
		jmrdetailDAO.updateJmrDetail(jmrdetail);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listJmrDetails
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all jmr details.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<JmrDetail>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<JmrDetail> listJmrDetails() {
		return this.jmrdetailDAO.listJmrDetails();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getJmrDetailById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get jmr details by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : JmrDetail
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public JmrDetail getJmrDetailById(int id) {
		return jmrdetailDAO.getJmrDetailById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeJmrDetail
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove jmr details by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeJmrDetail(int id) {
		jmrdetailDAO.removeJmrDetail(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getticketID
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get jmr tickets by using ticket id.
	 * 
	 * Input Params : ticketid
	 * 
	 * Return Value : JmrDetail
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	@Transactional
	public JmrDetail getticketID(Integer ticketid) {
		return jmrdetailDAO.getticketID(ticketid);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getJmrDetailByticketId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get jmr detail by using ticket id.
	 * 
	 * Input Params : ticketid
	 * 
	 * Return Value : JmrDetail
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	public JmrDetail getJmrDetailByticketId(int ticketid) {
		return jmrdetailDAO.getJmrDetailByticketId(ticketid);
	}

}
