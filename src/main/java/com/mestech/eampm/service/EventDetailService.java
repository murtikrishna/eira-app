package com.mestech.eampm.service;


import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.bean.OpenStateTicketsCount;
import com.mestech.eampm.model.EventDetail;

public interface EventDetailService {
	
	public void addEventDetail(EventDetail eventdetail);
    
	public void updateEventDetail(EventDetail eventdetail);
	    
	public void removeEventDetail(int id);
	
	public EventDetail getEventDetailById(int id,String TimezoneOffset);
	
	public List<EventDetail> getEventDetailListByUserId(int userId,String TimezoneOffset);

	public List<EventDetail> getTodayEventDetailListByUserId(int userId,String TimezoneOffset);
	
	public List<EventDetail> getTodayEventDetailListByCustomerId(int customerId,String TimezoneOffset);
	
	public List<EventDetail> getEventDetailListBySiteId(int siteId,String TimezoneOffset);	

	public List<EventDetail> getTodayEventDetailListBySiteId(int siteId,String TimezoneOffset);
	    
	public List<EventDetail> listEventDetailsForDisplay(int userid,String siteId,String fromDate,String toDate,int  eventcode,int errorid,String equipmentid, String priority, int severity ,String TimezoneOffset);
		  
	public EventDetail getEventDetailByMax(String MaxColumnName, String TimezoneOffset);
	
	public List<EventDetail> getEventDetailListByCustomerId(int customerId,String TimezoneOffset);
	
	public List<EventDetail> getEventDetailListByErrorId(int errorId,String TimezoneOffset);
    
	    
	public List<EventDetail> listEventDetails( String TimezoneOffset);
	
	public List<OpenStateTicketsCount> EventsCountByErrorMessage(int userId,String TimezoneOffset);
	
	public List<EventDetail> getEventDetailListByUserIdLimit(int userId, String TimezoneOffset);
	
	public List<EventDetail> listCustomerEventDetailsForDisplay(int customerid,String siteId,String fromDate,String toDate,int  eventcode,int errorid,String equipmentid, String priority, int severity ,String TimezoneOffset);
	
	public List<OpenStateTicketsCount> getEventDetailBasedOnDays(int userId,String TimezoneOffset);
	
	
	public List<EventDetail> getEventDetailBasedOnSite(String siteId,String TimezoneOffset);
	
	
	public List<EventDetail> getEventDetailListBySiteIdLimit(int siteId, String TimezoneOffset);
	public List<OpenStateTicketsCount> getEventDetailBasedOnDaysBySite(int siteId,String TimezoneOffset);
	
	
	
	public List<OpenStateTicketsCount> EventsCountByErrorMessageBySite(int siteId,String TimezoneOffset);
	
	
	public List<EventDetail> getEventDetailListByCustomerIdLimit(int customerId, String TimezoneOffset);
	public List<OpenStateTicketsCount> getEventDetailBasedOnDaysByCustomerId(int customerId,String TimezoneOffset);
	
	
	
	public List<OpenStateTicketsCount> EventsCountByErrorMessageByCustomerId(int customerId,String TimezoneOffset);
	
	public List<EventDetail> ClickByErrorMessage(String ErrorMessage,int userId,String TimezoneOffset);
	
	public List<EventDetail> ClickByErrorMessageBySite(String ErrorMessage,int siteId,String TimezoneOffset);
	public List<EventDetail> ClickByErrorMessageByCustomer(String ErrorMessage,int customerId,String TimezoneOffset);
	public List<EventDetail> findByEquipmentId(int equipmentId,String TimezoneOffset);
}
