
/******************************************************
 * 
 *    	Filename	: BatteryLimitsServiceImpl

 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 25-Apr-19 01:44 PM 
 *      
 *      Description : 
 *      
 *      
 *******************************************************/
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.ActivityDAO;
import com.mestech.eampm.dao.BatteryLimitsDAO;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.BatteryLimits;

/******************************************************
 * 
 * Filename : BatteryLimitsServiceImpl
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description : this class is implements from batterylimitsservice and its
 * 
 * access the information about batterylimitsdao.
 * 
 * 
 *******************************************************/

@Service

public class BatteryLimitsServiceImpl implements BatteryLimitsService {

	@Autowired
	private BatteryLimitsDAO batterylimitsDAO;

	/********************************************************************************************
	 * 
	 * Function Name : setactivityDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the betterylimitsdao.
	 * 
	 * Input Params : batterylimitsDAO
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	public void setactivityDAO(BatteryLimitsDAO batterylimitsDAO) {
		this.batterylimitsDAO = batterylimitsDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name : addBatteryLimits
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the betterylimmits.
	 * 
	 * Input Params : batterylimits
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void addBatteryLimits(BatteryLimits batterylimits) {
		batterylimitsDAO.addBatteryLimits(batterylimits);
	}

	/********************************************************************************************
	 * 
	 * Function Name : updateBatteryLimits
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the batterylimits.
	 * 
	 * Input Params : batterylimits
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void updateBatteryLimits(BatteryLimits batterylimits) {
		batterylimitsDAO.updateBatteryLimits(batterylimits);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listBatteryLimits
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the batterylimits.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<BatteryLimits>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<BatteryLimits> listBatteryLimits() {
		return this.batterylimitsDAO.listBatteryLimits();
	}

	/********************************************************************************************
	 * 
	 * Function Name : getBatteryLimitsById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get batterylimits by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : BatteryLimits
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public BatteryLimits getBatteryLimitsById(int id) {
		return batterylimitsDAO.getBatteryLimitsById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeBatteryLimits
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the battery limits by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeBatteryLimits(int id) {
		batterylimitsDAO.removeBatteryLimits(id);
	}

}
