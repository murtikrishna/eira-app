package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.dao.DataSourceDAO;
import com.mestech.eampm.model.DataSource;

/******************************************************
 * 
 * Filename : DataSourceServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from datasourceservice and its access
 * 
 * the information about datasourcedao.
 * 
 * 
 *******************************************************/
@Service
public class DataSourceServiceImpl implements DataSourceService {

	@Autowired
	private DataSourceDAO dataSourceDAO;

	/********************************************************************************************
	 * 
	 * Function Name : setdataSourceDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set datasourcedao.
	 * 
	 * Input Params : dataSourceDAO
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setdataSourceDAO(DataSourceDAO dataSourceDAO) {
		this.dataSourceDAO = dataSourceDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name : addDataSource
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save datasource.
	 * 
	 * Input Params : dataSource
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void addDataSource(DataSource dataSource) {
		dataSourceDAO.addDataSource(dataSource);
	}

	/********************************************************************************************
	 * 
	 * Function Name : updateDataSource
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update datasource.
	 * 
	 * Input Params : dataSource
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void updateDataSource(DataSource dataSource) {
		dataSourceDAO.updateDataSource(dataSource);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listDataSources
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all datasource.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<DataSource>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<DataSource> listDataSources() {
		return this.dataSourceDAO.listDataSources();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataSourceListByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the datasource by using customer id.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<DataSource>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataSource> getDataSourceListByUserId(int userId) {
		return this.dataSourceDAO.getDataSourceListByUserId(userId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataSourceListByCustomerId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get datasource by using customer id.
	 * 
	 * Input Params : customerId
	 * 
	 * Return Value : List<DataSource>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataSource> getDataSourceListByCustomerId(int customerId) {
		return this.dataSourceDAO.getDataSourceListByCustomerId(customerId);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDataSourceListBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get datasource by using site id.
	 * 
	 * Input Params : siteId
	 * 
	 * Return Value : List<DataSource>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataSource> getDataSourceListBySiteId(int siteId) {
		return this.dataSourceDAO.getDataSourceListBySiteId(siteId);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDataSourceListByEquipmentId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the datasource by using
	 * equipmentid.
	 * 
	 * Input Params : equipmentId
	 * 
	 * Return Value : List<DataSource>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataSource> getDataSourceListByEquipmentId(int equipmentId) {
		return this.dataSourceDAO.getDataSourceListByEquipmentId(equipmentId);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDataSourceById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get datasource by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : DataSource
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public DataSource getDataSourceById(int id) {
		return dataSourceDAO.getDataSourceById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataSourceByMax
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get datasource by using maximum column
	 * name.
	 * 
	 * Input Params : MaxColumnName
	 * 
	 * Return Value : DataSource
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public DataSource getDataSourceByMax(String MaxColumnName) {
		return dataSourceDAO.getDataSourceByMax(MaxColumnName);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeDataSource
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove datasource by using id
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeDataSource(int id) {
		dataSourceDAO.removeDataSource(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listDataSourceForInverters
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all datasource for inverters.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<DataSource>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<DataSource> listDataSourceForInverters() {
		return this.dataSourceDAO.listDataSourceForInverters();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataSourceForInvertersListByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all datasource for inverters by
	 * using user id.
	 * 
	 * Input Params : userId
	 * 
	 * Return Value : List<DataSource>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataSource> getDataSourceForInvertersListByUserId(int userId) {
		return this.dataSourceDAO.getDataSourceForInvertersListByUserId(userId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataSourceForInvertersListByCustomerId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all datasource for inverters by
	 * using customer id.
	 * 
	 * Input Params : customerId
	 * 
	 * Return Value : List<DataSource>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataSource> getDataSourceForInvertersListByCustomerId(int customerId) {
		return this.dataSourceDAO.getDataSourceForInvertersListByCustomerId(customerId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataSourceForInvertersListBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all data source for inverter by
	 * using site id.
	 * 
	 * Input Params : siteId
	 * 
	 * Return Value : List<DataSource>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<DataSource> getDataSourceForInvertersListBySiteId(int siteId) {
		return this.dataSourceDAO.getDataSourceForInvertersListBySiteId(siteId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataSourceForEnegrymetersListBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get data source for energymeter by using
	 * site id.
	 * 
	 * Input Params : siteId
	 * 
	 * Return Value : List<DataSource>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataSource> getDataSourceForEnegrymetersListBySiteId(int siteId) {
		return this.dataSourceDAO.getDataSourceForEnegrymetersListBySiteId(siteId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataSourceForScbListBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get data source for scblist by using site
	 * id..
	 * 
	 * Input Params : siteId
	 * 
	 * Return Value : List<DataSource>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataSource> getDataSourceForScbListBySiteId(int siteId) {
		return this.dataSourceDAO.getDataSourceForScbListBySiteId(siteId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataSourceForTrackerListBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data.
	 * 
	 * Input Params : siteId
	 * 
	 * Return Value : List<DataSource>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataSource> getDataSourceForTrackerListBySiteId(int siteId) {
		return this.dataSourceDAO.getDataSourceForTrackerListBySiteId(siteId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataSourceForInvertersListByEquipmentId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all datasource for inverters by
	 * equipment id.
	 * 
	 * Input Params : equipmentId
	 * 
	 * Return Value : List<DataSource>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataSource> getDataSourceForInvertersListByEquipmentId(int equipmentId) {
		return this.dataSourceDAO.getDataSourceForInvertersListByEquipmentId(equipmentId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getDataSourceForSesnorListBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data source for sesnor by
	 * using site id.
	 * 
	 * Input Params : siteId
	 * 
	 * Return Value : List<DataSource>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<DataSource> getDataSourceForSesnorListBySiteId(int siteId) {
		return this.dataSourceDAO.getDataSourceForSesnorListBySiteId(siteId);
	}
}
