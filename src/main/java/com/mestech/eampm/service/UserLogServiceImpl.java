package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.UserLogDAO;
import com.mestech.eampm.model.JmrDetail;
import com.mestech.eampm.model.UserLog;

/******************************************************
 * 
 * Filename : UserLogServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class implements from userlogservice and its access the
 * 
 * information about userlogdao.
 * 
 * 
 *******************************************************/
@Service

public class UserLogServiceImpl implements UserLogService {

	@Autowired
	private UserLogDAO userlogDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setuserlogDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the user log dao.
	 * 
	 * Input Params :userlogDAO
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setuserlogDAO(UserLogDAO userlogDAO) {
		this.userlogDAO = userlogDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name : addUserlog
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function save the user log .
	 * 
	 * Input Params :userlogDAO
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addUserlog(UserLog userlog) {
		userlogDAO.addUserlog(userlog);
	}

	/********************************************************************************************
	 * 
	 * Function Name : updateUserlog
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the user log .
	 * 
	 * Input Params :userlog
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void updateUserlog(UserLog userlog) {
		userlogDAO.updateUserlog(userlog);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listUserlogs
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the user logs .
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<UserLog>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<UserLog> listUserlogs() {
		return this.userlogDAO.listUserlogs();
	}

	/********************************************************************************************
	 * 
	 * Function Name : listFieldUsers
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the field users.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<UserLog>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<UserLog> listFieldUsers() {
		return this.userlogDAO.listFieldUserlogs();
	}

	/********************************************************************************************
	 * 
	 * Function Name : removeUserlog
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the user log by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeUserlog(int id) {
		userlogDAO.removeUserlog(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listFieldUserlogs
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the field user logs.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<UserLog>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	public List<UserLog> listFieldUserlogs() {
		// TODO Auto-generated method stub
		return null;
	}

	/********************************************************************************************
	 * 
	 * Function Name :getUserLogById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get user log by using session id
	 * 
	 * Input Params :sessionId
	 * 
	 * Return Value :UserLog
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public UserLog getUserLogById(String sessionId) {
		return userlogDAO.getUserLogById(sessionId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateUserlogDetails
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the user log details
	 * 
	 * Input Params :SessionID, IpAddress
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public void updateUserlogDetails(String SessionID, String IpAddress) {
		userlogDAO.updateUserlogDetails(SessionID, IpAddress);
	}

}
