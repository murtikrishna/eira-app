package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.EquipmentTypeDAO;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentType;

/******************************************************
 * 
 * Filename :EquipmentTypeServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class implements from equipmenttypeservice and its access
 * 
 * the information about equipmentdao
 * 
 * 
 *******************************************************/
@Service

public class EquipmentTypeServiceImpl implements EquipmentTypeService {

	@Autowired
	private EquipmentTypeDAO equipmenttypeDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setequipmenttypeDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the equipment type dao .
	 * 
	 * Input Params :equipmenttypeDAO.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setequipmenttypeDAO(EquipmentTypeDAO equipmenttypeDAO) {
		this.equipmenttypeDAO = equipmenttypeDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addEquipmentType
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the equipment type.
	 * 
	 * Input Params :equipmenttype.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addEquipmentType(EquipmentType equipmenttype) {
		equipmenttypeDAO.addEquipmentType(equipmenttype);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateEquipmentType
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the equipment type.
	 * 
	 * Input Params :equipmenttype.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void updateEquipmentType(EquipmentType equipmenttype) {
		equipmenttypeDAO.updateEquipmentType(equipmenttype);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listEquipmentTypes
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the equipment types.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<EquipmentType>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<EquipmentType> listEquipmentTypes() {
		return this.equipmenttypeDAO.listEquipmentTypes();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEquipmentTypeById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the equipment types by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : EquipmentType
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public EquipmentType getEquipmentTypeById(int id) {
		return equipmenttypeDAO.getEquipmentTypeById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getEquipmentTypeByMax
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the equipment type by using maximum
	 * column name.
	 * 
	 * Input Params : MaxColumnName
	 * 
	 * Return Value : EquipmentType
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public EquipmentType getEquipmentTypeByMax(String MaxColumnName) {
		return equipmenttypeDAO.getEquipmentTypeByMax(MaxColumnName);
	}

	/********************************************************************************************
	 * 
	 * Function Name : removeEquipmentType
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the equipment type by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeEquipmentType(int id) {
		equipmenttypeDAO.removeEquipmentType(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listallequipments
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the equipment types by using
	 * equipment id.
	 * 
	 * Input Params : equipmentid
	 * 
	 * Return Value :List<EquipmentType>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<EquipmentType> listallequipments(int equipmentid) {
		return equipmenttypeDAO.listallequipments(equipmentid);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listFilterEquipmentsByTypeId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the filter equipment by using
	 * type id.
	 * 
	 * Input Params : typeId
	 * 
	 * Return Value :List<EquipmentType>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<EquipmentType> listFilterEquipmentsByTypeId(int typeId) {
		return equipmenttypeDAO.listFilterEquipmentsByTypeId(typeId);

	}

}
