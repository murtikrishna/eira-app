package com.mestech.eampm.service;

import java.math.BigInteger;
import java.util.List;

import com.mestech.eampm.model.MDatalogger;
import com.mestech.eampm.model.MDataloggerType;

public interface MDataloggerTypeService {
	public MDataloggerType save(MDataloggerType datalogger);

	public MDataloggerType update(MDataloggerType datalogger);

	public void delete(int id);

	public MDataloggerType edit(int id);

	public List<MDataloggerType> listAll();
	
	public List<MDatalogger> listAllDatalogger(int flag);
	
	public void addDatalogger(MDataloggerType datalogger);
	
	public void maddDatalogger(MDatalogger datalogger);
	
	public List<BigInteger> listDataloggerIdFromStandards();
}
