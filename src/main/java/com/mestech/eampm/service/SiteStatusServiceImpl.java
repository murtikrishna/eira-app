package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.dao.SiteStatusDAO;
import com.mestech.eampm.model.SiteStatus;

/******************************************************
 * 
 * Filename :SiteStatusServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class implements from sitestatusservice and ist access the
 * 
 * information about sitestatusdao.
 * 
 * 
 *******************************************************/
@Service
public class SiteStatusServiceImpl implements SiteStatusService {
	@Autowired
	private SiteStatusDAO siteStatusDAO;

	/********************************************************************************************
	 * 
	 * Function Name :listSiteStatus
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site status.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<SiteStatus>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<SiteStatus> listSiteStatus() {
		return this.siteStatusDAO.listSiteStatus();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getSiteStatusListByCustomerId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site status by using
	 * customer id.
	 * 
	 * Input Params : customerId
	 * 
	 * Return Value :List<SiteStatus>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<SiteStatus> getSiteStatusListByCustomerId(int customerId) {
		return siteStatusDAO.getSiteStatusListByCustomerId(customerId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getSiteStatusListByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site status by using user
	 * id.
	 * 
	 * Input Params : userId
	 * 
	 * Return Value :List<SiteStatus>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public List<SiteStatus> getSiteStatusListByUserId(int userId) {
		return siteStatusDAO.getSiteStatusListByUserId(userId);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getSiteStatusBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the site status by using site id.
	 * 
	 * Input Params : siteId
	 * 
	 * Return Value :SiteStatus
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public SiteStatus getSiteStatusBySiteId(int siteId) {
		return siteStatusDAO.getSiteStatusBySiteId(siteId);
	}

	@Override
	@Transactional
	public SiteStatus save(SiteStatus siteStatus) {

		return siteStatusDAO.save(siteStatus);
	}
}