package com.mestech.eampm.service;


import java.util.List;
 
import com.mestech.eampm.model.CustomerMap;

public interface CustomerMapService {
	 
	public void addCustomerMap(CustomerMap customermap);
	    
	public void updateCustomerMap(CustomerMap customermap);
	    
	public CustomerMap getCustomerMapById(int id);
	    
	public void removeCustomerMap(int id);
	    
	public List<CustomerMap> listCustomerMaps();
	

	public List<CustomerMap> listCustomerMaps(int mapid);
}
