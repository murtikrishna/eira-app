package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.dao.StandardOperatingDAO;
import com.mestech.eampm.model.StandardOperatingProcedure;
import com.mestech.eampm.utility.SOPRequest;

/******************************************************
 * 
 * Filename : StandardOperationServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from standardoperation service and its
 *
 * access the information about standardoperatingdao.
 * 
 * 
 *******************************************************/
@Service
public class StandardOperationServiceImpl implements StandardOperationService {
	@Autowired
	private StandardOperatingDAO standardOperatingDAO;

	/********************************************************************************************
	 * 
	 * Function Name :saveOrUpdate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save or update the sop request.
	 * 
	 * Input Params :sopRequest
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public void saveOrUpdate(SOPRequest sopRequest) {
		// TODO Auto-generated method stub
		standardOperatingDAO.saveOrUpdate(sopRequest);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getSop
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the sop by using category id and type
	 * id.
	 * 
	 * Input Params :categoryId, typeId
	 * 
	 * Return Value :List<StandardOperatingProcedure>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public List<StandardOperatingProcedure> getSop(String categoryId, String typeId) {
		// TODO Auto-generated method stub
		return standardOperatingDAO.getSop(categoryId, typeId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :delete
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the operating procedure.
	 * 
	 * Input Params :operatingProcedure
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public void delete(StandardOperatingProcedure operatingProcedure) {
		// TODO Auto-generated method stub
		standardOperatingDAO.delete(operatingProcedure);
	}

}
