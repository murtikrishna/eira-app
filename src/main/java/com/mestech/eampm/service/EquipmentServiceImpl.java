package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.bean.SiteCommonBean;
import com.mestech.eampm.bean.SiteViewBean;
import com.mestech.eampm.bean.StandardParameterBean;
import com.mestech.eampm.dao.EquipmentDAO;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentCategory;

/******************************************************
 * 
 * Filename :EquipmentServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from equipmentservice and its access
 * 
 * the information about equipmentdao.
 * 
 * 
 *******************************************************/
@Service

public class EquipmentServiceImpl implements EquipmentService {

	@Autowired
	private EquipmentDAO equipmentDAO;

	/********************************************************************************************
	 * 
	 * Function Name : setequipmentDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the equipment .
	 * 
	 * Input Params : equipmentDAO.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setequipmentDAO(EquipmentDAO equipmentDAO) {
		this.equipmentDAO = equipmentDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name : addEquipment
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the equipment.
	 * 
	 * Input Params : equipment.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void addEquipment(Equipment equipment) {
		equipmentDAO.addEquipment(equipment);
	}

	/********************************************************************************************
	 * 
	 * Function Name : updateEquipment
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the equipment.
	 * 
	 * Input Params : equipment.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateEquipment(Equipment equipment) {
		equipmentDAO.updateEquipment(equipment);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listEquipments
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the equipments.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<Equipment> listEquipments() {
		return this.equipmentDAO.listEquipments();
	}

	/********************************************************************************************
	 * 
	 * Function Name :listEquipmentsByIds
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the equipments by using Ids.
	 * 
	 * Input Params :Ids
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listEquipmentsByIds(String Ids) {
		return this.equipmentDAO.listEquipmentsByIds(Ids);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listEquipmentsBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the equipments by using
	 * siterId.
	 * 
	 * Input Params :siterId
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listEquipmentsBySiteId(int siterId) {
		return this.equipmentDAO.listEquipmentsBySiteId(siterId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listEquipmentsByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the equipments by using
	 * userId.
	 * 
	 * Input Params :userId
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listEquipmentsByUserId(int userId) {
		return this.equipmentDAO.listEquipmentsByUserId(userId);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getEquipmentById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the equipments by using Id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value : Equipment
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public Equipment getEquipmentById(int id) {
		return equipmentDAO.getEquipmentById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEquipmentByMax
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the equipments by using Maximum
	 * column name.
	 * 
	 * Input Params : MaxColumnName
	 * 
	 * Return Value : Equipment
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Transactional
	public Equipment getEquipmentByMax(String MaxColumnName) {
		return equipmentDAO.getEquipmentByMax(MaxColumnName);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeEquipment
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the equipments by using id.
	 * 
	 * Input Params : id.
	 * 
	 * Return Value : void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeEquipment(int id) {
		equipmentDAO.removeEquipment(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listInvertersByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the inverter by using userid.
	 * 
	 * Input Params : userId.
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listInvertersByUserId(int userId) {
		return this.equipmentDAO.listInvertersByUserId(userId);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listFilterInvertersByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the filterinverters by using
	 * userid.
	 * 
	 * Input Params : userId.
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listFilterInvertersByUserId(int userId) {
		return this.equipmentDAO.listFilterInvertersByUserId(userId);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listInverters
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the inverters .
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listInverters() {
		return this.equipmentDAO.listInverters();
	}

	/********************************************************************************************
	 * 
	 * Function Name : listEnergymeters
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the energymeters .
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listEnergymeters() {
		return this.equipmentDAO.listEnergymeters();
	}

	/********************************************************************************************
	 * 
	 * Function Name : listScbs
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the scbs .
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listScbs() {
		return this.equipmentDAO.listScbs();
	}

	/********************************************************************************************
	 * 
	 * Function Name : listInvertersBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the list inverters by using
	 * site id .
	 * 
	 * Input Params : siteId
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listInvertersBySiteId(int siteId) {
		return this.equipmentDAO.listInvertersBySiteId(siteId);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listFilterInvertersBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the list filter inverters by
	 * using site id .
	 * 
	 * Input Params : siteId
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listFilterInvertersBySiteId(int siteId) {
		return this.equipmentDAO.listFilterInvertersBySiteId(siteId);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listSensorsByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the list sensors by using user
	 * id .
	 * 
	 * Input Params : userId
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listSensorsByUserId(int userId) {
		return this.equipmentDAO.listInvertersByUserId(userId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listSensors
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the list sensors .
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listSensors() {
		return this.equipmentDAO.listInverters();
	}

	/********************************************************************************************
	 * 
	 * Function Name :listSensorsBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the list sensors by using site
	 * id.
	 * 
	 * Input Params : siteId
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listSensorsBySiteId(int siteId) {
		return this.equipmentDAO.listInvertersBySiteId(siteId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getStandardParameterByEquipmentIds
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the standard parameter by
	 * using equipment ids.
	 * 
	 * Input Params : equipmentIds,EquipmentsIDs
	 * 
	 * Return Value : List<StandardParameterBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<StandardParameterBean> getStandardParameterByEquipmentIds(String equipmentIds, String EquipmentsIDs) {
		return this.equipmentDAO.getStandardParameterByEquipmentIds(equipmentIds, EquipmentsIDs);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getStandardParameterBySiteIds
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the standard parameter by
	 * using site ids.
	 * 
	 * Input Params : siteIds
	 * 
	 * Return Value : List<StandardParameterBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<StandardParameterBean> getStandardParameterBySiteIds(String siteIds) {
		return this.equipmentDAO.getStandardParameterBySiteIds(siteIds);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listallequipments
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the equipments by using
	 * equipment id.
	 * 
	 * Input Params : equipmentid
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listallequipments(int equipmentid) {
		return equipmentDAO.listallequipments(equipmentid);

	}

	/********************************************************************************************
	 * 
	 * Function Name :listConfigEquipments
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the list config equipments .
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listConfigEquipments() {
		return this.equipmentDAO.listConfigEquipments();
	}

	/********************************************************************************************
	 * 
	 * Function Name :listEnergymeterByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the list energy meter by using
	 * user id .
	 * 
	 * Input Params : userId
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listEnergymeterByUserId(int userId) {
		return this.equipmentDAO.listEnergymeterByUserId(userId);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listScbByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the list scb by using user id
	 * .
	 * 
	 * Input Params : userId
	 * 
	 * Return Value :List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listScbByUserId(int userId) {
		return this.equipmentDAO.listScbByUserId(userId);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listInverterDetails
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the list inverter details by
	 * using site id .
	 * 
	 * Input Params : siteid
	 * 
	 * Return Value :List<SiteCommonBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<SiteCommonBean> listInverterDetails(int siteid) {
		return this.equipmentDAO.listInverterDetails(siteid);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listEquipmentComunicationCount
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the list eqiupment
	 * communication count by using site id .
	 * 
	 * Input Params : siteid
	 * 
	 * Return Value :List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listEquipmentComunicationCount(int siteid) {
		return this.equipmentDAO.listEquipmentComunicationCount(siteid);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listTrackers
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the list tracker count by
	 * using site id .
	 * 
	 * Input Params : siteid
	 * 
	 * Return Value :List<Equipment>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Equipment> listTrackers(int siteid) {
		return this.equipmentDAO.listTrackers(siteid);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getCategoryByEquipmentIds
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the category by using
	 * equipment ids .
	 * 
	 * Input Params : equipmentIds
	 * 
	 * Return Value :List<EquipmentCategory>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<EquipmentCategory> getCategoryByEquipmentIds(int equipmentIds) {
		return this.equipmentDAO.getCategoryByEquipmentIds(equipmentIds);
	}
}
