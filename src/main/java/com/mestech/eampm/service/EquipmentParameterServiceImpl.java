package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.EquipmentParameterDAO;
import com.mestech.eampm.model.EquipmentParameter;

/******************************************************
 * 
 * Filename :EquipmentParameterServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from equipmentparameterservice and its
 * 
 * access the information about equipmentdao.
 * 
 * 
 *******************************************************/
@Service

public class EquipmentParameterServiceImpl implements EquipmentParameterService {

	@Autowired
	private EquipmentParameterDAO equipmentparameterDAO;

	/********************************************************************************************
	 * 
	 * Function Name : setequipmentparameterDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the equipment parameter dao.
	 * 
	 * Input Params : equipmentparameterDAO
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setequipmentparameterDAO(EquipmentParameterDAO equipmentparameterDAO) {
		this.equipmentparameterDAO = equipmentparameterDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name : addEquipmentParameter
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the equipment parameter.
	 * 
	 * Input Params : equipmentparameter
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addEquipmentParameter(EquipmentParameter equipmentparameter) {
		equipmentparameterDAO.addEquipmentParameter(equipmentparameter);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateEquipmentParameter
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the equipment parameter.
	 * 
	 * Input Params : equipmentparameter
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateEquipmentParameter(EquipmentParameter equipmentparameter) {
		equipmentparameterDAO.updateEquipmentParameter(equipmentparameter);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listEquipmentParameters
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the equipment parameter.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<EquipmentParameter>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<EquipmentParameter> listEquipmentParameters() {
		return this.equipmentparameterDAO.listEquipmentParameters();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getEquipmentParameterById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the equipment parameter by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : EquipmentParameter
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public EquipmentParameter getEquipmentParameterById(int id) {
		return equipmentparameterDAO.getEquipmentParameterById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeEquipmentParameter
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the equipment parameter by using
	 * id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeEquipmentParameter(int id) {
		equipmentparameterDAO.removeEquipmentParameter(id);
	}

}
