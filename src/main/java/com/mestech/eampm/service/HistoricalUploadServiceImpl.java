package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.dao.HistoricalUploadDAO;
import com.mestech.eampm.model.HistoricalUpload;

/******************************************************
 * 
 * Filename : HistoricalUploadServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from historicaluploadservice and its
 * 
 * access the information about historicaldao.
 * 
 * 
 *******************************************************/
@Service
public class HistoricalUploadServiceImpl implements HistoricalUploadService {
	@Autowired
	private HistoricalUploadDAO historicalUploadDAO;

	/********************************************************************************************
	 * 
	 * Function Name :save
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to upload in historical upload.
	 * 
	 * Input Params :historicalUpload.
	 * 
	 * Return Value : HistoricalUpload
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public HistoricalUpload save(HistoricalUpload historicalUpload) {
		// TODO Auto-generated method stub
		return historicalUploadDAO.save(historicalUpload);
	}

	/********************************************************************************************
	 * 
	 * Function Name : list
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function select all the historical uploads.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<HistoricalUpload>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public List<HistoricalUpload> list() {
		// TODO Auto-generated method stub
		return historicalUploadDAO.list();
	}

	/********************************************************************************************
	 * 
	 * Function Name : getHistory
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function select all the history by using site code.
	 * 
	 * Input Params : siteCode, directory
	 * 
	 * Return Value : List<HistoricalUpload>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public List<HistoricalUpload> getHistory(String siteCode, String directory) {
		// TODO Auto-generated method stub
		return historicalUploadDAO.getHistory(siteCode, directory);
	}

}
