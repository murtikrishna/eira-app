
package com.mestech.eampm.service;

import java.util.List;

import com.mestech.eampm.model.RoleActivity;

public interface RoleActivityService {

	public void addRoleActivity(RoleActivity roleactivity);

	public void updateRoleActivity(RoleActivity roleactivity);

	public RoleActivity getRoleActivityById(int id);

	public void removeRoleActivity(int id);

	public List<RoleActivity> listRoleActivities();

	public List<RoleActivity> getRoleActivityByRoleId(int roleId);

	public void deleteRoleActivity(RoleActivity roleActivity);
}
