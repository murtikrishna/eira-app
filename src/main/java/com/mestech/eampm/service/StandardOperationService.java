package com.mestech.eampm.service;

import java.util.List;

import com.mestech.eampm.model.StandardOperatingProcedure;
import com.mestech.eampm.utility.SOPRequest;

public interface StandardOperationService {

	public void saveOrUpdate(SOPRequest sopRequest);

	public List<StandardOperatingProcedure> getSop(String categoryId, String typeId);

	public void delete(StandardOperatingProcedure operatingProcedure);
}
