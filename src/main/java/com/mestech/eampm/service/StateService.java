
package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.State;

public interface StateService {

	public void addState(State state);
    
    public void updateState(State state);
    
    public State getStateById(int id);
    
    public void removeState(int id);
    
    public List<State> listStates();	
    
    public List<State> listAllStatesByCountryID(int id);
}

