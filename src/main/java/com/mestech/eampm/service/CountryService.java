package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.Country;

public interface CountryService {

	public void addCountry(Country country);
    
    public void updateCountry(Country country);
    
    public Country getCountryById(int id);
    
    public void removeCountry(int id);
    
    public List<Country> listCountries();	
}
