package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.ParameterList;

public interface ParameterListService {

	public void addParameterList(ParameterList parameterlist);
    
    public void updateParameterList(ParameterList parameterlist);
    
    public ParameterList getParameterListById(int id);
    
    public void removeParameterList(int id);
    
    public List<ParameterList> listParameterLists();	
}

