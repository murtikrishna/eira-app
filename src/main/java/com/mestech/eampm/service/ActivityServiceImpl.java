
/******************************************************
 * 
 *    	Filename	: ActivityServiceImpl
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 25-Apr-19 01:44 PM 
 *      
 *      Description : this service user for crud operation.
 *      
 *      
 *******************************************************/

package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.ActivityDAO;
import com.mestech.eampm.model.Activity;

/******************************************************
 * 
 * Filename : ActivityServiceImpl
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description : this class is implements from activityservice and its access
 * 
 * the information about activitydao.
 * 
 * 
 *******************************************************/

@Service

public class ActivityServiceImpl implements ActivityService {

	@Autowired
	private ActivityDAO activityDAO;

	/********************************************************************************************
	 * 
	 * Function Name : setactivityDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this method used to set the activity dao.
	 * 
	 * Input Params : activityDAO
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	public void setactivityDAO(ActivityDAO activityDAO) {
		this.activityDAO = activityDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name : addActivity
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this method used to save the activitydao.
	 * 
	 * Input Params : activity
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void addActivity(Activity activity) {
		activityDAO.addActivity(activity);
	}

	/********************************************************************************************
	 * 
	 * Function Name : updateActivity
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this method used to update the activitydao.
	 * 
	 * Input Params : activity
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void updateActivity(Activity activity) {
		activityDAO.updateActivity(activity);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listActivities
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this method used to select all the activities.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<Activity>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Activity> listActivities() {
		return this.activityDAO.listActivities();
	}

	/********************************************************************************************
	 * 
	 * Function Name : getActivityById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this method used to get activity by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : Activity
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public Activity getActivityById(int id) {
		return activityDAO.getActivityById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name : removeActivity
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this method used to remove the activity by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeActivity(int id) {
		activityDAO.removeActivity(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getname
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this method used to get the name.
	 * 
	 * Input Params :
	 * 
	 * Return Value : String
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	public String getname() {
		// TODO Auto-generated method stub
		return "sarath";
	}

	/********************************************************************************************
	 * 
	 * Function Name : getActivityName
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this method used to get activity name by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : Activity
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public Activity getActivityName(int Id) {
		return activityDAO.getActivityName(Id);
	}

}
