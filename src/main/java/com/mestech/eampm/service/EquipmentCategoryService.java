package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.EquipmentCategory;

public interface EquipmentCategoryService {

	public void addEquipmentCategory(EquipmentCategory equipmentcategory);
    
    public void updateEquipmentCategory(EquipmentCategory equipmentcategory);
    
    public EquipmentCategory getEquipmentCategoryById(int id);
    
    public void removeEquipmentCategory(int id);
    
    public List<EquipmentCategory> listEquipmentCategories();
    
    public EquipmentCategory getEquipmentCategoryByMax(String MaxColumnName);
    
    public List<EquipmentCategory> listEquipmentCategorys(int categoryid);
}
