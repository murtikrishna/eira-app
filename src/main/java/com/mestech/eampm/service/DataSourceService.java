package com.mestech.eampm.service;


import java.util.List;

import com.mestech.eampm.model.DataSource;

public interface DataSourceService {

	public void addDataSource(DataSource dataSource);
    
    public void updateDataSource(DataSource dataSource);
    
    public DataSource getDataSourceById(int id);

    public DataSource getDataSourceByMax(String MaxColumnName);
    
    public void removeDataSource(int id);
    
    public List<DataSource> listDataSources();	
    
	public List<DataSource> getDataSourceListByUserId(int userId);
	
	public List<DataSource> getDataSourceListByCustomerId(int customerId);
	
	public List<DataSource> getDataSourceListBySiteId(int siteId);

	public List<DataSource> getDataSourceListByEquipmentId(int equipmentId);
	
	

	public List<DataSource> listDataSourceForInverters();

	public List<DataSource> getDataSourceForInvertersListByUserId(int userId);

	public List<DataSource> getDataSourceForInvertersListByCustomerId(int customerId);

	public List<DataSource> getDataSourceForInvertersListBySiteId(int siteId);

	public List<DataSource> getDataSourceForInvertersListByEquipmentId(int equipmentId);
	
	
	public List<DataSource> getDataSourceForEnegrymetersListBySiteId(int siteId);
	
	public List<DataSource> getDataSourceForScbListBySiteId(int siteId);
	
	public List<DataSource> getDataSourceForTrackerListBySiteId(int siteId);
	
	public List<DataSource> getDataSourceForSesnorListBySiteId(int siteId);


}
