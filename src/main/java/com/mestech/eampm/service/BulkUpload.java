package com.mestech.eampm.service;

import java.util.List;

public interface BulkUpload {

	public <E> boolean upload(List<E> element);

}
