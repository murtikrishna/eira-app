package com.mestech.eampm.service;

import java.util.List;

import com.mestech.eampm.bean.SiteSummaryBean;
import com.mestech.eampm.model.SiteSummary;

public interface SiteSummaryService {

	public void addSiteSummary(SiteSummary SiteSummary);
    
	public void updateSiteSummary(SiteSummary SiteSummary);
		  
	public SiteSummary getSiteSummaryById(int id);

    public SiteSummary getSiteSummaryByMax(String MaxColumnName);
	    
	public void removeSiteSummary(int id);
	    
	public List<SiteSummary> listSiteSummarys();

	public List<SiteSummary> listSiteSummarysByFilter1(int equipmentId,int siteId, int lastNDays );
	
	public List<SiteSummary> getSiteSummaryListByUserId(int userId);
	
	public List<SiteSummary> getSiteSummaryListByCustomerId(int customerId);
	
	public List<SiteSummary> getSiteSummaryListBySiteId(int siteId);

	//public List<SiteSummaryBean> getSiteSummaryListBySiteIdsGroup(String siteIds);

	public List<SiteSummary> getSiteSummaryListBySiteIdsGroup(String siteIds);

	public List<SiteSummary> getSiteSummaryListByEquipmentId(int equipmentId);
	
    public List<SiteSummary> getSiteSummaryListBySiteIdWithDate(String siteId, String FromDate, String ToDate);
	
	public List<SiteSummary> getSiteSummaryListBySiteIdsGroupWithDate(String siteIds, String FromDate, String ToDate);

}
