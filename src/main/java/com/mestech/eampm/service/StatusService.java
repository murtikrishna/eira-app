

package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.Status;

public interface StatusService {

	public void addStatus(Status status);
    
    public void updateStatus(Status status);
    
    public Status getStatusById(Integer id);
    
    public void removeStatus(int id);
    
    public List<Status> listStatuses();	
}

