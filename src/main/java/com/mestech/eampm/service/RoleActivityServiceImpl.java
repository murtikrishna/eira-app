
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.RoleActivityDAO;
import com.mestech.eampm.model.RoleActivity;

/******************************************************
 * 
 * Filename :RoleActivityServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from roleactivityservice and its access
 * 
 * the information about roleactivitydao.
 * 
 * 
 *******************************************************/
@Service

public class RoleActivityServiceImpl implements RoleActivityService {

	@Autowired
	private RoleActivityDAO roleactivityDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setroleactivityDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set role activity dao.
	 * 
	 * Input Params :roleactivityDAO.
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setroleactivityDAO(RoleActivityDAO roleactivityDAO) {
		this.roleactivityDAO = roleactivityDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addRoleActivity
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save role activity.
	 * 
	 * Input Params :roleactivity.
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addRoleActivity(RoleActivity roleactivity) {
		roleactivityDAO.addRoleActivity(roleactivity);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateRoleActivity
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update role activity.
	 * 
	 * Input Params :roleactivity.
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateRoleActivity(RoleActivity roleactivity) {
		roleactivityDAO.updateRoleActivity(roleactivity);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listRoleActivities
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the role activities.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<RoleActivity>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<RoleActivity> listRoleActivities() {
		return this.roleactivityDAO.listRoleActivities();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getRoleActivityById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get role activity by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value :RoleActivity.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public RoleActivity getRoleActivityById(int id) {
		return roleactivityDAO.getRoleActivityById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeRoleActivity
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the role activity by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeRoleActivity(int id) {
		roleactivityDAO.removeRoleActivity(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getRoleActivityByRoleId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the role activity by using role id.
	 * 
	 * Input Params :roleId
	 * 
	 * Return Value :List<RoleActivity>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	@Transactional
	public List<RoleActivity> getRoleActivityByRoleId(int roleId) {
		// TODO Auto-generated method stub
		return roleactivityDAO.getRoleActivityByRoleId(roleId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :deleteRoleActivity
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the role activity.
	 * 
	 * Input Params :roleActivity
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	@Transactional
	public void deleteRoleActivity(RoleActivity roleActivity) {
		// TODO Auto-generated method stub
		roleactivityDAO.deleteRoleActivity(roleActivity);
	}

}
