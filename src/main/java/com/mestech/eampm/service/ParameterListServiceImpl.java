
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.ParameterListDAO;
import com.mestech.eampm.model.ParameterList;

/******************************************************
 * 
 * Filename :ParameterListServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from parameterlistservice and its
 * 
 * access the information about parameterlistdao
 * 
 * 
 *******************************************************/
@Service

public class ParameterListServiceImpl implements ParameterListService {

	@Autowired
	private ParameterListDAO parameterlistDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setparameterlistDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the parameter list dao.
	 * 
	 * Input Params :parameterlistDAO.
	 * 
	 * Return Value : void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	public void setparameterlistDAO(ParameterListDAO parameterlistDAO) {
		this.parameterlistDAO = parameterlistDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addParameterList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the parameter list.
	 * 
	 * Input Params :parameterlist.
	 * 
	 * Return Value : void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void addParameterList(ParameterList parameterlist) {
		parameterlistDAO.addParameterList(parameterlist);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateParameterList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the parameter list.
	 * 
	 * Input Params :parameterlist.
	 * 
	 * Return Value : void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateParameterList(ParameterList parameterlist) {
		parameterlistDAO.updateParameterList(parameterlist);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listParameterLists
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the parameter lists.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<ParameterList>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<ParameterList> listParameterLists() {
		return this.parameterlistDAO.listParameterLists();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getParameterListById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the parameter list by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : ParameterList.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public ParameterList getParameterListById(int id) {
		return parameterlistDAO.getParameterListById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeParameterList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the parameter lists.
	 * 
	 * Input Params : id.
	 * 
	 * Return Value : void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeParameterList(int id) {
		parameterlistDAO.removeParameterList(id);
	}

}
