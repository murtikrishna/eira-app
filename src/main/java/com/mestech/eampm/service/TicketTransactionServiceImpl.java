
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.TicketTransactionDAO;
import com.mestech.eampm.model.TicketTransaction;

/******************************************************
 * 
 * Filename :TicketTransactionServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class implements from tickettransectionservice and its
 * 
 * access the information about tickettransectiondao.
 * 
 * 
 *******************************************************/
@Service

public class TicketTransactionServiceImpl implements TicketTransactionService {

	@Autowired
	private TicketTransactionDAO ticketTransactionDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setticketTransactionDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the ticket transaction dao.
	 * 
	 * Input Params :ticketDetail
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setticketTransactionDAO(TicketTransactionDAO ticketTransactionDAO) {
		this.ticketTransactionDAO = ticketTransactionDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addTicketTransaction
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the ticket transection.
	 * 
	 * Input Params :ticketTransaction
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void addTicketTransaction(TicketTransaction ticketTransaction) {
		ticketTransactionDAO.addTicketTransaction(ticketTransaction);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateTicketTransaction
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the ticket transaction.
	 * 
	 * Input Params :ticketTransaction
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void updateTicketTransaction(TicketTransaction ticketTransaction) {
		ticketTransactionDAO.updateTicketTransaction(ticketTransaction);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeTicketTransaction
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the ticket transaction by using
	 * id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeTicketTransaction(int id) {
		ticketTransactionDAO.removeTicketTransaction(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listTicketTransactions
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the ticket transactions.
	 * 
	 * Input Params :TimezoneOffset
	 * 
	 * Return Value :List<TicketTransaction>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketTransaction> listTicketTransactions(String TimezoneOffset) {
		return this.ticketTransactionDAO.listTicketTransactions(TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTicketTransactionByMax
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get ticket transaction by using maximum
	 * column name.
	 * 
	 * Input Params :MaxColumnName, TimezoneOffset
	 * 
	 * Return Value :TicketTransaction
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public TicketTransaction getTicketTransactionByMax(String MaxColumnName, String TimezoneOffset) {
		return ticketTransactionDAO.getTicketTransactionByMax(MaxColumnName, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTicketTransactionById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get ticket transaction by using id.
	 * 
	 * Input Params :id, TimezoneOffset
	 * 
	 * Return Value :TicketTransaction
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public TicketTransaction getTicketTransactionById(int id, String TimezoneOffset) {
		return ticketTransactionDAO.getTicketTransactionById(id, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTicketTransactionListByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get ticket transaction by using user id.
	 * 
	 * Input Params :userId, TimezoneOffset
	 * 
	 * Return Value :List<TicketTransaction>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketTransaction> getTicketTransactionListByUserId(int userId, String TimezoneOffset) {
		return this.ticketTransactionDAO.getTicketTransactionListByUserId(userId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTicketTransactionListBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get ticket transaction by using site id.
	 * 
	 * Input Params : siteId, TimezoneOffset
	 * 
	 * Return Value :List<TicketTransaction>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TicketTransaction> getTicketTransactionListBySiteId(int siteId, String TimezoneOffset) {
		return this.ticketTransactionDAO.getTicketTransactionListBySiteId(siteId, TimezoneOffset);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getTicketTransactionListByTicketId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get ticket transaction by using ticket
	 * id.
	 * 
	 * Input Params : ticketId, TimezoneOffset
	 * 
	 * Return Value :List<TicketTransaction>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<TicketTransaction> getTicketTransactionListByTicketId(int ticketId, String TimezoneOffset) {
		return this.ticketTransactionDAO.getTicketTransactionListByTicketId(ticketId, TimezoneOffset);
	}

}
