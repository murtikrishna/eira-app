package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.CustomerMapDAO;
import com.mestech.eampm.dao.SiteMapDAO;
import com.mestech.eampm.model.CustomerMap;
import com.mestech.eampm.model.SiteMap;
import com.mestech.eampm.utility.Response;
import com.mestech.eampm.utility.SiteMapPojo;

/******************************************************
 * 
 * Filename :SiteMapServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from sitemapservice and its access the
 * 
 * information about sitemapdao.
 * 
 * 
 *******************************************************/
@Service

public class SiteMapServiceImpl implements SiteMapService {

	@Autowired
	private SiteMapDAO sitemapDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setsitemapDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the site map dao.
	 * 
	 * Input Params :sitemapDAO.
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setsitemapDAO(SiteMapDAO sitemapDAO) {
		this.sitemapDAO = sitemapDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addSiteMap
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the site map.
	 * 
	 * Input Params :sitemap.
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addSiteMap(SiteMap sitemap) {
		sitemapDAO.addSiteMap(sitemap);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateSiteMap
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the site map.
	 * 
	 * Input Params :sitemap.
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateSiteMap(SiteMap sitemap) {
		sitemapDAO.updateSiteMap(sitemap);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listSiteMaps
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site maps.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<SiteMap>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<SiteMap> listSiteMaps() {
		return this.sitemapDAO.listSiteMaps();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getSiteMapById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the site map by using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value :SiteMap.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public SiteMap getSiteMapById(int id) {
		return sitemapDAO.getSiteMapById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeSiteMap
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the site map by using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeSiteMap(int id) {
		sitemapDAO.removeSiteMap(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listSiteMaps
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site maps by using map id.
	 * 
	 * Input Params :mapid
	 * 
	 * Return Value :List<SiteMap>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<SiteMap> listSiteMaps(int mapid) {
		return this.sitemapDAO.listSiteMaps(mapid);
	}

	/********************************************************************************************
	 * 
	 * Function Name : updateSiteMapping
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the site map.
	 * 
	 * Input Params :siteMap
	 * 
	 * Return Value :Response.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public Response updateSiteMapping(SiteMapPojo siteMap) {

		return sitemapDAO.updateSiteMapping(siteMap);
	}

	/********************************************************************************************
	 * 
	 * Function Name : getSiteMapping
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the site map.
	 * 
	 * Input Params :
	 * 
	 * Return Value :Response.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public Response getSiteMapping() {
		// TODO Auto-generated method stub
		return sitemapDAO.getSiteMapping();
	}
}
