
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.TimeZoneDAO;
import com.mestech.eampm.model.TimeZone;

/******************************************************
 * 
 * Filename :TimeZoneServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class implements from timezoneservice and its access the
 * 
 * information about timezonedao.
 * 
 * 
 *******************************************************/
@Service

public class TimeZoneServiceImpl implements TimeZoneService {

	@Autowired
	private TimeZoneDAO timezoneDAO;

	/********************************************************************************************
	 * 
	 * Function Name :settimezoneDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the time zone dao.
	 * 
	 * Input Params :timezoneDAO
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void settimezoneDAO(TimeZoneDAO timezoneDAO) {
		this.timezoneDAO = timezoneDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addTimeZone
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the timezone.
	 * 
	 * Input Params :timezone
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addTimeZone(TimeZone timezone) {
		timezoneDAO.addTimeZone(timezone);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateTimeZone
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the timezone.
	 * 
	 * Input Params :timezone
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateTimeZone(TimeZone timezone) {
		timezoneDAO.updateTimeZone(timezone);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listTimeZones
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the time zone.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<TimeZone>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<TimeZone> listTimeZones() {
		return this.timezoneDAO.listTimeZones();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getTimeZoneById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the time zone by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value :TimeZone
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public TimeZone getTimeZoneById(int id) {
		return timezoneDAO.getTimeZoneById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeTimeZone
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the time zone by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeTimeZone(int id) {
		timezoneDAO.removeTimeZone(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :findByUTCANDName
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to find the time zone by using UTC and name.
	 * 
	 * Input Params : utc, name
	 * 
	 * Return Value :TimeZone
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public TimeZone findByUTCANDName(String utc, String name) {
		// TODO Auto-generated method stub
		return timezoneDAO.findByUTCANDName(utc, name);
	}

}
