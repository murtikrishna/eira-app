package com.mestech.eampm.service;


import java.util.List;

import com.mestech.eampm.model.JmrDetail;

public interface JmrDetailService {

	 
		public void addJmrDetail(JmrDetail jmrdetail);
		    
		public void updateJmrDetail(JmrDetail jmrdetail);
		    
		public JmrDetail getJmrDetailById(int id);
		
		public JmrDetail getJmrDetailByticketId(int ticketid);
		    
		public void removeJmrDetail(int id);
		    
		public List<JmrDetail> listJmrDetails();
		
		public JmrDetail getticketID(Integer ticketid);
}
