
package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.UnitOfMeasurement;

public interface UnitOfMeasurementService {

	public void addUnitOfMeasurement(UnitOfMeasurement unitofmeasurement);
    
    public void updateUnitOfMeasurement(UnitOfMeasurement unitofmeasurement);
    
    public UnitOfMeasurement getUnitOfMeasurementById(int id);
    
    public void removeUnitOfMeasurement(int id);
    
    public List<UnitOfMeasurement> listUnitOfMeasurements();	
}
