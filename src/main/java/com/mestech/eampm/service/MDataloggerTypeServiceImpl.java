package com.mestech.eampm.service;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.dao.MDataloggerTypeDAO;
import com.mestech.eampm.model.MDatalogger;
import com.mestech.eampm.model.MDataloggerType;

/******************************************************
 * 
 * Filename :MDataloggerTypeServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from mdataloggertypeservice and its
 *
 * access the information about mdataloggertypedao
 * 
 * 
 *******************************************************/
@Service
public class MDataloggerTypeServiceImpl implements MDataloggerTypeService {
	@Autowired
	private MDataloggerTypeDAO mDataloggerTypeDAO;

	/********************************************************************************************
	 * 
	 * Function Name :save
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the mdata logger type.
	 * 
	 * Input Params : mDataloggerType.
	 * 
	 * Return Value : MDataloggerType
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	@Transactional
	public MDataloggerType save(MDataloggerType mDataloggerType) {
		// TODO Auto-generated method stub
		return mDataloggerTypeDAO.save(mDataloggerType);
	}

	/********************************************************************************************
	 * 
	 * Function Name :update
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the data logger.
	 * 
	 * Input Params :datalogger.
	 * 
	 * Return Value : MDataloggerType
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public MDataloggerType update(MDataloggerType datalogger) {
		// TODO Auto-generated method stub
		return mDataloggerTypeDAO.update(datalogger);
	}

	/********************************************************************************************
	 * 
	 * Function Name :delete
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the mdata logger by using id.
	 * 
	 * Input Params :id.
	 * 
	 * Return Value : void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public void delete(int id) {
		mDataloggerTypeDAO.delete(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :edit
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the mdata logger type by using id.
	 * 
	 * Input Params :id.
	 * 
	 * Return Value :MDataloggerType.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public MDataloggerType edit(int id) {
		// TODO Auto-generated method stub
		return mDataloggerTypeDAO.edit(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listAll
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the mdata logger type.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<MDataloggerType>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public List<MDataloggerType> listAll() {
		// TODO Auto-generated method stub
		return mDataloggerTypeDAO.listAll();
	}

	/********************************************************************************************
	 * 
	 * Function Name : listAllDatalogger
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the mdata logger by using
	 * flag.
	 * 
	 * Input Params : flag
	 * 
	 * Return Value :List<MDatalogger>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public List<MDatalogger> listAllDatalogger(int flag) {
		return mDataloggerTypeDAO.listAllDatalogger(flag);
	}

	/********************************************************************************************
	 * 
	 * Function Name :addDatalogger
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the data logger.
	 * 
	 * Input Params : flag
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public void addDatalogger(MDataloggerType datalogger) {
		mDataloggerTypeDAO.addDatalogger(datalogger);
	}

	/********************************************************************************************
	 * 
	 * Function Name :maddDatalogger
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the mdata logger.
	 * 
	 * Input Params : datalogger.
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public void maddDatalogger(MDatalogger datalogger) {
		mDataloggerTypeDAO.maddDatalogger(datalogger);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listDataloggerIdFromStandards
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the data logger id.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<BigInteger>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	@Transactional
	public List<BigInteger> listDataloggerIdFromStandards() {
		return mDataloggerTypeDAO.listDataloggerIdFromStandards();
	}

}
