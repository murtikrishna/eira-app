package com.mestech.eampm.service;

import java.util.List;

import com.mestech.eampm.model.SiteStatus;

public interface SiteStatusService {

	public List<SiteStatus> listSiteStatus();

	public List<SiteStatus> getSiteStatusListByCustomerId(int customerId);

	public List<SiteStatus> getSiteStatusListByUserId(int userId);

	public SiteStatus getSiteStatusBySiteId(int siteId);

	public SiteStatus save(SiteStatus siteStatus);
}
