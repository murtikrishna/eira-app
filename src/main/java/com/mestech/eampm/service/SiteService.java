package com.mestech.eampm.service;

import java.util.List;

import com.mestech.eampm.bean.AnnualYeild;
import com.mestech.eampm.bean.DashboardEnergySummaryBean;
import com.mestech.eampm.bean.DashboardOandMBean;
import com.mestech.eampm.bean.DashboardSiteBean;
import com.mestech.eampm.bean.DashboardSiteListBean;
import com.mestech.eampm.model.Site;

public interface SiteService {

	public Site addSite(Site site);

	public void updateSite(Site site);

	public Site getSiteById(int id);

	public Site getSiteByName(String siteName);

	public List<Site> getSiteListByCustomerId(int customerId);

	public List<Site> getSiteListByUserId(int userId);

	public List<Site> listSiteById(int id);

	public Site getSiteByMax(String MaxColumnName);

	public void removeSite(int id);

	public List<Site> listSites();

	public List<DashboardSiteBean> getDashboardSiteDetails(String UserID, String TimezoneOffset);

	public List<DashboardEnergySummaryBean> getDashboardEnergySummaryDetails(String UserID, String TimezoneOffset);

	public List<DashboardOandMBean> getDashboardEventAndTicketDetails(String UserID, String TimezoneOffset);

	public List<DashboardSiteListBean> getDashboardSiteList(String UserID, String TimezoneOffset);

	public List<DashboardSiteBean> getDashboardSiteDetailsByCustomer(String CustomerID, String TimezoneOffset);

	public List<DashboardEnergySummaryBean> getDashboardEnergySummaryDetailsByCustomer(String CustomerID,
			String TimezoneOffset);

	public List<DashboardOandMBean> getDashboardEventAndTicketDetailsByCustomer(String CustomerID,
			String TimezoneOffset);

	public List<DashboardSiteListBean> getDashboardSiteListByCustomer(String CustomerID, String TimezoneOffset);

	public List<Site> getProductionSiteListByUserId(int userId);

	public List<Site> listallsites(int siteid);

	public List<Site> listConfigSites();

	public List<AnnualYeild> getAnnualyeildbySite(int siteid);

	public void updateSiteConfig(Site site);

	public List<Site> listSitesAgainstCollection();
}
