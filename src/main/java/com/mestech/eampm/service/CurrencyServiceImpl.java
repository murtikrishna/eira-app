package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.CurrencyDAO;
import com.mestech.eampm.model.Currency;

/******************************************************
 * 
 * Filename : CurrencyServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description : this class is implements from currencyservice and its access
 * 
 * the information about currencydao.
 * 
 * 
 *******************************************************/
@Service

public class CurrencyServiceImpl implements CurrencyService {

	@Autowired
	private CurrencyDAO currencyDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setcurrencyDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the currencydao.
	 * 
	 * Input Params : currencyDAO
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	public void setcurrencyDAO(CurrencyDAO currencyDAO) {
		this.currencyDAO = currencyDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addCurrency
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save currency.
	 * 
	 * Input Params : currency
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void addCurrency(Currency currency) {
		currencyDAO.addCurrency(currency);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateCurrency
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update currency
	 * 
	 * Input Params : currency
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void updateCurrency(Currency currency) {
		currencyDAO.updateCurrency(currency);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listCurrencies
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the currencies.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<Currency>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<Currency> listCurrencies() {
		return this.currencyDAO.listCurrencies();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getCurrencyById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get currency by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : Currency
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public Currency getCurrencyById(int id) {
		return currencyDAO.getCurrencyById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeCurrency
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove currency.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeCurrency(int id) {
		currencyDAO.removeCurrency(id);
	}

}
