package com.mestech.eampm.service;


import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.bean.OpenStateTicketsCount;
import com.mestech.eampm.model.TicketDetail;

public interface TicketDetailService {

	public void addTicketDetail(TicketDetail ticketDetail);	 
	public void updateTicketDetail(TicketDetail ticketdetail);	
	public void removeTicketDetail(int id);	    
	
	public TicketDetail getTicketDetailById(int id,String TimezoneOffset);
	public TicketDetail getTicketDetailByIdOnUTC(int id,String TimezoneOffset);
	public TicketDetail getTicketDetailByTicketCode(String TicketCode,String TimezoneOffset);   
	public TicketDetail getTicketDetailByMax(String MaxColumnName,String TimezoneOffset);
	
	public List<TicketDetail> getTicketDetailListByUserId(int userId,String TimezoneOffset);	
	public List<TicketDetail> getReportTicketDetailListByUserId(int userId,String TimezoneOffset);
	public List<TicketDetail> getTicketDetailListBySiteId(int siteId,String TimezoneOffset);	
	public List<TicketDetail> getTicketDetailListByCustomerId(int customerId,String TimezoneOffset);	
	public List<TicketDetail> getTicketDetailListByUserIdLimit(int userId, String TimezoneOffset);
	public List<TicketDetail> getTicketDetailListBySiteLimit(int siteid, String TimezoneOffset);	
	public List<TicketDetail> getTicketDetailListByCustomerLimit(int customerid, String TimezoneOffset);	    
	public List<TicketDetail> listTicketDetails(String TimezoneOffset);		
	public List<TicketDetail> listTicketDetailsForDisplay(int userid,String siteId, String fromDate,String toDate,String category, String type, String priority,String TimezoneOffset );
	public List<TicketDetail> listReportTicketDetailsForDisplay(int userid,String siteId, String fromDate,String toDate,String category, String type, String priority,String TimezoneOffset );
	
	
	public List<TicketDetail> listCustomerTicketDetailsForDisplay(int customerid,String siteId, String fromDate,String toDate,String category, String type, String priority,String TimezoneOffset );
				
	public List<OpenStateTicketsCount> listOpenticketstatecountsByUser(int userid, String TimezoneOffset);	
	public List<OpenStateTicketsCount> listOpenticketstatecountsByAssignedByUser(int userid, String TimezoneOffset);	
	public List<OpenStateTicketsCount> listOpenticketstatecountsByAssignedBySite(int siteid, String TimezoneOffset);	
	public List<OpenStateTicketsCount> listOpenticketstatecountsByAssignedByCustomer(int customerid, String TimezoneOffset);
	
	public List<OpenStateTicketsCount> listOpenticketstatecountsBySite(int siteid, String TimezoneOffset);	
	public List<OpenStateTicketsCount> listOpenticketstatecountsByCustomer(int customerid, String TimezoneOffset);
	
	public List<OpenStateTicketsCount> listOpenticketstatecountspiechartBySite(int siteid, String TimezoneOffset);	
	public List<OpenStateTicketsCount> listOpenticketstatecountspiechartByUser(int userid, String TimezoneOffset);	
	public List<OpenStateTicketsCount> listOpenticketstatecountspiechartByCustomer(int customerid, String TimezoneOffset);
	

	public List<OpenStateTicketsCount> listAllticketstatecountsByUser(int userid, String TimezoneOffset);	
	public List<OpenStateTicketsCount> listAllicketstatecountsBySite(int siteid, String TimezoneOffset);	
	public List<OpenStateTicketsCount> listAllicketstatecountsByCustomer(int customerid, String TimezoneOffset);
	
	
	
	public List<TicketDetail> listStatefilterTickets(String state,int userid, String TimezoneOffset);
	public List<TicketDetail> listStatefilterTicketsBySite(String state,int siteid, String TimezoneOffset);
	public List<TicketDetail> listStatefilterTicketsByCustomer(String state,int customerid, String TimezoneOffset);
	
	
	public List<TicketDetail> listStateandStatusfilterTickets(int state,int status,int userid, String TimezoneOffset);
	public List<TicketDetail> listStateandStatusfilterTicketsBySite(int state,int status,int siteid, String TimezoneOffset);
	public List<TicketDetail> listStateandStatusfilterTicketsByCustomer(int state,int status,int customerid, String TimezoneOffset);
	
	
	public List<TicketDetail> listDaylapsedsincecreationfilterTickets(int state,int status,int fromday,int today,int userid, String TimezoneOffset);
	public List<TicketDetail> listDaylapsedsincecreationfilterTicketsBySite(int state,int status,int fromday,int today,int siteid, String TimezoneOffset);
	public List<TicketDetail> listDaylapsedsincecreationfilterTicketsByCustomer(int state,int status,int fromday,int today,int customerid, String TimezoneOffset);

	public List<TicketDetail> findByEquipmentId(int siteId,String TimezoneOffset);

	
public List<OpenStateTicketsCount> Allstatepiechart(int userId,String TimezoneOffset);
	
	public List<OpenStateTicketsCount> Operationallstatepiechart(int userId,String TimezoneOffset);
	
	public List<OpenStateTicketsCount> Maintenanceallstatepiechart(int userId,String TimezoneOffset);
	
	public List<OpenStateTicketsCount> Openstateandstatustikcets(int userId,String TimezoneOffset);
	
	public List<OpenStateTicketsCount> Closestateandstatustikcets(int userId,String TimezoneOffset);
	
	public List<OpenStateTicketsCount> Holdstateandstatustikcets(int userId,String TimezoneOffset);
	
public List<OpenStateTicketsCount> AllstatepiechartBySite(int siteId,String TimezoneOffset);
	
	public List<OpenStateTicketsCount> OperationallstatepiechartBySite(int siteId,String TimezoneOffset);
	
	public List<OpenStateTicketsCount> MaintenanceallstatepiechartBySite(int siteId,String TimezoneOffset);
	
	
	public List<OpenStateTicketsCount> OpenstateandstatustikcetsBySite(int siteId,String TimezoneOffset);
	
	public List<OpenStateTicketsCount> ClosestateandstatustikcetsBySite(int siteId,String TimezoneOffset);
	
	public List<OpenStateTicketsCount> HoldstateandstatustikcetsBySite(int siteId,String TimezoneOffset);
	
	
public List<OpenStateTicketsCount> OpenstateandstatustikcetsByCustomer(int customerId,String TimezoneOffset);
	
	public List<OpenStateTicketsCount> ClosestateandstatustikcetsByCustomer(int customerId,String TimezoneOffset);
	
	public List<OpenStateTicketsCount> HoldstateandstatustikcetsByCustomer(int customerId,String TimezoneOffset);
	
public List<OpenStateTicketsCount> AllstatepiechartByCustomer(int customerId,String TimezoneOffset);
	
	public List<OpenStateTicketsCount> OperationallstatepiechartByCustomer(int customerId,String TimezoneOffset);
	
	public List<OpenStateTicketsCount> MaintenanceallstatepiechartByCustomer(int customerId,String TimezoneOffset);
	
	
	public List<TicketDetail> listStatefilterTickets(String type,String state,int userid, String TimezoneOffset);
	
	public List<TicketDetail> AllStateandStatusfilterTickets(String type,int status,int userid, String TimezoneOffset);
	
	
	public List<TicketDetail> listStatefilterTicketsBySite(String type,String state,int siteId, String TimezoneOffset);
	
	
	public List<TicketDetail> AllStateandStatusfilterTicketsBySite(String type,int status,int siteId, String TimezoneOffset);
	
public List<TicketDetail> listTypefilterTicketsByCustomer(String type,String state,int customerId, String TimezoneOffset);
	
	
	public List<TicketDetail> AllStateandStatusfilterTicketsByCustomer(String type,int status,int customerId, String TimezoneOffset);
	
	
}
