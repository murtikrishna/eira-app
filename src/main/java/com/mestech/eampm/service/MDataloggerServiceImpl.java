package com.mestech.eampm.service;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.dao.MDataloggerDAO;
import com.mestech.eampm.model.MDatalogger;

/******************************************************
 * 
 * Filename : MDataloggerServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from mdataloggerservice and its access
 *
 * the information about mdataloggerdao.
 * 
 * 
 *******************************************************/
@Service
public class MDataloggerServiceImpl implements MDataloggerService {
	@Autowired
	private MDataloggerDAO mDataloggerDAO;

	/********************************************************************************************
	 * 
	 * Function Name :save
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the data logger.
	 * 
	 * Input Params :datalogger.
	 * 
	 * Return Value : MDatalogger
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public MDatalogger save(MDatalogger datalogger) {
		// TODO Auto-generated method stub
		return mDataloggerDAO.save(datalogger);
	}

	/********************************************************************************************
	 * 
	 * Function Name :update
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the data logger.
	 * 
	 * Input Params :datalogger.
	 * 
	 * Return Value : MDatalogger
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	@Transactional
	public MDatalogger update(MDatalogger datalogger) {
		// TODO Auto-generated method stub
		return mDataloggerDAO.update(datalogger);
	}

	/********************************************************************************************
	 * 
	 * Function Name :delete
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the data logger.
	 * 
	 * Input Params :id.
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	@Transactional
	public void delete(BigInteger id) {
		mDataloggerDAO.delete(id);

	}

	/********************************************************************************************
	 * 
	 * Function Name :edit
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the data logger.
	 * 
	 * Input Params :id.
	 * 
	 * Return Value :MDatalogger.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	@Transactional
	public MDatalogger edit(BigInteger id) {
		// TODO Auto-generated method stub
		return mDataloggerDAO.edit(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listAll
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the mdata logger.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<MDatalogger>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	@Transactional
	public List<MDatalogger> listAll() {
		// TODO Auto-generated method stub
		return mDataloggerDAO.listAll();
	}

}
