package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.dao.SiteDocumentationDao;
import com.mestech.eampm.model.SiteDocumentation;

/******************************************************
 * 
 * Filename :SiteDocumentationServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from sitedocumentationservice and its
 * 
 * access the information about sitedocumentationdao.
 * 
 * 
 *******************************************************/
@Service
public class SiteDocumentationServiceImpl implements SiteDocumentationService {
	@Autowired
	private SiteDocumentationDao siteDocumentationDao;

	/********************************************************************************************
	 * 
	 * Function Name :save
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the site documentation.
	 * 
	 * Input Params :siteDocumentation.
	 * 
	 * Return Value :SiteDocumentation.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public SiteDocumentation save(SiteDocumentation siteDocumentation) {

		return siteDocumentationDao.save(siteDocumentation);
	}

	/********************************************************************************************
	 * 
	 * Function Name :update
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the site documentation.
	 * 
	 * Input Params :siteDocumentation.
	 * 
	 * Return Value :SiteDocumentation.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	@Transactional
	public SiteDocumentation update(SiteDocumentation siteDocumentation) {

		return siteDocumentationDao.update(siteDocumentation);
	}

	/********************************************************************************************
	 * 
	 * Function Name :delete
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to delete the document by using id.
	 * 
	 * Input Params :id.
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	@Transactional
	public void delete(Long id) {
		siteDocumentationDao.delete(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listAll
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site documentation.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<SiteDocumentation>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	@Transactional
	public List<SiteDocumentation> listAll() {

		return siteDocumentationDao.listAll();
	}

	/********************************************************************************************
	 * 
	 * Function Name :edit
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to edit the site documentation by using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value :SiteDocumentation
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	@Transactional
	public SiteDocumentation edit(Long id) {
		// TODO Auto-generated method stub
		return siteDocumentationDao.edit(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :findBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site documentation by
	 * using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value :List<SiteDocumentation>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	@Transactional
	public List<SiteDocumentation> findBySiteId(int id) {
		// TODO Auto-generated method stub
		return siteDocumentationDao.findBySiteId(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :deleteDocuments
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the documents.
	 * 
	 * Input Params :siteCode, docCategory, docName
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public void deleteDocuments(String siteCode, String docCategory, String docName) {
		siteDocumentationDao.deleteDocuments(siteCode, docCategory, docName);

	}

	/********************************************************************************************
	 * 
	 * Function Name :findBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the site documentation.
	 * 
	 * Input Params :id, category
	 * 
	 * Return Value : List<SiteDocumentation>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public List<SiteDocumentation> findBySiteId(int id, String category) {
		// TODO Auto-generated method stub
		return siteDocumentationDao.findBySiteId(id, category);
	}

}
