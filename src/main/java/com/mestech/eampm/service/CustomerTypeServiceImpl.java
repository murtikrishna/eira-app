package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.CustomerTypeDAO;
import com.mestech.eampm.model.CustomerType;

/******************************************************
 * 
 * Filename : CustomerTypeServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description : this class is implements from customertypeservice and its
 *
 * access the information about customertypedao.
 * 
 * 
 *******************************************************/
@Service

public class CustomerTypeServiceImpl implements CustomerTypeService {

	@Autowired
	private CustomerTypeDAO customertypeDAO;

	/********************************************************************************************
	 * 
	 * Function Name : setCustomerTypeDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set customertypedao.
	 * 
	 * Input Params : customertypeDAO
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	public void setCustomerTypeDAO(CustomerTypeDAO customertypeDAO) {
		this.customertypeDAO = customertypeDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name : addCustomerType
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save customertype.
	 * 
	 * Input Params : customertype
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void addCustomerType(CustomerType customertype) {
		customertypeDAO.addCustomerType(customertype);
	}

	/********************************************************************************************
	 * 
	 * Function Name : updateCustomerType
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update customertype.
	 * 
	 * Input Params : customertype
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void updateCustomerType(CustomerType customertype) {
		customertypeDAO.updateCustomerType(customertype);
	}

	/********************************************************************************************
	 * 
	 * Function Name : listCustomerTypes
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the customertypes
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<CustomerType>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<CustomerType> listCustomerTypes() {
		return this.customertypeDAO.listCustomerTypes();
	}

	/********************************************************************************************
	 * 
	 * Function Name : getCustomerTypeById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get customertype by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : CustomerType
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public CustomerType getCustomerTypeById(int id) {
		return customertypeDAO.getCustomerTypeById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name : removeCustomerType
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to removecustomertype by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeCustomerType(int id) {
		customertypeDAO.removeCustomerType(id);
	}

}
