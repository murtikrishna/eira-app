package com.mestech.eampm.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.CountryRegionDAO;
import com.mestech.eampm.model.CountryRegion;

/******************************************************
 * 
 * Filename : CountryRegionServiceImpl
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description : this class implements from countryregionservice and its access
 * 
 * the information about countryregiondao.
 * 
 * 
 *******************************************************/

@Service

public class CountryRegionServiceImpl implements CountryRegionService {

	@Autowired
	private CountryRegionDAO countryregionDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setcountryregionDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the countryregiondao
	 * 
	 * Input Params : countryregionDAO.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	public void setcountryregionDAO(CountryRegionDAO countryregionDAO) {
		this.countryregionDAO = countryregionDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addCountryRegion
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the countryregion.
	 * 
	 * Input Params : countryregion
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void addCountryRegion(CountryRegion countryregion) {
		countryregionDAO.addCountryRegion(countryregion);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateCountryRegion
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the countryregion.
	 * 
	 * Input Params : countryregion
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void updateCountryRegion(CountryRegion countryregion) {
		countryregionDAO.updateCountryRegion(countryregion);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listCountryRegions
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the countryregions.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<CountryRegion>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<CountryRegion> listCountryRegions() {
		return this.countryregionDAO.listCountryRegions();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getCountryRegionById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get countryregion by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : CountryRegion
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public CountryRegion getCountryRegionById(int id) {
		return countryregionDAO.getCountryRegionById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeCountryRegion
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the countryregion.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public void removeCountryRegion(int id) {
		countryregionDAO.removeCountryRegion(id);
	}

}
