package com.mestech.eampm.service;


import java.util.List;

import com.mestech.eampm.bean.CustomerListBean;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.EquipmentType;

public interface CustomerService {

	public void addCustomer(Customer customer);
    
    public void updateCustomer(Customer customer);
    
    public Customer getCustomerById(int id);
    
    public Customer getCustomerByMax(String MaxColumnName);
    
    public Customer getPrimaryCustomerByMax(String MaxColumnName);
    
    public  List<Customer> getPrimaryCustomerById(String id);
    
    public void removeCustomer(int id);
    
    public List<Customer> listCustomers();	

	public List<Customer> getCustomerListByUserId(int userId);
	
	public List<CustomerListBean> getAjaxCustomerListByUser(String UserID);
	
	public List<CustomerListBean> getAjaxCustomerListByCustomer(String CustomerID) ;
	
	 public List<Customer> listallcustomers(int customerid);
	 
	 public List<Customer> listConfigCustomers();
	 
	 public List<Customer> listprimarycustomers();
}
