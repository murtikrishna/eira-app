package com.mestech.eampm.service;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.dao.MServiceCodeDAO;
import com.mestech.eampm.model.MServiceCode;

/******************************************************
 * 
 * Filename :MServiceCodeServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from mservicecodeservice and its access
 *
 * the information about mservicecodedao.
 * 
 * 
 *******************************************************/
@Service
public class MServiceCodeServiceImpl implements MServiceCodeService {
	@Autowired
	private MServiceCodeDAO mServiceCodeDAO;

	/********************************************************************************************
	 * 
	 * Function Name :save
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the Mservice code.
	 * 
	 * Input Params :mServiceCode.
	 * 
	 * Return Value : MServiceCode.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	@Override
	public MServiceCode save(MServiceCode mServiceCode) {
		// TODO Auto-generated method stub
		return null;
	}

	/********************************************************************************************
	 * 
	 * Function Name :update
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the Mservice code.
	 * 
	 * Input Params :mServiceCode.
	 * 
	 * Return Value : MServiceCode.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	public MServiceCode update(MServiceCode mServiceCode) {
		// TODO Auto-generated method stub
		return null;
	}

	/********************************************************************************************
	 * 
	 * Function Name :delete
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the data by using id.
	 * 
	 * Input Params :id.
	 * 
	 * Return Value :void.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub

	}

	/********************************************************************************************
	 * 
	 * Function Name :edit
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the mservice code by using id.
	 * 
	 * Input Params :id.
	 * 
	 * Return Value :MServiceCode.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	public MServiceCode edit(BigInteger id) {
		// TODO Auto-generated method stub
		return null;
	}

	/********************************************************************************************
	 * 
	 * Function Name :listAll
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the mservice code.
	 * 
	 * Input Params :
	 * 
	 * Return Value :List<MServiceCode>.
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Override
	@Transactional
	public List<MServiceCode> listAll() {

		return mServiceCodeDAO.listAll();
	}

}
