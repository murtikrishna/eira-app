
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.StateDAO;
import com.mestech.eampm.model.State;

/******************************************************
 * 
 * Filename :StateServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class implements from stateservice and its access the
 * 
 * information about statedao.
 * 
 * 
 *******************************************************/
@Service

public class StateServiceImpl implements StateService {

	@Autowired
	private StateDAO stateDAO;

	/********************************************************************************************
	 * 
	 * Function Name :setstateDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the state dao.
	 * 
	 * Input Params :stateDAO
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void setstateDAO(StateDAO stateDAO) {
		this.stateDAO = stateDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addState
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the state.
	 * 
	 * Input Params :state
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addState(State state) {
		stateDAO.addState(state);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listAllStatesByCountryID
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the states by using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value : List<State>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<State> listAllStatesByCountryID(int id) {
		return stateDAO.listAllStatesByCountryID(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateState
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the state.
	 * 
	 * Input Params :state
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateState(State state) {
		stateDAO.updateState(state);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listStates
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the states.
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<State>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<State> listStates() {
		return this.stateDAO.listStates();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getStateById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get the state by using id.
	 * 
	 * Input Params : id
	 * 
	 * Return Value : State
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public State getStateById(int id) {
		return stateDAO.getStateById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeState
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove the state by using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeState(int id) {
		stateDAO.removeState(id);
	}

}
