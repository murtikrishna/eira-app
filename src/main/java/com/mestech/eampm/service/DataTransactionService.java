package com.mestech.eampm.service;

import java.util.List;

import com.mestech.eampm.bean.EnergyPerformanceBean;
import com.mestech.eampm.bean.EquipmentWithParameterListBean;
import com.mestech.eampm.bean.EquipmentWithoutParameterListBean;
import com.mestech.eampm.bean.GraphDataBean;
import com.mestech.eampm.model.DataTransaction;


	public interface DataTransactionService {
		 
		public void addDataTransaction(DataTransaction dataTransaction);
		    
		public void updateDataTransaction(DataTransaction dataTransaction);
			  
		public DataTransaction getDataTransactionById(int id);

	    public DataTransaction getDataTransactionByMax(String MaxColumnName);
		    
		public void removeDataTransaction(int id);
		    
		public List<DataTransaction> listDataTransactions();

		

		public List<DataTransaction> listDataTransactionsForDataDownload(int siteId, String fromDate,String toDate,int equipmentId,String TimezoneOffset );
		   
		public List<DataTransaction> getDataTransactionListByUserId(int userId,String TimezoneOffset);
		
		public List<DataTransaction> getDataTransactionListByCustomerId(int customerId,String TimezoneOffset);
		
		public List<DataTransaction> getDataTransactionListBySiteId(int siteId,String TimezoneOffset);

		public List<DataTransaction> getDataTransactionListByEquipmentId(int equipmentId,String TimezoneOffset);
		
		public List<DataTransaction> getDataTransactionListByEquipmentIdWithDate(int equipmentId, String FromDate, String ToDate,String TimezoneOffset);
		
		public List<GraphDataBean> getDataTransactionListByEquipmentIdsWithDate(String equipmentIds, String FromDate, String ToDate,String TimezoneOffset);
		
		public List<GraphDataBean> getParameterValuesByEquipmentIdsWithDate(String equipmentIds, String Parameter, String FromDate, String ToDate,String TimezoneOffset);
		
		public List<GraphDataBean> getParameterValuesBySiteIdsWithDate(String siteIds, String Parameter, String FromDate, String ToDate,String TimezoneOffset);
		
		public List<GraphDataBean> getIrradiationValuesBySitesWithDate(String siteId, String FromDate, String ToDate,String TimezoneOffset);
		
		public List<GraphDataBean> getDaywiseIrradiationValuesBySitesWithDate(String siteId, String FromDate, String ToDate,String TimezoneOffset);
		
		public List<EnergyPerformanceBean> getEnergyDataByEquipmentIdsWithDate(String equipmentIds,  String FromDate, String ToDate,String TimezoneOffset);
		
		public List<EnergyPerformanceBean> getEnergyDataBySiteIdsWithDate(String siteIds,  String FromDate, String ToDate,String TimezoneOffset);
			
		public String getYesterdayEnergyBySiteIdsWithDate(String siteId,  String FromDate, String ToDate,String TimezoneOffset);
		
		public String getYesterdayEnergyByEquipmentIdsWithDate(String equipmentIds,  String FromDate, String ToDate,String TimezoneOffset);
		
		
		public String getTodayFlagStatus(String equipmentIds,String TimezoneOffset);
		
		
		public List<EquipmentWithoutParameterListBean> getEquipmentList(String siteId);
		
		public List<EquipmentWithParameterListBean> getEquipmentWithParameterList(String siteId);
		 
		public String getParameterComparisionDateWithDate(String equipmentIds, String Parameters, String FromDate, String ToDate,String TimezoneOffset);
		
		public List<GraphDataBean> getEquipmentviewForSmb(String equipmentIds,String FromDate, String ToDate,String TimezoneOffset);
		
		public List<GraphDataBean> getEquipmentviewForTracker(String equipmentIds,String FromDate, String ToDate,String TimezoneOffset);
	}


