package com.mestech.eampm.service;


import java.util.List;

import com.mestech.eampm.model.ErrorCode;

public interface ErrorCodeService {

	public void addErrorCode(ErrorCode errorcode);
    
	public void updateErrorCode(ErrorCode errorcode);
	    
	public ErrorCode getErrorCodeById(int id);
	    
	public void removeErrorCode(int id);
	    
	public List<ErrorCode> listErrorCodes(int TypeID);
	
	public ErrorCode getErrorCodeByMax(String MaxColumnName);
	
	public List<ErrorCode> listErrorCodes();
	
public List<ErrorCode> getErrorCodeListByUserId(int userId);
	
	public List<ErrorCode> getErrorCodeListBySiteId(int siteId);
	
	//public List<ErrorCode> listErrorCodesForDisplay(int siteId, String fromDate,String toDate,String category, String type, String priority );
	  
	public List<ErrorCode> getErrorCodeListByCustomerId(int customerId);
	
	public List<ErrorCode> listexitsErrorCodes(int equipmenttypeid,int errorid);
}
