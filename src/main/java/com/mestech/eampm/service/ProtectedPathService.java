package com.mestech.eampm.service;

import java.util.List;

import com.mestech.eampm.model.ProtectedUrl;

public interface ProtectedPathService {
	public List<ProtectedUrl> getAll();
}
