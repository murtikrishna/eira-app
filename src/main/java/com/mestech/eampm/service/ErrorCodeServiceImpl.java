package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.ErrorCodeDAO;
import com.mestech.eampm.model.ErrorCode;
import com.mestech.eampm.model.TicketDetail;

/******************************************************
 * 
 * Filename : ErrorCodeServiceImpl
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :this class is implements from errorcodeservice and its access
 * 
 * the implements aboout errorcodedao.
 * 
 * 
 *******************************************************/
@Service

public class ErrorCodeServiceImpl implements ErrorCodeService {

	@Autowired
	private ErrorCodeDAO errorcodeDAO;

	/********************************************************************************************
	 * 
	 * Function Name :seterrorcodeDAO
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to set the error code dao .
	 * 
	 * Input Params :errorcodeDAO.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	public void seterrorcodeDAO(ErrorCodeDAO errorcodeDAO) {
		this.errorcodeDAO = errorcodeDAO;
	}

	/********************************************************************************************
	 * 
	 * Function Name :addErrorCode
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to save the error code .
	 * 
	 * Input Params :errorcode.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void addErrorCode(ErrorCode errorcode) {
		errorcodeDAO.addErrorCode(errorcode);
	}

	/********************************************************************************************
	 * 
	 * Function Name :updateErrorCode
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to update the error code .
	 * 
	 * Input Params :errorcode.
	 * 
	 * Return Value : void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void updateErrorCode(ErrorCode errorcode) {
		errorcodeDAO.updateErrorCode(errorcode);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listErrorCodes
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the error codes .
	 * 
	 * Input Params :TypeID.
	 * 
	 * Return Value : List<ErrorCode>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public List<ErrorCode> listErrorCodes(int TypeID) {
		return this.errorcodeDAO.listErrorCodes(TypeID);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listErrorCodes
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the error codes .
	 * 
	 * Input Params :
	 * 
	 * Return Value : List<ErrorCode>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<ErrorCode> listErrorCodes() {
		return this.errorcodeDAO.listErrorCodes();
	}

	/********************************************************************************************
	 * 
	 * Function Name :getErrorCodeByMax
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get error code by using maximum column
	 * name.
	 * 
	 * Input Params : MaxColumnName
	 * 
	 * Return Value : ErrorCode
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public ErrorCode getErrorCodeByMax(String MaxColumnName) {
		return this.errorcodeDAO.getErrorCodeByMax(MaxColumnName);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getErrorCodeById
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to get error code by using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value : ErrorCode
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public ErrorCode getErrorCodeById(int id) {
		return errorcodeDAO.getErrorCodeById(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :removeErrorCode
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to remove error code by using id.
	 * 
	 * Input Params :id
	 * 
	 * Return Value :void
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	// @Override
	@Transactional
	public void removeErrorCode(int id) {
		errorcodeDAO.removeErrorCode(id);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getErrorCodeListByUserId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the error codes by using user
	 * id.
	 * 
	 * Input Params :userId
	 * 
	 * Return Value :List<ErrorCode>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<ErrorCode> getErrorCodeListByUserId(int userId) {
		return this.errorcodeDAO.getErrorCodeListByUserId(userId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getErrorCodeListBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the error codes by using site
	 * id.
	 * 
	 * Input Params :siteId
	 * 
	 * Return Value :List<ErrorCode>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<ErrorCode> getErrorCodeListBySiteId(int siteId) {
		return this.errorcodeDAO.getErrorCodeListBySiteId(siteId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :getErrorCodeListByCustomerId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the error codes by using
	 * customer id.
	 * 
	 * Input Params :customerId
	 * 
	 * Return Value :List<ErrorCode>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	// @Override
	@Transactional
	public List<ErrorCode> getErrorCodeListByCustomerId(int customerId) {
		return this.errorcodeDAO.getErrorCodeListByCustomerId(customerId);
	}

	/********************************************************************************************
	 * 
	 * Function Name :listexitsErrorCodes
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 25-Apr-19 01:44 PM
	 * 
	 * Description : this function used to select all the exits error codes
	 * 
	 * Input Params :equipmenttypeid,errorid
	 * 
	 * Return Value :List<ErrorCode>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/
	@Transactional
	public List<ErrorCode> listexitsErrorCodes(int equipmenttypeid, int errorid) {
		return this.errorcodeDAO.listexitsErrorCodes(equipmenttypeid, errorid);
	}

}
