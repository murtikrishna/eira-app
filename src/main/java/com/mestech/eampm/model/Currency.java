package com.mestech.eampm.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/******************************************************
 * 
 * Filename : Currency
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/


@Entity
@Table(name="mCurrency")
public class Currency implements Serializable{

	private static final long serialVersionUID = -723583058586873479L;
	 
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "CurrencyID")
	 private Integer CurrencyId;
	 
	 @Column(name="CurrencyName")
	 private String CurrencyName;
	 
	 @Column(name="CurrencySymbol")
	 private String CurrencySymbol;
	
	 
	 @Column(name="ActiveFlag")
	 private Integer ActiveFlag;

	 @Column(name="CreationDate")
	 private Date CreationDate;

	 @Column(name="CreatedBy")
	 private Integer CreatedBy;

	 @Column(name="LastUpdatedDate")
	 private Date LastUpdatedDate;

	 @Column(name="LastUpdatedBy")
	 private Integer LastUpdatedBy;

	public Integer getCurrencyId() {
		return CurrencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		CurrencyId = currencyId;
	}

	public String getCurrencyName() {
		return CurrencyName;
	}

	public void setCurrencyName(String currencyName) {
		CurrencyName = currencyName;
	}

	public String getCurrencySymbol() {
		return CurrencySymbol;
	}

	public void setCurrencySymbol(String currencySymbol) {
		CurrencySymbol = currencySymbol;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public int getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(int lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}



	
	
		
	
}
