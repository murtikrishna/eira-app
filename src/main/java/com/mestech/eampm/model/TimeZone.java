package com.mestech.eampm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/******************************************************
 * 
 * Filename :TimeZone
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/

@Entity
@Table(name = "mTimezone")
public class TimeZone implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TimeZoneID")
	private Integer TimeZoneId;

	@Column(name = "TimeZoneCode")
	private String TimeZoneCode;

	@Column(name = "TimeZoneName")
	private String TimeZoneName;

	@Column(name = "TimeZoneSign")
	private Integer TimeZoneSign;

	/*
	 * @Column(name="TimeZoneValue") private Date TimeZoneValue;
	 */

	@Column(name = "UTCOffset")
	private String UtcOffset;

	@Column(name = "TimeZoneFormat")
	private String TimeZoneFormat = "DD-MM-YYYY HI24:MI:SS";

	@Column(name = "ActiveFlag")
	private Integer ActiveFlag;

	@Column(name = "CreationDate")
	private Date CreationDate;

	@Column(name = "CreatedBy")
	private Integer CreatedBy;

	@Column(name = "LastUpdatedDate")
	private Date LastUpdatedDate;

	@Column(name = "LastUpdatedBy")
	private Integer LastUpdatedBy;

	private Integer timeZoneOffSetInMin;

	public Integer getTimeZoneOffSetInMin() {
		return timeZoneOffSetInMin;
	}

	public void setTimeZoneOffSetInMin(Integer timeZoneOffSetInMin) {
		this.timeZoneOffSetInMin = timeZoneOffSetInMin;
	}

	public Integer getTimeZoneId() {
		return TimeZoneId;
	}

	public void setTimeZoneId(Integer timeZoneId) {
		TimeZoneId = timeZoneId;
	}

	public String getTimeZoneCode() {
		return TimeZoneCode;
	}

	public void setTimeZoneCode(String timeZoneCode) {
		TimeZoneCode = timeZoneCode;
	}

	public String getTimeZoneName() {
		return TimeZoneName;
	}

	public void setTimeZoneName(String timeZoneName) {
		TimeZoneName = timeZoneName;
	}

	public Integer getTimeZoneSign() {
		return TimeZoneSign;
	}

	public void setTimeZoneSign(Integer timeZoneSign) {
		TimeZoneSign = timeZoneSign;
	}

	/*
	 * public Date getTimeZoneValue() { return TimeZoneValue; }
	 * 
	 * public void setTimeZoneValue(Date timeZoneValue) { TimeZoneValue =
	 * timeZoneValue; }
	 */

	public String getUtcOffset() {
		return UtcOffset;
	}

	public void setUtcOffset(String utcOffset) {
		UtcOffset = utcOffset;
	}

	public String getTimeZoneFormat() {
		return TimeZoneFormat;
	}

	public void setTimeZoneFormat(String timeZoneFormat) {
		TimeZoneFormat = timeZoneFormat;
	}

	/*
	 * public Integer getCountryID() { return CountryID; }
	 * 
	 * public void setCountryID(Integer countryID) { CountryID = countryID; }
	 */

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

}
