package com.mestech.eampm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/******************************************************
 * 
 * Filename :RoleActivity
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/

@Entity
@Table(name = "mRoleMapping")
public class RoleActivity implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MapID")
	private Integer RoleActivityId;

	@Column(name = "RoleID")
	private Integer RoleID;

	@Column(name = "ActivityID")
	private Integer ActivityID;

	@Column(name = "ActiveFlag")
	private Integer ActiveFlag;

	@Column(name = "CreationDate")
	private Date CreationDate;

	@Column(name = "LastUpdatedDate")
	private Date LastUpdatedDate;

	public RoleActivity() {
		super();
	}

	public RoleActivity(Integer roleID, Integer activityID, Integer activeFlag, Date creationDate) {
		super();
		RoleID = roleID;
		ActivityID = activityID;
		ActiveFlag = activeFlag;
		CreationDate = creationDate;
	}

	public Integer getRoleActivityId() {
		return RoleActivityId;
	}

	public void setRoleActivityId(Integer roleActivityId) {
		RoleActivityId = roleActivityId;
	}

	public Integer getRoleID() {
		return RoleID;
	}

	public void setRoleID(Integer roleID) {
		RoleID = roleID;
	}

	public Integer getActivityID() {
		return ActivityID;
	}

	public void setActivityID(Integer activityID) {
		ActivityID = activityID;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	@Transient
	private String RoleName;

	@Transient
	private String AccessPages;

	public String getRoleName() {
		return RoleName;
	}

	public void setRoleName(String roleName) {
		RoleName = roleName;
	}

	public String getAccessPages() {
		return AccessPages;
	}

	public void setAccessPages(String accessPages) {
		AccessPages = accessPages;
	}

}
