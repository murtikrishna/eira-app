package com.mestech.eampm.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/******************************************************
 * 
 * Filename :StandardOperatingProcedure
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/
@Entity
@Table(name = "StandardOperatingProcedure")
public class StandardOperatingProcedure {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private Integer steps, option1, option2, option3, option4;

	private String workInstruction, workType, category, workInstructionInfo, remarks;

	public StandardOperatingProcedure() {
		super();
	}

	public StandardOperatingProcedure(Integer id, Integer steps, String workType, String category, Integer option1,
			Integer option2, Integer option3, Integer option4, String workInstruction, String workInstructionInfo,
			String remarks) {
		super();
		this.id = id;
		this.steps = steps;
		this.workType = workType;
		this.category = category;
		this.option1 = option1;
		this.option2 = option2;
		this.option3 = option3;
		this.option4 = option4;
		this.workInstruction = workInstruction;
		this.workInstructionInfo = workInstructionInfo;
		this.remarks = remarks;
	}

	public StandardOperatingProcedure(Integer steps, String workType, String category, Integer option1, Integer option2,
			Integer option3, Integer option4, String workInstruction, String workInstructionInfo, String remarks) {
		super();
		this.steps = steps;
		this.workType = workType;
		this.category = category;
		this.option1 = option1;
		this.option2 = option2;
		this.option3 = option3;
		this.option4 = option4;
		this.workInstruction = workInstruction;
		this.workInstructionInfo = workInstructionInfo;
		this.remarks = remarks;
	}

	public Integer getSteps() {
		return steps;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setSteps(Integer steps) {
		this.steps = steps;
	}

	public String getWorkType() {
		return workType;
	}

	public void setWorkType(String workType) {
		this.workType = workType;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getOption1() {
		return option1;
	}

	public void setOption1(Integer option1) {
		this.option1 = option1;
	}

	public Integer getOption2() {
		return option2;
	}

	public void setOption2(Integer option2) {
		this.option2 = option2;
	}

	public Integer getOption3() {
		return option3;
	}

	public void setOption3(Integer option3) {
		this.option3 = option3;
	}

	public Integer getOption4() {
		return option4;
	}

	public void setOption4(Integer option4) {
		this.option4 = option4;
	}

	public String getWorkInstruction() {
		return workInstruction;
	}

	public void setWorkInstruction(String workInstruction) {
		this.workInstruction = workInstruction;
	}

	public String getWorkInstructionInfo() {
		return workInstructionInfo;
	}

	public void setWorkInstructionInfo(String workInstructionInfo) {
		this.workInstructionInfo = workInstructionInfo;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
