
package com.mestech.eampm.model;

import java.io.Serializable;
import java.util.Date;

import javax.enterprise.inject.Default;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

/******************************************************
 * 
 * Filename :TicketDetail
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/
@Entity
@Table(name = "tTicketDetail")
public class TicketDetail implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TicketID")
	private Integer TicketID;

	/*
	 * @Id
	 * 
	 * @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
	 * "tticketdetail_seq")
	 * 
	 * @SequenceGenerator(name = "tticketdetail_seq", sequenceName =
	 * "tticketdetail_seq")
	 * 
	 * @Column(name="TicketID") private Integer TicketID;
	 */

	@Column(name = "TicketCode")
	private String TicketCode;

	@Column(name = "EventTransactionID")
	private Integer EventTransactionID;

	@Column(name = "SiteID")
	private Integer SiteID;

	@Column(name = "DayCycle")
	private Integer DayCycle;

	public Integer getDayCycle() {
		return DayCycle;
	}

	public void setDayCycle(Integer dayCycle) {
		DayCycle = dayCycle;
	}

	@Column(name = "Severity")
	private Integer Severity;

	@Column(name = "Description")
	private String Description;

	@Column(name = "AssignedTo")
	private Integer AssignedTo;

	@Transient
	private Integer ReassignedTo;

	@Transient
	private Date RescheduledOn;

	public Date getRescheduledOn() {
		return RescheduledOn;
	}

	public void setRescheduledOn(Date rescheduledOn) {
		RescheduledOn = rescheduledOn;
	}

	public Integer getReassignedTo() {
		return ReassignedTo;
	}

	public void setReassignedTo(Integer reassignedTo) {
		ReassignedTo = reassignedTo;
	}

	@Transient
	private String SiteName;

	public String getSiteName() {
		return SiteName;
	}

	public void setSiteName(String siteName) {
		SiteName = siteName;
	}

	public String getAssignedToWhom() {
		return AssignedToWhom;
	}

	public void setAssignedToWhom(String assignedToWhom) {
		AssignedToWhom = assignedToWhom;
	}

	@Transient
	private String AssignedToWhom;

	@Column(name = "ClosedTimeStamp")
	private Date ClosedTimeStamp;

	@Column(name = "StartedTimeStamp")
	private Date StartedTimeStamp;

	@Transient
	private String TimeZone;

	public String getTimeZone() {
		return TimeZone;
	}

	public void setTimeZone(String timeZone) {
		TimeZone = timeZone;
	}

	@Transient
	private String CreatedDateText;

	@Transient
	private String ScheduledOnText;

	public String getScheduledOnText() {
		return ScheduledOnText;
	}

	public void setScheduledOnText(String scheduledOnText) {
		ScheduledOnText = scheduledOnText;
	}

	public String getRescheduledOnText() {
		return RescheduledOnText;
	}

	public void setRescheduledOnText(String rescheduledOnText) {
		RescheduledOnText = rescheduledOnText;
	}

	@Transient
	private String RescheduledOnText;

	@Transient
	private String EquipmentName;

	public String getEquipmentName() {
		return EquipmentName;
	}

	public void setEquipmentName(String equipmentName) {
		EquipmentName = equipmentName;
	}

	@Transient
	private String ClosedDateText;

	public String getCreatedDateText() {
		return CreatedDateText;
	}

	public void setCreatedDateText(String createdDateText) {
		CreatedDateText = createdDateText;
	}

	public String getClosedDateText() {
		return ClosedDateText;
	}

	public void setClosedDateText(String closedDateText) {
		ClosedDateText = closedDateText;
	}

	public String getScheduledDateText() {
		return ScheduledDateText;
	}

	public void setScheduledDateText(String scheduledDateText) {
		ScheduledDateText = scheduledDateText;
	}

	public String getStartedDateText() {
		return StartedDateText;
	}

	public void setStartedDateText(String startedDateText) {
		StartedDateText = startedDateText;
	}

	@Transient
	private String ScheduledDateText;

	@Transient
	private String StartedDateText;

	@Column(name = "Remarks")
	private String Remarks;

	@Column(name = "RefTicketID")
	private Integer RefTicketID;

	@Column(name = "TicketStatus")
	private Integer TicketStatus;

	@Transient
	private String TicketStatusText;

	@Column(name = "ActiveFlag")
	private Integer ActiveFlag;

	@Column(name = "CreationDate")
	private Date CreationDate;

	@Column(name = "CreatedBy")
	private Integer CreatedBy;

	@Transient
	private String CreatedByName;

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	@Column(name = "LastUpdatedDate")
	private Date LastUpdatedDate;

	@Column(name = "LastUpdatedBy")
	private Integer LastUpdatedBy;

	@Column(name = "Priority")
	private Integer Priority;

	@Transient
	private String PriorityText;

	@Column(name = "State")
	private Integer State;

	@Transient
	private String StateText;

	@Column(name = "TicketMode")
	private String TicketMode;

	@Column(name = "TicketType")
	private String TicketType;

	@Column(name = "TicketCategory")
	private String TicketCategory;

	@Column(name = "TicketDetail")
	private String TicketDetail;

	@Column(name = "EquipmentID")
	private Integer EquipmentID;

	@Column(name = "NotificationStatus")
	private Integer NotificationStatus;

	@Column(name = "Longitude")
	private String Longitude;

	@Column(name = "Lattitude")
	private String Lattitude;

	@Column(name = "ProcessType")
	private String ProcessType;

	@Column(name = "ScheduledOn")
	private Date ScheduledOn;

	@Column(name = "ClosedBy")
	private Integer ClosedBy;

	@Transient
	private String ClosedByName;

	@Column(name = "CompletedOn")
	private Date CompletedOn;

	@Column(name = "CompletedTimeStamp")
	private Date CompletedTimeStamp;

	@Column(name = "TicketGroup")
	private String TicketGroup;

	@Transient
	private String ClosedRemarks;

	@Transient
	private String HoldRemarks;

	@Transient
	private String ReassignRemarks;

	public String getTicketStatusText() {
		return TicketStatusText;
	}

	public void setTicketStatusText(String ticketStatusText) {
		TicketStatusText = ticketStatusText;
	}

	public String getCreatedByName() {
		return CreatedByName;
	}

	public void setCreatedByName(String createdByName) {
		CreatedByName = createdByName;
	}

	public String getPriorityText() {
		return PriorityText;
	}

	public void setPriorityText(String priorityText) {
		PriorityText = priorityText;
	}

	public String getStateText() {
		return StateText;
	}

	public void setStateText(String stateText) {
		StateText = stateText;
	}

	public String getClosedByName() {
		return ClosedByName;
	}

	public void setClosedByName(String closedByName) {
		ClosedByName = closedByName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getCompletedTimeStamp() {
		return CompletedTimeStamp;
	}

	public void setCompletedTimeStamp(Date completedTimeStamp) {
		CompletedTimeStamp = completedTimeStamp;
	}

	public String getProcessType() {
		return ProcessType;
	}

	public void setProcessType(String processType) {
		ProcessType = processType;
	}

	public Date getScheduledOn() {
		return ScheduledOn;
	}

	public void setScheduledOn(Date scheduledOn) {
		ScheduledOn = scheduledOn;
	}

	public Integer getTicketID() {
		return TicketID;
	}

	public void setTicketID(Integer ticketID) {
		TicketID = ticketID;
	}

	public String getTicketCode() {
		return TicketCode;
	}

	public void setTicketCode(String ticketCode) {
		TicketCode = ticketCode;
	}

	public Integer getEventTransactionID() {
		return EventTransactionID;
	}

	public void setEventTransactionID(Integer eventTransactionID) {
		EventTransactionID = eventTransactionID;
	}

	public Integer getSiteID() {
		return SiteID;
	}

	public void setSiteID(Integer siteID) {
		SiteID = siteID;
	}

	public Integer getSeverity() {
		return Severity;
	}

	public void setSeverity(Integer severity) {
		Severity = severity;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public Integer getAssignedTo() {
		return AssignedTo;
	}

	public void setAssignedTo(Integer assignedTo) {
		AssignedTo = assignedTo;
	}

	public Date getClosedTimeStamp() {
		return ClosedTimeStamp;
	}

	public void setClosedTimeStamp(Date closedTimeStamp) {
		ClosedTimeStamp = closedTimeStamp;
	}

	public Date getStartedTimeStamp() {
		return StartedTimeStamp;
	}

	public void setStartedTimeStamp(Date startedTimeStamp) {
		StartedTimeStamp = startedTimeStamp;
	}

	public String getRemarks() {
		return Remarks;
	}

	public String getClosedRemarks() {
		return ClosedRemarks;
	}

	public void setClosedRemarks(String closedRemarks) {
		ClosedRemarks = closedRemarks;
	}

	public String getHoldRemarks() {
		return HoldRemarks;
	}

	public void setHoldRemarks(String holdRemarks) {
		HoldRemarks = holdRemarks;
	}

	public String getReassignRemarks() {
		return ReassignRemarks;
	}

	public void setReassignRemarks(String reassignRemarks) {
		ReassignRemarks = reassignRemarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}

	public Integer getRefTicketID() {
		return RefTicketID;
	}

	public void setRefTicketID(Integer refTicketID) {
		RefTicketID = refTicketID;
	}

	public Integer getTicketStatus() {
		return TicketStatus;
	}

	public void setTicketStatus(Integer ticketStatus) {
		TicketStatus = ticketStatus;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	public Integer getPriority() {
		return Priority;
	}

	public void setPriority(Integer priority) {
		Priority = priority;
	}

	public Integer getState() {
		return State;
	}

	public void setState(Integer state) {
		State = state;
	}

	public String getTicketMode() {
		return TicketMode;
	}

	public void setTicketMode(String ticketMode) {
		TicketMode = ticketMode;
	}

	public String getTicketType() {
		return TicketType;
	}

	public void setTicketType(String ticketType) {
		TicketType = ticketType;
	}

	public String getTicketCategory() {
		return TicketCategory;
	}

	public void setTicketCategory(String ticketCategory) {
		TicketCategory = ticketCategory;
	}

	public String getTicketDetail() {
		return TicketDetail;
	}

	public void setTicketDetail(String ticketDetail) {
		TicketDetail = ticketDetail;
	}

	public Integer getEquipmentID() {
		return EquipmentID;
	}

	public void setEquipmentID(Integer equipmentID) {
		EquipmentID = equipmentID;
	}

	public Integer getNotificationStatus() {
		return NotificationStatus;
	}

	public void setNotificationStatus(Integer notificationStatus) {
		NotificationStatus = notificationStatus;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	public String getLattitude() {
		return Lattitude;
	}

	public void setLattitude(String lattitude) {
		Lattitude = lattitude;
	}

	public Integer getClosedBy() {
		return ClosedBy;
	}

	public void setClosedBy(Integer closedBy) {
		ClosedBy = closedBy;
	}

	public Date getCompletedOn() {
		return CompletedOn;
	}

	public void setCompletedOn(Date completedOn) {
		CompletedOn = completedOn;
	}

	@Transient
	private String SiteCode;

	public String getSiteCode() {
		return SiteCode;
	}

	public void setSiteCode(String siteCode) {
		SiteCode = siteCode;
	}

	public String getTicketGroup() {
		return TicketGroup;
	}

	public void setTicketGroup(String ticketGroup) {
		TicketGroup = ticketGroup;
	}

	@Column(name = "UploadFlag")
	private Integer UploadFlag;

	public Integer getUploadFlag() {
		return UploadFlag;
	}

	public void setUploadFlag(Integer uploadFlag) {
		UploadFlag = uploadFlag;
	}

}
