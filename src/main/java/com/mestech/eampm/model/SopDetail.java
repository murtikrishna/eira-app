package com.mestech.eampm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/******************************************************
 * 
 * Filename :SopDetail
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/

@Entity
@Table(name = "mSOPDeatil")
public class SopDetail implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SOPID")
	private Integer SopDetailId;

	@Column(name = "SOPName")
	private String SopName;

	@Column(name = "VersionID")
	private Integer VersionID;

	@Column(name = "EquipmentTypeID")
	private Integer EquipmentTypeID;

	@Column(name = "FileName")
	private String FileName;

	@Column(name = "FilePath")
	private String FilePath;

	@Column(name = "ActiveFlag")
	private Integer ActiveFlag;

	@Column(name = "CreationDate")
	private Date CreationDate;

	@Column(name = "CreatedBy")
	private Integer CreatedBy;

	@Column(name = "LastUpdatedDate")
	private Date LastUpdatedDate;

	@Column(name = "LastUpdatedBy")
	private Integer LastUpdatedBy;

	public Integer getSopDetailId() {
		return SopDetailId;
	}

	public void setSopDetailId(Integer sopDetailId) {
		SopDetailId = sopDetailId;
	}

	public String getSopName() {
		return SopName;
	}

	public void setSopName(String sopName) {
		SopName = sopName;
	}

	public Integer getVersionID() {
		return VersionID;
	}

	public void setVersionID(Integer versionID) {
		VersionID = versionID;
	}

	public Integer getEquipmentTypeID() {
		return EquipmentTypeID;
	}

	public void setEquipmentTypeID(Integer equipmentTypeID) {
		EquipmentTypeID = equipmentTypeID;
	}

	public String getFileName() {
		return FileName;
	}

	public void setFileName(String fileName) {
		FileName = fileName;
	}

	public String getFilePath() {
		return FilePath;
	}

	public void setFilePath(String filePath) {
		FilePath = filePath;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

}
