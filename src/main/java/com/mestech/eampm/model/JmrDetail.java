
package com.mestech.eampm.model;

import java.io.Serializable;
import java.util.Date;

import javax.enterprise.inject.Default;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

/******************************************************
 * 
 * Filename : JmrDetail
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/
@Entity
@Table(name = "tJmrDetail")
public class JmrDetail implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TransactionID")
	private Integer TransactionID;

	@Column(name = "TicketID")
	private Integer TicketID;

	@Column(name = "EmserialNo")
	private String EmserialNo;

	@Column(name = "ImageName")
	private String ImageName;

	@Column(name = "ImagePath")
	private String ImagePath;

	@Column(name = "EmReading")
	private String EmReading;

	@Column(name = "Remarks")
	private String Remarks;

	@Column(name = "LmrTimestamp")
	private Date LmrTimestamp;

	@Column(name = "LmrValue")
	private String LmrValue;

	@Column(name = "PlantDowntime")
	private String PlantDowntime;

	@Column(name = "SlogDate")
	private Date SlogDate;

	@Column(name = "ActiveFlag")
	private Integer ActiveFlag;

	@Column(name = "CreationDate")
	private Date CreationDate;

	@Column(name = "CreatedBy")
	private Integer CreatedBy;

	@Column(name = "LastUpdatedDate")
	private Date LastUpdatedDate;

	@Column(name = "LastUpdatedBy")
	private Integer LastUpdatedBy;

	@Column(name = "ReportImageName")
	private String ReportImageName;

	@Column(name = "ReportImagePath")
	private String ReportImagePath;

	@Column(name = "ReportTimestamp")
	private Date ReportTimestamp;

	public Integer getTransactionID() {
		return TransactionID;
	}

	public void setTransactionID(Integer transactionID) {
		TransactionID = transactionID;
	}

	public Integer getTicketID() {
		return TicketID;
	}

	public void setTicketID(Integer ticketID) {
		TicketID = ticketID;
	}

	public String getEmserialNo() {
		return EmserialNo;
	}

	public void setEmserialNo(String emserialNo) {
		EmserialNo = emserialNo;
	}

	public String getImageName() {
		return ImageName;
	}

	public void setImageName(String imageName) {
		ImageName = imageName;
	}

	public String getImagePath() {
		return ImagePath;
	}

	public void setImagePath(String imagePath) {
		ImagePath = imagePath;
	}

	public String getEmReading() {
		return EmReading;
	}

	public void setEmReading(String emReading) {
		EmReading = emReading;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}

	public Date getLmrTimestamp() {
		return LmrTimestamp;
	}

	public void setLmrTimestamp(Date lmrTimestamp) {
		LmrTimestamp = lmrTimestamp;
	}

	public String getLmrValue() {
		return LmrValue;
	}

	public void setLmrValue(String lmrValue) {
		LmrValue = lmrValue;
	}

	public String getPlantDowntime() {
		return PlantDowntime;
	}

	public void setPlantDowntime(String plantDowntime) {
		PlantDowntime = plantDowntime;
	}

	public Date getSlogDate() {
		return SlogDate;
	}

	public void setSlogDate(Date slogDate) {
		SlogDate = slogDate;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	public String getReportImageName() {
		return ReportImageName;
	}

	public void setReportImageName(String reportImageName) {
		ReportImageName = reportImageName;
	}

	public String getReportImagePath() {
		return ReportImagePath;
	}

	public void setReportImagePath(String reportImagePath) {
		ReportImagePath = reportImagePath;
	}

	public Date getReportTimestamp() {
		return ReportTimestamp;
	}

	public void setReportTimestamp(Date reportTimestamp) {
		ReportTimestamp = reportTimestamp;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Transient
	private Integer Ticketid;

	@Transient
	private String LmrTimestamptext;

	public String getLmrTimestamptext() {
		return LmrTimestamptext;
	}

	public void setLmrTimestamptext(String lmrTimestamptext) {
		LmrTimestamptext = lmrTimestamptext;
	}

	public Integer getTicketid() {
		return Ticketid;
	}

	public void setTicketid(Integer ticketid) {
		Ticketid = ticketid;
	}

}
