package com.mestech.eampm.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
/******************************************************
 * 
 * Filename :Country
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/
/*
 * This is our model class and it corresponds to Country table in database
 */
@Entity
@Table(name="mCountry")
public class Country implements Serializable{
	
	 private static final long serialVersionUID = -723583058586873479L;

	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "CountryID")
	 private Integer CountryId;
	
	@Column(name="CountryCode")
	private String CountryCode;	
	

	@Column(name="CountryName")
	private String CountryName;	
	
	@Column(name="RegionID")
	private Integer RegionID;	
	

	@Transient
	private String RegionName;	
	
	

	@Transient
	@JoinColumn(name="RegionID",referencedColumnName="CountryRegionID")
	@ManyToOne(cascade = CascadeType.ALL)
	private CountryRegion MdlCountryRegion;	
	
	public CountryRegion getCountryRegion() {
		return this.MdlCountryRegion;
	}
	
	public void setCountryRegion(CountryRegion countryRegion) {
		this.MdlCountryRegion = countryRegion;
	}
	
	
	@Column(name="ActiveFlag")
	private Integer ActiveFlag;	
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreationDate", insertable=false)
	private Date CreationDate;	
	
	@Column(name="CreatedBy" , nullable=true)
	private Integer CreatedBy;	
	
	
	@Column(name="LastUpdatedDate", nullable=true)
	private Date LastUpdatedDate;	
	
	@Column(name="LastUpdatedBy", nullable=true)
	private Integer LastUpdatedBy;	
	
	public Country() {
		super();
	}

	


	public Integer getCountryId() {
		return CountryId;
	}

	public void setCountryId(Integer countryId) {
		CountryId = countryId;
	}

	public String getCountryCode() {
		return CountryCode;
	}

	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}

	public String getCountryName() {
		return CountryName;
	}

	public void setCountryName(String countryName) {
		CountryName = countryName;
	}

	public Integer getRegionID() {
		return RegionID;
	}

	public void setRegionID(Integer regionID) {
		RegionID = regionID;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}




	public Country(Integer countryId, String countryCode, String countryName, Integer regionID, Integer activeFlag,
			Date creationDate, Integer createdBy, Date lastUpdatedDate, Integer lastUpdatedBy) {
		super();
		CountryId = countryId;
		CountryCode = countryCode;
		CountryName = countryName;
		RegionID = regionID;
		ActiveFlag = activeFlag;
		CreationDate = creationDate;
		CreatedBy = createdBy;
		LastUpdatedDate = lastUpdatedDate;
		LastUpdatedBy = lastUpdatedBy;
	}




	public String getRegionName() {
		return RegionName;
	}




	public void setRegionName(String regionName) {
		RegionName = regionName;
	}
	
	
	
	
	
	
}