package com.mestech.eampm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.mestech.eampm.utility.ErrorDetector;

/******************************************************
 * 
 * Filename :MparameterIntegratedStandards
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/
@Entity
@Table(name = "mparameterintegratedstandards")
public class MparameterIntegratedStandards implements Serializable {
	
	public MparameterIntegratedStandards(){
		super();
	}
	
	public MparameterIntegratedStandards(Integer row, BigInteger dataloggerId, String parameterName,
			String parameterUOM, String standardName, Integer activeFlag, BigInteger sequenceId,
			BigDecimal coefficient) {
		super();
		this.row = row;
		this.dataloggerId = dataloggerId;
		this.parameterName = parameterName;
		this.parameterUOM = parameterUOM;
		this.standardName = standardName;
		this.activeFlag = activeFlag;
		this.sequenceId = sequenceId;
		this.coefficient = coefficient;
	}

	private static final long serialVersionUID = 13241234;

	@Transient
	private Integer row;

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	@Transient
	private List<ErrorDetector> errorDetectors;

	public List<ErrorDetector> getErrorDetectors() {
		return errorDetectors;
	}

	public void setErrorDetectors(List<ErrorDetector> errorDetectors) {
		this.errorDetectors = errorDetectors;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "integratedid")
	private BigInteger integratedId;

	@Column(name = "dataloggerid")
	private BigInteger dataloggerId;

	@Column(name = "parametername")
	private String parameterName;

	@Column(name = "parameteruom")
	private String parameterUOM;

	@Column(name = "standardname")
	private String standardName;

	@Column(name = "activeflag")
	private Integer activeFlag;

	@Column(name = "creationdate")
	private Date creationDate;

	@Column(name = "createdby")
	private BigInteger createdBy;

	@Column(name = "lastUpdatedDate")
	private Date lastUpdatedDate;

	@Column(name = "lastupdatedby")
	private BigInteger lastUpdatedBy;

	@Column(name = "sequenceid")
	private BigInteger sequenceId;

	@Column(name = "coefficient")
	private BigDecimal coefficient;

	public BigInteger getIntegratedId() {
		return integratedId;
	}

	public void setIntegratedId(BigInteger integratedId) {
		this.integratedId = integratedId;
	}

	public BigInteger getDataloggerId() {
		return dataloggerId;
	}

	public void setDataloggerId(BigInteger dataloggerId) {
		this.dataloggerId = dataloggerId;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public String getParameterUOM() {
		return parameterUOM;
	}

	public void setParameterUOM(String parameterUOM) {
		this.parameterUOM = parameterUOM;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public Integer getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public BigInteger getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigInteger createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public BigInteger getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigInteger lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public BigInteger getSequenceId() {
		return sequenceId;
	}

	public void setSequenceId(BigInteger sequenceId) {
		this.sequenceId = sequenceId;
	}

	public BigDecimal getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(BigDecimal coefficient) {
		this.coefficient = coefficient;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
