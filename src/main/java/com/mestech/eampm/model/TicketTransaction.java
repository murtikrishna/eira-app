package com.mestech.eampm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/******************************************************
 * 
 * Filename :TicketTransaction
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/

@Entity
@Table(name = "tTicketTransaction")
public class TicketTransaction implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TransactionID")
	private Integer TransactionID;

	@Column(name = "TicketID")
	private Integer TicketID;

	@Column(name = "Description")
	private String Description;

	@Column(name = "AssignedTo")
	private Integer AssignedTo;

	@Column(name = "TimeStamp")
	private Date TimeStamp;

	@Column(name = "Remarks")
	private String Remarks;

	@Column(name = "TicketStatus")
	private Integer TicketStatus;

	@Column(name = "DayCycle")
	private Integer DayCycle;

	public Integer getDayCycle() {
		return DayCycle;
	}

	public void setDayCycle(Integer dayCycle) {
		DayCycle = dayCycle;
	}

	@Column(name = "ActiveFlag")
	private Integer ActiveFlag;

	@Column(name = "CreationDate")
	private Date CreationDate;

	@Column(name = "CreatedBy")
	private Integer CreatedBy;

	@Column(name = "LastUpdatedDate")
	private Date LastUpdatedDate;

	@Column(name = "LastUpdatedBy")
	private Integer LastUpdatedBy;

	public Integer getTransactionID() {
		return TransactionID;
	}

	public void setTransactionID(Integer transactionID) {
		TransactionID = transactionID;
	}

	public Integer getTicketID() {
		return TicketID;
	}

	public void setTicketID(Integer ticketID) {
		TicketID = ticketID;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public Integer getAssignedTo() {
		return AssignedTo;
	}

	public void setAssignedTo(Integer assignedTo) {
		AssignedTo = assignedTo;
	}

	public Date getTimeStamp() {
		return TimeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		TimeStamp = timeStamp;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}

	public Integer getTicketStatus() {
		return TicketStatus;
	}

	public void setTicketStatus(Integer ticketStatus) {
		TicketStatus = ticketStatus;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

}
