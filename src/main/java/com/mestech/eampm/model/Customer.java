package com.mestech.eampm.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
/******************************************************
 * 
 * Filename : Customer
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/

@Entity
@Table(name="mCustomer")
public class Customer implements Serializable{

	public String getCustomerPODateText() {
		return CustomerPODateText;
	}
	public void setCustomerPODateText(String customerPODateText) {
		CustomerPODateText = customerPODateText;
	}
	private static final long serialVersionUID = -723583058586873479L;
	 
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "CustomerID")
	 private Integer CustomerId;
	 
	 @Column(name = "CustomerTypeID")
	 private Integer CustomerTypeID;
	 

		public String getCustomerTypeName() {
		return CustomerTypeName;
	}
	public void setCustomerTypeName(String customerTypeName) {
		CustomerTypeName = customerTypeName;
	}
		@Transient
		private String CustomerTypeName;	
		
	
	 @Column(name = "CustomerCode")
		private String CustomerCode;
	 
	 @Column(name = "CustomerName")
		private String CustomerName;
	 
	 @Column(name = "CustomerOrganization")
		private String CustomerOrganization;
	 
	 @Column(name = "CustomerDescription")
		private String CustomerDescription;
	 
	 @Column(name = "CustomerGroup")
		private String CustomerGroup;
	 
	 @Column(name = "PrimaryCustomer")
		private Integer PrimaryCustomer;
	 

		@Transient
		private String PrimaryCustomerName;	
		
	 
	 @Column(name = "ContactPerson")
		private String ContactPerson;
	 
	 @Column(name = "Address")
		private String Address;
	 
	 @Column(name = "City")
		private String City;
	 
	 @Column(name = "StateID")
		private Integer StateID;
	 
	 
		@Transient
		private String StateName;	
		
		
	 @Column(name = "CountryID")
		private Integer CountryID;
	 
		@Transient
		private String CountryName;	
		
	 @Column(name = "PostalCode")
		private String PostalCode;
	 
	 @Column(name = "EmailID")
		private String EmailID;
	 
	 @Column(name = "Mobile")
		private String Mobile;
	 
	 @Column(name = "Telephone")
		private String Telephone;
	 
	 @Column(name = "Fax")
	 private String Fax;
	 
	 @Column(name = "Website")
		private String Website;
	 
	 @Column(name = "CustomerPONumber")
		private String CustomerPONumber;
	 
	 @Column(name = "CustomerPODate", nullable = true)
		private Date CustomerPODate;
	 
	 @Transient
	 private String CustomerPODateText;	
	 
	 @Column(name = "ImagePath", nullable = true)
	 private String ImagePath;	
		
	 
	 @Column(name = "ImageType")
	 private Integer ImageType;	
	 
		
		
	
	public Integer getImageType() {
		return ImageType;
	}
	public void setImageType(Integer imageType) {
		ImageType = imageType;
	}
	public String getImagePath() {
		return ImagePath;
	}
	public void setImagePath(String imagePath) {
		ImagePath = imagePath;
	}
	@Column(name = "CustomerLogo")
		private String CustomerLogo;
	 
	 @Column(name = "ActiveFlag")
		private Integer ActiveFlag;
	 
	 @Column(name = "CreatedBy")
		private Integer CreatedBy;
	 
	 @Column(name = "CreationDate")
		private Date CreationDate;
	 
	 @Column(name = "LastUpdatedBy")
	 private Integer LastUpdatedBy;
	 
	 @Column(name = "LastUpdatedDate")
		private Date LastUpdatedDate;
	 
	public Integer getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(Integer customerId) {
		CustomerId = customerId;
	}
	public Integer getCustomerTypeID() {
		return CustomerTypeID;
	}
	public void setCustomerTypeID(Integer customerTypeID) {
		CustomerTypeID = customerTypeID;
	}
	public String getCustomerCode() {
		return CustomerCode;
	}
	public void setCustomerCode(String customerCode) {
		CustomerCode = customerCode;
	}
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getCustomerOrganization() {
		return CustomerOrganization;
	}
	public void setCustomerOrganization(String customerOrganization) {
		CustomerOrganization = customerOrganization;
	}
	public String getCustomerDescription() {
		return CustomerDescription;
	}
	public void setCustomerDescription(String customerDescription) {
		CustomerDescription = customerDescription;
	}
	public String getCustomerGroup() {
		return CustomerGroup;
	}
	public void setCustomerGroup(String customerGroup) {
		CustomerGroup = customerGroup;
	}
	public Integer getPrimaryCustomer() {
		return PrimaryCustomer;
	}
	public void setPrimaryCustomer(Integer primaryCustomer) {
		PrimaryCustomer = primaryCustomer;
	}
	public String getContactPerson() {
		return ContactPerson;
	}
	public void setContactPerson(String contactPerson) {
		ContactPerson = contactPerson;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public Integer getStateID() {
		return StateID;
	}
	public void setStateID(Integer stateID) {
		StateID = stateID;
	}
	public Integer getCountryID() {
		return CountryID;
	}
	public void setCountryID(Integer countryID) {
		CountryID = countryID;
	}
	public String getPostalCode() {
		return PostalCode;
	}
	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}
	public String getEmailID() {
		return EmailID;
	}
	public void setEmailID(String emailID) {
		EmailID = emailID;
	}
	public String getMobile() {
		return Mobile;
	}
	public void setMobile(String mobile) {
		Mobile = mobile;
	}
	public String getTelephone() {
		return Telephone;
	}
	public void setTelephone(String telephone) {
		Telephone = telephone;
	}
	public String getFax() {
		return Fax;
	}
	public void setFax(String fax) {
		Fax = fax;
	}
	public String getWebsite() {
		return Website;
	}
	public void setWebsite(String website) {
		Website = website;
	}
	public String getCustomerPONumber() {
		return CustomerPONumber;
	}
	public void setCustomerPONumber(String customerPONumber) {
		CustomerPONumber = customerPONumber;
	}
	public Date getCustomerPODate() {
		return CustomerPODate;
	}
	public void setCustomerPODate(Date customerPODate) {
		CustomerPODate = customerPODate;
	}
	public String getCustomerLogo() {
		return CustomerLogo;
	}
	public void setCustomerLogo(String customerLogo) {
		CustomerLogo = customerLogo;
	}
	public Integer getActiveFlag() {
		return ActiveFlag;
	}
	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}
	public Integer getCreatedBy() {
		return CreatedBy;
	}
	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}
	public Date getCreationDate() {
		return CreationDate;
	}
	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}
	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}
	public String getPrimaryCustomerName() {
		return PrimaryCustomerName;
	}
	public void setPrimaryCustomerName(String primaryCustomerName) {
		PrimaryCustomerName = primaryCustomerName;
	}
	public String getStateName() {
		return StateName;
	}
	public void setStateName(String stateName) {
		StateName = stateName;
	}
	public String getCountryName() {
		return CountryName;
	}
	public void setCountryName(String countryName) {
		CountryName = countryName;
	}

	@Column(name = "CustomerReference")
	private String CustomerReference;


	public String getCustomerReference() {
		return CustomerReference;
	}
	public void setCustomerReference(String customerReference) {
		CustomerReference = customerReference;
	}
		
	 @Column(name = "PrimaryCustomerCode")
		private String PrimaryCustomerCode;


	public String getPrimaryCustomerCode() {
		return PrimaryCustomerCode;
	}
	public void setPrimaryCustomerCode(String primaryCustomerCode) {
		PrimaryCustomerCode = primaryCustomerCode;
	}
	
}
