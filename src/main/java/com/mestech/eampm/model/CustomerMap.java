
package com.mestech.eampm.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
/******************************************************
 * 
 * Filename : CustomerMap
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/

@Entity
@Table(name="mCustomerMap")
public class CustomerMap implements Serializable{

	 private static final long serialVersionUID = -723583058586873479L;
	 
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "MapID")
	 private Integer MapId;
	 
	 @Column(name = "UserID")
	 private Integer UserID;
	 
	 @Column(name="CustomerID")
	 private Integer CustomerID;
	
	 @Column(name="ActiveFlag")
	 private Integer ActiveFlag;

	 @Column(name="CreationDate")
	 private Date CreationDate;

	 @Column(name="CreatedBy")
	 private Integer CreatedBy;

	 @Column(name="LastUpdatedDate")
	 private Date LastUpdatedDate;

	 @Column(name="LastUpdatedBy")
	 private Integer LastUpdatedBy;

	public Integer getMapId() {
		return MapId;
	}

	public void setMapId(Integer mapId) {
		MapId = mapId;
	}

	public Integer getUserID() {
		return UserID;
	}

	public void setUserID(Integer userID) {
		UserID = userID;
	}

	public Integer getCustomerID() {
		return CustomerID;
	}

	public void setCustomerID(Integer customerID) {
		CustomerID = customerID;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}


	
	 @Transient
	 private String CustomerName;
	 
	 @Transient
	 private String UserName;

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}
	 

	
}
