
package com.mestech.eampm.model;


import java.io.Serializable;
import java.util.Date;

import javax.enterprise.inject.Default;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
/******************************************************
 * 
 * Filename :BatteryLimit
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/
@Entity
@Table(name="mBatteryLimits")
public class BatteryLimits implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;
	@Id
 	@GeneratedValue(strategy = GenerationType.IDENTITY)
 	@Column(name="Id")
	private Integer Id;
 
 	@Column(name="SiteId")
	 private Integer SiteId;
 	
 	
	 
	 @Column(name="EquipmentGroup")
	 private String EquipmentGroup;
	 
	 
	 @Column(name="StandardParameterName")
	 private String ParameterName;
	 
	 @Column(name="MinimumValue")
	 private String MinimumValue;
	 
	 @Column(name="MaximumValue")
	 private String MaximumValue;
	 
	 @Column(name="Uom")
	 private String Uom;
	 
	 @Column(name="Dataloggerid")
	 private Integer Dataloggerid;
	 
	
	 
	 @Column(name="ActiveFlag")
	 private Integer ActiveFlag;
	 
	 @Column(name="CreationDate")
	 private Date CreationDate;

	 @Column(name="LastUpdatedDate")
	 private Date LastUpdatedDate;
	
	 @Column(name="CreatedBy" , nullable=true)
	private Integer CreatedBy;	
			
	@Column(name="LastUpdatedBy", nullable=true)
	private Integer LastUpdatedBy;

	
	@Transient
	private String SiteName;
	
	@Transient
	private String DataloggerName;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public Integer getSiteId() {
		return SiteId;
	}

	public void setSiteId(Integer siteId) {
		SiteId = siteId;
	}

	public String getEquipmentGroup() {
		return EquipmentGroup;
	}

	public void setEquipmentGroup(String equipmentGroup) {
		EquipmentGroup = equipmentGroup;
	}

	public String getParameterName() {
		return ParameterName;
	}

	public void setParameterName(String parameterName) {
		ParameterName = parameterName;
	}

	public String getMinimumValue() {
		return MinimumValue;
	}

	public void setMinimumValue(String minimumValue) {
		MinimumValue = minimumValue;
	}

	public String getMaximumValue() {
		return MaximumValue;
	}

	public void setMaximumValue(String maximumValue) {
		MaximumValue = maximumValue;
	}

	public String getUom() {
		return Uom;
	}

	public void setUom(String uom) {
		Uom = uom;
	}

	public Integer getDataloggerid() {
		return Dataloggerid;
	}

	public void setDataloggerid(Integer dataloggerid) {
		Dataloggerid = dataloggerid;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	public String getSiteName() {
		return SiteName;
	}

	public void setSiteName(String siteName) {
		SiteName = siteName;
	}

	public String getDataloggerName() {
		return DataloggerName;
	}

	public void setDataloggerName(String dataloggerName) {
		DataloggerName = dataloggerName;
	}
	
	
	
	
}
