package com.mestech.eampm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/******************************************************
 * 
 * Filename :User
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/

@Entity
@Table(name = "mUser")
public class User implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "UserID")
	private Integer UserId;

	@Column(name = "UserCode")
	private String UserCode;

	@Column(name = "UserName")
	private String UserName;

	@Column(name = "Password")
	private String Password;

	@Column(name = "EncryptPassword")
	private String EncryptPassword;

	public String getEncryptPassword() {
		return EncryptPassword;
	}

	public void setEncryptPassword(String encryptPassword) {
		EncryptPassword = encryptPassword;
	}

	@Column(name = "Designation")
	private String Designation;

	@Column(name = "Department")
	private String Department;

	@Column(name = "RoleID")
	private Integer RoleID;

	@Column(name = "BloodGroup")
	private String BloodGroup;

	/*
	 * @Column(name="DateOfBirth") private Date DateOfBirth;
	 */

	@Column(name = "MobileNumber")
	private String MobileNumber;

	@Column(name = "EmailID")
	private String EmailID;

	@Column(name = "ActiveFlag")
	private Integer ActiveFlag;

	@Column(name = "CreationDate")
	private Date CreationDate;

	@Column(name = "CreatedBy")
	private Integer CreatedBy;

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	@Column(name = "LastUpdatedBy")
	private Integer LastUpdatedBy;

	/*
	 * @Column(name="DeviceID") private String DeviceID;
	 * 
	 * 
	 * @Column(name="RequestStatus") private String requestStatus;
	 * 
	 * 
	 * @Column(name="NewDeviceID") private String NewDeviceID;
	 * 
	 * 
	 * @Column(name="DeviceToken") private String DeviceToken;
	 */

	@Column(name = "LastUpdatedDate")
	private Date LastUpdatedDate;

	/*
	 * @Column(name="LastUpdatedBy") private Integer LastUpdatedBy;
	 */

	/*
	 * public String getDeviceID() { return DeviceID; }
	 * 
	 * 
	 * public void setDeviceID(String deviceID) { DeviceID = deviceID; }
	 * 
	 * 
	 * public String getRequestStatus() { return requestStatus; }
	 * 
	 * 
	 * public void setRequestStatus(String requestStatus) { this.requestStatus =
	 * requestStatus; }
	 * 
	 * 
	 * public String getNewDeviceID() { return NewDeviceID; }
	 * 
	 * 
	 * public void setNewDeviceID(String newDeviceID) { NewDeviceID = newDeviceID; }
	 * 
	 * 
	 * public String getDeviceToken() { return DeviceToken; }
	 * 
	 * 
	 * public void setDeviceToken(String deviceToken) { DeviceToken = deviceToken; }
	 */

	/*
	 * public Integer getLastUpdatedBy() { return LastUpdatedBy; }
	 * 
	 * 
	 * public void setLastUpdatedBy(Integer lastUpdatedBy) { LastUpdatedBy =
	 * lastUpdatedBy; }
	 */

	@Transient
	private String RoleName;

	public String getRoleName() {
		return RoleName;
	}

	public void setRoleName(String roleName) {
		RoleName = roleName;
	}

	@Transient
	private String ErrorMessage;

	public String getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

	public Integer getUserId() {
		return UserId;
	}

	public void setUserId(Integer userId) {
		UserId = userId;
	}

	public String getUserCode() {
		return UserCode;
	}

	public void setUserCode(String userCode) {
		UserCode = userCode;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getDesignation() {
		return Designation;
	}

	public void setDesignation(String designation) {
		Designation = designation;
	}

	public String getDepartment() {
		return Department;
	}

	public void setDepartment(String department) {
		Department = department;
	}

	public Integer getRoleID() {
		return RoleID;
	}

	public void setRoleID(Integer roleID) {
		RoleID = roleID;
	}

	public String getBloodGroup() {
		return BloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		BloodGroup = bloodGroup;
	}

	/*
	 * public Date getDateOfBirth() { return DateOfBirth; }
	 * 
	 * 
	 * public void setDateOfBirth(Date dateOfBirth) { DateOfBirth = dateOfBirth; }
	 */

	public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}

	public String getEmailID() {
		return EmailID;
	}

	public void setEmailID(String emailID) {
		EmailID = emailID;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	@Column(name = "FirstName")
	private String FirstName;

	@Column(name = "LastName")
	private String LastName;

	@Column(name = "ShortName")
	private String ShortName;

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getShortName() {
		return ShortName;
	}

	public void setShortName(String shortName) {
		ShortName = shortName;
	}

	@Transient
	private String localIp;

	public String getLocalIp() {
		return localIp;
	}

	public void setLocalIp(String localIp) {
		this.localIp = localIp;
	}

}
