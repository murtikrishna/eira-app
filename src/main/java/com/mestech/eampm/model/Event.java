package com.mestech.eampm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/******************************************************
 * 
 * Filename : Event
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/

@Entity
@Table(name = "mEvent")
public class Event implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EventID")
	private Integer EventId;

	@Column(name = "EventCode")
	private String EventCode;

	@Column(name = "EventName")
	private String EventName;

	@Column(name = "EventType")
	private String EventType;

	@Column(name = "EventDescription")
	private String EventDescription;

	@Column(name = "EquipmentTypeID")
	private Integer EquipmentTypeID;

	@Column(name = "Severity")
	private Integer Severity;

	@Column(name = "ActiveFlag")
	private Integer ActiveFlag;

	@Column(name = "CreationDate")
	private Date CreationDate;

	@Column(name = "CreatedBy")
	private Integer CreatedBy;

	@Column(name = "LastUpdatedDate")
	private Date LastUpdatedDate;

	@Column(name = "LastUpdatedBy")
	private Integer LastUpdatedBy;

	public Integer getEventId() {
		return EventId;
	}

	public void setEventId(Integer eventId) {
		EventId = eventId;
	}

	public String getEventCode() {
		return EventCode;
	}

	public void setEventCode(String eventCode) {
		EventCode = eventCode;
	}

	public String getEventName() {
		return EventName;
	}

	public void setEventName(String eventName) {
		EventName = eventName;
	}

	public String getEventType() {
		return EventType;
	}

	public void setEventType(String eventType) {
		EventType = eventType;
	}

	public String getEventDescription() {
		return EventDescription;
	}

	public void setEventDescription(String eventDescription) {
		EventDescription = eventDescription;
	}

	public Integer getEquipmentTypeID() {
		return EquipmentTypeID;
	}

	public void setEquipmentTypeID(Integer equipmentTypeID) {
		EquipmentTypeID = equipmentTypeID;
	}

	public Integer getSeverity() {
		return Severity;
	}

	public void setSeverity(Integer severity) {
		Severity = severity;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

}
