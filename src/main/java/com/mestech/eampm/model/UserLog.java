package com.mestech.eampm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/******************************************************
 * 
 * Filename :UserLog
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/

@Entity
@Table(name = "tUserLog")
public class UserLog implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TransactionId")
	private Integer TransactionId;

	public Integer getTransactionId() {
		return TransactionId;
	}

	public void setTransactionId(Integer transactionId) {
		TransactionId = transactionId;
	}

	public void setSessionId(String sessionId) {
		SessionId = sessionId;
	}

	@Column(name = "UserID")
	private Integer UserId;

	@Column(name = "UserName")
	private String UserName;

	@Column(name = "SessionId")
	private String SessionId;

	@Column(name = "DlogDate")
	private Date DlogDate;

	@Column(name = "ActiveFlag")
	private Integer ActiveFlag;

	@Column(name = "SlogDate")
	private Date SlogDate;

	@Column(name = "CreationDate")
	private Date CreationDate;

	@Column(name = "IpAddress")
	private String IpAddress;

	@Column(name = "LocalIpAddress")
	private String LocalIpAddress;

	@Column(name = "MacAddress")
	private String MacAddress;

	public String getMacAddress() {
		return MacAddress;
	}

	public void setMacAddress(String macAddress) {
		MacAddress = macAddress;
	}

	public String getIpAddress() {
		return IpAddress;
	}

	public void setIpAddress(String ipAddress) {
		IpAddress = ipAddress;
	}

	public String getLocalIpAddress() {
		return LocalIpAddress;
	}

	public void setLocalIpAddress(String localIpAddress) {
		LocalIpAddress = localIpAddress;
	}

	@Transient
	private String ErrorMessage;

	public Integer getUserId() {
		return UserId;
	}

	public void setUserId(Integer userId) {
		UserId = userId;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getSessionId() {
		return SessionId;
	}

	public void setSessioId(String sessionId) {
		SessionId = sessionId;
	}

	public Date getDlogDate() {
		return DlogDate;
	}

	public void setDlogDate(Date dlogDate) {
		DlogDate = dlogDate;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getSlogDate() {
		return SlogDate;
	}

	public void setSlogDate(Date slogDate) {
		SlogDate = slogDate;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public String getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
