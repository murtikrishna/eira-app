package com.mestech.eampm.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/******************************************************
 * 
 * Filename :EquipmentStatus
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/

@Entity
@Table(name="tEquipmentStatus")
public class EquipmentStatus implements Serializable{

	 private static final long serialVersionUID = -723583058586873479L;
	 

	@Id
	@Column(name="EquipmentID")
	 private Integer EquipmentID;
	
	@Column(name = "SiteID")
	 private Integer SiteID;
	 
	 
	 @Column(name="PhysicalStatus")
	 private String PhysicalStatus;

	 @Column(name="OperationalStatus")
	 private String OperationalStatus;

	 @Column(name="EquipmentStatus")
	 private String EquipmentStatus;


	 @Column(name="ErrorCode")
	 private String ErrorCode;
	 

	 @Column(name="ErrorDetail")
	 private String ErrorDetail;
	 

	 @Column(name="ErrorDescription")
	 private String ErrorDescription;
	 

	 @Column(name="ErrorTimestamp")
	 private Date ErrorTimestamp;
	 
	 

		@Column(name="ActiveFlag")
		 private Integer ActiveFlag;

		 @Column(name="CreationDate")
		 private Date CreationDate;

		 @Column(name="CreatedBy")
		 private Integer CreatedBy;

		 @Column(name="LastUpdatedDate")
		 private Date LastUpdatedDate;

		 @Column(name="LastUpdatedBy")
		 private Integer LastUpdatedBy;

		 
	  public Integer getEquipmentID() {
		return EquipmentID;
	}

	public void setEquipmentID(Integer equipmentID) {
		EquipmentID = equipmentID;
	}

	public Integer getSiteID() {
		return SiteID;
	}

	public void setSiteID(Integer siteID) {
		SiteID = siteID;
	}

	public String getPhysicalStatus() {
		return PhysicalStatus;
	}

	public void setPhysicalStatus(String physicalStatus) {
		PhysicalStatus = physicalStatus;
	}

	public String getOperationalStatus() {
		return OperationalStatus;
	}

	public void setOperationalStatus(String operationalStatus) {
		OperationalStatus = operationalStatus;
	}

	

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public String getEquipmentStatus() {
		return EquipmentStatus;
	}

	public void setEquipmentStatus(String equipmentStatus) {
		EquipmentStatus = equipmentStatus;
	}

	public String getErrorCode() {
		return ErrorCode;
	}

	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}

	public String getErrorDetail() {
		return ErrorDetail;
	}

	public void setErrorDetail(String errorDetail) {
		ErrorDetail = errorDetail;
	}

	public String getErrorDescription() {
		return ErrorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		ErrorDescription = errorDescription;
	}

	public Date getErrorTimestamp() {
		return ErrorTimestamp;
	}

	public void setErrorTimestamp(Date errorTimestamp) {
		ErrorTimestamp = errorTimestamp;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}


		 
	}
