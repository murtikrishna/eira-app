
/******************************************************
 * 
 *    	Filename	: Activity.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class used to access Activity table..
 *      
 *      
 *******************************************************/
package com.mestech.eampm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/******************************************************
 * 
 * Filename : Activity
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/
@Entity
@Table(name = "mActivity")
public class Activity implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;

	// @Id
	// //@Default
	// @GeneratedValue(strategy = GenerationType.IDENTITY, generator =
	// "system-uuid")
	// @GenericGenerator(name = "system-uuid", strategy = "uuid")
	// @Column(name = "ActivityId")
	// private Integer ActivityId;

	// @Id
	// @Column(name = "ActivityID")
	// @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_Sequence")
	// @SequenceGenerator(name = "id_Sequence", sequenceName = "ID_SEQ")
	// private int ActivityId;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ActivityId")
	private int ActivityId;

	@Column(name = "ActivityName")
	private String ActivityName;

	@Column(name = "ActivityDescription")
	private String ActivityDescription;

	@Column(name = "ActiveFlag")
	private Integer ActiveFlag;

	@Column(name = "CreationDate")
	private Date CreationDate;

	@Column(name = "LastUpdatedDate")
	private Date LastUpdatedDate;
	private String activityUrl;
	/*
	 * @OneToOne
	 * 
	 * @JoinColumn(name="FlagStatusId") private FlagStatus FlagStatus;
	 */

	// private String ActiveFlagText;

	public Integer getActivityId() {
		return ActivityId;
	}

	public String getActivityUrl() {
		return activityUrl;
	}

	public void setActivityUrl(String activityUrl) {
		this.activityUrl = activityUrl;
	}

	public void setActivityId(int activityId) {
		ActivityId = activityId;
	}

	public void setActivityId(Integer activityId) {
		ActivityId = activityId;
	}

	public String getActivityName() {
		return ActivityName;
	}

	public void setActivityName(String activityName) {
		ActivityName = activityName;
	}

	public String getActivityDescription() {
		return ActivityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		ActivityDescription = activityDescription;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	/*
	 * public FlagStatus getFlagStatus() { return FlagStatus; }
	 * 
	 * public void setFlagStatus(FlagStatus flagStatus) { FlagStatus = flagStatus; }
	 */

	/*
	 * public String getActiveFlagText() { return ActiveFlagText; }
	 * 
	 * public void setActiveFlagText(String activeFlagText) { ActiveFlagText =
	 * activeFlagText; }
	 */

}
