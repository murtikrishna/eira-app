package com.mestech.eampm.model;


import java.io.Serializable;
import java.util.Date;

import javax.enterprise.inject.Default;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

/******************************************************
 * 
 * Filename :ErrorCode
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/
@Entity
@Table(name="mErrorCode")
public class ErrorCode implements Serializable {
	
 private static final long serialVersionUID = -723583058586873479L;
	 
 	 
 	@Id
 	@GeneratedValue(strategy = GenerationType.IDENTITY)
 	@Column(name="ErrorId")
	private Integer ErrorId;
 
 	@Column(name="ErrorCode")
	 private String ErrorCode;
	 
	 @Column(name="ErrorMessage")
	 private String ErrorMessage;
	 
	 @Column(name="ComponentType")
	 private String ComponentType;
	 
	 @Column(name="ErrorDescription")
	 private String ErrorDescription;

	 @Column(name="EquipmentTypeId")
	 private Integer EquipmentTypeId;
	 
	 @Column(name="EquipmentType")
	 private String EquipmentType;
	 
	 @Column(name="EquipmentCategory")
	 private String EquipmentCategory;
	 
	 @Column(name="MessageType" , nullable=true)
	 private String MessageType;
	 
	 @Column(name="Severity" , nullable=true)
	 private Integer Severity;

	 @Column(name="ActiveFlag")
	 private Integer ActiveFlag;
	 
	 @Column(name="CreationDate")
	 private Date CreationDate;

	 @Column(name="LastUpdatedDate")
	 private Date LastUpdatedDate;
	
	 @Column(name="CreatedBy" , nullable=true)
	private Integer CreatedBy;	
			
	@Column(name="LastUpdatedBy", nullable=true)
	private Integer LastUpdatedBy;
	
	@Column(name="Priority")
	private Integer Priority;
	
	@Column(name="EiraErrorMessage")
	private String EiraErrorMessage;
	
	
	 @Column(name="ApprovedBy")
	 private String ApprovedBy;

	public Integer getErrorId() {
		return ErrorId;
	}

	public void setErrorId(Integer errorId) {
		ErrorId = errorId;
	}

	public String getErrorCode() {
		return ErrorCode;
	}

	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}

	public String getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

	public String getComponentType() {
		return ComponentType;
	}

	public void setComponentType(String componentType) {
		ComponentType = componentType;
	}

	public String getErrorDescription() {
		return ErrorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		ErrorDescription = errorDescription;
	}

	public Integer getEquipmentTypeId() {
		return EquipmentTypeId;
	}

	public void setEquipmentTypeId(Integer equipmentTypeId) {
		EquipmentTypeId = equipmentTypeId;
	}

	public String getEquipmentType() {
		return EquipmentType;
	}

	public void setEquipmentType(String equipmentType) {
		EquipmentType = equipmentType;
	}

	public String getEquipmentCategory() {
		return EquipmentCategory;
	}

	public void setEquipmentCategory(String equipmentCategory) {
		EquipmentCategory = equipmentCategory;
	}

	public String getMessageType() {
		return MessageType;
	}

	public void setMessageType(String messageType) {
		MessageType = messageType;
	}

	public Integer getSeverity() {
		return Severity;
	}

	public void setSeverity(Integer severity) {
		Severity = severity;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getPriority() {
		return Priority;
	}

	public void setPriority(Integer priority) {
		Priority = priority;
	}

	public String getEiraErrorMessage() {
		return EiraErrorMessage;
	}

	public void setEiraErrorMessage(String eiraErrorMessage) {
		EiraErrorMessage = eiraErrorMessage;
	}

	public String getApprovedBy() {
		return ApprovedBy;
	}

	public void setApprovedBy(String approvedBy) {
		ApprovedBy = approvedBy;
	}	
	     

	@Transient
	 private String CreationDateText;
	
	@Transient
	 private String LastUpdatedDateText;


	public String getCreationDateText() {
		return CreationDateText;
	}

	public void setCreationDateText(String creationDateText) {
		CreationDateText = creationDateText;
	}

	public String getLastUpdatedDateText() {
		return LastUpdatedDateText;
	}

	public void setLastUpdatedDateText(String lastUpdatedDateText) {
		LastUpdatedDateText = lastUpdatedDateText;
	}
	
	
	@Column(name="Code")
	private String Code;

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}
	 
	 
}