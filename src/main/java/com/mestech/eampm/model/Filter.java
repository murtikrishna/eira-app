package com.mestech.eampm.model;


import java.io.Serializable;
import java.util.Date;

import javax.enterprise.inject.Default;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/******************************************************
 * 
 * Filename :Filter
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/
@Entity
@Table(name="mFilter")
public class Filter implements Serializable {
	
 private static final long serialVersionUID = -723583058586873479L;
	 
	 

 	 
 	@Id
 	@GeneratedValue(strategy = GenerationType.IDENTITY)
 	@Column(name="FilterId")
	private Integer FilterId;
 
	 
	 @Column(name="FilterName")
	 private String FilterName;
	 
	 @Column(name="FilterValue")
	 private String FilterValue;
	 
	 @Column(name="FilterText")
	 private String FilterText;
	 
	 @Column(name="ActiveFlag")
	 private Integer ActiveFlag;

	 @Column(name="Constraint1")
	 private String Constraint1;
	 
	 @Column(name="Constraint2")
	 private String Constraint2;

	 @Column(name="Constraint3")
	 private String Constraint3;

	public Integer getFilterId() {
		return FilterId;
	}

	public void setFilterId(Integer filterId) {
		FilterId = filterId;
	}

	public String getFilterName() {
		return FilterName;
	}

	public void setFilterName(String filterName) {
		FilterName = filterName;
	}

	public String getFilterValue() {
		return FilterValue;
	}

	public void setFilterValue(String filterValue) {
		FilterValue = filterValue;
	}

	public String getFilterText() {
		return FilterText;
	}

	public void setFilterText(String filterText) {
		FilterText = filterText;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public String getConstraint1() {
		return Constraint1;
	}

	public void setConstraint1(String constraint1) {
		Constraint1 = constraint1;
	}

	public String getConstraint2() {
		return Constraint2;
	}

	public void setConstraint2(String constraint2) {
		Constraint2 = constraint2;
	}

	public String getConstraint3() {
		return Constraint3;
	}

	public void setConstraint3(String constraint3) {
		Constraint3 = constraint3;
	}



	
}
