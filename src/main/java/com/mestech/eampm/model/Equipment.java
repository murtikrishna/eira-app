package com.mestech.eampm.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mestech.eampm.utility.ErrorDetector;

/******************************************************
 * 
 * Filename : Equipment
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/
@Entity
@Table(name = "mEquipment")
public class Equipment implements Serializable {
	private static final long serialVersionUID = -723583058586873479L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EquipmentID")
	private Integer EquipmentId;
	@Transient
	private Integer row;
	@Transient
	private List<ErrorDetector> errorDetectors;
	@Transient
	private String warrentyDateInString;
	@Transient
	private String installationDateInString;

	public Equipment() {
		this.DismandalFlag = 0;
	}

	public String getWarrentyDateInString() {
		return warrentyDateInString;
	}

	public void setWarrentyDateInString(String warrentyDateInString) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			WarrantyDate = dateFormat.parse(warrentyDateInString.concat(" ").concat("00:00:00"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.warrentyDateInString = warrentyDateInString;
	}

	public String getInstallationDateInString() {
		return installationDateInString;
	}

	public void setInstallationDateInString(String installationDateInString) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			InstallationDate = dateFormat.parse(installationDateInString.concat(" ").concat("00:00:00"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.installationDateInString = installationDateInString;
	}

	public List<ErrorDetector> getErrorDetectors() {
		return errorDetectors;
	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public void setErrorDetectors(List<ErrorDetector> errorDetectors) {
		this.errorDetectors = errorDetectors;
	}

	public Integer getSiteID() {
		return SiteID;
	}

	public void setSiteID(Integer siteID) {
		SiteID = siteID;
	}

	@Column(name = "EquipmentTypeID")
	private Integer EquipmentTypeID;

	@Column(name = "SiteID")
	private Integer SiteID;

	public String getEquipmentTypeName() {
		return EquipmentTypeName;
	}

	public void setEquipmentTypeName(String equipmentTypeName) {
		EquipmentTypeName = equipmentTypeName;
	}

	@Transient
	private String EquipmentTypeName;

	@Transient
	private String SiteName;

	public String getSiteName() {
		return SiteName;
	}

	public void setSiteName(String siteName) {
		SiteName = siteName;
	}

	@Column(name = "EquipmentCode")
	private String EquipmentCode;

	@Column(name = "PrimarySerialNumber")
	private String PrimarySerialNumber;

	@Column(name = "Description")
	private String Description;

	@Column(name = "DisplayName")
	private String DisplayName;

	@Column(name = "DLConfigurationID")
	private Integer DLConfigurationID;

	@Column(name = "Remarks")
	private String Remarks;

	@Column(name = "CustomerReference")
	private String CustomerReference;

	@Column(name = "CustomerNaming")
	private String CustomerNaming;

	@Column(name = "Capacity", nullable = false)
	private Double Capacity;

	@Column(name = "InstallationDate")
	private Date InstallationDate;

	@Column(name = "WarrantyDate")
	private Date WarrantyDate;

	@Column(name = "ActiveFlag")
	private Integer ActiveFlag;

	@Column(name = "CreationDate")
	private Date CreationDate;

	@Column(name = "CreatedBy")
	private Integer CreatedBy;

	@Column(name = "LastUpdatedDate")
	private Date LastUpdatedDate;

	@Column(name = "LastUpdatedBy")
	private Integer LastUpdatedBy;

	@Column(name = "DismandalFlag")
	private Integer DismandalFlag;

	public Integer getDismandalFlag() {
		return DismandalFlag;
	}

	public void setDismandalFlag(Integer dismandalFlag) {
		DismandalFlag = dismandalFlag;
	}

	public Integer getEquipmentId() {
		return EquipmentId;
	}

	public void setEquipmentId(Integer equipmentId) {
		EquipmentId = equipmentId;
	}

	public Integer getEquipmentTypeID() {
		return EquipmentTypeID;
	}

	public void setEquipmentTypeID(Integer equipmentTypeID) {
		EquipmentTypeID = equipmentTypeID;
	}

	public String getEquipmentCode() {
		return EquipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		EquipmentCode = equipmentCode;
	}

	public String getPrimarySerialNumber() {
		return PrimarySerialNumber;
	}

	public void setPrimarySerialNumber(String primarySerialNumber) {
		PrimarySerialNumber = primarySerialNumber;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getDisplayName() {
		return DisplayName;
	}

	public void setDisplayName(String displayName) {
		DisplayName = displayName;
	}

	public Integer getDLConfigurationID() {
		return DLConfigurationID;
	}

	public void setDLConfigurationID(Integer dLConfigurationID) {
		DLConfigurationID = dLConfigurationID;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}

	public String getCustomerReference() {
		return CustomerReference;
	}

	public void setCustomerReference(String customerReference) {
		CustomerReference = customerReference;
	}

	public String getCustomerNaming() {
		return CustomerNaming;
	}

	public void setCustomerNaming(String customerNaming) {
		CustomerNaming = customerNaming;
	}

	public Double getCapacity() {
		return Capacity;
	}

	public void setCapacity(Double capacity) {
		Capacity = capacity;
	}

	public Date getInstallationDate() {
		return InstallationDate;
	}

	public void setInstallationDate(Date installationDate) {
		InstallationDate = installationDate;
	}

	public Date getWarrantyDate() {
		return WarrantyDate;
	}

	public void setWarrantyDate(Date warrantyDate) {
		WarrantyDate = warrantyDate;
	}

	@Transient
	public String WarrantyDateText;
	@Transient
	public String InstallationDateText;

	public String getWarrantyDateText() {
		return WarrantyDateText;
	}

	public void setWarrantyDateText(String warrantyDateText) {
		WarrantyDateText = warrantyDateText;
	}

	public String getInstallationDateText() {
		return InstallationDateText;
	}

	public void setInstallationDateText(String installationDateText) {
		InstallationDateText = installationDateText;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	@Transient
	private String SiteReference;

	public String getSiteReference() {
		return SiteReference;
	}

	public void setSiteReference(String siteReference) {
		SiteReference = siteReference;
	}

	@Transient
	private String CategoryName;

	public String getCategoryName() {
		return CategoryName;
	}

	public void setCategoryName(String categoryName) {
		CategoryName = categoryName;
	}

	@Column(name = "EquipmentSelection")
	private String EquipmentSelection;

	@Column(name = "Components")
	private String Components;

	@Column(name = "DisconnectRating")
	private String DisconnectRating;

	@Column(name = "DisconnectType")
	private String DisconnectType;

	public String getEquipmentSelection() {
		return EquipmentSelection;
	}

	public void setEquipmentSelection(String equipmentSelection) {
		EquipmentSelection = equipmentSelection;
	}

	public String getComponents() {
		return Components;
	}

	public void setComponents(String components) {
		Components = components;
	}

	public String getDisconnectRating() {
		return DisconnectRating;
	}

	public void setDisconnectRating(String disconnectRating) {
		DisconnectRating = disconnectRating;
	}

	public String getDisconnectType() {
		return DisconnectType;
	}

	public void setDisconnectType(String disconnectType) {
		DisconnectType = disconnectType;
	}

}
