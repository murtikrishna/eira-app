package com.mestech.eampm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/******************************************************
 * 
 * Filename :SiteStatus
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/

@Entity
@Table(name = "tSiteStatus")
public class SiteStatus implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;

	@Id
	@Column(name = "SiteID", unique = true, nullable = false)
	private Integer SiteId;

	@Column(name = "CustomerID")
	private Integer CustomerID;

	@Column(name = "TodayEnergy")
	private String TodayEnergy;

	@Column(name = "MonthEnergy")
	private String MonthEnergy;

	@Column(name = "TotalEnergy")
	private String TotalEnergy;

	@Column(name = "PhysicalStatus")
	private String PhysicalStatus;

	@Column(name = "OperationalStatus")
	private String OperationalStatus;

	@Column(name = "SiteStatus")
	private String SiteStatus;

	@Column(name = "ErrorCode")
	private String ErrorCode;

	@Column(name = "ErrorDetail")
	private String ErrorDetail;

	@Column(name = "ErrorDescription")
	private String ErrorDescription;

	@Column(name = "ErrorTimestamp")
	private Date ErrorTimestamp;

	@Column(name = "ActiveFlag")
	private Integer ActiveFlag;

	@Column(name = "CreationDate")
	private Date CreationDate;

	@Column(name = "CreatedBy")
	private Integer CreatedBy;

	@Column(name = "LastUpdatedDate")
	private Date LastUpdatedDate;

	@Column(name = "LastUpdatedBy")
	private Integer LastUpdatedBy;

	/*
	 * @OneToOne(fetch = FetchType.LAZY)
	 * 
	 * @PrimaryKeyJoinColumn private Site site;
	 * 
	 * 
	 * public Site getSite() { return site; }
	 * 
	 * public void setSite(Site site) { site = site; }
	 */

	/*
	 * @Transient private Site site;
	 * 
	 * 
	 * @OneToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name="SiteID") public Site getSite() { return site; }
	 * 
	 * public void setSite(Site site) { site = site; }
	 */

	@Column(name = "Irradiation")
	private String Irradiation;

	public SiteStatus() {

	}

	public SiteStatus(Integer siteId, Integer customerID, String siteStatus, Integer activeFlag, Date creationDate) {
		super();
		SiteId = siteId;
		CustomerID = customerID;
		SiteStatus = siteStatus;
		ActiveFlag = activeFlag;
		CreationDate = creationDate;
	}

	public String getIrradiation() {
		return Irradiation;
	}

	public void setIrradiation(String irradiation) {
		Irradiation = irradiation;
	}

	@Transient
	@OneToOne
	@JoinColumn(name = "SiteID")
	private Site site;

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		site = site;
	}

	public Integer getSiteId() {
		return SiteId;
	}

	public void setSiteId(Integer siteId) {
		SiteId = siteId;
	}

	public Integer getCustomerID() {
		return CustomerID;
	}

	public void setCustomerID(Integer customerID) {
		CustomerID = customerID;
	}

	public String getTodayEnergy() {
		return TodayEnergy;
	}

	public void setTodayEnergy(String todayEnergy) {
		TodayEnergy = todayEnergy;
	}

	public String getMonthEnergy() {
		return MonthEnergy;
	}

	public void setMonthEnergy(String monthEnergy) {
		MonthEnergy = monthEnergy;
	}

	public String getTotalEnergy() {
		return TotalEnergy;
	}

	public void setTotalEnergy(String totalEnergy) {
		TotalEnergy = totalEnergy;
	}

	public String getPhysicalStatus() {
		return PhysicalStatus;
	}

	public void setPhysicalStatus(String physicalStatus) {
		PhysicalStatus = physicalStatus;
	}

	public String getOperationalStatus() {
		return OperationalStatus;
	}

	public void setOperationalStatus(String operationalStatus) {
		OperationalStatus = operationalStatus;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public String getSiteStatus() {
		return SiteStatus;
	}

	public void setSiteStatus(String siteStatus) {
		SiteStatus = siteStatus;
	}

	public String getErrorCode() {
		return ErrorCode;
	}

	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}

	public String getErrorDetail() {
		return ErrorDetail;
	}

	public void setErrorDetail(String errorDetail) {
		ErrorDetail = errorDetail;
	}

	public String getErrorDescription() {
		return ErrorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		ErrorDescription = errorDescription;
	}

	public Date getErrorTimestamp() {
		return ErrorTimestamp;
	}

	public void setErrorTimestamp(Date errorTimestamp) {
		ErrorTimestamp = errorTimestamp;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

}
