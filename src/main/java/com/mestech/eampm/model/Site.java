package com.mestech.eampm.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/******************************************************
 * 
 * Filename :Site
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/
@Entity
@Table(name = "mSite")
public class Site implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SiteID")
	private Integer SiteId;

	private Integer serviceFlag2, serviceFlag3, serviceFlag4;

	public Integer getServiceFlag2() {
		return serviceFlag2;
	}

	public void setServiceFlag2(Integer serviceFlag2) {
		this.serviceFlag2 = serviceFlag2;
	}

	public Integer getServiceFlag3() {
		return serviceFlag3;
	}

	public void setServiceFlag3(Integer serviceFlag3) {
		this.serviceFlag3 = serviceFlag3;
	}

	public Integer getServiceFlag4() {
		return serviceFlag4;
	}

	public void setServiceFlag4(Integer serviceFlag4) {
		this.serviceFlag4 = serviceFlag4;
	}

	@Column(name = "CustomerID")
	private Integer CustomerID;

	@Transient
	private String CustomerName;

	@Column(name = "SiteTypeID")
	private Integer SiteTypeID;

	@Transient
	private String SiteTypeName;

	private BigInteger Dataloggertypeid, DataLoggerID_Sensor;

	public BigInteger getDataloggertypeid() {
		return Dataloggertypeid;
	}

	public void setDataloggertypeid(BigInteger dataloggertypeid) {
		Dataloggertypeid = dataloggertypeid;
	}

	@Column(name = "SiteCode")
	private String SiteCode;

	@Column(name = "ProdFlag")
	private Integer ProdFlag;

	public Integer getProdFlag() {
		return ProdFlag;
	}

	public void setProdFlag(Integer prodFlag) {
		ProdFlag = prodFlag;
	}

	@Column(name = "SiteName")
	private String SiteName;

	@Column(name = "SiteDescription")
	private String SiteDescription;

	@Column(name = "ContactPerson")
	private String ContactPerson;

	@Column(name = "Address")
	private String Address;

	@Column(name = "City")
	private String City;

	@Column(name = "StateID")
	private Integer StateID;

	@Transient
	private String StateName;

	@Transient
	private String SitePoDateText;

	@Transient
	private String CodText;

	private BigInteger ApiScheduleInterval;

	private Integer ApiDataInterval, DataLoggerID_ENERGYMETER, ServiceCode_ENERGYMETER, DataLoggerID_SCB,
			ServiceCode_SCB, DataLoggerID_TRACKER, ServiceCode_TRACKER;
	private String Sensorkey, SensorUrl, EnergyMeterkey, EnergyMeterUrl, Scbkey, ScbUrl, Trackerkey, TrackerUrl;

	public Integer getDataLoggerID_TRACKER() {
		return DataLoggerID_TRACKER;
	}

	public void setDataLoggerID_TRACKER(Integer dataLoggerID_TRACKER) {
		DataLoggerID_TRACKER = dataLoggerID_TRACKER;
	}

	public Integer getServiceCode_TRACKER() {
		return ServiceCode_TRACKER;
	}

	public void setServiceCode_TRACKER(Integer serviceCode_TRACKER) {
		ServiceCode_TRACKER = serviceCode_TRACKER;
	}

	public Integer getDataLoggerID_SCB() {
		return DataLoggerID_SCB;
	}

	public void setDataLoggerID_SCB(Integer dataLoggerID_SCB) {
		DataLoggerID_SCB = dataLoggerID_SCB;
	}

	public Integer getServiceCode_SCB() {
		return ServiceCode_SCB;
	}

	public void setServiceCode_SCB(Integer serviceCode_SCB) {
		ServiceCode_SCB = serviceCode_SCB;
	}

	public Integer getDataLoggerID_ENERGYMETER() {
		return DataLoggerID_ENERGYMETER;
	}

	public void setDataLoggerID_ENERGYMETER(Integer dataLoggerID_ENERGYMETER) {
		DataLoggerID_ENERGYMETER = dataLoggerID_ENERGYMETER;
	}

	public Integer getServiceCode_ENERGYMETER() {
		return ServiceCode_ENERGYMETER;
	}

	public void setServiceCode_ENERGYMETER(Integer serviceCode_ENERGYMETER) {
		ServiceCode_ENERGYMETER = serviceCode_ENERGYMETER;
	}

	public Integer getApiDataInterval() {
		return ApiDataInterval;
	}

	public void setApiDataInterval(Integer apiDataInterval) {
		ApiDataInterval = apiDataInterval;
	}

	public BigInteger getApiScheduleInterval() {
		return ApiScheduleInterval;
	}

	public void setApiScheduleInterval(BigInteger apiScheduleInterval) {
		ApiScheduleInterval = apiScheduleInterval;
	}

	public String getSensorkey() {
		return Sensorkey;
	}

	public void setSensorkey(String sensorkey) {
		Sensorkey = sensorkey;
	}

	public String getSensorUrl() {
		return SensorUrl;
	}

	public void setSensorUrl(String sensorUrl) {
		SensorUrl = sensorUrl;
	}

	public String getEnergyMeterkey() {
		return EnergyMeterkey;
	}

	public void setEnergyMeterkey(String energyMeterkey) {
		EnergyMeterkey = energyMeterkey;
	}

	public String getEnergyMeterUrl() {
		return EnergyMeterUrl;
	}

	public void setEnergyMeterUrl(String energyMeterUrl) {
		EnergyMeterUrl = energyMeterUrl;
	}

	public String getScbkey() {
		return Scbkey;
	}

	public void setScbkey(String scbkey) {
		Scbkey = scbkey;
	}

	public String getScbUrl() {
		return ScbUrl;
	}

	public void setScbUrl(String scbUrl) {
		ScbUrl = scbUrl;
	}

	public String getTrackerkey() {
		return Trackerkey;
	}

	public void setTrackerkey(String trackerkey) {
		Trackerkey = trackerkey;
	}

	public String getTrackerUrl() {
		return TrackerUrl;
	}

	public void setTrackerUrl(String trackerUrl) {
		TrackerUrl = trackerUrl;
	}

	public String getSitePoDateText() {
		return SitePoDateText;
	}

	public void setSitePoDateText(String sitePoDateText) {
		SitePoDateText = sitePoDateText;
	}

	@Column(name = "CountryID")
	private Integer CountryID;

	@Transient
	private String CountryName;

	@Column(name = "PostalCode")
	private String PostalCode;

	@Column(name = "Longitude")
	private String Longitude;

	@Column(name = "Latitude")
	private String Latitude;

	@Column(name = "Altitude")
	private String Altitude;

	@Column(name = "SiteImage")
	private String SiteImage;

	@Column(name = "InstallationCapacity")
	private double InstallationCapacity;

	@Column(name = "SitePONumber")
	private String SitePONumber;

	@Column(name = "SitePODate")
	private Date SitePODate;

	@Column(name = "Cod")
	private Date Cod;

	/*
	 * @Column(name="SiteHandoverDate") private Date SiteHandoverDate;
	 * 
	 * 
	 * @Column(name="SiteCommisioningDate") private Date SiteCommisioningDate;
	 * 
	 * 
	 * @Column(name="InstalledOn") private Date InstalledOn;
	 */

	@Column(name = "SiteOperator")
	private String SiteOperator;

	@Column(name = "SiteManafacturer")
	private String SiteManafacturer;

	@Column(name = "ModuleName")
	private String ModuleName;

	@Column(name = "CommunicationType")
	private String CommunicationType;

	@Column(name = "CollectionType")
	private String CollectionType;

	@Column(name = "FileType")
	private String FileType;

	@Column(name = "CustomerReference")
	private String CustomerReference;

	@Column(name = "CustomerNaming")
	private String CustomerNaming;

	/*
	 * @Column(name="Income") private float Income;
	 */

	@Column(name = "CurrencyID")
	private Integer CurrencyID;

	@Transient
	private String CurrencyName;

	@Column(name = "EmailID")
	private String EmailID;

	@Column(name = "Mobile")
	private String Mobile;

	@Column(name = "Telephone")
	private String Telephone;

	@Column(name = "Fax")
	private String Fax;

	@Column(name = "ActiveFlag")
	private Integer ActiveFlag;

	@Column(name = "CreationDate")
	private Date CreationDate;

	@Column(name = "CreatedBy")
	private Integer CreatedBy;

	@Column(name = "LastUpdatedDate")
	private Date LastUpdatedDate;

	@Column(name = "LastUpdatedBy")
	private Integer LastUpdatedBy;

	@Column(name = "DataLoggerID")
	private Integer DataLoggerID;

	@Column(name = "OperationMode")
	private Integer OperationMode;

	@Column(name = "RemoteFtpServer")
	private String RemoteFtpServer;

	@Column(name = "RemoteFtpUserName")
	private String RemoteFtpUserName;

	@Column(name = "RemoteFtpPassword")
	private String RemoteFtpPassword;

	@Column(name = "RemoteFtpServerPort")
	private String RemoteFtpServerPort;

	@Column(name = "RemoteFtpDirectoryPath")
	private String RemoteFtpDirectoryPath;

	@Column(name = "FilePrefix")
	private String FilePrefix;

	@Column(name = "FileSuffix")
	private String FileSuffix;

	@Column(name = "FileSubstring")
	private String FileSubstring;

	@Column(name = "SubFolder")
	private Integer SubFolder;

	@Column(name = "LocalFtpDirectory")
	private String LocalFtpDirectory;

	@Column(name = "LocalFtpDirectoryPath")
	private String LocalFtpDirectoryPath;

	@Column(name = "LocalFtpHomeDirectory")
	private String LocalFtpHomeDirectory;

	@Column(name = "LocalFtpUserName")
	private String LocalFtpUserName;

	@Column(name = "LocalFtpPassword")
	private String LocalFtpPassword;

	@Column(name = "ApiUrl")
	private String ApiUrl;

	@Column(name = "ApiKey")
	private String ApiKey;

	/*
	 * @Column(name="ServiceCode") private Integer ServiceCode;
	 */

	@Column(name = "LocalFtpDirectoryPath_Sensor")
	private String LocalFtpDirectoryPath_Sensor;

	@Column(name = "LocalFtpDirectoryPath_Alarm")
	private String LocalFtpDirectoryPath_Alarm;

	@Column(name = "LocalFtpDirectoryPath_Log")
	private String LocalFtpDirectoryPath_Log;

	@Column(name = "LocalFtpDirectory_Sensor")
	private String LocalFtpDirectory_Sensor;

	@Column(name = "LocalFtpDirectory_Alarm")
	private String LocalFtpDirectory_Alarm;

	@Column(name = "LocalFtpDirectory_Log")
	private String LocalFtpDirectory_Log;

	@Column(name = "DataLoggerID_Alarm")
	private Integer DataLoggerID_Alarm;

	@Column(name = "DataLoggerID_Log")
	private Integer DataLoggerID_Log;

	@Column(name = "ServiceCode_Sensor")
	private Integer ServiceCode_Sensor;

	@Column(name = "ServiceCode_Alarm")
	private Integer ServiceCode_Alarm;

	@Column(name = "ServiceCode_Log")
	private Integer ServiceCode_Log;

	@Column(name = "Timezone")
	private String Timezone;

	private BigInteger timeZoneOffSetMin;

	public BigInteger getTimeZoneOffSetMin() {
		return timeZoneOffSetMin;
	}

	public void setTimeZoneOffSetMin(BigInteger timeZoneOffSetMin) {
		this.timeZoneOffSetMin = timeZoneOffSetMin;
	}

	@Column(name = "LocalFtpDirectory_Modbus")
	private String LocalFtpDirectory_Modbus;

	@Column(name = "LocalFtpDirectory_Inverter")
	private String LocalFtpDirectory_Inverter;

	@Column(name = "LocalFtpDirectory_Data")
	private String LocalFtpDirectory_Data;

	@Column(name = "LocalFtpDirectoryPath_Modbus")
	private String LocalFtpDirectoryPath_Modbus;

	@Column(name = "LocalFtpDirectoryPath_Inverter")
	private String LocalFtpDirectoryPath_Inverter;

	@Column(name = "LocalFtpDirectoryPath_Data")
	private String LocalFtpDirectoryPath_Data;

	@Column(name = "DataLoggerID_Modbus")
	private Integer DataLoggerID_Modbus;

	@Column(name = "DataLoggerID_Inverter")
	private Integer DataLoggerID_Inverter;

	@Column(name = "DataLoggerID_Data")
	private Integer DataLoggerID_Data;

	@Column(name = "ServiceCode_Modbus")
	private Integer ServiceCode_Modbus;

	@Column(name = "ServiceCode_Inverter")
	private Integer ServiceCode_Inverter;

	@Column(name = "ServiceCode_Data")
	private Integer ServiceCode_Data;

	public Integer getTodayEnergyFlag() {
		return TodayEnergyFlag;
	}

	public void setTodayEnergyFlag(Integer todayEnergyFlag) {
		TodayEnergyFlag = todayEnergyFlag;
	}

	@Column(name = "TodayEnergyFlag")
	private Integer TodayEnergyFlag;

	/*
	 * @Transient private SiteStatus siteStatus;
	 * 
	 * 
	 * @OneToOne(fetch = FetchType.LAZY, mappedBy = "mSite", cascade =
	 * CascadeType.ALL) public SiteStatus getSiteStatus() { return this.siteStatus;
	 * }
	 * 
	 * public void setSiteStatus(SiteStatus siteStatus) { this.siteStatus =
	 * siteStatus; }
	 */

	/*
	 * @Transient private Set<SiteStatus> siteStatus = new HashSet<SiteStatus>(0);
	 * 
	 * @OneToOne(fetch = FetchType.LAZY, mappedBy = "mSite") public Set<SiteStatus>
	 * getSiteStatus() { return siteStatus; }
	 * 
	 * public void setSiteStatus(Set<SiteStatus> siteStatus) { siteStatus =
	 * siteStatus; }
	 */

	public Integer getSiteId() {
		return SiteId;
	}

	public void setSiteId(Integer siteId) {
		SiteId = siteId;
	}

	public Integer getCustomerID() {
		return CustomerID;
	}

	public void setCustomerID(Integer customerID) {
		CustomerID = customerID;
	}

	public Integer getSiteTypeID() {
		return SiteTypeID;
	}

	public void setSiteTypeID(Integer siteTypeID) {
		SiteTypeID = siteTypeID;
	}

	public String getSiteCode() {
		return SiteCode;
	}

	public void setSiteCode(String siteCode) {
		SiteCode = siteCode;
	}

	public String getSiteName() {
		return SiteName;
	}

	public void setSiteName(String siteName) {
		SiteName = siteName;
	}

	public String getSiteDescription() {
		return SiteDescription;
	}

	public void setSiteDescription(String siteDescription) {
		SiteDescription = siteDescription;
	}

	public String getContactPerson() {
		return ContactPerson;
	}

	public void setContactPerson(String contactPerson) {
		ContactPerson = contactPerson;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public Integer getStateID() {
		return StateID;
	}

	public void setStateID(Integer stateID) {
		StateID = stateID;
	}

	public Integer getCountryID() {
		return CountryID;
	}

	public void setCountryID(Integer countryID) {
		CountryID = countryID;
	}

	public String getPostalCode() {
		return PostalCode;
	}

	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	public String getLatitude() {
		return Latitude;
	}

	public void setLatitude(String latitude) {
		Latitude = latitude;
	}

	public String getAltitude() {
		return Altitude;
	}

	public void setAltitude(String altitude) {
		Altitude = altitude;
	}

	public String getSiteImage() {
		return SiteImage;
	}

	public void setSiteImage(String siteImage) {
		SiteImage = siteImage;
	}

	public double getInstallationCapacity() {
		return InstallationCapacity;
	}

	public void setInstallationCapacity(double installationCapacity) {
		InstallationCapacity = installationCapacity;
	}

	public String getSitePONumber() {
		return SitePONumber;
	}

	public void setSitePONumber(String sitePONumber) {
		SitePONumber = sitePONumber;
	}

	public Date getSitePODate() {
		return SitePODate;
	}

	public void setSitePODate(Date sitePODate) {
		SitePODate = sitePODate;
	}

	/*
	 * public Date getSiteHandoverDate() { return SiteHandoverDate; }
	 * 
	 * public void setSiteHandoverDate(Date siteHandoverDate) { SiteHandoverDate =
	 * siteHandoverDate; }
	 * 
	 * public Date getSiteCommisioningDate() { return SiteCommisioningDate; }
	 * 
	 * public void setSiteCommisioningDate(Date siteCommisioningDate) {
	 * SiteCommisioningDate = siteCommisioningDate; }
	 * 
	 * public Date getInstalledOn() { return InstalledOn; }
	 * 
	 * public void setInstalledOn(Date installedOn) { InstalledOn = installedOn; }
	 */
	public String getSiteOperator() {
		return SiteOperator;
	}

	public void setSiteOperator(String siteOperator) {
		SiteOperator = siteOperator;
	}

	public String getSiteManafacturer() {
		return SiteManafacturer;
	}

	public void setSiteManafacturer(String siteManafacturer) {
		SiteManafacturer = siteManafacturer;
	}

	public String getModuleName() {
		return ModuleName;
	}

	public void setModuleName(String moduleName) {
		ModuleName = moduleName;
	}

	public String getCommunicationType() {
		return CommunicationType;
	}

	public void setCommunicationType(String communicationType) {
		CommunicationType = communicationType;
	}

	public String getCollectionType() {
		return CollectionType;
	}

	public void setCollectionType(String collectionType) {
		CollectionType = collectionType;
	}

	public String getFileType() {
		return FileType;
	}

	public void setFileType(String fileType) {
		FileType = fileType;
	}

	/*
	 * public float getIncome() { return Income; }
	 * 
	 * public void setIncome(float income) { Income = income; }
	 */
	public Integer getCurrencyID() {
		return CurrencyID;
	}

	public void setCurrencyID(Integer currencyID) {
		CurrencyID = currencyID;
	}

	public String getEmailID() {
		return EmailID;
	}

	public void setEmailID(String emailID) {
		EmailID = emailID;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getTelephone() {
		return Telephone;
	}

	public void setTelephone(String telephone) {
		Telephone = telephone;
	}

	public String getFax() {
		return Fax;
	}

	public void setFax(String fax) {
		Fax = fax;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getSiteTypeName() {
		return SiteTypeName;
	}

	public void setSiteTypeName(String siteTypeName) {
		SiteTypeName = siteTypeName;
	}

	public String getStateName() {
		return StateName;
	}

	public void setStateName(String stateName) {
		StateName = stateName;
	}

	public String getCountryName() {
		return CountryName;
	}

	public void setCountryName(String countryName) {
		CountryName = countryName;
	}

	public String getCurrencyName() {
		return CurrencyName;
	}

	public void setCurrencyName(String currencyName) {
		CurrencyName = currencyName;
	}

	public Integer getDataLoggerID() {
		return DataLoggerID;
	}

	public void setDataLoggerID(Integer dataLoggerID) {
		DataLoggerID = dataLoggerID;
	}

	public Integer getOperationMode() {
		return OperationMode;
	}

	public void setOperationMode(Integer operationMode) {
		OperationMode = operationMode;
	}

	public String getFilePrefix() {
		return FilePrefix;
	}

	public void setFilePrefix(String filePrefix) {
		FilePrefix = filePrefix;
	}

	public String getFileSuffix() {
		return FileSuffix;
	}

	public void setFileSuffix(String fileSuffix) {
		FileSuffix = fileSuffix;
	}

	public String getFileSubstring() {
		return FileSubstring;
	}

	public void setFileSubstring(String fileSubstring) {
		FileSubstring = fileSubstring;
	}

	public Integer getSubFolder() {
		return SubFolder;
	}

	public void setSubFolder(Integer subFolder) {
		SubFolder = subFolder;
	}

	public String getCustomerReference() {
		return CustomerReference;
	}

	public void setCustomerReference(String customerReference) {
		CustomerReference = customerReference;
	}

	public String getCustomerNaming() {
		return CustomerNaming;
	}

	public void setCustomerNaming(String customerNaming) {
		CustomerNaming = customerNaming;
	}

	public String getRemoteFtpServer() {
		return RemoteFtpServer;
	}

	public void setRemoteFtpServer(String remoteFtpServer) {
		RemoteFtpServer = remoteFtpServer;
	}

	public String getRemoteFtpUserName() {
		return RemoteFtpUserName;
	}

	public void setRemoteFtpUserName(String remoteFtpUserName) {
		RemoteFtpUserName = remoteFtpUserName;
	}

	public String getRemoteFtpPassword() {
		return RemoteFtpPassword;
	}

	public void setRemoteFtpPassword(String remoteFtpPassword) {
		RemoteFtpPassword = remoteFtpPassword;
	}

	public String getRemoteFtpServerPort() {
		return RemoteFtpServerPort;
	}

	public void setRemoteFtpServerPort(String remoteFtpServerPort) {
		RemoteFtpServerPort = remoteFtpServerPort;
	}

	public String getRemoteFtpDirectoryPath() {
		return RemoteFtpDirectoryPath;
	}

	public void setRemoteFtpDirectoryPath(String remoteFtpDirectoryPath) {
		RemoteFtpDirectoryPath = remoteFtpDirectoryPath;
	}

	public String getLocalFtpDirectory() {
		return LocalFtpDirectory;
	}

	public void setLocalFtpDirectory(String localFtpDirectory) {
		LocalFtpDirectory = localFtpDirectory;
	}

	public String getLocalFtpDirectoryPath() {
		return LocalFtpDirectoryPath;
	}

	public void setLocalFtpDirectoryPath(String localFtpDirectoryPath) {
		LocalFtpDirectoryPath = localFtpDirectoryPath;
	}

	public String getLocalFtpHomeDirectory() {
		return LocalFtpHomeDirectory;
	}

	public void setLocalFtpHomeDirectory(String localFtpHomeDirectory) {
		LocalFtpHomeDirectory = localFtpHomeDirectory;
	}

	public String getLocalFtpUserName() {
		return LocalFtpUserName;
	}

	public void setLocalFtpUserName(String localFtpUserName) {
		LocalFtpUserName = localFtpUserName;
	}

	public String getLocalFtpPassword() {
		return LocalFtpPassword;
	}

	public void setLocalFtpPassword(String localFtpPassword) {
		LocalFtpPassword = localFtpPassword;
	}

	public String getApiUrl() {
		return ApiUrl;
	}

	public void setApiUrl(String apiUrl) {
		ApiUrl = apiUrl;
	}

	public String getApiKey() {
		return ApiKey;
	}

	public void setApiKey(String apiKey) {
		ApiKey = apiKey;
	}

	/*
	 * public String getServiceCode() { return ServiceCode; }
	 * 
	 * public void setServiceCode(String serviceCode) { ServiceCode = serviceCode; }
	 */
	public String getLocalFtpDirectoryPath_Sensor() {
		return LocalFtpDirectoryPath_Sensor;
	}

	public void setLocalFtpDirectoryPath_Sensor(String localFtpDirectoryPath_Sensor) {
		LocalFtpDirectoryPath_Sensor = localFtpDirectoryPath_Sensor;
	}

	public String getLocalFtpDirectoryPath_Alarm() {
		return LocalFtpDirectoryPath_Alarm;
	}

	public void setLocalFtpDirectoryPath_Alarm(String localFtpDirectoryPath_Alarm) {
		LocalFtpDirectoryPath_Alarm = localFtpDirectoryPath_Alarm;
	}

	public String getLocalFtpDirectoryPath_Log() {
		return LocalFtpDirectoryPath_Log;
	}

	public void setLocalFtpDirectoryPath_Log(String localFtpDirectoryPath_Log) {
		LocalFtpDirectoryPath_Log = localFtpDirectoryPath_Log;
	}

	public String getLocalFtpDirectory_Sensor() {
		return LocalFtpDirectory_Sensor;
	}

	public void setLocalFtpDirectory_Sensor(String localFtpDirectory_Sensor) {
		LocalFtpDirectory_Sensor = localFtpDirectory_Sensor;
	}

	public String getLocalFtpDirectory_Alarm() {
		return LocalFtpDirectory_Alarm;
	}

	public void setLocalFtpDirectory_Alarm(String localFtpDirectory_Alarm) {
		LocalFtpDirectory_Alarm = localFtpDirectory_Alarm;
	}

	public String getLocalFtpDirectory_Log() {
		return LocalFtpDirectory_Log;
	}

	public void setLocalFtpDirectory_Log(String localFtpDirectory_Log) {
		LocalFtpDirectory_Log = localFtpDirectory_Log;
	}

	public BigInteger getDataLoggerID_Sensor() {
		return DataLoggerID_Sensor;
	}

	public void setDataLoggerID_Sensor(BigInteger dataLoggerID_Sensor) {
		DataLoggerID_Sensor = dataLoggerID_Sensor;
	}

	public Integer getDataLoggerID_Alarm() {
		return DataLoggerID_Alarm;
	}

	public void setDataLoggerID_Alarm(Integer dataLoggerID_Alarm) {
		DataLoggerID_Alarm = dataLoggerID_Alarm;
	}

	public Integer getDataLoggerID_Log() {
		return DataLoggerID_Log;
	}

	public void setDataLoggerID_Log(Integer dataLoggerID_Log) {
		DataLoggerID_Log = dataLoggerID_Log;
	}

	public Integer getServiceCode_Sensor() {
		return ServiceCode_Sensor;
	}

	public void setServiceCode_Sensor(Integer serviceCode_Sensor) {
		ServiceCode_Sensor = serviceCode_Sensor;
	}

	public Integer getServiceCode_Alarm() {
		return ServiceCode_Alarm;
	}

	public void setServiceCode_Alarm(Integer serviceCode_Alarm) {
		ServiceCode_Alarm = serviceCode_Alarm;
	}

	public Integer getServiceCode_Log() {
		return ServiceCode_Log;
	}

	public void setServiceCode_Log(Integer serviceCode_Log) {
		ServiceCode_Log = serviceCode_Log;
	}

	public String getTimezone() {
		return Timezone;
	}

	public void setTimezone(String timezone) {
		Timezone = timezone;
	}

	public String getLocalFtpDirectory_Modbus() {
		return LocalFtpDirectory_Modbus;
	}

	public void setLocalFtpDirectory_Modbus(String localFtpDirectory_Modbus) {
		LocalFtpDirectory_Modbus = localFtpDirectory_Modbus;
	}

	public String getLocalFtpDirectory_Inverter() {
		return LocalFtpDirectory_Inverter;
	}

	public void setLocalFtpDirectory_Inverter(String localFtpDirectory_Inverter) {
		LocalFtpDirectory_Inverter = localFtpDirectory_Inverter;
	}

	public String getLocalFtpDirectory_Data() {
		return LocalFtpDirectory_Data;
	}

	public void setLocalFtpDirectory_Data(String localFtpDirectory_Data) {
		LocalFtpDirectory_Data = localFtpDirectory_Data;
	}

	public String getLocalFtpDirectoryPath_Modbus() {
		return LocalFtpDirectoryPath_Modbus;
	}

	public void setLocalFtpDirectoryPath_Modbus(String localFtpDirectoryPath_Modbus) {
		LocalFtpDirectoryPath_Modbus = localFtpDirectoryPath_Modbus;
	}

	public String getLocalFtpDirectoryPath_Inverter() {
		return LocalFtpDirectoryPath_Inverter;
	}

	public void setLocalFtpDirectoryPath_Inverter(String localFtpDirectoryPath_Inverter) {
		LocalFtpDirectoryPath_Inverter = localFtpDirectoryPath_Inverter;
	}

	public String getLocalFtpDirectoryPath_Data() {
		return LocalFtpDirectoryPath_Data;
	}

	public void setLocalFtpDirectoryPath_Data(String localFtpDirectoryPath_Data) {
		LocalFtpDirectoryPath_Data = localFtpDirectoryPath_Data;
	}

	public Integer getDataLoggerID_Modbus() {
		return DataLoggerID_Modbus;
	}

	public void setDataLoggerID_Modbus(Integer dataLoggerID_Modbus) {
		DataLoggerID_Modbus = dataLoggerID_Modbus;
	}

	public Integer getDataLoggerID_Inverter() {
		return DataLoggerID_Inverter;
	}

	public void setDataLoggerID_Inverter(Integer dataLoggerID_Inverter) {
		DataLoggerID_Inverter = dataLoggerID_Inverter;
	}

	public Integer getDataLoggerID_Data() {
		return DataLoggerID_Data;
	}

	public void setDataLoggerID_Data(Integer dataLoggerID_Data) {
		DataLoggerID_Data = dataLoggerID_Data;
	}

	public Integer getServiceCode_Modbus() {
		return ServiceCode_Modbus;
	}

	public void setServiceCode_Modbus(Integer serviceCode_Modbus) {
		ServiceCode_Modbus = serviceCode_Modbus;
	}

	public Integer getServiceCode_Inverter() {
		return ServiceCode_Inverter;
	}

	public void setServiceCode_Inverter(Integer serviceCode_Inverter) {
		ServiceCode_Inverter = serviceCode_Inverter;
	}

	public Integer getServiceCode_Data() {
		return ServiceCode_Data;
	}

	public void setServiceCode_Data(Integer serviceCode_Data) {
		ServiceCode_Data = serviceCode_Data;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Column(name = "ContractID")
	private String ContractId;

	@Column(name = "TypeOfContract")
	private String TypeOfContract;

	@Column(name = "ServicePackage")
	private String ServicePackage;

	@Column(name = "Daf_Service")
	private String Daf_Service;

	@Column(name = "Daf_Frequency")
	private String Daf_Frequency;

	@Column(name = "Tbt_Service")
	private String Tbt_Service;

	@Column(name = "Tbt_Frequency")
	private String Tbt_Frequency;

	@Column(name = "Tat_Service")
	private String Tat_Service;
	@Column(name = "Tat_Frequency")
	private String Tat_Frequency;
	@Column(name = "Sym_Service")
	private String Sym_Service;
	@Column(name = "Sym_frequency")
	private String Sym_frequency;
	@Column(name = "Tls_Service")
	private String Tls_Service;
	@Column(name = "Tls_Frequency")
	private String Tls_Frequency;

	@Column(name = "Tlm_Service")
	private String Tlm_Service;
	@Column(name = "Tlm_Frequency")
	private String Tlm_Frequency;
	@Column(name = "Com_Service")
	private String Com_Service;
	@Column(name = "Com_Frequency")
	private String Com_Frequency;
	@Column(name = "Rt_Service")
	private String Rt_Service;
	@Column(name = "Rt_Frequency")
	private String Rt_Frequency;

	@Column(name = "Security_Service")
	private String Security_Service;
	@Column(name = "Security_Frequency")
	private String Security_Frequency;
	@Column(name = "Rowfc_Service")
	private String Rowfc_Service;
	@Column(name = "Rowfc_Frequency")
	private String Rowfc_Frequency;
	@Column(name = "Dti_Service")
	private String Dti_Service;
	@Column(name = "Dti_Frequency")
	private String Dti_Frequency;

	@Column(name = "Di_Service")
	private String Di_Service;

	@Column(name = "Di_Frequency")
	private String Di_Frequency;

	@Column(name = "Sact_Service")
	private String Sact_Service;

	@Column(name = "Sact_Frequency")
	private String Sact_Frequency;

	@Column(name = "Eltml_Service")
	private String Eltml_Service;

	@Column(name = "Eltml_Frequency")
	private String Eltml_Frequency;

	@Column(name = "Eltms_Service")
	private String Eltms_Service;
	@Column(name = "Eltms_Frequency")
	private String Eltms_Frequency;
	@Column(name = "Ir_Service")
	private String Ir_Service;
	@Column(name = "Ir_Frequency")
	private String Ir_Frequency;
	@Column(name = "Revampdc_Service")
	private String Revampdc_Service;
	@Column(name = "Revampdc_Frequency")
	private String Revampdc_Frequency;

	@Column(name = "Revampaclt_Service")
	private String Revampaclt_Service;
	@Column(name = "Revampaclt_Frequency")
	private String Revampaclt_Frequency;
	@Column(name = "Revampacht_Service")
	private String Revampacht_Service;
	@Column(name = "Revampacht_Frequency")
	private String Revampacht_Frequency;
	@Column(name = "Revampacht_Service_Civil")
	private String Revampacht_Service_Civil;
	@Column(name = "Revampacht_Frequency_Civil")
	private String Revampacht_Frequency_Civil;

	@Column(name = "Jmr_Service")
	private String Jmr_Service;

	@Column(name = "Jmr_Frequency")
	private String Jmr_Frequency;

	@Column(name = "Sldc_Documentation")
	private String Sldc_Documentation;

	@Column(name = "Sldc_Frequency")
	private String Sldc_Frequency;

	@Column(name = "Spares_Management")
	private String Spares_Management;

	@Column(name = "Sm_frequency")
	private String Sm_frequency;

	@Column(name = "CleaningCycle")
	private String CleaningCycle;

	@Column(name = "CleaningCycle_Frequency")
	private String CleaningCycle_Frequency;

	@Column(name = "Vegetation_Mangement")
	private String VEGETATION_MANGEMENT;

	@Column(name = "Vm_Frequency")
	private String Vm_Frequency;

	@Column(name = "Plantdown_Issueidentified")
	private String Plantdown_Issueidentified;

	@Column(name = "Plantdown_Assigned")
	private String Plantdown_Assigned;

	@Column(name = "Plantdown_Resolved")
	private String Plantdown_Resolved;

	public String getPlantdown_Resolved() {
		return Plantdown_Resolved;
	}

	public void setPlantdown_Resolved(String plantdown_Resolved) {
		Plantdown_Resolved = plantdown_Resolved;
	}

	@Column(name = "Plantwarning_Issueidentified")
	private String Plantwarning_Issueidentified;

	@Column(name = "Plantwarning_Assigned")
	private String Plantwarning_Assigned;

	@Column(name = "Plantwarning_Resolved")
	private String Plantwarning_Resolved;

	@Column(name = "Equgendown_Issueidentified")
	private String Equgendown_Issueidentified;

	@Column(name = "Equgendown_Assigned")
	private String Equgendown_Assigned;

	@Column(name = "Equgendown_Resolved")
	private String Equgendown_Resolved;

	@Column(name = "Equgenwarning_Issueidentified")
	private String Equgenwarning_Issueidentified;

	@Column(name = "Equgenwarning_Assigned")
	private String Equgenwarning_Assigned;

	@Column(name = "Equgenwarning_Resolved")
	private String Equgenwarning_Resolved;

	@Column(name = "Equgenoffline_Issueidentified")
	private String Equgenoffline_Issueidentified;

	@Column(name = "Equgenoffline_Assigned")
	private String Equgenoffline_Assigned;

	@Column(name = "Equgenoffline_Resolved")
	private String Equgenoffline_Resolved;

	@Column(name = "Equnongendown_Issueidentified")
	private String Equnongendown_Issueidentified;

	@Column(name = "Equnongendown_Assigned")
	private String Equnongendown_Assigned;

	@Column(name = "Equnongendown_Resolved")
	private String Equnongendown_Resolved;

	@Column(name = "Equnongenwarning_Issueidentified")
	private String Equnongenwarning_Issueidentified;

	@Column(name = "Equnongenwarning_Assigned")
	private String Equnongenwarning_Assigned;

	@Column(name = "Equnongenwarning_Resolved")
	private String Equnongenwarning_Resolved;

	@Column(name = "Equnongenoffline_Issueidentified")
	private String Equnongenoffline_Issueidentified;

	@Column(name = "Equnongenoffline_Assigned")
	private String Equnongenoffline_Assigned;

	@Column(name = "Equnongenoffline_Resolved")
	private String Equnongenoffline_Resolved;

	@Column(name = "Commencementofwork")
	private Date Commencementofwork;
	@Transient
	private String CommercementofworkText;

	@Column(name = "Performanceguaranteedamages")
	private String Performanceguaranteedamages;

	@Column(name = "Sld")
	private String Sld;

	@Column(name = "Pvsyst")
	private String Pvsyst;

	@Column(name = "ConstructionLayout")
	private String ConstructionLayout;

	@Column(name = "WarrantyCertificate")
	private String WarrantyCertificate;

	@Column(name = "InssuranceCertificate")
	private String InssuranceCertificate;

	@Column(name = "ApprovalCertificate")
	private String ApprovalCertificate;

	@Column(name = "Permits")
	private String Permits;

	@Column(name = "TestCertificate")
	private String TestCertificate;

	@Column(name = "ServiceReport")
	private String ServiceReport;

	@Column(name = "IncidentReport")
	private String IncidentReport;

	@Column(name = "PreventiveMaintenance")
	private String PreventiveMaintenance;

	@Column(name = "CalibrationReport")
	private String CalibrationReport;

	public String getCommercementofworkText() {
		return CommercementofworkText;
	}

	public void setCommercementofworkText(String commercementofworkText) {
		CommercementofworkText = commercementofworkText;
	}

	public String getContractId() {
		return ContractId;
	}

	public void setContractId(String contractId) {
		ContractId = contractId;
	}

	public String getTypeOfContract() {
		return TypeOfContract;
	}

	public void setTypeOfContract(String typeOfContract) {
		TypeOfContract = typeOfContract;
	}

	public String getServicePackage() {
		return ServicePackage;
	}

	public void setServicePackage(String servicePackage) {
		ServicePackage = servicePackage;
	}

	public String getDaf_Service() {
		return Daf_Service;
	}

	public void setDaf_Service(String daf_Service) {
		Daf_Service = daf_Service;
	}

	public String getDaf_Frequency() {
		return Daf_Frequency;
	}

	public void setDaf_Frequency(String daf_Frequency) {
		Daf_Frequency = daf_Frequency;
	}

	public String getTbt_Service() {
		return Tbt_Service;
	}

	public void setTbt_Service(String tbt_Service) {
		Tbt_Service = tbt_Service;
	}

	public String getTbt_Frequency() {
		return Tbt_Frequency;
	}

	public void setTbt_Frequency(String tbt_Frequency) {
		Tbt_Frequency = tbt_Frequency;
	}

	public String getTat_Service() {
		return Tat_Service;
	}

	public void setTat_Service(String tat_Service) {
		Tat_Service = tat_Service;
	}

	public String getTat_Frequency() {
		return Tat_Frequency;
	}

	public void setTat_Frequency(String tat_Frequency) {
		Tat_Frequency = tat_Frequency;
	}

	public String getSym_Service() {
		return Sym_Service;
	}

	public void setSym_Service(String sym_Service) {
		Sym_Service = sym_Service;
	}

	public String getSym_frequency() {
		return Sym_frequency;
	}

	public void setSym_frequency(String sym_frequency) {
		Sym_frequency = sym_frequency;
	}

	public String getTls_Service() {
		return Tls_Service;
	}

	public void setTls_Service(String tls_Service) {
		Tls_Service = tls_Service;
	}

	public String getTls_Frequency() {
		return Tls_Frequency;
	}

	public void setTls_Frequency(String tls_Frequency) {
		Tls_Frequency = tls_Frequency;
	}

	public String getTlm_Service() {
		return Tlm_Service;
	}

	public void setTlm_Service(String tlm_Service) {
		Tlm_Service = tlm_Service;
	}

	public String getTlm_Frequency() {
		return Tlm_Frequency;
	}

	public void setTlm_Frequency(String tlm_Frequency) {
		Tlm_Frequency = tlm_Frequency;
	}

	public String getCom_Service() {
		return Com_Service;
	}

	public void setCom_Service(String com_Service) {
		Com_Service = com_Service;
	}

	public String getCom_Frequency() {
		return Com_Frequency;
	}

	public void setCom_Frequency(String com_Frequency) {
		Com_Frequency = com_Frequency;
	}

	public String getRt_Service() {
		return Rt_Service;
	}

	public void setRt_Service(String rt_Service) {
		Rt_Service = rt_Service;
	}

	public String getRt_Frequency() {
		return Rt_Frequency;
	}

	public void setRt_Frequency(String rt_Frequency) {
		Rt_Frequency = rt_Frequency;
	}

	public String getRowfc_Service() {
		return Rowfc_Service;
	}

	public void setRowfc_Service(String rowfc_Service) {
		Rowfc_Service = rowfc_Service;
	}

	public String getRowfc_Frequency() {
		return Rowfc_Frequency;
	}

	public void setRowfc_Frequency(String rowfc_Frequency) {
		Rowfc_Frequency = rowfc_Frequency;
	}

	public String getDti_Service() {
		return Dti_Service;
	}

	public void setDti_Service(String dti_Service) {
		Dti_Service = dti_Service;
	}

	public String getDti_Frequency() {
		return Dti_Frequency;
	}

	public void setDti_Frequency(String dti_Frequency) {
		Dti_Frequency = dti_Frequency;
	}

	public String getDi_Service() {
		return Di_Service;
	}

	public void setDi_Service(String di_Service) {
		Di_Service = di_Service;
	}

	public String getDi_Frequency() {
		return Di_Frequency;
	}

	public void setDi_Frequency(String di_Frequency) {
		Di_Frequency = di_Frequency;
	}

	public String getSact_Service() {
		return Sact_Service;
	}

	public void setSact_Service(String sact_Service) {
		Sact_Service = sact_Service;
	}

	public String getSact_Frequency() {
		return Sact_Frequency;
	}

	public void setSact_Frequency(String sact_Frequency) {
		Sact_Frequency = sact_Frequency;
	}

	public String getEltml_Service() {
		return Eltml_Service;
	}

	public void setEltml_Service(String eltml_Service) {
		Eltml_Service = eltml_Service;
	}

	public String getEltml_Frequency() {
		return Eltml_Frequency;
	}

	public void setEltml_Frequency(String eltml_Frequency) {
		Eltml_Frequency = eltml_Frequency;
	}

	public String getEltms_Service() {
		return Eltms_Service;
	}

	public void setEltms_Service(String eltms_Service) {
		Eltms_Service = eltms_Service;
	}

	public String getEltms_Frequency() {
		return Eltms_Frequency;
	}

	public void setEltms_Frequency(String eltms_Frequency) {
		Eltms_Frequency = eltms_Frequency;
	}

	public String getIr_Service() {
		return Ir_Service;
	}

	public void setIr_Service(String ir_Service) {
		Ir_Service = ir_Service;
	}

	public String getIr_Frequency() {
		return Ir_Frequency;
	}

	public void setIr_Frequency(String ir_Frequency) {
		Ir_Frequency = ir_Frequency;
	}

	public String getRevampdc_Service() {
		return Revampdc_Service;
	}

	public void setRevampdc_Service(String revampdc_Service) {
		Revampdc_Service = revampdc_Service;
	}

	public String getRevampdc_Frequency() {
		return Revampdc_Frequency;
	}

	public void setRevampdc_Frequency(String revampdc_Frequency) {
		Revampdc_Frequency = revampdc_Frequency;
	}

	public String getRevampaclt_Service() {
		return Revampaclt_Service;
	}

	public void setRevampaclt_Service(String revampaclt_Service) {
		Revampaclt_Service = revampaclt_Service;
	}

	public String getRevampaclt_Frequency() {
		return Revampaclt_Frequency;
	}

	public void setRevampaclt_Frequency(String revampaclt_Frequency) {
		Revampaclt_Frequency = revampaclt_Frequency;
	}

	public String getRevampacht_Service() {
		return Revampacht_Service;
	}

	public void setRevampacht_Service(String revampacht_Service) {
		Revampacht_Service = revampacht_Service;
	}

	public String getRevampacht_Frequency() {
		return Revampacht_Frequency;
	}

	public void setRevampacht_Frequency(String revampacht_Frequency) {
		Revampacht_Frequency = revampacht_Frequency;
	}

	public String getRevampacht_Service_Civil() {
		return Revampacht_Service_Civil;
	}

	public void setRevampacht_Service_Civil(String revampacht_Service_Civil) {
		Revampacht_Service_Civil = revampacht_Service_Civil;
	}

	public String getRevampacht_Frequency_Civil() {
		return Revampacht_Frequency_Civil;
	}

	public void setRevampacht_Frequency_Civil(String revampacht_Frequency_Civil) {
		Revampacht_Frequency_Civil = revampacht_Frequency_Civil;
	}

	public String getJmr_Service() {
		return Jmr_Service;
	}

	public void setJmr_Service(String jmr_Service) {
		Jmr_Service = jmr_Service;
	}

	public String getJmr_Frequency() {
		return Jmr_Frequency;
	}

	public void setJmr_Frequency(String jmr_Frequency) {
		Jmr_Frequency = jmr_Frequency;
	}

	public String getSldc_Documentation() {
		return Sldc_Documentation;
	}

	public void setSldc_Documentation(String sldc_Documentation) {
		Sldc_Documentation = sldc_Documentation;
	}

	public String getSldc_Frequency() {
		return Sldc_Frequency;
	}

	public void setSldc_Frequency(String sldc_Frequency) {
		Sldc_Frequency = sldc_Frequency;
	}

	public String getSpares_Management() {
		return Spares_Management;
	}

	public void setSpares_Management(String spares_Management) {
		Spares_Management = spares_Management;
	}

	public String getSm_frequency() {
		return Sm_frequency;
	}

	public void setSm_frequency(String sm_frequency) {
		Sm_frequency = sm_frequency;
	}

	public String getCleaningCycle() {
		return CleaningCycle;
	}

	public void setCleaningCycle(String cleaningCycle) {
		CleaningCycle = cleaningCycle;
	}

	public String getCleaningCycle_Frequency() {
		return CleaningCycle_Frequency;
	}

	public void setCleaningCycle_Frequency(String cleaningCycle_Frequency) {
		CleaningCycle_Frequency = cleaningCycle_Frequency;
	}

	public String getVEGETATION_MANGEMENT() {
		return VEGETATION_MANGEMENT;
	}

	public void setVEGETATION_MANGEMENT(String vEGETATION_MANGEMENT) {
		VEGETATION_MANGEMENT = vEGETATION_MANGEMENT;
	}

	public String getVm_Frequency() {
		return Vm_Frequency;
	}

	public void setVm_Frequency(String vm_Frequency) {
		Vm_Frequency = vm_Frequency;
	}

	public String getPlantdown_Issueidentified() {
		return Plantdown_Issueidentified;
	}

	public void setPlantdown_Issueidentified(String plantdown_Issueidentified) {
		Plantdown_Issueidentified = plantdown_Issueidentified;
	}

	public String getPlantdown_Assigned() {
		return Plantdown_Assigned;
	}

	public void setPlantdown_Assigned(String plantdown_Assigned) {
		Plantdown_Assigned = plantdown_Assigned;
	}

	public String getPlantwarning_Issueidentified() {
		return Plantwarning_Issueidentified;
	}

	public void setPlantwarning_Issueidentified(String plantwarning_Issueidentified) {
		Plantwarning_Issueidentified = plantwarning_Issueidentified;
	}

	public String getPlantwarning_Assigned() {
		return Plantwarning_Assigned;
	}

	public void setPlantwarning_Assigned(String plantwarning_Assigned) {
		Plantwarning_Assigned = plantwarning_Assigned;
	}

	public String getPlantwarning_Resolved() {
		return Plantwarning_Resolved;
	}

	public void setPlantwarning_Resolved(String plantwarning_Resolved) {
		Plantwarning_Resolved = plantwarning_Resolved;
	}

	public String getEqugendown_Issueidentified() {
		return Equgendown_Issueidentified;
	}

	public void setEqugendown_Issueidentified(String equgendown_Issueidentified) {
		Equgendown_Issueidentified = equgendown_Issueidentified;
	}

	public String getEqugendown_Assigned() {
		return Equgendown_Assigned;
	}

	public void setEqugendown_Assigned(String equgendown_Assigned) {
		Equgendown_Assigned = equgendown_Assigned;
	}

	public String getEqugendown_Resolved() {
		return Equgendown_Resolved;
	}

	public void setEqugendown_Resolved(String equgendown_Resolved) {
		Equgendown_Resolved = equgendown_Resolved;
	}

	public String getEqugenwarning_Issueidentified() {
		return Equgenwarning_Issueidentified;
	}

	public void setEqugenwarning_Issueidentified(String equgenwarning_Issueidentified) {
		Equgenwarning_Issueidentified = equgenwarning_Issueidentified;
	}

	public String getEqugenwarning_Assigned() {
		return Equgenwarning_Assigned;
	}

	public void setEqugenwarning_Assigned(String equgenwarning_Assigned) {
		Equgenwarning_Assigned = equgenwarning_Assigned;
	}

	public String getEqugenwarning_Resolved() {
		return Equgenwarning_Resolved;
	}

	public void setEqugenwarning_Resolved(String equgenwarning_Resolved) {
		Equgenwarning_Resolved = equgenwarning_Resolved;
	}

	public String getEqugenoffline_Issueidentified() {
		return Equgenoffline_Issueidentified;
	}

	public void setEqugenoffline_Issueidentified(String equgenoffline_Issueidentified) {
		Equgenoffline_Issueidentified = equgenoffline_Issueidentified;
	}

	public String getEqugenoffline_Assigned() {
		return Equgenoffline_Assigned;
	}

	public void setEqugenoffline_Assigned(String equgenoffline_Assigned) {
		Equgenoffline_Assigned = equgenoffline_Assigned;
	}

	public String getEqugenoffline_Resolved() {
		return Equgenoffline_Resolved;
	}

	public void setEqugenoffline_Resolved(String equgenoffline_Resolved) {
		Equgenoffline_Resolved = equgenoffline_Resolved;
	}

	public String getEqunongendown_Issueidentified() {
		return Equnongendown_Issueidentified;
	}

	public void setEqunongendown_Issueidentified(String equnongendown_Issueidentified) {
		Equnongendown_Issueidentified = equnongendown_Issueidentified;
	}

	public String getEqunongendown_Assigned() {
		return Equnongendown_Assigned;
	}

	public void setEqunongendown_Assigned(String equnongendown_Assigned) {
		Equnongendown_Assigned = equnongendown_Assigned;
	}

	public String getEqunongendown_Resolved() {
		return Equnongendown_Resolved;
	}

	public void setEqunongendown_Resolved(String equnongendown_Resolved) {
		Equnongendown_Resolved = equnongendown_Resolved;
	}

	public String getEqunongenwarning_Issueidentified() {
		return Equnongenwarning_Issueidentified;
	}

	public void setEqunongenwarning_Issueidentified(String equnongenwarning_Issueidentified) {
		Equnongenwarning_Issueidentified = equnongenwarning_Issueidentified;
	}

	public String getEqunongenwarning_Assigned() {
		return Equnongenwarning_Assigned;
	}

	public void setEqunongenwarning_Assigned(String equnongenwarning_Assigned) {
		Equnongenwarning_Assigned = equnongenwarning_Assigned;
	}

	public String getEqunongenwarning_Resolved() {
		return Equnongenwarning_Resolved;
	}

	public void setEqunongenwarning_Resolved(String equnongenwarning_Resolved) {
		Equnongenwarning_Resolved = equnongenwarning_Resolved;
	}

	public String getEqunongenoffline_Issueidentified() {
		return Equnongenoffline_Issueidentified;
	}

	public void setEqunongenoffline_Issueidentified(String equnongenoffline_Issueidentified) {
		Equnongenoffline_Issueidentified = equnongenoffline_Issueidentified;
	}

	public String getEqunongenoffline_Assigned() {
		return Equnongenoffline_Assigned;
	}

	public void setEqunongenoffline_Assigned(String equnongenoffline_Assigned) {
		Equnongenoffline_Assigned = equnongenoffline_Assigned;
	}

	public String getEqunongenoffline_Resolved() {
		return Equnongenoffline_Resolved;
	}

	public void setEqunongenoffline_Resolved(String equnongenoffline_Resolved) {
		Equnongenoffline_Resolved = equnongenoffline_Resolved;
	}

	public Date getCommencementofwork() {
		return Commencementofwork;
	}

	public void setCommencementofwork(Date commencementofwork) {
		Commencementofwork = commencementofwork;
	}

	public String getPerformanceguaranteedamages() {
		return Performanceguaranteedamages;
	}

	public void setPerformanceguaranteedamages(String performanceguaranteedamages) {
		Performanceguaranteedamages = performanceguaranteedamages;
	}

	public String getSld() {
		return Sld;
	}

	public void setSld(String sld) {
		Sld = sld;
	}

	public String getPvsyst() {
		return Pvsyst;
	}

	public void setPvsyst(String pvsyst) {
		Pvsyst = pvsyst;
	}

	public String getConstructionLayout() {
		return ConstructionLayout;
	}

	public void setConstructionLayout(String constructionLayout) {
		ConstructionLayout = constructionLayout;
	}

	public String getWarrantyCertificate() {
		return WarrantyCertificate;
	}

	public void setWarrantyCertificate(String warrantyCertificate) {
		WarrantyCertificate = warrantyCertificate;
	}

	public String getInssuranceCertificate() {
		return InssuranceCertificate;
	}

	public void setInssuranceCertificate(String inssuranceCertificate) {
		InssuranceCertificate = inssuranceCertificate;
	}

	public String getApprovalCertificate() {
		return ApprovalCertificate;
	}

	public void setApprovalCertificate(String approvalCertificate) {
		ApprovalCertificate = approvalCertificate;
	}

	public String getPermits() {
		return Permits;
	}

	public void setPermits(String permits) {
		Permits = permits;
	}

	public String getTestCertificate() {
		return TestCertificate;
	}

	public void setTestCertificate(String testCertificate) {
		TestCertificate = testCertificate;
	}

	public String getServiceReport() {
		return ServiceReport;
	}

	public void setServiceReport(String serviceReport) {
		ServiceReport = serviceReport;
	}

	public String getIncidentReport() {
		return IncidentReport;
	}

	public void setIncidentReport(String incidentReport) {
		IncidentReport = incidentReport;
	}

	public String getPreventiveMaintenance() {
		return PreventiveMaintenance;
	}

	public void setPreventiveMaintenance(String preventiveMaintenance) {
		PreventiveMaintenance = preventiveMaintenance;
	}

	public String getCalibrationReport() {
		return CalibrationReport;
	}

	public void setCalibrationReport(String calibrationReport) {
		CalibrationReport = calibrationReport;
	}

	@Column(name = "DecisionMaker")
	private String DecisionMaker;
	@Column(name = "Dm_Address")
	private String Dm_Address;
	@Column(name = "Dm_City")
	private String Dm_City;
	@Column(name = "Dm_State")
	private String Dm_State;
	@Column(name = "Dm_Country")
	private String Dm_Country;

	@Column(name = "Dm_Postalcode")
	private String Dm_Postalcode;
	@Column(name = "Dm_Emailid")
	private String Dm_Emailid;
	@Column(name = "Dm_Phoneno")
	private String Dm_Phoneno;

	@Column(name = "Champion")
	private String Champion;
	@Column(name = "Ch_Address")
	private String Ch_Address;
	@Column(name = "Ch_City")
	private String Ch_City;
	@Column(name = "Ch_State")
	private String Ch_State;
	@Column(name = "Ch_Country")
	private String Ch_Country;

	@Column(name = "Ch_Postalcode")
	private String Ch_Postalcode;
	@Column(name = "Ch_Emailid")
	private String Ch_Emailid;
	@Column(name = "Ch_Phoneno")
	private String Ch_Phoneno;

	@Column(name = "RoadBlock")
	private String RoadBlock;
	@Column(name = "Rd_Address")
	private String Rd_Address;
	@Column(name = "Rd_City")
	private String Rd_City;
	@Column(name = "Rd_State")
	private String Rd_State;
	@Column(name = "Rd_Country")
	private String Rd_Country;

	@Column(name = "Rd_Postalcode")
	private String Rd_Postalcode;
	@Column(name = "Rd_Emailid")
	private String Rd_Emailid;
	@Column(name = "Rd_Phoneno")
	private String Rd_Phoneno;

	@Column(name = "KeyDeliverables")
	private String KeyDeliverables;
	@Column(name = "Kd_Address")
	private String Kd_Address;
	@Column(name = "Kd_City")
	private String Kd_City;
	@Column(name = "Kd_State")
	private String Kd_State;
	@Column(name = "Kd_Country")
	private String Kd_Country;

	@Column(name = "Kd_Postalcode")
	private String Kd_Postalcode;
	@Column(name = "Kd_Emailid")
	private String Kd_Emailid;
	@Column(name = "Kd_Phoneno")
	private String Kd_Phoneno;
	private String ModuleArea;

	public String getModuleArea() {
		return ModuleArea;
	}

	public void setModuleArea(String moduleArea) {
		ModuleArea = moduleArea;
	}

	public String getSecurity_Service() {
		return Security_Service;
	}

	public void setSecurity_Service(String security_Service) {
		Security_Service = security_Service;
	}

	public String getSecurity_Frequency() {
		return Security_Frequency;
	}

	public void setSecurity_Frequency(String security_Frequency) {
		Security_Frequency = security_Frequency;
	}

	public String getDecisionMaker() {
		return DecisionMaker;
	}

	public void setDecisionMaker(String decisionMaker) {
		DecisionMaker = decisionMaker;
	}

	public String getDm_Address() {
		return Dm_Address;
	}

	public void setDm_Address(String dm_Address) {
		Dm_Address = dm_Address;
	}

	public String getDm_City() {
		return Dm_City;
	}

	public void setDm_City(String dm_City) {
		Dm_City = dm_City;
	}

	public String getDm_State() {
		return Dm_State;
	}

	public void setDm_State(String dm_State) {
		Dm_State = dm_State;
	}

	public String getDm_Country() {
		return Dm_Country;
	}

	public void setDm_Country(String dm_Country) {
		Dm_Country = dm_Country;
	}

	public String getDm_Postalcode() {
		return Dm_Postalcode;
	}

	public void setDm_Postalcode(String dm_Postalcode) {
		Dm_Postalcode = dm_Postalcode;
	}

	public String getDm_Emailid() {
		return Dm_Emailid;
	}

	public void setDm_Emailid(String dm_Emailid) {
		Dm_Emailid = dm_Emailid;
	}

	public String getDm_Phoneno() {
		return Dm_Phoneno;
	}

	public void setDm_Phoneno(String dm_Phoneno) {
		Dm_Phoneno = dm_Phoneno;
	}

	public String getChampion() {
		return Champion;
	}

	public void setChampion(String champion) {
		Champion = champion;
	}

	public String getCh_Address() {
		return Ch_Address;
	}

	public void setCh_Address(String ch_Address) {
		Ch_Address = ch_Address;
	}

	public String getCh_City() {
		return Ch_City;
	}

	public void setCh_City(String ch_City) {
		Ch_City = ch_City;
	}

	public String getCh_State() {
		return Ch_State;
	}

	public void setCh_State(String ch_State) {
		Ch_State = ch_State;
	}

	public String getCh_Country() {
		return Ch_Country;
	}

	public void setCh_Country(String ch_Country) {
		Ch_Country = ch_Country;
	}

	public String getCh_Postalcode() {
		return Ch_Postalcode;
	}

	public void setCh_Postalcode(String ch_Postalcode) {
		Ch_Postalcode = ch_Postalcode;
	}

	public String getCh_Emailid() {
		return Ch_Emailid;
	}

	public void setCh_Emailid(String ch_Emailid) {
		Ch_Emailid = ch_Emailid;
	}

	public String getCh_Phoneno() {
		return Ch_Phoneno;
	}

	public void setCh_Phoneno(String ch_Phoneno) {
		Ch_Phoneno = ch_Phoneno;
	}

	public String getRoadBlock() {
		return RoadBlock;
	}

	public void setRoadBlock(String roadBlock) {
		RoadBlock = roadBlock;
	}

	public String getRd_Address() {
		return Rd_Address;
	}

	public void setRd_Address(String rd_Address) {
		Rd_Address = rd_Address;
	}

	public String getRd_City() {
		return Rd_City;
	}

	public void setRd_City(String rd_City) {
		Rd_City = rd_City;
	}

	public String getRd_State() {
		return Rd_State;
	}

	public void setRd_State(String rd_State) {
		Rd_State = rd_State;
	}

	public String getRd_Country() {
		return Rd_Country;
	}

	public void setRd_Country(String rd_Country) {
		Rd_Country = rd_Country;
	}

	public String getRd_Postalcode() {
		return Rd_Postalcode;
	}

	public void setRd_Postalcode(String rd_Postalcode) {
		Rd_Postalcode = rd_Postalcode;
	}

	public String getRd_Emailid() {
		return Rd_Emailid;
	}

	public void setRd_Emailid(String rd_Emailid) {
		Rd_Emailid = rd_Emailid;
	}

	public String getRd_Phoneno() {
		return Rd_Phoneno;
	}

	public void setRd_Phoneno(String rd_Phoneno) {
		Rd_Phoneno = rd_Phoneno;
	}

	public String getKeyDeliverables() {
		return KeyDeliverables;
	}

	public void setKeyDeliverables(String keyDeliverables) {
		KeyDeliverables = keyDeliverables;
	}

	public String getKd_Address() {
		return Kd_Address;
	}

	public void setKd_Address(String kd_Address) {
		Kd_Address = kd_Address;
	}

	public String getKd_City() {
		return Kd_City;
	}

	public void setKd_City(String kd_City) {
		Kd_City = kd_City;
	}

	public String getKd_State() {
		return Kd_State;
	}

	public void setKd_State(String kd_State) {
		Kd_State = kd_State;
	}

	public String getKd_Country() {
		return Kd_Country;
	}

	public void setKd_Country(String kd_Country) {
		Kd_Country = kd_Country;
	}

	public String getKd_Postalcode() {
		return Kd_Postalcode;
	}

	public void setKd_Postalcode(String kd_Postalcode) {
		Kd_Postalcode = kd_Postalcode;
	}

	public String getKd_Emailid() {
		return Kd_Emailid;
	}

	public void setKd_Emailid(String kd_Emailid) {
		Kd_Emailid = kd_Emailid;
	}

	public String getKd_Phoneno() {
		return Kd_Phoneno;
	}

	public void setKd_Phoneno(String kd_Phoneno) {
		Kd_Phoneno = kd_Phoneno;
	}

	@Column(name = "EquipmentFlag")
	private Integer EquipmentFlag;

	public Integer getEquipmentFlag() {
		return EquipmentFlag;
	}

	public void setEquipmentFlag(Integer equipmentFlag) {
		EquipmentFlag = equipmentFlag;
	}

	public String getCodText() {
		return CodText;
	}

	public void setCodText(String codText) {
		CodText = codText;
	}

	public Date getCod() {
		return Cod;
	}

	public void setCod(Date cod) {
		Cod = cod;
	}

	@Column(name = "CorrectiveVisits_Service")
	private String CorrectiveVisits_Service;

	@Column(name = "CorrectiveVisits_Frequency")
	private String CorrectiveVisits_Frequency;

	@Column(name = "Assethealth_Service")
	private String Assethealth_Service;

	@Column(name = "Assethealth_Frequency")
	private String Assethealth_Frequency;

	@Column(name = "Insurance_Service")
	private String Insurance_Service;

	@Column(name = "Insurance_Frequency")
	private String Insurance_Frequency;

	@Column(name = "Warranty_Service")
	private String Warranty_Service;

	@Column(name = "Warranty_Frequency")
	private String Warranty_Frequency;

	@Column(name = "CompletePreventive_Service")
	private String CompletePreventive_Service;

	@Column(name = "CompletePreventive_Frequency")
	private String CompletePreventive_Frequency;

	@Column(name = "EquipmentRepair")
	private String EquipmentRepair;

	public String getCorrectiveVisits_Service() {
		return CorrectiveVisits_Service;
	}

	public void setCorrectiveVisits_Service(String correctiveVisits_Service) {
		CorrectiveVisits_Service = correctiveVisits_Service;
	}

	public String getCorrectiveVisits_Frequency() {
		return CorrectiveVisits_Frequency;
	}

	public void setCorrectiveVisits_Frequency(String correctiveVisits_Frequency) {
		CorrectiveVisits_Frequency = correctiveVisits_Frequency;
	}

	public String getAssethealth_Service() {
		return Assethealth_Service;
	}

	public void setAssethealth_Service(String assethealth_Service) {
		Assethealth_Service = assethealth_Service;
	}

	public String getAssethealth_Frequency() {
		return Assethealth_Frequency;
	}

	public void setAssethealth_Frequency(String assethealth_Frequency) {
		Assethealth_Frequency = assethealth_Frequency;
	}

	public String getInsurance_Service() {
		return Insurance_Service;
	}

	public void setInsurance_Service(String insurance_Service) {
		Insurance_Service = insurance_Service;
	}

	public String getInsurance_Frequency() {
		return Insurance_Frequency;
	}

	public void setInsurance_Frequency(String insurance_Frequency) {
		Insurance_Frequency = insurance_Frequency;
	}

	public String getWarranty_Service() {
		return Warranty_Service;
	}

	public void setWarranty_Service(String warranty_Service) {
		Warranty_Service = warranty_Service;
	}

	public String getWarranty_Frequency() {
		return Warranty_Frequency;
	}

	public void setWarranty_Frequency(String warranty_Frequency) {
		Warranty_Frequency = warranty_Frequency;
	}

	public String getCompletePreventive_Service() {
		return CompletePreventive_Service;
	}

	public void setCompletePreventive_Service(String completePreventive_Service) {
		CompletePreventive_Service = completePreventive_Service;
	}

	public String getCompletePreventive_Frequency() {
		return CompletePreventive_Frequency;
	}

	public void setCompletePreventive_Frequency(String completePreventive_Frequency) {
		CompletePreventive_Frequency = completePreventive_Frequency;
	}

	public String getEquipmentRepair() {
		return EquipmentRepair;
	}

	public void setEquipmentRepair(String equipmentRepair) {
		EquipmentRepair = equipmentRepair;
	}

}
