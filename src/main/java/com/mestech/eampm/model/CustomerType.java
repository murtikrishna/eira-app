package com.mestech.eampm.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/******************************************************
 * 
 * Filename : CustomerType
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/

@Entity
@Table(name="mCustomerType")
public class CustomerType implements Serializable{

	private static final long serialVersionUID = -723583058586873479L;
	 
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "CustomerTypeID")
	 private Integer CustomerTypeId;
	 
	 @Column(name="CustomerType")
	 private String CustomerType;
	 
	 @Column(name="ShortName")
	 private String ShortName;
	 
	 @Column(name="Description")
	 private String Description;
	 
	 @Column(name="ActiveFlag")
	 private Integer ActiveFlag;

	 @Column(name="CreationDate")
	 private Date CreationDate;

	 @Column(name="CreatedBy")
	 private Integer CreatedBy;

	 @Column(name="LastUpdatedDate")
	 private Date LastUpdatedDate;

	 @Column(name="LastUpdatedBy")
	 private Integer LastUpdatedBy;

	public Integer getCustomerTypeId() {
		return CustomerTypeId;
	}

	public void setCustomerTypeId(Integer customerTypeId) {
		CustomerTypeId = customerTypeId;
	}

	public String getCustomerType() {
		return CustomerType;
	}

	public void setCustomerType(String customerType) {
		CustomerType = customerType;
	}

	public String getShortName() {
		return ShortName;
	}

	public void setShortName(String shortName) {
		ShortName = shortName;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
		
	
}
