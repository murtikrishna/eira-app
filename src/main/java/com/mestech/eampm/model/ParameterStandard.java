package com.mestech.eampm.model;

import java.io.Serializable;
import java.util.Date;

import javax.enterprise.inject.Default;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

/******************************************************
 * 
 * Filename :ParameterStandard
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/

@Entity
@Table(name = "mParameterStandards")
public class ParameterStandard implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "StandardId")
	private Integer StandardId;

	@Column(name = "StandardParameterName")
	private String StandardParameterName;

	@Column(name = "StandardParameterUom")
	private String StandardParameterUom;

	@Column(name = "ActiveFlag")
	private Integer ActiveFlag;

	@Column(name = "CreationDate")
	private Date CreationDate;

	@Column(name = "LastUpdatedDate")
	private Date LastUpdatedDate;

	@Column(name = "CreatedBy")
	private Integer CreatedBy;

	@Column(name = "LastUpdatedBy")
	private Integer LastUpdatedBy;

	@Column(name = "ParameterDescription")
	private String ParameterDescription;

	@Column(name = "CoreParameterFlag")
	private int CoreParameterFlag;

	public Integer getStandardId() {
		return StandardId;
	}

	public void setStandardId(Integer standardId) {
		StandardId = standardId;
	}

	public String getStandardParameterName() {
		return StandardParameterName;
	}

	public void setStandardParameterName(String standardParameterName) {
		StandardParameterName = standardParameterName;
	}

	public String getStandardParameterUom() {
		return StandardParameterUom;
	}

	public void setStandardParameterUom(String standardParameterUom) {
		StandardParameterUom = standardParameterUom;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	public String getParameterDescription() {
		return ParameterDescription;
	}

	public void setParameterDescription(String parameterDescription) {
		ParameterDescription = parameterDescription;
	}

	public int getCoreParameterFlag() {
		return CoreParameterFlag;
	}

	public void setCoreParameterFlag(int coreParameterFlag) {
		CoreParameterFlag = coreParameterFlag;
	}

	@Column(name = "ParameterCode")
	private String ParameterCode;

	public String getParameterCode() {
		return ParameterCode;
	}

	public void setParameterCode(String parameterCode) {
		ParameterCode = parameterCode;
	}

	@Transient
	private String CreationDateText;

	@Transient
	private String LastUpdatedDateText;

	public String getCreationDateText() {
		return CreationDateText;
	}

	public void setCreationDateText(String creationDateText) {
		CreationDateText = creationDateText;
	}

	public String getLastUpdatedDateText() {
		return LastUpdatedDateText;
	}

	public void setLastUpdatedDateText(String lastUpdatedDateText) {
		LastUpdatedDateText = lastUpdatedDateText;
	}

	@Column(name = "ApprovedBy")
	private String ApprovedBy;

	@Column(name = "Remarks")
	private String Remarks;

	public String getApprovedBy() {
		return ApprovedBy;
	}

	public void setApprovedBy(String approvedBy) {
		ApprovedBy = approvedBy;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}

}
