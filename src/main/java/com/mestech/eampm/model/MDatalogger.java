package com.mestech.eampm.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/******************************************************
 * 
 * Filename :MDatalogger
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/
@Entity
@Table(name = "MDatalogger")
public class MDatalogger implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4119886224303597914L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private BigInteger dataloggerid;

	private String description;

	private String remarks;

	private Integer activeflag;

	private Date creationdate;

	private Date lastupdateddate;

	private Integer createdby, lastupdatedby;

	public BigInteger getDataloggerid() {
		return dataloggerid;
	}

	public void setDataloggerid(BigInteger dataloggerid) {
		this.dataloggerid = dataloggerid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getActiveflag() {
		return activeflag;
	}

	public void setActiveflag(Integer activeflag) {
		this.activeflag = activeflag;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public Date getLastupdateddate() {
		return lastupdateddate;
	}

	public void setLastupdateddate(Date lastupdateddate) {
		this.lastupdateddate = lastupdateddate;
	}

	public Integer getCreatedby() {
		return createdby;
	}

	public void setCreatedby(Integer createdby) {
		this.createdby = createdby;
	}

	public Integer getLastupdatedby() {
		return lastupdatedby;
	}

	public void setLastupdatedby(Integer lastupdatedby) {
		this.lastupdatedby = lastupdatedby;
	}

}
