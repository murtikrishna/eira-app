package com.mestech.eampm.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/******************************************************
 * 
 * Filename : MServiceCode
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/
@Entity
@Table(name = "MServiceCode")
public class MServiceCode implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6593160150842145325L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger servicecode;
	private BigInteger createdby, lastupdatedby;
	private String description, remarks;
	private Integer activeflag;
	private Date creationdate, lastupdateddate;

	public BigInteger getServicecode() {
		return servicecode;
	}

	public void setServicecode(BigInteger servicecode) {
		this.servicecode = servicecode;
	}

	public BigInteger getCreatedby() {
		return createdby;
	}

	public void setCreatedby(BigInteger createdby) {
		this.createdby = createdby;
	}

	public BigInteger getLastupdatedby() {
		return lastupdatedby;
	}

	public void setLastupdatedby(BigInteger lastupdatedby) {
		this.lastupdatedby = lastupdatedby;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getActiveflag() {
		return activeflag;
	}

	public void setActiveflag(Integer activeflag) {
		this.activeflag = activeflag;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public Date getLastupdateddate() {
		return lastupdateddate;
	}

	public void setLastupdateddate(Date lastupdateddate) {
		this.lastupdateddate = lastupdateddate;
	}

}
