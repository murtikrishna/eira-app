package com.mestech.eampm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/******************************************************
 * 
 * Filename :Status
 * 
 * 
 * Author : Sarath Babu E
 * 
 * Time Stamp : 25-Apr-19 01:44 PM
 * 
 * Description :
 * 
 *******************************************************/

@Entity
@Table(name = "mStatus")
public class Status implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "StatusID")
	private Integer StatusId;

	@Column(name = "StatusCode")
	private String StatusCode;

	@Column(name = "Status")
	private String Status;

	@Column(name = "DerivedStatus")
	private Integer DerivedStatus;

	@Column(name = "Description")
	private String Description;

	@Column(name = "LastUpdatedDate")
	private Date LastUpdatedDate;

	@Column(name = "CreationDate")
	private Date CreationDate;

	@Column(name = "LastUpdatedBy")
	private Integer LastUpdatedBy;

	public Integer getStatusId() {
		return StatusId;
	}

	public void setStatusId(Integer statusId) {
		StatusId = statusId;
	}

	public String getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(String statusCode) {
		StatusCode = statusCode;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	@Transient
	private String CreationDateText;

	@Transient
	private String LastUpdatedDateText;

	public String getCreationDateText() {
		return CreationDateText;
	}

	public void setCreationDateText(String creationDateText) {
		CreationDateText = creationDateText;
	}

	public String getLastUpdatedDateText() {
		return LastUpdatedDateText;
	}

	public void setLastUpdatedDateText(String lastUpdatedDateText) {
		LastUpdatedDateText = lastUpdatedDateText;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	@Transient
	private String DerivedStatusText;

	public Integer getDerivedStatus() {
		return DerivedStatus;
	}

	public void setDerivedStatus(Integer derivedStatus) {
		DerivedStatus = derivedStatus;
	}

	public String getDerivedStatusText() {
		return DerivedStatusText;
	}

	public void setDerivedStatusText(String derivedStatusText) {
		DerivedStatusText = derivedStatusText;
	}

	@Column(name = "ActiveFlag")
	private Integer ActiveFlag;

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	@Column(name = "CreatedBy")
	private Integer CreatedBy;

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	@Column(name = "DataLoggerTypeId")
	private Integer DataLoggerTypeId;

	public Integer getDataLoggerTypeId() {
		return DataLoggerTypeId;
	}

	public void setDataLoggerTypeId(Integer dataLoggerTypeId) {
		DataLoggerTypeId = dataLoggerTypeId;
	}

	@Transient
	private String DataLoggerTypeIdText;

	public String getDataLoggerTypeIdText() {
		return DataLoggerTypeIdText;
	}

	public void setDataLoggerTypeIdText(String dataLoggerTypeIdText) {
		DataLoggerTypeIdText = dataLoggerTypeIdText;
	}

}
