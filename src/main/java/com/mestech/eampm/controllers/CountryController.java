
/******************************************************
 * 
 *    	Filename	: CountryController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 25-Apr-19 01:44 PM 
 *      
 *      Description : This controller deals with the country related actions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.CountryRegion;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.User;
import com.mestech.eampm.model.UserRole;
import com.mestech.eampm.service.CountryRegionService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class CountryController {

	@Autowired
	private CountryService countryService;

	@Autowired
	private CountryRegionService countryRegionService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns dropdown values for country dropdown.
	 * 
	 * Input Params  : DropdownName
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "CountryRegion") {
			List<CountryRegion> ddlList = countryRegionService.listCountryRegions();

			for (int i = 0; i < ddlList.size(); i++) {
				if (ddlList.get(i).getCountryRegionId() != null) {
					ddlMap.put(ddlList.get(i).getCountryRegionId().toString(), ddlList.get(i).getCountryRegionName());
				}

			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getUpdatedCountryList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns list of countries.
	 * 
	 * Return Value  : List<Country>
	 * 
	 * 
	 **********************************************************************************************/
	public List<Country> getUpdatedCountryList() {
		List<Country> ddlCountriesList = countryService.listCountries();
		List<CountryRegion> ddlCountryRegionList = countryRegionService.listCountryRegions();

		for (int i = 0; i < ddlCountriesList.size(); i++) {

			List<CountryRegion> lstCountryRegionList = new ArrayList<CountryRegion>();
			int cid = ddlCountriesList.get(i).getRegionID();
			lstCountryRegionList = ddlCountryRegionList.stream().filter(p -> p.getCountryRegionId() == cid)
					.collect(Collectors.toList());
			if (lstCountryRegionList.size() > 0) {
				ddlCountriesList.get(i).setRegionName(lstCountryRegionList.get(0).getCountryRegionName());
			}

			/*
			 * //String RegionName =
			 * countryRegionService.getCountryRegionById(ddlCountriesList.get(i).getRegionID
			 * ()).getCountryRegionName(); String RegionName = "";
			 * if(ddlCountriesList.get(i).getCountryRegion()!= null) { RegionName =
			 * ddlCountriesList.get(i).getCountryRegion().getCountryRegionName(); }
			 * 
			 * ddlCountriesList.get(i).setRegionName(RegionName);
			 */
		}

		return ddlCountriesList;
	}

	/********************************************************************************************
	 * 
	 * Function Name : listCountries
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads country page.
	 * 
	 * Input Params  : model,session
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/countries", method = RequestMethod.GET)
	public String listCountries(Model model, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());

		AccessListBean objAccessListBean = new AccessListBean();

		try {
			User objUser = userService.getUserById(id);
			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
			List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getUserName());

			if (objUser.getRoleID() != 4) {
				objAccessListBean.setCustomerListView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActivityID() == 1 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setDashboard("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 2 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setOverview("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 3 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setSystemMonitoring("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 4 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setVisualization("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 5 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalytics("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 6 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setPortfolioManagement("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 7 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setTicketing("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 8 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setForcasting("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 9 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setConfiguration("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 10
						&& lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalysis("visible");
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("access", objAccessListBean);

		model.addAttribute("country", new Country());

		model.addAttribute("countryList", getUpdatedCountryList());
		model.addAttribute("countryRegionList", getDropdownList("CountryRegion"));
		model.addAttribute("activeStatusList", getStatusDropdownList());

		return "country";
	}

	/********************************************************************************************
	 * 
	 * Function Name : addnewcountry
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This method adds or updates country
	 * 
	 * Input Params  : country,session
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// Same method For both Add and Update Country
	// @RequestMapping(value = "/country/add", method = RequestMethod.POST)
	@RequestMapping(value = "/addnewcountry", method = RequestMethod.POST)
	public String addcountry(@ModelAttribute("country") Country country, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		Date date = new Date();
		if (country.getCountryId() == null || country.getCountryId() == 0) {
			// new country, add it
			country.setCreationDate(date);
			countryService.addCountry(country);
		} else {
			// existing country, call update
			country.setLastUpdatedDate(date);
			countryService.updateCountry(country);
		}

		return "redirect:/countries";

	}

	/********************************************************************************************
	 * 
	 * Function Name : removecountry
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This method removes a country
	 * 
	 * Input Params  : id,session
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// @RequestMapping("/country/remove/{id}")
	@RequestMapping("/removecountry{id}")
	public String removecountry(@PathVariable("id") int id, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		countryService.removeCountry(id);
		return "redirect:/countries";
	}

	/********************************************************************************************
	 * 
	 * Function Name : editcountry
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This method used to update country.
	 * 
	 * Input Params  : id,model,session
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	// @RequestMapping("/country/edit/{id}")
	@RequestMapping("/editcountry{id}")
	public String editcountry(@PathVariable("id") int id, Model model, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		int refid = Integer.parseInt(session.getAttribute("eampmuserid").toString());
		AccessListBean objAccessListBean = new AccessListBean();

		try {
			User objUser = userService.getUserById(refid);
			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
			List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(refid);
			objAccessListBean.setUserName(objUser.getUserName());

			if (objUser.getRoleID() != 4) {
				objAccessListBean.setCustomerListView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActivityID() == 1 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setDashboard("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 2 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setOverview("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 3 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setSystemMonitoring("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 4 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setVisualization("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 5 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalytics("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 6 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setPortfolioManagement("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 7 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setTicketing("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 8 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setForcasting("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 9 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setConfiguration("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 10
						&& lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalysis("visible");
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("access", objAccessListBean);

		model.addAttribute("country", countryService.getCountryById(id));

		model.addAttribute("countryList", getUpdatedCountryList());
		model.addAttribute("countryRegionList", getDropdownList("CountryRegion"));
		model.addAttribute("activeStatusList", getStatusDropdownList());

		return "country";
	}
}
