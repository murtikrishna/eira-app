
/******************************************************
 * 
 *    	Filename	: TicketDetailController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Ticket detail Activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.bean.TicketFilter;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EventDetail;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.CurrencyService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteTypeService;
import com.mestech.eampm.service.StateService;
import com.mestech.eampm.service.TicketDetailService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class TicketDetailController {

	@Autowired
	private TicketDetailService ticketdetailService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private CustomerService customerService;
	@Autowired
	private SiteTypeService sitetypeService;
	@Autowired
	private CountryService countryService;
	@Autowired
	private StateService stateService;
	@Autowired
	private CurrencyService currencyService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown.
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : TicketGridLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function retrns ticket details for ticket page.
	 * 
	 * Input Params  : session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/ticketdetail", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> TicketGridLoad(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<TicketDetail> lstTicketDetail = ticketdetailService.getTicketDetailListByUserId(id, timezonevalue);
				List<Site> lstSite = siteService.getSiteListByUserId(id);

				List<Equipment> lstEquipment = equipmentService.listEquipmentsByUserId(id);
				List<User> lstUser = userService.listUsers();

				// System.out.println("Mohan : " +lstTicketDetail.size());

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				TicketFilter objTicketFilter = new TicketFilter();

				AccessListBean objAccessListBean = new AccessListBean();

				try {

					for (int i = 0; i < lstTicketDetail.size(); i++) {
						Integer intSiteID = lstTicketDetail.get(i).getSiteID();
						Integer intAssignedID = lstTicketDetail.get(i).getAssignedTo();
						Integer intequipmentID = lstTicketDetail.get(i).getEquipmentID();
						Integer intCreatedBy = lstTicketDetail.get(i).getCreatedBy();

						if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
								.size() >= 1) {
							Site objSite = new Site();
							objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setSiteName(objSite.getSiteName());
						}
						if (lstEquipment.stream().filter(p -> p.getEquipmentId().equals(intequipmentID))
								.collect(Collectors.toList()).size() >= 1) {
							Equipment objEquipment = new Equipment();
							objEquipment = lstEquipment.stream().filter(p -> p.getEquipmentId().equals(intequipmentID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setEquipmentName(objEquipment.getCustomerNaming().toString());
						}

						if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setAssignedToWhom(objUser.getUserName());

						}
						if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setCreatedByName(objUser.getUserName());

						}

						if (lstTicketDetail.get(i).getCreationDate() != null) {
							lstTicketDetail.get(i).setCreationDate(lstTicketDetail.get(i).getCreationDate());
							lstTicketDetail.get(i)
									.setCreatedDateText(sdf.format(lstTicketDetail.get(i).getCreationDate()));

							// System.out.println( " Mohanraj ::: " +
							// sdf.format(lstTicketDetail.get(i).getCreationDate()));
						}

						if (lstTicketDetail.get(i).getScheduledOn() != null) {
							lstTicketDetail.get(i).setScheduledOn(lstTicketDetail.get(i).getScheduledOn());
							lstTicketDetail.get(i)
									.setScheduledDateText(sdfdate.format(lstTicketDetail.get(i).getScheduledOn()));

						}

						lstTicketDetail.get(i).setTimeZone(timezonevalue);

					}

					User objUser = userService.getUserById(id);
					// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
					List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

					lstRoleActivity = roleActivityService.listRoleActivities().stream()
							.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

					objAccessListBean.setUserID(id);
					objAccessListBean.setUserName(objUser.getShortName());

					if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
						objAccessListBean.setCustomerListView("visible");
					}
					if (objUser.getRoleID() == 6) {
						objAccessListBean.setMonitoringView("visible");
					}

					for (int l = 0; l < lstRoleActivity.size(); l++) {
						if (lstRoleActivity.get(l).getActivityID() == 1
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setDashboard("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 2
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setOverview("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 3
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setSystemMonitoring("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 4
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setVisualization("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 5
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setAnalytics("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 6
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setPortfolioManagement("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 7
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setTicketing("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 8
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setForcasting("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 9
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setConfiguration("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 10
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setAnalysis("visible");
						}
					}

				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

				objResponseEntity.put("access", objAccessListBean);
				objResponseEntity.put("siteList", getDropdownList("Site", id));
				objResponseEntity.put("ticketfilter", objTicketFilter);
				objResponseEntity.put("ticketdetaillist", lstTicketDetail);
				objResponseEntity.put("siteList", getDropdownList("Site", id));
				objResponseEntity.put("userList", getDropdownList("User", id));

				objResponseEntity.put("ticketcreation", new TicketDetail());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : TicketFilterLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns ticket details with filter.
	 * 
	 * Input Params  : lstfilter,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/ticketfilterdetail", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> TicketFilterLoad(@RequestBody List<TicketFilter> lstfilter,
			HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				TicketFilter ticketfilter = lstfilter.get(0);

				Integer siteid = ticketfilter.getSiteID();
				String state = ticketfilter.getState();

				String category = ticketfilter.getTicketCategory();

				String priority = ticketfilter.getPriority();

				String fromdate = ticketfilter.getFromDate();

				String todate = ticketfilter.getToDate();

				System.out.println(siteid);
				System.out.println(state);
				System.out.println(category);
				System.out.println(priority);
				System.out.println(fromdate);
				System.out.println(todate);

				int varSiteID = 0;
				String varFromDate = "";
				String varToDate = "";
				String varTicketCategory = "";
				String varTicketType = "";
				String varPriority = "";
				String varState = "";

				if (ticketfilter != null) {
					if (ticketfilter.getSiteID() != null) {
						varSiteID = ticketfilter.getSiteID();
					}
					if (ticketfilter.getFromDate() != null) {
						varFromDate = ticketfilter.getFromDate();
					}
					if (ticketfilter.getToDate() != null) {
						varToDate = ticketfilter.getToDate();
					}
					if (ticketfilter.getTicketCategory() != null) {
						varTicketCategory = ticketfilter.getTicketCategory();
					}
					if (ticketfilter.getTicketType() != null) {
						varTicketType = ticketfilter.getTicketType();
					}
					if (ticketfilter.getPriority() != null) {
						varPriority = ticketfilter.getPriority();
					}
					if (ticketfilter.getState() != null) {
						varState = ticketfilter.getState();
					}

				}

				List<TicketDetail> lstTicketDetail = ticketdetailService.listTicketDetailsForDisplay(id,
						String.valueOf(siteid), fromdate, todate, category, state, priority, timezonevalue);

				System.out.println(lstTicketDetail.size());

				List<Site> lstSite = siteService.getSiteListByUserId(id);
				List<User> lstUser = userService.listUsers();
				List<Equipment> lstEquipment = equipmentService.listEquipmentsByUserId(id);

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				AccessListBean objAccessListBean = new AccessListBean();

				try {

					for (int i = 0; i < lstTicketDetail.size(); i++) {
						Integer intSiteID = lstTicketDetail.get(i).getSiteID();
						Integer intAssignedID = lstTicketDetail.get(i).getAssignedTo();
						Integer intCreatedBy = lstTicketDetail.get(i).getCreatedBy();
						if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
								.size() >= 1) {
							Site objSite = new Site();
							objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setSiteName(objSite.getSiteName());
						}

						if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setAssignedToWhom(objUser.getUserName());

						}
						if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setCreatedByName(objUser.getUserName());

						}

						if (lstTicketDetail.get(i).getCreationDate() != null) {
							lstTicketDetail.get(i).setCreationDate(lstTicketDetail.get(i).getCreationDate());
							lstTicketDetail.get(i)
									.setCreatedDateText(sdf.format(lstTicketDetail.get(i).getCreationDate()));

							// System.out.println( " Mohanraj ::: " +
							// sdf.format(lstTicketDetail.get(i).getCreationDate()));
						}

						if (lstTicketDetail.get(i).getScheduledOn() != null) {
							lstTicketDetail.get(i).setScheduledOn(lstTicketDetail.get(i).getScheduledOn());
							lstTicketDetail.get(i)
									.setScheduledDateText(sdfdate.format(lstTicketDetail.get(i).getScheduledOn()));

						}

						lstTicketDetail.get(i).setTimeZone(timezonevalue);

					}

					User objUser = userService.getUserById(id);
					// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
					List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

					lstRoleActivity = roleActivityService.listRoleActivities().stream()
							.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

					objAccessListBean.setUserID(id);
					objAccessListBean.setUserName(objUser.getUserName());

					if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
						objAccessListBean.setCustomerListView("visible");
					}
					if (objUser.getRoleID() == 6) {
						objAccessListBean.setMonitoringView("visible");
					}

					for (int l = 0; l < lstRoleActivity.size(); l++) {
						if (lstRoleActivity.get(l).getActivityID() == 1
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setDashboard("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 2
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setOverview("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 3
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setSystemMonitoring("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 4
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setVisualization("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 5
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setAnalytics("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 6
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setPortfolioManagement("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 7
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setTicketing("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 8
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setForcasting("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 9
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setConfiguration("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 10
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setAnalysis("visible");
						}
					}

				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

				TicketFilter objTicketFilter = new TicketFilter();

				objTicketFilter.setSiteID(ticketfilter.getSiteID());
				objTicketFilter.setFromDate(ticketfilter.getFromDate());
				objTicketFilter.setToDate(ticketfilter.getToDate());
				objTicketFilter.setTicketCategory(ticketfilter.getTicketCategory());
				objTicketFilter.setState(ticketfilter.getState());
				objTicketFilter.setTicketType(ticketfilter.getTicketType());
				objTicketFilter.setPriority(ticketfilter.getPriority());

				objResponseEntity.put("access", objAccessListBean);
				objResponseEntity.put("siteList", getDropdownList("Site", id));
				objResponseEntity.put("ticketfilter", objTicketFilter);
				objResponseEntity.put("ticketfilterdetaillist", lstTicketDetail);
				objResponseEntity.put("siteList", getDropdownList("Site", id));
				objResponseEntity.put("userList", getDropdownList("User", id));
				objResponseEntity.put("ticketcreation", new TicketDetail());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : TicketsPageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads the ticket details page.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/ticketdetails", method = RequestMethod.GET)
	public String TicketsPageLoad(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String timezonevalue = "0";
		if (!timezonevalue.equals("") && timezonevalue != null) {
			timezonevalue = session.getAttribute("timezonevalue").toString();
		}
		loginUserDetails.setTimezoneoffsetmin(timezonevalue);

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());
			objAccessListBean.setMySiteFilter(false);
			objAccessListBean.setMySiteID(0);

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("ticketfilter", new TicketFilter());
		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("ticketdetail", new TicketDetail());

		model.addAttribute("siteList", getDropdownList("Site", id));
		model.addAttribute("userList", getDropdownList("User", id));

		model.addAttribute("equipmentList", getDropdownList("Equipment", id));
		model.addAttribute("ticketcreation", new TicketDetail());

		return "ticketdetail";
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		}

		if (DropdownName == "CustomerSites") {
			List<Site> ddlList = siteService.getSiteListByCustomerId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "SingleSite") {
			List<Site> ddlList = siteService.listSiteById(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "User") {
			List<User> ddlList = userService.listFieldUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : SiteTicketsPageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads site ticket details page.
	 * 
	 * Input Params  : siteid,model,session
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/siteticketdetails{id}", method = RequestMethod.GET)
	public String SiteTicketsPageLoad(@PathVariable("id") int siteid, Model model, HttpSession session,
			Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int userid = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(userid);

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(userid);
			objAccessListBean.setUserName(objUser.getShortName());

			objAccessListBean.setMySiteFilter(true);
			objAccessListBean.setMySiteID(siteid);

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("ticketfilter", new TicketFilter());

		model.addAttribute("eventdetail", new EventDetail());
		model.addAttribute("access", objAccessListBean);

		model.addAttribute("siteList", getDropdownList("Site", userid));
		model.addAttribute("userList", getDropdownList("User", userid));

		// model.addAttribute("errorcodeList", getDropdownList("ErrorCode",userid));
		// model.addAttribute("equipmentList", getDropdownList("Equipment",userid));
		model.addAttribute("ticketcreation", new TicketDetail());

		return "ticketdetail";
	}

	/********************************************************************************************
	 * 
	 * Function Name : SiteTicketDetailsGridLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns ticket details for a site.
	 * 
	 * Input Params  : id,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/siteticketdetail", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> SiteTicketDetailsGridLoad(Integer id, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			} else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int userid = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				int varSiteID = 0;
				String varFromDate = "";
				String varToDate = "";
				String varTicketCategory = "";
				String varTicketType = "";
				String varPriority = "";
				String varState = "";

				List<TicketDetail> lstTicketDetail = ticketdetailService.getTicketDetailListBySiteId(id, timezonevalue);

				// Site objSite = siteService.getSiteById(id);

				List<Site> lstSite = siteService.listSiteById(id);
				List<User> lstUser = userService.listUsers();
				List<Equipment> lstEquipment = equipmentService.listEquipmentsBySiteId(id);

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				AccessListBean objAccessListBean = new AccessListBean();

				try {

					for (int i = 0; i < lstTicketDetail.size(); i++) {
						Integer intSiteID = lstTicketDetail.get(i).getSiteID();
						Integer intAssignedID = lstTicketDetail.get(i).getAssignedTo();
						Integer intCreatedBy = lstTicketDetail.get(i).getCreatedBy();

						if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
								.size() >= 1) {
							Site objSite = new Site();
							objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setSiteName(objSite.getSiteName());
						}

						if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setAssignedToWhom(objUser.getUserName());

						}

						if (lstTicketDetail.get(i).getCreationDate() != null) {
							lstTicketDetail.get(i).setCreationDate(lstTicketDetail.get(i).getCreationDate());
							lstTicketDetail.get(i)
									.setCreatedDateText(sdf.format(lstTicketDetail.get(i).getCreationDate()));

							// System.out.println( " Mohanraj ::: " +
							// sdf.format(lstTicketDetail.get(i).getCreationDate()));
						}

						if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setCreatedByName(objUser.getUserName());

						}

						if (lstTicketDetail.get(i).getScheduledOn() != null) {
							lstTicketDetail.get(i).setScheduledOn(lstTicketDetail.get(i).getScheduledOn());
							lstTicketDetail.get(i)
									.setScheduledDateText(sdfdate.format(lstTicketDetail.get(i).getScheduledOn()));

						}

					}

					User objUser = userService.getUserById(userid);
					// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
					List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

					lstRoleActivity = roleActivityService.listRoleActivities().stream()
							.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

					objAccessListBean.setUserID(userid);
					objAccessListBean.setUserName(objUser.getUserName());
					objAccessListBean.setFilterFlag(1);
					objAccessListBean.setMySiteID(id);

					if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
						objAccessListBean.setCustomerListView("visible");
					}
					if (objUser.getRoleID() == 6) {
						objAccessListBean.setMonitoringView("visible");
					}

					for (int l = 0; l < lstRoleActivity.size(); l++) {
						if (lstRoleActivity.get(l).getActivityID() == 1
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setDashboard("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 2
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setOverview("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 3
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setSystemMonitoring("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 4
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setVisualization("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 5
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setAnalytics("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 6
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setPortfolioManagement("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 7
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setTicketing("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 8
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setForcasting("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 9
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setConfiguration("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 10
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setAnalysis("visible");
						}
					}

				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

				objResponseEntity.put("access", objAccessListBean);
				objResponseEntity.put("siteList", getDropdownList("SingleSite", id));
				objResponseEntity.put("ticketfilter", new TicketFilter());
				objResponseEntity.put("ticketdetaillist", lstTicketDetail);

				objResponseEntity.put("userList", getDropdownList("User", id));
				objResponseEntity.put("ticketcreation", new TicketDetail());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : SitefilterTicketDetails
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns tickets for site with filter.
	 * 
	 * Input Params  : id,lstfilter,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/sitefilterticketdetails{id}", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> SitefilterTicketDetails(@PathVariable("id") int id,
			@RequestBody List<TicketFilter> lstfilter, HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int userid = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				TicketFilter ticketfilter = lstfilter.get(0);

				Integer siteid = ticketfilter.getSiteID();
				String state = ticketfilter.getState();

				String category = ticketfilter.getTicketCategory();

				String priority = ticketfilter.getPriority();

				String fromdate = ticketfilter.getFromDate();

				String todate = ticketfilter.getToDate();

				System.out.println(siteid);
				System.out.println(state);
				System.out.println(category);
				System.out.println(priority);
				System.out.println(fromdate);
				System.out.println(todate);
				System.out.println(id);

				int varSiteID = 0;
				String varFromDate = "";
				String varToDate = "";
				String varTicketCategory = "";
				String varTicketType = "";
				String varPriority = "";
				String varState = "";

				if (ticketfilter != null) {
					if (ticketfilter.getSiteID() != null) {
						varSiteID = ticketfilter.getSiteID();
					}
					if (ticketfilter.getFromDate() != null) {
						varFromDate = ticketfilter.getFromDate();
					}
					if (ticketfilter.getToDate() != null) {
						varToDate = ticketfilter.getToDate();
					}
					if (ticketfilter.getTicketCategory() != null) {
						varTicketCategory = ticketfilter.getTicketCategory();
					}
					if (ticketfilter.getTicketType() != null) {
						varTicketType = ticketfilter.getTicketType();
					}
					if (ticketfilter.getPriority() != null) {
						varPriority = ticketfilter.getPriority();
					}
					if (ticketfilter.getState() != null) {
						varState = ticketfilter.getState();
					}

				}

				List<TicketDetail> lstTicketDetail = ticketdetailService.listTicketDetailsForDisplay(userid,
						String.valueOf(id), fromdate, todate, category, state, priority, timezonevalue);

				System.out.println(lstTicketDetail.size());

				Site objSite = siteService.getSiteById(id);
				List<User> lstUser = userService.listUsers();
				List<Equipment> lstEquipment = equipmentService.listEquipmentsByUserId(id);

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				AccessListBean objAccessListBean = new AccessListBean();

				try {

					for (int i = 0; i < lstTicketDetail.size(); i++) {
						Integer intSiteID = lstTicketDetail.get(i).getSiteID();
						Integer intAssignedID = lstTicketDetail.get(i).getAssignedTo();
						Integer intCreatedBy = lstTicketDetail.get(i).getCreatedBy();

						if (objSite != null) {

							lstTicketDetail.get(i).setSiteName(objSite.getSiteName());
						}

						if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setAssignedToWhom(objUser.getUserName());

						}

						if (lstTicketDetail.get(i).getCreationDate() != null) {
							lstTicketDetail.get(i).setCreationDate(lstTicketDetail.get(i).getCreationDate());
							lstTicketDetail.get(i)
									.setCreatedDateText(sdf.format(lstTicketDetail.get(i).getCreationDate()));

							// System.out.println( " Mohanraj ::: " +
							// sdf.format(lstTicketDetail.get(i).getCreationDate()));
						}
						if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setCreatedByName(objUser.getUserName());

						}

						if (lstTicketDetail.get(i).getScheduledOn() != null) {
							lstTicketDetail.get(i).setScheduledOn(lstTicketDetail.get(i).getScheduledOn());
							lstTicketDetail.get(i)
									.setScheduledDateText(sdfdate.format(lstTicketDetail.get(i).getScheduledOn()));

						}

						System.out.println(lstTicketDetail.get(i).getSiteName());

					}

					User objUser = userService.getUserById(id);
					// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
					List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

					lstRoleActivity = roleActivityService.listRoleActivities().stream()
							.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

					objAccessListBean.setUserID(id);
					objAccessListBean.setUserName(objUser.getUserName());

					if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
						objAccessListBean.setCustomerListView("visible");
					}
					if (objUser.getRoleID() == 6) {
						objAccessListBean.setMonitoringView("visible");
					}

					for (int l = 0; l < lstRoleActivity.size(); l++) {
						if (lstRoleActivity.get(l).getActivityID() == 1
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setDashboard("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 2
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setOverview("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 3
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setSystemMonitoring("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 4
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setVisualization("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 5
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setAnalytics("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 6
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setPortfolioManagement("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 7
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setTicketing("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 8
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setForcasting("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 9
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setConfiguration("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 10
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setAnalysis("visible");
						}
					}

				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

				TicketFilter objTicketFilter = new TicketFilter();

				objTicketFilter.setSiteID(ticketfilter.getSiteID());
				objTicketFilter.setFromDate(ticketfilter.getFromDate());
				objTicketFilter.setToDate(ticketfilter.getToDate());
				objTicketFilter.setTicketCategory(ticketfilter.getTicketCategory());
				objTicketFilter.setState(ticketfilter.getState());
				objTicketFilter.setTicketType(ticketfilter.getTicketType());
				objTicketFilter.setPriority(ticketfilter.getPriority());

				objResponseEntity.put("access", objAccessListBean);
				objResponseEntity.put("siteList", getDropdownList("SingleSite", id));
				objResponseEntity.put("ticketfilter", objTicketFilter);
				objResponseEntity.put("ticketfilterdetaillist", lstTicketDetail);
				objResponseEntity.put("siteList", getDropdownList("Site", id));
				objResponseEntity.put("userList", getDropdownList("User", id));
				objResponseEntity.put("ticketcreation", new TicketDetail());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : CustomerTicketsPageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads customer ticket details page.
	 * 
	 * Input Params  : id,model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customerticketdetails{id}", method = RequestMethod.GET)
	public String CustomerTicketsPageLoad(@PathVariable("id") int id, Model model, HttpSession session,
			Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int userid = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(userid);

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(userid);
			objAccessListBean.setUserName(objUser.getShortName());
			objAccessListBean.setMyCustomerFilter(true);
			objAccessListBean.setMyCustomerID(id);

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("ticketfilter", new TicketFilter());
		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("eventdetail", new EventDetail());
		model.addAttribute("access", objAccessListBean);

		model.addAttribute("siteList", getDropdownList("Site", userid));
		model.addAttribute("userList", getDropdownList("User", userid));

		// model.addAttribute("errorcodeList", getDropdownList("ErrorCode",userid));
		model.addAttribute("equipmentList", getDropdownList("Equipment", userid));
		model.addAttribute("ticketcreation", new TicketDetail());

		return "ticketdetail";
	}

	/********************************************************************************************
	 * 
	 * Function Name : CustomerTicketDetailsGridLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns tickets details for customer.
	 * 
	 * Input Params  : id,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customerticketdetail", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> CustomerTicketDetailsGridLoad(Integer id, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int userid = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				int varSiteID = 0;
				String varFromDate = "";
				String varToDate = "";
				String varTicketCategory = "";
				String varTicketType = "";
				String varPriority = "";
				String varState = "";

				List<TicketDetail> lstTicketDetail = ticketdetailService.getTicketDetailListByCustomerId(id,
						timezonevalue);

				// Site objSite = siteService.getSiteById(id);

//				List<Site> lstSite = siteService.getSiteListByUserId(id);
				List<Site> lstSite =siteService.getSiteListByCustomerId(id);
				List<User> lstUser = userService.listUsers();
				List<Equipment> lstEquipment = equipmentService.listEquipmentsByUserId(id);

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				AccessListBean objAccessListBean = new AccessListBean();

				try {

					for (int i = 0; i < lstTicketDetail.size(); i++) {
						Integer intSiteID = lstTicketDetail.get(i).getSiteID();
						Integer intAssignedID = lstTicketDetail.get(i).getAssignedTo();
						Integer intCreatedBy = lstTicketDetail.get(i).getCreatedBy();

						if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
								.size() >= 1) {
							Site objSite = new Site();
							objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setSiteName(objSite.getSiteName());
						}

						if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setAssignedToWhom(objUser.getUserName());

						}

						if (lstTicketDetail.get(i).getCreationDate() != null) {
							lstTicketDetail.get(i).setCreationDate(lstTicketDetail.get(i).getCreationDate());
							lstTicketDetail.get(i)
									.setCreatedDateText(sdf.format(lstTicketDetail.get(i).getCreationDate()));

							// System.out.println( " Mohanraj ::: " +
							// sdf.format(lstTicketDetail.get(i).getCreationDate()));
						}
						if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setCreatedByName(objUser.getUserName());

						}

						if (lstTicketDetail.get(i).getScheduledOn() != null) {
							lstTicketDetail.get(i).setScheduledOn(lstTicketDetail.get(i).getScheduledOn());
							lstTicketDetail.get(i)
									.setScheduledDateText(sdfdate.format(lstTicketDetail.get(i).getScheduledOn()));

						}

					}

					User objUser = userService.getUserById(userid);
					// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
					List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

					lstRoleActivity = roleActivityService.listRoleActivities().stream()
							.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

					objAccessListBean.setUserID(userid);
					objAccessListBean.setUserName(objUser.getUserName());
					objAccessListBean.setFilterFlag(1);
					objAccessListBean.setMySiteID(id);

					if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
						objAccessListBean.setCustomerListView("visible");
					}
					if (objUser.getRoleID() == 6) {
						objAccessListBean.setMonitoringView("visible");
					}

					for (int l = 0; l < lstRoleActivity.size(); l++) {
						if (lstRoleActivity.get(l).getActivityID() == 1
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setDashboard("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 2
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setOverview("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 3
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setSystemMonitoring("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 4
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setVisualization("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 5
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setAnalytics("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 6
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setPortfolioManagement("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 7
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setTicketing("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 8
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setForcasting("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 9
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setConfiguration("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 10
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setAnalysis("visible");
						}
					}

				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

				objResponseEntity.put("access", objAccessListBean);
//				objResponseEntity.put("siteList", getNewTicketSiteList("Site", id));
				objResponseEntity.put("siteList", getDropdownList("CustomerSites", id));
				objResponseEntity.put("ticketfilter", new TicketFilter());
				objResponseEntity.put("ticketdetaillist", lstTicketDetail);

				objResponseEntity.put("userList", getDropdownList("User", id));
				objResponseEntity.put("ticketcreation", new TicketDetail());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : CustomerfilterTicketDetails
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   :  This function returns tickets details for customer with filter.
	 * 
	 * Input Params  : id,lstfilter,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customerfilterticketdetails{id}", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> CustomerfilterTicketDetails(@PathVariable("id") int id,
			@RequestBody List<TicketFilter> lstfilter, HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int userid = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				TicketFilter ticketfilter = lstfilter.get(0);

				Integer siteid = ticketfilter.getSiteID();
				String state = ticketfilter.getState();

				String category = ticketfilter.getTicketCategory();

				String priority = ticketfilter.getPriority();

				String fromdate = ticketfilter.getFromDate();

				String todate = ticketfilter.getToDate();

				System.out.println(siteid);
				System.out.println(state);
				System.out.println(category);
				System.out.println(priority);
				System.out.println(fromdate);
				System.out.println(todate);

				int varSiteID = 0;
				String varFromDate = "";
				String varToDate = "";
				String varTicketCategory = "";
				String varTicketType = "";
				String varPriority = "";
				String varState = "";

				if (ticketfilter != null) {
					if (ticketfilter.getSiteID() != null) {
						varSiteID = ticketfilter.getSiteID();
					}
					if (ticketfilter.getFromDate() != null) {
						varFromDate = ticketfilter.getFromDate();
					}
					if (ticketfilter.getToDate() != null) {
						varToDate = ticketfilter.getToDate();
					}
					if (ticketfilter.getTicketCategory() != null) {
						varTicketCategory = ticketfilter.getTicketCategory();
					}
					if (ticketfilter.getTicketType() != null) {
						varTicketType = ticketfilter.getTicketType();
					}
					if (ticketfilter.getPriority() != null) {
						varPriority = ticketfilter.getPriority();
					}
					if (ticketfilter.getState() != null) {
						varState = ticketfilter.getState();
					}

				}

				List<TicketDetail> lstTicketDetail = ticketdetailService.listCustomerTicketDetailsForDisplay(userid,
						String.valueOf(siteid), fromdate, todate, category, state, priority, timezonevalue);

				System.out.println(lstTicketDetail.size());

				List<Site> lstSite = siteService.getSiteListByUserId(userid);
				List<User> lstUser = userService.listUsers();
				List<Equipment> lstEquipment = equipmentService.listEquipmentsByUserId(userid);

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				AccessListBean objAccessListBean = new AccessListBean();

				try {

					for (int i = 0; i < lstTicketDetail.size(); i++) {
						Integer intSiteID = lstTicketDetail.get(i).getSiteID();
						Integer intAssignedID = lstTicketDetail.get(i).getAssignedTo();
						Integer intCreatedBy = lstTicketDetail.get(i).getCreatedBy();

						if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
								.size() >= 1) {
							Site objSite = new Site();
							objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setSiteName(objSite.getSiteName());
						}

						if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setAssignedToWhom(objUser.getUserName());

						}

						if (lstTicketDetail.get(i).getCreationDate() != null) {
							lstTicketDetail.get(i).setCreationDate(lstTicketDetail.get(i).getCreationDate());
							lstTicketDetail.get(i)
									.setCreatedDateText(sdf.format(lstTicketDetail.get(i).getCreationDate()));

							// System.out.println( " Mohanraj ::: " +
							// sdf.format(lstTicketDetail.get(i).getCreationDate()));
						}
						if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setCreatedByName(objUser.getUserName());

						}

						if (lstTicketDetail.get(i).getScheduledOn() != null) {
							lstTicketDetail.get(i).setScheduledOn(lstTicketDetail.get(i).getScheduledOn());
							lstTicketDetail.get(i)
									.setScheduledDateText(sdfdate.format(lstTicketDetail.get(i).getScheduledOn()));

						}

					}

					User objUser = userService.getUserById(userid);
					// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
					List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

					lstRoleActivity = roleActivityService.listRoleActivities().stream()
							.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

					objAccessListBean.setUserID(id);
					objAccessListBean.setUserName(objUser.getUserName());

					if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
						objAccessListBean.setCustomerListView("visible");
					}
					if (objUser.getRoleID() == 6) {
						objAccessListBean.setMonitoringView("visible");
					}

					for (int l = 0; l < lstRoleActivity.size(); l++) {
						if (lstRoleActivity.get(l).getActivityID() == 1
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setDashboard("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 2
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setOverview("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 3
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setSystemMonitoring("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 4
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setVisualization("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 5
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setAnalytics("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 6
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setPortfolioManagement("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 7
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setTicketing("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 8
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setForcasting("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 9
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setConfiguration("visible");
						} else if (lstRoleActivity.get(l).getActivityID() == 10
								&& lstRoleActivity.get(l).getActiveFlag() == 1) {
							objAccessListBean.setAnalysis("visible");
						}
					}

				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

				TicketFilter objTicketFilter = new TicketFilter();

				objTicketFilter.setSiteID(ticketfilter.getSiteID());
				objTicketFilter.setFromDate(ticketfilter.getFromDate());
				objTicketFilter.setToDate(ticketfilter.getToDate());
				objTicketFilter.setTicketCategory(ticketfilter.getTicketCategory());
				objTicketFilter.setState(ticketfilter.getState());
				objTicketFilter.setTicketType(ticketfilter.getTicketType());
				objTicketFilter.setPriority(ticketfilter.getPriority());

				objResponseEntity.put("access", objAccessListBean);
				objResponseEntity.put("siteList", getDropdownList("CustomerSites", id));
				objResponseEntity.put("ticketfilter", objTicketFilter);
				objResponseEntity.put("ticketfilterdetaillist", lstTicketDetail);

				objResponseEntity.put("userList", getDropdownList("User", id));
				objResponseEntity.put("ticketcreation", new TicketDetail());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : listReportTickets
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function load the download reports page.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/downloadreports", method = RequestMethod.GET)
	public String listReportTickets(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String timezonevalue = "0";
		if (!timezonevalue.equals("") && timezonevalue != null) {
			timezonevalue = session.getAttribute("timezonevalue").toString();
		}
		loginUserDetails.setTimezoneoffsetmin(timezonevalue);

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());
			objAccessListBean.setMySiteFilter(false);
			objAccessListBean.setMySiteID(0);

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("ticketfilter", new TicketFilter());
		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("ticketdetail", new TicketDetail());

		model.addAttribute("siteList", getDropdownList("Site", id));
		model.addAttribute("userList", getDropdownList("User", id));

		model.addAttribute("equipmentList", getDropdownList("Equipment", id));
		model.addAttribute("ticketcreation", new TicketDetail());

		return "report";
	}

	/********************************************************************************************
	 * 
	 * Function Name : ReportTicketFilterPageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns ticket reports with filter.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/reportticketfilterdetail", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> ReportTicketFilterPageLoad(@RequestBody List<TicketFilter> lstfilter,
			HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				TicketFilter ticketfilter = lstfilter.get(0);

				Integer siteid = ticketfilter.getSiteID();
				String state = ticketfilter.getState();

				String category = ticketfilter.getTicketCategory();

				String priority = ticketfilter.getPriority();

				String fromdate = ticketfilter.getFromDate();

				String todate = ticketfilter.getToDate();

				int varSiteID = 0;
				String varFromDate = "";
				String varToDate = "";
				String varTicketCategory = "";
				String varTicketType = "";
				String varPriority = "";
				String varState = "";

				if (ticketfilter != null) {
					if (ticketfilter.getSiteID() != null) {
						varSiteID = ticketfilter.getSiteID();
					}
					if (ticketfilter.getFromDate() != null) {
						varFromDate = ticketfilter.getFromDate();
					}
					if (ticketfilter.getToDate() != null) {
						varToDate = ticketfilter.getToDate();
					}
					if (ticketfilter.getTicketCategory() != null) {
						varTicketCategory = ticketfilter.getTicketCategory();
					}
					if (ticketfilter.getTicketType() != null) {
						varTicketType = ticketfilter.getTicketType();
					}
					if (ticketfilter.getPriority() != null) {
						varPriority = ticketfilter.getPriority();
					}
					if (ticketfilter.getState() != null) {
						varState = ticketfilter.getState();
					}

				}

				List<TicketDetail> lstTicketDetail = ticketdetailService.listReportTicketDetailsForDisplay(id,
						String.valueOf(siteid), fromdate, todate, category, state, priority, timezonevalue);

				System.out.println(lstTicketDetail.size());

				List<Site> lstSite = siteService.getSiteListByUserId(id);
				List<User> lstUser = userService.listUsers();
				List<Equipment> lstEquipment = equipmentService.listEquipmentsByUserId(id);

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				try {

					for (int i = 0; i < lstTicketDetail.size(); i++) {
						Integer intSiteID = lstTicketDetail.get(i).getSiteID();
						Integer intAssignedID = lstTicketDetail.get(i).getAssignedTo();
						Integer intCreatedBy = lstTicketDetail.get(i).getCreatedBy();
						if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
								.size() >= 1) {
							Site objSite = new Site();
							objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setSiteName(objSite.getSiteName());
						}

						if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setAssignedToWhom(objUser.getUserName());

						}
						if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setCreatedByName(objUser.getUserName());

						}

						if (lstTicketDetail.get(i).getCreationDate() != null) {
							lstTicketDetail.get(i).setCreationDate(lstTicketDetail.get(i).getCreationDate());
							lstTicketDetail.get(i)
									.setCreatedDateText(sdf.format(lstTicketDetail.get(i).getCreationDate()));

							// System.out.println( " Mohanraj ::: " +
							// sdf.format(lstTicketDetail.get(i).getCreationDate()));
						}

						if (lstTicketDetail.get(i).getScheduledOn() != null) {
							lstTicketDetail.get(i).setScheduledOn(lstTicketDetail.get(i).getScheduledOn());
							lstTicketDetail.get(i)
									.setScheduledDateText(sdfdate.format(lstTicketDetail.get(i).getScheduledOn()));

						}

						lstTicketDetail.get(i).setTimeZone(timezonevalue);

					}

				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

				TicketFilter objTicketFilter = new TicketFilter();

				objTicketFilter.setSiteID(ticketfilter.getSiteID());
				objTicketFilter.setFromDate(ticketfilter.getFromDate());
				objTicketFilter.setToDate(ticketfilter.getToDate());
				objTicketFilter.setTicketCategory(ticketfilter.getTicketCategory());
				objTicketFilter.setState(ticketfilter.getState());
				objTicketFilter.setTicketType(ticketfilter.getTicketType());
				objTicketFilter.setPriority(ticketfilter.getPriority());

				objResponseEntity.put("siteList", getDropdownList("Site", id));
				objResponseEntity.put("ticketfilter", objTicketFilter);
				objResponseEntity.put("ticketfilterdetaillist", lstTicketDetail);
				objResponseEntity.put("siteList", getDropdownList("Site", id));
				objResponseEntity.put("userList", getDropdownList("User", id));
				objResponseEntity.put("ticketcreation", new TicketDetail());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : ReportTicketPageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns report ticket details.
	 * 
	 * Input Params  : session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/reportticketdetail", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> ReportTicketPageLoad(HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<TicketDetail> lstTicketDetail = ticketdetailService.getReportTicketDetailListByUserId(id,
						timezonevalue);
				List<Site> lstSite = siteService.getSiteListByUserId(id);

				List<Equipment> lstEquipment = equipmentService.listEquipmentsByUserId(id);
				List<User> lstUser = userService.listUsers();

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				TicketFilter objTicketFilter = new TicketFilter();

				try {

					for (int i = 0; i < lstTicketDetail.size(); i++) {
						Integer intSiteID = lstTicketDetail.get(i).getSiteID();
						Integer intAssignedID = lstTicketDetail.get(i).getAssignedTo();
						Integer intequipmentID = lstTicketDetail.get(i).getEquipmentID();
						Integer intCreatedBy = lstTicketDetail.get(i).getCreatedBy();

						if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
								.size() >= 1) {
							Site objSite = new Site();
							objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setSiteName(objSite.getSiteName());
						}
						if (lstEquipment.stream().filter(p -> p.getEquipmentId().equals(intequipmentID))
								.collect(Collectors.toList()).size() >= 1) {
							Equipment objEquipment = new Equipment();
							objEquipment = lstEquipment.stream().filter(p -> p.getEquipmentId().equals(intequipmentID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setEquipmentName(objEquipment.getCustomerNaming().toString());
						}

						if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setAssignedToWhom(objUser.getUserName());

						}
						if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
									.collect(Collectors.toList()).get(0);

							lstTicketDetail.get(i).setCreatedByName(objUser.getUserName());

						}

						if (lstTicketDetail.get(i).getCreationDate() != null) {
							lstTicketDetail.get(i).setCreationDate(lstTicketDetail.get(i).getCreationDate());
							lstTicketDetail.get(i)
									.setCreatedDateText(sdf.format(lstTicketDetail.get(i).getCreationDate()));

							// System.out.println( " Mohanraj ::: " +
							// sdf.format(lstTicketDetail.get(i).getCreationDate()));
						}

						if (lstTicketDetail.get(i).getScheduledOn() != null) {
							lstTicketDetail.get(i).setScheduledOn(lstTicketDetail.get(i).getScheduledOn());
							lstTicketDetail.get(i)
									.setScheduledDateText(sdfdate.format(lstTicketDetail.get(i).getScheduledOn()));

						}

						lstTicketDetail.get(i).setTimeZone(timezonevalue);

					}

				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

				objResponseEntity.put("siteList", getDropdownList("Site", id));
				objResponseEntity.put("ticketfilter", objTicketFilter);
				objResponseEntity.put("ticketdetaillist", lstTicketDetail);
				objResponseEntity.put("siteList", getDropdownList("Site", id));
				objResponseEntity.put("userList", getDropdownList("User", id));

				objResponseEntity.put("ticketcreation", new TicketDetail());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

}
