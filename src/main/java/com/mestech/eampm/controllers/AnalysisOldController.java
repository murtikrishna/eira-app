package com.mestech.eampm.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.AnalysisListBean;
import com.mestech.eampm.bean.EnergyPerformanceBean;
import com.mestech.eampm.bean.EquipmentListBean;
import com.mestech.eampm.bean.EquipmentWithParameterListBean;
import com.mestech.eampm.bean.EquipmentWithoutParameterListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.bean.SiteSummaryBean;
import com.mestech.eampm.bean.StandardParameterBean;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.DataTransaction;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteSummary;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.DataTransactionService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteSummaryService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.bean.GraphDataBean;

@Controller
public class AnalysisOldController {
	
	 @Autowired
	    private SiteService siteService;

	 @Autowired
	    private DataTransactionService dataTransactionService;

	 @Autowired
	    private EquipmentService equipmentService;
	 
	 
	 @Autowired
	    private CustomerService customerService;
	 
	 
	 @Autowired
	    private UserService userService;
	 

	 @Autowired
	    private UserRoleService userRoleService;
	
	 @Autowired
	 private RoleActivityService roleActivityService;
	 
	 
	 @Autowired
	    private SiteSummaryService siteSummaryService;
	
	 
	 
	 public String SitewsieDailyEnergy(Integer id ,String FromDate,String ToDate, String timezonevalue)
	 {
			SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd");
			String ChartJsonData1 ="";
			String ChartJsonData1Parameter ="";
		    String ChartJsonData1GraphValue ="";
		    String SiteName = "";
		    
		    
		    List<SiteSummary> lstSiteSummary = siteSummaryService.getSiteSummaryListBySiteIdWithDate(String.valueOf(id),FromDate,ToDate);
		    Site objSite =  siteService.getSiteById(id);
		    
		    
		    if(objSite != null)
		    {
		    	SiteName = objSite.getSiteName() + " - " + objSite.getCustomerNaming();
		    }
		    
		     for(int a=0;a<lstSiteSummary.size();a++)
		     {
		    	 
		    	 String varChartDate = sdfchart.format(lstSiteSummary.get(a).getTimestamp()).toString();
		    	 
		    	 Calendar cal = Calendar. getInstance();
		         cal.setTime(lstSiteSummary.get(a).getTimestamp());
		         cal. add(Calendar.HOUR, 5);
		         cal. add(Calendar.MINUTE, 30);
		         
		         
		    	 //long millis = lstSiteSummary.get(a).getTimestamp().getTime();
		         long millis = cal.getTime().getTime();
		    	 varChartDate =String.valueOf(millis);
		    	 
		    	// System.out.println(millis+330);
		    	 
		    	 Double dbTodayEnergy = lstSiteSummary.get(a).getTodayEnergy();
		    	 String varChartValue ="";
		  
		    	 
		    	 varChartValue = String.format("%.2f", dbTodayEnergy * 1000 * 1000);
		    	 
		    	
		    	 if(a==0)
		    	 {
		    		 ChartJsonData1 = "[" + varChartDate + "," + varChartValue + "]";
		    	 }
		    	 else
		    	 {
		    		 ChartJsonData1 =  ChartJsonData1 + ",[" + varChartDate + "," + varChartValue + "]";
		    	    	
		    	 }
		        		
		     }
		     

			 ChartJsonData1 = "[" + ChartJsonData1 + "]";
		 	
			 //return ChartJsonData1;
			 return SiteName + "||" + ChartJsonData1;
	 }
	 
	 public String SiteGroupDailyEnergy(String siteidList ,String FromDate,String ToDate, String timezonevalue)
	 {
			SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd");
			String ChartJsonData1 ="";
			String ChartJsonData1Parameter ="";
		    String ChartJsonData1GraphValue ="";
		    String SiteName = "All Selected Sites";
		    
		    
		    List<SiteSummary> lstSiteSummary = siteSummaryService.getSiteSummaryListBySiteIdsGroupWithDate(siteidList,FromDate,ToDate);
		    //Site objSite =  siteService.getSiteById(id);
		    
		    
		   /* if(objSite != null)
		    {
		    	SiteName = objSite.getSiteName() + " - " + objSite.getCustomerNaming();
		    }
		    */
		    
		    
		    
		    if(lstSiteSummary.size()>0) {
		    	Integer a=0;
		    	SiteSummaryBean objSiteSummaryBean =new SiteSummaryBean();
				for(Object object:lstSiteSummary)
				{
					Object []obj=(Object[]) object;
					objSiteSummaryBean.setSummaryID((String)obj[0]);
					objSiteSummaryBean.setSiteID(obj[1]!=null?(String)obj[1]:null);
					objSiteSummaryBean.setTimestamp((Date) obj[2]);
					objSiteSummaryBean.setTodayEnergy((Double) obj[3]);
					
					
					if(objSiteSummaryBean.getTimestamp() !=null)
			    	 {
			    		 
						String varChartDate = sdfchart.format(objSiteSummaryBean.getTimestamp()).toString();
					    	
				    	try {
							
							
							 Calendar cal = Calendar. getInstance();
					         cal.setTime(objSiteSummaryBean.getTimestamp());
					         cal. add(Calendar.HOUR, 5);
					         cal. add(Calendar.MINUTE, 30);
					         
					         long millis = cal.getTime().getTime();
					    	 varChartDate =String.valueOf(millis);
					    	 
					    	 
					    	 Double dbTodayEnergy = 0.0;
					    	 if(objSiteSummaryBean.getTodayEnergy() != null)
					    	 {
					    		 dbTodayEnergy = objSiteSummaryBean.getTodayEnergy();
					    	 }
					    	 
					    	 String varChartValue ="";
					  
					    	 
					    	 varChartValue = String.format("%.2f", dbTodayEnergy * 1000 * 1000);
					    	 
					    	
					    	 if(a==0)
					    	 {
					    		 ChartJsonData1 = "[" + varChartDate + "," + varChartValue + "]";
					    	 }
					    	 else
					    	 {
					    		 ChartJsonData1 =  ChartJsonData1 + ",[" + varChartDate + "," + varChartValue + "]";
					    	 }
					    	 
					    	 a=a+1;
					    	 
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				    	
			    		 
			    	 }
			    	
					
					
				}
				
			}

		    
		    
		    
		    
		    
		   

			 ChartJsonData1 = "[" + ChartJsonData1 + "]";
		 	
			 //return ChartJsonData1;
			 return SiteName + "||" + ChartJsonData1;
	 }
	  
	 
	 public String EquipmentwiseEnergyPerformance(Integer id,String FromDate,String ToDate, String timezonevalue)
	 {
		 SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
		 
		 String ChartJsonData1 ="";
		 String ChartJsonData1Parameter ="";
		 String ChartJsonData1GraphValue ="";
		 String SiteName = "";
		 String EquipmentName = "";
		 String isEToday = "0";  
		 
		 
		 List<DataTransaction> lstDataTransaction = dataTransactionService.getDataTransactionListByEquipmentIdWithDate(id,FromDate, ToDate,  timezonevalue);
		 Equipment objEquipment = equipmentService.getEquipmentById(id);
		
		    
		 
		 if(objEquipment != null)
		    {
			 EquipmentName = objEquipment.getCustomerNaming() ;
		    	
		    	if(objEquipment.getSiteID()!=null)
				 { 
					 Site objSite =  siteService.getSiteById(objEquipment.getSiteID());
			    	 if(objSite != null)
					    {
					    	SiteName = "(" + objSite.getSiteName() + " - " + objSite.getCustomerNaming() + ")";
					    	
					    	if(objSite.getTodayEnergyFlag() != null)
					    	{
					    		if(objSite.getTodayEnergyFlag().equals("1"))
					    		{
					    			
					    			
					    			
					    			
					    			
					    			isEToday = "1";
					    		}
					    	}
					    	
					    }
				 }
		  	 
		    }
		    
		   
		    
			 
		     for(int a=0;a<lstDataTransaction.size();a++)
		     {
		    	 String varChartDate = sdfchart.format(lstDataTransaction.get(a).getTimestamp()).toString();
		    	 
		         Calendar cal = Calendar. getInstance();
		         cal.setTime(lstDataTransaction.get(a).getTimestamp());
		         cal. add(Calendar.HOUR, 5);
		         cal. add(Calendar.MINUTE, 30);
		         
		         
		         
		    	 //long millis = lstDataTransaction.get(a).getTimestamp().getTime();
		         long millis = cal.getTime().getTime();
		    	 varChartDate =String.valueOf(millis);
		    	 
		    	 Double dbTotalEnergy = 0.0;
		    	 Double dbTotalEnergyPrevious =0.0;
		    	 
		    	 if(isEToday.equals("1"))
		    	 {
		    		
		    		 dbTotalEnergy = lstDataTransaction.get(a).getTodayEnergy();
			    	 dbTotalEnergyPrevious = lstDataTransaction.get(a).getTodayEnergy();
		    	 }
		    	 else
		    	 {
		    		 dbTotalEnergy = lstDataTransaction.get(a).getTotalEnergy();
			    	 dbTotalEnergyPrevious = lstDataTransaction.get(a).getTotalEnergy();
		    	 }
		    	 
		    	 Double dbTotalEnergyPresent = 0.0;
		    	 if(a!=0)
		    	 {
		    		 if(sdfdate.format(lstDataTransaction.get(a).getTimestamp()).toString().equals(sdfdate.format(lstDataTransaction.get(a-1).getTimestamp()).toString()))
		    		 {
		    			 if(a==1)
		        		 {
		    				 if(isEToday.equals("1"))
		    		    	 {
		    					 dbTotalEnergyPrevious = lstDataTransaction.get(a-1).getTodayEnergy();
		    		    	 }
		    				 else
		    				 {
		    					 dbTotalEnergyPrevious = lstDataTransaction.get(a-1).getTotalEnergy();
		    				 }
		        			 
		            		 dbTotalEnergyPresent = dbTotalEnergy - dbTotalEnergyPrevious;
		        		 }
		    			 else if(lstDataTransaction.get(a-1).getTotalEnergy() >= lstDataTransaction.get(a-2).getTotalEnergy())
		        		 {
		    				 if(isEToday.equals("1"))
		    		    	 {
		    					 dbTotalEnergyPrevious = lstDataTransaction.get(a-1).getTodayEnergy();
		    		    	 }
		    				 else
		    				 {
		    					 dbTotalEnergyPrevious = lstDataTransaction.get(a-1).getTotalEnergy();
		    				 }
		        			 
		            		 dbTotalEnergyPresent = dbTotalEnergy - dbTotalEnergyPrevious;
		        		 }
		        		 
		    		 }
		    		 
		    	 }
		    	 
		    	 
		    	 String varChartValue = String.format("%.2f", dbTotalEnergyPresent * 1000 );
		    	
		    	 
		    	 if(a==0)
		    	 {
		    		 ChartJsonData1 = "[" + varChartDate + "," + varChartValue + "]";
		    	 }
		    	 else
		    	 {
		    		 ChartJsonData1 =  ChartJsonData1 + ",[" + varChartDate + "," + varChartValue + "]";
		    		 	
		    	 }
		        		
		     }
		     


			 ChartJsonData1 = "[" + ChartJsonData1 + "]";
		   
		     
		     
		 	
			 //return ChartJsonData1;
			 return  EquipmentName + " " + SiteName + "||" + ChartJsonData1;
	 }
	 
	 

	 public String EquipmentGroupEnergyPerformance(String ids,String FromDate,String ToDate, String timezonevalue)
	 {
		
		 SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
		 
		 String ChartJsonData1 ="";
		 String ChartJsonData1Parameter ="";
		 String ChartJsonData1GraphValue ="";
		 String SiteName = "";
		 String EquipmentName = "";
		 
		 String EquipmentNameList = "";
		   
		 
		 List<EnergyPerformanceBean> lstDataTransaction = dataTransactionService.getEnergyDataByEquipmentIdsWithDate(ids,FromDate,ToDate,  timezonevalue);
		 //List<GraphDataBean> lstDataTransaction = dataTransactionService.getDataTransactionListByEquipmentIdsWithDate(ids,FromDate,ToDate);
		 List<Equipment> lstEquipment = equipmentService.listEquipmentsByIds(ids);
		
		 String YesterdayEnergy = dataTransactionService.getYesterdayEnergyByEquipmentIdsWithDate(ids, FromDate, ToDate,  timezonevalue);
		 Double dblYesterdayEnergyValue = Double.valueOf(YesterdayEnergy) * 1000.00;
		 
		// System.out.println("YesterdayEnergy"+ YesterdayEnergy);
		 
		 String TodayEnergyStatus = dataTransactionService.getTodayFlagStatus(ids,  timezonevalue);
		 
		 //System.out.println("TodayEnergyStatus"+ TodayEnergyStatus);
		
		    
		 List<GraphDataBean> lstGraphDataBean = new ArrayList<GraphDataBean>();
		 
		 if(lstDataTransaction.size()>0) {
		    	Integer iCnt=0;
		    	
				for(Object object:lstDataTransaction)
				{
					Object []obj=(Object[]) object;
					GraphDataBean objGraphDataBean =new GraphDataBean();
					objGraphDataBean.setTimestamp((Date) obj[0]);

					
					if(TodayEnergyStatus.equals("1"))
					{
						if(obj[2]==null)
						{
							objGraphDataBean.setValue(0.0);
						}
						else
						{
							Double dblTodayValue = ((java.math.BigDecimal)obj[2]).doubleValue() * 1000.00;
							
							
							if(iCnt.equals(0))
							{
								objGraphDataBean.setValue(0.0);
							}
							else if(iCnt <=3 && dblTodayValue > 10.00)
							{
								objGraphDataBean.setValue(0.0);
							}
							else
							{
								objGraphDataBean.setValue(dblTodayValue);
							}
							
						}
					}
					else
					{
						if(obj[1]==null)
						{
							objGraphDataBean.setValue(0.0);
						}
						else
						{
							Double dblTotalValue = ((java.math.BigDecimal)obj[1]).doubleValue() * 1000.00;
							Double dblTodayValue = dblTotalValue - dblYesterdayEnergyValue;
							
							
							objGraphDataBean.setValue(dblTodayValue);
							
							/*if(iCnt.equals(0))
							{
								objGraphDataBean.setValue(0.0);
							}
							else if(iCnt <=3 && dblTodayValue > 10.00)
							{
								objGraphDataBean.setValue(0.0);
							}
							else
							{
								objGraphDataBean.setValue(dblTodayValue);
							}*/
							
						}
					}
					
					
					
					
					if(objGraphDataBean.getTimestamp() !=null)
			    	 {
			    		 	
				    	try {
							
				    		lstGraphDataBean.add(objGraphDataBean);
					    	 
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				    	
			    		 
			    	 }
			    	
					iCnt = iCnt +1;
					
				}
				
			}

		 
		   
		 
		    
			 
		     for(int a=0;a<lstGraphDataBean.size();a++)
		     {
		    	 String varChartDate = sdfchart.format(lstGraphDataBean.get(a).getTimestamp()).toString();
		    	 
		         Calendar cal = Calendar. getInstance();
		         cal.setTime(lstGraphDataBean.get(a).getTimestamp());
		         cal. add(Calendar.HOUR, 5);
		         cal. add(Calendar.MINUTE, 30);
		         
		         
		         
		    	 //long millis = lstDataTransaction.get(a).getTimestamp().getTime();
		         long millis = cal.getTime().getTime();
		    	 varChartDate =String.valueOf(millis);
		    	 
		    	 Double dbTotalEnergy = lstGraphDataBean.get(a).getValue();
		    	 Double dbTotalEnergyPrevious = lstGraphDataBean.get(a).getValue();
		    	 
		    	 Double dbTotalEnergyPresent = 0.0;
		    	 if(a!=0)
		    	 {
		    		 if(sdfdate.format(lstGraphDataBean.get(a).getTimestamp()).toString().equals(sdfdate.format(lstGraphDataBean.get(a-1).getTimestamp()).toString()))
		    		 {
		    			 if(a==1)
		        		 {
		        			 dbTotalEnergyPrevious = lstGraphDataBean.get(a-1).getValue();
		            		 dbTotalEnergyPresent = dbTotalEnergy - dbTotalEnergyPrevious;
		        		 }
		    			 else if(lstGraphDataBean.get(a-1).getValue() >= lstGraphDataBean.get(a-2).getValue())
		        		 {
		        			 dbTotalEnergyPrevious = lstGraphDataBean.get(a-1).getValue();
		            		 dbTotalEnergyPresent = dbTotalEnergy - dbTotalEnergyPrevious;
		        		 }
		        		 
		    		 }
		    		 
		    	 }
		    	 
		    	 
		    	 String varChartValue = String.format("%.2f", dbTotalEnergyPresent);
		    	
		    	 
		    	 if(a==0)
		    	 {
		    		 ChartJsonData1 = "[" + varChartDate + "," + varChartValue + "]";
		    	 }
		    	 else
		    	 {
		    		 ChartJsonData1 =  ChartJsonData1 + ",[" + varChartDate + "," + varChartValue + "]";
		    		 	
		    	 }
		        		
		     }
		     


			 ChartJsonData1 = "[" + ChartJsonData1 + "]";
		   
		     System.out.println(ChartJsonData1);
		     
			 EquipmentNameList = "For All Selected Equipments";
			 //return ChartJsonData1;
			 return  EquipmentNameList + "||" + ChartJsonData1;
	 }
	 
	 
	 
	 public String EquipmentwiseParameterComparison(String ids,String Parameter1,String Parameter2,String FromDate,String ToDate, String timezonevalue)
	 {
		 SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
		
		 Boolean blnIsRecordExist =false;
		 
		 String ChartJsonData ="";
		 String ChartJsonDataParameter ="";
		 String ChartJsonDataGraphValue ="";
		 String SiteName = "";
		 String EquipmentName = "";
		 		 
		
					 
		 String Parameter1AllChartData = "";
		 String Parameter2AllChartData = "";
		
		 List<Equipment> lstEquipment = equipmentService.listEquipmentsByIds(ids);
		
		 
		 if(Parameter1.equals("") == false)
		 {			 		
			 
			 for(int z=0; z< lstEquipment.size();z++)
			 {
				 
				 
				 String EquipmentID = String.valueOf(lstEquipment.get(z).getEquipmentId());
				 String EquipmentRefName = lstEquipment.get(z).getCustomerNaming();
				 
				 String Parameter1ChartData = "";
				 
				 List<GraphDataBean> lstDataTransaction1 = dataTransactionService.getParameterValuesByEquipmentIdsWithDate(ids,Parameter1,FromDate,ToDate, timezonevalue);
				 
				 
				 if(lstDataTransaction1.size()>0) {
				    	Integer a=0;
				    	
						for(Object object:lstDataTransaction1)
						{
							Object []obj=(Object[]) object;
							GraphDataBean objGraphDataBean =new GraphDataBean();
							objGraphDataBean.setTimestamp((Date) obj[0]);
							
							if(obj[1]==null)
							{
								objGraphDataBean.setValue(0.0);
							}
							else
							{
								objGraphDataBean.setValue((Double) obj[1]);
							}
							
							
							
							
							if(objGraphDataBean.getTimestamp() !=null)
					    	 {
					    		 	
						    	try {
									
						    		String varChartDate = sdfchart.format(objGraphDataBean.getTimestamp()).toString();
							    	 
							         Calendar cal = Calendar. getInstance();
							         cal.setTime(objGraphDataBean.getTimestamp());
							         cal. add(Calendar.HOUR, 5);
							         cal. add(Calendar.MINUTE, 30);
							         
							         
							         
							    	 //long millis = lstDataTransaction.get(a).getTimestamp().getTime();
							         long millis = cal.getTime().getTime();
							    	 varChartDate =String.valueOf(millis);
							    	 						    	 
							    	 Double dbValue = 0.0;
							    	 dbValue =  GetParameterValueInUnit( Parameter1, objGraphDataBean.getValue());
							    	 
							    	 String varChartValue = String.format("%.2f", dbValue);
							    	
							    	 
							    	 if(a==0)
							    	 {
							    		 blnIsRecordExist =true;
							    		 Parameter1ChartData = "[" + varChartDate + "," + varChartValue + "]";
							    	 }
							    	 else
							    	 {
							    		 blnIsRecordExist =true;
							    		 Parameter1ChartData =  Parameter1ChartData + ",[" + varChartDate + "," + varChartValue + "]";
							    		 	
							    	 }
							      
							    	 
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
						    	
					    		 
					    	 }
					    	
							
							a = a + 1;
						}
						
					}

				 
				 Parameter1ChartData = "[" + Parameter1ChartData + "]";
				 
				 
				 if(z==0)
		    	 {
					 Parameter1AllChartData = EquipmentRefName + " - " + Parameter1 + "||" + Parameter1ChartData;
		    	 }
		    	 else
		    	 {
		    		 Parameter1AllChartData =  Parameter1AllChartData + "&&" + EquipmentRefName + " - " + Parameter1 + "||" + Parameter1ChartData;
		    		 	
		    	 }
				 
			 }
			 
			 
			
				 
		 }
		 


		 if(Parameter2.equals("") == false)
		 {			 		 
			 for(int z=0; z< lstEquipment.size();z++)
			 {
				 
				 
				 String EquipmentID = String.valueOf(lstEquipment.get(z).getEquipmentId());
				 String EquipmentRefName = lstEquipment.get(z).getCustomerNaming();
				 
				 String Parameter2ChartData = "";
				 
				 List<GraphDataBean> lstDataTransaction2 = dataTransactionService.getParameterValuesByEquipmentIdsWithDate(ids,Parameter2,FromDate,ToDate, timezonevalue);
				 
				 
				 if(lstDataTransaction2.size()>0) {
				    	Integer a=0;
				    	
						for(Object object:lstDataTransaction2)
						{
							Object []obj=(Object[]) object;
							GraphDataBean objGraphDataBean =new GraphDataBean();
							objGraphDataBean.setTimestamp((Date) obj[0]);

							if(obj[1]==null)
							{
								objGraphDataBean.setValue(0.0);
							}
							else
							{
								objGraphDataBean.setValue((Double) obj[1]);
							}
							
							
							if(objGraphDataBean.getTimestamp() !=null)
					    	 {
					    		 	
						    	try {
									
						    		String varChartDate = sdfchart.format(objGraphDataBean.getTimestamp()).toString();
							    	 
							         Calendar cal = Calendar. getInstance();
							         cal.setTime(objGraphDataBean.getTimestamp());
							         cal. add(Calendar.HOUR, 5);
							         cal. add(Calendar.MINUTE, 30);
							         
							         
							         
							    	 //long millis = lstDataTransaction.get(a).getTimestamp().getTime();
							         long millis = cal.getTime().getTime();
							    	 varChartDate =String.valueOf(millis);
							    	 						    	 
							    	 Double dbValue = 0.0;
							    	 dbValue =  GetParameterValueInUnit( Parameter2, objGraphDataBean.getValue());
							    	 
							    	 String varChartValue = String.format("%.2f", dbValue );
							    	
							    	 
							    	 if(a==0)
							    	 {
							    		 blnIsRecordExist =true;
							    		 Parameter2ChartData = "[" + varChartDate + "," + varChartValue + "]";
							    	 }
							    	 else
							    	 {
							    		 blnIsRecordExist =true;
							    		 Parameter2ChartData =  Parameter2ChartData + ",[" + varChartDate + "," + varChartValue + "]";
							    		 	
							    	 }
							      
							    	 
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
						    	
					    		 
					    	 }
					    	
							
							a = a + 1;
						}
						
					}

				 
				 Parameter2ChartData = "[" + Parameter2ChartData + "]";
				 
				 if(z==0)
		    	 {
					 Parameter2AllChartData = EquipmentRefName + " - " + Parameter2 + "||" + Parameter2ChartData;
		    	 }
		    	 else
		    	 {
		    		 Parameter2AllChartData =  Parameter2AllChartData + "&&" + EquipmentRefName + " - " + Parameter2 + "||" + Parameter2ChartData;
		    		 	
		    	 }
				 
			 }
			 
			 
			
				 
		 }
		 
		 if(blnIsRecordExist ==false)
		 {
			 ChartJsonData ="";
		 }
		 else if(Parameter1AllChartData.equals("")==true && Parameter2AllChartData.equals("")==true)
		 {
			 ChartJsonData ="";
		 }
		 else if(Parameter1.equals("")==false)
		 {
			 
			 
			 ChartJsonData =  "0$$" +  GetParameterUOM(Parameter1) + "$$" + Parameter1AllChartData;
			 
			 if(Parameter2.equals("")==false)
			 {
				 ChartJsonData =  ChartJsonData + "##" + "1$$" +  GetParameterUOM(Parameter2) + "$$" + Parameter2AllChartData;
			 }
		 }
		 else if(Parameter2.equals("")==false)
		 {
			 ChartJsonData =  "1$$" +  GetParameterUOM(Parameter2) + "$$" + Parameter2AllChartData;
		 }
		 else
		 {
			 ChartJsonData ="";
		 }
		 
		 
		  
		 
		 System.out.println(ChartJsonData);
		 
		 
		 
		 
		 return ChartJsonData;
		 
	 }
	 
	 
	 
	 public String EquipmentwiseParameterComparisonAll(String ids,String Parameter1,String FromDate,String ToDate, String timezonevalue)
	 {
		 SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
		
		 Boolean blnIsRecordExist =false;
		 
		 String ChartJsonData ="";
		 String ChartJsonDataParameter ="";
		 String ChartJsonDataGraphValue ="";
		 String SiteName = "";
		 String EquipmentName = "";
		 		 
		
		 String Parameter = "";	 
		 String Parameter1AllChartData = "";
		 String ParameterAllChartData ="";
		 
		 
		 List<Equipment> lstEquipment = equipmentService.listEquipmentsByIds(ids);
		
		 
		 if(Parameter1.equals("") == false)
		 {			 		
			 String lstParameter1[] = null;
			 
			 if(Parameter1.contains(","))
			 {
				 
				 lstParameter1 = Parameter1.split(",");
			 }
			 
			 
			 
			 
			 for(int y=0; y< lstParameter1.length ;y++)
			 {
				 Parameter = lstParameter1[y];
			 
				 System.out.println( "y:" + y);
				 System.out.println("Parameter:" + Parameter);
			 for(int z=0; z< lstEquipment.size();z++)
			 {
				 
				 
				 String EquipmentID = String.valueOf(lstEquipment.get(z).getEquipmentId());
				 String EquipmentRefName = lstEquipment.get(z).getCustomerNaming();
				 
				 
				 System.out.println("z" + z);
				 System.out.println("EquipmentID" + EquipmentID);
				 System.out.println("EquipmentRefName" + EquipmentRefName);
				 
				 String Parameter1ChartData = "";
				 
				 List<GraphDataBean> lstDataTransaction1 = dataTransactionService.getParameterValuesByEquipmentIdsWithDate(EquipmentID,Parameter,FromDate,ToDate, timezonevalue);
				 
				 
				 if(lstDataTransaction1.size()>0) {
				    	Integer a=0;
				    	
						for(Object object:lstDataTransaction1)
						{
							Object []obj=(Object[]) object;
							GraphDataBean objGraphDataBean =new GraphDataBean();
							objGraphDataBean.setTimestamp((Date) obj[0]);
							
							if(obj[1]==null)
							{
								objGraphDataBean.setValue(0.0);
							}
							else
							{
								objGraphDataBean.setValue((Double) obj[1]);
							}
							
							
							
							
							if(objGraphDataBean.getTimestamp() !=null)
					    	 {
					    		 	
						    	try {
									
						    		String varChartDate = sdfchart.format(objGraphDataBean.getTimestamp()).toString();
							    	 
							         Calendar cal = Calendar. getInstance();
							         cal.setTime(objGraphDataBean.getTimestamp());
							         cal. add(Calendar.HOUR, 5);
							         cal. add(Calendar.MINUTE, 30);
							         
							         
							         
							    	 //long millis = lstDataTransaction.get(a).getTimestamp().getTime();
							         long millis = cal.getTime().getTime();
							    	 varChartDate =String.valueOf(millis);
							    	 						    	 
							    	 Double dbValue = 0.0;
							    	 dbValue =  GetParameterValueInUnit( Parameter, objGraphDataBean.getValue());
							    	 
							    	 String varChartValue = String.format("%.2f", dbValue);
							    	
							    	 
							    	 if(a==0)
							    	 {
							    		 blnIsRecordExist =true;
							    		 Parameter1ChartData = "[" + varChartDate + "," + varChartValue + "]";
							    	 }
							    	 else
							    	 {
							    		 blnIsRecordExist =true;
							    		 Parameter1ChartData =  Parameter1ChartData + ",[" + varChartDate + "," + varChartValue + "]";
							    		 	
							    	 }
							      
							    	 
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
						    	
					    		 
					    	 }
					    	
							
							a = a + 1;
						}
						
					}

				 
				 Parameter1ChartData = "[" + Parameter1ChartData + "]";
				 
				 
				 System.out.println("z:" + z);
				 if(z==0)
		    	 {
					 Parameter1AllChartData = EquipmentRefName + " - " + Parameter + "||" + Parameter1ChartData;
		    	 }
		    	 else
		    	 {
		    		 Parameter1AllChartData =  Parameter1AllChartData + "&&" + EquipmentRefName + " - " + Parameter + "||" + Parameter1ChartData;
		    		 	
		    	 }
				 System.out.println("Parameter1AllChartData:" + Parameter1AllChartData);
			 }
			 
			 
			 System.out.println( "y:" +y);
			 if(y==0)
	    	 {
				 ParameterAllChartData =Parameter1AllChartData;
	    	 }
	    	 else
	    	 {
	    		 ParameterAllChartData =  ParameterAllChartData + "&&" + Parameter1AllChartData;
	    		 	
	    	 }

			 System.out.println("ParameterAllChartData:" + ParameterAllChartData);
			 
			 
			 }
				 
		 }
		 


		
		 
		 if(blnIsRecordExist ==false)
		 {
			 ChartJsonData ="";
		 }
		 else if(ParameterAllChartData.equals("")==true )
		 {
			 ChartJsonData ="";
		 }
		 else if(Parameter1.equals("")==false)
		 {
			 
			 
			 ChartJsonData =  "0$$" +  "Parameters" + "$$" + ParameterAllChartData;
			 
			
		 }		
		 else
		 {
			 ChartJsonData ="";
		 }
		 
		 
		  
		 
		 System.out.println(ChartJsonData);
		 
		 
		 
		 
		 return ChartJsonData;
		 
	 }
	 
	 

	 
	 public String EquipmentwiseParameterComparisonOne(String ids,String Parameter1,String FromDate,String ToDate, String timezonevalue)
	 {
		 SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
		
		 Boolean blnIsRecordExist =false;
		 
		 String ChartJsonData ="";
		 String ChartJsonDataParameter ="";
		 String ChartJsonDataGraphValue ="";
		 String SiteName = "";
		 String EquipmentName = "";
		 		 
		
		 String Parameter = "";	 
		 String Parameter1AllChartData = "";
		 String ParameterAllChartData ="";
		 
		 
		 List<Equipment> lstEquipment = equipmentService.listEquipmentsByIds(ids);
		
		 
		 if(Parameter1.equals("") == false)
		 {			 		
			 String lstParameter1[] = null;
			 			 
			 Parameter = Parameter1;
			 
		
			 for(int z=0; z< lstEquipment.size();z++)
			 {
				 
				 
				 String EquipmentID = String.valueOf(lstEquipment.get(z).getEquipmentId());
				 String EquipmentRefName = lstEquipment.get(z).getCustomerNaming();
				 
				 
				 System.out.println("z" + z);
				 System.out.println("EquipmentID" + EquipmentID);
				 System.out.println("EquipmentRefName" + EquipmentRefName);
				 
				 String Parameter1ChartData = "";
				 
				 List<GraphDataBean> lstDataTransaction1 = dataTransactionService.getParameterValuesByEquipmentIdsWithDate(EquipmentID,Parameter,FromDate,ToDate, timezonevalue);
				 
				 
				 if(lstDataTransaction1.size()>0) {
				    	Integer a=0;
				    	
						for(Object object:lstDataTransaction1)
						{
							Object []obj=(Object[]) object;
							GraphDataBean objGraphDataBean =new GraphDataBean();
							objGraphDataBean.setTimestamp((Date) obj[0]);
							
							if(obj[1]==null)
							{
								objGraphDataBean.setValue(0.0);
							}
							else
							{
								objGraphDataBean.setValue((Double) obj[1]);
							}
							
							
							
							
							if(objGraphDataBean.getTimestamp() !=null)
					    	 {
					    		 	
						    	try {
									
						    		String varChartDate = sdfchart.format(objGraphDataBean.getTimestamp()).toString();
							    	 
							         Calendar cal = Calendar. getInstance();
							         cal.setTime(objGraphDataBean.getTimestamp());
							         cal. add(Calendar.HOUR, 5);
							         cal. add(Calendar.MINUTE, 30);
							         
							         
							         
							    	 //long millis = lstDataTransaction.get(a).getTimestamp().getTime();
							         long millis = cal.getTime().getTime();
							    	 varChartDate =String.valueOf(millis);
							    	 						    	 
							    	 Double dbValue = 0.0;
							    	 dbValue =  GetParameterValueInUnit( Parameter, objGraphDataBean.getValue());
							    	 
							    	 String varChartValue = String.format("%.2f", dbValue);
							    	
							    	 
							    	 if(a==0)
							    	 {
							    		 blnIsRecordExist =true;
							    		 Parameter1ChartData = "[" + varChartDate + "," + varChartValue + "]";
							    	 }
							    	 else
							    	 {
							    		 blnIsRecordExist =true;
							    		 Parameter1ChartData =  Parameter1ChartData + ",[" + varChartDate + "," + varChartValue + "]";
							    		 	
							    	 }
							      
							    	 
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
						    	
					    		 
					    	 }
					    	
							
							a = a + 1;
						}
						
					}

				 
				 Parameter1ChartData = "[" + Parameter1ChartData + "]";
				 
				 
				 System.out.println("z:" + z);
				 if(z==0)
		    	 {
					 Parameter1AllChartData = EquipmentRefName + " - " + Parameter + "||" + Parameter1ChartData;
		    	 }
		    	 else
		    	 {
		    		 Parameter1AllChartData =  Parameter1AllChartData + "&&" + EquipmentRefName + " - " + Parameter + "||" + Parameter1ChartData;
		    		 	
		    	 }
				 System.out.println("Parameter1AllChartData:" + Parameter1AllChartData);
			 }
			 
			 
			 ParameterAllChartData =Parameter1AllChartData;

			 System.out.println("ParameterAllChartData:" + ParameterAllChartData);
			 
			 
			 
				 
		 }
		 


		
		 
		 if(blnIsRecordExist ==false)
		 {
			 ChartJsonData ="";
		 }
		 else if(ParameterAllChartData.equals("")==true )
		 {
			 ChartJsonData ="";
		 }
		 else if(Parameter1.equals("")==false)
		 {
			 
			 
			 ChartJsonData =  "0$$" +  Parameter1 + "$$" + ParameterAllChartData;
			 
			
		 }		
		 else
		 {
			 ChartJsonData ="";
		 }
		 
		 
		  
		 
		 System.out.println(ChartJsonData);
		 
		 
		 
		 
		 return ChartJsonData;
		 
	 }
	 
	 
	 
	 
	 public String GetParameterUOM(String Paramerter)
	 {
		 if(Paramerter.equals("InputCurrent_01") || Paramerter.equals("InputCurrent_02") || Paramerter.equals("InputCurrent_03") || Paramerter.equals("InputCurrent_04")|| Paramerter.equals("InputCurrent_05")|| Paramerter.equals("InputCurrent_06") || Paramerter.equals("InputCurrent_07") || Paramerter.equals("InputCurrent_08")|| Paramerter.equals("InputCurrent_09")|| Paramerter.equals("InputCurrent_10"))
		 {return Paramerter + " (A)";}
		 else  if(Paramerter.equals("InputVolatge_01") || Paramerter.equals("InputVolatge_02") || Paramerter.equals("InputVolatge_03") || Paramerter.equals("InputVolatge_04")|| Paramerter.equals("InputVolatge_05")|| Paramerter.equals("InputVolatge_06") || Paramerter.equals("InputVolatge_07") || Paramerter.equals("InputVolatge_08")|| Paramerter.equals("InputVolatge_09")|| Paramerter.equals("InputVolatge_10"))
		 {return Paramerter + " (V)";}
			 else  if(Paramerter.equals("InputPower_01") || Paramerter.equals("InputPower_02") || Paramerter.equals("InputPower_03") || Paramerter.equals("InputPower_04")|| Paramerter.equals("InputPower_05")|| Paramerter.equals("InputPower_06") || Paramerter.equals("InputPower_07") || Paramerter.equals("InputPower_08")|| Paramerter.equals("InputPower_09")|| Paramerter.equals("InputPower_10"))
			 {return Paramerter + " (W)";}
			 else  if(Paramerter.equals("PhaseCurrent") || Paramerter.equals("PhaseCurrent_L1") || Paramerter.equals("PhaseCurrent_L2")|| Paramerter.equals("PhaseCurrent_L3"))
			 {return Paramerter + " (A)";}
			 else  if(Paramerter.equals("PhaseVolatge") || Paramerter.equals("PhaseVolatge_L1") || Paramerter.equals("PhaseVolatge_L2")|| Paramerter.equals("PhaseVolatge_L3"))
			 {return Paramerter + " (V)";}
			 else  if(Paramerter.equals("ApparentPower") )
			 {return Paramerter + " (VA)";}
			 else  if(Paramerter.equals("ActivePower") )
			 {return Paramerter + " (W)";}
			 else  if(Paramerter.equals("ReactivePower") )
			 {return Paramerter + " (VAr)";}
			 else  if(Paramerter.equals("TodayEnergy") && Paramerter.equals("TotalEnergy") )
			 {return Paramerter + " (Wh)";}
			 else  if(Paramerter.equals("IsolationResistance") )
			 {return Paramerter + " (Ohm)";}
			 else  if(Paramerter.equals("OutputFrequency") )
			 {return Paramerter + " (Hz)";}
			 else  if(Paramerter.equals("AmbientTemperature") || Paramerter.equals("ModuleTemperature") || Paramerter.equals("InverterTemperature") )
			 {return Paramerter + " (�C)";}
			 else  if(Paramerter.equals("WindSpeed") )
			 {return Paramerter + " (m/s)";}								 
			 else  if(Paramerter.equals("Rainfall") )
			 {return Paramerter + " ";}
			 else  if(Paramerter.equals("TotalHoursOn") )
			 {return Paramerter + " hours";}
			 else  if(Paramerter.equals("TodayHoursOn") )
			 {return Paramerter + " hours";}
			 else  if(Paramerter.equals("PhasePowerBalancer") )
			 {return Paramerter + " ";}
			 else  if(Paramerter.equals("DifferentialCurrent") )
			 {return Paramerter + " mA";}
			 else  if(Paramerter.equals("Irradiation") )
			 {return Paramerter + " W/m2";}
		       
		     
		 return Paramerter;
		 
		 
	 }
	 
	 
	 public Double GetParameterValueInUnit(String Paramerter,Double ParamerterValue)
	 {
			 if(Paramerter.equals("TodayEnergy") && Paramerter.equals("TotalEnergy") )
			 {
				 return ParamerterValue * 1000.00;
			 }
			 else 
			 {
				 return ParamerterValue;
			 }
			
	 }
	 
	 
	 
	    @RequestMapping(value="/filterstandardparameters",method=RequestMethod.POST)
        public ResponseEntity<Map<String,Object>> StandardParameters(String EquipmentIDList,String EquipmentsIDs,HttpSession session,Authentication authentication)throws ServletException
        {
              Map<String,Object> objResponseEntity=new HashMap<String,Object>();
              if(authentication== null)
              {
            	  System.out.println("login");
              }

              try{
            	  List<StandardParameterBean> lstStandardParameter = equipmentService.getStandardParameterByEquipmentIds(EquipmentIDList,EquipmentsIDs);
                  

            	  objResponseEntity.put("equipmentparameters", lstStandardParameter);  
            	  objResponseEntity.put("status", true);
            	  objResponseEntity.put("data", "Success");
            	  return new ResponseEntity<Map<String,Object>>(objResponseEntity, HttpStatus.OK);

              }catch(Exception e){
            	  objResponseEntity.put("parameters", null); 
            	  objResponseEntity.put("status", false);
            	  objResponseEntity.put("data", e.getMessage());
            	  return new ResponseEntity<Map<String,Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

              }
        }
              
	 
	 
	    @RequestMapping(value="/filterequipments{id}",method=RequestMethod.POST)
        public ResponseEntity<Map<String,Object>> Equipments(@PathVariable("id") String siteid,HttpSession session,Authentication authentication)throws ServletException
        {
	    	System.out.println("Sarath: "+siteid);
              Map<String,Object> objResponseEntity=new HashMap<String,Object>();

              List<EquipmentWithoutParameterListBean> lstEquipment = dataTransactionService.getEquipmentList(siteid);
              try{

            	  objResponseEntity.put("equipments", lstEquipment);  
            	  objResponseEntity.put("status", true);
            	  objResponseEntity.put("data", "Success");
            	  return new ResponseEntity<Map<String,Object>>(objResponseEntity, HttpStatus.OK);

              }catch(Exception e){
            	  objResponseEntity.put("equipments", null); 
            	  objResponseEntity.put("status", false);
            	  objResponseEntity.put("data", e.getMessage());
            	  return new ResponseEntity<Map<String,Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

              }
        }
              
	    
	    
	    @RequestMapping(value="/filterequipmentparameters{id}",method=RequestMethod.POST)
        public ResponseEntity<Map<String,Object>> EquipmentParameters(@PathVariable("id") String SiteID ,HttpSession session,Authentication authentication)throws ServletException
        {
              Map<String,Object> objResponseEntity=new HashMap<String,Object>();

              //List<EquipmentWithParameterListBean> lstEquipmentParameters = dataTransactionService.getEquipmentWithParameterList(SiteID);
              List<StandardParameterBean> lstEquipmentParameters = equipmentService.getStandardParameterBySiteIds(SiteID);
              try{

            	  objResponseEntity.put("equipmentparameters", lstEquipmentParameters);  
            	  objResponseEntity.put("status", true);
            	  objResponseEntity.put("data", "Success");
            	  return new ResponseEntity<Map<String,Object>>(objResponseEntity, HttpStatus.OK);

              }catch(Exception e){
            	  objResponseEntity.put("equipmentparameters", null); 
            	  objResponseEntity.put("status", false);
            	  objResponseEntity.put("data", e.getMessage());
            	  return new ResponseEntity<Map<String,Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

              }
        }
              
	    
	    
	    
	    
	    
	 
	 
	 @RequestMapping(value = "/chartrequest", method = RequestMethod.POST)
	 @ResponseBody
	 public String JsonChartData(String SiteIDList, String EquipmentIDList,String Parameter1, String Parameter2,String Range,String FromDate, String ToDate, String GraphType,HttpSession session) {
		 String timezonevalue = "0"; 
		 
		 String ChartValue = "";
		 Integer SiteID = 0;
		 Integer InverterID = 0;
		 
		 Boolean bolAllSite = false; 
		 Boolean bolAllEquipment = false; 
		 Boolean bolAllParameter =false;
		 
		 
		 try
		 {
			 if(!timezonevalue.equals("") && timezonevalue !=null)
				{
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}	
			 
			 EquipmentIDList = EquipmentIDList.replace("Inverter,","");
			 EquipmentIDList = EquipmentIDList.replace(",Inverter","");
			 EquipmentIDList = EquipmentIDList.replace("Sensor,","");
			 EquipmentIDList = EquipmentIDList.replace(",Sensor","");
				
				
			 if(SiteIDList.contains(","))
			 {
				 String lstSite[] = SiteIDList.split(",");
				 SiteID = Integer.parseInt(lstSite[0]);
				 bolAllSite = true;
			 }
			 else
			 {
				 SiteID = Integer.parseInt(SiteIDList);
			 }
			 
			 
			 
			 if(EquipmentIDList.contains(","))
			 {
				 String lstEquipment[] = EquipmentIDList.split(",");
				 InverterID = Integer.parseInt(lstEquipment[0]);
				 bolAllEquipment = true;
			 }
			 else
			 {
				 InverterID = Integer.parseInt(EquipmentIDList);
			 }
			 
			 
			 
			 if(Parameter1.contains(","))
			 {
				 String lstParameter1[] = Parameter1.split(",");
				 bolAllParameter = true;
			 }
			
			 
		 }
		 catch(Exception e)
		    {
		    	System.out.println(e.getMessage());
		    }
		

		 
		 if(GraphType.equals("Daily Energy Generation"))
		 {
			 FromDate = FromDate + " 00:00:00";
			 ToDate = ToDate + " 23:59:59";
		 }
		/* else if(GraphType.equals("Energy Performance"))
		 {
			 FromDate = FromDate + " 05:30:00";
			 ToDate = ToDate + " 20:00:00";

		 }*/
		 else if(Range.equals("daily"))
		 {
			 FromDate = FromDate + " 06:00:00";
			 ToDate = ToDate + " 19:00:00";
		 }
		 else 
		 {
			 FromDate = FromDate + " 06:00:00";
			 ToDate = ToDate + " 19:00:00";
		 }
		 
		 
		 //System.out.println("Inverter ID  "+InverterID);
		 
		 if(GraphType.equals("Daily Energy Generation"))
		 {
			 /*if(Range.equals("daily"))
			 {
			 
			 }	*/	 
			 if(bolAllSite)
			 {
				 ChartValue = SiteGroupDailyEnergy(SiteIDList,FromDate,ToDate,  timezonevalue);
			 }
			 else
			 {
				 ChartValue = SitewsieDailyEnergy(SiteID,FromDate,ToDate,  timezonevalue);
			 }
			 
			 
		 }
		 else if(GraphType.equals("Energy Performance"))
		 {
			 
			 
			 if(bolAllEquipment)
			 {
				 ChartValue = EquipmentGroupEnergyPerformance(EquipmentIDList,FromDate,ToDate,  timezonevalue);
			 }
			 else
			 {
				 ChartValue = EquipmentwiseEnergyPerformance(InverterID,FromDate,ToDate,  timezonevalue);
			 }
			
		 }
		 else if(GraphType.equals("Parameter Comparison"))
		 {
			 
			 if(bolAllParameter)
			 {
				 ChartValue = EquipmentwiseParameterComparisonAll(EquipmentIDList,Parameter1,FromDate,ToDate,  timezonevalue);
			 }
			 else
			 {
				 //ChartValue = EquipmentwiseParameterComparison(EquipmentIDList,Parameter1,Parameter2,FromDate,ToDate,  timezonevalue);
				 ChartValue = EquipmentwiseParameterComparisonOne(EquipmentIDList,Parameter1,FromDate,ToDate,  timezonevalue);
			 }
			 
			 
			 
		 }
		 
		 
		 //System.out.println(ChartValue);
		 
		
		 
		 //return "{\"success\":" + ChartValue + "}";
		 return ChartValue;
		 }
	 
 @RequestMapping(value = "/analysis_old", method = RequestMethod.GET)
 public String listAnalysisViews( Model model,HttpSession session,Authentication authentication) {
 	
	 if(authentication==null)
		{
			return "redirect:/login";
		}
		
			LoginUserDetails loginUserDetails=(LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();
 	
 	EquipmentListBean objEquipmentListBean = new EquipmentListBean();
 	
 	/*Equipment objEquipment = equipmentService.getEquipmentById(id);
	List<DataTransaction> lstDataTransaction = dataTransactionService.getDataTransactionListByEquipmentId(id);
	*/
	
	 String ChartJsonData1 = "";
	 String ChartJsonData1Parameter = "";
	 String ChartJsonData1GraphValue = "";
	 

	 SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	 //SimpleDateFormat sdfchart = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	 SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
			
	 String varEquipmentRef ="";
	 String varSiteID ="";
	 String varEquipmentID ="";
	 String varEquipmentCode ="";
	 String varEquipmentName ="";
	 String varEquipmentType ="";
	 
	 
	 /*if(objEquipment.getCustomerReference()!=null)
	 {
		 varEquipmentRef = objEquipment.getCustomerNaming(); 
	 }
	 
	 if(objEquipment.getSiteID()!=null)
	 {
		 varSiteID = objEquipment.getSiteID().toString(); 
	 }
	 
	 if(objEquipment.getEquipmentId()!=null)
	 {
		 varEquipmentID = objEquipment.getEquipmentId().toString(); 
	 }
	 
	 if(objEquipment.getEquipmentCode()!=null)
	 {
		 varEquipmentCode = objEquipment.getEquipmentCode(); 
	 }
	 
	 if(objEquipment.getDisplayName()!=null)
	 {
		 varEquipmentName = objEquipment.getDisplayName(); 
	 }
	
	 if(objEquipment.getEquipmentTypeName()!=null)
	 {
		 varEquipmentType = objEquipment.getEquipmentTypeName(); 
	 }
	 
	 objEquipmentListBean.setSiteID(varSiteID);
     objEquipmentListBean.setEquipmentID(varEquipmentID);
     objEquipmentListBean.setEquipmentCode(varEquipmentCode);
     objEquipmentListBean.setEquipmentReference(varEquipmentRef);
     objEquipmentListBean.setEquipmentName(varEquipmentName);
     objEquipmentListBean.setEquipmentType(varEquipmentType);
    
    */

     List<Customer> lstCustomer = customerService.getCustomerListByUserId(id);
     List<Site> lstSite = siteService.getSiteListByUserId(id);
     List<Equipment> lstEquipment = equipmentService.listEquipmentsByUserId(id);
     List<Equipment> lstInverter = equipmentService.listInvertersByUserId(id);
     List<Equipment> lstSensor = equipmentService.listSensorsByUserId(9999);
 	
     System.out.println("mohan sensor size : " + lstSensor.size());
     
     for(int a=0;a<lstSite.size();a++)
     {
    	 Integer varStrSiteID = lstSite.get(a).getSiteId();
    	 String varStrSiteRefCode = lstSite.get(a).getCustomerNaming();
    	 
    	 lstEquipment.stream().filter(p -> p.getSiteID().equals(varStrSiteID)).collect(Collectors.toList()).forEach(p -> p.setSiteReference(varStrSiteRefCode));
    	 lstInverter.stream().filter(p -> p.getSiteID().equals(varStrSiteID)).collect(Collectors.toList()).forEach(p -> p.setSiteReference(varStrSiteRefCode));
    	 lstSensor.stream().filter(p -> p.getSiteID().equals(varStrSiteID)).collect(Collectors.toList()).forEach(p -> p.setSiteReference(varStrSiteRefCode));
     }
     
  
     

	    System.out.println("mohan sensor size : " + lstSensor.size());
	    
	    
     
    
 	
    
     
     objEquipmentListBean.setChartJsonData1(ChartJsonData1);
     objEquipmentListBean.setChartJsonData1Parameter(ChartJsonData1Parameter);
     objEquipmentListBean.setChartJsonData1GraphValue(ChartJsonData1GraphValue);
     
     
     System.out.println(ChartJsonData1);
     
     
     
     
	 AccessListBean objAccessListBean = new AccessListBean();
	   
	    try
	    {
	    User objUser =  userService.getUserById(id);
		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
	 	
	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
	   
	    objAccessListBean.setUserID(id);
	    objAccessListBean.setUserName(objUser.getShortName());
	    
	    
	    
	    /*
	    Site objSite = siteService.getSiteById(objEquipment.getSiteID()); 
	    
	    if(objSite!=null)
	    {
	    	objAccessListBean.setMySiteID(objSite.getSiteId());
	  	    objAccessListBean.setMySiteName(objSite.getSiteName());
	  	    objAccessListBean.setMyCustomerID(objSite.getCustomerID());
	    	
	  	    Customer objCustomer = customerService.getCustomerById(objSite.getCustomerID()); 
	  	    if(objCustomer!=null)
	  	    {
	  	    	objAccessListBean.setMyCustomerName(objCustomer.getCustomerName());
	  	    }
	  	    else
	  	    {
	  	    	objAccessListBean.setMyCustomerName("Customer View");
	  	    }
	  	    
	    }
	    else
	    {
	    	objAccessListBean.setMySiteName("Site View");
	    }
	    
	    */
	    
	    
	  
	    
	    
	    
	    if(objUser.getRoleID()!=4)
		{
			objAccessListBean.setCustomerListView("visible");
		}
	    
	    
	    for(int l=0;l<lstRoleActivity.size();l++)
	    {
	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setDashboard("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setOverview("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setSystemMonitoring("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setVisualization("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setAnalytics("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setPortfolioManagement("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setTicketing("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setForcasting("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setConfiguration("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==10 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setAnalysis("visible");
	    	}
	    }
	    
	    }
	    catch(Exception e)
	    {
	    	System.out.println(e.getMessage());
	    }
	    
	     model.addAttribute("access",objAccessListBean);
	     model.addAttribute("siteList", getDropdownList("Site",id));
 	     model.addAttribute("userList", getDropdownList("User",id));
	     model.addAttribute("equipmentview", objEquipmentListBean);
 	     model.addAttribute("analysis", new AnalysisListBean());
 	     model.addAttribute("ticketcreation", new TicketDetail());
	     
	     model.addAttribute("lstCustomer", lstCustomer);
	     model.addAttribute("lstSite", lstSite);
 	     model.addAttribute("lstInverter", lstInverter);
	     model.addAttribute("lstSensor", lstSensor);
	     
	     
    
     return "analysis";
 }
 
 public  Map<String,String> getDropdownList(String DropdownName , int refid) 
	{
    	Map<String,String> ddlMap = new LinkedHashMap<String,String>();
    	
    	if(DropdownName == "Site")
    	{
    		List<Site> ddlList = siteService.getSiteListByUserId(refid);
    		
    		for(int i=0;i< ddlList.size();i++)
    		{
    			ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
    		}
    		
    	}
    	
    	else if(DropdownName == "ProductionSites")
    	{
    		List<Site> ddlList = siteService.getProductionSiteListByUserId(refid);
    		
    		for(int i=0;i< ddlList.size();i++)
    		{
    			ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
    		}
    		
    	}
    	else if(DropdownName == "User")
    	{
    		List<User> ddlList = userService.listFieldUsers();
    		
    		for(int i=0;i< ddlList.size();i++)
    		{
    			ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
    		}
    		
    	}
    	
		return ddlMap;
	}
 
 
 
 
 
 
 
 
}
