
/******************************************************
 * 
 *    	Filename	: EquipmentViewController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals Equipment display functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.EnergyPerformanceBean;
import com.mestech.eampm.bean.EquipmentListBean;
import com.mestech.eampm.bean.GraphDataBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.bean.TicketFilter;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.DataTransaction;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentCategory;
import com.mestech.eampm.model.EquipmentType;
import com.mestech.eampm.model.EventDetail;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.DataTransactionService;
import com.mestech.eampm.service.EquipmentCategoryService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.EquipmentTypeService;
import com.mestech.eampm.service.EventDetailService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.TicketDetailService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.EquipmentHistroy;
import com.mestech.eampm.utility.EquipmentPojo;
import com.mestech.eampm.utility.UtilityCommon;
import com.mestech.eampm.utility.UtilityCommonDAO;

@Controller
public class EquipmentViewController {

	@Autowired
	private SiteService siteService;

	@Autowired
	private DataTransactionService dataTransactionService;

	@Autowired
	private EquipmentService equipmentService;
	@Autowired
	private EquipmentTypeService equipmentTypeService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private EquipmentCategoryService equipmentcategoryService;

	@Autowired
	private EventDetailService eventDetailService;

	@Autowired
	private TicketDetailService ticketDetailService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	private UtilityCommon utilityCommon = new UtilityCommon();
	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();

	/********************************************************************************************
	 * 
	 * Function Name : listEvents
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This method loads equipment view page.
	 * 
	 * Input Params  : equipmentid,model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/equipmentview{id}", method = RequestMethod.GET)
	public String listEvents(@PathVariable("id") String equipmentid, Model model, HttpSession session,
			Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		session.setAttribute("EquipmentID", equipmentid);
		Equipment equipment = equipmentService.getEquipmentById(Integer.valueOf(equipmentid));

		Site site = siteService.getSiteById(equipment.getSiteID());
		session.setAttribute("SiteID", site.getSiteName());
		Customer customer = customerService.getCustomerById(site.getCustomerID());
		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());
			objAccessListBean.setTicketid(Integer.valueOf(equipmentid));
			objAccessListBean.setMyCustomerID(site.getCustomerID());
			objAccessListBean.setMySiteID(site.getSiteId());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("ticketfilter", new TicketFilter());
		model.addAttribute("eventdetail", new EventDetail());

		model.addAttribute("siteList", getNewTicketSiteList("Site", id));

		model.addAttribute("equipmentList", getDropdownList("Equipment", id));
		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("customername", customer.getCustomerName());
		model.addAttribute("equipmentname", equipment.getEquipmentCode());
		model.addAttribute("sitename", site.getSiteName());

		return "equipmentviews";
	}

	/********************************************************************************************
	 * 
	 * Function Name : GetDateInMilliSecond
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns mills from date.
	 * 
	 * Input Params  : Timestamp
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	public String GetDateInMilliSecond(Date Timestamp) {
		String varChartDate = "";
		Calendar cal = Calendar.getInstance();
		cal.setTime(Timestamp);

		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)
				&& utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
			// No Change
		} else { // For IST
			cal.add(Calendar.HOUR, 5);
			cal.add(Calendar.MINUTE, 30);
		}

		long millis = cal.getTime().getTime();
		varChartDate = String.valueOf(millis);

		return varChartDate;
	}

	/********************************************************************************************
	 * 
	 * Function Name : equipmentDataAgaintId
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This method returns equipments details.
	 * 
	 * Input Params  : session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getEquipmentDetails", method = RequestMethod.GET)

	public ResponseEntity<Map<String, Object>> equipmentDataAgaintId(HttpSession session,
			Authentication authentication) {
		String timezonevalue = "0";
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int userid = loginUserDetails.getEampmuserid();

		if (!timezonevalue.equals("") && timezonevalue != null) {
			timezonevalue = session.getAttribute("timezonevalue").toString();
		}
		int id = Integer.valueOf(session.getAttribute("EquipmentID").toString());
		String sitename = session.getAttribute("SiteID").toString();
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			Equipment equipment = equipmentService.getEquipmentById(id);

			if (equipment != null) {
				EquipmentType equipmentType = equipmentTypeService.getEquipmentTypeById(equipment.getEquipmentTypeID());
				if (equipmentType != null) {
					EquipmentCategory equipmentCategory = equipmentcategoryService
							.getEquipmentCategoryById(equipmentType.getCategoryID());
					if (equipmentCategory != null) {
						EquipmentPojo equipmentPojo = new EquipmentPojo(equipment.getEquipmentCode(),
								equipment.getCapacity().toString(), equipment.getCustomerReference(),
								equipmentCategory.getEquipmentCategory(), equipment.getCustomerNaming(),
								equipmentType.getEquipmentType(), equipment.getComponents(),
								equipment.getDisconnectRating(), equipment.getDisconnectType());
						objResponseEntity.put("equipment", equipmentPojo);
						objResponseEntity.put("sitename", sitename);

						if (equipment.getWarrantyDate() != null) {
							equipment.setWarrantyDateText(sdf1.format(equipment.getWarrantyDate()));
						}
						if (equipment.getInstallationDate() != null) {
							equipment.setInstallationDateText(sdf1.format(equipment.getInstallationDate()));
						}
						EquipmentHistroy equipmentHistroy = new EquipmentHistroy(equipment.getEquipmentSelection(),
								equipment.getInstallationDateText(), equipment.getWarrantyDateText(),
								equipment.getDescription(), equipment.getRemarks(), null, null);
						objResponseEntity.put("equipmentHistory", equipmentHistroy);
						List<EventDetail> eventDetails = eventDetailService
								.findByEquipmentId(equipment.getEquipmentId(), timezonevalue);

						objResponseEntity.put("eventDetail", eventDetails);
						List<TicketDetail> ticketDetails = ticketDetailService
								.findByEquipmentId(equipment.getEquipmentId(), timezonevalue);
						List<Site> lstSite = siteService.getSiteListByUserId(userid);
						List<User> lstUser = userService.listUsers();
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
						SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

						for (int i = 0; i < ticketDetails.size(); i++) {
							Integer intSiteID = ticketDetails.get(i).getSiteID();
							Integer intAssignedID = ticketDetails.get(i).getAssignedTo();
							Integer intequipmentID = ticketDetails.get(i).getEquipmentID();
							Integer intCreatedBy = ticketDetails.get(i).getCreatedBy();
							if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
									.collect(Collectors.toList()).size() >= 1) {
								Site objSite = new Site();
								objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
										.collect(Collectors.toList()).get(0);

								ticketDetails.get(i).setSiteName(objSite.getSiteName());
							}

							if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
									.collect(Collectors.toList()).size() >= 1) {
								User objUser = new User();
								objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
										.collect(Collectors.toList()).get(0);

								ticketDetails.get(i).setAssignedToWhom(objUser.getUserName());

							}
							if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
									.collect(Collectors.toList()).size() >= 1) {
								User objUser = new User();
								objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
										.collect(Collectors.toList()).get(0);

								ticketDetails.get(i).setCreatedByName(objUser.getUserName());

							}

							if (ticketDetails.get(i).getCreationDate() != null) {
								ticketDetails.get(i).setCreationDate(ticketDetails.get(i).getCreationDate());
								ticketDetails.get(i)
										.setCreatedDateText(sdf.format(ticketDetails.get(i).getCreationDate()));

								// System.out.println( " Mohanraj ::: " +
								// sdf.format(lstTicketDetail.get(i).getCreationDate()));
							}

							if (ticketDetails.get(i).getScheduledOn() != null) {
								ticketDetails.get(i).setScheduledOn(ticketDetails.get(i).getScheduledOn());
								ticketDetails.get(i)
										.setScheduledDateText(sdfdate.format(ticketDetails.get(i).getScheduledOn()));

							}

						}

						objResponseEntity.put("ticketDetail", ticketDetails);
						String FromDate = sdfdate.format(new Date());
						String ToDate = sdfdate.format(new Date());
						FromDate = FromDate + " 04:00:00";
						ToDate = ToDate + " 20:00:00";
						String ChartJsonData1 = "";
						if (equipmentCategory.getEquipmentCategory().equals("CENTRLINVRTR")
								|| equipmentCategory.getEquipmentCategory().equals("STRINGINVRTR")
								|| equipmentCategory.getEquipmentCategory().equals("Primary Energy Meter")
								|| equipmentCategory.getEquipmentCategory().equals("Secondary Energy Meter")) {
							SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							sdfdate = new SimpleDateFormat("dd-MM-yyyy");
							id = loginUserDetails.getEampmuserid();
							session.setAttribute("EquipmentID", equipment.getEquipmentId());
							String SiteName = "";
							String EquipmentName = "";

							if (!timezonevalue.equals("") && timezonevalue != null) {
								timezonevalue = session.getAttribute("timezonevalue").toString();
							}

							List<EnergyPerformanceBean> lstDataTransaction = dataTransactionService
									.getEnergyDataByEquipmentIdsWithDate(equipment.getEquipmentId().toString(),
											FromDate, ToDate, timezonevalue);
							String YesterdayEnergy = dataTransactionService.getYesterdayEnergyByEquipmentIdsWithDate(
									equipment.getEquipmentId().toString(), FromDate, ToDate, timezonevalue);
							Double dblYesterdayEnergyValue = Double.valueOf(YesterdayEnergy) * 1000.00;

							String TodayEnergyStatus = dataTransactionService
									.getTodayFlagStatus(equipment.getEquipmentId().toString(), timezonevalue);
							lstSite = siteService.listSiteById(Integer.valueOf(equipment.getSiteID()));
							if (lstSite != null && lstSite.size() > 0) {
								TodayEnergyStatus = String.valueOf(lstSite.get(0).getTodayEnergyFlag());
								SiteName = lstSite.get(0).getSiteName() + " (" + lstSite.get(0).getCustomerReference()
										+ ")";

							}
							List<GraphDataBean> lstGraphDataBean = new ArrayList<GraphDataBean>();
							Boolean bolInitialCheckFlag = false;
							Double dblInitialPeakValue = 0.0;

							if (lstDataTransaction.size() > 0) {
								Integer iCnt = 0;

								for (Object object : lstDataTransaction) {
									Object[] obj = (Object[]) object;
									GraphDataBean objGraphDataBean = new GraphDataBean();
									objGraphDataBean.setTimestamp((Date) obj[0]);

									if (TodayEnergyStatus.equals("1")) {
										if (obj[2] == null) {
											objGraphDataBean.setValue(0.0);
										} else {
											Double dblTodayValue = ((java.math.BigDecimal) obj[2]).doubleValue()
													* 1000.00;

											if (iCnt.equals(0)) {
												objGraphDataBean.setValue(0.0);
												dblInitialPeakValue = dblTodayValue;
											} else if (bolInitialCheckFlag == false
													&& dblInitialPeakValue.equals(dblTodayValue)) {
												objGraphDataBean.setValue(0.0);
											} else {
												objGraphDataBean.setValue(dblTodayValue);
												bolInitialCheckFlag = true;
											}

										}
									} else {
										if (obj[1] == null) {
											objGraphDataBean.setValue(0.0);
										} else {
											Double dblTotalValue = ((java.math.BigDecimal) obj[1]).doubleValue()
													* 1000.00;
											Double dblTodayValue = dblTotalValue - dblYesterdayEnergyValue;

											objGraphDataBean.setValue(dblTodayValue);

										}
									}

									if (objGraphDataBean.getTimestamp() != null) {

										try {

											lstGraphDataBean.add(objGraphDataBean);

										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

									}

									iCnt = iCnt + 1;

								}

							}

							for (int a = 0; a < lstGraphDataBean.size(); a++) {
								String varChartDate = sdfchart.format(lstGraphDataBean.get(a).getTimestamp())
										.toString();
								varChartDate = GetDateInMilliSecond(lstGraphDataBean.get(a).getTimestamp());

								Double dbTotalEnergy = lstGraphDataBean.get(a).getValue();
								Double dbTotalEnergyPrevious = lstGraphDataBean.get(a).getValue();

								Double dbTotalEnergyPresent = 0.0;
								if (a != 0) {
									if (sdfdate.format(lstGraphDataBean.get(a).getTimestamp()).toString().equals(
											sdfdate.format(lstGraphDataBean.get(a - 1).getTimestamp()).toString())) {
										if (a == 1) {
											dbTotalEnergyPrevious = lstGraphDataBean.get(a - 1).getValue();
											dbTotalEnergyPresent = dbTotalEnergy - dbTotalEnergyPrevious;
										} else if (lstGraphDataBean.get(a - 1).getValue() >= lstGraphDataBean.get(a - 2)
												.getValue()) {
											dbTotalEnergyPrevious = lstGraphDataBean.get(a - 1).getValue();
											dbTotalEnergyPresent = dbTotalEnergy - dbTotalEnergyPrevious;

										}

									}

								}

								String varChartValue = String.format("%.2f", dbTotalEnergyPresent);

								if (a == 0) {
									Date FirstDateTime = null;

									try {
										FirstDateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(FromDate);
									} catch (ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

									String varFirstDate = GetDateInMilliSecond(FirstDateTime);

									if (varFirstDate.equals(varChartDate)) {
										ChartJsonData1 = "[" + varChartDate + "," + varChartValue + "]";
									} else {
										ChartJsonData1 = "[" + varFirstDate + ",0]," + "[" + varChartDate + ","
												+ varChartValue + "]";
									}

								} else if (a == lstGraphDataBean.size() - 1) {

									Date FinalDateTime = null;

									try {
										FinalDateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(ToDate);
									} catch (ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

									String varChartDateLast = GetDateInMilliSecond(FinalDateTime);

									if (varChartDateLast.equals(varChartDate)) {
										ChartJsonData1 = ChartJsonData1 + ",[" + varChartDate + "," + varChartValue
												+ "]";
									} else {
										ChartJsonData1 = ChartJsonData1 + ",[" + varChartDate + "," + varChartValue
												+ "]" + ",[" + varChartDateLast + ",null]";
									}

								} else {
									ChartJsonData1 = ChartJsonData1 + ",[" + varChartDate + "," + varChartValue + "]";

								}

							}
							objResponseEntity.put("equipmentgraphdata", ChartJsonData1);
							objResponseEntity.put("yaxis", "Energy (Wh)");

						} else {
							String categoryType = equipmentCategory.getEquipmentCategory();
							switch (categoryType) {
							case "STRINGCOMBINER":
								ChartJsonData1 = EquipmentviewSmb(equipment.getEquipmentId().toString(), FromDate,
										ToDate, timezonevalue);
								objResponseEntity.put("equipmentgraphdata", ChartJsonData1);
								objResponseEntity.put("yaxis", "InputPower (W)");
								break;
							case "Tracker Sensor":
								ChartJsonData1 = EquipmentviewTracker(equipment.getEquipmentId().toString(), FromDate,
										ToDate, timezonevalue);
								objResponseEntity.put("equipmentgraphdata", ChartJsonData1);
								objResponseEntity.put("yaxis", "CommandAngle ( � )");
								break;
							default:
								break;
							}

						}

					}
				}
			}
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "User") {
			List<User> ddlList = userService.listFieldUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentviewSmb
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns smb data.
	 * 
	 * Input Params  : strEquipmentID,FromDate,ToDate,timezonevalue
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	public String EquipmentviewSmb(String strEquipmentID, String FromDate, String ToDate, String timezonevalue) {
		SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
		Boolean blnIsRecordExist = false;
		String ParameterChartData = "";

		List<GraphDataBean> lstDataTransaction = dataTransactionService.getEquipmentviewForSmb(strEquipmentID, FromDate,
				ToDate, timezonevalue);

		if (lstDataTransaction.size() > 0) {
			Integer a = 0;

			for (Object object : lstDataTransaction) {
				Object[] obj = (Object[]) object;
				GraphDataBean objGraphDataBean = new GraphDataBean();

				objGraphDataBean.setTimestamp((Date) obj[0]);
				if (obj[1] == null) {
					objGraphDataBean.setValue(0.0);
				} else {
					// objGraphDataBean.setValue((Double) obj[1]);
					objGraphDataBean.setValue(utilityCommon.DoubleFromBigDecimalObject(obj[1]));

				}

				if (objGraphDataBean.getTimestamp() != null) {

					try {

						String varChartDate = sdfchart.format(objGraphDataBean.getTimestamp()).toString();

						Calendar cal = Calendar.getInstance();
						cal.setTime(objGraphDataBean.getTimestamp());

						if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)
								&& utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {

						} else { // For IST
							cal.add(Calendar.HOUR, 5);
							cal.add(Calendar.MINUTE, 30);
						}

						long millis = cal.getTime().getTime();
						varChartDate = String.valueOf(millis);

						Double dbValue = 0.0;
						dbValue = GetParameterValueInUnit("InputPower_01", objGraphDataBean.getValue());

						String varChartValue = String.format("%.2f", dbValue);

						if (a == 0) {
							blnIsRecordExist = true;
							ParameterChartData = "[" + varChartDate + "," + varChartValue + "]";
						} else {
							blnIsRecordExist = true;
							ParameterChartData = ParameterChartData + ",[" + varChartDate + "," + varChartValue + "]";

						}

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				a = a + 1;
			}

		}
		return ParameterChartData;
	}

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentviewTracker
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns Tracker data.
	 * 
	 * Input Params  : strEquipmentID,FromDate,ToDate,timezonevalue
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	public String EquipmentviewTracker(String strEquipmentID, String FromDate, String ToDate, String timezonevalue) {
		SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
		Boolean blnIsRecordExist = false;
		String ParameterChartData = "";

		List<GraphDataBean> lstDataTransaction = dataTransactionService.getEquipmentviewForTracker(strEquipmentID,
				FromDate, ToDate, timezonevalue);

		if (lstDataTransaction.size() > 0) {
			Integer a = 0;

			for (Object object : lstDataTransaction) {
				Object[] obj = (Object[]) object;
				GraphDataBean objGraphDataBean = new GraphDataBean();

				objGraphDataBean.setTimestamp((Date) obj[0]);
				if (obj[1] == null) {
					objGraphDataBean.setValue(0.0);
				} else {
					// objGraphDataBean.setValue((Double) obj[1]);
					objGraphDataBean.setValue(utilityCommon.DoubleFromBigDecimalObject(obj[1]));

				}

				if (objGraphDataBean.getTimestamp() != null) {

					try {

						String varChartDate = sdfchart.format(objGraphDataBean.getTimestamp()).toString();

						Calendar cal = Calendar.getInstance();
						cal.setTime(objGraphDataBean.getTimestamp());

						if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)
								&& utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {

						} else { // For IST
							cal.add(Calendar.HOUR, 5);
							cal.add(Calendar.MINUTE, 30);
						}

						long millis = cal.getTime().getTime();
						varChartDate = String.valueOf(millis);

						Double dbValue = 0.0;
						dbValue = GetParameterValueInUnit("SunAngle", objGraphDataBean.getValue());

						String varChartValue = String.format("%.2f", dbValue);

						if (a == 0) {
							blnIsRecordExist = true;
							ParameterChartData = "[" + varChartDate + "," + varChartValue + "]";
						} else {
							blnIsRecordExist = true;
							ParameterChartData = ParameterChartData + ",[" + varChartDate + "," + varChartValue + "]";

						}

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				a = a + 1;
			}

		}

		return ParameterChartData;
	}

	/********************************************************************************************
	 * 
	 * Function Name : GetParameterValueInUnit
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns parameter value in unit.
	 * 
	 * Input Params  : Paramerter,ParamerterValue
	 * 
	 * Return Value  : Double
	 * 
	 * 
	 **********************************************************************************************/
	public Double GetParameterValueInUnit(String Paramerter, Double ParamerterValue) {
		if (Paramerter.equals("TodayEnergy") && Paramerter.equals("TotalEnergy")) {
			return ParamerterValue * 1000.00;
		} else {
			return ParamerterValue;
		}
	}

}
