
/******************************************************
 * 
 *    	Filename	: TicketViewController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Tickets Displaying Activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.bean.TicketFilter;
import com.mestech.eampm.bean.TicketHistory;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.TicketTransaction;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.Currency;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.JmrDetail;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteType;
import com.mestech.eampm.model.State;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.TicketDetailService;
import com.mestech.eampm.service.TicketTransactionService;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.CurrencyService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.JmrDetailService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteTypeService;
import com.mestech.eampm.service.StateService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

/*import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;*/
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Controller
public class TicketViewController {

	@Autowired
	private TicketDetailService ticketdetailService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private JmrDetailService jmrdetailService;

	@Autowired
	private TicketTransactionService ticketTransactionService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	String pdfdownloadurl = "http://localhost/pdf/";
	String PdfOutputPath = "C:/Program Files/apache-tomcat-8.5.20/webapps/pdf/";

	// String pdfdownloadurl= "http://www.inspirece.com/pdf/";
	// String PdfOutputPath = "E:/apache-tomcat-8.5.23/webapps/pdf/";

	/*
	 * String DBDriver1 = "oracle.jdbc.driver.OracleDriver"; String DBConnection1 =
	 * "jdbc:oracle:thin:@182.72.70.78:1521:eampmdb"; String DBUser1 ="eampm";
	 * String DBPassword1 = "Eampm123#";
	 * 
	 * 
	 * String DBDriver2 = "org.postgresql.Driver"; String DBConnection2 =
	 * "jdbc:postgresql://localhost:5432/eampmdb"; String DBUser2 ="eampm"; String
	 * DBPassword2 = "Admin123";
	 */

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function return values for dropdown.
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getUpdatedSiteList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns list of sites.
	 * 
	 * Return Value  : List<Site>
	 * 
	 * 
	 **********************************************************************************************/
	public List<Site> getUpdatedSiteList() {
		List<Site> ddlSitesList = siteService.listSites();

		for (int i = 0; i < ddlSitesList.size(); i++) {

			List<State> lstStateList = new ArrayList<State>();

			int cid = ddlSitesList.get(i).getCustomerID();
			int stateid = ddlSitesList.get(i).getStateID();
			int currid = ddlSitesList.get(i).getCurrencyID();

		}
		return ddlSitesList;
	}

	/********************************************************************************************
	 * 
	 * Function Name : GetCastedGraphDataBeanList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returs vlaues for dropdown list.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "FieldUser") {
			List<User> ddlList = userService.listFieldUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		} else if (DropdownName == "User") {
			List<User> ddlList = userService.listFieldUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : listTicketDetails
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads tickets view page.
	 * 
	 * Input Params  : refid,model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/ticketviews{id}", method = RequestMethod.GET)
	public String listTicketDetails(@PathVariable("id") int refid, Model model, HttpSession session,
			Authentication authentication) throws ParseException {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String timezonevalue = "0";
		if (!timezonevalue.equals("") && timezonevalue != null) {
			timezonevalue = session.getAttribute("timezonevalue").toString();
		}
		loginUserDetails.setTimezoneoffsetmin(timezonevalue);

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

		List<User> lstUsers = userService.listUsers();
		List<Site> lstSites = siteService.listSites();
		TicketDetail objTicketdetail = ticketdetailService.getTicketDetailByIdOnUTC(refid, timezonevalue);

		List<Equipment> lstEquipment = equipmentService.listEquipmentsByUserId(id);
		List<TicketTransaction> lstTicketTransactions = ticketTransactionService
				.getTicketTransactionListByTicketId(refid, timezonevalue);
		List<TicketHistory> lstTicketHistories = new ArrayList<TicketHistory>();

		/*
		 * for(int j=0;j<lstTicketTransactions.size();j++) {
		 * System.out.println(lstTicketTransactions.get(j).getRemarks() + " " +
		 * lstTicketTransactions.get(j).getTicketID() + " " +
		 * lstTicketTransactions.get(j).getTicketStatus() + " " +
		 * lstTicketTransactions.get(j).getTimeStamp()); }
		 */

		System.out.println("UploadFlag :" + objTicketdetail.getUploadFlag());

		if (lstTicketTransactions != null) {
			for (int i = 0; i < lstTicketTransactions.size(); i++) {
				TicketHistory objTicketHistory = new TicketHistory();

				if (lstTicketTransactions.get(i).getTicketStatus() != null) {
					if (lstTicketTransactions.get(i).getTicketStatus() == 0) {

					}

					// objTicketHistory.setUserName("");

					objTicketHistory.setTicketTransaction(lstTicketTransactions.get(i).getDescription());

					int assigned_id = lstTicketTransactions.get(i).getAssignedTo();
					if (assigned_id != 0) {
						objTicketHistory.setUserName(userService.getUserById(assigned_id).getUserName());
					}
					objTicketHistory.setTicketID(lstTicketTransactions.get(i).getTicketID().toString());

					if (lstTicketTransactions.get(i).getTimeStamp() != null) {
						objTicketHistory.setTicketDate(sdf.format(lstTicketTransactions.get(i).getTimeStamp()));
					} else {
						objTicketHistory.setTicketDate("");
					}

					if (lstTicketTransactions.get(i).getRemarks() != null) {
						objTicketHistory.setRemarks(lstTicketTransactions.get(i).getRemarks());
					}

					else {
						objTicketHistory.setRemarks("");
					}

					objTicketHistory.setSerialNo(String.valueOf(i + 1));

				}

				lstTicketHistories.add(objTicketHistory);
			}

		}

		if (objTicketdetail != null) {

			if (objTicketdetail.getDayCycle() != null) {
				objTicketdetail.setDayCycle(objTicketdetail.getDayCycle() - 1);
			}

			if (objTicketdetail.getCreationDate() != null) {
				String StrCreatedDate = sdf.format(objTicketdetail.getCreationDate());

				objTicketdetail.setCreatedDateText(StrCreatedDate);
			}

			if (objTicketdetail.getStartedTimeStamp() != null) {
				String StrStartedDate = sdf.format(objTicketdetail.getStartedTimeStamp());

				objTicketdetail.setStartedDateText(StrStartedDate);
			}

			if (objTicketdetail.getScheduledOn() != null) {
				String StrScheduledDate = sdfdate.format(objTicketdetail.getScheduledOn());

				objTicketdetail.setScheduledDateText(StrScheduledDate);
			}

			if (objTicketdetail.getClosedTimeStamp() != null) {
				String StrClosedDate = sdf.format(objTicketdetail.getClosedTimeStamp());

				objTicketdetail.setClosedDateText(StrClosedDate);
			}

			if (lstUsers.stream().filter(p -> p.getUserId().equals(objTicketdetail.getAssignedTo()))
					.collect(Collectors.toList()).size() > 0) {
				objTicketdetail.setAssignedToWhom(
						lstUsers.stream().filter(p -> p.getUserId().equals(objTicketdetail.getAssignedTo()))
								.collect(Collectors.toList()).get(0).getUserName());
			}

			if (lstUsers.stream().filter(p -> p.getUserId().equals(objTicketdetail.getCreatedBy()))
					.collect(Collectors.toList()).size() > 0) {
				objTicketdetail.setCreatedByName(
						lstUsers.stream().filter(p -> p.getUserId().equals(objTicketdetail.getCreatedBy()))
								.collect(Collectors.toList()).get(0).getUserName());
			}

			/*
			 * if(lstUsers.stream().filter(p ->
			 * p.getUserId()==objTicketdetail.getAssignedTo()).collect(Collectors.toList()).
			 * size()>0) { objTicketdetail.setAssignedToWhom(lstUsers.stream().filter(p ->
			 * p.getUserId()==objTicketdetail.getAssignedTo()).collect(Collectors.toList()).
			 * get(0).getUserName()); }
			 * 
			 * if(lstUsers.stream().filter(p ->
			 * p.getUserId()==objTicketdetail.getCreatedBy()).collect(Collectors.toList()).
			 * size()>0) { objTicketdetail.setCreatedByName(lstUsers.stream().filter(p ->
			 * p.getUserId()==objTicketdetail.getCreatedBy()).collect(Collectors.toList()).
			 * get(0).getUserName()); }
			 */
			if (lstUsers.stream().filter(p -> p.getUserId().equals(objTicketdetail.getClosedBy()))
					.collect(Collectors.toList()).size() > 0) {
				objTicketdetail.setClosedByName(
						lstUsers.stream().filter(p -> p.getUserId().equals(objTicketdetail.getClosedBy()))
								.collect(Collectors.toList()).get(0).getUserName());
			}
			if (lstSites.stream().filter(p -> p.getSiteId().equals(objTicketdetail.getSiteID()))
					.collect(Collectors.toList()).size() > 0) {
				objTicketdetail
						.setSiteName(lstSites.stream().filter(p -> p.getSiteId().equals(objTicketdetail.getSiteID()))
								.collect(Collectors.toList()).get(0).getSiteName());
				objTicketdetail
						.setSiteCode(lstSites.stream().filter(p -> p.getSiteId().equals(objTicketdetail.getSiteID()))
								.collect(Collectors.toList()).get(0).getCustomerNaming());

			}
			if (lstEquipment.stream().filter(p -> p.getEquipmentId().equals(objTicketdetail.getEquipmentID()))
					.collect(Collectors.toList()).size() >= 1) {
				objTicketdetail.setEquipmentName(
						lstEquipment.stream().filter(p -> p.getEquipmentId().equals(objTicketdetail.getEquipmentID()))
								.collect(Collectors.toList()).get(0).getCustomerNaming());

			}
			System.out.println(objTicketdetail.getClosedBy());
			System.out.println(objTicketdetail.getClosedByName());
			System.out.println(" Closed By" + objTicketdetail.getClosedByName());
			System.out.println(objTicketdetail.getSiteName());

			if (objTicketdetail.getPriority() != null) {
				if (objTicketdetail.getPriority() == 2) {
					objTicketdetail.setPriorityText("Medium");
				} else if (objTicketdetail.getPriority() == 3) {
					objTicketdetail.setPriorityText("High");
				} else {
					objTicketdetail.setPriorityText("Low");
				}
			} else {
				objTicketdetail.setPriorityText("Low");
			}

		}

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());
			objAccessListBean.setTicketid(refid);

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		objTicketdetail.setHoldRemarks("");
		objTicketdetail.setClosedRemarks("");
		objTicketdetail.setReassignRemarks("");
		objTicketdetail.setRemarks("");
		objTicketdetail.setTimeZone(timezonevalue);

		objTicketdetail.setScheduledOn(null);
		objTicketdetail.setAssignedTo(null);

		model.addAttribute("siteList", getDropdownList("Site", id));
		// model.addAttribute("userList", getDropdownList("User",id));

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());

		List<User> ddlFieldUserList = userService.listFieldUsers();
		model.addAttribute("userList", ddlFieldUserList);

		model.addAttribute("ticketcreation", new TicketDetail());
		// model.addAttribute("userList", getDropdownList("FieldUser",id));
		model.addAttribute("ticketview", objTicketdetail);
		model.addAttribute("tickethistorylist", lstTicketHistories);
		model.addAttribute("activeStatusList", getStatusDropdownList());

		return "ticketview";
	}

	/********************************************************************************************
	 * 
	 * Function Name : listTicketDetailsAssign
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function assigns a ticket.
	 * 
	 * Input Params  : refid,ticketdetail,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/assignticket{id}", method = RequestMethod.POST)
	public String listTicketDetailsAssign(@PathVariable("id") int refid,
			@ModelAttribute("ticketcreation") TicketDetail ticketdetail, HttpSession session,
			Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String timezonevalue = "0";
		if (!timezonevalue.equals("") && timezonevalue != null) {
			timezonevalue = session.getAttribute("timezonevalue").toString();
		}
		loginUserDetails.setTimezoneoffsetmin(timezonevalue);

		Date date = new Date();
		try {

			Date dateScheduled = new SimpleDateFormat("dd/MM/yyyy").parse(ticketdetail.getScheduledOnText());

			TicketDetail objTicketdetail = ticketdetailService.getTicketDetailById(refid, timezonevalue);
			objTicketdetail.setState(1);
			objTicketdetail.setTicketStatus(2);
			objTicketdetail.setNotificationStatus(0);
			objTicketdetail.setAssignedTo(ticketdetail.getAssignedTo());
			objTicketdetail.setRemarks(ticketdetail.getRemarks());
			objTicketdetail.setScheduledOn(dateScheduled);
			objTicketdetail.setLastUpdatedBy(id);
			objTicketdetail.setLastUpdatedDate(date);
			ticketdetailService.updateTicketDetail(objTicketdetail);

			System.out.println("mohan reached ");

			TicketTransaction tickettransaction = new TicketTransaction();

			tickettransaction.setTicketID(refid);
			tickettransaction.setTicketStatus(2);
			tickettransaction.setRemarks(ticketdetail.getRemarks());
			tickettransaction.setDescription("Ticket has been assigned");
			tickettransaction.setAssignedTo(ticketdetail.getAssignedTo());
			tickettransaction.setActiveFlag(1);
			tickettransaction.setTimeStamp(date);
			tickettransaction.setCreatedBy(id);
			tickettransaction.setCreationDate(date);
			tickettransaction.setDayCycle(ticketdetail.getDayCycle());
			ticketTransactionService.addTicketTransaction(tickettransaction);

		} catch (Exception ex) {

		}

		return "redirect:/ticketviews" + refid;
	}

	/********************************************************************************************
	 * 
	 * Function Name : listTicketDetailsReassign
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function reassigns a ticket.
	 * 
	 * Input Params  : refid,ticketdetail,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/reassignticket{id}", method = RequestMethod.POST)
	public String listTicketDetailsReassign(@PathVariable("id") int refid,
			@ModelAttribute("ticketcreation") TicketDetail ticketdetail, HttpSession session,
			Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String timezonevalue = "0";
		if (!timezonevalue.equals("") && timezonevalue != null) {
			timezonevalue = session.getAttribute("timezonevalue").toString();
		}
		loginUserDetails.setTimezoneoffsetmin(timezonevalue);

		Date date = new Date();
		try {
			Date dateRescheduled = new SimpleDateFormat("dd/MM/yyyy").parse(ticketdetail.getRescheduledOnText());

			TicketDetail objTicketdetail = ticketdetailService.getTicketDetailById(refid, timezonevalue);
			objTicketdetail.setTicketStatus(2);
			objTicketdetail.setState(1);
			objTicketdetail.setNotificationStatus(0);
			objTicketdetail.setRemarks(ticketdetail.getReassignRemarks());
			objTicketdetail.setLastUpdatedBy(id);
			objTicketdetail.setAssignedTo(ticketdetail.getReassignedTo());
			objTicketdetail.setScheduledOn(dateRescheduled);
			objTicketdetail.setLastUpdatedDate(date);
			ticketdetailService.updateTicketDetail(objTicketdetail);

			TicketTransaction tickettransaction = new TicketTransaction();
			tickettransaction.setTicketID(refid);
			tickettransaction.setTicketStatus(2);
			tickettransaction.setRemarks(ticketdetail.getReassignRemarks());
			tickettransaction.setDescription("Ticket has been reassigned");
			tickettransaction.setCreatedBy(id);
			tickettransaction.setAssignedTo(ticketdetail.getReassignedTo());
			tickettransaction.setActiveFlag(1);
			tickettransaction.setTimeStamp(date);
			tickettransaction.setCreationDate(date);
			tickettransaction.setDayCycle(ticketdetail.getDayCycle());
			ticketTransactionService.addTicketTransaction(tickettransaction);

		} catch (Exception ex) {
			System.out.println("Exception" + ex.getMessage());
		}

		return "redirect:/ticketviews" + refid;
	}

	/********************************************************************************************
	 * 
	 * Function Name : listTicketDetailsHold
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function puts ticket to hold.
	 * 
	 * Input Params  : refid,ticketdetail,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/holdticket{id}", method = RequestMethod.POST)
	public String listTicketDetailsHold(@PathVariable("id") int refid,
			@ModelAttribute("ticketcreation") TicketDetail ticketdetail, HttpSession session,
			Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String timezonevalue = "0";
		if (!timezonevalue.equals("") && timezonevalue != null) {
			timezonevalue = session.getAttribute("timezonevalue").toString();
		}
		loginUserDetails.setTimezoneoffsetmin(timezonevalue);

		Date date = new Date();
		try {

			TicketDetail objTicketdetail = ticketdetailService.getTicketDetailById(refid, timezonevalue);

			if (objTicketdetail.getTicketStatus() != 4 && objTicketdetail.getTicketStatus() != 5) {
				// objTicketdetail.setTicketStatus(7);
			}

			objTicketdetail.setState(3);
			objTicketdetail.setRemarks(ticketdetail.getHoldRemarks());
			// objTicketdetail.setClosedBy(id);
			objTicketdetail.setLastUpdatedBy(id);
			objTicketdetail.setLastUpdatedDate(date);
			ticketdetailService.updateTicketDetail(objTicketdetail);

			TicketTransaction tickettransaction = new TicketTransaction();
			tickettransaction.setTicketID(refid);
			tickettransaction.setTicketStatus(ticketdetail.getTicketStatus());
			// tickettransaction.setTicketStatus(7);
			tickettransaction.setRemarks(ticketdetail.getHoldRemarks());
			tickettransaction.setDescription("Ticket has been hold");
			tickettransaction.setCreatedBy(id);
			tickettransaction.setActiveFlag(1);
			tickettransaction.setTimeStamp(date);
			tickettransaction.setCreationDate(date);
			tickettransaction.setDayCycle(ticketdetail.getDayCycle());
			ticketTransactionService.addTicketTransaction(tickettransaction);

			// download_pdf("343");

		} catch (Exception ex) {

			System.out.println("Exception at Service Report : " + ex.getMessage());
		}

		return "redirect:/ticketviews" + refid;
	}

	/********************************************************************************************
	 * 
	 * Function Name : listTicketDetails
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function puts a ticket to closed state.
	 * 
	 * Input Params  : refid,ticketdetail,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/closeticket{id}", method = RequestMethod.POST)
	public String listTicketDetails(@PathVariable("id") int refid,
			@ModelAttribute("ticketcreation") TicketDetail ticketdetail, HttpSession session,
			Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String timezonevalue = "0";
		if (!timezonevalue.equals("") && timezonevalue != null) {
			timezonevalue = session.getAttribute("timezonevalue").toString();
		}
		loginUserDetails.setTimezoneoffsetmin(timezonevalue);

		Date date = new Date();
		try {

			TicketDetail objTicketdetail = ticketdetailService.getTicketDetailById(refid, timezonevalue);

			/*
			 * if(objTicketdetail.getTicketStatus() !=4 && objTicketdetail.getTicketStatus()
			 * !=5) { //objTicketdetail.setTicketStatus(6); }
			 */

			objTicketdetail.setState(2);
			System.out.println("1");
			objTicketdetail.setRemarks(ticketdetail.getClosedRemarks());
			System.out.println("2");
			objTicketdetail.setClosedBy(id);
			System.out.println("3");
			objTicketdetail.setClosedTimeStamp(date);
			objTicketdetail.setLastUpdatedBy(id);
			System.out.println("4");
			ticketdetailService.updateTicketDetail(objTicketdetail);

			System.out.println("5");

			TicketTransaction tickettransaction = new TicketTransaction();
			tickettransaction.setTicketID(refid);
			System.out.println("6");
			tickettransaction.setTicketStatus(ticketdetail.getTicketStatus());
			System.out.println("7");
			tickettransaction.setRemarks(ticketdetail.getClosedRemarks());
			System.out.println("8");
			tickettransaction.setDescription("Ticket has been closed");
			System.out.println("9");
			tickettransaction.setCreatedBy(id);
			System.out.println("10");
			tickettransaction.setActiveFlag(1);
			System.out.println("11");
			tickettransaction.setTimeStamp(date);
			System.out.println("12");
			tickettransaction.setCreationDate(date);
			System.out.println("13");
			tickettransaction.setDayCycle(ticketdetail.getDayCycle());
			ticketTransactionService.addTicketTransaction(tickettransaction);

			System.out.println("14");

		} catch (Exception ex) {
			System.out.println("Exception::::" + ex.getMessage());
		}

		return "redirect:/ticketviews" + refid;
	}

	/********************************************************************************************
	 * 
	 * Function Name : jmrdetails
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function updates jmr details.
	 * 
	 * Input Params  : jmrdetail,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/jmrdetails", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> jmrdetails(@RequestBody List<JmrDetail> jmrdetail, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			} else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				JmrDetail ticketfilter = jmrdetail.get(0);
				Date date = new Date();
				Date lmrtimestampvalue = new SimpleDateFormat("dd/MM/yyyy").parse(ticketfilter.getLmrTimestamptext());

				System.out.println(ticketfilter.getPlantDowntime());
				System.out.println(ticketfilter.getLmrValue());
				System.out.println(ticketfilter.getTicketid());
				System.out.println(lmrtimestampvalue);

				String plantdowntime = ticketfilter.getPlantDowntime();
				String lmrvalue = ticketfilter.getLmrValue();
				Integer ticketid = ticketfilter.getTicketid();
				ticketfilter.setLmrTimestamp(lmrtimestampvalue);

				JmrDetail lstJmrDetail = jmrdetailService.getticketID(ticketfilter.getTicketid());

				lstJmrDetail.setLmrValue(ticketfilter.getLmrValue());
				lstJmrDetail.setPlantDowntime(ticketfilter.getPlantDowntime());
				lstJmrDetail.setLmrTimestamp(lmrtimestampvalue);
				lstJmrDetail.setLastUpdatedDate(date);

				System.out.println("entered");

				jmrdetailService.updateJmrDetail(lstJmrDetail);

				TicketDetail ticketdetail = ticketdetailService.getTicketDetailById(ticketfilter.getTicketid(),
						timezonevalue);

				System.out.println("1 entered");

				ticketdetail.setUploadFlag(2);

				ticketdetail.setLastUpdatedDate(date);

				ticketdetailService.updateTicketDetail(ticketdetail);

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/*
	 * @RequestMapping(value = "/reportdownload{id}", method = RequestMethod.POST)
	 * 
	 * @RequestMapping(value ="/pdf/download/pmreport/{ticketid}/{daycycleid}",
	 * method = RequestMethod.POST) public String
	 * downloadservicereport_pdf(@PathVariable("ticketid") String
	 * ticketid,@PathVariable("daycycleid") String daycycleid) {
	 * 
	 * try { //return download_pdf(ticketid,""); } catch (IOException |
	 * DocumentException | JRException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); return ""; }
	 * 
	 * }
	 * 
	 */

	/*
	 * private Map map; private Connection con;
	 * 
	 * @RequestMapping(value ="/pdf/download/pmreport/{ticketid}/{daycycleid}",
	 * method = RequestMethod.POST) public String
	 * downloadservicereport_pdf(@PathVariable("ticketid") String
	 * ticketid,@PathVariable("daycycleid") String daycycleid) { String file_name =
	 * "E:/Report Template/ServiceReport.jrxml"; System.out.println(file_name);
	 * 
	 * SimpleDateFormat sdf = new SimpleDateFormat("_ddMMyyyy_HHmmss"); Date dt =
	 * new Date(); String currentdate =sdf.format(dt); String op="";
	 * 
	 * JasperPrint jasperPrint = null;
	 * 
	 * File path = new File(file_name);
	 * 
	 * 
	 * try { Class.forName(DBDriver); con =
	 * DriverManager.getConnection(DBConnection,DBUser,DBPassword); JasperReport
	 * jasperReport = JasperCompileManager.compileReport(path.toString()); map = new
	 * HashMap(); map.put("ticketid",ticketid); map.put("daycycleid",daycycleid);
	 * //map.put("ticketid",Integer.parseInt(ticketid));
	 * 
	 * 
	 * jasperPrint = JasperFillManager.fillReport(jasperReport,map, con);
	 * OutputStream output = new FileOutputStream(new File(PdfOutputPath +
	 * "ServiceReport" + currentdate + ".pdf"));
	 * JasperExportManager.exportReportToPdfStream(jasperPrint, output);
	 * 
	 * 
	 * 
	 * } catch (Exception e) { System.out.println("e--"+e.getMessage()); }
	 * 
	 * 
	 * op = pdfdownloadurl + "ServiceReport" + currentdate + ".pdf";
	 * 
	 * return op; }
	 */

	/*
	 * 
	 * private Map map1; private Connection con1;
	 * 
	 * @RequestMapping(value ="/pdf/download/pmreport/{ticketid}", method =
	 * RequestMethod.POST) public String
	 * downloadpmport_pdf(@PathVariable("ticketid") String ticketid) {
	 * 
	 * 
	 * String file_name = "E:/Report Template/PMReport.jrxml";
	 * System.out.println(file_name);
	 * 
	 * SimpleDateFormat sdf = new SimpleDateFormat("_ddMMyyyy_HHmmss"); Date dt =
	 * new Date(); String currentdate =sdf.format(dt); String op="";
	 * 
	 * JasperPrint jasperPrint = null;
	 * 
	 * File path = new File(file_name);
	 * 
	 * try { Class.forName(DBDriver); con1 =
	 * DriverManager.getConnection(DBConnection,DBUser,DBPassword); JasperReport
	 * jasperReport = JasperCompileManager.compileReport(path.toString()); map1 =
	 * new HashMap(); map1.put("ticketid",ticketid);
	 * //map.put("ticketid",Integer.parseInt(ticketid));
	 * 
	 * 
	 * 
	 * jasperPrint = JasperFillManager.fillReport(jasperReport,map1, con1);
	 * OutputStream output = new FileOutputStream(new File( PdfOutputPath +
	 * "PMReport" + currentdate + ".pdf"));
	 * JasperExportManager.exportReportToPdfStream(jasperPrint, output);
	 * 
	 * 
	 * 
	 * } catch (Exception e) { System.out.println("e--"+e.getMessage()); }
	 * 
	 * op = pdfdownloadurl + "PMReport" + currentdate + ".pdf"; return op; }
	 */

	/*
	 * private Map map; private Connection con; static ServletContext sc; public
	 * String download_pdf(String ticketid, String reportname) throws IOException,
	 * DocumentException, JRException {
	 * 
	 * String TemplatePath = "D:/Report Template/ServiceReport.jrxml";
	 * if(reportname.equals("PM")) { TemplatePath =
	 * "D:/Report Template/PMReport.jrxml"; }
	 * 
	 * File path = new File(TemplatePath); System.out.println(path); String op="";
	 * if(ticketid!=null && ticketid!=""){ JasperPrint jasperPrint = null; try {
	 * Class.forName("oracle.jdbc.driver.OracleDriver"); con =
	 * DriverManager.getConnection("jdbc:oracle:thin:@182.72.70.78:1521:eampmdb",
	 * "eampm","Eampm123#"); JasperReport jasperReport =
	 * JasperCompileManager.compileReport(path.toString()); map = new HashMap();
	 * map.put("ticketid",Integer.parseInt(ticketid));
	 * 
	 * jasperPrint = JasperFillManager.fillReport(jasperReport,map, con);
	 * OutputStream output = new FileOutputStream(new
	 * File("D:/Reports/PDF/ServiceReport.pdf"));
	 * JasperExportManager.exportReportToPdfStream(jasperPrint, output);
	 * 
	 * con.close();
	 * 
	 * } catch (Exception e) { System.out.println("e "+e.getMessage()); } op =
	 * "<mestech><response name='pdf' value='http://192.168.31.160/pdf/ServiceReport.pdf'/></mes>"
	 * ; } return op; }
	 * 
	 * 
	 * 
	 * 
	 */

}
