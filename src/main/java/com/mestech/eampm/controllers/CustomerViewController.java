
/******************************************************
 * 
 *    	Filename	: CustomerViewController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Controller deals with customer related activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.CustomerViewBean;
import com.mestech.eampm.bean.DashboardEnergySummaryBean;
import com.mestech.eampm.bean.DashboardOandMBean;
import com.mestech.eampm.bean.DashboardSiteBean;
import com.mestech.eampm.bean.DashboardSiteListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.bean.SiteListBean;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.CustomerTypeService;
import com.mestech.eampm.service.DataSourceService;
import com.mestech.eampm.service.EventDetailService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteStatusService;
import com.mestech.eampm.service.TicketDetailService;
import com.mestech.eampm.service.TicketTransactionService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.JsonMapData;
import com.mestech.eampm.utility.UtilityCommon;

@Controller
public class CustomerViewController {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private SiteStatusService siteStatusService;

	@Autowired
	private CustomerTypeService customerTypeService;

	@Autowired
	private TicketDetailService ticketDetailService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private TicketTransactionService ticketTransactionService;

	@Autowired
	private DataSourceService dataSourceService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	@Autowired
	private EventDetailService eventDetailService;

	private UtilityCommon utilityCommon = new UtilityCommon();

	/********************************************************************************************
	 * 
	 * Function Name : lstCustomerViews
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads customer view page.
	 * 
	 * Input Params : model,session,authentication
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : NumberFormatException,IndexOutOfBoundsException,Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customerview{id}", method = RequestMethod.GET)
	public String lstCustomerViews(@PathVariable("id") int custid, Model model, HttpSession session,
			Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		} else {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			String timezonevalue = "0";
			if (!timezonevalue.equals("") && timezonevalue != null) {
				timezonevalue = session.getAttribute("timezonevalue").toString();
			}
			loginUserDetails.setTimezoneoffsetmin(timezonevalue);

			CustomerViewBean objCustomerViewBean = new CustomerViewBean();

			List<SiteListBean> lstSiteList = new ArrayList<SiteListBean>();
			// List<SiteMapListBean> lstSiteMapList = new ArrayList<SiteMapListBean>();

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

			try {

				List<DashboardSiteBean> lstDashboardSiteDetails = siteService
						.getDashboardSiteDetailsByCustomer(String.valueOf(custid), timezonevalue);
				List<DashboardEnergySummaryBean> lstDashboardEnergySummaryDetails = siteService
						.getDashboardEnergySummaryDetailsByCustomer(String.valueOf(custid), timezonevalue);
				List<DashboardOandMBean> lstDashboardEventAndTicketDetails = siteService
						.getDashboardEventAndTicketDetailsByCustomer(String.valueOf(custid), timezonevalue);
				List<DashboardSiteListBean> lstDashboardSiteListDetails = siteService
						.getDashboardSiteListByCustomer(String.valueOf(custid), timezonevalue);

				DashboardSiteBean objDashboardSiteBean = null;
				DashboardEnergySummaryBean objDashboardEnergySummaryBean = null;
				DashboardOandMBean objDashboardOandMBean = null;

				if (lstDashboardSiteDetails.size() > 0) {
					for (Object object : lstDashboardSiteDetails) {
						Object[] obj = (Object[]) object;
						// objDashboardSiteBean = new DashboardSiteBean(totalSites, roofTopSites,
						// utilitySites, offlineStatus, activeStatus, warningStatus, downStatus);
						// objDashboardSiteBean = new
						// DashboardSiteBean(utilityCommon.StringFromBigDecimalObject(obj[0]),
						// utilityCommon.StringFromBigDecimalObject(obj[1]),utilityCommon.StringFromBigDecimalObject(obj[2]),utilityCommon.StringFromBigDecimalObject(obj[3]),utilityCommon.StringFromBigDecimalObject(obj[4]),utilityCommon.StringFromBigDecimalObject(obj[5]),utilityCommon.StringFromBigDecimalObject(obj[6]));
						objDashboardSiteBean = new DashboardSiteBean(
								utilityCommon.StringFromBigDecimalOrIntegerObject(obj[0]),
								utilityCommon.StringFromBigDecimalOrIntegerObject(obj[1]),
								utilityCommon.StringFromBigDecimalOrIntegerObject(obj[2]),
								utilityCommon.StringFromBigDecimalOrIntegerObject(obj[3]),
								utilityCommon.StringFromBigDecimalOrIntegerObject(obj[4]),
								utilityCommon.StringFromBigDecimalOrIntegerObject(obj[5]),
								utilityCommon.StringFromBigDecimalOrIntegerObject(obj[6]));
					}
				}

				if (lstDashboardEnergySummaryDetails.size() > 0) {
					for (Object object : lstDashboardEnergySummaryDetails) {
						Object[] obj = (Object[]) object;
						Double dblTotalValue = ((java.math.BigDecimal) obj[4]).doubleValue();
						Double dblTodayValue = ((java.math.BigDecimal) obj[5]).doubleValue();
						Double dblTodayHoursOn = ((java.math.BigDecimal) obj[6]).doubleValue();
						Double Co2 = ((java.math.BigDecimal) obj[7]).doubleValue();
						Double TotalCo2 = ((java.math.BigDecimal) obj[8]).doubleValue();

						// objDashboardEnergySummaryBean = new DashboardEnergySummaryBean(productionOn,
						// lastChecked, lastDataReceived, lastDownTime, totalEnergy, todayEnergy,
						// todayHoursOn);
						// objDashboardEnergySummaryBean = new
						// DashboardEnergySummaryBean(StringFromStringObject(obj[0]),StringFromStringObject(obj[1]),StringFromStringObject(obj[2]),StringFromStringObject(obj[3]),dblTotalValue,
						// dblTodayValue, dblTodayHoursOn);
						objDashboardEnergySummaryBean = new DashboardEnergySummaryBean(
								utilityCommon.StringFromStringObject(obj[0]),
								utilityCommon.StringFromStringObject(obj[1]),
								utilityCommon.StringFromStringObject(obj[2]),
								utilityCommon.StringFromStringObject(obj[3]), dblTotalValue, dblTodayValue,
								dblTodayHoursOn, Co2, TotalCo2);

					}
				}

				if (lstDashboardEventAndTicketDetails.size() > 0) {
					for (Object object : lstDashboardEventAndTicketDetails) {
						Object[] obj = (Object[]) object;
						// objDashboardOandMBean =new DashboardOandMBean(totalTicket, openTicket,
						// closedTicket, totalEvent, todayEvent)
						// objDashboardOandMBean =new
						// DashboardOandMBean(StringFromBigDecimalObject(obj[0]),StringFromBigDecimalObject(obj[1]),StringFromBigDecimalObject(obj[2]),StringFromBigDecimalObject(obj[3]),StringFromBigDecimalObject(obj[4]));
						objDashboardOandMBean = new DashboardOandMBean(
								utilityCommon.StringFromBigDecimalOrIntegerObject(obj[0]),
								utilityCommon.StringFromBigDecimalOrIntegerObject(obj[1]),
								utilityCommon.StringFromBigDecimalOrIntegerObject(obj[2]),
								utilityCommon.StringFromBigDecimalOrIntegerObject(obj[3]),
								utilityCommon.StringFromBigDecimalOrIntegerObject(obj[4]));
					}
				}

				String MapJsonData = "";
				if (lstDashboardSiteListDetails.size() > 0) {
					Integer a = 0;

					String SiteID = "";
					String SiteTypeID = "";
					String SiteCode = "";
					String SiteRef = "";
					String SiteName = "";
					String Lattitude = "";
					String Longtitude = "";
					String Address = "";
					String City = "";
					String State = "";
					String Country = "";
					String PostalCode = "";
					String ActiveInverterCount = "";
					String ActiveEMCount = "";
					String EMCount = "";
					String InverterCount = "";
					String LastChecked = "";
					Double TotalEnergy = 0.0;
					Double TodayEnergy = 0.0;
					String LastDownTime = "";
					String LastDataReceived = "";
					Double TodayHoursOn = 0.0;
					String Status = "";
					String Inverters = "";
					String EMs = "";
					Double InstallationCapacity = 0.0;
					String SpecificYield = "-";
					List<JsonMapData> jsonMapDatas = new ArrayList<>();
					for (Object object : lstDashboardSiteListDetails) {
						Object[] obj = (Object[]) object;

						SiteID = utilityCommon.StringFromBigDecimalOrIntegerObject(obj[0]);
						SiteTypeID = obj[1].toString();
						SiteCode = utilityCommon.StringFromStringObject(obj[2]);
						SiteRef = utilityCommon.StringFromStringObject(obj[3]);
						SiteName = utilityCommon.StringFromStringObject(obj[4]);
						Lattitude = utilityCommon.StringFromStringObject(obj[5]);
						Longtitude = utilityCommon.StringFromStringObject(obj[6]);
						Address = utilityCommon.StringFromStringObject(obj[7]);
						City = utilityCommon.StringFromStringObject(obj[8]);
						State = utilityCommon.StringFromStringObject(obj[9]);
						Country = utilityCommon.StringFromStringObject(obj[10]);
						PostalCode = utilityCommon.StringFromStringObject(obj[11]);
						ActiveInverterCount = obj[12].toString();
						InverterCount = obj[13].toString();
						LastChecked = utilityCommon.StringFromStringObject(obj[14]);
						TotalEnergy = utilityCommon.DoubleFromBigDecimalObject(obj[15]);
						TodayEnergy = utilityCommon.DoubleFromBigDecimalObject(obj[16]);
						LastDownTime = utilityCommon.StringFromStringObject(obj[17]);
						LastDataReceived = utilityCommon.StringFromStringObject(obj[18]);
						TodayHoursOn = utilityCommon.DoubleFromBigDecimalObject(obj[19]);
						Status = utilityCommon.StringFromStringObject(obj[20]);
						InstallationCapacity = utilityCommon.DoubleFromBigDecimalObject(obj[21]);
						ActiveEMCount = obj[26].toString();
						EMCount = obj[27].toString();

						if (EMCount != null && EMCount != "0" && EMCount != "") {
							EMs = ActiveEMCount + " / " + EMCount;
						} else {
							EMs = "";
						}

						if (InverterCount != null && InverterCount != "0" && InverterCount != "") {
							Inverters = ActiveInverterCount + " / " + InverterCount;
						} else {
							Inverters = "";
						}

						Double dblSY = (((TodayEnergy) / (InstallationCapacity)));

						if (dblSY < 7.0) {
							SpecificYield = String.format("%.2f", dblSY);
						} else {
							SpecificYield = "-";
						}

						SiteListBean objSiteListBean = new SiteListBean();
						objSiteListBean.setInverters(Inverters);
						objSiteListBean.setEms(EMs);
						objSiteListBean.setLastDownTime(LastDownTime);
						objSiteListBean.setLastUpdate(LastDataReceived);
						objSiteListBean.setNetworkStatus(Status);
						objSiteListBean.setSiteTypeID(SiteTypeID);
						objSiteListBean.setPerformanceRatio(SpecificYield);
						objSiteListBean.setSiteCode(SiteCode);
						objSiteListBean.setSiteID(SiteID);
						objSiteListBean.setSiteName(SiteName);
						objSiteListBean.setSiteReference(SiteRef);
						objSiteListBean.setTodayEnergy(String.format("%.2f", TodayEnergy));
						objSiteListBean.setTotalEnergy(String.format("%.2f", TotalEnergy / 1000));
						objSiteListBean.setInstallationCapacity(InstallationCapacity);

						lstSiteList.add(objSiteListBean);

						String varZIndex = "99996";
						if (Status.equals("Active")) {
							varZIndex = "99997";
						} else if (Status.equals("Warning")) {
							varZIndex = "99998";
						} else if (Status.equals("Down")) {
							varZIndex = "99999";
						} else {
							varZIndex = "99996";
						}

						JsonMapData jsonMapData = new JsonMapData(
								"<b>" + SiteName + " - " + SiteRef + "</b><p>" + Address + "</p><p>" + City + " - "
										+ PostalCode + ".</p>",
								Lattitude, Longtitude, SiteName, Status.toLowerCase(), new Integer(varZIndex),
								"/eampm-maven/siteview" + SiteID + "&ref=" + custid);
						jsonMapDatas.add(jsonMapData);
//						if (a == 0) {
//							MapJsonData = "['<b>" + SiteName + " - " + SiteRef + "</b><p>" + Address + "</p><p>" + City
//									+ " - " + PostalCode + ".</p>'," + Lattitude + ", " + Longtitude + ",'" + SiteName
//									+ "'," + Status.toLowerCase() + "," + varZIndex + ",'/eampm-maven/siteview" + SiteID
//									+ "&ref=" + custid + "']";
//						} else {
//							MapJsonData = MapJsonData + ",['<b>" + SiteName + " - " + SiteRef + "</b><p>" + Address
//									+ "</p><p>" + City + " - " + PostalCode + ".</p>'," + Lattitude + ", " + Longtitude
//									+ ",'" + SiteName + "'," + Status.toLowerCase() + "," + varZIndex
//									+ ",'/eampm-maven/siteview" + SiteID + "&ref=" + custid + "']";
//						}
//
//						a = a + 1;
					}
					if (jsonMapDatas != null && !jsonMapDatas.isEmpty()) {

						Gson gson = new Gson();
						String jsonData = gson.toJson(jsonMapDatas);
						jsonData = jsonData.replaceAll("\"", "\'");
						model.addAttribute("mapData", jsonData);
					}
				}

				String varStrTotalSiteCount = "0";
				String varStrRooftopSiteCount = "0";
				String varStrUtilitySiteCount = "0";
				String varStrRooftopUtilitySiteCount = "0";
				String varStrOfflineSiteCount = "0";
				String varStrActiveSiteCount = "0";
				String varStrWarningSiteCount = "0";
				String varStrDownSiteCount = "0";

				String varStrTotalEvents = "0";
				String varStrTodayEvents = "0";
				String varStrTotalOpenTicket = "0";
				String varStrTotalClosedTicket = "0";
				String varStrTotalHoldTicket = "0";
				String varStrTotalCompletedTicket = "0";

				String varStrProductionOn = "";
				String varStrLastChecked = "";
				String varStrLastDataReceived = "";
				String varStrLastDownTime = "";
				Double varDblTotalEnergy = 0.0;
				Double varDblTodayEnergy = 0.0;
				Double varDblTodayHoursOn = 0.0;
				Double Co2Avioded = 0.0;
				Double TotalCo2Avioded = 0.0;
				String varStrTotalEnergyUOM = " kWh";
				String varStrTodayEnergyUOM = " kWh";
				String varStrTodayHoursOnUOM = " hours";

				if (objDashboardSiteBean != null) {
					varStrTotalSiteCount = objDashboardSiteBean.getTotalSites();
					varStrRooftopSiteCount = objDashboardSiteBean.getRoofTopSites();
					varStrUtilitySiteCount = objDashboardSiteBean.getUtilitySites();

					varStrOfflineSiteCount = objDashboardSiteBean.getOfflineStatus();
					varStrActiveSiteCount = objDashboardSiteBean.getActiveStatus();
					varStrWarningSiteCount = objDashboardSiteBean.getWarningStatus();
					varStrDownSiteCount = objDashboardSiteBean.getDownStatus();
				}

				if (objDashboardEnergySummaryBean != null) {
					varStrProductionOn = objDashboardEnergySummaryBean.getProductionOn();
					varStrLastChecked = objDashboardEnergySummaryBean.getLastChecked();
					varStrLastDataReceived = objDashboardEnergySummaryBean.getLastDataReceived();
					varStrLastDownTime = objDashboardEnergySummaryBean.getLastDownTime();
					varDblTotalEnergy = objDashboardEnergySummaryBean.getTotalEnergy();
					varDblTodayEnergy = objDashboardEnergySummaryBean.getTodayEnergy();
					varDblTodayHoursOn = objDashboardEnergySummaryBean.getTodayHoursOn();
					Co2Avioded = objDashboardEnergySummaryBean.getCo2();
					TotalCo2Avioded = objDashboardEnergySummaryBean.getTotalCo2();

				}

				if (objDashboardOandMBean != null) {
					varStrTotalEvents = objDashboardOandMBean.getTotalEvent();
					varStrTodayEvents = objDashboardOandMBean.getTodayEvent();

					varStrTotalOpenTicket = objDashboardOandMBean.getOpenTicket();
					varStrTotalClosedTicket = objDashboardOandMBean.getClosedTicket();

				}

				objCustomerViewBean.setTotalSiteCount(varStrTotalSiteCount);
				objCustomerViewBean.setRooftopCount(varStrRooftopSiteCount);
				objCustomerViewBean.setUtilityCount(varStrUtilitySiteCount);
				objCustomerViewBean.setRooftopUtilityCount(varStrRooftopUtilitySiteCount);
				objCustomerViewBean.setActiveSiteCount(varStrActiveSiteCount);
				objCustomerViewBean.setWarningSiteCount(varStrWarningSiteCount);
				objCustomerViewBean.setDownSiteCount(varStrDownSiteCount);
				objCustomerViewBean.setOfflineSiteCount(varStrOfflineSiteCount);

				if (varDblTodayEnergy >= 1000000) {
					varDblTodayEnergy = varDblTodayEnergy / 1000000;
					varStrTodayEnergyUOM = " GWh";

				} else if (varDblTodayEnergy >= 1000) {
					varDblTodayEnergy = varDblTodayEnergy / 1000;
					varStrTodayEnergyUOM = " MWh";

				} else if (varDblTodayEnergy < 1) {
					varDblTodayEnergy = varDblTodayEnergy * 1000;
					varStrTodayEnergyUOM = " Wh";

				}

				if (varDblTotalEnergy >= 1000000) {
					varDblTotalEnergy = varDblTotalEnergy / 1000000;
					varStrTotalEnergyUOM = " GWh";

				} else if (varDblTotalEnergy >= 1000) {
					varDblTotalEnergy = varDblTotalEnergy / 1000;
					varStrTotalEnergyUOM = " MWh";

				} else if (varDblTotalEnergy < 1) {
					varDblTotalEnergy = varDblTotalEnergy * 1000;
					varStrTotalEnergyUOM = " Wh";

				}

				objCustomerViewBean.setTodayEnergy(String.format("%.2f", varDblTodayEnergy) + varStrTodayEnergyUOM);
				objCustomerViewBean.setTotalEnergy(String.format("%.2f", varDblTotalEnergy) + varStrTotalEnergyUOM);
				objCustomerViewBean.setTotalDownTime("");
				objCustomerViewBean.setProductionDate(varStrProductionOn);
				objCustomerViewBean.setLastUpdatedTime(varStrLastChecked);
				objCustomerViewBean.setEventTime(varStrLastDataReceived);
				objCustomerViewBean.setLastDownTime(varStrLastDownTime);

				objCustomerViewBean.setOpenTicketCount(varStrTotalOpenTicket);
				objCustomerViewBean.setYettoStartCount(varStrTotalOpenTicket);
				objCustomerViewBean.setCompletedTicketCount(varStrTotalClosedTicket);
				objCustomerViewBean.setTicketMessages("");
				objCustomerViewBean.setInprocessCount("");
				objCustomerViewBean.setTodayEventCount(varStrTodayEvents);
				objCustomerViewBean.setTotalEventCount(varStrTotalEvents);

				objCustomerViewBean.setTodayCo2Avoided(String.format("%.2f", Co2Avioded) + " T");
				objCustomerViewBean.setTotalCo2Avoided(String.format("%.2f", TotalCo2Avioded) + " T");
				objCustomerViewBean.setTotalProductionYield("");
				objCustomerViewBean.setTodayProductionYield("");
				objCustomerViewBean.setTotalPerformanceRatio("");
				objCustomerViewBean.setTodayPerformanceRatio("");

				MapJsonData = "[" + MapJsonData + "]";

				objCustomerViewBean.setMapJsonData(MapJsonData);
				objCustomerViewBean.setUserID(id);

			} catch (NumberFormatException ex) {
				return "redirect:/login";
			} catch (IndexOutOfBoundsException ex) {
				return "redirect:/login";
			} catch (Exception ex) {
				return "redirect:/login";
			}

			AccessListBean objAccessListBean = new AccessListBean();
			List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
			List<String> objactivitys = new ArrayList<>();

			StringBuilder stringBuilder = new StringBuilder();

			try {
				User objUser = userService.getUserById(id);

				// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

				lstRoleActivity = roleActivityService.listRoleActivities().stream()
						.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

				objAccessListBean.setUserID(id);
				objAccessListBean.setUserName(objUser.getShortName());
				objAccessListBean.setMyCustomerID(custid);

				if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
					objAccessListBean.setCustomerListView("visible");
				}
				if (objUser.getRoleID() == 6) {
					objAccessListBean.setMonitoringView("visible");
				}

				for (int l = 0; l < lstRoleActivity.size(); l++) {
					if (lstRoleActivity.get(l).getActiveFlag() == 1) {
						Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
						if (null != activity) {
							if (activity.getActivityDescription() != null) {
								stringBuilder.append(activity.getActivityDescription());
								stringBuilder.append("#");
							}
						}
					}

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			model.addAttribute("siteList", getNewTicketSiteList("Site", custid));
			model.addAttribute("classtate", "active");
			model.addAttribute("access", objAccessListBean);
			model.addAttribute("accessPages", stringBuilder.toString());
			model.addAttribute("customerview", objCustomerViewBean);
			model.addAttribute("customerviewSiteList", lstSiteList);
			model.addAttribute("ticketcreation", new TicketDetail());
			return "customerview";

		}
	}

	/********************************************************************************************
	 * 
	 * Function Name : DashboardSiteDetail
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns customer details.
	 * 
	 * Input Params : custid,session,authentication
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customerviewsitedetail{id}", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> DashboardSiteDetail(@PathVariable("id") int custid, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", false);
				objResponseEntity.put("customerviewSiteList", null);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			} else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				// int userid = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<DashboardSiteListBean> lstDashboardSiteListDetails = siteService
						.getDashboardSiteListByCustomer(String.valueOf(custid), timezonevalue);

				List<SiteListBean> lstSiteList = new ArrayList<SiteListBean>();

				if (lstDashboardSiteListDetails.size() > 0) {
					Integer a = 0;

					String SiteID = "";
					String SiteTypeID = "";
					String SiteCode = "";
					String SiteRef = "";
					String SiteName = "";
					String Lattitude = "";
					String Longtitude = "";
					String Address = "";
					String City = "";
					String State = "";
					String Country = "";
					String PostalCode = "";
					String ActiveInverterCount = "";
					String EMCount = "";
					String ActiveEMCount = "";
					String InverterCount = "";
					String LastChecked = "";
					Double TotalEnergy = 0.0;
					Double TodayEnergy = 0.0;
					String LastDownTime = "";
					String LastDataReceived = "";
					Double TodayHoursOn = 0.0;
					String Status = "";
					String Inverters = "";
					String EMs = "";
					Double InstallationCapacity = 0.0;
					String SpecificYield = "-";

					for (Object object : lstDashboardSiteListDetails) {
						Object[] obj = (Object[]) object;

						SiteID = utilityCommon.StringFromBigDecimalOrIntegerObject(obj[0]);
						SiteTypeID = obj[1].toString();
						SiteCode = utilityCommon.StringFromStringObject(obj[2]);
						SiteRef = utilityCommon.StringFromStringObject(obj[3]);
						SiteName = utilityCommon.StringFromStringObject(obj[4]);
						Lattitude = utilityCommon.StringFromStringObject(obj[5]);
						Longtitude = utilityCommon.StringFromStringObject(obj[6]);
						Address = utilityCommon.StringFromStringObject(obj[7]);
						City = utilityCommon.StringFromStringObject(obj[8]);
						State = utilityCommon.StringFromStringObject(obj[9]);
						Country = utilityCommon.StringFromStringObject(obj[10]);
						PostalCode = utilityCommon.StringFromStringObject(obj[11]);
						ActiveInverterCount = obj[12].toString();
						InverterCount = obj[13].toString();
						LastChecked = utilityCommon.StringFromStringObject(obj[14]);
						TotalEnergy = utilityCommon.DoubleFromBigDecimalObject(obj[15]);
						TodayEnergy = utilityCommon.DoubleFromBigDecimalObject(obj[16]);
						LastDownTime = utilityCommon.StringFromStringObject(obj[17]);
						LastDataReceived = utilityCommon.StringFromStringObject(obj[18]);
						TodayHoursOn = utilityCommon.DoubleFromBigDecimalObject(obj[19]);
						Status = utilityCommon.StringFromStringObject(obj[20]);
						InstallationCapacity = utilityCommon.DoubleFromBigDecimalObject(obj[21]);
						ActiveEMCount = obj[26].toString();
						EMCount = obj[27].toString();

						if (EMCount != null && EMCount != "0" && EMCount != "") {
							EMs = ActiveEMCount + " / " + EMCount;
						} else {
							EMs = "";
						}

						if (InverterCount != null && InverterCount != "0" && InverterCount != "") {
							Inverters = ActiveInverterCount + " / " + InverterCount;
						} else {
							Inverters = "";
						}

						Double dblSY = (((TodayEnergy) / (InstallationCapacity)));

						if (dblSY < 7.0) {
							SpecificYield = String.format("%.2f", dblSY);
						} else {
							SpecificYield = "-";
						}

						SiteListBean objSiteListBean = new SiteListBean();
						objSiteListBean.setInverters(Inverters);
						objSiteListBean.setEms(EMs);
						objSiteListBean.setLastDownTime(LastDownTime);
						objSiteListBean.setLastUpdate(LastDataReceived);
						objSiteListBean.setNetworkStatus(Status);
						objSiteListBean.setPerformanceRatio(SpecificYield);
						objSiteListBean.setSiteCode(SiteCode);
						objSiteListBean.setSiteTypeID(SiteTypeID);
						objSiteListBean.setSiteID(SiteID);
						objSiteListBean.setSiteName(SiteName);
						objSiteListBean.setSiteReference(SiteRef);
						objSiteListBean.setTodayEnergy(String.format("%.2f", TodayEnergy));
						objSiteListBean.setTotalEnergy(String.format("%.2f", TotalEnergy / 1000));
						objSiteListBean.setInstallationCapacity(InstallationCapacity);

						lstSiteList.add(objSiteListBean);

						a = a + 1;

					}
				}

				objResponseEntity.put("customerviewSiteList", lstSiteList);
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("customerviewSiteList", null);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns value for drop down list.
	 * 
	 * Input Params : DropdownName,refid
	 * 
	 * Return Value : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns value for drop down list.
	 * 
	 * Input Params : DropdownName,refid
	 * 
	 * Return Value : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "User") {
			List<User> ddlList = userService.listFieldUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		}

		return ddlMap;
	}

}
