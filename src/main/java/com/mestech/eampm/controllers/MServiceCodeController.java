
/******************************************************
 * 
 *    	Filename	: MServiceCodeController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Service Code Activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.service.MServiceCodeService;
import com.mestech.eampm.utility.Response;

@Controller
public class MServiceCodeController {
	@Autowired
	private MServiceCodeService mServiceCodeService;

	/********************************************************************************************
	 * 
	 * Function Name : getAllServiceCodes
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This method returns all service codes.
	 * 
	 * Return Value  : Response
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getAllServiceCodes", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Response getAllServiceCodes() {
		Response response = new Response();
		response.setData(mServiceCodeService.listAll());
		response.setStatus("Success");
		return response;

	}

}
