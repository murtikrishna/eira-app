
/******************************************************
 * 
 *    	Filename	: TimeZoneController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Timezone related Activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.TimeZone;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.TimeZoneService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class TimeZoneController {

	@Autowired
	private TimeZoneService timezoneService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown.
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : listTimeZones
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads timezone page.
	 * 
	 * Input Params  : model,session
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/timezones", method = RequestMethod.GET)
	public String listTimeZones(Model model, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());

		AccessListBean objAccessListBean = new AccessListBean();

		try {
			User objUser = userService.getUserById(id);
			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
			List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getUserName());

			if (objUser.getRoleID() != 4) {
				objAccessListBean.setCustomerListView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActivityID() == 1 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setDashboard("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 2 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setOverview("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 3 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setSystemMonitoring("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 4 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setVisualization("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 5 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalytics("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 6 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setPortfolioManagement("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 7 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setTicketing("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 8 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setForcasting("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 9 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setConfiguration("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 10
						&& lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalysis("visible");
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("access", objAccessListBean);

		model.addAttribute("timezone", new TimeZone());
		model.addAttribute("timezoneList", timezoneService.listTimeZones());
		model.addAttribute("activeStatusList", getStatusDropdownList());

		return "timezone";
	}

	/********************************************************************************************
	 * 
	 * Function Name : addtimezone
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function adds a timezone.
	 * 
	 * Input Params  : timezone,session
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// Same method For both Add and Update TimeZone
	// @RequestMapping(value = "/timezone/add", method = RequestMethod.POST)
	@RequestMapping(value = "/addnewtimezone", method = RequestMethod.POST)
	public String addtimezone(@ModelAttribute("timezone") TimeZone timezone, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		Date date = new Date();

		if (timezone.getTimeZoneId() == null || timezone.getTimeZoneId() == 0) {
			// new timezone, add it
			timezone.setCreationDate(date);
			timezoneService.addTimeZone(timezone);
		} else {
			// existing timezone, call update
			timezone.setLastUpdatedDate(date);
			timezoneService.updateTimeZone(timezone);
		}

		return "redirect:/timezones";

	}

	/********************************************************************************************
	 * 
	 * Function Name : removetimezone
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function removes a timezone.
	 * 
	 * Input Params  : id,session
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// @RequestMapping("/timezone/remove/{id}")
	@RequestMapping("/removetimezone{id}")
	public String removetimezone(@PathVariable("id") int id, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		timezoneService.removeTimeZone(id);
		return "redirect:/timezones";
	}

	/********************************************************************************************
	 * 
	 * Function Name : edittimezone
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function updates a timezone.
	 * 
	 * Input Params  : id,model,session
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// @RequestMapping("/timezone/edit/{id}")
	@RequestMapping("/edittimezone{id}")
	public String edittimezone(@PathVariable("id") int id, Model model, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		model.addAttribute("timezone", timezoneService.getTimeZoneById(id));
		model.addAttribute("timezoneList", timezoneService.listTimeZones());
		model.addAttribute("activeStatusList", getStatusDropdownList());
		return "timezone";
	}
}
