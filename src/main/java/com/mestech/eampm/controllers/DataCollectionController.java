
/******************************************************
 * 
 *    	Filename	: DataCollectionController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Controller deals with datacollection activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TimeZone;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.TimeZoneService;
import com.mestech.eampm.utility.DataCollection;
import com.mestech.eampm.utility.OperationMode;
import com.mestech.eampm.utility.Response;

@Controller
@RequestMapping(value = "/dataCollection")
public class DataCollectionController {
	@Autowired
	private SiteService siteService;
	@Autowired
	private TimeZoneService timeZoneService;

	/********************************************************************************************
	 * 
	 * Function Name : listAllSites
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function return all sites.
	 * 
	 * Return Value : List<Site>
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getAllSites", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Site> listAllSites() {
		List<Site> sites = siteService.listSitesAgainstCollection();
		if (sites != null && !sites.isEmpty()) {
			Collections.sort(sites, new Comparator<Site>() {
				@Override
				public int compare(Site site1, Site site2) {

					if (((site1.getLastUpdatedDate() != null) ? site1.getLastUpdatedDate().getTime()
							: new Date().getTime()) < ((site2.getLastUpdatedDate() != null)
									? site2.getLastUpdatedDate().getTime()
									: new Date().getTime())) {
						return 1;
					} else if ((site1.getLastUpdatedDate() != null ? site1.getLastUpdatedDate().getTime()
							: new Date().getTime()) > (site2.getLastUpdatedDate() != null
									? site2.getLastUpdatedDate().getTime()
									: new Date().getTime())) {
						return -1;
					}

					return 0;
				}
			});
		}
		return sites;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getAllTimeZone
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns all timezones.
	 * 
	 * Return Value : List<String>
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getAllTimeZone", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<String> getAllTimeZone() {
		List<String> timeZonesString = new ArrayList<>();
		List<TimeZone> timeZones = timeZoneService.listTimeZones();
		if (timeZones != null && !timeZones.isEmpty()) {
			for (TimeZone timeZone : timeZones) {
				timeZonesString.add("(" + timeZone.getUtcOffset() + ")" + timeZone.getTimeZoneName());
			}
		}
		return timeZonesString;
	}

	/********************************************************************************************
	 * 
	 * Function Name : findTimeZoneFormat
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This funciton finds timezone format.
	 * 
	 * Input Params : timeZone
	 * 
	 * Return Value : String
	 * 
	 * 
	 **********************************************************************************************/
	public String findTimeZoneFormat(String timeZone) {
		String utcFormat = timeZone.split("\\)")[0];
		utcFormat = utcFormat.replaceAll("\\(", "");
		String timeZoneName = timeZone.split("\\)")[1];
		TimeZone timeZone2 = timeZoneService.findByUTCANDName(utcFormat, timeZoneName);
		if (timeZone2 != null) {
			String name = timeZone2.getUtcOffset();
			Integer offSetInMin = timeZone2.getTimeZoneOffSetInMin();
			if (name != null && offSetInMin != null)
				return name.concat("#").concat(offSetInMin.toString());
			else
				return null;
		}
		return null;
	}

	/********************************************************************************************
	 * 
	 * Function Name : saveConfiguration
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function saves a configuration
	 * 
	 * Input Params : dataCollection
	 * 
	 * Return Value : Response
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/saveConfiguration", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Response saveConfiguration(@RequestBody DataCollection dataCollection) {
		Response response = new Response();
		try {
			if (dataCollection.getSiteId() != null && dataCollection.getSiteId().intValue() != 0) {
				Site site = siteService.getSiteById(dataCollection.getSiteId());
				if (site != null) {
					site.setEquipmentFlag(dataCollection.getEquipmentFlag());
					site.setDataloggertypeid(dataCollection.getDataloggertypeid());
					site.setTodayEnergyFlag(dataCollection.getTodayEnergyFlag());
					site.setCollectionType(dataCollection.getCollectionType());
					site.setProdFlag(dataCollection.getProdFlag());
					site.setServiceFlag2(dataCollection.getServiceFlag2());
					site.setServiceFlag3(dataCollection.getServiceFlag3());
					site.setServiceFlag4(dataCollection.getServiceFlag4());
					String timeZone = dataCollection.getTimezone();
					site.setProdFlag(dataCollection.getProdFlag());
					String timeZones[] = timeZone.split("\\)");
					String Utc = timeZones[0].replaceAll("\\(", "").trim();
					String countryName = timeZones[1].trim();
					site.setTimezone(dataCollection.getTimezone());
					TimeZone timeZon = timeZoneService.findByUTCANDName(Utc, countryName);
					if (timeZon != null && timeZon.getTimeZoneOffSetInMin() != null)
						site.setTimeZoneOffSetMin(new BigInteger(timeZon.getTimeZoneOffSetInMin().toString()));
					if (OperationMode.FTP.getType().equalsIgnoreCase(dataCollection.getCollectionType())) {
						site.setLocalFtpUserName(dataCollection.getLocalFtpUserName());
						site.setLocalFtpPassword(dataCollection.getLocalFtpPassword());
						site.setLocalFtpHomeDirectory(dataCollection.getLocalFtpHomeDirectory());
						site.setFileType(dataCollection.getFileType());
						site.setOperationMode(new Integer(1));

						if (dataCollection.getLocalFtpDirectory_Inverter() != null
								&& dataCollection.getLocalFtpHomeDirectory() != null
								&& dataCollection.getLocalFtpDirectoryPath_Inverter() != null) {
							site.setLocalFtpDirectory_Inverter(dataCollection.getLocalFtpDirectory_Inverter());
							site.setLocalFtpDirectoryPath_Inverter(dataCollection.getLocalFtpHomeDirectory()
									+ dataCollection.getLocalFtpDirectoryPath_Inverter());
						} else {
							site.setLocalFtpDirectory_Inverter(null);
							site.setLocalFtpDirectoryPath_Inverter(null);
						}

						site.setDataLoggerID_Inverter(dataCollection.getDataLoggerIdInverter());
						site.setServiceCode_Inverter(dataCollection.getServiceCodeInverter());

						if (dataCollection.getLocalFtpDirectory_Modbus() != null
								&& dataCollection.getLocalFtpHomeDirectory() != null
								&& dataCollection.getLocalFtpDirectoryPath_Modbus() != null) {
							site.setLocalFtpDirectory_Modbus(dataCollection.getLocalFtpDirectory_Modbus());
							site.setLocalFtpDirectoryPath_Modbus(dataCollection.getLocalFtpHomeDirectory()
									+ dataCollection.getLocalFtpDirectoryPath_Modbus());
						} else {
							site.setLocalFtpDirectory_Modbus(null);
							site.setLocalFtpDirectoryPath_Modbus(null);
						}

						site.setDataLoggerID_Modbus(dataCollection.getDataLoggerIdModBus());
						site.setServiceCode_Modbus(dataCollection.getServiceCodeModeBus());

						if (dataCollection.getLocalFtpDirectory_Sensor() != null
								&& dataCollection.getLocalFtpHomeDirectory() != null
								&& dataCollection.getLocalFtpDirectoryPath_Sensor() != null) {
							site.setLocalFtpDirectory_Sensor(dataCollection.getLocalFtpDirectory_Sensor());
							site.setLocalFtpDirectoryPath_Sensor(dataCollection.getLocalFtpHomeDirectory()
									+ dataCollection.getLocalFtpDirectoryPath_Sensor());
						} else {
							site.setLocalFtpDirectory_Sensor(null);
							site.setLocalFtpDirectoryPath_Sensor(null);
						}
						site.setDataLoggerID_Sensor(dataCollection.getDataLoggerIdSensor());
						site.setServiceCode_Sensor(dataCollection.getServiceCodeSensor());

						site.setServiceCode_SCB(dataCollection.getServiceCodeScb());
						site.setServiceCode_TRACKER(dataCollection.getServiceCodeTracker());
						site.setDataLoggerID_SCB(dataCollection.getDataLoggerIdScb());
						site.setDataLoggerID_TRACKER(dataCollection.getDataLoggerIdTracker());
						site.setDataLoggerID_ENERGYMETER(dataCollection.getDataLoggerIdEnergyMeter());
						site.setServiceCode_ENERGYMETER(dataCollection.getServiceCodeEnergyMeter());

						/* API COLUMNS ARE NULL IN FTP SITE CONFIG */
						site.setApiDataInterval(null);
						site.setApiScheduleInterval(null);
						site.setApiKey(null);
						site.setApiUrl(null);
						site.setSensorkey(null);
						site.setSensorUrl(null);
						site.setEnergyMeterkey(null);
						site.setEnergyMeterUrl(null);
						site.setScbkey(null);
						site.setScbUrl(null);
						site.setTrackerkey(null);
						site.setTrackerUrl(null);

					} else if (OperationMode.API.getType().equalsIgnoreCase(dataCollection.getCollectionType())) {
						site.setOperationMode(new Integer(2));
						site.setApiDataInterval(dataCollection.getApiDataInterval());
						site.setApiScheduleInterval(dataCollection.getApiScheduleInterval());
						if (dataCollection.getApiKey() != null && !dataCollection.getApiKey().equalsIgnoreCase("")
								&& dataCollection.getApiUrl() != null
								&& !dataCollection.getApiUrl().equalsIgnoreCase("")
								&& dataCollection.getDataLoggerIdInverter() != null
								&& dataCollection.getServiceCodeInverter() != null) {
							site.setApiKey(dataCollection.getApiKey());
							site.setApiUrl(dataCollection.getApiUrl());
							site.setDataLoggerID_Inverter(dataCollection.getDataLoggerIdInverter());
							site.setServiceCode_Inverter(dataCollection.getServiceCodeInverter());
						}
						if (dataCollection.getSensorkey() != null && !dataCollection.getSensorkey().equalsIgnoreCase("")
								&& dataCollection.getSensorUrl() != null
								&& !dataCollection.getSensorUrl().equalsIgnoreCase("")
								&& dataCollection.getDataLoggerIdSensor() != null
								&& dataCollection.getServiceCodeSensor() != null) {
							site.setSensorkey(dataCollection.getSensorkey());
							site.setSensorUrl(dataCollection.getSensorUrl());
							site.setDataLoggerID_Sensor(dataCollection.getDataLoggerIdSensor());
							site.setServiceCode_Sensor(dataCollection.getServiceCodeSensor());
						}

						if (dataCollection.getEnergyMeterkey() != null
								&& !dataCollection.getEnergyMeterkey().equalsIgnoreCase("")
								&& dataCollection.getEnergyMeterUrl() != null
								&& !dataCollection.getEnergyMeterUrl().equalsIgnoreCase("")
								&& dataCollection.getDataLoggerIdEnergyMeter() != null
								&& dataCollection.getServiceCodeEnergyMeter() != null) {
							site.setEnergyMeterkey(dataCollection.getEnergyMeterkey());
							site.setEnergyMeterUrl(dataCollection.getEnergyMeterUrl());
							site.setDataLoggerID_ENERGYMETER(dataCollection.getDataLoggerIdEnergyMeter());
							site.setServiceCode_ENERGYMETER(dataCollection.getServiceCodeEnergyMeter());

						}

						if (dataCollection.getScbkey() != null && !dataCollection.getScbkey().equalsIgnoreCase("")
								&& dataCollection.getScbUrl() != null
								&& !dataCollection.getScbUrl().equalsIgnoreCase("")
								&& dataCollection.getDataLoggerIdScb() != null
								&& dataCollection.getServiceCodeScb() != null) {
							site.setScbkey(dataCollection.getScbkey());
							site.setScbUrl(dataCollection.getScbUrl());
							site.setDataLoggerID_SCB(dataCollection.getDataLoggerIdScb());
							site.setServiceCode_SCB(dataCollection.getServiceCodeScb());
						}
						if (dataCollection.getTrackerkey() != null
								&& !dataCollection.getTrackerkey().equalsIgnoreCase("")
								&& dataCollection.getTrackerUrl() != null
								&& !dataCollection.getTrackerUrl().equalsIgnoreCase("")
								&& dataCollection.getDataLoggerIdTracker() != null
								&& dataCollection.getServiceCodeTracker() != null) {
							site.setTrackerkey(dataCollection.getTrackerkey());
							site.setTrackerUrl(dataCollection.getTrackerUrl());
							site.setDataLoggerID_TRACKER(dataCollection.getDataLoggerIdTracker());
							site.setServiceCode_TRACKER(dataCollection.getServiceCodeTracker());
						}

						/* FTP COLUMNS ARE NULL IN API SITE CONFIG */
						site.setFileType(null);
						site.setLocalFtpDirectory_Inverter(null);
						site.setLocalFtpDirectory_Modbus(null);
						site.setLocalFtpDirectory_Sensor(null);
						site.setLocalFtpUserName(null);
						site.setLocalFtpPassword(null);
						site.setLocalFtpHomeDirectory(null);
						site.setLocalFtpDirectoryPath_Inverter(null);
						site.setLocalFtpDirectoryPath_Modbus(null);
						site.setLocalFtpDirectoryPath_Sensor(null);
					}
					siteService.updateSiteConfig(site);
					response.setStatus("Success");
					response.setData(site);
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			response.setStatus("Failed");
			response.setData(null);
		}
		return response;
	}

}
