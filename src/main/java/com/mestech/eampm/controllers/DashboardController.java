/******************************************************
 *
 *    	Filename	: DashboardController.java
 *
 *      Author		: Sarath Babu E
 *
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *
 *      Description : This Controller deals with dashboard related activities.
 *
 *
 *******************************************************/
package com.mestech.eampm.controllers;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.CustomerViewBean;
import com.mestech.eampm.bean.DashboardEnergySummaryBean;
import com.mestech.eampm.bean.DashboardOandMBean;
import com.mestech.eampm.bean.DashboardSiteBean;
import com.mestech.eampm.bean.DashboardSiteListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.bean.SiteListBean;
import com.mestech.eampm.bean.SiteMapListBean;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentType;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.TicketTransaction;
import com.mestech.eampm.model.User;
import com.mestech.eampm.model.UserLog;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.DataSourceService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.EquipmentTypeService;
import com.mestech.eampm.service.EventDetailService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteStatusService;
import com.mestech.eampm.service.TicketDetailService;
import com.mestech.eampm.service.TicketTransactionService;
import com.mestech.eampm.service.UserLogService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.JsonMapData;
import com.mestech.eampm.utility.UtilityCommon;

@Controller
public class DashboardController {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private SiteStatusService siteStatusService;

	@Autowired
	private DataSourceService dataSourceService;

	@Autowired
	private EquipmentTypeService equipmenttypeService;

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private UserService userService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	@Autowired
	private TicketDetailService ticketDetailService;

	@Autowired
	private TicketTransactionService ticketTransactionService;

	@Autowired
	private EventDetailService eventDetailService;

	@Autowired
	private UserLogService userLogService;

	private UtilityCommon utilityCommon = new UtilityCommon();

	/********************************************************************************************
	 *
	 * Function Name : gettimezoneoffsetmin
	 *
	 * Author : Sarath Babu E
	 *
	 * Time Stamp : 29-Apr-19 09:47 AM
	 *
	 * Description : This method returns timezone offset.
	 *
	 * Input Params : timezonevalue,IpAddress,session,authentication
	 *
	 * Return Value : ResponseEntity<Map<String,Object>>
	 *
	 * Exceptions : Exception
	 *
	 *
	 **********************************************************************************************/
	@RequestMapping(value = "/timezoneoffsetmin", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> gettimezoneoffsetmin(String timezonevalue, String IpAddress,
			HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			session.setAttribute("timezonevalue", timezonevalue);

			session.setAttribute("ipaddress", IpAddress);

			// String Mac = getMACAddress(IpAddress);

			// System.out.println("MAC Address:::"+ Mac);

			String SSSS = session.getId();
			session.setAttribute("sessionID", SSSS);
			System.out.println("Timezone Method:::: " + SSSS);

			System.out.println("TimeZone" + timezonevalue);
			// System.out.println("Ip Addr"+ IpAddress);

			objResponseEntity.put("status", true);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}
		return null;

	}

	/********************************************************************************************
	 *
	 * Function Name : listDashboard
	 *
	 * Author : Sarath Babu E
	 *
	 * Time Stamp : 29-Apr-19 09:47 AM
	 *
	 * Description : This method loads dashboard page.
	 *
	 * Input Params : model,session,authentication,httpServletRequest
	 *
	 * Return Value : String
	 *
	 * Exceptions : UnknownHostException,IndexOutOfBounceException,Exception.
	 *
	 *
	 **********************************************************************************************/
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public String listDashboard(Model model, HttpSession session, Authentication authentication,
			HttpServletRequest httpServletRequest) throws UnknownHostException {

		if (authentication == null) {

			return "redirect:/login";

		} else if (session == null) {

			return "redirect:/login";

		} else if (session.getAttribute("timezonevalue") == null) {

			return "redirect:/login";

		} else {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			String timezonevalue = "0";
			String ipaddress = "0";
			String SessionID = "0";
			if (!timezonevalue.equals("") && timezonevalue != null && !ipaddress.equals("") && ipaddress != null) {
				timezonevalue = session.getAttribute("timezonevalue").toString();
				ipaddress = session.getAttribute("ipaddress").toString();
				SessionID = session.getAttribute("sessionID").toString();
			}
			loginUserDetails.setTimezoneoffsetmin(timezonevalue);

			UserLog objuserlog = userLogService.getUserLogById(SessionID);

			String remoteAddress = httpServletRequest.getRemoteAddr();

			InetAddress inetAddress = null;
			try {
				inetAddress = InetAddress.getByName(remoteAddress);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			String hostName = inetAddress.getHostName();

			objuserlog.setCreationDate(new Date());
			objuserlog.setLocalIpAddress(ipaddress);
			objuserlog.setMacAddress(hostName);

			userLogService.updateUserlog(objuserlog);

			CustomerViewBean objCustomerViewBean = new CustomerViewBean();

			List<SiteListBean> lstSiteList = new ArrayList<SiteListBean>();
			// List<SiteMapListBean> lstSiteMapList = new ArrayList<SiteMapListBean>();

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

			try {

				List<DashboardSiteBean> lstDashboardSiteDetails = siteService
						.getDashboardSiteDetails(String.valueOf(id), timezonevalue);
				List<DashboardEnergySummaryBean> lstDashboardEnergySummaryDetails = siteService
						.getDashboardEnergySummaryDetails(String.valueOf(id), timezonevalue);
				List<DashboardOandMBean> lstDashboardEventAndTicketDetails = siteService
						.getDashboardEventAndTicketDetails(String.valueOf(id), timezonevalue);
				List<DashboardSiteListBean> lstDashboardSiteListDetails = siteService
						.getDashboardSiteList(String.valueOf(id), timezonevalue);

				DashboardSiteBean objDashboardSiteBean = null;
				DashboardEnergySummaryBean objDashboardEnergySummaryBean = null;
				DashboardOandMBean objDashboardOandMBean = null;

				if (lstDashboardSiteDetails.size() > 0) {
					for (Object object : lstDashboardSiteDetails) {
						Object[] obj = (Object[]) object;
						// objDashboardSiteBean = new DashboardSiteBean(totalSites, roofTopSites,
						// utilitySites, offlineStatus, activeStatus, warningStatus, downStatus);
						// objDashboardSiteBean = new
						// DashboardSiteBean(StringFromBigDecimalObject(obj[0]),
						// StringFromBigDecimalObject(obj[1]),StringFromBigDecimalObject(obj[2]),StringFromBigDecimalObject(obj[3]),StringFromBigDecimalObject(obj[4]),StringFromBigDecimalObject(obj[5]),StringFromBigDecimalObject(obj[6]));
						objDashboardSiteBean = new DashboardSiteBean(
								UtilityCommon.StringFromBigDecimalOrIntegerObject(obj[0]),
								UtilityCommon.StringFromBigDecimalOrIntegerObject(obj[1]),
								UtilityCommon.StringFromBigDecimalOrIntegerObject(obj[2]),
								UtilityCommon.StringFromBigDecimalOrIntegerObject(obj[3]),
								UtilityCommon.StringFromBigDecimalOrIntegerObject(obj[4]),
								UtilityCommon.StringFromBigDecimalOrIntegerObject(obj[5]),
								UtilityCommon.StringFromBigDecimalOrIntegerObject(obj[6]));
					}
				}

				if (lstDashboardEnergySummaryDetails.size() > 0) {
					for (Object object : lstDashboardEnergySummaryDetails) {
						Object[] obj = (Object[]) object;
						Double dblTotalValue = ((java.math.BigDecimal) obj[4]).doubleValue();
						Double dblTodayValue = ((java.math.BigDecimal) obj[5]).doubleValue();
						Double dblTodayHoursOn = ((java.math.BigDecimal) obj[6]).doubleValue();
						Double Co2 = ((java.math.BigDecimal) obj[7]).doubleValue();
						Double TotalCo2 = ((java.math.BigDecimal) obj[8]).doubleValue();

						// objDashboardEnergySummaryBean = new DashboardEnergySummaryBean(productionOn,
						// lastChecked, lastDataReceived, lastDownTime, totalEnergy, todayEnergy,
						// todayHoursOn);
						// objDashboardEnergySummaryBean = new
						// DashboardEnergySummaryBean(StringFromStringObject(obj[0]),StringFromStringObject(obj[1]),StringFromStringObject(obj[2]),StringFromStringObject(obj[3]),dblTotalValue,
						// dblTodayValue, dblTodayHoursOn);
						objDashboardEnergySummaryBean = new DashboardEnergySummaryBean(
								UtilityCommon.StringFromStringObject(obj[0]),
								UtilityCommon.StringFromStringObject(obj[1]),
								UtilityCommon.StringFromStringObject(obj[2]),
								UtilityCommon.StringFromStringObject(obj[3]), dblTotalValue, dblTodayValue,
								dblTodayHoursOn, Co2, TotalCo2);

					}
				}

				if (lstDashboardEventAndTicketDetails.size() > 0) {
					for (Object object : lstDashboardEventAndTicketDetails) {
						Object[] obj = (Object[]) object;
						// objDashboardOandMBean =new DashboardOandMBean(totalTicket, openTicket,
						// closedTicket, totalEvent, todayEvent)
						// objDashboardOandMBean =new
						// DashboardOandMBean(StringFromBigDecimalObject(obj[0]),StringFromBigDecimalObject(obj[1]),StringFromBigDecimalObject(obj[2]),StringFromBigDecimalObject(obj[3]),StringFromBigDecimalObject(obj[4]));
						objDashboardOandMBean = new DashboardOandMBean(
								UtilityCommon.StringFromBigDecimalOrIntegerObject(obj[0]),
								UtilityCommon.StringFromBigDecimalOrIntegerObject(obj[1]),
								UtilityCommon.StringFromBigDecimalOrIntegerObject(obj[2]),
								UtilityCommon.StringFromBigDecimalOrIntegerObject(obj[3]),
								UtilityCommon.StringFromBigDecimalOrIntegerObject(obj[4]));
					}
				}

				String MapJsonData = "";
				if (lstDashboardSiteListDetails.size() > 0) {
					Integer a = 0;

					String SiteID = "";
					String SiteTypeID = "";
					String SiteCode = "";
					String SiteRef = "";
					String SiteName = "";
					String Lattitude = "";
					String Longtitude = "";
					String Address = "";
					String City = "";
					String State = "";
					String Country = "";
					String PostalCode = "";
					String ActiveInverterCount = "";
					String ActiveEMCount = "";
					String EMCount = "";
					String InverterCount = "";
					String LastChecked = "";
					Double TotalEnergy = 0.0;
					Double TodayEnergy = 0.0;
					String LastDownTime = "";
					String LastDataReceived = "";
					Double TodayHoursOn = 0.0;
					String Status = "";
					String Inverters = "";
					String EMs = "";
					Double InstallationCapacity = 0.0;
					String SpecificYield = "-";
					Double PlantLoadfactor = 0.0;
					List<JsonMapData> jsonMapDatas = new ArrayList<>();
					for (Object object : lstDashboardSiteListDetails) {
						Object[] obj = (Object[]) object;

						SiteID = UtilityCommon.StringFromBigDecimalOrIntegerObject(obj[0]);

						SiteTypeID = obj[1].toString();

						SiteCode = UtilityCommon.StringFromStringObject(obj[2]);

						SiteRef = UtilityCommon.StringFromStringObject(obj[3]);

						SiteName = UtilityCommon.StringFromStringObject(obj[4]);

						Lattitude = UtilityCommon.StringFromStringObject(obj[5]);

						Longtitude = UtilityCommon.StringFromStringObject(obj[6]);

						Address = UtilityCommon.StringFromStringObject(obj[7]);

						City = UtilityCommon.StringFromStringObject(obj[8]);

						State = UtilityCommon.StringFromStringObject(obj[9]);

						Country = UtilityCommon.StringFromStringObject(obj[10]);

						PostalCode = UtilityCommon.StringFromStringObject(obj[11]);

						ActiveInverterCount = obj[12].toString();

						InverterCount = obj[13].toString();

						LastChecked = UtilityCommon.StringFromStringObject(obj[14]);

						TotalEnergy = UtilityCommon.DoubleFromBigDecimalObject(obj[15]);

						TodayEnergy = UtilityCommon.DoubleFromBigDecimalObject(obj[16]);

						LastDownTime = UtilityCommon.StringFromStringObject(obj[17]);

						LastDataReceived = UtilityCommon.StringFromStringObject(obj[18]);

						TodayHoursOn = UtilityCommon.DoubleFromBigDecimalObject(obj[19]);

						Status = UtilityCommon.StringFromStringObject(obj[20]);

						InstallationCapacity = UtilityCommon.DoubleFromBigDecimalObject(obj[21]);
						ActiveEMCount = obj[26].toString();
						EMCount = obj[27].toString();

						if (EMCount != null && EMCount != "0" && EMCount != "") {
							EMs = ActiveEMCount + " / " + EMCount;
						} else {
							EMs = "";
						}

						if (InverterCount != null && InverterCount != "0" && InverterCount != "") {
							Inverters = ActiveInverterCount + " / " + InverterCount;
						} else {
							Inverters = "";
						}

						Double dblSY = (((TodayEnergy) / (InstallationCapacity)));

						if (dblSY < 7.0) {
							SpecificYield = String.format("%.2f", dblSY);
						} else {
							SpecificYield = "-";
						}

						PlantLoadfactor = (TodayEnergy / InstallationCapacity) * 24;

						SiteListBean objSiteListBean = new SiteListBean();
						objSiteListBean.setInverters(Inverters);
						objSiteListBean.setEms(EMs);
						objSiteListBean.setLastDownTime(LastDownTime);
						objSiteListBean.setLastUpdate(LastDataReceived);
						objSiteListBean.setNetworkStatus(Status);
						objSiteListBean.setPerformanceRatio(SpecificYield);
						objSiteListBean.setSiteCode(SiteCode);
						objSiteListBean.setSiteID(SiteID);
						objSiteListBean.setSiteTypeID(SiteTypeID);
						objSiteListBean.setSiteName(SiteName);
						objSiteListBean.setSiteReference(SiteRef);
						objSiteListBean.setTodayEnergy(String.format("%.2f", TodayEnergy));
						objSiteListBean.setTotalEnergy(String.format("%.2f", TotalEnergy / 1000));
						objSiteListBean.setInstallationCapacity(InstallationCapacity);
						objSiteListBean.setLoadFactor(String.format("%.2f", PlantLoadfactor));

						lstSiteList.add(objSiteListBean);

						String varZIndex = "99996";
						if (Status.equals("Active")) {
							varZIndex = "99997";
						} else if (Status.equals("Warning")) {
							varZIndex = "99998";
						} else if (Status.equals("Down")) {
							varZIndex = "99999";
						} else {
							varZIndex = "99996";
						}

						JsonMapData jsonMapData = new JsonMapData(
								"<b>" + SiteName + " - " + SiteRef + "</b><p>" + Address + "</p><p>" + City + " - "
										+ PostalCode + ".</p>",
								Lattitude, Longtitude, SiteName, Status.toLowerCase(), new Integer(varZIndex),
								"/eampm-maven/siteview" + SiteID + "&ref=" + id);
						jsonMapDatas.add(jsonMapData);

//						if (a == 0) {
//							MapJsonData = "['<b>" + SiteName + " - " + SiteRef + "</b><p>" + Address + "</p><p>" + City
//									+ " - " + PostalCode + ".</p>'," + Lattitude + ", " + Longtitude + ",'" + SiteName
//									+ "'," + Status.toLowerCase() + "," + varZIndex + ",'/eampm-maven/siteview" + SiteID
//									+ "&ref=" + id + "']";
//						} else {
//							MapJsonData = MapJsonData + ",['<b>" + SiteName + " - " + SiteRef + "</b><p>" + Address
//									+ "</p><p>" + City + " - " + PostalCode + ".</p>'," + Lattitude + ", " + Longtitude
//									+ ",'" + SiteName + "'," + Status.toLowerCase() + "," + varZIndex
//									+ ",'/eampm-maven/siteview" + SiteID + "&ref=" + id + "']";
//						}
//
//						a = a + 1;

					}

					if (jsonMapDatas != null && !jsonMapDatas.isEmpty()) {

						Gson gson = new Gson();
						String jsonData = gson.toJson(jsonMapDatas);
						jsonData = jsonData.replaceAll("\"", "\'");
						model.addAttribute("mapData", jsonData);

					}
				}

				String varStrTotalSiteCount = "0";
				String varStrRooftopSiteCount = "0";
				String varStrUtilitySiteCount = "0";
				String varStrRooftopUtilitySiteCount = "0";
				String varStrOfflineSiteCount = "0";
				String varStrActiveSiteCount = "0";
				String varStrWarningSiteCount = "0";
				String varStrDownSiteCount = "0";

				String varStrTotalEvents = "0";
				String varStrTodayEvents = "0";
				String varStrTotalOpenTicket = "0";
				String varStrTotalClosedTicket = "0";
				String varStrTotalHoldTicket = "0";
				String varStrTotalCompletedTicket = "0";

				String varStrProductionOn = "";
				String varStrLastChecked = "";
				String varStrLastDataReceived = "";
				String varStrLastDownTime = "";
				Double varDblTotalEnergy = 0.0;
				Double varDblTodayEnergy = 0.0;
				Double varDblTodayHoursOn = 0.0;
				Double Co2Avioded = 0.0;
				Double LoadFactor = 0.0;
				Double TotalCo2Avioded = 0.0;
				String varStrTotalEnergyUOM = " kWh";
				String varStrTodayEnergyUOM = " kWh";
				String varStrTodayHoursOnUOM = " hours";

				if (objDashboardSiteBean != null) {
					varStrTotalSiteCount = objDashboardSiteBean.getTotalSites();
					varStrRooftopSiteCount = objDashboardSiteBean.getRoofTopSites();
					varStrUtilitySiteCount = objDashboardSiteBean.getUtilitySites();

					varStrOfflineSiteCount = objDashboardSiteBean.getOfflineStatus();
					varStrActiveSiteCount = objDashboardSiteBean.getActiveStatus();
					varStrWarningSiteCount = objDashboardSiteBean.getWarningStatus();
					varStrDownSiteCount = objDashboardSiteBean.getDownStatus();
				}

				if (objDashboardEnergySummaryBean != null) {
					varStrProductionOn = objDashboardEnergySummaryBean.getProductionOn();
					varStrLastChecked = objDashboardEnergySummaryBean.getLastChecked();
					varStrLastDataReceived = objDashboardEnergySummaryBean.getLastDataReceived();
					varStrLastDownTime = objDashboardEnergySummaryBean.getLastDownTime();
					varDblTotalEnergy = objDashboardEnergySummaryBean.getTotalEnergy();
					varDblTodayEnergy = objDashboardEnergySummaryBean.getTodayEnergy();
					varDblTodayHoursOn = objDashboardEnergySummaryBean.getTodayHoursOn();
					Co2Avioded = objDashboardEnergySummaryBean.getCo2();
					TotalCo2Avioded = objDashboardEnergySummaryBean.getTotalCo2();

				}

				if (objDashboardOandMBean != null) {
					varStrTotalEvents = objDashboardOandMBean.getTotalEvent();
					varStrTodayEvents = objDashboardOandMBean.getTodayEvent();

					varStrTotalOpenTicket = objDashboardOandMBean.getOpenTicket();
					varStrTotalClosedTicket = objDashboardOandMBean.getClosedTicket();

				}

				objCustomerViewBean.setTotalSiteCount(varStrTotalSiteCount);
				objCustomerViewBean.setRooftopCount(varStrRooftopSiteCount);
				objCustomerViewBean.setUtilityCount(varStrUtilitySiteCount);
				objCustomerViewBean.setRooftopUtilityCount(varStrRooftopUtilitySiteCount);
				objCustomerViewBean.setActiveSiteCount(varStrActiveSiteCount);
				objCustomerViewBean.setWarningSiteCount(varStrWarningSiteCount);
				objCustomerViewBean.setDownSiteCount(varStrDownSiteCount);
				objCustomerViewBean.setOfflineSiteCount(varStrOfflineSiteCount);

				if (varDblTodayEnergy >= 1000000) {
					varDblTodayEnergy = varDblTodayEnergy / 1000000;
					varStrTodayEnergyUOM = " GWh";

					Double co2avioded = 0.00067 * varDblTodayEnergy * 1000000 * 1000;
					objCustomerViewBean.setTodayCo2Avoided(String.format("%.2f", co2avioded) + " T");
					
					User objUser = userService.getUserById(id);
					if (objUser.getRoleID() == 6) {
						objCustomerViewBean.setLoadfactor(
								String.format("%.2f", ((varDblTodayEnergy / (11.019 * 24)) * 100)) + " %");
						

					}

				} else if (varDblTodayEnergy >= 1000) {
					varDblTodayEnergy = varDblTodayEnergy / 1000;
					varStrTodayEnergyUOM = " MWh";

					Double co2avioded = 0.00067 * varDblTodayEnergy * 1000 * 1000;
					objCustomerViewBean.setTodayCo2Avoided(String.format("%.2f", co2avioded) + " T");

					User objUser = userService.getUserById(id);
					if (objUser.getRoleID() == 6) {
						objCustomerViewBean.setLoadfactor(
								String.format("%.2f", ((varDblTodayEnergy / (11.019 * 24)) * 100)) + " %");
						System.out.println("1::::: " + objCustomerViewBean.getLoadfactor());

					}

				} else if (varDblTodayEnergy < 1) {
					varDblTodayEnergy = varDblTodayEnergy * 1000;
					varStrTodayEnergyUOM = " Wh";
					Double co2avioded = 0.00067 * varDblTodayEnergy * 1000;
					objCustomerViewBean.setTodayCo2Avoided(String.format("%.2f", co2avioded) + " T");
					
					User objUser = userService.getUserById(id);
					if (objUser.getRoleID() == 6) {
						objCustomerViewBean.setLoadfactor(
								String.format("%.2f", ((varDblTodayEnergy / (11.019 * 24)) * 100)) + " %");
						

					}

				}
				else {
					varStrTodayEnergyUOM = " kWh";
					Double co2avioded = 0.00067 * varDblTodayEnergy * 1000;
					objCustomerViewBean.setTodayCo2Avoided(String.format("%.2f", co2avioded) + " T");
					
					User objUser = userService.getUserById(id);
					if (objUser.getRoleID() == 6) {
						objCustomerViewBean.setLoadfactor(
								String.format("%.2f", ((varDblTodayEnergy / (11.019 * 24)) * 100)) + " %");
						

					}
					

				}

				if (varDblTotalEnergy >= 1000000) {
					varDblTotalEnergy = varDblTotalEnergy / 1000000;
					varStrTotalEnergyUOM = " GWh";

					Double co2avioded = 0.00067 * varDblTotalEnergy * 1000000 * 1000;
					objCustomerViewBean.setTotalCo2Avoided(String.format("%.2f", co2avioded) + " T");

				} else if (varDblTotalEnergy >= 1000) {
					varDblTotalEnergy = varDblTotalEnergy / 1000;
					varStrTotalEnergyUOM = " MWh";

					Double co2avioded = 0.00067 * varDblTotalEnergy * 1000 * 1000;
					objCustomerViewBean.setTotalCo2Avoided(String.format("%.2f", co2avioded) + " T");

				} else if (varDblTotalEnergy < 1) {
					varDblTotalEnergy = varDblTotalEnergy * 1000;
					varStrTotalEnergyUOM = " Wh";

					Double co2avioded = 0.00067 * varDblTotalEnergy * 1000;
					objCustomerViewBean.setTotalCo2Avoided(String.format("%.2f", co2avioded) + " T");

				}

				objCustomerViewBean.setTodayEnergy(String.format("%.2f", varDblTodayEnergy) + varStrTodayEnergyUOM);
				objCustomerViewBean.setTotalEnergy(String.format("%.2f", varDblTotalEnergy) + varStrTotalEnergyUOM);
				objCustomerViewBean.setTotalDownTime("");
				objCustomerViewBean.setProductionDate(varStrProductionOn);
				objCustomerViewBean.setLastUpdatedTime(varStrLastChecked);
				objCustomerViewBean.setEventTime(varStrLastDataReceived);
				objCustomerViewBean.setLastDownTime(varStrLastDownTime);

				objCustomerViewBean.setOpenTicketCount(varStrTotalOpenTicket);
				objCustomerViewBean.setYettoStartCount(varStrTotalOpenTicket);
				objCustomerViewBean.setCompletedTicketCount(varStrTotalClosedTicket);
				objCustomerViewBean.setTicketMessages("");
				objCustomerViewBean.setInprocessCount("");
				objCustomerViewBean.setTodayEventCount(varStrTodayEvents);
				objCustomerViewBean.setTotalEventCount(varStrTotalEvents);

				/*
				 * objCustomerViewBean.setTodayCo2Avoided(String.format("%.2f", Co2Avioded) +
				 * " T"); objCustomerViewBean.setTotalCo2Avoided(String.format("%.2f",
				 * TotalCo2Avioded) + " T");
				 */
				objCustomerViewBean.setTotalProductionYield("");
				objCustomerViewBean.setTodayProductionYield("");
				objCustomerViewBean.setTotalPerformanceRatio("");
				objCustomerViewBean.setTodayPerformanceRatio("");

				MapJsonData = "[" + MapJsonData + "]";

				objCustomerViewBean.setMapJsonData(MapJsonData);
				objCustomerViewBean.setUserID(id);

			} catch (NumberFormatException ex) {

				return "redirect:/login";
			} catch (IndexOutOfBoundsException ex) {

				return "redirect:/login";
			} catch (Exception ex) {

				return "redirect:/login";
			}

			AccessListBean objAccessListBean = new AccessListBean();
			List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
			List<String> objactivitys = new ArrayList<>();

			StringBuilder stringBuilder = new StringBuilder();

			try {
				User objUser = userService.getUserById(id);

				// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

				lstRoleActivity = roleActivityService.listRoleActivities().stream()
						.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

				objAccessListBean.setUserID(id);
				objAccessListBean.setUserName(objUser.getShortName());

				if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
					objAccessListBean.setCustomerListView("visible");
				}
				if (objUser.getRoleID() == 6) {
					objAccessListBean.setMonitoringView("visible");
				}

				for (int l = 0; l < lstRoleActivity.size(); l++) {
					if (lstRoleActivity.get(l).getActiveFlag() == 1) {
						Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
						if (null != activity) {
							if (activity.getActivityDescription() != null) {
								stringBuilder.append(activity.getActivityDescription());
								stringBuilder.append("#");
							}
						}
					}

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			model.addAttribute("siteList", getNewTicketSiteList("Site", id));

			model.addAttribute("classtate", "active");
			model.addAttribute("access", objAccessListBean);
			model.addAttribute("accessPages", stringBuilder.toString());
			model.addAttribute("dashboard", objCustomerViewBean);
			model.addAttribute("ticketcreation", new TicketDetail());
			model.addAttribute("dashboardSiteList", lstSiteList);

			return "dashboard";
		}
	}

	/********************************************************************************************
	 *
	 * Function Name : DashboardSiteDetail
	 *
	 * Author : Sarath Babu E
	 *
	 * Time Stamp : 29-Apr-19 09:47 AM
	 *
	 * Description : This function returns data for dashboard page grid.
	 *
	 * Input Params : session,authentication
	 *
	 * Return Value : ResponseEntity<Map<String,Object>>
	 *
	 * Exceptions : Exception
	 *
	 *
	 **********************************************************************************************/
	@RequestMapping(value = "/dashboardsitedetail", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> DashboardSiteDetail(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", false);
				objResponseEntity.put("dashboardSiteList", null);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			} else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<DashboardSiteListBean> lstDashboardSiteListDetails = siteService
						.getDashboardSiteList(String.valueOf(id), timezonevalue);
				List<SiteListBean> lstSiteList = new ArrayList<SiteListBean>();

				if (lstDashboardSiteListDetails.size() > 0) {
					Integer a = 0;

					String SiteID = "";
					String SiteTypeID = "";
					String SiteCode = "";
					String SiteRef = "";
					String SiteName = "";
					String Lattitude = "";
					String Longtitude = "";
					String Address = "";
					String City = "";
					String State = "";
					String Country = "";
					String PostalCode = "";
					String ActiveInverterCount = "";
					String EMCount = "";
					String ActiveEMCount = "";
					String InverterCount = "";
					String LastChecked = "";
					Double TotalEnergy = 0.0;
					Double TodayEnergy = 0.0;
					String LastDownTime = "";
					String LastDataReceived = "";
					Double TodayHoursOn = 0.0;
					String Status = "";
					String Inverters = "";
					String EMs = "";
					Double InstallationCapacity = 0.0;
					String SpecificYield = "-";

					for (Object object : lstDashboardSiteListDetails) {
						Object[] obj = (Object[]) object;

						SiteID = UtilityCommon.StringFromBigDecimalOrIntegerObject(obj[0]);
						SiteTypeID = obj[1].toString();
						SiteCode = UtilityCommon.StringFromStringObject(obj[2]);
						SiteRef = UtilityCommon.StringFromStringObject(obj[3]);
						SiteName = UtilityCommon.StringFromStringObject(obj[4]);
						Lattitude = UtilityCommon.StringFromStringObject(obj[5]);
						Longtitude = UtilityCommon.StringFromStringObject(obj[6]);
						Address = UtilityCommon.StringFromStringObject(obj[7]);
						City = UtilityCommon.StringFromStringObject(obj[8]);
						State = UtilityCommon.StringFromStringObject(obj[9]);
						Country = UtilityCommon.StringFromStringObject(obj[10]);
						PostalCode = UtilityCommon.StringFromStringObject(obj[11]);
						ActiveInverterCount = obj[12].toString();
						InverterCount = obj[13].toString();
						LastChecked = UtilityCommon.StringFromStringObject(obj[14]);
						TotalEnergy = UtilityCommon.DoubleFromBigDecimalObject(obj[15]);
						TodayEnergy = UtilityCommon.DoubleFromBigDecimalObject(obj[16]);
						LastDownTime = UtilityCommon.StringFromStringObject(obj[17]);
						LastDataReceived = UtilityCommon.StringFromStringObject(obj[18]);
						TodayHoursOn = UtilityCommon.DoubleFromBigDecimalObject(obj[19]);
						Status = UtilityCommon.StringFromStringObject(obj[20]);
						InstallationCapacity = UtilityCommon.DoubleFromBigDecimalObject(obj[21]);
						ActiveEMCount = obj[26].toString();
						EMCount = obj[27].toString();

						if (EMCount != null && EMCount != "0" && EMCount != "") {
							EMs = ActiveEMCount + " / " + EMCount;
						} else {
							EMs = "";
						}

						if (InverterCount != null && InverterCount != "0" && InverterCount != "") {
							Inverters = ActiveInverterCount + " / " + InverterCount;
						} else {
							Inverters = "";
						}

						Double dblSY = (((TodayEnergy) / (InstallationCapacity)));

						if (dblSY < 7.0) {
							SpecificYield = String.format("%.2f", dblSY);
						} else {
							SpecificYield = "-";
						}

						SiteListBean objSiteListBean = new SiteListBean();

						objSiteListBean.setInverters(Inverters);
						objSiteListBean.setEms(EMs);
						objSiteListBean.setLastDownTime(LastDownTime);
						objSiteListBean.setLastUpdate(LastDataReceived);
						objSiteListBean.setNetworkStatus(Status);
						objSiteListBean.setPerformanceRatio(SpecificYield);
						objSiteListBean.setSiteCode(SiteCode);
						objSiteListBean.setSiteID(SiteID);
						objSiteListBean.setSiteTypeID(SiteTypeID);
						objSiteListBean.setSiteName(SiteName);
						objSiteListBean.setSiteReference(SiteRef);
						objSiteListBean.setTodayEnergy(String.format("%.2f", TodayEnergy));
						objSiteListBean.setTotalEnergy(String.format("%.2f", TotalEnergy / 1000));
						objSiteListBean.setInstallationCapacity(InstallationCapacity);

						lstSiteList.add(objSiteListBean);

						a = a + 1;

					}
				}
				AccessListBean objAccessListBean = new AccessListBean();
				User objUser = userService.getUserById(id);

				List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

				lstRoleActivity = roleActivityService.listRoleActivities().stream()
						.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

				if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
					objAccessListBean.setCustomerListView("visible");
				}
				if (objUser.getRoleID() == 6) {
					objAccessListBean.setMonitoringView("visible");
					for (int i = 0; i < lstSiteList.size(); i++) {
						if (lstSiteList.get(i).getSiteName().equals("Repal Renewables, Komatikuntla")) {
							lstSiteList.get(i).setSiteName("Demo Site");
						}
					}
				}

				objResponseEntity.put("dashboardSiteList", lstSiteList);
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("dashboardSiteList", null);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 *
	 * Function Name : getDropdownList
	 *
	 * Author : Sarath Babu E
	 *
	 * Time Stamp : 29-Apr-19 09:47 AM
	 *
	 * Description : This function returns data for dropdown list.
	 *
	 * Input Params : DropdownName,refid
	 *
	 * Return Value : Map<String,String>
	 *
	 *
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "User") {
			List<User> ddlList = userService.listFieldUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 *
	 * Function Name : getNewTicketSiteList
	 *
	 * Author : Sarath Babu E
	 *
	 * Time Stamp : 29-Apr-19 09:47 AM
	 *
	 * Description : This funciton returns data for dropdown list.
	 *
	 * Input Params : DropdownName,refid
	 *
	 * Return Value : Map<String,String>
	 *
	 *
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 *
	 * Function Name : getTicketCode
	 *
	 * Author : Sarath Babu E
	 *
	 * Time Stamp : 29-Apr-19 09:47 AM
	 *
	 * Description : This function returns ticket code.
	 *
	 * Input Params : timezonevalue
	 *
	 * Return Value : String
	 *
	 *
	 **********************************************************************************************/
	public String getTicketCode(String timezonevalue) {
		TicketDetail objTicketDetail = new TicketDetail();
		objTicketDetail = ticketDetailService.getTicketDetailByMax("TicketId", timezonevalue);

		String TC = "";

		if (objTicketDetail != null) {
			TC = objTicketDetail.getTicketCode();
		}

		/*
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		Date date = new Date(); // your date
		String dateInString = dateFormat.format(date);

		String finaldate = dateInString.split("-")[0];

		String finalmonth = dateInString.split("-")[1];

		String finalyear = dateInString.split("-")[2];

		finalyear = finalyear.replace("20", "");
		*/
		DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
		Date date = new Date(); // your date
		String dateInString = dateFormat.format(date);

		String ticketno = dateInString;
		if (TC == null || TC == "") {
			TC = dateInString + "0001";
		} else {
			if ((TC.substring(0, 6)).equals(ticketno)) {
				Integer val = (Integer.parseInt(TC) + 1);
				TC = val.toString();
			} else {
				TC = ticketno + "0001";
			}

		}
		return TC;
	}

	/********************************************************************************************
	 *
	 * Function Name : listTicketDetails
	 *
	 * Author : Sarath Babu E
	 *
	 * Time Stamp : 29-Apr-19 09:47 AM
	 *
	 * Description : This function adds new ticket.
	 *
	 * Input Params : ticketdetail,session,authentication
	 *
	 * Return Value : String
	 *
	 * Exceptions : Exception
	 *
	 *
	 **********************************************************************************************/
	@RequestMapping(value = "/addnewticket", method = RequestMethod.POST)
	public String listTicketDetails(@ModelAttribute("ticketcreation") TicketDetail ticketdetail, HttpSession session,
			Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String timezonevalue = "0";
		if (!timezonevalue.equals("") && timezonevalue != null) {
			timezonevalue = session.getAttribute("timezonevalue").toString();
		}
		loginUserDetails.setTimezoneoffsetmin(timezonevalue);

		Integer TicketID = 0;
		Date date = new Date();
		try {
			String TicketCode = getTicketCode(timezonevalue);

			ticketdetail.setTicketCode(TicketCode);

			ticketdetail.setTicketMode("Manual Ticket");

			ticketdetail.setNotificationStatus(0);

			ticketdetail.setActiveFlag(1);

			ticketdetail.setTicketStatus(1);

			ticketdetail.setState(1);

			ticketdetail.setDayCycle(1);

			ticketdetail.setRefTicketID(0);

			ticketdetail.setCreatedBy(id);

			ticketdetail.setCreationDate(date);

			TicketTransaction tickettransaction = new TicketTransaction();

			tickettransaction.setTicketStatus(1);
			// tickettransaction.setRemarks(ticketdetail.getDescription());
			// tickettransaction.setAssignedTo(ticketdetail.getAssignedTo());

			tickettransaction.setDescription("Ticket has been created");

			tickettransaction.setCreatedBy(id);

			tickettransaction.setActiveFlag(1);

			tickettransaction.setTimeStamp(date);

			tickettransaction.setCreationDate(date);

			tickettransaction.setDayCycle(1);

			ticketDetailService.addTicketDetail(ticketdetail);

			TicketDetail objTicketDetail = null;
			objTicketDetail = ticketDetailService.getTicketDetailByTicketCode(TicketCode, timezonevalue);
			if (objTicketDetail != null) {
				TicketID = objTicketDetail.getTicketID();
				tickettransaction.setTicketID(TicketID);
				ticketTransactionService.addTicketTransaction(tickettransaction);

			}

			return "redirect:/ticketviews" + TicketID;

		} catch (Exception ex) {
			System.out.println("Mohanraj ::" + ex.getMessage());
			return "redirect:/dashboard";
		}

	}

	/********************************************************************************************
	 *
	 * Function Name : DashboardLoad
	 *
	 * Author : Sarath Babu E
	 *
	 * Time Stamp : 29-Apr-19 09:47 AM
	 *
	 * Description : This function returns user dashboard data.
	 *
	 * Input Params : session,authentication
	 *
	 * Return Value : ResponseEntity<Map<String,Object>>
	 *
	 * Exceptions : Exception
	 *
	 *
	 **********************************************************************************************/
	@RequestMapping(value = "/userdashboard", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> DashboardLoad(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);
			} else {
				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				CustomerViewBean objCustomerViewBean = new CustomerViewBean();

				List<Site> lstSite = siteService.getSiteListByUserId(id);
				List<SiteStatus> lstSiteStatus = siteStatusService.getSiteStatusListByUserId(id);
				List<TicketDetail> lstTicketDetail = ticketDetailService.getTicketDetailListByUserId(id, timezonevalue);

				List<SiteListBean> lstSiteList = new ArrayList<SiteListBean>();
				List<SiteMapListBean> lstSiteMapList = new ArrayList<SiteMapListBean>();

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/*
	 * @RequestMapping(value = "/equipmentsBysites", method = RequestMethod.GET,
	 * produces = "application/json") public @ResponseBody List<Equipment>
	 * equipmentArrangement(@RequestParam("siteId") String siteId) { if (siteId !=
	 * null) { List<Equipment> equipments =
	 * equipmentService.listEquipmentsBySiteId(Integer.parseInt(siteId));
	 *
	 * return equipments; } else { return null; }
	 *
	 * }
	 */

	/********************************************************************************************
	 *
	 * Function Name : equipmentArrangement
	 *
	 * Author : Sarath Babu E
	 *
	 * Time Stamp : 29-Apr-19 09:47 AM
	 *
	 * Description : This function return equipment by sites.
	 *
	 * Input Params : siteId,session,authentication
	 *
	 * Return Value : ResponseEntity<Map<String,Object>>
	 *
	 * Exceptions : Exception
	 *
	 *
	 **********************************************************************************************/
	@RequestMapping(value = "/equipmentsBysites", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> equipmentArrangement(@RequestParam("siteId") String siteId,
			HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			} else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();
				List<Equipment> ddlEquipmentsList = null;
				if (siteId != null) {
					ddlEquipmentsList = equipmentService.listEquipmentsBySiteId(Integer.parseInt(siteId));
				}
				List<EquipmentType> ddlEquipmentTypeList = equipmenttypeService.listEquipmentTypes();

				for (int i = 0; i < ddlEquipmentsList.size(); i++) {
					List<EquipmentType> lstEquipmentTypeList = new ArrayList<EquipmentType>();
					int etype = ddlEquipmentsList.get(i).getEquipmentTypeID();
					lstEquipmentTypeList = ddlEquipmentTypeList.stream().filter(p -> p.getEquipmentTypeId() == etype)
							.collect(Collectors.toList());

					if (lstEquipmentTypeList.size() > 0) {
						ddlEquipmentsList.get(i).setEquipmentTypeName(lstEquipmentTypeList.get(0).getEquipmentType());
					}

				}

				objResponseEntity.put("equipmentlist", ddlEquipmentsList);

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

}
