
/******************************************************
 * 
 *    	Filename	: EquipmentController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This Controller deals with equipment related actions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentCategory;
import com.mestech.eampm.model.EquipmentType;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.EquipmentCategoryService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.EquipmentTypeService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class EquipmentController {

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private EquipmentTypeService equipmenttypeService;

	@Autowired
	private EquipmentCategoryService equipmentcategoryService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private EquipmentTypeService equipmentTypeService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentPageLoad
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns equipment details.
	 * 
	 * Input Params : session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/equipment", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> EquipmentPageLoad(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				objResponseEntity.put("equipmentList", getUpdatedEquipmentList());
				objResponseEntity.put("siteList", getDropdownList("Site"));
				objResponseEntity.put("equipmenttypeList", getDropdownList("EquipmentType"));
				objResponseEntity.put("equipmentcategory", getDropdownList("EquipmentCategory"));

				objResponseEntity.put("ticketcreation", new TicketDetail());
				objResponseEntity.put("activeStatusList", getStatusDropdownList());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns equipment list.
	 * 
	 * Input Params : session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/equipmentdetailslist", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> EquipmentList(HttpSession session, Authentication authentication) {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();

		try {

			if (authentication == null) {
				objResponseEntity.put("equipmentList", getUpdatedEquipmentList());
				// objResponseEntity.put("status", true);
				// objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);
			} else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				objResponseEntity.put("equipmentList", getUpdatedEquipmentList());
				// objResponseEntity.put("status", true);
				// objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			// objResponseEntity.put("status", false);
			// objResponseEntity.put("data", e.getMessage());
			objResponseEntity.put("equipmentList", null);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : NewEquipmentSaveorUpdate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function saves or update equipment.
	 * 
	 * Input Params : lstequipment,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions : ConstraintViolationException,Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/newequipment", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> NewEquipmentSaveorUpdate(@RequestBody List<Equipment> lstequipment,
			HttpSession session, Authentication authentication) throws ServletException {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String ResponseMessage = "";
		String Dataexists1 = "";
		String Dataexists2 = "";

		try {

			if (lstequipment != null) {
				Equipment equipment = lstequipment.get(0);

				if (equipment != null) {
					System.out.println("Equipment Code:: " + equipment.getEquipmentCode());

					Date podate = null;
					Date podate1 = null;
					if (equipment.getWarrantyDateText() != null && !equipment.getWarrantyDateText().equals("")) {
						podate = new SimpleDateFormat("dd/MM/yyyy").parse(equipment.getWarrantyDateText());

					}
					if (equipment.getInstallationDateText() != null
							&& !equipment.getInstallationDateText().equals("")) {
						podate1 = new SimpleDateFormat("dd/MM/yyyy").parse(equipment.getInstallationDateText());

					}

					Date date = new Date();
					if (equipment.getEquipmentId() == null || equipment.getEquipmentId() == 0) {

						List<Equipment> lstequipments = equipmentService.listallequipments(0);

						for (int i = 0; i < lstequipments.size(); i++) {
							String DisplayName = lstequipments.get(i).getCustomerNaming();
							String Customerreference = lstequipments.get(i).getCustomerReference();

							if (equipment.getCustomerNaming().equals(DisplayName)) {

								Dataexists1 = "Customer Naming Already Exist";

								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						Equipment objequipment = equipmentService.getEquipmentByMax(equipment.getSiteID().toString());

						Site site = siteService.getSiteById(equipment.getSiteID());

						String EQ = "";

						if (objequipment != null) {
							EQ = objequipment.getEquipmentCode();
						}

						String sitecode = site.getSiteCode().replaceAll("-", "");

						if (EQ == null || EQ == "") {
							EQ = sitecode + "EQ001";
						} else if (EQ.contains(sitecode)) {

							int NextIndex = Integer.parseInt(EQ.replaceAll(sitecode + "EQ", "")) + 1;

							if (NextIndex >= 100) {
								EQ = String.valueOf(NextIndex);
							} else if (NextIndex >= 10) {
								EQ = "0" + String.valueOf(NextIndex);
							} else {
								EQ = "00" + String.valueOf(NextIndex);
							}

							EQ = sitecode + "EQ" + EQ;

						} else {
							EQ = sitecode + "EQ" + "001";
						}

						equipment.setEquipmentCode(EQ);
						equipment.setCreationDate(date);
						equipment.setDismandalFlag(0);
						equipment.setCreatedBy(id);
						if (podate != null || podate1 != null) {
							equipment.setWarrantyDate(podate);
							equipment.setInstallationDate(podate1);
						}

						equipmentService.addEquipment(equipment);
					} else {
						Equipment objEquipment = equipmentService.getEquipmentById(equipment.getEquipmentId());

						List<Equipment> lstequipments = equipmentService.listallequipments(equipment.getEquipmentId());

						for (int i = 0; i < lstequipments.size(); i++) {
							String DisplayName = lstequipments.get(i).getCustomerNaming();
							String Customerreference = lstequipments.get(i).getCustomerReference();

							if (equipment.getCustomerNaming().equals(DisplayName)) {

								Dataexists1 = "Customer Naming Already Exist";
								System.out.println("12345");
								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						objEquipment.setEquipmentTypeID(equipment.getEquipmentTypeID());
						objEquipment.setSiteID(equipment.getSiteID());
						objEquipment.setCustomerReference(equipment.getCustomerReference());
						objEquipment.setCustomerNaming(equipment.getCustomerNaming());

						objEquipment.setDLConfigurationID(equipment.getDLConfigurationID());
						objEquipment.setCapacity(equipment.getCapacity());

						objEquipment.setPrimarySerialNumber(equipment.getPrimarySerialNumber());
						objEquipment.setDescription(equipment.getDescription());

						objEquipment.setComponents(equipment.getComponents());
						objEquipment.setEquipmentSelection(equipment.getEquipmentSelection());
						objEquipment.setDisconnectRating(equipment.getDisconnectRating());
						objEquipment.setDisconnectType(equipment.getDisconnectType());

						objEquipment.setRemarks(equipment.getRemarks());
						objEquipment.setActiveFlag(equipment.getActiveFlag());
						if (equipment.getWarrantyDateText() != null && !equipment.getWarrantyDateText().equals("")) {
							objEquipment.setWarrantyDate(podate);

						}
						if (equipment.getInstallationDateText() != null
								&& !equipment.getInstallationDateText().equals("")) {
							objEquipment.setInstallationDate(podate1);

						}

						objEquipment.setLastUpdatedDate(date);
						objEquipment.setLastUpdatedBy(id);

						equipmentService.updateEquipment(objEquipment);

						objResponseEntity.put("status", true);
						objResponseEntity.put("equipment", objEquipment);
						objResponseEntity.put("data", "Success");
						return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

					}
					objResponseEntity.put("status", true);
					objResponseEntity.put("equipment", equipment);
					ResponseMessage = "Success";
				} else {
					objResponseEntity.put("status", false);
					ResponseMessage = "Invalid Request";
					objResponseEntity.put("equipment", null);
				}

			} else {
				objResponseEntity.put("status", false);
				ResponseMessage = "Invalid Request";
				objResponseEntity.put("equipment", null);
			}

			objResponseEntity.put("data", ResponseMessage);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (ConstraintViolationException cve) {
			objResponseEntity.put("equipment", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", cve.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			objResponseEntity.put("equipment", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This method returns values for dropdown list.
	 * 
	 * Return Value : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This method returns map with Site name and IDs.
	 * 
	 * Input Params : DropdownName,refid
	 * 
	 * Return Value : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This method returns values for dropdown list.
	 * 
	 * Input Params : DropdownName
	 * 
	 * Return Value : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "EquipmentType") {
			List<EquipmentType> ddlList = equipmenttypeService.listEquipmentTypes();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getEquipmentTypeId().toString(), ddlList.get(i).getEquipmentType());

			}

		} else if (DropdownName == "EquipmentCategory") {
			List<EquipmentCategory> ddlList = equipmentcategoryService.listEquipmentCategories();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getCategoryId().toString(), ddlList.get(i).getEquipmentCategory());

			}

		} else if (DropdownName == "Site") {
			List<Site> ddlList = siteService.listSites();

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				/*
				 * if(ddlList.get(i).getCustomerReference() != null) { SiteName = SiteName +
				 * " - " + ddlList.get(i).getCustomerReference(); }
				 */

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getUpdatedEquipmentList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function return all equipments.
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * 
	 **********************************************************************************************/
	public List<Equipment> getUpdatedEquipmentList() {
		// List<Equipment> ddlEquipmentsList = equipmentService.listEquipments();
		List<Equipment> ddlEquipmentsList = equipmentService.listConfigEquipments();
		List<EquipmentType> ddlEquipmentTypeList = equipmenttypeService.listEquipmentTypes();

		for (int i = 0; i < ddlEquipmentsList.size(); i++) {
			List<EquipmentType> lstEquipmentTypeList = new ArrayList<EquipmentType>();
			int etype = ddlEquipmentsList.get(i).getEquipmentTypeID();
			lstEquipmentTypeList = ddlEquipmentTypeList.stream().filter(p -> p.getEquipmentTypeId() == etype)
					.collect(Collectors.toList());

			if (lstEquipmentTypeList.size() > 0) {
				ddlEquipmentsList.get(i).setEquipmentTypeName(lstEquipmentTypeList.get(0).getEquipmentType());
			}

		}

		return ddlEquipmentsList;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getEquipmentCode
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns equipment code.
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	public String getEquipmentCode() {

		Equipment objEquipment = new Equipment();
		objEquipment = equipmentService.getEquipmentByMax("EquipmentCode");

		String EQ = "";

		if (objEquipment != null) {
			EQ = objEquipment.getEquipmentCode();
		}

		if (EQ == null || EQ == "") {
			EQ = "EQ00001";
		} else {
			try {
				int NextIndex = Integer.parseInt(EQ.replaceAll("EQ", "")) + 1;

				if (NextIndex >= 10000) {
					EQ = String.valueOf(NextIndex);
				} else if (NextIndex >= 1000) {
					EQ = "0" + String.valueOf(NextIndex);
				} else if (NextIndex >= 100) {
					EQ = "00" + String.valueOf(NextIndex);
				} else if (NextIndex >= 10) {
					EQ = "000" + String.valueOf(NextIndex);
				} else {
					EQ = "0000" + String.valueOf(NextIndex);
				}

				EQ = "EQ" + EQ;
			} catch (Exception e) {
				EQ = "EQ00001";
			}
		}

		return EQ;
	}

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentsPageload
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads equipments page.
	 * 
	 * Input Params : model,session,authentication.
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/equipments", method = RequestMethod.GET)
	public String EquipmentsPageload(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("equipment", new Equipment());

		model.addAttribute("equipmentList", getUpdatedEquipmentList());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));
		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("activeStatusList", getStatusDropdownList());

		return "equipment";
	}

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentUpdation
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function updates and equipment.
	 * 
	 * Input Params : EquipmentId,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/editequipment", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> EquipmentUpdation(Integer EquipmentId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			Equipment objequipment = equipmentService.getEquipmentById(EquipmentId);

			int EquipmentTypeID = objequipment.getEquipmentTypeID();

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			if (objequipment.getWarrantyDate() != null) {
				objequipment.setWarrantyDateText(sdf.format(objequipment.getWarrantyDate()));
			}
			if (objequipment.getInstallationDate() != null) {
				objequipment.setInstallationDateText(sdf.format(objequipment.getInstallationDate()));
			}

			objResponseEntity.put("equipmenttype", equipmenttypeService.getEquipmentTypeById(EquipmentTypeID));

			objResponseEntity.put("equipment", objequipment);
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : eqipmentCodeGenerator
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function generate equipment code by site ID.
	 * 
	 * Input Params : siteId
	 * 
	 * Return Value : String
	 * 
	 * 
	 **********************************************************************************************/
	public String eqipmentCodeGenerator(Integer siteId) {
		Equipment objEquipment = equipmentService.getEquipmentByMax(siteId.toString());
		Site site = siteService.getSiteById(siteId);

		String EQ = "";

		if (objEquipment != null) {
			EQ = objEquipment.getEquipmentCode();
		}

		String sitecode = site.getSiteCode().replaceAll("-", "");

		if (EQ == null || EQ == "") {
			EQ = sitecode + "EQ001";
		} else if (EQ.contains(sitecode)) {

			int NextIndex = Integer.parseInt(EQ.replaceAll(sitecode + "EQ", "")) + 1;

			if (NextIndex >= 100) {
				EQ = String.valueOf(NextIndex);
			} else if (NextIndex >= 10) {
				EQ = "0" + String.valueOf(NextIndex);
			} else {
				EQ = "00" + String.valueOf(NextIndex);
			}

			EQ = sitecode + "EQ" + EQ;

		} else {
			EQ = sitecode + "EQ" + "001";
		}

		return EQ;
	}

}
