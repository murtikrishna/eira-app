
/******************************************************
 * 
 *    	Filename	: EquipmentTypeController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals Equipment Type operations.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.CountryRegion;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentCategory;
import com.mestech.eampm.model.EquipmentType;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.EquipmentCategoryService;
import com.mestech.eampm.service.EquipmentTypeService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class EquipmentTypeController {

	@Autowired
	private EquipmentTypeService equipmenttypeService;

	@Autowired
	private EquipmentCategoryService equipmentcategoryService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private UserService userService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentTypePageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns all equipments with type.
	 * 
	 * Input Params  : session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/equipmenttype", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> EquipmentTypePageLoad(HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				objResponseEntity.put("equipmenttypeList", getUpdatedEquipmentTypeList());
				objResponseEntity.put("equipmentcategory", getDropdownList("EquipmentCategory"));

				objResponseEntity.put("activeStatusList", getStatusDropdownList());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : addNewEquipmentTypeSaveorUpdate
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function adds or update Equipment Type.
	 * 
	 * Input Params  : lstequipmenttype,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions    : ConstraintViolationException,Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/newequipmenttype", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> addNewEquipmentTypeSaveorUpdate(
			@RequestBody List<EquipmentType> lstequipmenttype, HttpSession session, Authentication authentication)
			throws ServletException {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String ResponseMessage = "";
		String Dataexists1 = "";
		String Dataexists2 = "";

		try {

			if (lstequipmenttype != null) {
				EquipmentType equipmenttype = lstequipmenttype.get(0);

				if (equipmenttype != null) {

					Date date = new Date();
					if (equipmenttype.getEquipmentTypeId() == null || equipmenttype.getEquipmentTypeId() == 0) {

						List<EquipmentType> lstequipments = equipmenttypeService.listallequipments(0);

						for (int i = 0; i < lstequipments.size(); i++) {
							String EquipmentType = lstequipments.get(i).getEquipmentType();

							if (equipmenttype.getEquipmentType().equals(EquipmentType)) {

								Dataexists1 = "Equipment Type Already Exist";

								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						equipmenttype.setEquipmentTypeCode((getEquipmentTypeCode()));
						equipmenttype.setCreationDate(date);
						equipmenttype.setCreatedBy(id);

						equipmenttypeService.addEquipmentType(equipmenttype);
					} else {
						EquipmentType objEquipmentType = equipmenttypeService
								.getEquipmentTypeById(equipmenttype.getEquipmentTypeId());

						List<EquipmentType> lstequipments = equipmenttypeService
								.listallequipments(equipmenttype.getEquipmentTypeId());

						for (int i = 0; i < lstequipments.size(); i++) {
							String EquipmentType = lstequipments.get(i).getEquipmentType();

							if (equipmenttype.getEquipmentType().equals(EquipmentType)) {

								Dataexists1 = "Equipment Type Already Exist";

								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						objEquipmentType.setEquipmentType(equipmenttype.getEquipmentType());
						objEquipmentType.setCategoryID(equipmenttype.getCategoryID());
						objEquipmentType.setDescription(equipmenttype.getDescription());
						objEquipmentType.setDisplayName(equipmenttype.getDisplayName());

						objEquipmentType.setRemarks(equipmenttype.getRemarks());
						objEquipmentType.setManufacturer(equipmenttype.getManufacturer());
						objEquipmentType.setActiveFlag(equipmenttype.getActiveFlag());

						objEquipmentType.setLastUpdatedDate(date);
						objEquipmentType.setLastUpdatedBy(id);

						equipmenttypeService.updateEquipmentType(objEquipmentType);

						objResponseEntity.put("status", true);
						objResponseEntity.put("equipmenttype", objEquipmentType);
						objResponseEntity.put("data", "Success");
						return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

					}
					objResponseEntity.put("status", true);
					objResponseEntity.put("equipmenttype", equipmenttype);
					ResponseMessage = "Success";
				} else {
					objResponseEntity.put("status", false);
					ResponseMessage = "Invalid Request";
					objResponseEntity.put("equipmenttype", null);
				}

			} else {
				objResponseEntity.put("status", false);
				ResponseMessage = "Invalid Request";
				objResponseEntity.put("equipmenttype", null);
			}

			objResponseEntity.put("data", ResponseMessage);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (ConstraintViolationException cve) {
			objResponseEntity.put("equipmenttype", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", cve.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			objResponseEntity.put("equipmenttype", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns values for dropdown.
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown site.  
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns values for dropdown.
	 * 
	 * Input Params  : DropdownName
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "EquipmentCategory") {
			List<EquipmentCategory> ddlList = equipmentcategoryService.listEquipmentCategories();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getCategoryId().toString(), ddlList.get(i).getEquipmentCategory());

			}

		} else if (DropdownName == "Site") {
			List<Site> ddlList = siteService.listSites();

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getUpdatedEquipmentTypeList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns list of equipment types.
	 * 
	 * Return Value  : List<EquipmentType>
	 * 
	 * 
	 **********************************************************************************************/
	public List<EquipmentType> getUpdatedEquipmentTypeList() {
		List<EquipmentType> ddlEquipmentTypesList = equipmenttypeService.listEquipmentTypes();

		for (int i = 0; i < ddlEquipmentTypesList.size(); i++) {
			String CategoryName = equipmentcategoryService
					.getEquipmentCategoryById(ddlEquipmentTypesList.get(i).getCategoryID()).getEquipmentCategory();
			ddlEquipmentTypesList.get(i).setCategoryName(CategoryName);
		}

		return ddlEquipmentTypesList;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getEquipmentTypeCode
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns equipment type code.
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	public String getEquipmentTypeCode() {
		String ET = equipmenttypeService.getEquipmentTypeByMax("EquipmentTypeCode").getEquipmentTypeCode();
		if (ET == null || ET == "") {
			ET = "ET00001";
		} else {
			try {
				int NextIndex = Integer.parseInt(ET.replaceAll("ET", "")) + 1;

				if (NextIndex >= 10000) {
					ET = String.valueOf(NextIndex);
				} else if (NextIndex >= 1000) {
					ET = "0" + String.valueOf(NextIndex);
				} else if (NextIndex >= 100) {
					ET = "00" + String.valueOf(NextIndex);
				} else if (NextIndex >= 10) {
					ET = "000" + String.valueOf(NextIndex);
				} else {
					ET = "0000" + String.valueOf(NextIndex);
				}

				ET = "ET" + ET;
			} catch (Exception e) {
				ET = "ET00001";
			}
		}

		return ET;
	}

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentTypesPageload
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads equipment types page.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/equipmenttypes", method = RequestMethod.GET)
	public String EquipmentTypesPageload(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("equipmenttype", new EquipmentType());

		model.addAttribute("activeStatusList", getStatusDropdownList());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));
		return "equipmenttype";
	}

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentTypeUpdation
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function updates equipment type.
	 * 
	 * Input Params  : EquipmentTypeId,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/editequipmenttype", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> EquipmentTypeUpdation(Integer EquipmentTypeId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			objResponseEntity.put("equipmenttype", equipmenttypeService.getEquipmentTypeById(EquipmentTypeId));
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : EquipmenttypeUpdation
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function updates equipment type.
	 * 
	 * Input Params  : SiteID,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getequipmenttype", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> EquipmenttypeUpdation(Integer SiteID, HttpSession session,
			Authentication authentication) {
		List<EquipmentType> objEquipment = new ArrayList<EquipmentType>();
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			objEquipment = equipmenttypeService.listFilterEquipmentsByTypeId(SiteID);

			objResponseEntity.put("equipmentList", objEquipment);
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("equipmentList", objEquipment);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}
}
