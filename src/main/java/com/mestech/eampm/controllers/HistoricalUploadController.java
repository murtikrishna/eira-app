
/******************************************************
 * 
 *    	Filename	: HistoricalUploadController.java
 *      
 *      Author		: Johnson Edwin Raj
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Historic Upload functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mestech.eampm.model.HistoricalUpload;
import com.mestech.eampm.service.HistoricalUploadService;
import com.mestech.eampm.utility.Response;

@Controller
public class HistoricalUploadController {
	@Autowired
	private Environment environment;

	@Autowired
	private HistoricalUploadService historicalUploadService;

	/********************************************************************************************
	 * 
	 * Function Name : uploadDocument
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function uploads historical document.
	 * 
	 * Input Params  : multipartFiles,directory,siteCode,siteName
	 * 
	 * Return Value  : Response
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/uploadHistorical", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Response uploadDocument(@RequestParam("files") MultipartFile[] multipartFiles,
			@RequestParam("directory") String directory, @RequestParam("siteCode") String siteCode,
			@RequestParam("siteName") String siteName) {
		Response response = new Response();
		try {
			if (multipartFiles != null && multipartFiles.length != 0) {
				for (MultipartFile multipartFile : multipartFiles) {
					byte[] bytes = multipartFile.getBytes();
					File file = new File(
							environment.getProperty("serverHistoricPath") + siteCode + File.separator + "DATA");
					if (file.isDirectory()) {
						boolean isWrite = false;
						String[] directorys = file.list();
						for (String dir : directorys) {
							if (dir.equals(directory)) {
								BufferedOutputStream stream = new BufferedOutputStream(
										new FileOutputStream(file.getAbsolutePath() + File.separator + directory
												+ File.separator + multipartFile.getOriginalFilename()));
								stream.write(bytes);
								stream.close();
								HistoricalUpload historicalUpload = new HistoricalUpload();
								historicalUpload.setDirectory(directory);
								historicalUpload.setSiteCode(siteCode);
								historicalUpload.setSiteName(siteName);
								historicalUpload.setUploadDate(new Date());
								historicalUpload.setFileName(multipartFile.getOriginalFilename());
								historicalUploadService.save(historicalUpload);
								isWrite = true;
							}
						}
						if (isWrite) {
							response.setStatus("Success");
							response.setData(null);
							return response;
						} else {
							response.setStatus("Failed");
							response.setData("Directory not exist");
							return response;
						}

					} else {
						response.setStatus("Failed");
						response.setData("Directory not exist");
						return response;
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();// TODO: handle exception
			response.setStatus("Failed");
		}
		return response;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getHistory
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns historic data.
	 * 
	 * Input Params  : directory,siteCode
	 * 
	 * Return Value  : Response
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getHistory", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Response getHistory(@RequestParam("directory") String directory,
			@RequestParam("siteCode") String siteCode) {
		Response response = new Response();
		try {

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			List<HistoricalUpload> lsthistoricdetails = historicalUploadService.getHistory(siteCode, directory);
			for (int i = 0; i < lsthistoricdetails.size(); i++) {

				lsthistoricdetails.get(i).setCreationDateText(sdf.format(lsthistoricdetails.get(i).getUploadDate()));
			}
			response.setStatus("success");
			response.setData(lsthistoricdetails);
		} catch (Exception e) {
			e.printStackTrace();// TODO: handle exception
			response.setStatus("Failed");
		}
		return response;
	}
}
