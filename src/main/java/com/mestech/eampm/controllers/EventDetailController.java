
/******************************************************
 * 
 *    	Filename	: EventDetailController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Event Details functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.DataExport;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.bean.TicketFilter;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.Currency;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.CustomerType;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.ErrorCode;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;
import com.mestech.eampm.model.SiteType;
import com.mestech.eampm.model.State;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.TicketDetailService;
import com.mestech.eampm.model.EventDetail;
import com.mestech.eampm.service.EventDetailService;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.CurrencyService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.ErrorCodeService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteTypeService;
import com.mestech.eampm.service.StateService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class EventDetailController {

	@Autowired
	private TicketDetailService ticketdetailService;

	@Autowired
	private ErrorCodeService errorcodeService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private EventDetailService eventdetailService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private SiteTypeService sitetypeService;

	@Autowired
	private CountryService countryService;

	@Autowired
	private StateService stateService;

	@Autowired
	private CurrencyService currencyService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : EventsPageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads event details page.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/eventdetails", method = RequestMethod.GET)
	public String EventsPageLoad(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("ticketfilter", new TicketFilter());
		model.addAttribute("eventdetail", new EventDetail());

		model.addAttribute("siteList", getNewTicketSiteList("Site", id));

		model.addAttribute("ticketcreation", new TicketDetail());

		return "eventdetail";
	}

	/********************************************************************************************
	 * 
	 * Function Name : EventGridLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function return event details.
	 * 
	 * Input Params  : session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/eventdetail", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> EventGridLoad(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<EventDetail> lstEventDetail = eventdetailService.getEventDetailListByUserId(id, timezonevalue);
				TicketFilter objTicketFilter = new TicketFilter();

				objResponseEntity.put("eventdetails", lstEventDetail);

				objResponseEntity.put("siteList", getNewTicketSiteList("Site", id));
				objResponseEntity.put("equipmentList", getDropdownList("Equipment", id));
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

			}

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : EventFilterPageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This fuction returns event filter details.
	 * 
	 * Input Params  : lstfilter,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/eventfilterdetail", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> EventFilterPageLoad(@RequestBody List<TicketFilter> lstfilter,
			HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();

		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			} else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				TicketFilter ticketfilter = lstfilter.get(0);

				Integer siteid = ticketfilter.getSiteID();
				Integer equipmentid = ticketfilter.getEquipmentname();
				String priority = ticketfilter.getPriority();
				String fromdate = ticketfilter.getFromDate();
				String todate = ticketfilter.getToDate();

				int varSiteID = 0;
				Integer varErrorId = 0;
				Integer varEventId = 0;
				Integer varEquipmentId = 0;
				String varPriority = "";
				Integer varSeverity = 0;
				String varErrorCode = "";
				String varErrorMessage = "";
				String varFromDate = "";
				String varToDate = "";

				if (ticketfilter != null) {
					if (ticketfilter.getSiteID() != null) {
						varSiteID = ticketfilter.getSiteID();
					}
					if (ticketfilter.getFromDate() != null) {
						varFromDate = ticketfilter.getFromDate();
					}
					if (ticketfilter.getToDate() != null) {
						varToDate = ticketfilter.getToDate();
					}
					if (ticketfilter.getEquipmentname() != null) {
						varEquipmentId = ticketfilter.getEquipmentname();
					}

					if (ticketfilter.getPriority() != null) {
						varPriority = ticketfilter.getPriority();
					}

				}

				List<EventDetail> lstEventDetail = eventdetailService.listEventDetailsForDisplay(id,
						String.valueOf(siteid), fromdate, todate, varEventId, varErrorId, String.valueOf(equipmentid),
						priority, varSeverity, timezonevalue);

				TicketFilter objTicketFilter = new TicketFilter();

				objTicketFilter.setSiteID(ticketfilter.getSiteID());
				objTicketFilter.setFromDate(ticketfilter.getFromDate());
				objTicketFilter.setToDate(ticketfilter.getToDate());
				objTicketFilter.setPriority(ticketfilter.getPriority());
				objTicketFilter.setEquipmentname(ticketfilter.getEquipmentname());

				objResponseEntity.put("siteList", getDropdownList("Site", id));
				objResponseEntity.put("eventdetaillist", lstEventDetail);

				objResponseEntity.put("equipmentList", getDropdownList("Equipment", id));

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : SiteEventsPageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads the site event details page.
	 * 
	 * Input Params  : id,model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/siteeventdetails{id}", method = RequestMethod.GET)
	public String SiteEventsPageLoad(@PathVariable("id") int id, Model model, HttpSession session,
			Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int userid = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(userid);

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(userid);
			objAccessListBean.setUserName(objUser.getShortName());

			objAccessListBean.setMySiteFilter(true);
			objAccessListBean.setMySiteID(id);

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("ticketfilter", new TicketFilter());

		model.addAttribute("eventdetail", new EventDetail());
		model.addAttribute("access", objAccessListBean);

		model.addAttribute("siteList", getNewTicketSiteList("Site", userid));

		model.addAttribute("ticketcreation", new TicketDetail());

		return "eventdetail";
	}

	/********************************************************************************************
	 * 
	 * Function Name : SiteEventDetailsGridLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function return value for event grid.
	 * 
	 * Input Params  : id,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/siteeventdetail", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> SiteEventDetailsGridLoad(Integer id, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int userid = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<EventDetail> lstEventDetail = eventdetailService.getEventDetailListBySiteId(id, timezonevalue);

				objResponseEntity.put("siteList", getNewTicketSiteList("SingleSite", id));
				objResponseEntity.put("eventdetaillist", lstEventDetail);

				objResponseEntity.put("equipmentList", getDropdownList("Equipment", userid));

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : SitefilterEventDetails
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function return event based on filter.
	 * 
	 * Input Params  : id,lstfilter,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/sitefiltereventdetails{id}", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> SitefilterEventDetails(@PathVariable("id") int id,
			@RequestBody List<TicketFilter> lstfilter, HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int userid = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				TicketFilter ticketfilter = lstfilter.get(0);

				Integer siteid = ticketfilter.getSiteID();
				Integer equipmentid = ticketfilter.getEquipmentname();
				String priority = ticketfilter.getPriority();
				String fromdate = ticketfilter.getFromDate();
				String todate = ticketfilter.getToDate();

				int varSiteID = 0;
				Integer varErrorId = 0;
				Integer varEventId = 0;
				Integer varEquipmentId = 0;
				String varPriority = "";
				Integer varSeverity = 0;
				String varErrorCode = "";
				String varErrorMessage = "";
				String varFromDate = "";
				String varToDate = "";

				List<EventDetail> lstEventDetail = eventdetailService.listEventDetailsForDisplay(userid,
						String.valueOf(id), fromdate, todate, varEventId, varErrorId, String.valueOf(equipmentid),
						priority, varSeverity, timezonevalue);

				TicketFilter objTicketFilter = new TicketFilter();

				objTicketFilter.setSiteID(ticketfilter.getSiteID());
				objTicketFilter.setFromDate(ticketfilter.getFromDate());
				objTicketFilter.setToDate(ticketfilter.getToDate());
				objTicketFilter.setPriority(ticketfilter.getPriority());
				objTicketFilter.setEquipmentname(ticketfilter.getEquipmentname());

				objResponseEntity.put("siteList", getNewTicketSiteList("SingleSite", id));

				objResponseEntity.put("eventdetaillist", lstEventDetail);
				objResponseEntity.put("ticketfilter", objTicketFilter);

				objResponseEntity.put("equipmentList", getDropdownList("Equipment", id));

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : CustomerEventsPageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns event based on customer.
	 * 
	 * Input Params  : id,model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customereventdetails{id}", method = RequestMethod.GET)
	public String CustomerEventsPageLoad(@PathVariable("id") int id, Model model, HttpSession session,
			Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int userid = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(userid);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(userid);
			objAccessListBean.setUserName(objUser.getShortName());

			objAccessListBean.setMyCustomerFilter(true);
			objAccessListBean.setMyCustomerID(id);

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("ticketfilter", new TicketFilter());

		model.addAttribute("eventdetail", new EventDetail());
		model.addAttribute("access", objAccessListBean);

		model.addAttribute("siteList", getNewTicketSiteList("Site", userid));
		model.addAttribute("ticketcreation", new TicketDetail());

		return "eventdetail";
	}

	/********************************************************************************************
	 * 
	 * Function Name : CustomerEventDetailsGridLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function return events based on customer.
	 * 
	 * Input Params  : id,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customereventdetail", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> CustomerEventDetailsGridLoad(Integer id, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int userid = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<EventDetail> lstEventDetail = eventdetailService.getEventDetailListByCustomerId(userid,
						timezonevalue);

				TicketFilter objTicketFilter = new TicketFilter();

				objResponseEntity.put("siteList", getNewTicketSiteList("Site", userid));
				objResponseEntity.put("eventdetaillist", lstEventDetail);
				objResponseEntity.put("ticketfilter", objTicketFilter);

				objResponseEntity.put("equipmentList", getDropdownList("Equipment", userid));

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : CustomerfilterEventDetails
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns events based on customer.
	 * 
	 * Input Params  : id,lstfilter,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customerfiltereventdetails{id}", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> CustomerfilterEventDetails(@PathVariable("id") int id,
			@RequestBody List<TicketFilter> lstfilter, HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int userid = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				TicketFilter ticketfilter = lstfilter.get(0);

				Integer siteid = ticketfilter.getSiteID();
				Integer equipmentid = ticketfilter.getEquipmentname();
				String priority = ticketfilter.getPriority();
				String fromdate = ticketfilter.getFromDate();
				String todate = ticketfilter.getToDate();

				int varSiteID = 0;
				Integer varErrorId = 0;
				Integer varEventId = 0;
				Integer varEquipmentId = 0;
				String varPriority = "";
				Integer varSeverity = 0;
				String varErrorCode = "";
				String varErrorMessage = "";
				String varFromDate = "";
				String varToDate = "";

				List<EventDetail> lstEventDetail = eventdetailService.listCustomerEventDetailsForDisplay(userid,
						String.valueOf(siteid), fromdate, todate, varEventId, varErrorId, String.valueOf(equipmentid),
						priority, varSeverity, timezonevalue);

				TicketFilter objTicketFilter = new TicketFilter();

				objTicketFilter.setSiteID(ticketfilter.getSiteID());
				objTicketFilter.setFromDate(ticketfilter.getFromDate());
				objTicketFilter.setToDate(ticketfilter.getToDate());
				objTicketFilter.setPriority(ticketfilter.getPriority());
				objTicketFilter.setEquipmentname(ticketfilter.getEquipmentname());

				objResponseEntity.put("siteList", getNewTicketSiteList("Site", userid));

				objResponseEntity.put("eventdetaillist", lstEventDetail);
				objResponseEntity.put("ticketfilter", objTicketFilter);

				objResponseEntity.put("equipmentList", getDropdownList("Equipment", id));

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		}
		if (DropdownName == "CustomerSites") {
			List<Site> ddlList = siteService.getSiteListByCustomerId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "SingleSite") {
			List<Site> ddlList = siteService.listSiteById(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "User") {
			List<User> ddlList = userService.listFieldUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		} else if (DropdownName == "EquipmentSingle") {
			// List<Equipment> ddlList = equipmentService.listEquipmentsByUserId(refid);
			List<Equipment> ddlList = equipmentService.listFilterInvertersBySiteId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getEquipmentId().toString(), ddlList.get(i).getCustomerNaming());
			}

		} else if (DropdownName == "Equipment") {
			// List<Equipment> ddlList = equipmentService.listEquipmentsByUserId(refid);
			List<Equipment> ddlList = equipmentService.listFilterInvertersByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getEquipmentId().toString(), ddlList.get(i).getCustomerNaming());
			}

		}

		else if (DropdownName == "ErrorCode") {

			List<ErrorCode> ddlList = errorcodeService.listErrorCodes();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getErrorMessage().toString(), ddlList.get(i).getErrorMessage());
			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : EventSupressUpdation
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns supressed event details.
	 * 
	 * Input Params  : id,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/supresseventdetails{id}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> EventSupressUpdation(@PathVariable("id") int id, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int userid = loginUserDetails.getEampmuserid();

			eventdetailService.removeEventDetail(id);

			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentUpdation
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns equipment against site.
	 * 
	 * Input Params  : SiteID,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getequipment", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> EquipmentUpdation(Integer SiteID, HttpSession session,
			Authentication authentication) {
		List<Equipment> objEquipment = new ArrayList<Equipment>();
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			objEquipment = equipmentService.listFilterInvertersBySiteId(SiteID);

			objResponseEntity.put("equipmentList", objEquipment);
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("equipmentList", objEquipment);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

}
