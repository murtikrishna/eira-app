
/******************************************************
 * 
 *    	Filename	: AnalysisController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Controller is for Analysis page.
 *      
 *      
 *******************************************************/

package com.mestech.eampm.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/*import com.ibm.icu.math.BigDecimal;*/
import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.AnalysisListBean;
import com.mestech.eampm.bean.EnergyPerformanceBean;
import com.mestech.eampm.bean.EquipmentListBean;
import com.mestech.eampm.bean.GraphDataBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.bean.SiteSummaryBean;
import com.mestech.eampm.bean.StandardParameterBean;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteSummary;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.DataTransactionService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteSummaryService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.UtilityCommon;
import com.mestech.eampm.utility.UtilityCommonDAO;

@Controller
public class AnalysisController {

	@Autowired
	private SiteService siteService;

	@Autowired
	private DataTransactionService dataTransactionService;

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	@Autowired
	private SiteSummaryService siteSummaryService;

	private UtilityCommon utilityCommon = new UtilityCommon();
	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();

	/********************************************************************************************
	 * 
	 * Function Name : GetCastedGraphDataBeanList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns casted GraphDataBean objects.
	 * 
	 * Input Params : lstGraphDataBean
	 * 
	 * Return Value : List<GraphDataBean>
	 * 
	 * Exceptions :
	 * 
	 * 
	 **********************************************************************************************/

	public static void main(String[] args) {

		String s = "Hi This is sudharsan";

		String[] arr = s.split(" ");

		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i] + " " + arr[i].length());
		}

	}

	private List<GraphDataBean> GetCastedGraphDataBeanList(List<GraphDataBean> lstGraphDataBean) {
		List<GraphDataBean> lstGraphDataBeanCasted = new ArrayList<GraphDataBean>();

		if (lstGraphDataBean.size() > 0) {

			Date Timestamp = null;
			Double Value = 0.00;
			for (Object object : lstGraphDataBean) {
				Object[] obj = (Object[]) object;

				Timestamp = utilityCommon.DateFromDateObject(obj[0]);
				Value = utilityCommon.DoubleFromBigDecimalObject(obj[1]);

				GraphDataBean objGraphDataBeanCasted = new GraphDataBean();

				objGraphDataBeanCasted.setTimestamp(Timestamp);
				objGraphDataBeanCasted.setValue(Value);

				lstGraphDataBeanCasted.add(objGraphDataBeanCasted);

			}
		}

		return lstGraphDataBeanCasted;
	}

	/********************************************************************************************
	 * 
	 * Function Name : JsonChartData
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function is used to generate graph of equipment with
	 * parameter.
	 * 
	 * Input Params :
	 * SiteID,EquipmentIDList,EquipmentIDWithParameterList,Range,FromDate.ToDate.GraphType,SummaryType,session
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/chartdatarequest", method = RequestMethod.POST)
	@ResponseBody
	public String JsonChartData(String SiteID, String EquipmentIDList, String EquipmentIDWithParameterList,
			String Range, String FromDate, String ToDate, String GraphType, String SummaryType, String Tag,
			HttpSession session) {
		String timezonevalue = "0";

		System.out.println(SiteID);
		System.out.println(EquipmentIDList);
		System.out.println(EquipmentIDWithParameterList);
		System.out.println(Range);
		System.out.println(FromDate);
		System.out.println(ToDate);
		System.out.println(GraphType);
		System.out.println(SummaryType);

		String ChartValue = "";
		String EquipmentIDWithParameter = "";

		Boolean bolAllSite = false;
		Boolean bolAllEquipment = false;
		Boolean bolAllParameter = false;

		EquipmentIDList = EquipmentIDList.replace("Inverter,", "");
		EquipmentIDList = EquipmentIDList.replace(",Inverter", "");
		EquipmentIDList = EquipmentIDList.replace("Sensor,", "");
		EquipmentIDList = EquipmentIDList.replace(",Sensor", "");
		EquipmentIDList = EquipmentIDList.replace("Energymeter,", "");
		EquipmentIDList = EquipmentIDList.replace(",Energymeter", "");
		EquipmentIDList = EquipmentIDList.replace("SMB,", "");
		EquipmentIDList = EquipmentIDList.replace(",SMB", "");
		EquipmentIDList = EquipmentIDList.replace("Tracker,", "");
		EquipmentIDList = EquipmentIDList.replace(",Tracker", "");

		try {

			if (!timezonevalue.equals("") && timezonevalue != null) {
				timezonevalue = session.getAttribute("timezonevalue").toString();
			}

			String lstEqu[] = EquipmentIDList.split(",");
			String lstParam[] = EquipmentIDWithParameterList.split(",");
			List<StandardParameterBean> beans = equipmentService.getStandardParameterByEquipmentIds("",
					lstEqu[1]);

			List<Equipment> lstEquipment = null;
			String EquipmentRefName = "";

			for (int i = 0; i < lstEqu.length; i++) {
				String Equ = lstEqu[i];
				lstEquipment = equipmentService.listEquipmentsByIds(Equ);
				EquipmentRefName = lstEquipment.get(0).getCustomerNaming();
				for (int j = 0; j < lstParam.length; j++) {
					String Param = lstParam[j];
					if ((EquipmentRefName.equalsIgnoreCase("Sensor") && Param.equalsIgnoreCase("Irradiation")) || (!EquipmentRefName.equalsIgnoreCase("Sensor") && !Param.equalsIgnoreCase("Irradiation"))) {
						if (!Param.equals("")) {
							if (EquipmentIDWithParameter.equals("")) {
								EquipmentIDWithParameter = Equ + "|" + Param;
							} else {
								EquipmentIDWithParameter = EquipmentIDWithParameter + "," + Equ + "|" + Param;
							}
						}
					}else {
                       continue;
					}
				}
			}

		} catch (Exception e) {
		}

		System.out.println("EquipmentIDWithParameter :: " + EquipmentIDWithParameter);
		EquipmentIDWithParameterList = EquipmentIDWithParameter;

		if (GraphType.equals("Daily Energy Generation") || GraphType.equals("1")) {
			if (Range.equals("daily")) {
				FromDate = FromDate + " 04:00:00";
				ToDate = ToDate + " 20:00:00";
			} else {
				FromDate = FromDate + " 00:00:00";
				ToDate = ToDate + " 23:59:59";
			}

			if (SummaryType.equals("Summary")) {
				ChartValue = SitewsieDailyEnergy(SiteID, FromDate, ToDate, Range, SummaryType, timezonevalue, Tag);
			} else {
				ChartValue = SitewsieDailyEnergy(SiteID, FromDate, ToDate, Range, SummaryType, timezonevalue, Tag);
			}
		} else if (GraphType.equals("Energy Performance") || GraphType.equals("2")) {
			FromDate = FromDate + " 04:00:00";
			ToDate = ToDate + " 20:00:00";

			if (SummaryType.equals("Summary")) {
				ChartValue = EquipmentwiseEnergyPerformanceSummary(SiteID, EquipmentIDList, FromDate, ToDate, Range,
						SummaryType, timezonevalue);
			} else {
				ChartValue = EquipmentwiseEnergyPerformanceSummary(SiteID, EquipmentIDList, FromDate, ToDate, Range,
						SummaryType, timezonevalue);
			}
		} else if (GraphType.equals("Parameter Comparison") || GraphType.equals("3")) {
			if (Range.equals("daily")) {
				FromDate = FromDate + " 04:00:00";
				ToDate = ToDate + " 20:00:00";
			} else {
				FromDate = FromDate + " 00:00:00";
				ToDate = ToDate + " 23:59:59";
			}
			System.out.println("Reached");
			ChartValue = EquipmentwiseParametersComparison(SiteID, EquipmentIDWithParameterList, FromDate, ToDate,
					Range, SummaryType, timezonevalue);
		}

		return ChartValue;
	}

	/********************************************************************************************
	 * 
	 * Function Name : GetDateInMilliSecond
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : return mills from a date.
	 * 
	 * Input Params : Timestamp
	 * 
	 * Return Value : String
	 * 
	 * 
	 * 
	 **********************************************************************************************/
	public String GetDateInMilliSecond(Date Timestamp) {
		String varChartDate = "";
		Calendar cal = Calendar.getInstance();
		cal.setTime(Timestamp);

		if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)
				&& utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
			// No Change
		} else { // For IST
			cal.add(Calendar.HOUR, 5);
			cal.add(Calendar.MINUTE, 30);
		}

		long millis = cal.getTime().getTime();
		varChartDate = String.valueOf(millis);

		return varChartDate;
	}

	/********************************************************************************************
	 * 
	 * Function Name : SitewsieDailyEnergy
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns daily energy of a site by date.
	 * 
	 * Input Params : SiteID,FromDate,ToDate,Range,SummaryType,timezonevalue
	 * 
	 * Return Value : String
	 * 
	 * Exception :Exception,ParseException
	 * 
	 * 
	 **********************************************************************************************/
	public String SitewsieDailyEnergy(String SiteID, String FromDate, String ToDate, String Range, String SummaryType,
			String timezonevalue, String Tag) {
		if (Range.equals("daily")) {
			SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

			String ChartJsonData1 = "";
			String ChartJsonData2 = "";
			String SiteName = "";
			List<EnergyPerformanceBean> lstDataTransaction = new ArrayList<EnergyPerformanceBean>();
			List<GraphDataBean> lstGraphDataIrradiation = new ArrayList<GraphDataBean>();

			if (Tag.equals("energy")) {
				lstDataTransaction = dataTransactionService.getEnergyDataBySiteIdsWithDate(SiteID, FromDate, ToDate,
						timezonevalue);
			}

			if (Tag.equals("irradiance")) {
				lstGraphDataIrradiation = dataTransactionService.getIrradiationValuesBySitesWithDate(SiteID, FromDate,
						ToDate, timezonevalue);

			}
			// System.out.println("Chart Value --- "+lstDataTransaction);

			// System.out.println("Irradiation Values --- "+lstGraphDataIrradiation);

			List<Site> lstSite = siteService.listSiteById(Integer.valueOf(SiteID));

			String YesterdayEnergy = dataTransactionService.getYesterdayEnergyBySiteIdsWithDate(SiteID, FromDate,
					ToDate, timezonevalue);
			Double dblYesterdayEnergyValue = Double.valueOf(YesterdayEnergy) * 1000.00;

			// System.out.println("YesterdayEnergy"+ YesterdayEnergy);

			String TodayEnergyStatus = "0";

			System.out.println("Testing start : " + new Date().toString());
			if (lstSite != null && lstSite.size() > 0) {
				TodayEnergyStatus = String.valueOf(lstSite.get(0).getTodayEnergyFlag());
				SiteName = lstSite.get(0).getSiteName() + " (" + lstSite.get(0).getCustomerReference() + ")";
			}
			System.out.println("Testing end : " + new Date().toString());

			List<GraphDataBean> lstGraphDataBean = new ArrayList<GraphDataBean>();

			Boolean bolInitialCheckFlag = false;
			Double dblInitialPeakValue = 0.0;

			System.out.println("Testing  start: " + new Date().toString());
			if (lstDataTransaction.size() > 0) {
				Integer iCnt = 0;

				for (Object object : lstDataTransaction) {
					Object[] obj = (Object[]) object;
					GraphDataBean objGraphDataBean = new GraphDataBean();
					objGraphDataBean.setTimestamp((Date) obj[0]);

					if (TodayEnergyStatus.equals("1")) {
						if (obj[2] == null) {
							objGraphDataBean.setValue(0.0);
						} else {
							Double dblTodayValue = ((java.math.BigDecimal) obj[2]).doubleValue() * 1000.00;

							if (iCnt.equals(0)) {
								objGraphDataBean.setValue(0.0);
								dblInitialPeakValue = dblTodayValue;
							} else if (bolInitialCheckFlag == false && dblInitialPeakValue.equals(dblTodayValue)) {
								objGraphDataBean.setValue(0.0);
							} else {
								objGraphDataBean.setValue(dblTodayValue);
								bolInitialCheckFlag = true;
							}

						}
					} else {
						if (obj[1] == null) {
							objGraphDataBean.setValue(0.0);
						} else {
							Double dblTotalValue = ((java.math.BigDecimal) obj[1]).doubleValue() * 1000.00;
							Double dblTodayValue = dblTotalValue - dblYesterdayEnergyValue;

							objGraphDataBean.setValue(dblTodayValue);

						}
					}

					if (objGraphDataBean.getTimestamp() != null) {

						try {

							lstGraphDataBean.add(objGraphDataBean);

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					iCnt = iCnt + 1;

				}

			}

			System.out.println("Testing End : " + new Date().toString());

			System.out.println("Testing Start : " + new Date().toString());
			for (int a = 0; a < lstGraphDataBean.size(); a++) {
				String varChartDate = sdfchart.format(lstGraphDataBean.get(a).getTimestamp()).toString();

				varChartDate = GetDateInMilliSecond(lstGraphDataBean.get(a).getTimestamp());

				Double dbTotalEnergy = lstGraphDataBean.get(a).getValue();
				Double dbTotalEnergyPrevious = lstGraphDataBean.get(a).getValue();

				Double dbTotalEnergyPresent = 0.0;
				if (a != 0) {
					if (sdfdate.format(lstGraphDataBean.get(a).getTimestamp()).toString()
							.equals(sdfdate.format(lstGraphDataBean.get(a - 1).getTimestamp()).toString())) {
						if (a == 1) {
							dbTotalEnergyPrevious = lstGraphDataBean.get(a - 1).getValue();
							dbTotalEnergyPresent = dbTotalEnergy - dbTotalEnergyPrevious;
						} else if (lstGraphDataBean.get(a - 1).getValue() >= lstGraphDataBean.get(a - 2).getValue()) {
							dbTotalEnergyPrevious = lstGraphDataBean.get(a - 1).getValue();
							dbTotalEnergyPresent = dbTotalEnergy - dbTotalEnergyPrevious;
						}

					}

				}

				String varChartValue = String.format("%.2f", dbTotalEnergyPresent);

				if (a == 0) {

					Date FirstDateTime = null;
					try {
						FirstDateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(FromDate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					String varFirstDate = GetDateInMilliSecond(FirstDateTime);

					if (varFirstDate.equals(varChartDate)) {
						ChartJsonData1 = "[" + varChartDate + "," + varChartValue + "]";
					} else {
						ChartJsonData1 = "[" + varFirstDate + ",0]," + "[" + varChartDate + "," + varChartValue + "]";
					}

				} else if (a == lstGraphDataBean.size() - 1) {

					Date FinalDateTime = null;

					try {
						FinalDateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(ToDate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					String varChartDateLast = GetDateInMilliSecond(FinalDateTime);

					if (varChartDateLast.equals(varChartDate)) {
						ChartJsonData1 = ChartJsonData1 + ",[" + varChartDate + "," + varChartValue + "]";
					} else {
						ChartJsonData1 = ChartJsonData1 + ",[" + varChartDate + "," + varChartValue + "]" + ",["
								+ varChartDateLast + ",null]";
					}

				} else {
					ChartJsonData1 = ChartJsonData1 + ",[" + varChartDate + "," + varChartValue + "]";
				}

			}

			System.out.println("Testing End : " + new Date().toString());

			if (lstGraphDataBean.size() == 1) {
				ChartJsonData1 = "";
			}

			System.out.println("Testing Start : " + new Date().toString());
			if (lstGraphDataIrradiation.size() > 0) {
				Integer b = 0;

				for (Object object : lstGraphDataIrradiation) {
					Object[] obj = (Object[]) object;
					GraphDataBean objGraphData = new GraphDataBean();
					objGraphData.setTimestamp((Date) obj[0]);
					Double dblIrradiationValue = ((java.math.BigDecimal) obj[1]).doubleValue();
					objGraphData.setValue(dblIrradiationValue);

					if (objGraphData.getTimestamp() != null) {
						String varChartDate = sdfchart.format(objGraphData.getTimestamp()).toString();

						try {

							varChartDate = GetDateInMilliSecond(objGraphData.getTimestamp());
							Double dbIrradiation = 0.0;
							if (objGraphData.getValue() != null) {
								dbIrradiation = objGraphData.getValue();
							}

							String varChartValue = "";

							varChartValue = String.format("%.2f", dbIrradiation);

							if (b == 0) {
								Date FirstDateTime = null;
								try {
									FirstDateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(FromDate);
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								String varFirstDate = GetDateInMilliSecond(FirstDateTime);

								if (varFirstDate.equals(varChartDate)) {
									ChartJsonData2 = "[" + varChartDate + "," + varChartValue + "]";
								} else {
									ChartJsonData2 = "[" + varFirstDate + ",0]," + "[" + varChartDate + ","
											+ varChartValue + "]";
								}

							} else {
								ChartJsonData2 = ChartJsonData2 + ",[" + varChartDate + "," + varChartValue + "]";
							}

							b = b + 1;

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					b = b + 1;

				}

			}

			System.out.println("Testing End : " + new Date().toString());

			String ChartJson = "";
//			if (ChartJsonData1.equals("") || ChartJsonData1.equals("[]")) {
//				ChartJson = "";
//			} else {
//				if (ChartJsonData2.equals("") || ChartJsonData2.equals("[]")) {
//					ChartJson = SiteName + "||" + "[" + ChartJsonData1 + "]";
//				} else {
//					ChartJson = SiteName + "||" + "[" + ChartJsonData1 + "]" + "&&" + "Irradiation" + "||" + "["
//							+ ChartJsonData2 + "]";
//				}
//			}

			if (Tag.equals("energy")) {
				if (ChartJsonData1.equals("") || ChartJsonData1.equals("[]")) {
					ChartJson = "";
				} else {
					ChartJson = SiteName + "||" + "[" + ChartJsonData1 + "]";
				}
			}
			if (Tag.equals("irradiance")) {
				if (ChartJsonData2.equals("") || ChartJsonData2.equals("[]")) {
					ChartJson = "";
				} else {
					ChartJson = "Irradiation" + "||" + "[" + ChartJsonData2 + "]";
				}
			}

			System.out.println("Json :: " + ChartJson);
			return ChartJson;
		} else {
			SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd");
			String ChartJsonData1 = "";
			String ChartJsonData2 = "";
			String SiteName = "";

			List<SiteSummary> lstSiteSummary = siteSummaryService.getSiteSummaryListBySiteIdWithDate(SiteID, FromDate,
					ToDate);
			List<GraphDataBean> lstGraphDataBean = dataTransactionService
					.getDaywiseIrradiationValuesBySitesWithDate(SiteID, FromDate, ToDate, timezonevalue);

			if (lstSiteSummary.size() > 0) {
				Integer a = 0;
				SiteSummaryBean objSiteSummaryBean = new SiteSummaryBean();
				for (Object object : lstSiteSummary) {
					Object[] obj = (Object[]) object;
					objSiteSummaryBean.setSiteID(utilityCommon.StringFromBigDecimalOrIntegerObject(obj[0]));

					if (a == 0) {
						SiteName = utilityCommon.StringFromStringObject(obj[1]);
						// SiteName = SiteName + " (Energy Generation)";
					}

					objSiteSummaryBean.setTimestamp((Date) obj[2]);
					objSiteSummaryBean.setTodayEnergy(utilityCommon.DoubleFromBigDecimalObject(obj[3]));

					if (objSiteSummaryBean.getTimestamp() != null) {
						String varChartDate = sdfchart.format(objSiteSummaryBean.getTimestamp()).toString();

						try {

							varChartDate = GetDateInMilliSecond(objSiteSummaryBean.getTimestamp());

							Double dbTodayEnergy = 0.0;
							if (objSiteSummaryBean.getTodayEnergy() != null) {
								dbTodayEnergy = objSiteSummaryBean.getTodayEnergy();
							}

							String varChartValue = "";

							varChartValue = String.format("%.2f", dbTodayEnergy * 1000);

							if (a == 0) {
								ChartJsonData1 = "[" + varChartDate + "," + varChartValue + "]";
							} else {
								ChartJsonData1 = ChartJsonData1 + ",[" + varChartDate + "," + varChartValue + "]";
							}

							a = a + 1;

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}

			}

			if (lstGraphDataBean.size() > 0) {
				Integer b = 0;

				for (Object object : lstGraphDataBean) {
					Object[] obj = (Object[]) object;
					GraphDataBean objGraphData = new GraphDataBean();
					objGraphData.setTimestamp((Date) obj[0]);
					Double dblIrradiationValue = ((java.math.BigDecimal) obj[1]).doubleValue();
					objGraphData.setValue(dblIrradiationValue);

					if (objGraphData.getTimestamp() != null) {
						String varChartDate = sdfchart.format(objGraphData.getTimestamp()).toString();

						try {

							Calendar cal = Calendar.getInstance();
							cal.setTime(objGraphData.getTimestamp());

							if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)
									&& utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
								// No Change
							} else { // For IST
								cal.add(Calendar.HOUR, 5);
								cal.add(Calendar.MINUTE, 30);
							}

							long millis = cal.getTime().getTime();
							varChartDate = String.valueOf(millis);

							Double dbIrradiation = 0.0;
							if (objGraphData.getValue() != null) {
								dbIrradiation = objGraphData.getValue();
							}

							String varChartValue = "";

							varChartValue = String.format("%.2f", dbIrradiation);

							if (b == 0) {
								ChartJsonData2 = "[" + varChartDate + "," + varChartValue + "]";
							} else {
								ChartJsonData2 = ChartJsonData2 + ",[" + varChartDate + "," + varChartValue + "]";
							}

							b = b + 1;

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					b = b + 1;

				}

			}

			String ChartJson = "";
			if (ChartJsonData1.equals("") || ChartJsonData1.equals("[]")) {
				ChartJson = "";
			} else {
				if (ChartJsonData2.equals("") || ChartJsonData2.equals("[]")) {
					ChartJson = SiteName + "||" + "[" + ChartJsonData1 + "]";
				} else {
					ChartJson = SiteName + "||" + "[" + ChartJsonData1 + "]" + "&&" + "Irradiation" + "||" + "["
							+ ChartJsonData2 + "]";
				}

			}

			System.out.println("Json :: " + ChartJson);
			return ChartJson;

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentwiseEnergyPerformanceSummary
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns Energy perfomance by equipment IDs and
	 * date.
	 * 
	 * Input Params :
	 * SiteID,EquipmentIDList,FromDate,ToDate,Range,SummaryType,timezonevalue
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception,ParseException
	 * 
	 * 
	 **********************************************************************************************/
	public String EquipmentwiseEnergyPerformanceSummary(String SiteID, String EquipmentIDList, String FromDate,
			String ToDate, String Range, String SummaryType, String timezonevalue) {

		SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

		String ChartJsonData1 = "";
		String ChartJsonData2 = "";
		String ChartJsonData1Parameter = "";
		String ChartJsonData1GraphValue = "";
		String SiteName = "";
		String EquipmentName = "";

		String EquipmentNameList = "";

		System.out.println("EquipmentIDList :: " + EquipmentIDList);

		if (EquipmentIDList.equals("") || EquipmentIDList.equals(null)) {
			EquipmentIDList = "0";
			System.out.println("EquipmentIDList1 :: " + EquipmentIDList);
		} else {
			System.out.println("EquipmentIDList2 :: " + EquipmentIDList);
		}

		List<EnergyPerformanceBean> lstDataTransaction = dataTransactionService
				.getEnergyDataByEquipmentIdsWithDate(EquipmentIDList, FromDate, ToDate, timezonevalue);
		List<Equipment> lstEquipment = equipmentService.listEquipmentsByIds(EquipmentIDList);
		List<GraphDataBean> lstGraphDataIrradiation = dataTransactionService.getIrradiationValuesBySitesWithDate(SiteID,
				FromDate, ToDate, timezonevalue);

		String YesterdayEnergy = dataTransactionService.getYesterdayEnergyByEquipmentIdsWithDate(EquipmentIDList,
				FromDate, ToDate, timezonevalue);
		Double dblYesterdayEnergyValue = Double.valueOf(YesterdayEnergy) * 1000.00;

		String TodayEnergyStatus = dataTransactionService.getTodayFlagStatus(EquipmentIDList, timezonevalue);

		List<Site> lstSite = siteService.listSiteById(Integer.valueOf(SiteID));

		if (lstSite != null && lstSite.size() > 0) {
			TodayEnergyStatus = String.valueOf(lstSite.get(0).getTodayEnergyFlag());
			SiteName = lstSite.get(0).getSiteName() + " (" + lstSite.get(0).getCustomerReference() + ")";

		}

		List<GraphDataBean> lstGraphDataBean = new ArrayList<GraphDataBean>();

		Boolean bolInitialCheckFlag = false;
		Double dblInitialPeakValue = 0.0;

		if (lstDataTransaction.size() > 0) {
			Integer iCnt = 0;

			for (Object object : lstDataTransaction) {
				Object[] obj = (Object[]) object;
				GraphDataBean objGraphDataBean = new GraphDataBean();
				objGraphDataBean.setTimestamp((Date) obj[0]);

				if (TodayEnergyStatus.equals("1")) {
					if (obj[2] == null) {
						objGraphDataBean.setValue(0.0);
					} else {
						Double dblTodayValue = ((java.math.BigDecimal) obj[2]).doubleValue() * 1000.00;

						if (iCnt.equals(0)) {
							objGraphDataBean.setValue(0.0);
							dblInitialPeakValue = dblTodayValue;
						} else if (bolInitialCheckFlag == false && dblInitialPeakValue.equals(dblTodayValue)) {
							objGraphDataBean.setValue(0.0);
						} else {
							objGraphDataBean.setValue(dblTodayValue);
							bolInitialCheckFlag = true;
						}

					}
				} else {
					if (obj[1] == null) {
						objGraphDataBean.setValue(0.0);
					} else {
						Double dblTotalValue = ((java.math.BigDecimal) obj[1]).doubleValue() * 1000.00;
						Double dblTodayValue = dblTotalValue - dblYesterdayEnergyValue;

						objGraphDataBean.setValue(dblTodayValue);

					}
				}

				if (objGraphDataBean.getTimestamp() != null) {

					try {

						lstGraphDataBean.add(objGraphDataBean);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				iCnt = iCnt + 1;

			}

		}

		for (int a = 0; a < lstGraphDataBean.size(); a++) {
			String varChartDate = sdfchart.format(lstGraphDataBean.get(a).getTimestamp()).toString();
			varChartDate = GetDateInMilliSecond(lstGraphDataBean.get(a).getTimestamp());

			Double dbTotalEnergy = lstGraphDataBean.get(a).getValue();
			Double dbTotalEnergyPrevious = lstGraphDataBean.get(a).getValue();

			Double dbTotalEnergyPresent = 0.0;
			if (a != 0) {
				if (sdfdate.format(lstGraphDataBean.get(a).getTimestamp()).toString()
						.equals(sdfdate.format(lstGraphDataBean.get(a - 1).getTimestamp()).toString())) {
					if (a == 1) {
						dbTotalEnergyPrevious = lstGraphDataBean.get(a - 1).getValue();
						dbTotalEnergyPresent = dbTotalEnergy - dbTotalEnergyPrevious;
					} else if (lstGraphDataBean.get(a - 1).getValue() >= lstGraphDataBean.get(a - 2).getValue()) {
						dbTotalEnergyPrevious = lstGraphDataBean.get(a - 1).getValue();
						dbTotalEnergyPresent = dbTotalEnergy - dbTotalEnergyPrevious;

					}

				}

			}

			String varChartValue = String.format("%.2f", dbTotalEnergyPresent);

			if (a == 0) {
				Date FirstDateTime = null;

				try {
					FirstDateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(FromDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String varFirstDate = GetDateInMilliSecond(FirstDateTime);

				if (varFirstDate.equals(varChartDate)) {
					ChartJsonData1 = "[" + varChartDate + "," + varChartValue + "]";
				} else {
					ChartJsonData1 = "[" + varFirstDate + ",0]," + "[" + varChartDate + "," + varChartValue + "]";
				}

			} else if (a == lstGraphDataBean.size() - 1) {

				Date FinalDateTime = null;

				try {
					FinalDateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(ToDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String varChartDateLast = GetDateInMilliSecond(FinalDateTime);

				if (varChartDateLast.equals(varChartDate)) {
					ChartJsonData1 = ChartJsonData1 + ",[" + varChartDate + "," + varChartValue + "]";
				} else {
					ChartJsonData1 = ChartJsonData1 + ",[" + varChartDate + "," + varChartValue + "]" + ",["
							+ varChartDateLast + ",null]";
				}

			} else {
				ChartJsonData1 = ChartJsonData1 + ",[" + varChartDate + "," + varChartValue + "]";

			}

		}

		if (lstGraphDataBean.size() == 1) {
			ChartJsonData1 = "";
		}

		if (lstGraphDataIrradiation.size() > 0) {
			Integer b = 0;

			for (Object object : lstGraphDataIrradiation) {
				Object[] obj = (Object[]) object;
				GraphDataBean objGraphData = new GraphDataBean();
				objGraphData.setTimestamp((Date) obj[0]);
				Double dblIrradiationValue = ((java.math.BigDecimal) obj[1]).doubleValue();
				objGraphData.setValue(dblIrradiationValue);

				if (objGraphData.getTimestamp() != null) {
					String varChartDate = sdfchart.format(objGraphData.getTimestamp()).toString();

					try {

						varChartDate = GetDateInMilliSecond(objGraphData.getTimestamp());

						Double dbIrradiation = 0.0;
						if (objGraphData.getValue() != null) {
							dbIrradiation = objGraphData.getValue();
						}

						String varChartValue = "";

						varChartValue = String.format("%.2f", dbIrradiation);

						if (b == 0) {
							Date FirstDateTime = null;

							try {
								FirstDateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(FromDate);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							String varFirstDate = GetDateInMilliSecond(FirstDateTime);

							if (varFirstDate.equals(varChartDate)) {
								ChartJsonData2 = "[" + varChartDate + "," + varChartValue + "]";
							} else {
								ChartJsonData2 = "[" + varFirstDate + ",0]," + "[" + varChartDate + "," + varChartValue
										+ "]";
							}

						} else {
							ChartJsonData2 = ChartJsonData2 + ",[" + varChartDate + "," + varChartValue + "]";
						}

						b = b + 1;

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				b = b + 1;

			}

		}

		String ChartJson = "";
		if (ChartJsonData1.equals("") || ChartJsonData1.equals("[]")) {
			ChartJson = "";
		} else {
			// lstEquipment

			if (ChartJsonData2.equals("") || ChartJsonData2.equals("[]")) {
				ChartJson = SiteName + "||" + "[" + ChartJsonData1 + "]";
			} else {
				ChartJson = SiteName + "||" + "[" + ChartJsonData1 + "]" + "&&" + "Irradiation" + "||" + "["
						+ ChartJsonData2 + "]";
			}

		}

		System.out.println("Json :: " + ChartJson);
		return ChartJson;

	}

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentwiseParametersComparison
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns Equipment wise parameter energy for
	 * graph.
	 * 
	 * Input Params :
	 * SiteID,EquipmentIDWithParameterList,FromDate,ToDate,Range,SummaryType,timezonevalue
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	public String EquipmentwiseParametersComparison(String SiteID, String EquipmentIDWithParameterList, String FromDate,
			String ToDate, String Range, String SummaryType, String timezonevalue) {
		SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

		Boolean blnIsRecordExist = false;
		String ChartJsonData = "";
		String ChartJsonDataParameter = "";
		String ChartJsonDataGraphValue = "";
		String SiteName = "";
		String EquipmentName = "";

		String Parameter = "";
		String ParameterAllChartData = "";

		if (EquipmentIDWithParameterList.equals("") != true && EquipmentIDWithParameterList != null) {
			// System.out.println("EquipmentIDWithParameterList :: " +
			// EquipmentIDWithParameterList);
			String lstEquipmentParameter[] = null;

			lstEquipmentParameter = EquipmentIDWithParameterList.split(",");

			if (lstEquipmentParameter != null) {

				// System.out.println("lstEquipmentParameter.length :: " +
				// lstEquipmentParameter.length);

				for (int z = 0; z < lstEquipmentParameter.length; z++) {
					String EquipmentParameter = lstEquipmentParameter[z].toString();
					String lstEP[] = null;
					lstEP = EquipmentParameter.split("\\|");

					// System.out.println("EquipmentParameter :: " + EquipmentParameter);
					// System.out.println("lstEP :: " + lstEP.length);

					String strEquipmentID = lstEP[0];
					String strParameterID = lstEP[1];

					// System.out.println("strEquipmentID :: " + strEquipmentID);
					// System.out.println("strParameterID :: " + strParameterID);

					String EquipmentID = "0";
					String EquipmentRefName = "0";

					List<Equipment> lstEquipment = equipmentService.listEquipmentsByIds(strEquipmentID);

					if (lstEquipment != null && lstEquipment.size() > 0) {
						EquipmentID = String.valueOf(lstEquipment.get(0).getEquipmentId());
						EquipmentRefName = lstEquipment.get(0).getCustomerNaming();
					}

					// System.out.println("EquipmentID :: " + EquipmentID);
					// System.out.println("EquipmentRefName :: " + EquipmentRefName);

					// System.out.println("z" + z);

					String ParameterChartData = "";

					List<GraphDataBean> lstDataTransaction = dataTransactionService
							.getParameterValuesByEquipmentIdsWithDate(strEquipmentID, strParameterID, FromDate, ToDate,
									timezonevalue);

					// List<GraphDataBean> lstDataTransaction =
					// GetCastedGraphDataBeanList(lstDataTransactionWithOutCast);

					if (lstDataTransaction.size() > 0) {
						Integer a = 0;

						for (Object object : lstDataTransaction) {
							Object[] obj = (Object[]) object;
							GraphDataBean objGraphDataBean = new GraphDataBean();

							objGraphDataBean.setTimestamp((Date) obj[0]);
							if (obj[1] == null) {
								objGraphDataBean.setValue(0.0);
							} else {
								// objGraphDataBean.setValue((Double) obj[1]);
								objGraphDataBean.setValue(utilityCommon.DoubleFromBigDecimalObject(obj[1]));

							}

							if (objGraphDataBean.getTimestamp() != null) {

								try {

									String varChartDate = sdfchart.format(objGraphDataBean.getTimestamp()).toString();

									Calendar cal = Calendar.getInstance();
									Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm")
											.parse(sdfchart.format(objGraphDataBean.getTimestamp()));
									cal.setTime(date1);

									if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)
											&& utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {
										// No Change
									} else { // For IST
										cal.add(Calendar.HOUR, 5);
										cal.add(Calendar.MINUTE, 30);
									}

									long millis = cal.getTime().getTime();
									varChartDate = String.valueOf(millis);

									Double dbValue = 0.0;
									dbValue = GetParameterValueInUnit(Parameter, objGraphDataBean.getValue());

									String varChartValue = String.format("%.2f", dbValue);

									if (a == 0) {
										blnIsRecordExist = true;
										ParameterChartData = "[" + varChartDate + "," + varChartValue + "]";
									} else {
										blnIsRecordExist = true;
										ParameterChartData = ParameterChartData + ",[" + varChartDate + ","
												+ varChartValue + "]";

									}

								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}

							a = a + 1;
						}

					}

					ParameterChartData = "[" + ParameterChartData + "]";

					// System.out.println("z:" + z);
					if (z == 0) {
						ParameterAllChartData = EquipmentRefName + "||" + GetParameterUOM(strParameterID) + "||"
								+ GetParameterUOMOnly(strParameterID) + "||" + GetParameterAxisGroup(strParameterID)
								+ "||" + ParameterChartData;
					} else {
						ParameterAllChartData = ParameterAllChartData + "&&" + EquipmentRefName + "||"
								+ GetParameterUOM(strParameterID) + "||" + GetParameterUOMOnly(strParameterID) + "||"
								+ GetParameterAxisGroup(strParameterID) + "||" + ParameterChartData;

					}

				}

				System.out.println("ParameterAllChartData:" + ParameterAllChartData);

			}

		}

		if (blnIsRecordExist == false) {
			ChartJsonData = "";
		} else if (ParameterAllChartData.equals("") == true) {
			ChartJsonData = "";
		} else if (EquipmentIDWithParameterList.equals("") == false) {
			ChartJsonData = ParameterAllChartData;
			// ChartJsonData = "0$$" + "Parameters" + "$$" + ParameterAllChartData;
		} else {
			ChartJsonData = "";
		}

		System.out.println(ChartJsonData);

		System.out.println("Commit by mohan");

		return ChartJsonData;

	}

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentVsParametersComparison
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns Equipment vs Parameter Comparison data
	 * for chart.
	 * 
	 * Input Params :
	 * SiteID,EquipmentIDWithParameterList,FromDate,ToDate,Range,SummaryType,timezonevalue
	 * 
	 * Return Value : String
	 * 
	 * 
	 **********************************************************************************************/
	public String EquipmentVsParametersComparison(String SiteID, String EquipmentIDWithParameterList, String FromDate,
			String ToDate, String Range, String SummaryType, String timezonevalue) {
		SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

		Boolean blnIsRecordExist = false;

		String ChartJsonData = "";
		String ChartJsonDataParameter = "";
		String ChartJsonDataGraphValue = "";
		String SiteName = "";
		String EquipmentName = "";

		String Parameter = "";
		String ParameterAllChartData = "";

		if (EquipmentIDWithParameterList.equals("") != true && EquipmentIDWithParameterList != null) {
			// System.out.println("EquipmentIDWithParameterList :: " +
			// EquipmentIDWithParameterList);
			String lstEquipmentParameter[] = null;

			lstEquipmentParameter = EquipmentIDWithParameterList.split(",");

			if (lstEquipmentParameter != null) {

				// System.out.println("lstEquipmentParameter.length :: " +
				// lstEquipmentParameter.length);

				for (int z = 0; z < lstEquipmentParameter.length; z++) {
					String EquipmentParameter = lstEquipmentParameter[z].toString();
					String lstEP[] = null;
					lstEP = EquipmentParameter.split("\\|");

					// System.out.println("EquipmentParameter :: " + EquipmentParameter);
					// System.out.println("lstEP :: " + lstEP.length);

					String strEquipmentID = lstEP[0];
					String strParameterID = lstEP[1];

					// System.out.println("strEquipmentID :: " + strEquipmentID);
					// System.out.println("strParameterID :: " + strParameterID);

					String EquipmentID = "0";
					String EquipmentRefName = "0";

					List<Equipment> lstEquipment = equipmentService.listEquipmentsByIds(strEquipmentID);

					if (lstEquipment != null && lstEquipment.size() > 0) {
						EquipmentID = String.valueOf(lstEquipment.get(0).getEquipmentId());
						EquipmentRefName = lstEquipment.get(0).getCustomerNaming();
					}

					// System.out.println("EquipmentID :: " + EquipmentID);
					// System.out.println("EquipmentRefName :: " + EquipmentRefName);

					// System.out.println("z" + z);

					String ParameterChartData = dataTransactionService.getParameterComparisionDateWithDate(
							strEquipmentID, strParameterID, FromDate, ToDate, timezonevalue);

					ParameterChartData = "[" + ParameterChartData + "]";

					// System.out.println("z:" + z);
					if (z == 0) {
						ParameterAllChartData = EquipmentRefName + "||" + GetParameterUOM(strParameterID) + "||"
								+ GetParameterUOMOnly(strParameterID) + "||" + GetParameterAxisGroup(strParameterID)
								+ "||" + ParameterChartData;
					} else {
						ParameterAllChartData = ParameterAllChartData + "&&" + EquipmentRefName + "||"
								+ GetParameterUOM(strParameterID) + "||" + GetParameterUOMOnly(strParameterID) + "||"
								+ GetParameterAxisGroup(strParameterID) + "||" + ParameterChartData;

					}

				}

				System.out.println("ParameterAllChartData:" + ParameterAllChartData);

			}

		}

		if (blnIsRecordExist == false) {
			ChartJsonData = "";
		} else if (ParameterAllChartData.equals("") == true) {
			ChartJsonData = "";
		} else if (EquipmentIDWithParameterList.equals("") == false) {
			ChartJsonData = ParameterAllChartData;
			// ChartJsonData = "0$$" + "Parameters" + "$$" + ParameterAllChartData;
		} else {
			ChartJsonData = "";
		}

		System.out.println(ChartJsonData);

		System.out.println("Commit by mohan");

		return ChartJsonData;

	}

	/********************************************************************************************
	 * 
	 * Function Name : GetParameterValueInUnit
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns parameter value in unit
	 * 
	 * Input Params : Paramerter,ParamerterValue
	 * 
	 * Return Value : Double
	 * 
	 * 
	 **********************************************************************************************/
	public Double GetParameterValueInUnit(String Paramerter, Double ParamerterValue) {
		if (Paramerter.equals("TodayEnergy") && Paramerter.equals("TotalEnergy")) {
			return ParamerterValue * 1000.00;
		} else {
			return ParamerterValue;
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : GetParameterUOM
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns parameter UOM with brckets by parameter.
	 * 
	 * Input Params : Paramerter
	 * 
	 * Return Value : String
	 * 
	 * 
	 **********************************************************************************************/
	public String GetParameterUOM(String Paramerter) {
		if (Paramerter.equals("InputCurrent_01") || Paramerter.equals("InputCurrent_02")
				|| Paramerter.equals("InputCurrent_03") || Paramerter.equals("InputCurrent_04")
				|| Paramerter.equals("InputCurrent_05") || Paramerter.equals("InputCurrent_06")
				|| Paramerter.equals("InputCurrent_07") || Paramerter.equals("InputCurrent_08")
				|| Paramerter.equals("InputCurrent_09") || Paramerter.equals("InputCurrent_10")
				|| Paramerter.equals("InputCurrent_11") || Paramerter.equals("InputCurrent_12")
				|| Paramerter.equals("InputCurrent_13") || Paramerter.equals("InputCurrent_14")
				|| Paramerter.equals("InputCurrent_15") || Paramerter.equals("InputCurrent_16")
				|| Paramerter.equals("InputCurrent_17") || Paramerter.equals("InputCurrent_18")
				|| Paramerter.equals("InputCurrent_19") || Paramerter.equals("InputCurrent_20")
				|| Paramerter.equals("InputCurrent_21") || Paramerter.equals("InputCurrent_22")
				|| Paramerter.equals("InputCurrent_23") || Paramerter.equals("InputCurrent_24")) {
			return Paramerter + " (A)";
		} else if (Paramerter.equals("InputVoltage_01") || Paramerter.equals("InputVoltage_02")
				|| Paramerter.equals("InputVoltage_03") || Paramerter.equals("InputVoltage_04")
				|| Paramerter.equals("InputVoltage_05") || Paramerter.equals("InputVoltage_06")
				|| Paramerter.equals("InputVoltage_07") || Paramerter.equals("InputVoltage_08")
				|| Paramerter.equals("InputVoltage_09") || Paramerter.equals("InputVoltage_10")
				|| Paramerter.equals("InputVoltage_11") || Paramerter.equals("InputVoltage_12")
				|| Paramerter.equals("InputVoltage_13") || Paramerter.equals("InputVoltage_14")
				|| Paramerter.equals("InputVoltage_15") || Paramerter.equals("InputVoltage_16")
				|| Paramerter.equals("InputVoltage_17") || Paramerter.equals("InputVoltage_18")
				|| Paramerter.equals("InputVoltage_19") || Paramerter.equals("InputVoltage_20")
				|| Paramerter.equals("InputVoltage_21") || Paramerter.equals("InputVoltage_22")
				|| Paramerter.equals("InputVoltage_23") || Paramerter.equals("InputVoltage_24")) {
			return Paramerter + " (V)";
		} else if (Paramerter.equals("InputPower_01") || Paramerter.equals("InputPower_02")
				|| Paramerter.equals("InputPower_03") || Paramerter.equals("InputPower_04")
				|| Paramerter.equals("InputPower_05") || Paramerter.equals("InputPower_06")
				|| Paramerter.equals("InputPower_07") || Paramerter.equals("InputPower_08")
				|| Paramerter.equals("InputPower_09") || Paramerter.equals("InputPower_10")
				|| Paramerter.equals("InputPower_11") || Paramerter.equals("InputPower_12")
				|| Paramerter.equals("InputPower_13") || Paramerter.equals("InputPower_14")
				|| Paramerter.equals("InputPower_15") || Paramerter.equals("InputPower_16")
				|| Paramerter.equals("InputPower_17") || Paramerter.equals("InputPower_18")
				|| Paramerter.equals("InputPower_19") || Paramerter.equals("InputPower_20")
				|| Paramerter.equals("InputPower_21") || Paramerter.equals("InputPower_22")
				|| Paramerter.equals("InputPower_23") || Paramerter.equals("InputPower_24")) {
			return Paramerter + " (W)";
		} else if (Paramerter.equals("PhaseCurrent") || Paramerter.equals("PhaseCurrent_L1")
				|| Paramerter.equals("PhaseCurrent_L2") || Paramerter.equals("PhaseCurrent_L3")) {
			return Paramerter + " (A)";
		} else if (Paramerter.equals("PhaseVoltage") || Paramerter.equals("PhaseVoltage_L1")
				|| Paramerter.equals("PhaseVoltage_L2") || Paramerter.equals("PhaseVoltage_L3")) {
			return Paramerter + " (V)";
		} else if (Paramerter.equals("ApparentPower")) {
			return Paramerter + " (VA)";
		} else if (Paramerter.equals("ActivePower")) {
			return Paramerter + " (W)";
		} else if (Paramerter.equals("ReactivePower")) {
			return Paramerter + " (VAr)";
		} else if (Paramerter.equals("TodayEnergy") && Paramerter.equals("TotalEnergy")) {
			return Paramerter + " (Wh)";
		} else if (Paramerter.equals("IsolationResistance")) {
			return Paramerter + " (Ohm)";
		} else if (Paramerter.equals("OutputFrequency")) {
			return Paramerter + " (Hz)";
		} else if (Paramerter.equals("AmbientTemperature") || Paramerter.equals("ModuleTemperature")
				|| Paramerter.equals("InverterTemperature") || Paramerter.equals("HeatSinkTemp")) {
			return Paramerter + " (�C)";
		} else if (Paramerter.equals("WindSpeed")) {
			return Paramerter + " (m/s)";
		} else if (Paramerter.equals("Rainfall")) {
			return Paramerter + " ";
		} else if (Paramerter.equals("TotalHoursOn")) {
			return Paramerter + " (hours)";
		} else if (Paramerter.equals("TodayHoursOn")) {
			return Paramerter + " (hours)";
		} else if (Paramerter.equals("PhasePowerBalancer")) {
			return Paramerter + " ";
		} else if (Paramerter.equals("DifferentialCurrent")) {
			return Paramerter + " (mA)";
		} else if (Paramerter.equals("Irradiation") || Paramerter.equals("GobalIrradiance")) {
			return Paramerter + " (W/m2)";
		} else if (Paramerter.equals("Irradiance")) {
			return Paramerter + " (Wh/m2)";
		} else if (Paramerter.equals("PhaseToPhaseVoltage_L1_L2") || Paramerter.equals("PhaseToPhaseVoltage_L1_L3")
				|| Paramerter.equals("PhaseToPhaseVoltage_L3_L1")) {
			return Paramerter + " (V)";
		} else if (Paramerter.equals("ApparentPower_L1") || Paramerter.equals("ApparentPower_L2")
				|| Paramerter.equals("ApparentPower_L3")) {
			return Paramerter + " (kVA)";
		} else if (Paramerter.equals("ActivePower_L1") || Paramerter.equals("ActivePower_L2")
				|| Paramerter.equals("ActivePower_L3")) {
			return Paramerter + " (kW)";
		} else if (Paramerter.equals("ReactivePower_L1") || Paramerter.equals("ReactivePower_L2")
				|| Paramerter.equals("ReactivePower_L3")) {
			return Paramerter + " (kVAr)";
		} else if (Paramerter.equals("ExportTotalEnergy") || Paramerter.equals("ImportTotalEnergy")
				|| Paramerter.equals("ExportActiveEvergy") || Paramerter.equals("ImportActiveEnergy")) {
			return Paramerter + " (kWh)";
		} else if (Paramerter.equals("ReactiveEnergyLAG") || Paramerter.equals("ReactiveEnergyLEAD")) {
			return Paramerter + " (kVAr)";
		} else if (Paramerter.equals("WindDirection") || Paramerter.equals("TrackerAngle")
				|| Paramerter.equals("SunAngle")) {
			return Paramerter + " (�)";
		} else if (Paramerter.equals("Humidity")) {
			return Paramerter + " (%)";
		} else if (Paramerter.equals("BatteryVoltage")) {
			return Paramerter + " (V)";
		} else if (Paramerter.equals("PhaseFrequency_L1") || Paramerter.equals("PhaseFrequency_L2")
				|| Paramerter.equals("PhaseFrequency_L3")) {
			return Paramerter + " (Hz)";
		} else if (Paramerter.equals("PhasePower_L1") || Paramerter.equals("PhasePower_L2")
				|| Paramerter.equals("PhasePower_L3")) {
			return Paramerter + " (kW)";
		} else if (Paramerter.equals("PhaseEnergy_L1") || Paramerter.equals("PhaseEnergy_L2")
				|| Paramerter.equals("PhaseEnergy_L3")) {
			return Paramerter + " (kWh)";
		}

		return Paramerter;

	}

	/********************************************************************************************
	 * 
	 * Function Name : GetParameterUOMOnly
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns parameter UOM by parameter.
	 * 
	 * Input Params : Paramerter
	 * 
	 * Return Value : String
	 * 
	 * 
	 **********************************************************************************************/
	public String GetParameterUOMOnly(String Paramerter) {

		if (Paramerter.equals("InputCurrent_01") || Paramerter.equals("InputCurrent_02")
				|| Paramerter.equals("InputCurrent_03") || Paramerter.equals("InputCurrent_04")
				|| Paramerter.equals("InputCurrent_05") || Paramerter.equals("InputCurrent_06")
				|| Paramerter.equals("InputCurrent_07") || Paramerter.equals("InputCurrent_08")
				|| Paramerter.equals("InputCurrent_09") || Paramerter.equals("InputCurrent_10")
				|| Paramerter.equals("InputCurrent_11") || Paramerter.equals("InputCurrent_12")
				|| Paramerter.equals("InputCurrent_13") || Paramerter.equals("InputCurrent_14")
				|| Paramerter.equals("InputCurrent_15") || Paramerter.equals("InputCurrent_16")
				|| Paramerter.equals("InputCurrent_17") || Paramerter.equals("InputCurrent_18")
				|| Paramerter.equals("InputCurrent_19") || Paramerter.equals("InputCurrent_20")
				|| Paramerter.equals("InputCurrent_21") || Paramerter.equals("InputCurrent_22")
				|| Paramerter.equals("InputCurrent_23") || Paramerter.equals("InputCurrent_24")) {
			return "A";
		} else if (Paramerter.equals("InputVoltage_01") || Paramerter.equals("InputVoltage_02")
				|| Paramerter.equals("InputVoltage_03") || Paramerter.equals("InputVoltage_04")
				|| Paramerter.equals("InputVoltage_05") || Paramerter.equals("InputVoltage_06")
				|| Paramerter.equals("InputVoltage_07") || Paramerter.equals("InputVoltage_08")
				|| Paramerter.equals("InputVoltage_09") || Paramerter.equals("InputVoltage_10")
				|| Paramerter.equals("InputVoltage_11") || Paramerter.equals("InputVoltage_12")
				|| Paramerter.equals("InputVoltage_13") || Paramerter.equals("InputVoltage_14")
				|| Paramerter.equals("InputVoltage_15") || Paramerter.equals("InputVoltage_16")
				|| Paramerter.equals("InputVoltage_17") || Paramerter.equals("InputVoltage_18")
				|| Paramerter.equals("InputVoltage_19") || Paramerter.equals("InputVoltage_20")
				|| Paramerter.equals("InputVoltage_21") || Paramerter.equals("InputVoltage_22")
				|| Paramerter.equals("InputVoltage_23") || Paramerter.equals("InputVoltage_24")) {
			return "V";
		} else if (Paramerter.equals("InputPower_01") || Paramerter.equals("InputPower_02")
				|| Paramerter.equals("InputPower_03") || Paramerter.equals("InputPower_04")
				|| Paramerter.equals("InputPower_05") || Paramerter.equals("InputPower_06")
				|| Paramerter.equals("InputPower_07") || Paramerter.equals("InputPower_08")
				|| Paramerter.equals("InputPower_09") || Paramerter.equals("InputPower_10")
				|| Paramerter.equals("InputPower_11") || Paramerter.equals("InputPower_12")
				|| Paramerter.equals("InputPower_13") || Paramerter.equals("InputPower_14")
				|| Paramerter.equals("InputPower_15") || Paramerter.equals("InputPower_16")
				|| Paramerter.equals("InputPower_17") || Paramerter.equals("InputPower_18")
				|| Paramerter.equals("InputPower_19") || Paramerter.equals("InputPower_20")
				|| Paramerter.equals("InputPower_21") || Paramerter.equals("InputPower_22")
				|| Paramerter.equals("InputPower_23") || Paramerter.equals("InputPower_24")) {
			return "W";
		} else if (Paramerter.equals("PhaseCurrent") || Paramerter.equals("PhaseCurrent_L1")
				|| Paramerter.equals("PhaseCurrent_L2") || Paramerter.equals("PhaseCurrent_L3")) {
			return "A";
		} else if (Paramerter.equals("PhaseVoltage") || Paramerter.equals("PhaseVoltage_L1")
				|| Paramerter.equals("PhaseVoltage_L2") || Paramerter.equals("PhaseVoltage_L3")) {
			return "V";
		} else if (Paramerter.equals("ApparentPower")) {
			return "VA";
		} else if (Paramerter.equals("ActivePower")) {
			return "W";
		} else if (Paramerter.equals("ReactivePower")) {
			return "VAr";
		} else if (Paramerter.equals("TodayEnergy") || Paramerter.equals("TotalEnergy")) {
			return "Wh";
		} else if (Paramerter.equals("IsolationResistance")) {
			return "Ohm";
		} else if (Paramerter.equals("OutputFrequency")) {
			return "Hz";
		} else if (Paramerter.equals("AmbientTemperature") || Paramerter.equals("ModuleTemperature")
				|| Paramerter.equals("InverterTemperature")) {
			return "�C";
		} else if (Paramerter.equals("WindSpeed")) {
			return "m/s";
		} else if (Paramerter.equals("Rainfall")) {
			return " ";
		} else if (Paramerter.equals("TotalHoursOn")) {
			return "hours";
		} else if (Paramerter.equals("TodayHoursOn")) {
			return "hours";
		} else if (Paramerter.equals("PhasePowerBalancer")) {
			return " ";
		} else if (Paramerter.equals("DifferentialCurrent")) {
			return "mA";
		} else if (Paramerter.equals("Irradiation") || Paramerter.equals("GobalIrradiance")) {
			return "W/m�";
		} else if (Paramerter.equals("Irradiance")) {
			return "Wh/m�";
		} else if (Paramerter.equals("PhaseToPhaseVoltage_L1_L2") || Paramerter.equals("PhaseToPhaseVoltage_L1_L3")
				|| Paramerter.equals("PhaseToPhaseVoltage_L3_L1")) {
			return "V";
		} else if (Paramerter.equals("ApparentPower_L1") || Paramerter.equals("ApparentPower_L2")
				|| Paramerter.equals("ApparentPower_L3")) {
			return "kVA";
		} else if (Paramerter.equals("ActivePower_L1") || Paramerter.equals("ActivePower_L2")
				|| Paramerter.equals("ActivePower_L3")) {
			return "kW";
		} else if (Paramerter.equals("ReactivePower_L1") || Paramerter.equals("ReactivePower_L2")
				|| Paramerter.equals("ReactivePower_L3")) {
			return "kVAr";
		} else if (Paramerter.equals("ExportTotalEnergy") || Paramerter.equals("ImportTotalEnergy")
				|| Paramerter.equals("ExportActiveEvergy") || Paramerter.equals("ImportActiveEnergy")) {
			return "kWh";
		} else if (Paramerter.equals("ReactiveEnergyLAG") || Paramerter.equals("ReactiveEnergyLEAD")) {
			return "kVAr";
		} else if (Paramerter.equals("WindDirection") || Paramerter.equals("TrackerAngle")
				|| Paramerter.equals("SunAngle")) {
			return "�";
		} else if (Paramerter.equals("Humidity")) {
			return "%";
		} else if (Paramerter.equals("BatteryVoltage")) {
			return "V";
		} else if (Paramerter.equals("PhaseFrequency_L1") || Paramerter.equals("PhaseFrequency_L2")
				|| Paramerter.equals("PhaseFrequency_L3")) {
			return "Hz";
		} else if (Paramerter.equals("PhasePower_L1") || Paramerter.equals("PhasePower_L2")
				|| Paramerter.equals("PhasePower_L3")) {
			return "kW";
		} else if (Paramerter.equals("PhaseEnergy_L1") || Paramerter.equals("PhaseEnergy_L2")
				|| Paramerter.equals("PhaseEnergy_L3")) {
			return "kWh";
		}

		return "";

	}

	/********************************************************************************************
	 * 
	 * Function Name : GetParameterAxisGroup
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns Axis Group by parameter.
	 * 
	 * Input Params : Paramerter
	 * 
	 * Return Value : String
	 * 
	 * 
	 **********************************************************************************************/
	public String GetParameterAxisGroup(String Paramerter) {
		if (Paramerter.equals("InputCurrent_01") || Paramerter.equals("InputCurrent_02")
				|| Paramerter.equals("InputCurrent_03") || Paramerter.equals("InputCurrent_04")
				|| Paramerter.equals("InputCurrent_05") || Paramerter.equals("InputCurrent_06")
				|| Paramerter.equals("InputCurrent_07") || Paramerter.equals("InputCurrent_08")
				|| Paramerter.equals("InputCurrent_09") || Paramerter.equals("InputCurrent_10")
				|| Paramerter.equals("InputCurrent_11") || Paramerter.equals("InputCurrent_12")
				|| Paramerter.equals("InputCurrent_13") || Paramerter.equals("InputCurrent_14")
				|| Paramerter.equals("InputCurrent_15") || Paramerter.equals("InputCurrent_16")
				|| Paramerter.equals("InputCurrent_17") || Paramerter.equals("InputCurrent_18")
				|| Paramerter.equals("InputCurrent_19") || Paramerter.equals("InputCurrent_20")
				|| Paramerter.equals("InputCurrent_21") || Paramerter.equals("InputCurrent_22")
				|| Paramerter.equals("InputCurrent_23") || Paramerter.equals("InputCurrent_24")) {
			return "Input Current";
		} else if (Paramerter.equals("InputVoltage_01") || Paramerter.equals("InputVoltage_02")
				|| Paramerter.equals("InputVoltage_03") || Paramerter.equals("InputVoltage_04")
				|| Paramerter.equals("InputVoltage_05") || Paramerter.equals("InputVoltage_06")
				|| Paramerter.equals("InputVoltage_07") || Paramerter.equals("InputVoltage_08")
				|| Paramerter.equals("InputVoltage_09") || Paramerter.equals("InputVoltage_10")
				|| Paramerter.equals("InputVoltage_11") || Paramerter.equals("InputVoltage_12")
				|| Paramerter.equals("InputVoltage_13") || Paramerter.equals("InputVoltage_14")
				|| Paramerter.equals("InputVoltage_15") || Paramerter.equals("InputVoltage_16")
				|| Paramerter.equals("InputVoltage_17") || Paramerter.equals("InputVoltage_18")
				|| Paramerter.equals("InputVoltage_19") || Paramerter.equals("InputVoltage_20")
				|| Paramerter.equals("InputVoltage_21") || Paramerter.equals("InputVoltage_22")
				|| Paramerter.equals("InputVoltage_23") || Paramerter.equals("InputVoltage_24")) {
			return "Input Voltage";
		} else if (Paramerter.equals("InputPower_01") || Paramerter.equals("InputPower_02")
				|| Paramerter.equals("InputPower_03") || Paramerter.equals("InputPower_04")
				|| Paramerter.equals("InputPower_05") || Paramerter.equals("InputPower_06")
				|| Paramerter.equals("InputPower_07") || Paramerter.equals("InputPower_08")
				|| Paramerter.equals("InputPower_09") || Paramerter.equals("InputPower_10")
				|| Paramerter.equals("InputPower_11") || Paramerter.equals("InputPower_12")
				|| Paramerter.equals("InputPower_13") || Paramerter.equals("InputPower_14")
				|| Paramerter.equals("InputPower_15") || Paramerter.equals("InputPower_16")
				|| Paramerter.equals("InputPower_17") || Paramerter.equals("InputPower_18")
				|| Paramerter.equals("InputPower_19") || Paramerter.equals("InputPower_20")
				|| Paramerter.equals("InputPower_21") || Paramerter.equals("InputPower_22")
				|| Paramerter.equals("InputPower_23") || Paramerter.equals("InputPower_24")) {
			return "Input Power";
		} else if (Paramerter.equals("PhaseCurrent") || Paramerter.equals("PhaseCurrent_L1")
				|| Paramerter.equals("PhaseCurrent_L2") || Paramerter.equals("PhaseCurrent_L3")) {
			return "Phase Current";
		} else if (Paramerter.equals("PhaseVoltage") || Paramerter.equals("PhaseVoltage_L1")
				|| Paramerter.equals("PhaseVoltage_L2") || Paramerter.equals("PhaseVoltage_L3")) {
			return "Phase Voltage";
		} else if (Paramerter.equals("ApparentPower")) {
			return "Output Power";
		} else if (Paramerter.equals("ActivePower")) {
			return "Output Power";
		} else if (Paramerter.equals("ReactivePower")) {
			return "Output Power";
		} else if (Paramerter.equals("TodayEnergy")) {
			return "Today Energy";
		} else if (Paramerter.equals("TotalEnergy")) {
			return "Total Energy";
		} else if (Paramerter.equals("IsolationResistance")) {
			return "Isolation Resistance";
		} else if (Paramerter.equals("OutputFrequency")) {
			return "Output Frequency";
		} else if (Paramerter.equals("AmbientTemperature") || Paramerter.equals("ModuleTemperature")
				|| Paramerter.equals("InverterTemperature")) {
			return "Temperature";
		} else if (Paramerter.equals("WindSpeed")) {
			return "Wind Speed";
		} else if (Paramerter.equals("Rainfall")) {
			return "Rainfall";
		} else if (Paramerter.equals("TotalHoursOn")) {
			return "Total Hours On";
		} else if (Paramerter.equals("TodayHoursOn")) {
			return "Today Hours On";
		} else if (Paramerter.equals("PhasePowerBalancer")) {
			return "Phase Power Balancer";
		} else if (Paramerter.equals("DifferentialCurrent")) {
			return "Differential Current";
		} else if (Paramerter.equals("Irradiation") || Paramerter.equals("GobalIrradiance")) {
			return "Irradiation";
		} else if (Paramerter.equals("Irradiance")) {
			return "Irradiance";
		}

		else if (Paramerter.equals("PhaseToPhaseVoltage_L1_L2") || Paramerter.equals("PhaseToPhaseVoltage_L1_L3")
				|| Paramerter.equals("PhaseToPhaseVoltage_L3_L1")) {
			return "Phase to Phase Voltage";
		} else if (Paramerter.equals("ApparentPower_L1") || Paramerter.equals("ApparentPower_L2")
				|| Paramerter.equals("ApparentPower_L3")) {
			return "Output Power";
		} else if (Paramerter.equals("ActivePower_L1") || Paramerter.equals("ActivePower_L2")
				|| Paramerter.equals("ActivePower_L3")) {
			return "Output Power";
		} else if (Paramerter.equals("ReactivePower_L1") || Paramerter.equals("ReactivePower_L2")
				|| Paramerter.equals("ReactivePower_L3")) {
			return "Output Power";
		} else if (Paramerter.equals("ExportTotalEnergy") || Paramerter.equals("ImportTotalEnergy")
				|| Paramerter.equals("ExportActiveEvergy") || Paramerter.equals("ImportActiveEnergy")) {
			return "Energy";
		} else if (Paramerter.equals("ReactiveEnergyLAG") || Paramerter.equals("ReactiveEnergyLEAD")) {
			return "Output Power";
		} else if (Paramerter.equals("WindDirection") || Paramerter.equals("TrackerAngle")
				|| Paramerter.equals("SunAngle")) {
			return "Angle";
		} else if (Paramerter.equals("Humidity")) {
			return "Humidity";
		} else if (Paramerter.equals("BatteryVoltage")) {
			return "Battery Voltage";
		} else if (Paramerter.equals("PhaseFrequency_L1") || Paramerter.equals("PhaseFrequency_L2")
				|| Paramerter.equals("PhaseFrequency_L3")) {
			return "Phase Frequency";
		} else if (Paramerter.equals("PhasePower_L1") || Paramerter.equals("PhasePower_L2")
				|| Paramerter.equals("PhasePower_L3")) {
			return "Phase Power";
		} else if (Paramerter.equals("PhaseEnergy_L1") || Paramerter.equals("PhaseEnergy_L2")
				|| Paramerter.equals("PhaseEnergy_L3")) {
			return "Phase Energy";
		}

		return "";

	}

	/********************************************************************************************
	 * 
	 * Function Name : AnalysisPageLoad
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads the analysis page.
	 * 
	 * Input Params : model,session,authentication
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/analysis", method = RequestMethod.GET)
	public String AnalysisPageLoad(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		EquipmentListBean objEquipmentListBean = new EquipmentListBean();

		String ChartJsonData1 = "";
		String ChartJsonData1Parameter = "";
		String ChartJsonData1GraphValue = "";

		SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// SimpleDateFormat sdfchart = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

		String varEquipmentRef = "";
		String varSiteID = "";
		String varEquipmentID = "";
		String varEquipmentCode = "";
		String varEquipmentName = "";
		String varEquipmentType = "";

		List<Customer> lstCustomer = customerService.getCustomerListByUserId(id);
		List<Site> lstSite = siteService.getProductionSiteListByUserId(id);
		List<Equipment> lstEquipment = equipmentService.listEquipmentsByUserId(id);
		List<Equipment> lstInverter = equipmentService.listInvertersByUserId(id);
		List<Equipment> lstSensor = equipmentService.listSensorsByUserId(9999);
		List<Equipment> lstEnergymeters = equipmentService.listEnergymeterByUserId(id);
		List<Equipment> lstSmbs = equipmentService.listScbByUserId(id);

		System.out.println("mohan sensor size : " + lstSensor.size());

		for (int a = 0; a < lstSite.size(); a++) {
			Integer varStrSiteID = lstSite.get(a).getSiteId();
			String varStrSiteRefCode = lstSite.get(a).getCustomerNaming();

			lstEquipment.stream().filter(p -> p.getSiteID().equals(varStrSiteID)).collect(Collectors.toList())
					.forEach(p -> p.setSiteReference(varStrSiteRefCode));
			lstInverter.stream().filter(p -> p.getSiteID().equals(varStrSiteID)).collect(Collectors.toList())
					.forEach(p -> p.setSiteReference(varStrSiteRefCode));
			lstSensor.stream().filter(p -> p.getSiteID().equals(varStrSiteID)).collect(Collectors.toList())
					.forEach(p -> p.setSiteReference(varStrSiteRefCode));
			lstEnergymeters.stream().filter(p -> p.getSiteID().equals(varStrSiteID)).collect(Collectors.toList())
					.forEach(p -> p.setSiteReference(varStrSiteRefCode));
			lstSmbs.stream().filter(p -> p.getSiteID().equals(varStrSiteID)).collect(Collectors.toList())
					.forEach(p -> p.setSiteReference(varStrSiteRefCode));

		}

		System.out.println("mohan sensor size : " + lstSensor.size());

		objEquipmentListBean.setChartJsonData1(ChartJsonData1);
		objEquipmentListBean.setChartJsonData1Parameter(ChartJsonData1Parameter);
		objEquipmentListBean.setChartJsonData1GraphValue(ChartJsonData1GraphValue);

		System.out.println(ChartJsonData1);

		String Irradiationunits = "W/m�";
		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));

		model.addAttribute("equipmentview", objEquipmentListBean);
		model.addAttribute("analysis", new AnalysisListBean());
		model.addAttribute("ticketcreation", new TicketDetail());

		model.addAttribute("lstCustomer", lstCustomer);
		model.addAttribute("lstSite", lstSite);

		model.addAttribute("lstInverter", lstInverter);
		model.addAttribute("lstSensor", lstSensor);
		model.addAttribute("lstEnergymeters", lstEnergymeters);
		model.addAttribute("lstSmbs", lstSmbs);
		model.addAttribute("Irradiationunits", Irradiationunits);

		return "analysis";
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns dropdown values for
	 * Site,ProductionSites,User
	 * 
	 * Input Params : DropdownName,refid
	 * 
	 * Return Value : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "ProductionSites") {
			List<Site> ddlList = siteService.getProductionSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		}

		else if (DropdownName == "User") {
			List<User> ddlList = userService.listFieldUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns map of Site and its IDs.
	 * 
	 * Input Params : DropdownName,refid
	 * 
	 * Return Value : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}
}
