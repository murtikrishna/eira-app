
/******************************************************
 * 
 *    	Filename	: UserRoleController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with user role activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.model.UserRole;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class UserRoleController {

	@Autowired
	private UserRoleService userroleservice;

	@Autowired
	private UserService userService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : CustomerGridLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns user role data.
	 * 
	 * Input Params  : session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/userrole", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> CustomerGridLoad(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				objResponseEntity.put("userrole", new UserRole());
				objResponseEntity.put("siteList", getDropdownList("Site"));
				objResponseEntity.put("userroleList", userroleservice.listUserRoles());
				objResponseEntity.put("activeStatusList", getStatusDropdownList());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : NewUserRoleSaveorUpdate
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This funcion adds or updates user role.
	 * 
	 * Input Params  : lstuserrole,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions    : ConstraintViolationException,Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/newuserrole", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> NewUserRoleSaveorUpdate(@RequestBody List<UserRole> lstuserrole,
			HttpSession session, Authentication authentication) {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();

		String ResponseMessage = "";
		String Dataexists1 = "";

		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();
			if (lstuserrole != null) {
				UserRole userrole = lstuserrole.get(0);

				if (userrole != null) {

					Date date = new Date();
					if (userrole.getRoleId() == null || userrole.getRoleId() == 0) {

						List<UserRole> lstroles = userroleservice.listRoles(0);

						for (int i = 0; i < lstroles.size(); i++) {
							String Rolename = lstroles.get(i).getRoleName();

							if (userrole.getRoleName().equals(Rolename)) {

								Dataexists1 = "RoleName Already Exist";
								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						userrole.setCreationDate(date);
						userrole.setCreatedBy(id);

						userroleservice.addUserRole(userrole);
					} else {
						UserRole objuserrole = userroleservice.getUserRoleById(userrole.getRoleId());
						List<UserRole> lstroles = userroleservice.listRoles(userrole.getRoleId());

						for (int i = 0; i < lstroles.size(); i++) {
							String Rolename = lstroles.get(i).getRoleName();

							if (userrole.getRoleName().equals(Rolename)) {

								Dataexists1 = "RoleName Already Exist";
								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						objuserrole.setRoleName(userrole.getRoleName());
						objuserrole.setRoleDescription(userrole.getRoleDescription());
						objuserrole.setLastUpdatedBy(id);
						objuserrole.setLastUpdatedDate(date);

						objuserrole.setActiveFlag(userrole.getActiveFlag());

						userroleservice.updateUserRole(objuserrole);

						objResponseEntity.put("status", true);
						objResponseEntity.put("userrole", objuserrole);
						objResponseEntity.put("data", "Success");
						return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

					}
					objResponseEntity.put("status", true);
					objResponseEntity.put("userrole", userrole);
					ResponseMessage = "Success";
				} else {
					objResponseEntity.put("status", false);
					ResponseMessage = "Invalid Request";
					objResponseEntity.put("userrole", null);
				}

			} else {
				objResponseEntity.put("status", false);
				ResponseMessage = "Invalid Request";
				objResponseEntity.put("userrole", null);
			}

			objResponseEntity.put("data", ResponseMessage);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (ConstraintViolationException cve) {
			objResponseEntity.put("userrole", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", cve.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			objResponseEntity.put("userrole", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : UserrolesPageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads user role page.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/userroles", method = RequestMethod.GET)
	public String UserrolesPageLoad(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getUserName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("userrole", new UserRole());
		model.addAttribute("siteList", getDropdownList("Site"));
		model.addAttribute("userroleList", userroleservice.listUserRoles());
		model.addAttribute("activeStatusList", getStatusDropdownList());
		model.addAttribute("ticketcreation", new TicketDetail());

		return "userrole";
	}

	/********************************************************************************************
	 * 
	 * Function Name : UserRoleUpdation
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This funciton edit user role.
	 * 
	 * Input Params  : RoleId,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : ResponseEntity<Map<String,Object>>
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/edituserrole", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> UserRoleUpdation(Integer RoleId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			objResponseEntity.put("userrole", userroleservice.getUserRoleById(RoleId));
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Input Params  : DropdownName
	 * 
	 * Return Value  : Map<String,String> 
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.listSites();

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				/*
				 * if(ddlList.get(i).getCustomerReference() != null) { SiteName = SiteName +
				 * " - " + ddlList.get(i).getCustomerReference(); }
				 */

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}

		return ddlMap;
	}
}
