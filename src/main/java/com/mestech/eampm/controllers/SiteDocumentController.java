
/******************************************************
 * 
 *    	Filename	: SiteDocumentController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Site Document functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteDocumentation;
import com.mestech.eampm.service.SiteDocumentationService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.utility.Response;

@Controller
@RequestMapping(value = "sitedocument")
public class SiteDocumentController {

	@Autowired
	private SiteDocumentationService siteDocumentationService;
	@Autowired
	private Environment environment;
	@Autowired
	private SiteService siteService;

	/********************************************************************************************
	 * 
	 * Function Name : uploadDocument
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function uploads document against site.
	 * 
	 * Input Params  : multipartFiles,siteId,docType
	 * 
	 * Return Value  : Response
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/uploadDoc", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Response uploadDocument(@RequestParam("files") MultipartFile[] multipartFiles,
			@RequestParam("siteId") String siteId, @RequestParam("docType") String docType) {
		Response response = new Response();
		List<SiteDocumentation> siteDocumentations = new ArrayList<>();
		try {
			if (multipartFiles != null && multipartFiles.length != 0) {
				for (MultipartFile multipartFile : multipartFiles) {
					byte[] bytes = multipartFile.getBytes();
					String tomcatRootPath = System.getProperty("catalina.home");
					if (siteId != null) {
						Site site = siteService.getSiteById(Integer.parseInt(siteId));
						if (site != null) {
							String rootPath = environment.getProperty("document.rootpath");
							File dir = new File(tomcatRootPath + File.separator + "webapps" + File.separator + rootPath
									+ File.separator + site.getSiteCode() + File.separator + docType);
							if (!dir.exists())
								dir.mkdirs();
							UUID uuid = UUID.randomUUID();
							String[] fileTypeSpliter = multipartFile.getOriginalFilename().split("\\.(?=[^\\.]+$)");
							String fileName = fileTypeSpliter[0].replaceAll("\\s+", "");
							File serverFile = new File(dir.getAbsolutePath() + File.separator
									+ fileName.concat(uuid.toString()).concat(".").concat(fileTypeSpliter[1]));
							BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
							stream.write(bytes);
							stream.close();
							SiteDocumentation siteDocumentation = new SiteDocumentation();
							siteDocumentation.setCreateddate(new Date());
							siteDocumentation
									.setDirectoryPath(site.getSiteCode() + File.separator + docType + File.separator);
							siteDocumentation.setServerFileName(
									fileName.concat(uuid.toString()).concat(".").concat(fileTypeSpliter[1]));
							siteDocumentation.setFileName(fileTypeSpliter[0]);
							siteDocumentation.setFileType(fileTypeSpliter[1]);
							siteDocumentation.setPrefixpath(rootPath + File.separator);
							siteDocumentation.setSiteCode(site.getSiteCode());
							siteDocumentation.setSiteName(site.getSiteName());
							DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
							siteDocumentation.setDateCreated(dateFormat.format(siteDocumentation.getCreateddate()));
							siteDocumentation.setActiveflag(new Integer(1));
							siteDocumentation.setSiteId(Integer.parseInt(siteId));
							siteDocumentation.setDocCategory(docType);
							siteDocumentation = siteDocumentationService.save(siteDocumentation);
							siteDocumentations.add(siteDocumentation);
						}

					}

				}
				response.setData(siteDocumentations);
				response.setStatus("Success");
			} else {
				response.setStatus("Failed");
				response.setData(null);
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			response.setData(null);
			response.setStatus("Failed");
			return response;
		}
	}

	/********************************************************************************************
	 * 
	 * Function Name : deleteDocument
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function deletes a document.
	 * 
	 * Input Params  : siteCode,docCategory,docName
	 * 
	 * Return Value  : Response
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/deleteDocument", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Response deleteDocument(@RequestParam("siteCode") String siteCode,
			@RequestParam("docCategory") String docCategory, @RequestParam("docName") String docName) {
		Response response = new Response();
		try {
			String[] fileTypeSpliter = docName.split("\\.(?=[^\\.]+$)");
			siteDocumentationService.deleteDocuments(siteCode, docCategory, fileTypeSpliter[0]);
			response.setStatus("Success");
			return response;
		} catch (Exception e) {
			response.setData(null);
			response.setStatus("Failed");
			return response;
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getAllSites
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns documents againt a site.
	 * 
	 * Return Value  : Response
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getAllSitesAgainstSiteDocument", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Response getAllSites() {
		Response response = new Response();
		try {
			List<SiteDocumentation> siteDocumentations = siteDocumentationService.listAll();
			if (siteDocumentations != null && !siteDocumentations.isEmpty()) {
				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				for (SiteDocumentation documentation : siteDocumentations) {
					String createdDate = dateFormat.format(documentation.getCreateddate());
					documentation.setDateCreated(createdDate);
				}
				response.setData(siteDocumentations);
				response.setStatus("Success");
			}
			return response;
		} catch (Exception e) {
			response.setData(null);
			response.setStatus("Failed");
			return response;
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : deleteDocument
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns site document by site id.
	 * 
	 * Input Params  : siteId
	 * 
	 * Return Value  : Response
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getBySiteId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Response deleteDocument(@RequestParam("siteId") String siteId) {
		Response response = new Response();
		try {
			response.setStatus("Success");
			List<SiteDocumentation> siteDocumentations = siteDocumentationService
					.findBySiteId(Integer.parseInt(siteId));
			if (siteDocumentations != null && !siteDocumentations.isEmpty()) {
				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				for (SiteDocumentation documentation : siteDocumentations) {
					String createdDate = dateFormat.format(documentation.getCreateddate());
					documentation.setDateCreated(createdDate);
				}
			}
			response.setData(siteDocumentations);
			return response;
		} catch (Exception e) {
			response.setData(null);
			response.setStatus("Failed");
			return response;
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getdocumentsbysite
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns docuemnt by site and category.
	 * 
	 * Input Params  : siteId,category
	 * 
	 * Return Value  : Response
	 *  
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getDocumentBySiteCategory", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Response getdocumentsbysite(@RequestParam("siteId") String siteId,
			@RequestParam("category") String category) {
		Response response = new Response();
		try {
			response.setStatus("Success");
			List<SiteDocumentation> siteDocumentations = siteDocumentationService.findBySiteId(Integer.parseInt(siteId),
					category);
			if (siteDocumentations != null && !siteDocumentations.isEmpty()) {
				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				for (SiteDocumentation documentation : siteDocumentations) {
					String createdDate = dateFormat.format(documentation.getCreateddate());
					documentation.setDateCreated(createdDate);
				}
			}
			response.setData(siteDocumentations);
			return response;
		} catch (Exception e) {
			response.setData(null);
			response.setStatus("Failed");
			return response;
		}

	}

}
