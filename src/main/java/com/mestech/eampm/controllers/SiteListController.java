
/******************************************************
 * 
 *    	Filename	: SiteListController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Site related functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.CustomerViewBean;
import com.mestech.eampm.bean.DashboardEnergySummaryBean;
import com.mestech.eampm.bean.DashboardOandMBean;
import com.mestech.eampm.bean.DashboardSiteBean;
import com.mestech.eampm.bean.DashboardSiteListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.bean.SiteListBean;
import com.mestech.eampm.bean.SiteViewBean;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.DataSource;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.DataSourceService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteStatusService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.UtilityCommon;

@Controller
public class SiteListController {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private SiteStatusService siteStatusService;

	@Autowired
	private DataSourceService dataSourceService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	private UtilityCommon utilityCommon = new UtilityCommon();

	/********************************************************************************************
	 * 
	 * Function Name : listCustomerLists
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads site list page.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/sitelist", method = RequestMethod.GET)
	public String listCustomerLists(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String timezonevalue = "0";
		if (!timezonevalue.equals("") && timezonevalue != null) {
			timezonevalue = session.getAttribute("timezonevalue").toString();
		}
		loginUserDetails.setTimezoneoffsetmin(timezonevalue);

		List<SiteViewBean> lstSiteViewBean = new ArrayList<SiteViewBean>();

		List<DashboardSiteListBean> lstDashboardSiteListDetails = siteService.getDashboardSiteList(String.valueOf(id),
				timezonevalue);

		DashboardSiteBean objDashboardSiteBean = null;

		String MapJsonData = "";
		if (lstDashboardSiteListDetails.size() > 0) {
			Integer a = 0;

			String SiteID = "";
			String SiteTypeID = "";
			String SiteCode = "";
			String SiteRef = "";
			String SiteName = "";
			String Lattitude = "";
			String Longtitude = "";
			String Address = "";
			String City = "";
			String State = "";
			String Country = "";
			String PostalCode = "";
			String ActiveInverterCount = "";
			String InverterCount = "";
			String LastChecked = "";
			Double TotalEnergy = 0.0;
			Double TodayEnergy = 0.0;
			String LastDownTime = "";
			String LastDataReceived = "";
			Double TodayHoursOn = 0.0;
			String Status = "";
			String Inverters = "";
			Double InstallationCapacity = 0.0;
			String SpecificYield = "-";
			String TotalEnergyUOM = " kWh";
			String TodayEnergyUOM = " kWh";
			String Mobile = "";
			String Telephone = "";
			String LocationUrl = "";
			String SitePoDate = "";
			String ProFlag = "";

			for (Object object : lstDashboardSiteListDetails) {
				Object[] obj = (Object[]) object;

				SiteID = utilityCommon.StringFromBigDecimalOrIntegerObject(obj[0]);
				SiteCode = utilityCommon.StringFromStringObject(obj[1]);
				SiteTypeID = utilityCommon.StringFromStringObject(obj[2]);
				SiteRef = utilityCommon.StringFromStringObject(obj[3]);
				SiteName = utilityCommon.StringFromStringObject(obj[4]);
				Lattitude = utilityCommon.StringFromStringObject(obj[5]);
				Longtitude = utilityCommon.StringFromStringObject(obj[6]);
				Address = utilityCommon.StringFromStringObject(obj[7]);
				City = utilityCommon.StringFromStringObject(obj[8]);
				State = utilityCommon.StringFromStringObject(obj[9]);
				Country = utilityCommon.StringFromStringObject(obj[10]);
				PostalCode = utilityCommon.StringFromStringObject(obj[11]);
				ActiveInverterCount = obj[12].toString();
				InverterCount = obj[13].toString();
				LastChecked = utilityCommon.StringFromStringObject(obj[14]);
				TotalEnergy = utilityCommon.DoubleFromBigDecimalObject(obj[15]);
				TodayEnergy = utilityCommon.DoubleFromBigDecimalObject(obj[16]);
				LastDownTime = utilityCommon.StringFromStringObject(obj[17]);
				LastDataReceived = utilityCommon.StringFromStringObject(obj[18]);
				TodayHoursOn = utilityCommon.DoubleFromBigDecimalObject(obj[19]);
				Status = utilityCommon.StringFromStringObject(obj[20]);
				InstallationCapacity = utilityCommon.DoubleFromBigDecimalObject(obj[21]);
				Mobile = utilityCommon.StringFromStringObject(obj[22]);
				Telephone = utilityCommon.StringFromStringObject(obj[23]);
				LocationUrl = utilityCommon.StringFromStringObject(obj[24]);
				SitePoDate = utilityCommon.StringFromStringObject(obj[25]);
				ProFlag = obj[29].toString();

				TotalEnergyUOM = " kWh";
				TodayEnergyUOM = " kWh";
				SiteViewBean objSiteViewBean = new SiteViewBean();

				objSiteViewBean.setSiteID(SiteID);
				objSiteViewBean.setSiteRef(SiteRef);
				objSiteViewBean.setSiteCode(SiteCode);
				objSiteViewBean.setSiteName(SiteName);
				objSiteViewBean.setSiteAddress(Address);
				objSiteViewBean.setSiteMobileNo(Mobile);
				objSiteViewBean.setSiteTelephoneNo(Telephone);
				objSiteViewBean.setSiteLocation(LocationUrl);
				objSiteViewBean.setDataSearch(SiteCode.toLowerCase() + " " + SiteName.toLowerCase());
				objSiteViewBean.setTotalEquipment(InverterCount);
				objSiteViewBean.setInstallationCapacity(InstallationCapacity);
				objSiteViewBean.setCommisiongDate(SitePoDate);

				if (TotalEnergy >= 1000000) {
					TotalEnergy = TotalEnergy / 1000000;
					TotalEnergyUOM = " GWh";

				} else if (TotalEnergy >= 1000) {
					TotalEnergy = TotalEnergy / 1000;
					TotalEnergyUOM = " MWh";

				} else if (TotalEnergy < 1) {
					TotalEnergy = TotalEnergy * 1000;
					TotalEnergyUOM = " Wh";

				}

				if (TodayEnergy >= 1000000) {
					TodayEnergy = TodayEnergy / 1000000;
					TodayEnergyUOM = " GWh";

				} else if (TodayEnergy >= 1000) {
					TodayEnergy = TodayEnergy / 1000;
					TodayEnergyUOM = " MWh";

				} else if (TodayEnergy < 1) {
					TodayEnergy = TodayEnergy * 1000;
					TodayEnergyUOM = " Wh";

				}

				if (ProFlag.equals("1") == true) {
					objSiteViewBean.setTodayEnergy(String.format("%.2f", TodayEnergy) + TodayEnergyUOM);
					objSiteViewBean.setTotalEnergy(String.format("%.2f", TotalEnergy) + TotalEnergyUOM);
					objSiteViewBean.setEnergyLastUpdate(LastDataReceived);

				} else {

					objSiteViewBean.setTodayEnergy("Not Connected");
					objSiteViewBean.setTotalEnergy("Not Connected");
				}

				lstSiteViewBean.add(objSiteViewBean);

				a = a + 1;

			}
		}

		/*
		 * if(lstSite.size()==1) { return "redirect:/siteview" +
		 * lstSite.get(0).getSiteId() + "&ref=" + id; }
		 * 
		 */

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
				for (int i = 0; i < lstSiteViewBean.size(); i++) {
					if (lstSiteViewBean.get(i).getSiteName().equals("Repal Renewables, Komatikuntla")) {
						lstSiteViewBean.get(i).setSiteName("Demo Site");
					}
				}
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());

		model.addAttribute("siteList", getDropdownList("Site", id));
		model.addAttribute("userList", getDropdownList("User", id));
		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("sitelist", lstSiteViewBean);

		return "sitelist";
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String, String> 
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "User") {
			List<User> ddlList = userService.listFieldUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		}

		return ddlMap;
	}

}
