
/******************************************************
 * 
 *    	Filename	: CountryRegionController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Controller is for country region functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.model.CountryRegion;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.CountryRegionService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;


@Controller
public class CountryRegionController {
	
	 @Autowired
	private CountryRegionService countryregionService;
	 

	 @Autowired
	    private UserService userService;

	 @Autowired
	    private UserRoleService userRoleService;
	
	 @Autowired
	 private RoleActivityService roleActivityService;
	 

		/********************************************************************************************
		 * 
		 * Function Name : getStatusDropdownList
		 * 
		 * Author        : Sarath Babu E
		 * 
		 * Time Stamp    : 29-Apr-19 09:47 AM 
		 * 
		 * Description   : This function returns value for dropdown list.
		 * 
		 * Return Value  : Map<String,String>
		 * 
		 * 
		 **********************************************************************************************/
		public Map<String,String> getStatusDropdownList() 
		{
			Map<String,String> ddlMap = new LinkedHashMap<String,String>();
			ddlMap.put("1", "Active");
			ddlMap.put("0", "In-active");
			return ddlMap;
		}
	 
		/********************************************************************************************
		 * 
		 * Function Name : listCountryRegions
		 * 
		 * Author        : Sarath Babu E
		 * 
		 * Time Stamp    : 29-Apr-19 09:47 AM 
		 * 
		 * Description   : This function loads country region page.
		 * 
		 * Input Params  : model,session
		 * 
		 * Return Value  : String
		 * 
		 * Exceptions    : Exception
		 * 
		 * 
		 **********************************************************************************************/
	    @RequestMapping(value = "/countryregions", method = RequestMethod.GET)
	    public String listCountryRegions(Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	    	 
	    	 AccessListBean objAccessListBean = new AccessListBean();
	    	   
	    	    try
	    	    {
	    	    User objUser =  userService.getUserById(id);
	    		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
	    	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
	    	 	
	    	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
	    	   
	    	    objAccessListBean.setUserID(id);
	    	    objAccessListBean.setUserName(objUser.getUserName());
	    	    
	    	    
	    	    
	    	    if(objUser.getRoleID()!=4)
	    		{
	    			objAccessListBean.setCustomerListView("visible");
	    		}
	    	    
	    	    
	    	    for(int l=0;l<lstRoleActivity.size();l++)
	    	    {
	    	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setDashboard("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setOverview("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setSystemMonitoring("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setVisualization("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setAnalytics("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setPortfolioManagement("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setTicketing("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setForcasting("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setConfiguration("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==10 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setAnalysis("visible");
	    	    	}
	    	    }
	    	    
	    	    }
	    	    catch(Exception e)
	    	    {
	    	    	System.out.println(e.getMessage());
	    	    }
	    	    
	    	     model.addAttribute("access",objAccessListBean);
	    	    
	
	    	 
	        model.addAttribute("countryregion", new CountryRegion());
	        model.addAttribute("countryregionList", countryregionService.listCountryRegions());
	        model.addAttribute("activeStatusList", getStatusDropdownList());
	    	
	    	
	        
	        return "countryregion";
	    }
	 
	    
		/********************************************************************************************
		 * 
		 * Function Name : addcountryregion
		 * 
		 * Author        : Sarath Babu E
		 * 
		 * Time Stamp    : 29-Apr-19 09:47 AM 
		 * 
		 * Description   : This function adds or updates a country region.
		 * 
		 * Input Params  : countryregion,session
		 * 
		 * Return Value  : String
		 * 
		 * 
		 **********************************************************************************************/
	    // Same method For both Add and Update CountryRegion
	   // @RequestMapping(value = "/countryregion/add", method = RequestMethod.POST)
	    @RequestMapping(value = "/addnewcountryregion", method = RequestMethod.POST)
	    public String addcountryregion(@ModelAttribute("countryregion") CountryRegion countryregion,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 

	    	Date date = new Date();
	        if (countryregion.getCountryRegionId()==null || countryregion.getCountryRegionId() == 0) {
	            // new countryregion, add it
	        	countryregion.setCreationDate(date);
	            countryregionService.addCountryRegion(countryregion);
	        } else {
	            // existing countryregion, call update
	        	countryregion.setLastUpdatedDate(date);
	            countryregionService.updateCountryRegion(countryregion);
	        }
	 
	        
	        
	        
	        return "redirect:/countryregions";
	 
	    }
	 
	    
		/********************************************************************************************
		 * 
		 * Function Name : removecountryregion
		 * 
		 * Author        : Sarath Babu E
		 * 
		 * Time Stamp    : 29-Apr-19 09:47 AM 
		 * 
		 * Description   : This function removes a country region.
		 * 
		 * Input Params  : id,session
		 * 
		 * Return Value  : String
		 * 
		 * 
		 **********************************************************************************************/
	    //@RequestMapping("/countryregion/remove/{id}")
	    @RequestMapping("/removecountryregion{id}")
	    public String removecountryregion(@PathVariable("id") int id,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	 
	        countryregionService.removeCountryRegion(id);
	        return "redirect:/countryregions";
	    }
	 
		/********************************************************************************************
		 * 
		 * Function Name : editcountryregion
		 * 
		 * Author        : Sarath Babu E
		 * 
		 * Time Stamp    : 29-Apr-19 09:47 AM 
		 * 
		 * Description   : This function updates country region by id.
		 * 
		 * Input Params  : id,model,session
		 * 
		 * Return Value  : String
		 * 
		 * Exceptions    : Exception
		 * 
		 * 
		 **********************************************************************************************/
	    //@RequestMapping("/countryregion/edit/{id}")
	    @RequestMapping("/editcountryregion{id}")
	    public String editcountryregion(@PathVariable("id") int id, Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 int refid = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	    	 AccessListBean objAccessListBean = new AccessListBean();
	    	   
	    	    try
	    	    {
	    	    User objUser =  userService.getUserById(refid);
	    		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
	    	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
	    	 	
	    	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
	    	   
	    	    objAccessListBean.setUserID(refid);
	    	    objAccessListBean.setUserName(objUser.getUserName());
	    	    
	    	    
	    	    
	    	    if(objUser.getRoleID()!=4)
	    		{
	    			objAccessListBean.setCustomerListView("visible");
	    		}
	    	    
	    	    
	    	    for(int l=0;l<lstRoleActivity.size();l++)
	    	    {
	    	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setDashboard("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setOverview("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setSystemMonitoring("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setVisualization("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setAnalytics("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setPortfolioManagement("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setTicketing("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setForcasting("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setConfiguration("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==10 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setAnalysis("visible");
	    	    	}
	    	    }
	    	    
	    	    }
	    	    catch(Exception e)
	    	    {
	    	    	System.out.println(e.getMessage());
	    	    }
	    	    
	    	     model.addAttribute("access",objAccessListBean);
	    	 
	        model.addAttribute("countryregion", countryregionService.getCountryRegionById(id));
	        model.addAttribute("countryregionList", countryregionService.listCountryRegions());
	        model.addAttribute("activeStatusList", getStatusDropdownList());
	    	
	    	
	    	
	        return "countryregion";
	    }
}
