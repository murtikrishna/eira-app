
/******************************************************
 * 
 *    	Filename	: ParameterStandardController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Standard Parameter functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.CustomerType;
import com.mestech.eampm.model.EquipmentCategory;
import com.mestech.eampm.model.EquipmentType;
import com.mestech.eampm.model.ErrorCode;
import com.mestech.eampm.model.ParameterStandard;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.State;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.CustomerTypeService;
import com.mestech.eampm.service.ParameterStandardsService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.StateService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.UtilityCommon;
import com.mestech.eampm.utility.UtilityCommonDAO;

@Controller
public class ParameterStandardController {

	@Autowired
	private CustomerService customerService;
	@Autowired
	private CustomerTypeService customertypeService;
	@Autowired
	private CountryService countryService;
	@Autowired
	private StateService stateService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private RoleActivityService roleActivityService;

	@Autowired
	private ParameterStandardsService parameterstandardsService;

	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();
	private UtilityCommon utilityCommon = new UtilityCommon();

	/********************************************************************************************
	 * 
	 * Function Name : ParameterstandardsPageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads the parameter standard page.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/parameterstandards", method = RequestMethod.GET)
	public String ParameterstandardsPageLoad(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("parameterstandard", new ParameterStandard());

		model.addAttribute("siteList", getDropdownList("Site"));

		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("activeStatusList", getStatusDropdownList());

		return "parameterstandard";
	}

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns values for dropdown.
	 * 
	 * Return Value  : Map<String,String> 
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function return value for dropdown.
	 * 
	 * Input Params  : DropdownName
	 * 
	 * Return Value  : Map<String,String> 
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.listSites();

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getParameterStandardsList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns all standard parameters.
	 * 
	 * Input Params  : timezonevalue
	 * 
	 * Return Value  : List<ParameterStandard>
	 * 
	 * 
	 **********************************************************************************************/
	public List<ParameterStandard> getParameterStandardsList(String timezonevalue) {
		List<ParameterStandard> ddlParameteStandardsList = parameterstandardsService.listParameterStandards();

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Calendar cal = Calendar.getInstance();

		for (int i = 0; i < ddlParameteStandardsList.size(); i++) {

			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {

					if (ddlParameteStandardsList.get(i).getCreationDate() != null) {

						cal.setTime(ddlParameteStandardsList.get(i).getCreationDate());
						cal.add(Calendar.MINUTE, Integer.valueOf(timezonevalue));

						ddlParameteStandardsList.get(i).setCreationDateText(sdf.format(cal.getTime()));

					}
					if (ddlParameteStandardsList.get(i).getLastUpdatedDate() != null) {

						cal.setTime(ddlParameteStandardsList.get(i).getLastUpdatedDate());
						cal.add(Calendar.MINUTE, Integer.valueOf(timezonevalue));
						ddlParameteStandardsList.get(i).setLastUpdatedDateText(sdf.format(cal.getTime()));

					}
				} else {
					if (ddlParameteStandardsList.get(i).getCreationDate() != null) {
						ddlParameteStandardsList.get(i)
								.setCreationDateText(sdf.format(ddlParameteStandardsList.get(i).getCreationDate()));

					}
					if (ddlParameteStandardsList.get(i).getLastUpdatedDate() != null) {
						ddlParameteStandardsList.get(i).setLastUpdatedDateText(
								sdf.format(ddlParameteStandardsList.get(i).getLastUpdatedDate()));

					}
				}

			}

		}

		return ddlParameteStandardsList;
	}

	/********************************************************************************************
	 * 
	 * Function Name : ParameterStandardsGridLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This method loads parameter standard grid.
	 * 
	 * Input Params  : session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/parameterstandardlist", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> ParameterStandardsGridLoad(HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}

				objResponseEntity.put("siteList", getDropdownList("Site"));

				objResponseEntity.put("ticketcreation", new TicketDetail());
				objResponseEntity.put("activeStatusList", getStatusDropdownList());
				objResponseEntity.put("parametestandardsList", getParameterStandardsList(timezonevalue));

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : Parameterstandards
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function creates or updates Standard parameters.
	 * 
	 * Input Params  : lstparameterstandards,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>> 
	 * 
	 * Exceptions    : ConstraintViolationException,Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/newparameterstandards", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> Parameterstandards(
			@RequestBody List<ParameterStandard> lstparameterstandards, HttpSession session,
			Authentication authentication) throws ServletException {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String ResponseMessage = "";
		String Dataexists1 = "";
		String Dataexists2 = "";

		try {

			if (lstparameterstandards != null) {
				ParameterStandard parameterstandards = lstparameterstandards.get(0);

				if (parameterstandards != null) {

					Date date = new Date();
					if (parameterstandards.getStandardId() == null || parameterstandards.getStandardId() == 0) {

						List<ParameterStandard> lstParameterStandards = parameterstandardsService
								.listParameterStandards(0);
						for (int i = 0; i < lstParameterStandards.size(); i++) {
							String ParameterName = lstParameterStandards.get(i).getStandardParameterName();

							if (parameterstandards.getStandardParameterName().equals(ParameterName)) {

								Dataexists1 = "Standard parameter name Already Exists";

								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						parameterstandards.setParameterCode(getParameterCode());
						parameterstandards.setCreationDate(date);
						parameterstandards.setCreatedBy(id);

						parameterstandardsService.addParameterStandards(parameterstandards);
					} else {

						ParameterStandard objParameterStandards = parameterstandardsService
								.getParameterStandardsById(parameterstandards.getStandardId());

						List<ParameterStandard> lstParameterStandards = parameterstandardsService
								.listParameterStandards(parameterstandards.getStandardId());
						for (int i = 0; i < lstParameterStandards.size(); i++) {
							String ParameterName = lstParameterStandards.get(i).getStandardParameterName();

							if (parameterstandards.getStandardParameterName().equals(ParameterName)) {

								Dataexists1 = "Standard parameter name Already Exists";

								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}
						objParameterStandards.setStandardParameterName(parameterstandards.getStandardParameterName());
						objParameterStandards.setStandardParameterUom(parameterstandards.getStandardParameterUom());
						objParameterStandards.setParameterDescription(parameterstandards.getParameterDescription());
						objParameterStandards.setRemarks(parameterstandards.getRemarks());
						objParameterStandards.setApprovedBy(parameterstandards.getApprovedBy());

						objParameterStandards.setActiveFlag(parameterstandards.getActiveFlag());

						objParameterStandards.setLastUpdatedDate(date);
						objParameterStandards.setLastUpdatedBy(id);

						parameterstandardsService.updateParameterStandards(objParameterStandards);

						objResponseEntity.put("status", true);
						objResponseEntity.put("parameterstandards", objParameterStandards);
						objResponseEntity.put("data", "Success");
						return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

					}
					objResponseEntity.put("status", true);
					objResponseEntity.put("parameterstandards", parameterstandards);
					ResponseMessage = "Success";
				} else {
					objResponseEntity.put("status", false);
					ResponseMessage = "Invalid Request";
					objResponseEntity.put("parameterstandards", null);
				}

			} else {
				objResponseEntity.put("status", false);
				ResponseMessage = "Invalid Request";
				objResponseEntity.put("parameterstandards", null);
			}

			objResponseEntity.put("data", ResponseMessage);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (ConstraintViolationException cve) {
			objResponseEntity.put("parameterstandards", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", cve.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			objResponseEntity.put("parameterstandards", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getParameterCode
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns parameter code.
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	public String getParameterCode() {

		ParameterStandard objCust = parameterstandardsService.getParameterStandardByMax("ParameterCode");

		String CR = "";

		if (objCust != null) {
			CR = objCust.getParameterCode();
		}

		if (CR == null || CR == "") {
			CR = "PI0001";
		} else {
			try {
				int NextIndex = Integer.parseInt(CR.replaceAll("PI", "")) + 1;

				if (NextIndex >= 1000) {
					CR = String.valueOf(NextIndex);
				} else if (NextIndex >= 100) {
					CR = "0" + String.valueOf(NextIndex);
				} else if (NextIndex >= 10) {
					CR = "00" + String.valueOf(NextIndex);
				} else {
					CR = "000" + String.valueOf(NextIndex);
				}

				CR = "PI" + CR;
			} catch (Exception e) {
				CR = "PI0001";
			}
		}

		return CR;
	}

	/********************************************************************************************
	 * 
	 * Function Name : ParameterStandardsUpdation
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns parameter standards by id.
	 * 
	 * Input Params  : StandardId,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/editparameterstandards", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> ParameterStandardsUpdation(Integer StandardId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			objResponseEntity.put("parameterstandards",
					parameterstandardsService.getParameterStandardsById(StandardId));
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

}
