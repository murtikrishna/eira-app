
/******************************************************
 * 
 *    	Filename	: CustomerListController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Controller deals with customer list related activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.CustomerListBean;
import com.mestech.eampm.bean.CustomerViewBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.bean.SiteListBean;
import com.mestech.eampm.bean.SiteMapListBean;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.DataSource;
import com.mestech.eampm.model.DataTransaction;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.DataSourceService;
import com.mestech.eampm.service.DataTransactionService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteStatusService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.UtilityCommon;

@Controller
public class CustomerListController {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private RoleActivityService roleActivityService;

	private UtilityCommon utilityCommon = new UtilityCommon();

	/********************************************************************************************
	 * 
	 * Function Name : listCustomerLists
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads the customer list page.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/customerlist", method = RequestMethod.GET)
	public String listCustomerLists(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		List<CustomerViewBean> lstCustomerViewBean = new ArrayList<CustomerViewBean>();
		AccessListBean objAccessListBean = new AccessListBean();

		List<CustomerListBean> lstCustomer = customerService.getAjaxCustomerListByUser(String.valueOf(id));

		CustomerListBean objCustomerListBean = null;
		CustomerViewBean objCustomerViewBean = null;

		int i = 1;
		if (lstCustomer.size() > 0) {
			for (Object object : lstCustomer) {
				Object[] obj = (Object[]) object;

				// objCustomerListBean = new CustomerListBean(customerId, customerCode,
				// customerReference, customerName, address, contactPerson, mobile, totalSites)

				objCustomerViewBean = new CustomerViewBean();

				objCustomerViewBean.setDivID(i);

				if (utilityCommon.StringFromStringObject(obj[9]).equals("Secondary")) {
					List<Customer> customer = customerService
							.getPrimaryCustomerById(utilityCommon.StringFromBigDecimalOrIntegerObject(obj[10]));
					objCustomerViewBean.setCustomerCode(
							customer.get(0).getCustomerCode() + " - " + utilityCommon.StringFromStringObject(obj[11]));
					objCustomerViewBean.setCustomerID(utilityCommon.StringFromBigDecimalOrIntegerObject(obj[0]));
					objCustomerViewBean.setCustomerreference(utilityCommon.StringFromStringObject(obj[2]));
					objCustomerViewBean.setCustomerName(utilityCommon.StringFromStringObject(obj[3]));
					objCustomerViewBean.setCustomerAddress(utilityCommon.StringFromStringObject(obj[4]));
					objCustomerViewBean.setContactPerson(utilityCommon.StringFromStringObject(obj[5]));

					objCustomerViewBean.setCustomerMobileNo(utilityCommon.StringFromStringObject(obj[6]));
					objCustomerViewBean.setCustomerTelephoneNo(utilityCommon.StringFromStringObject(obj[6]));
					objCustomerViewBean.setTotalSiteCount(utilityCommon.StringFromBigDecimalOrIntegerObject(obj[7]));

					objCustomerViewBean.setTotalCapacity(
							String.format("%.2f", utilityCommon.DoubleFromBigDecimalObject(obj[8]) / 1000));
					objCustomerViewBean.setOpenTicketCount("-");
					objCustomerViewBean.setTotalEventCount("-");
				}

				else {
					objCustomerViewBean.setCustomerID(utilityCommon.StringFromBigDecimalOrIntegerObject(obj[0]));
					objCustomerViewBean.setCustomerCode(utilityCommon.StringFromStringObject(obj[1]));
					objCustomerViewBean.setCustomerreference(utilityCommon.StringFromStringObject(obj[2]));
					objCustomerViewBean.setCustomerName(utilityCommon.StringFromStringObject(obj[3]));
					objCustomerViewBean.setCustomerAddress(utilityCommon.StringFromStringObject(obj[4]));
					objCustomerViewBean.setContactPerson(utilityCommon.StringFromStringObject(obj[5]));

					objCustomerViewBean.setCustomerMobileNo(utilityCommon.StringFromStringObject(obj[6]));
					objCustomerViewBean.setCustomerTelephoneNo(utilityCommon.StringFromStringObject(obj[6]));
					objCustomerViewBean.setTotalSiteCount(utilityCommon.StringFromBigDecimalOrIntegerObject(obj[7]));

					objCustomerViewBean.setTotalCapacity(
							String.format("%.2f", utilityCommon.DoubleFromBigDecimalObject(obj[8]) / 1000));
					objCustomerViewBean.setOpenTicketCount("-");
					objCustomerViewBean.setTotalEventCount("-");
				}

				lstCustomerViewBean.add(objCustomerViewBean);

				i = i + 1;
			}
		}

		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("siteList", getNewTicketSiteList("Site", id));

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());

		model.addAttribute("customerlist", lstCustomerViewBean);

		model.addAttribute("ticketcreation", new TicketDetail());
		return "customerlist";
	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "User") {
			List<User> ddlList = userService.listFieldUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		}

		return ddlMap;
	}

}
