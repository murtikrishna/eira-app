
/******************************************************
 * 
 *    	Filename	: SiteController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Site related functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.Currency;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;
import com.mestech.eampm.model.SiteType;
import com.mestech.eampm.model.State;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.CurrencyService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteStatusService;
import com.mestech.eampm.service.SiteTypeService;
import com.mestech.eampm.service.StateService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class SiteController {

	@Autowired
	private SiteService siteService;

	@Autowired
	private CustomerService customerService;
	@Autowired
	private SiteTypeService sitetypeService;

	@Autowired
	private ActivityService activityService;
	@Autowired
	private CountryService countryService;
	@Autowired
	private StateService stateService;
	@Autowired
	private CurrencyService currencyService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private SiteStatusService siteStatusService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : SiteGridLoad
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns data for site data load.
	 * 
	 * Input Params : session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/site", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> SiteGridLoad(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();
				objResponseEntity.put("sitetypeList", getDropdownList("SiteType"));
				objResponseEntity.put("siteList", getDropdownList("Site"));

				objResponseEntity.put("customerList", getDropdownList("ConfigCustomer"));

				objResponseEntity.put("countryList", getDropdownList("Country"));
				objResponseEntity.put("stateList", getDropdownList("State"));
				objResponseEntity.put("operationmodeList", getDropdownList("OperationMode"));
				objResponseEntity.put("subfolderList", getDropdownList("SubFolder"));
				objResponseEntity.put("activeStatusList", getStatusDropdownList());
				objResponseEntity.put("siteList", getUpdatedSiteList());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : SiteList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns site data list.
	 * 
	 * Input Params : session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/sitedetailslist", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> SiteList(HttpSession session, Authentication authentication) {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();

		try {

			if (authentication == null) {
				objResponseEntity.put("siteList", getUpdatedSiteList());
				// objResponseEntity.put("status", true);
				// objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);
			} else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				objResponseEntity.put("siteList", getUpdatedSiteList());
				// objResponseEntity.put("status", true);
				// objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			// objResponseEntity.put("status", false);
			// objResponseEntity.put("data", e.getMessage());
			objResponseEntity.put("siteList", null);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : NewSiteSaveorupdate
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function add a new site.
	 * 
	 * Input Params : lstsite,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : ConstraintViolationException,Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/newsite", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> NewSiteSaveorupdate(@RequestBody List<Site> lstsite, HttpSession session,
			Authentication authentication) throws ServletException {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String ResponseMessage = "";
		String Dataexists1 = "";
		String Dataexists2 = "";

		try {

			if (lstsite != null) {
				Site site = lstsite.get(0);

				if (site != null) {
					System.out.println("Site Name :: " + site.getSiteName());
					Date podate = null;
					Date coddate = null;
					Date workdate = null;
					if (site.getSitePoDateText() != null && !site.getSitePoDateText().equals("")) {
						podate = new SimpleDateFormat("dd/MM/yyyy").parse(site.getSitePoDateText());

					}
					if (site.getCommercementofworkText() != null && !site.getCommercementofworkText().equals("")) {
						workdate = new SimpleDateFormat("dd/MM/yyyy").parse(site.getCommercementofworkText());

					}
					if (site.getCodText() != null && !site.getCodText().equals("")) {
						coddate = new SimpleDateFormat("dd/MM/yyyy").parse(site.getCodText());

					}

					Date date = new Date();
					if (site.getSiteId() == null || site.getSiteId() == 0) {

						List<Site> lstsites = siteService.listallsites(0);

						for (int i = 0; i < lstsites.size(); i++) {
							String Sitename = lstsites.get(i).getSiteName();

							if (site.getSiteName().equals(Sitename)) {

								Dataexists1 = "SiteName Already Exist";

								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						// Site ID Auto Generation
						Country country = countryService.getCountryById(site.getCountryID());
						State state = stateService.getStateById(site.getStateID());
						SiteType sitetype = sitetypeService.getSiteTypeById(site.getSiteTypeID());

						// String FinalSiteName = country.getCountryCode() + '-'+ state.getStateCode() +
						// '-' + sitetype.getShortName()+'-'+site.getCustomerReference();

						List<Site> lstcomapresite = siteService.listConfigSites();
						Map<Integer, List<Site>> map = new HashMap<>();
						if (lstcomapresite != null && lstcomapresite.size() > 0) {
							for (Site site2 : lstcomapresite) {
								if (map.get(site2.getStateID()) == null) {
									List<Site> siteCodes = new ArrayList<>();
									siteCodes.add(site2);
									map.put(site2.getStateID(), siteCodes);
								} else {
									List<Site> siteCodes = map.get(site2.getStateID());
									siteCodes.add(site2);
									map.put(site2.getStateID(), siteCodes);
								}
							}
						}
						String sitename = "";
						try {
							if (map != null && map.size() > 0 && map.get(site.getStateID()) != null) {
								lstcomapresite = map.get(site.getStateID());
								Map<Integer, List<Site>> map2 = new HashMap<>();
								for (Site site2 : lstcomapresite) {
									if (map2.get(site2.getSiteTypeID()) == null
											|| map2.get(site.getSiteTypeID()).isEmpty()) {
										List<Site> sites = new ArrayList<>();
										sites.add(site2);
										map2.put(site2.getSiteTypeID(), sites);
									} else {
										List<Site> sites = map2.get(site2.getSiteTypeID());
										sites.add(site2);
									}
								}
								if (map2.get(site.getSiteTypeID()) != null
										&& map2.get(site.getSiteTypeID()).size() > 0) {
									lstcomapresite = map2.get(site.getSiteTypeID());
									List<String> siteCodes = new ArrayList<>();
									for (Site siteMap : lstcomapresite) {
										String siteCode = siteMap.getSiteCode();
										if (siteCode == null || siteCode.startsWith("SE")) {
											continue;
										} else {
											siteCodes.add(siteCode);
										}
									}
									Collections.sort(siteCodes, new Comparator<String>() {
										@Override
										public int compare(String o1, String o2) {
											// TODO Auto-generated method stub
											return o2.compareTo(o1);
										}
									});
									int NextIndex = Integer.parseInt(siteCodes.get(0)
											.replaceAll(state.getStateCode() + '-' + sitetype.getShortName() + '-', "")
											.trim()) + 1;
									if (NextIndex >= 1000) {
										sitename = String.valueOf(NextIndex);

									} else if (NextIndex >= 100) {
										sitename = "0" + String.valueOf(NextIndex);

									} else if (NextIndex >= 10) {
										sitename = "00" + String.valueOf(NextIndex);

									} else {
										sitename = "000" + String.valueOf(NextIndex);

									}
								}
							} else {
								int NextIndex = 1;
								sitename = "000" + String.valueOf(NextIndex);
							}
							sitename = state.getStateCode() + '-' + sitetype.getShortName() + '-' + sitename;
						} catch (Exception e) {
							sitename = state.getStateCode() + '-' + sitetype.getShortName() + '-' + "0001";

						}

						// new site, add it
						site.setSiteCode(sitename);
						site.setCreationDate(date);
						site.setCreatedBy(id);
						site.setEquipmentFlag(0);
						if (podate != null) {
							site.setSitePODate(podate);
						}
						if (coddate != null) {
							site.setCod(coddate);
						}
						if (workdate != null) {
							site.setCommencementofwork(workdate);
						}

						site = siteService.addSite(site);

						siteStatusService.save(new SiteStatus(site.getSiteId(), site.getCustomerID(),
								new Integer(0).toString(), new Integer(1), new Date()));

						objResponseEntity.put("status", true);

						objResponseEntity.put("site", site);
						ResponseMessage = "Success";
					} else {
						// existing site, call update
						Site existingSite = siteService.getSiteById(site.getSiteId());

						List<Site> lstsites = siteService.listallsites(site.getSiteId());

						for (int i = 0; i < lstsites.size(); i++) {
							String Sitename = lstsites.get(i).getSiteName();

							if (site.getSiteName().equals(Sitename)) {

								Dataexists1 = "SiteName Already Exist";

								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						existingSite.setLastUpdatedDate(date);

						existingSite.setCustomerID(site.getCustomerID());

						existingSite.setActiveFlag(site.getActiveFlag());
						existingSite.setAddress(site.getAddress());
						// existingSite.setAltitude(site.getAltitude());
						// existingSite.setApiKey(site.getApiKey());
						// existingSite.setApiUrl(site.getApiUrl());
						existingSite.setCity(site.getCity());
						// existingSite.setCollectionType(site.getCollectionType());
						// existingSite.setCommunicationType(site.getCommunicationType());
						existingSite.setContactPerson(site.getContactPerson());
						existingSite.setCountryID(site.getCountryID());
						// existingSite.setCurrencyID(site.getCurrencyID());
						// existingSite.setCustomerID(site.getCustomerID());
						existingSite.setCustomerNaming(site.getCustomerNaming());
						existingSite.setCustomerReference(site.getCustomerReference());
						existingSite.setModuleArea(site.getModuleArea());
						// existingSite.setDataLoggerID_Alarm(site.getDataLoggerID_Alarm());
						// existingSite.setDataLoggerID_Data(site.getDataLoggerID_Data());
						// existingSite.setDataLoggerID_Inverter(site.getDataLoggerID_Inverter());
						// existingSite.setDataLoggerID_Log(site.getDataLoggerID_Log());
						// existingSite.setDataLoggerID_Modbus(site.getDataLoggerID_Modbus());
						// existingSite.setDataLoggerID_Sensor(site.getDataLoggerID_Sensor());
						existingSite.setEmailID(site.getEmailID());
						// existingSite.setFax(site.getFax());
						// existingSite.setFileType(site.getFileType());

						existingSite.setInstallationCapacity(site.getInstallationCapacity());
						// existingSite.setLastUpdatedBy(site.getLastUpdatedBy());
						existingSite.setLatitude(site.getLatitude());
						/*
						 * existingSite.setLocalFtpDirectory_Alarm(site.getLocalFtpDirectory_Alarm());
						 * existingSite.setLocalFtpDirectory_Data(site.getLocalFtpDirectory_Data());
						 * existingSite.setLocalFtpDirectory_Inverter(site.getLocalFtpDirectory_Inverter
						 * ()); existingSite.setLocalFtpDirectory_Log(site.getLocalFtpDirectory_Log());
						 * existingSite.setLocalFtpDirectory_Modbus(site.getLocalFtpDirectory_Modbus());
						 * existingSite.setLocalFtpDirectory_Sensor(site.getLocalFtpDirectory_Sensor());
						 * existingSite.setLocalFtpDirectoryPath_Alarm(site.
						 * getLocalFtpDirectoryPath_Alarm());
						 * existingSite.setLocalFtpDirectoryPath_Data(site.getLocalFtpDirectoryPath_Data
						 * ()); existingSite.setLocalFtpDirectoryPath_Inverter(site.
						 * getLocalFtpDirectoryPath_Inverter());
						 * existingSite.setLocalFtpDirectoryPath_Log(site.getLocalFtpDirectoryPath_Log()
						 * ); existingSite.setLocalFtpDirectoryPath_Modbus(site.
						 * getLocalFtpDirectoryPath_Modbus());
						 * existingSite.setLocalFtpDirectoryPath_Sensor(site.
						 * getLocalFtpDirectoryPath_Sensor());
						 * existingSite.setLocalFtpHomeDirectory(site.getLocalFtpHomeDirectory());
						 * existingSite.setLocalFtpPassword(site.getLocalFtpPassword());
						 * existingSite.setLocalFtpUserName(site.getLocalFtpUserName());
						 */
						existingSite.setLongitude(site.getLongitude());
						existingSite.setMobile(site.getMobile());
						// existingSite.setModuleName(site.getModuleName());
						// existingSite.setOperationMode(site.getOperationMode());
						existingSite.setPostalCode(site.getPostalCode());
						/*
						 * existingSite.setRemoteFtpDirectoryPath(site.getRemoteFtpDirectoryPath());
						 * existingSite.setRemoteFtpPassword(site.getRemoteFtpPassword());
						 * existingSite.setRemoteFtpServer(site.getRemoteFtpServer());
						 * existingSite.setRemoteFtpServerPort(site.getRemoteFtpServerPort());
						 * existingSite.setRemoteFtpUserName(site.getRemoteFtpUserName());
						 * existingSite.setServiceCode_Alarm(site.getServiceCode_Alarm());
						 * existingSite.setServiceCode_Data(site.getServiceCode_Data());
						 * existingSite.setServiceCode_Inverter(site.getServiceCode_Inverter());
						 * existingSite.setServiceCode_Log(site.getServiceCode_Log());
						 * existingSite.setServiceCode_Modbus(site.getServiceCode_Modbus());
						 * existingSite.setServiceCode_Sensor(site.getServiceCode_Sensor());
						 */

						existingSite.setSiteDescription(site.getSiteDescription());
						existingSite.setSiteImage(site.getSiteImage());
						// existingSite.setSiteManafacturer(site.getSiteManafacturer());
						existingSite.setSiteName(site.getSiteName());
						// existingSite.setSiteOperator(site.getSiteOperator());
						// existingSite.setSitePONumber(site.getSitePONumber());
						existingSite.setSiteTypeID(site.getSiteTypeID());
						existingSite.setStateID(site.getStateID());
						if (site.getSitePoDateText() != null && !site.getSitePoDateText().equals("")) {
							existingSite.setSitePODate(podate);
						}
						if (site.getCodText() != null && !site.getCodText().equals("")) {
							existingSite.setCod(coddate);
						}
						if (site.getSitePONumber() != null) {
							existingSite.setSitePONumber(site.getSitePONumber());
						}

						if (site.getCommercementofworkText() != null && !site.getCommercementofworkText().equals("")) {
							existingSite.setCommencementofwork(workdate);
						}

						existingSite.setTelephone(site.getTelephone());
						// existingSite.setTimezone(site.getTelephone());

						existingSite.setContractId(site.getContractId());
						existingSite.setTypeOfContract(site.getTypeOfContract());
						existingSite.setServicePackage(site.getServicePackage());
						existingSite.setDaf_Service(site.getDaf_Service());
						existingSite.setDaf_Frequency(site.getDaf_Frequency());
						existingSite.setTbt_Service(site.getTbt_Service());
						existingSite.setTbt_Frequency(site.getTbt_Frequency());
						existingSite.setTat_Service(site.getTat_Service());
						existingSite.setTat_Frequency(site.getTat_Frequency());
						existingSite.setSym_Service(site.getSym_Service());
						existingSite.setSym_frequency(site.getSym_frequency());
						existingSite.setTls_Service(site.getTls_Service());
						existingSite.setTls_Frequency(site.getTls_Frequency());
						existingSite.setTlm_Frequency(site.getTlm_Frequency());
						existingSite.setTlm_Service(site.getTlm_Service());
						existingSite.setCom_Service(site.getCom_Service());
						existingSite.setCom_Frequency(site.getCom_Frequency());
						existingSite.setRt_Service(site.getRt_Service());
						existingSite.setRt_Frequency(site.getRt_Frequency());
						existingSite.setSecurity_Service(site.getSecurity_Service());
						existingSite.setSecurity_Frequency(site.getSecurity_Frequency());
						existingSite.setRowfc_Service(site.getRowfc_Service());
						existingSite.setRowfc_Frequency(site.getRowfc_Frequency());
						existingSite.setDti_Service(site.getDti_Service());
						existingSite.setDti_Frequency(site.getDti_Frequency());
						existingSite.setDi_Service(site.getDi_Service());
						existingSite.setDi_Frequency(site.getDi_Frequency());
						existingSite.setSact_Service(site.getSact_Service());
						existingSite.setSact_Frequency(site.getSact_Frequency());
						existingSite.setEltml_Service(site.getEltml_Service());
						existingSite.setEltml_Frequency(site.getEltml_Frequency());
						existingSite.setEltms_Service(site.getEltms_Service());
						existingSite.setEltms_Frequency(site.getEltms_Frequency());
						existingSite.setIr_Service(site.getIr_Service());
						existingSite.setIr_Frequency(site.getIr_Frequency());
						existingSite.setRevampdc_Service(site.getRevampdc_Service());
						existingSite.setRevampdc_Frequency(site.getRevampdc_Frequency());
						existingSite.setRevampaclt_Service(site.getRevampaclt_Service());
						existingSite.setRevampaclt_Service(site.getRevampaclt_Frequency());
						existingSite.setRevampacht_Service(site.getRevampacht_Service());
						existingSite.setRevampacht_Service(site.getRevampacht_Frequency());
						existingSite.setRevampacht_Service_Civil(site.getRevampacht_Service_Civil());
						existingSite.setRevampacht_Frequency_Civil(site.getRevampacht_Frequency_Civil());
						existingSite.setJmr_Service(site.getJmr_Service());
						existingSite.setJmr_Frequency(site.getJmr_Frequency());
						existingSite.setSldc_Documentation(site.getSldc_Documentation());
						existingSite.setSldc_Frequency(site.getSldc_Frequency());
						existingSite.setSpares_Management(site.getSpares_Management());
						existingSite.setSm_frequency(site.getSm_frequency());
						existingSite.setCleaningCycle(site.getCleaningCycle());
						existingSite.setCleaningCycle_Frequency(site.getCleaningCycle_Frequency());
						existingSite.setVEGETATION_MANGEMENT(site.getVEGETATION_MANGEMENT());
						existingSite.setVm_Frequency(site.getVm_Frequency());

						existingSite.setPlantdown_Issueidentified(site.getPlantdown_Issueidentified());
						existingSite.setPlantdown_Assigned(site.getPlantdown_Assigned());
						existingSite.setPlantdown_Resolved(site.getPlantdown_Resolved());

						existingSite.setPlantwarning_Issueidentified(site.getPlantwarning_Issueidentified());
						existingSite.setPlantwarning_Assigned(site.getPlantwarning_Assigned());
						existingSite.setPlantwarning_Resolved(site.getPlantwarning_Resolved());

						existingSite.setEqugenwarning_Issueidentified(site.getEqugenwarning_Issueidentified());
						existingSite.setEqugenwarning_Assigned(site.getEqugenwarning_Assigned());
						existingSite.setEqugenwarning_Resolved(site.getEqugenwarning_Resolved());

						existingSite.setEqugendown_Issueidentified(site.getEqugendown_Issueidentified());
						existingSite.setEqugendown_Assigned(site.getEqugendown_Assigned());
						existingSite.setEqugendown_Resolved(site.getEqugendown_Resolved());

						existingSite.setEqugenoffline_Assigned(site.getEqugenoffline_Assigned());
						existingSite.setEqugenoffline_Issueidentified(site.getEqugenoffline_Issueidentified());
						existingSite.setEqugenoffline_Resolved(site.getEqugenoffline_Resolved());

						existingSite.setEqunongenwarning_Issueidentified(site.getEqunongenwarning_Issueidentified());
						existingSite.setEqunongenwarning_Assigned(site.getEqunongenwarning_Assigned());
						existingSite.setEqunongenwarning_Resolved(site.getEqunongenwarning_Resolved());

						existingSite.setEqunongendown_Issueidentified(site.getEqunongendown_Issueidentified());
						existingSite.setEqunongendown_Assigned(site.getEqunongendown_Assigned());
						existingSite.setEqunongendown_Resolved(site.getEqunongendown_Resolved());

						existingSite.setEqunongenoffline_Assigned(site.getEqunongenoffline_Assigned());
						existingSite.setEqunongenoffline_Issueidentified(site.getEqunongenoffline_Issueidentified());
						existingSite.setEqunongenoffline_Resolved(site.getEqunongenoffline_Resolved());

						existingSite.setPerformanceguaranteedamages(site.getPerformanceguaranteedamages());

						existingSite.setDecisionMaker(site.getDecisionMaker());
						existingSite.setDm_Address(site.getDm_Address());
						existingSite.setDm_City(site.getDm_City());
						existingSite.setDm_State(site.getDm_State());
						existingSite.setDm_Country(site.getDm_Country());
						existingSite.setDm_Postalcode(site.getDm_Postalcode());
						existingSite.setDm_Emailid(site.getDm_Emailid());
						existingSite.setDm_Phoneno(site.getDm_Phoneno());

						existingSite.setChampion(site.getChampion());
						existingSite.setCh_Address(site.getCh_Address());
						existingSite.setCh_City(site.getCh_City());
						existingSite.setCh_State(site.getCh_State());
						existingSite.setCh_Country(site.getCh_Country());
						existingSite.setCh_Postalcode(site.getCh_Postalcode());
						existingSite.setCh_Emailid(site.getCh_Emailid());
						existingSite.setCh_Phoneno(site.getCh_Phoneno());

						existingSite.setRoadBlock(site.getRoadBlock());
						existingSite.setRd_Address(site.getRd_Address());
						existingSite.setRd_City(site.getRd_City());
						existingSite.setRd_State(site.getRd_State());
						existingSite.setRd_Country(site.getRd_Country());
						existingSite.setRd_Postalcode(site.getRd_Postalcode());
						existingSite.setRd_Emailid(site.getRd_Emailid());
						existingSite.setRd_Phoneno(site.getRd_Phoneno());

						existingSite.setKeyDeliverables(site.getKeyDeliverables());
						existingSite.setKd_Address(site.getKd_Address());
						existingSite.setKd_City(site.getKd_City());
						existingSite.setKd_State(site.getKd_State());
						existingSite.setKd_Country(site.getKd_Country());
						existingSite.setKd_Postalcode(site.getKd_Postalcode());
						existingSite.setKd_Emailid(site.getKd_Emailid());
						existingSite.setKd_Phoneno(site.getKd_Phoneno());

						existingSite.setLastUpdatedBy(id);

						siteService.updateSite(existingSite);
						objResponseEntity.put("status", true);
						objResponseEntity.put("site", existingSite);
						ResponseMessage = "Success";
					}

				} else {
					objResponseEntity.put("status", false);
					ResponseMessage = "Invalid Request";
					objResponseEntity.put("site", null);
				}

			} else {
				objResponseEntity.put("status", false);
				ResponseMessage = "Invalid Request";
				objResponseEntity.put("site", null);
			}

			objResponseEntity.put("data", ResponseMessage);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (

		ConstraintViolationException cve) {
			cve.printStackTrace();
			objResponseEntity.put("site", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", cve.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("site", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns data for dropdown.
	 * 
	 * Return Value : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns data for dropdown.
	 * 
	 * Input Params : DropdownName
	 * 
	 * Return Value : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Customer") {
			List<Customer> ddlList = customerService.listCustomers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getCustomerId().toString(), ddlList.get(i).getCustomerName());
			}

		} else if (DropdownName == "ConfigCustomer") {
			List<Customer> ddlList = customerService.listConfigCustomers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getCustomerId().toString(), ddlList.get(i).getCustomerName());
			}

		} else if (DropdownName == "SiteType") {
			List<SiteType> ddlList = sitetypeService.listSiteTypes();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteTypeId().toString(), ddlList.get(i).getSiteType());
			}

		}

		else if (DropdownName == "OperationMode") {

			ddlMap.put("2", "Api Mode");
			ddlMap.put("1", "Fetch Mode");
			ddlMap.put("0", "Receive Mode");
		} else if (DropdownName == "SubFolder") {
			ddlMap.put("0", "Don't allow subfolder files");
			ddlMap.put("1", "Allow subfolder files");
		} else if (DropdownName == "Country") {
			List<Country> ddlList = countryService.listCountries();
			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getCountryId().toString(), ddlList.get(i).getCountryName());
			}
		} else if (DropdownName == "State") {
			List<State> ddlList = stateService.listStates();
			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getStateId().toString(), ddlList.get(i).getStateName());
			}
		} else if (DropdownName == "Site") {
			List<Site> ddlList = siteService.listSites();

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				/*
				 * if(ddlList.get(i).getCustomerReference() != null) { SiteName = SiteName +
				 * " - " + ddlList.get(i).getCustomerReference(); }
				 */

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getUpdatedSiteList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns list of site data.
	 * 
	 * Return Value : List<Site>
	 * 
	 * 
	 **********************************************************************************************/
	public List<Site> getUpdatedSiteList() {
		// List<Site> ddlSitesList = siteService.listSites();

		List<Site> ddlSitesList = siteService.listConfigSites();

		List<Customer> ddlCustomerList = customerService.listCustomers();
		List<SiteType> ddlSiteTypeList = sitetypeService.listSiteTypes();
		List<Country> ddlCountryList = countryService.listCountries();
		List<State> ddlStateList = stateService.listStates();
		List<Currency> ddlCurrencyList = currencyService.listCurrencies();

		for (int i = 0; i < ddlSitesList.size(); i++) {
			List<Customer> lstCustomerList = new ArrayList<Customer>();
			List<SiteType> lstSiteTypeList = new ArrayList<SiteType>();
			List<Country> lstCountryList = new ArrayList<Country>();
			List<State> lstStateList = new ArrayList<State>();
			List<Currency> lstCurrencyList = new ArrayList<Currency>();

			int cid = ddlSitesList.get(i).getCustomerID();
			int stype = ddlSitesList.get(i).getSiteTypeID();
			int countryid = ddlSitesList.get(i).getCountryID();
			int stateid = ddlSitesList.get(i).getStateID();
			/* int currid = ddlSitesList.get(i).getCurrencyID(); */

			lstSiteTypeList = ddlSiteTypeList.stream().filter(p -> p.getSiteTypeId() == stype)
					.collect(Collectors.toList());
			lstCustomerList = ddlCustomerList.stream().filter(p -> p.getCustomerId() == cid)
					.collect(Collectors.toList());
			lstCountryList = ddlCountryList.stream().filter(p -> p.getCountryId() == countryid)
					.collect(Collectors.toList());
			lstStateList = ddlStateList.stream().filter(p -> p.getStateId() == stateid).collect(Collectors.toList());
			// lstCurrencyList = ddlCurrencyList.stream().filter(p -> p.getCurrencyId() ==
			// currid ).collect(Collectors.toList());

			if (lstSiteTypeList.size() > 0) {
				ddlSitesList.get(i).setSiteTypeName(lstSiteTypeList.get(0).getSiteType());
			}

			if (lstCustomerList.size() > 0) {
				ddlSitesList.get(i).setCustomerName(lstCustomerList.get(0).getCustomerName());
			}

			if (lstCountryList.size() > 0) {
				ddlSitesList.get(i).setCountryName(lstCountryList.get(0).getCountryName());
			}

			if (lstStateList.size() > 0) {
				ddlSitesList.get(i).setStateName(lstStateList.get(0).getStateName());
			}

			/*
			 * if(lstCurrencyList.size() >0) {
			 * ddlSitesList.get(i).setCurrencyName(lstCurrencyList.get(0).getCurrencyName())
			 * ; }
			 */

		}

		return ddlSitesList;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getSiteCode
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns site code.
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	public String getSiteCode() {
		Site objSite = new Site();
		objSite = siteService.getSiteByMax("SiteCode");

		String SE = "";

		if (objSite != null) {
			SE = objSite.getSiteCode();
		}

		if (SE == null || SE == "") {
			SE = "SE00001";
		} else {
			try {
				int NextIndex = Integer.parseInt(SE.replaceAll("SE", "")) + 1;

				if (NextIndex >= 10000) {
					SE = String.valueOf(NextIndex);
				} else if (NextIndex >= 1000) {
					SE = "0" + String.valueOf(NextIndex);
				} else if (NextIndex >= 100) {
					SE = "00" + String.valueOf(NextIndex);
				} else if (NextIndex >= 10) {
					SE = "000" + String.valueOf(NextIndex);
				} else {
					SE = "0000" + String.valueOf(NextIndex);
				}

				SE = "SE" + SE;
			} catch (Exception e) {
				SE = "SE00001";
			}
		}

		return SE;
	}

	/********************************************************************************************
	 * 
	 * Function Name : SitesPageLoad
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads site page.
	 * 
	 * Input Params : model,session,authentication
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/sites", method = RequestMethod.GET)
	public String SitesPageLoad(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());

		model.addAttribute("site", new Site());

		model.addAttribute("siteList", getUpdatedSiteList());
		model.addAttribute("sitetypeList", getDropdownList("SiteType"));
		model.addAttribute("customerList", getDropdownList("ConfigCustomer"));

		model.addAttribute("countryList", getDropdownList("Country"));
		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("stateList", getDropdownList("State"));
		model.addAttribute("operationmodeList", getDropdownList("OperationMode"));
		model.addAttribute("subfolderList", getDropdownList("SubFolder"));
		model.addAttribute("activeStatusList", getStatusDropdownList());
		model.addAttribute("siteList", getDropdownList("Site"));
		return "site";
	}

	/********************************************************************************************
	 * 
	 * Function Name : SiteUpdation
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function updates a site.
	 * 
	 * Input Params : SiteId,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/editsite", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> SiteUpdation(Integer SiteId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			Site objSite = siteService.getSiteById(SiteId);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			if (objSite.getSitePODate() != null) {
				objSite.setSitePoDateText(sdf.format(objSite.getSitePODate()));
			}
			if (objSite.getCod() != null) {
				objSite.setCodText(sdf.format(objSite.getCod()));
			}
			if (objSite.getCommencementofwork() != null) {
				objSite.setCommercementofworkText(sdf.format(objSite.getCommencementofwork()));
			}
			objResponseEntity.put("site", objSite);
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : viewSiteDocument
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads document upload page.
	 * 
	 * Input Params : model,session,authentication
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/DocumentsUpload", method = RequestMethod.GET)
	public String viewSiteDocument(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("site", new Site());

		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));
		return "sitedocument";
	}

	/********************************************************************************************
	 * 
	 * Function Name : downloadDoc
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads document download page.
	 * 
	 * Input Params : model,session,authentication
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/DocumentsDownload", method = RequestMethod.GET)
	public String downloadDoc(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("site", new Site());

		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));
		return "sitedocumentdownload";

	}

	/********************************************************************************************
	 * 
	 * Function Name : historicalPage
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads historical upload page.
	 * 
	 * Input Params : model,session,authentication
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/historicaluploads", method = RequestMethod.GET)
	public String historicalPage(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("site", new Site());

		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));
		return "historicalUpload";

	}

	/********************************************************************************************
	 * 
	 * Function Name : getSites
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns sites by customer.
	 * 
	 * Input Params : customerId
	 * 
	 * Return Value : String
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getSites", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Site> getSites(@RequestParam("customerId") String customerId) {
		return siteService.getSiteListByUserId(Integer.parseInt(customerId));
	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns value for dropdown.
	 * 
	 * Input Params : DropdownName,refid
	 * 
	 * Return Value : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}
}
