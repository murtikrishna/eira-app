
/******************************************************
 * 
 *    	Filename	: CustomerMapController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Controller deals with customer map related activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.CustomerMap;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.CurrencyService;
import com.mestech.eampm.service.CustomerMapService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteTypeService;
import com.mestech.eampm.service.StateService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class CustomerMapController {

	@Autowired
	private SiteService siteService;

	@Autowired
	private CustomerService customerService;
	@Autowired
	private SiteTypeService sitetypeService;
	@Autowired
	private CountryService countryService;
	@Autowired
	private StateService stateService;
	@Autowired
	private CurrencyService currencyService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private CustomerMapService customermapService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : SitePageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function retuns customer map details.
	 * 
	 * Input Params  : session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customermap", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> SitePageLoad(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				objResponseEntity.put("customermapList", getUpdatedCustomerMapList());
				objResponseEntity.put("customerList", getDropdownList("ConfigCustomer"));
				objResponseEntity.put("activeStatusList", getStatusDropdownList());
				objResponseEntity.put("userList", getDropdownList("User"));
				objResponseEntity.put("ticketcreation", new TicketDetail());
				objResponseEntity.put("siteList", getDropdownList("Site"));

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : SiteList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns customer map details list.
	 * 
	 * Input Params  : session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customermapdetailslist", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> SiteList(HttpSession session, Authentication authentication) {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();

		try {

			if (authentication == null) {
				objResponseEntity.put("customermapList", getUpdatedCustomerMapList());
				// objResponseEntity.put("status", true);
				// objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);
			} else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				objResponseEntity.put("customermapList", getUpdatedCustomerMapList());
				// objResponseEntity.put("status", true);
				// objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			// objResponseEntity.put("status", false);
			// objResponseEntity.put("data", e.getMessage());
			objResponseEntity.put("customermapList", null);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : Site
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function saves new customer map.
	 * 
	 * Input Params  : lstcustomermap,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>> 
	 * 
	 * Exceptions    : ConstraintViolationException,Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/newcustomermap", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> Site(@RequestBody List<CustomerMap> lstcustomermap, HttpSession session,
			Authentication authentication) throws ServletException {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String ResponseMessage = "";
		String Dataexists1 = "";

		try {

			if (lstcustomermap != null) {
				CustomerMap customermap = lstcustomermap.get(0);

				if (customermap != null) {

					Date date = new Date();
					if (customermap.getMapId() == null || customermap.getMapId() == 0) {

						List<CustomerMap> lstcustomermaps = customermapService.listCustomerMaps(0);

						System.out.println(lstcustomermaps.size());
						for (int i = 0; i < lstcustomermaps.size(); i++) {
							Integer customername = lstcustomermaps.get(i).getCustomerID();
							Integer username = lstcustomermaps.get(i).getUserID();

							System.out.println("enter this loop");
							System.out.println(customername);
							System.out.println(username);
							System.out.println(customermap.getCustomerID());
							System.out.println(customermap.getUserID());
							if (customermap.getCustomerID().equals(customername)
									&& customermap.getUserID().equals(username)) {

								Dataexists1 = "Customer Name and User Name Already Exist";

								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							} else {

							}

						}
						// new site, add it

						customermap.setCreationDate(date);
						customermap.setCreatedBy(id);

						customermapService.addCustomerMap(customermap);
						objResponseEntity.put("status", true);
						objResponseEntity.put("customermap", customermap);
						ResponseMessage = "Success";
					} else {
						// existing site, call update
						CustomerMap existingCustomerMap = customermapService
								.getCustomerMapById((customermap.getMapId()));

						List<CustomerMap> lstcustomermaps = customermapService.listCustomerMaps(customermap.getMapId());

						for (int i = 0; i < lstcustomermaps.size(); i++) {
							Integer customername = lstcustomermaps.get(i).getCustomerID();
							Integer username = lstcustomermaps.get(i).getUserID();

							if (customermap.getCustomerID().equals(customername)
									&& customermap.getUserID().equals(username)) {

								Dataexists1 = "CustomerName and UserName Already Exist";

								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						existingCustomerMap.setLastUpdatedDate(date);

						existingCustomerMap.setActiveFlag(customermap.getActiveFlag());
						existingCustomerMap.setCustomerID((customermap.getCustomerID()));
						existingCustomerMap.setUserID((customermap.getUserID()));

						existingCustomerMap.setLastUpdatedBy(id);

						customermapService.updateCustomerMap(existingCustomerMap);
						objResponseEntity.put("status", true);
						objResponseEntity.put("customermap", existingCustomerMap);
						ResponseMessage = "Success";
					}

				} else {
					objResponseEntity.put("status", false);
					ResponseMessage = "Invalid Request";
					objResponseEntity.put("customermap", null);
				}

			} else {
				objResponseEntity.put("status", false);
				ResponseMessage = "Invalid Request";
				objResponseEntity.put("customermap", null);
			}

			objResponseEntity.put("data", ResponseMessage);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (ConstraintViolationException cve) {
			objResponseEntity.put("customermap", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", cve.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			objResponseEntity.put("customermap", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns values for dropdown list.
	 * 
	 * Input Params  : DropdownName
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Customer") {
			List<Customer> ddlList = customerService.listCustomers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getCustomerId().toString(), ddlList.get(i).getCustomerName());
			}

		}
		if (DropdownName == "ConfigCustomer") {
			List<Customer> ddlList = customerService.listConfigCustomers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getCustomerId().toString(), ddlList.get(i).getCustomerName());
			}

		} else if (DropdownName == "User") {
			List<User> ddlList = userService.listAppUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		} else if (DropdownName == "Site") {
			List<Site> ddlList = siteService.listSites();

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				/*
				 * if(ddlList.get(i).getCustomerReference() != null) { SiteName = SiteName +
				 * " - " + ddlList.get(i).getCustomerReference(); }
				 */

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns values for dropdown list.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getUpdatedCustomerMapList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns updated customer map list.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : List<CustomerMap>
	 * 
	 * 
	 **********************************************************************************************/
	public List<CustomerMap> getUpdatedCustomerMapList() {
		List<CustomerMap> ddlCustomermapsList = customermapService.listCustomerMaps();

		List<Customer> ddlCustomerList = customerService.listConfigCustomers();
		List<User> ddlUserList = userService.listAppUsers();

		for (int i = 0; i < ddlCustomermapsList.size(); i++) {
			List<Customer> lstCustomerList = new ArrayList<Customer>();

			List<User> lstUserList = new ArrayList<User>();

			int cid = ddlCustomermapsList.get(i).getCustomerID();
			int uid = ddlCustomermapsList.get(i).getUserID();

			lstCustomerList = ddlCustomerList.stream().filter(p -> p.getCustomerId().equals(cid))
					.collect(Collectors.toList());
			lstUserList = ddlUserList.stream().filter(p -> p.getUserId().equals(uid)).collect(Collectors.toList());

			if (lstCustomerList.size() > 0) {
				ddlCustomermapsList.get(i).setCustomerName(lstCustomerList.get(0).getCustomerName());
			}
			if (lstUserList.size() > 0) {
				ddlCustomermapsList.get(i).setUserName(lstUserList.get(0).getUserName());

			}

		}

		return ddlCustomermapsList;
	}

	/********************************************************************************************
	 * 
	 * Function Name : listCustomermaps
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads customer map page.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customermaps", method = RequestMethod.GET)
	public String listCustomermaps(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("customermap", new CustomerMap());
		model.addAttribute("customermapList", getUpdatedCustomerMapList());
		model.addAttribute("customerList", getDropdownList("Customer"));
		model.addAttribute("activeStatusList", getStatusDropdownList());

		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));
		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());

		return "customermap";
	}

	/********************************************************************************************
	 * 
	 * Function Name : CustomerMapUpdation
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function updates customer map.
	 * 
	 * Input Params  : MapId,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/editcustomermap", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> CustomerMapUpdation(Integer MapId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			objResponseEntity.put("customermap", customermapService.getCustomerMapById(MapId));
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

}
