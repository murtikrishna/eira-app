
/******************************************************
 * 
 *    	Filename	: KpiController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with KPI related functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.bean.OpenStateTicketsCount;
import com.mestech.eampm.bean.TicketFilter;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EventDetail;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.CurrencyService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteTypeService;
import com.mestech.eampm.service.StateService;
import com.mestech.eampm.service.TicketDetailService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class KpiController {

	@Autowired
	private TicketDetailService ticketdetailService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private CustomerService customerService;
	@Autowired
	private SiteTypeService sitetypeService;
	@Autowired
	private CountryService countryService;
	@Autowired
	private StateService stateService;
	@Autowired
	private CurrencyService currencyService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : StateTicketFilterBySite
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns  tickets by site,filter
	 * 
	 * Input Params  : State,Siteid,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/sitestateticketfilter", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> StateTicketFilterBySite(String State, int Siteid, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				if (Siteid == 0) {
					List<TicketDetail> lstTicketDetails = ticketdetailService.listStatefilterTickets(State, id,
							timezonevalue);
					List<Site> lstSite = siteService.listSites();

					List<User> lstUser = userService.listUsers();

					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
					SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

					for (int i = 0; i < lstTicketDetails.size(); i++) {
						Integer intSiteID = lstTicketDetails.get(i).getSiteID();
						Integer intAssignedID = lstTicketDetails.get(i).getAssignedTo();
						Integer intequipmentID = lstTicketDetails.get(i).getEquipmentID();

						if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
								.size() >= 1) {
							Site objSite = new Site();
							objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetails.get(i).setSiteName(objSite.getSiteName());
						}

						if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetails.get(i).setAssignedToWhom(objUser.getUserName());

						}

						if (lstTicketDetails.get(i).getCreationDate() != null) {
							lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
							lstTicketDetails.get(i)
									.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

							// System.out.println( " Mohanraj ::: " +
							// sdf.format(lstTicketDetail.get(i).getCreationDate()));
						}

						if (lstTicketDetails.get(i).getScheduledOn() != null) {
							lstTicketDetails.get(i).setScheduledOn(lstTicketDetails.get(i).getScheduledOn());
							lstTicketDetails.get(i)
									.setScheduledDateText(sdfdate.format(lstTicketDetails.get(i).getScheduledOn()));

						}

					}

					objResponseEntity.put("ticketgriddata", lstTicketDetails);
					objResponseEntity.put("ticketcreation", new TicketDetail());
					objResponseEntity.put("data", "Success");
					return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
				}

				else {
					List<TicketDetail> lstTicketDetails = ticketdetailService.listStatefilterTicketsBySite(State,
							Siteid, timezonevalue);
					List<Site> lstSite = siteService.listSiteById(Siteid);

					List<User> lstUser = userService.listUsers();

					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
					SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

					for (int i = 0; i < lstTicketDetails.size(); i++) {
						Integer intSiteID = lstTicketDetails.get(i).getSiteID();
						Integer intAssignedID = lstTicketDetails.get(i).getAssignedTo();
						Integer intequipmentID = lstTicketDetails.get(i).getEquipmentID();

						if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
								.size() >= 1) {
							Site objSite = new Site();
							objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetails.get(i).setSiteName(objSite.getSiteName());
						}

						if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).size() >= 1) {
							User objUser = new User();
							objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
									.collect(Collectors.toList()).get(0);

							lstTicketDetails.get(i).setAssignedToWhom(objUser.getUserName());

						}

						if (lstTicketDetails.get(i).getCreationDate() != null) {
							lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
							lstTicketDetails.get(i)
									.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

							// System.out.println( " Mohanraj ::: " +
							// sdf.format(lstTicketDetail.get(i).getCreationDate()));
						}

						if (lstTicketDetails.get(i).getScheduledOn() != null) {
							lstTicketDetails.get(i).setScheduledOn(lstTicketDetails.get(i).getScheduledOn());
							lstTicketDetails.get(i)
									.setScheduledDateText(sdfdate.format(lstTicketDetails.get(i).getScheduledOn()));

						}

					}

					objResponseEntity.put("ticketgriddata", lstTicketDetails);

					objResponseEntity.put("data", "Success");
					return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : StateTicketFilterByCustomer
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns tickets by customer,filter
	 * 
	 * Input Params  : State,Customerid,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customerstateticketfilter", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> StateTicketFilterByCustomer(String State, int Customerid,
			HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<TicketDetail> lstTicketDetails = ticketdetailService.listStatefilterTickets(State, id,
						timezonevalue);
				List<Site> lstSite = siteService.getSiteListByUserId(id);

				List<User> lstUser = userService.listUsers();

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				for (int i = 0; i < lstTicketDetails.size(); i++) {
					Integer intSiteID = lstTicketDetails.get(i).getSiteID();
					Integer intAssignedID = lstTicketDetails.get(i).getAssignedTo();
					Integer intequipmentID = lstTicketDetails.get(i).getEquipmentID();

					if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
							.size() >= 1) {
						Site objSite = new Site();
						objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setSiteName(objSite.getSiteName());
					}

					if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setAssignedToWhom(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getCreationDate() != null) {
						lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
						lstTicketDetails.get(i)
								.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

						// System.out.println( " Mohanraj ::: " +
						// sdf.format(lstTicketDetail.get(i).getCreationDate()));
					}

					if (lstTicketDetails.get(i).getScheduledOn() != null) {
						lstTicketDetails.get(i).setScheduledOn(lstTicketDetails.get(i).getScheduledOn());
						lstTicketDetails.get(i)
								.setScheduledDateText(sdfdate.format(lstTicketDetails.get(i).getScheduledOn()));

					}

				}

				objResponseEntity.put("ticketgriddata", lstTicketDetails);

				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : kpiTicketsPgaeLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads tickets kpi page.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/ticketskpi", method = RequestMethod.GET)
	public String kpiTicketsPgaeLoad(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());
			objAccessListBean.setMySiteFilter(false);
			objAccessListBean.setMySiteID(0);

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("ticketfilter", new TicketFilter());

		model.addAttribute("ticketdetail", new TicketDetail());
		model.addAttribute("access", objAccessListBean);

		model.addAttribute("siteList", getNewTicketSiteList("Site", id));

		model.addAttribute("ticketcreation", new TicketDetail());

		return "ticketskpi";
	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		}

		if (DropdownName == "CustomerSites") {
			List<Site> ddlList = siteService.getSiteListByCustomerId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "SingleSite") {
			List<Site> ddlList = siteService.listSiteById(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "User") {
			List<User> ddlList = userService.listFieldUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : SiteTicketsPageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads tickets kpi page by site.
	 * 
	 * Input Params  : siteid,model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/siteticketskpi{id}", method = RequestMethod.GET)
	public String SiteTicketsPageLoad(@PathVariable("id") int siteid, Model model, HttpSession session,
			Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int userid = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(userid);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(userid);
			objAccessListBean.setUserName(objUser.getShortName());

			objAccessListBean.setMySiteFilter(true);
			objAccessListBean.setMySiteID(siteid);

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("ticketfilter", new TicketFilter());

		model.addAttribute("eventdetail", new EventDetail());
		model.addAttribute("access", objAccessListBean);

		model.addAttribute("siteList", getNewTicketSiteList("Site", userid));

		model.addAttribute("ticketcreation", new TicketDetail());

		return "ticketskpi";
	}

	/********************************************************************************************
	 * 
	 * Function Name : CustomerTicketsPageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads tickets kpi page by customer.
	 * 
	 * Input Params  : id,model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customerticketskpi{id}", method = RequestMethod.GET)
	public String CustomerTicketsPageLoad(@PathVariable("id") int id, Model model, HttpSession session,
			Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int userid = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(userid);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(userid);
			objAccessListBean.setUserName(objUser.getShortName());
			objAccessListBean.setMyCustomerFilter(true);
			objAccessListBean.setMyCustomerID(id);

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("ticketfilter", new TicketFilter());

		model.addAttribute("eventdetail", new EventDetail());
		model.addAttribute("access", objAccessListBean);

		model.addAttribute("siteList", getNewTicketSiteList("Site", userid));

		model.addAttribute("ticketcreation", new TicketDetail());

		return "ticketskpi";
	}

	/********************************************************************************************
	 * 
	 * Function Name : OpenStateTicketFilter
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns tickets by open state.
	 * 
	 * Input Params  : State,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/openticketfilter", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> OpenStateTicketFilter(String State, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<TicketDetail> lstTicketDetails = ticketdetailService.listStatefilterTickets(State, id,
						timezonevalue);
				List<Site> lstSite = siteService.listSites();

				List<User> lstUser = userService.listUsers();

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				for (int i = 0; i < lstTicketDetails.size(); i++) {
					Integer intSiteID = lstTicketDetails.get(i).getSiteID();
					Integer intAssignedID = lstTicketDetails.get(i).getAssignedTo();
					Integer intequipmentID = lstTicketDetails.get(i).getEquipmentID();

					if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
							.size() >= 1) {
						Site objSite = new Site();
						objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setSiteName(objSite.getSiteName());
					}

					if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setAssignedToWhom(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getCreationDate() != null) {
						lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
						lstTicketDetails.get(i)
								.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

						// System.out.println( " Mohanraj ::: " +
						// sdf.format(lstTicketDetail.get(i).getCreationDate()));
					}
					if (lstTicketDetails.get(i).getCreationDate() != null) {
						lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
						lstTicketDetails.get(i)
								.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

						// System.out.println( " Mohanraj ::: " +
						// sdf.format(lstTicketDetail.get(i).getCreationDate()));
					}

					if (lstTicketDetails.get(i).getScheduledOn() != null) {
						lstTicketDetails.get(i).setScheduledOn(lstTicketDetails.get(i).getScheduledOn());
						lstTicketDetails.get(i)
								.setScheduledDateText(sdfdate.format(lstTicketDetails.get(i).getScheduledOn()));

					}

				}

				objResponseEntity.put("ticketgriddata", lstTicketDetails);
				objResponseEntity.put("ticketcreation", new TicketDetail());
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : TicketsKpiSlide1
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns data for slide 1 of Tickets kpi.
	 * 
	 * Input Params  : session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>> 
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/slide1", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> TicketsKpiSlide1(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				Map<String, List<OpenStateTicketsCount>> result = new LinkedHashMap<String, List<OpenStateTicketsCount>>();

				List<String> categories = new ArrayList<String>();

				List<Map<String, Object>> series = new ArrayList<Map<String, Object>>();

				List<OpenStateTicketsCount> lstTicketDetail1 = ticketdetailService.Allstatepiechart(id, timezonevalue);
				List<OpenStateTicketsCount> lstTicketDetail2 = ticketdetailService.Operationallstatepiechart(id,
						timezonevalue);
				List<OpenStateTicketsCount> lstTicketDetail3 = ticketdetailService.Maintenanceallstatepiechart(id,
						timezonevalue);

				String finaldata = "";
				String chartdata1 = "";
				String chartdata2 = "";
				String chartdata3 = "";
				Integer iCnt = 0;
				Integer iCnt1 = 0;
				Integer iCnt2 = 0;
				if (lstTicketDetail1.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail1) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata1 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata1 = chartdata1 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt = iCnt + 1;
						System.out.println(chartdata1);
					}

				}
				if (lstTicketDetail2.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail2) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt1 == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata2 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata2 = chartdata2 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt1 = iCnt1 + 1;
						System.out.println(chartdata2);
					}

				}
				if (lstTicketDetail3.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail3) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt2 == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata3 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata3 = chartdata3 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt2 = iCnt2 + 1;
						System.out.println(chartdata3);
					}

				}

				objResponseEntity.put("chart1", chartdata1);
				objResponseEntity.put("chart2", chartdata2);
				objResponseEntity.put("chart3", chartdata3);

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : TicketsKpiSlide2
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns data for slide 2 of Tickets kpi.
	 * 
	 * Input Params  : session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>> 
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/slide2", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> TicketsKpiSlide2(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				Map<String, List<OpenStateTicketsCount>> result = new LinkedHashMap<String, List<OpenStateTicketsCount>>();

				List<String> categories = new ArrayList<String>();

				List<Map<String, Object>> series = new ArrayList<Map<String, Object>>();

				List<OpenStateTicketsCount> lstTicketDetail1 = ticketdetailService.Holdstateandstatustikcets(id,
						timezonevalue);
				List<OpenStateTicketsCount> lstTicketDetail2 = ticketdetailService.Openstateandstatustikcets(id,
						timezonevalue);
				List<OpenStateTicketsCount> lstTicketDetail3 = ticketdetailService.Closestateandstatustikcets(id,
						timezonevalue);

				String finaldata = "";
				String chartdata1 = "";
				String chartdata2 = "";
				String chartdata3 = "";
				Integer iCnt = 0;
				Integer iCnt1 = 0;
				Integer iCnt2 = 0;
				if (lstTicketDetail1.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail1) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata1 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata1 = chartdata1 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt = iCnt + 1;
						System.out.println(chartdata1);
					}

				}
				if (lstTicketDetail2.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail2) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt1 == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata2 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata2 = chartdata2 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt1 = iCnt1 + 1;
						System.out.println(chartdata2);
					}

				}
				if (lstTicketDetail3.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail3) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt2 == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata3 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata3 = chartdata3 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt2 = iCnt2 + 1;
						System.out.println(chartdata3);
					}

				}

				List<TicketDetail> lstTicketDetails = ticketdetailService.getTicketDetailListByUserIdLimit(id,
						timezonevalue);
				List<Site> lstSite = siteService.getSiteListByUserId(id);

				List<Equipment> lstEquipment = equipmentService.listEquipmentsByUserId(id);

				List<User> lstUser = userService.listUsers();
				for (int i = 0; i < lstTicketDetails.size(); i++) {
					Integer intSiteID = lstTicketDetails.get(i).getSiteID();
					Integer intAssignedID = lstTicketDetails.get(i).getAssignedTo();
					Integer intequipmentID = lstTicketDetails.get(i).getEquipmentID();
					Integer intCreatedBy = lstTicketDetails.get(i).getCreatedBy();

					if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
							.size() >= 1) {
						Site objSite = new Site();
						objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setSiteName(objSite.getSiteName());
					}
					if (lstEquipment.stream().filter(p -> p.getEquipmentId().equals(intequipmentID))
							.collect(Collectors.toList()).size() >= 1) {
						Equipment objEquipment = new Equipment();
						objEquipment = lstEquipment.stream().filter(p -> p.getEquipmentId().equals(intequipmentID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setEquipmentName(objEquipment.getCustomerNaming().toString());
					}

					if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setAssignedToWhom(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getCreationDate() != null) {
						lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
						lstTicketDetails.get(i)
								.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

						// System.out.println( " Mohanraj ::: " +
						// sdf.format(lstTicketDetail.get(i).getCreationDate()));
					}
					if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setCreatedByName(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getScheduledOn() != null) {
						lstTicketDetails.get(i).setScheduledOn(lstTicketDetails.get(i).getScheduledOn());
						lstTicketDetails.get(i)
								.setScheduledDateText(sdfdate.format(lstTicketDetails.get(i).getScheduledOn()));

					}

				}

				objResponseEntity.put("ticketgriddata", lstTicketDetails);
				objResponseEntity.put("chart1", chartdata1);
				objResponseEntity.put("chart2", chartdata2);
				objResponseEntity.put("chart3", chartdata3);
				objResponseEntity.put("siteList", getDropdownList("Site", id));
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : TicketsKpiSlide1BySite
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns data for slide 1 of ticket kpi by site.
	 * 
	 * Input Params  : SiteId,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/slide1BySite", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> TicketsKpiSlide1BySite(Integer SiteId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				Map<String, List<OpenStateTicketsCount>> result = new LinkedHashMap<String, List<OpenStateTicketsCount>>();

				List<String> categories = new ArrayList<String>();

				List<Map<String, Object>> series = new ArrayList<Map<String, Object>>();

				List<OpenStateTicketsCount> lstTicketDetail1 = ticketdetailService.AllstatepiechartBySite(SiteId,
						timezonevalue);
				List<OpenStateTicketsCount> lstTicketDetail2 = ticketdetailService
						.OperationallstatepiechartBySite(SiteId, timezonevalue);
				List<OpenStateTicketsCount> lstTicketDetail3 = ticketdetailService
						.MaintenanceallstatepiechartBySite(SiteId, timezonevalue);

				Site sitedetails = siteService.getSiteById(SiteId);
				String finaldata = "";
				String chartdata1 = "";
				String chartdata2 = "";
				String chartdata3 = "";
				Integer iCnt = 0;
				Integer iCnt1 = 0;
				Integer iCnt2 = 0;
				if (lstTicketDetail1.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail1) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata1 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata1 = chartdata1 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt = iCnt + 1;
						System.out.println(chartdata1);
					}

				}
				if (lstTicketDetail2.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail2) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt1 == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata2 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata2 = chartdata2 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt1 = iCnt1 + 1;
						System.out.println(chartdata2);
					}

				}
				if (lstTicketDetail3.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail3) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt2 == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata3 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata3 = chartdata3 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt2 = iCnt2 + 1;
						System.out.println(chartdata3);
					}

				}

				objResponseEntity.put("chart1", chartdata1);
				objResponseEntity.put("chart2", chartdata2);
				objResponseEntity.put("chart3", chartdata3);
				objResponseEntity.put("sitedetails", sitedetails);
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : TicketsKpiSlide2BySite
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns data for slide 2 of ticket kpi by site.
	 * 
	 * Input Params  : SiteId,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/slide2BySite", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> TicketsKpiSlide2BySite(Integer SiteId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				Map<String, List<OpenStateTicketsCount>> result = new LinkedHashMap<String, List<OpenStateTicketsCount>>();

				List<String> categories = new ArrayList<String>();

				List<Map<String, Object>> series = new ArrayList<Map<String, Object>>();

				List<OpenStateTicketsCount> lstTicketDetail1 = ticketdetailService
						.HoldstateandstatustikcetsBySite(SiteId, timezonevalue);
				List<OpenStateTicketsCount> lstTicketDetail2 = ticketdetailService
						.OpenstateandstatustikcetsBySite(SiteId, timezonevalue);
				List<OpenStateTicketsCount> lstTicketDetail3 = ticketdetailService
						.ClosestateandstatustikcetsBySite(SiteId, timezonevalue);
				Site sitedetails = siteService.getSiteById(SiteId);
				String finaldata = "";
				String chartdata1 = "";
				String chartdata2 = "";
				String chartdata3 = "";
				Integer iCnt = 0;
				Integer iCnt1 = 0;
				Integer iCnt2 = 0;
				if (lstTicketDetail1.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail1) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata1 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata1 = chartdata1 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt = iCnt + 1;
						System.out.println(chartdata1);
					}

				}
				if (lstTicketDetail2.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail2) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt1 == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata2 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata2 = chartdata2 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt1 = iCnt1 + 1;
						System.out.println(chartdata2);
					}

				}
				if (lstTicketDetail3.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail3) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt2 == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata3 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata3 = chartdata3 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt2 = iCnt2 + 1;
						System.out.println(chartdata3);
					}

				}

				List<TicketDetail> lstTicketDetails = ticketdetailService.getTicketDetailListBySiteLimit(SiteId,
						timezonevalue);
				List<Site> lstSite = siteService.getSiteListByUserId(id);

				List<Equipment> lstEquipment = equipmentService.listEquipmentsByUserId(id);

				List<User> lstUser = userService.listUsers();
				for (int i = 0; i < lstTicketDetails.size(); i++) {
					Integer intSiteID = lstTicketDetails.get(i).getSiteID();
					Integer intAssignedID = lstTicketDetails.get(i).getAssignedTo();
					Integer intequipmentID = lstTicketDetails.get(i).getEquipmentID();
					Integer intCreatedBy = lstTicketDetails.get(i).getCreatedBy();

					if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
							.size() >= 1) {
						Site objSite = new Site();
						objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setSiteName(objSite.getSiteName());
					}
					if (lstEquipment.stream().filter(p -> p.getEquipmentId().equals(intequipmentID))
							.collect(Collectors.toList()).size() >= 1) {
						Equipment objEquipment = new Equipment();
						objEquipment = lstEquipment.stream().filter(p -> p.getEquipmentId().equals(intequipmentID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setEquipmentName(objEquipment.getCustomerNaming().toString());
					}

					if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setAssignedToWhom(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getCreationDate() != null) {
						lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
						lstTicketDetails.get(i)
								.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

						// System.out.println( " Mohanraj ::: " +
						// sdf.format(lstTicketDetail.get(i).getCreationDate()));
					}
					if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setCreatedByName(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getScheduledOn() != null) {
						lstTicketDetails.get(i).setScheduledOn(lstTicketDetails.get(i).getScheduledOn());
						lstTicketDetails.get(i)
								.setScheduledDateText(sdfdate.format(lstTicketDetails.get(i).getScheduledOn()));

					}

				}

				objResponseEntity.put("ticketgriddata", lstTicketDetails);
				objResponseEntity.put("chart1", chartdata1);
				objResponseEntity.put("chart2", chartdata2);
				objResponseEntity.put("chart3", chartdata3);
				objResponseEntity.put("sitedetails", sitedetails);

				objResponseEntity.put("siteList", getDropdownList("SingleSite", SiteId));
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : slide1ByCustomer
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns data for side 1 of tickets kpi by customer
	 * 
	 * Input Params  : CustomerId,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/slide1ByCustomer", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> slide1ByCustomer(Integer CustomerId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				Map<String, List<OpenStateTicketsCount>> result = new LinkedHashMap<String, List<OpenStateTicketsCount>>();

				List<String> categories = new ArrayList<String>();

				List<Map<String, Object>> series = new ArrayList<Map<String, Object>>();

				List<OpenStateTicketsCount> lstTicketDetail1 = ticketdetailService.AllstatepiechartByCustomer(id,
						timezonevalue);
				List<OpenStateTicketsCount> lstTicketDetail2 = ticketdetailService
						.OperationallstatepiechartByCustomer(id, timezonevalue);
				List<OpenStateTicketsCount> lstTicketDetail3 = ticketdetailService
						.MaintenanceallstatepiechartByCustomer(id, timezonevalue);
				Customer customerdetails = customerService.getCustomerById(CustomerId);
				String finaldata = "";
				String chartdata1 = "";
				String chartdata2 = "";
				String chartdata3 = "";
				Integer iCnt = 0;
				Integer iCnt1 = 0;
				Integer iCnt2 = 0;
				if (lstTicketDetail1.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail1) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata1 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata1 = chartdata1 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt = iCnt + 1;
						System.out.println(chartdata1);
					}

				}
				if (lstTicketDetail2.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail2) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt1 == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata2 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata2 = chartdata2 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt1 = iCnt1 + 1;
						System.out.println(chartdata2);
					}

				}
				if (lstTicketDetail3.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail3) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt2 == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata3 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata3 = chartdata3 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt2 = iCnt2 + 1;
						System.out.println(chartdata3);
					}

				}

				objResponseEntity.put("chart1", chartdata1);
				objResponseEntity.put("chart2", chartdata2);
				objResponseEntity.put("chart3", chartdata3);
				objResponseEntity.put("customerdetails", customerdetails);

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : slide2ByCustomer
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns data for slide 2 of tickets kpi page by customer.
	 * 
	 * Input Params  : CustomerId,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/slide2ByCustomer", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> slide2ByCustomer(Integer CustomerId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				Map<String, List<OpenStateTicketsCount>> result = new LinkedHashMap<String, List<OpenStateTicketsCount>>();

				List<String> categories = new ArrayList<String>();

				List<Map<String, Object>> series = new ArrayList<Map<String, Object>>();

				List<OpenStateTicketsCount> lstTicketDetail1 = ticketdetailService
						.HoldstateandstatustikcetsByCustomer(id, timezonevalue);
				List<OpenStateTicketsCount> lstTicketDetail2 = ticketdetailService
						.OpenstateandstatustikcetsByCustomer(id, timezonevalue);
				List<OpenStateTicketsCount> lstTicketDetail3 = ticketdetailService
						.ClosestateandstatustikcetsByCustomer(id, timezonevalue);
				Customer customerdetails = customerService.getCustomerById(CustomerId);
				String finaldata = "";
				String chartdata1 = "";
				String chartdata2 = "";
				String chartdata3 = "";
				Integer iCnt = 0;
				Integer iCnt1 = 0;
				Integer iCnt2 = 0;
				if (lstTicketDetail1.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail1) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata1 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata1 = chartdata1 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt = iCnt + 1;
						System.out.println(chartdata1);
					}

				}
				if (lstTicketDetail2.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail2) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt1 == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata2 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata2 = chartdata2 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt1 = iCnt1 + 1;
						System.out.println(chartdata2);
					}

				}
				if (lstTicketDetail3.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					for (Object obj : lstTicketDetail3) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						Color = str[2].toString();
						/*
						 * o.setTicketcount(str[1].toString()); o.setColor(str[2].toString());
						 */

						if (iCnt2 == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata3 = "{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							chartdata3 = chartdata3 + ",{name:'" + State + "',y:" + Count + ",color:'" + Color + "'}";
						}

						iCnt2 = iCnt2 + 1;
						System.out.println(chartdata3);
					}

				}

				List<TicketDetail> lstTicketDetails = ticketdetailService.getTicketDetailListByCustomerLimit(id,
						timezonevalue);
				List<Site> lstSite = siteService.getSiteListByUserId(id);

				List<Equipment> lstEquipment = equipmentService.listEquipmentsByUserId(id);

				List<User> lstUser = userService.listUsers();
				for (int i = 0; i < lstTicketDetails.size(); i++) {
					Integer intSiteID = lstTicketDetails.get(i).getSiteID();
					Integer intAssignedID = lstTicketDetails.get(i).getAssignedTo();
					Integer intequipmentID = lstTicketDetails.get(i).getEquipmentID();
					Integer intCreatedBy = lstTicketDetails.get(i).getCreatedBy();

					if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
							.size() >= 1) {
						Site objSite = new Site();
						objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setSiteName(objSite.getSiteName());
					}
					if (lstEquipment.stream().filter(p -> p.getEquipmentId().equals(intequipmentID))
							.collect(Collectors.toList()).size() >= 1) {
						Equipment objEquipment = new Equipment();
						objEquipment = lstEquipment.stream().filter(p -> p.getEquipmentId().equals(intequipmentID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setEquipmentName(objEquipment.getCustomerNaming().toString());
					}

					if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setAssignedToWhom(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getCreationDate() != null) {
						lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
						lstTicketDetails.get(i)
								.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

						// System.out.println( " Mohanraj ::: " +
						// sdf.format(lstTicketDetail.get(i).getCreationDate()));
					}
					if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setCreatedByName(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getScheduledOn() != null) {
						lstTicketDetails.get(i).setScheduledOn(lstTicketDetails.get(i).getScheduledOn());
						lstTicketDetails.get(i)
								.setScheduledDateText(sdfdate.format(lstTicketDetails.get(i).getScheduledOn()));

					}

				}

				objResponseEntity.put("ticketgriddata", lstTicketDetails);
				objResponseEntity.put("chart1", chartdata1);
				objResponseEntity.put("chart2", chartdata2);
				objResponseEntity.put("chart3", chartdata3);
				objResponseEntity.put("customerdetails", customerdetails);
				objResponseEntity.put("siteList", getNewTicketSiteList("Site", id));
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : TicketTypeFilter
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns ticket type by filter.
	 * 
	 * Input Params  : Type,State,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/typeticketfilter", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> TicketTypeFilter(String Type, String State, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<TicketDetail> lstTicketDetails = ticketdetailService.listStatefilterTickets(Type, State, id,
						timezonevalue);
				List<Site> lstSite = siteService.listSites();

				List<User> lstUser = userService.listUsers();

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				for (int i = 0; i < lstTicketDetails.size(); i++) {
					Integer intSiteID = lstTicketDetails.get(i).getSiteID();
					Integer intAssignedID = lstTicketDetails.get(i).getAssignedTo();
					Integer intequipmentID = lstTicketDetails.get(i).getEquipmentID();
					Integer intCreatedBy = lstTicketDetails.get(i).getCreatedBy();

					if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
							.size() >= 1) {
						Site objSite = new Site();
						objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setSiteName(objSite.getSiteName());
					}

					if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setAssignedToWhom(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getCreationDate() != null) {
						lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
						lstTicketDetails.get(i)
								.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

						// System.out.println( " Mohanraj ::: " +
						// sdf.format(lstTicketDetail.get(i).getCreationDate()));
					}
					if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setCreatedByName(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getScheduledOn() != null) {
						lstTicketDetails.get(i).setScheduledOn(lstTicketDetails.get(i).getScheduledOn());
						lstTicketDetails.get(i)
								.setScheduledDateText(sdfdate.format(lstTicketDetails.get(i).getScheduledOn()));

					}

				}

				objResponseEntity.put("ticketgriddata", lstTicketDetails);
				objResponseEntity.put("ticketcreation", new TicketDetail());
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : StateandstatusTicketFilter
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns tickets by status.
	 * 
	 * Input Params  : Type,Status,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/Allstateandstatusticketfilter", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> StateandstatusTicketFilter(String Type, int Status, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<TicketDetail> lstTicketDetails = ticketdetailService.AllStateandStatusfilterTickets(Type, Status,
						id, timezonevalue);
				List<Site> lstSite = siteService.listSites();

				List<User> lstUser = userService.listUsers();

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				for (int i = 0; i < lstTicketDetails.size(); i++) {
					Integer intSiteID = lstTicketDetails.get(i).getSiteID();
					Integer intAssignedID = lstTicketDetails.get(i).getAssignedTo();
					Integer intequipmentID = lstTicketDetails.get(i).getEquipmentID();
					Integer intCreatedBy = lstTicketDetails.get(i).getCreatedBy();

					if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
							.size() >= 1) {
						Site objSite = new Site();
						objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setSiteName(objSite.getSiteName());
					}

					if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setAssignedToWhom(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getCreationDate() != null) {
						lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
						lstTicketDetails.get(i)
								.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

						// System.out.println( " Mohanraj ::: " +
						// sdf.format(lstTicketDetail.get(i).getCreationDate()));
					}
					if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setCreatedByName(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getScheduledOn() != null) {
						lstTicketDetails.get(i).setScheduledOn(lstTicketDetails.get(i).getScheduledOn());
						lstTicketDetails.get(i)
								.setScheduledDateText(sdfdate.format(lstTicketDetails.get(i).getScheduledOn()));

					}

				}

				objResponseEntity.put("ticketgriddata", lstTicketDetails);
				objResponseEntity.put("ticketcreation", new TicketDetail());
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : TicketTypeFilterBySite
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns tickets by site.
	 * 
	 * Input Params  : Type,State,SiteId,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>> 
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/typeticketfilterBySite", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> TicketTypeFilterBySite(String Type, String State, int SiteId,
			HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<TicketDetail> lstTicketDetails = ticketdetailService.listStatefilterTicketsBySite(Type, State,
						SiteId, timezonevalue);
				List<Site> lstSite = siteService.listSites();

				List<User> lstUser = userService.listUsers();

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				for (int i = 0; i < lstTicketDetails.size(); i++) {
					Integer intSiteID = lstTicketDetails.get(i).getSiteID();
					Integer intAssignedID = lstTicketDetails.get(i).getAssignedTo();
					Integer intequipmentID = lstTicketDetails.get(i).getEquipmentID();
					Integer intCreatedBy = lstTicketDetails.get(i).getCreatedBy();

					if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
							.size() >= 1) {
						Site objSite = new Site();
						objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setSiteName(objSite.getSiteName());
					}

					if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setAssignedToWhom(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getCreationDate() != null) {
						lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
						lstTicketDetails.get(i)
								.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

						// System.out.println( " Mohanraj ::: " +
						// sdf.format(lstTicketDetail.get(i).getCreationDate()));
					}
					if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setCreatedByName(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getScheduledOn() != null) {
						lstTicketDetails.get(i).setScheduledOn(lstTicketDetails.get(i).getScheduledOn());
						lstTicketDetails.get(i)
								.setScheduledDateText(sdfdate.format(lstTicketDetails.get(i).getScheduledOn()));

					}

				}

				objResponseEntity.put("ticketgriddata", lstTicketDetails);
				objResponseEntity.put("ticketcreation", new TicketDetail());
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : StateandstatusTicketFilterBySite
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns ticket by filtering state and site.
	 * 
	 * Input Params  : Type,Status,SiteId,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>> 
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/AllstateandstatusticketfilterBySite", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> StateandstatusTicketFilterBySite(String Type, int Status, int SiteId,
			HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<TicketDetail> lstTicketDetails = ticketdetailService.AllStateandStatusfilterTicketsBySite(Type,
						Status, SiteId, timezonevalue);
				List<Site> lstSite = siteService.listSites();

				List<User> lstUser = userService.listUsers();

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				for (int i = 0; i < lstTicketDetails.size(); i++) {
					Integer intSiteID = lstTicketDetails.get(i).getSiteID();
					Integer intAssignedID = lstTicketDetails.get(i).getAssignedTo();
					Integer intequipmentID = lstTicketDetails.get(i).getEquipmentID();
					Integer intCreatedBy = lstTicketDetails.get(i).getCreatedBy();

					if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
							.size() >= 1) {
						Site objSite = new Site();
						objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setSiteName(objSite.getSiteName());
					}

					if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setAssignedToWhom(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getCreationDate() != null) {
						lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
						lstTicketDetails.get(i)
								.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

						// System.out.println( " Mohanraj ::: " +
						// sdf.format(lstTicketDetail.get(i).getCreationDate()));
					}
					if (lstTicketDetails.get(i).getCreationDate() != null) {
						lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
						lstTicketDetails.get(i)
								.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

						// System.out.println( " Mohanraj ::: " +
						// sdf.format(lstTicketDetail.get(i).getCreationDate()));
					}
					if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setCreatedByName(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getScheduledOn() != null) {
						lstTicketDetails.get(i).setScheduledOn(lstTicketDetails.get(i).getScheduledOn());
						lstTicketDetails.get(i)
								.setScheduledDateText(sdfdate.format(lstTicketDetails.get(i).getScheduledOn()));

					}

				}

				objResponseEntity.put("ticketgriddata", lstTicketDetails);
				objResponseEntity.put("ticketcreation", new TicketDetail());
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : TicketTypeFilterByCustomer
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns tickets by type and customer.
	 * 
	 * Input Params  : Type,State,CustomerId,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/typeticketfilterByCustomer", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> TicketTypeFilterByCustomer(String Type, String State, int CustomerId,
			HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<TicketDetail> lstTicketDetails = ticketdetailService.listTypefilterTicketsByCustomer(Type, State,
						id, timezonevalue);
				List<Site> lstSite = siteService.listSites();

				List<User> lstUser = userService.listUsers();

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				for (int i = 0; i < lstTicketDetails.size(); i++) {
					Integer intSiteID = lstTicketDetails.get(i).getSiteID();
					Integer intAssignedID = lstTicketDetails.get(i).getAssignedTo();
					Integer intequipmentID = lstTicketDetails.get(i).getEquipmentID();
					Integer intCreatedBy = lstTicketDetails.get(i).getCreatedBy();

					if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
							.size() >= 1) {
						Site objSite = new Site();
						objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setSiteName(objSite.getSiteName());
					}

					if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setAssignedToWhom(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getCreationDate() != null) {
						lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
						lstTicketDetails.get(i)
								.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

						// System.out.println( " Mohanraj ::: " +
						// sdf.format(lstTicketDetail.get(i).getCreationDate()));
					}
					if (lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intCreatedBy))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setCreatedByName(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getScheduledOn() != null) {
						lstTicketDetails.get(i).setScheduledOn(lstTicketDetails.get(i).getScheduledOn());
						lstTicketDetails.get(i)
								.setScheduledDateText(sdfdate.format(lstTicketDetails.get(i).getScheduledOn()));

					}

				}

				objResponseEntity.put("ticketgriddata", lstTicketDetails);
				objResponseEntity.put("ticketcreation", new TicketDetail());
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : StateandstatusTicketFilterByCustomer
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns tickets by state,status and customer.
	 * 
	 * Input Params  : Type,Status,CustomerId,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/AllstateandstatusticketfilterByCustomer", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> StateandstatusTicketFilterByCustomer(String Type, int Status,
			int CustomerId, HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<TicketDetail> lstTicketDetails = ticketdetailService.AllStateandStatusfilterTicketsByCustomer(Type,
						Status, id, timezonevalue);
				List<Site> lstSite = siteService.listSites();

				List<User> lstUser = userService.listUsers();

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				for (int i = 0; i < lstTicketDetails.size(); i++) {
					Integer intSiteID = lstTicketDetails.get(i).getSiteID();
					Integer intAssignedID = lstTicketDetails.get(i).getAssignedTo();
					Integer intequipmentID = lstTicketDetails.get(i).getEquipmentID();

					if (lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID)).collect(Collectors.toList())
							.size() >= 1) {
						Site objSite = new Site();
						objSite = lstSite.stream().filter(p -> p.getSiteId().equals(intSiteID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setSiteName(objSite.getSiteName());
					}

					if (lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID)).collect(Collectors.toList())
							.size() >= 1) {
						User objUser = new User();
						objUser = lstUser.stream().filter(p -> p.getUserId().equals(intAssignedID))
								.collect(Collectors.toList()).get(0);

						lstTicketDetails.get(i).setAssignedToWhom(objUser.getUserName());

					}

					if (lstTicketDetails.get(i).getCreationDate() != null) {
						lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
						lstTicketDetails.get(i)
								.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

						// System.out.println( " Mohanraj ::: " +
						// sdf.format(lstTicketDetail.get(i).getCreationDate()));
					}
					if (lstTicketDetails.get(i).getCreationDate() != null) {
						lstTicketDetails.get(i).setCreationDate(lstTicketDetails.get(i).getCreationDate());
						lstTicketDetails.get(i)
								.setCreatedDateText(sdf.format(lstTicketDetails.get(i).getCreationDate()));

						// System.out.println( " Mohanraj ::: " +
						// sdf.format(lstTicketDetail.get(i).getCreationDate()));
					}

					if (lstTicketDetails.get(i).getScheduledOn() != null) {
						lstTicketDetails.get(i).setScheduledOn(lstTicketDetails.get(i).getScheduledOn());
						lstTicketDetails.get(i)
								.setScheduledDateText(sdfdate.format(lstTicketDetails.get(i).getScheduledOn()));

					}

				}

				objResponseEntity.put("ticketgriddata", lstTicketDetails);
				objResponseEntity.put("ticketcreation", new TicketDetail());
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

}
