
/******************************************************
 * 
 *    	Filename	: EventsKpiController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Event Kpi functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.bean.OpenStateTicketsCount;
import com.mestech.eampm.bean.TicketFilter;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EventDetail;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.CurrencyService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.EventDetailService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteTypeService;
import com.mestech.eampm.service.StateService;
import com.mestech.eampm.service.TicketDetailService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class EventsKpiController {

	@Autowired
	private TicketDetailService ticketdetailService;

	@Autowired
	private EventDetailService eventdetailService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private CustomerService customerService;
	@Autowired
	private SiteTypeService sitetypeService;
	@Autowired
	private CountryService countryService;
	@Autowired
	private StateService stateService;
	@Autowired
	private CurrencyService currencyService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns value for dropdown list.
	 * 
	 * Return Value : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : EventByErrorMessage
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns Events based on error messages.
	 * 
	 * Input Params : session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/EventByErrorMessage", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> EventByErrorMessage(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			} else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<EventDetail> lstEventDetails = eventdetailService.getEventDetailListByUserIdLimit(id,
						timezonevalue);

				Map<String, List<OpenStateTicketsCount>> result = new LinkedHashMap<String, List<OpenStateTicketsCount>>();
				List<OpenStateTicketsCount> lstTicketDetail = eventdetailService.EventsCountByErrorMessage(id,
						timezonevalue);
				List<OpenStateTicketsCount> lstChart2 = eventdetailService.getEventDetailBasedOnDays(id, timezonevalue);

				Map<String, Integer> series = new HashMap<>();
				Map<String, Object> sitenames = new HashMap<String, Object>();
				List<String> Categories = new ArrayList<String>();

				List<Integer> Data = new ArrayList<Integer>();

				for (Object obj : lstChart2) {

					Object[] str = (Object[]) obj;
					OpenStateTicketsCount o = new OpenStateTicketsCount();

					o.setTicketcount(str[0].toString());
					o.setSitename(str[1].toString());
					o.setSiteref(str[2].toString());
					sitenames.put(o.getSiteref(), o.getSitename());
					series.put(o.getSiteref(), Integer.valueOf(o.getTicketcount()));
				}

				List<Map.Entry<String, Integer>> entries = new LinkedList<>(series.entrySet());

				Collections.sort(entries, new Comparator<Map.Entry<String, Integer>>() {

					@Override
					public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {

						return o2.getValue().compareTo(o1.getValue());
					}
				});
				series = new LinkedHashMap<String, Integer>();
				for (Map.Entry<String, Integer> entry : entries) {
					series.put(entry.getKey(), entry.getValue());
				}

				Set<String> keys = series.keySet();

				for (String key : keys) {
					Categories.add(key);
					Data.add(series.get(key));

				}

				String finaldata = "";
				String chartdata = "";
				Integer iCnt = 0;
				if (lstTicketDetail.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					List<String> lstcolours = new ArrayList<String>();
					lstcolours.add("#FF0000");
					lstcolours.add("#FFA500");
					lstcolours.add("#FFFF00");
					lstcolours.add("#ADD8E6");
					lstcolours.add("#0000FF");

					for (Object obj : lstTicketDetail) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						// Color= str[2].toString();
						o.setTicketcount(str[1].toString());
						// o.setColor(str[2].toString());

						if (iCnt == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							// chartdata = "{name:'"+State+"',y:"+Count+",color:'"+Color+"'}";
							chartdata = "{name:'" + State + "',y:" + Count + ",color:'" + lstcolours.get(iCnt) + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							// chartdata = chartdata +",{name:'"+State+"',y:"+Count+",color:'"+Color+"'}";
							chartdata = chartdata + ",{name:'" + State + "',y:" + Count + ",color:'"
									+ lstcolours.get(iCnt) + "'}";
						}

						iCnt = iCnt + 1;
						System.out.println(chartdata);
					}

				}

				objResponseEntity.put("kpiData", chartdata);
				objResponseEntity.put("Categories", Categories);
				objResponseEntity.put("Data", Data);
				objResponseEntity.put("Series", sitenames);
				objResponseEntity.put("eventsgriddata", lstEventDetails);
				objResponseEntity.put("siteList", getNewTicketSiteList("Site", id));
				objResponseEntity.put("equipmentList", getDropdownList("Equipment", id));
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : EventByErrorMessageBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns events by error message and site.
	 * 
	 * Input Params : SiteId,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/EventByErrorMessageBySite", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> EventByErrorMessageBySite(Integer SiteId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			} else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				if (SiteId == 0) {
					List<EventDetail> lstEventDetails = eventdetailService.getEventDetailListByUserIdLimit(id,
							timezonevalue);

					Map<String, List<OpenStateTicketsCount>> result = new LinkedHashMap<String, List<OpenStateTicketsCount>>();
					List<OpenStateTicketsCount> lstTicketDetail = eventdetailService.EventsCountByErrorMessage(id,
							timezonevalue);
					List<OpenStateTicketsCount> lstChart2 = eventdetailService.getEventDetailBasedOnDays(id,
							timezonevalue);

					Map<String, Object> series = new HashMap<String, Object>();
					Map<String, Object> sitenames = new HashMap<String, Object>();
					List<String> Categories = new ArrayList<String>();

					List<Integer> Data = new ArrayList<Integer>();

					for (Object obj : lstChart2) {

						Object[] str = (Object[]) obj;
						OpenStateTicketsCount o = new OpenStateTicketsCount();

						o.setTicketcount(str[0].toString());
						o.setSitename(str[1].toString());
						o.setSiteref(str[2].toString());
						sitenames.put(o.getSiteref(), o.getSitename());
						series.put(o.getSiteref(), o.getTicketcount());
					}

					Set<String> keys = series.keySet();
					for (String key : keys) {
						Categories.add(key);
						Data.add(Integer.valueOf((String) series.get(key)));

					}

					String finaldata = "";
					String chartdata = "";
					Integer iCnt = 0;
					if (lstTicketDetail.size() > 0) {

						String State = "";
						String Count = "";
						String Color = "";

						OpenStateTicketsCount o = new OpenStateTicketsCount();
						List<String> lstcolours = new ArrayList<String>();
						lstcolours.add("#FF0000");
						lstcolours.add("#FFA500");
						lstcolours.add("#FFFF00");
						lstcolours.add("#ADD8E6");
						lstcolours.add("#0000FF");

						for (Object obj : lstTicketDetail) {
							Object[] str = (Object[]) obj;

							State = str[0].toString();
							Count = str[1].toString();
							// Color= str[2].toString();
							o.setTicketcount(str[1].toString());
							// o.setColor(str[2].toString());

							if (iCnt == 0) {
								if (Count.equals("0")) {
									Count = "null";
								}
								// chartdata = "{name:'"+State+"',y:"+Count+",color:'"+Color+"'}";
								chartdata = "{name:'" + State + "',y:" + Count + ",color:'" + lstcolours.get(iCnt)
										+ "'}";
							} else {
								if (Count.equals("0")) {
									Count = "null";
								}
								// chartdata = chartdata +",{name:'"+State+"',y:"+Count+",color:'"+Color+"'}";
								chartdata = chartdata + ",{name:'" + State + "',y:" + Count + ",color:'"
										+ lstcolours.get(iCnt) + "'}";
							}

							iCnt = iCnt + 1;
							System.out.println(chartdata);
						}

					}

					objResponseEntity.put("eventsgriddata", lstEventDetails);
					objResponseEntity.put("kpiData", chartdata);
					objResponseEntity.put("Categories", Categories);
					objResponseEntity.put("Data", Data);
					objResponseEntity.put("Series", sitenames);
					objResponseEntity.put("siteList", getNewTicketSiteList("SingleSite", SiteId));
					objResponseEntity.put("equipmentList", getDropdownList("Equipment", id));
					objResponseEntity.put("status", true);
					objResponseEntity.put("data", "Success");
					return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
				}

				else {

					Site site = siteService.getSiteById(SiteId);
					List<EventDetail> lstEventDetails = eventdetailService.getEventDetailListBySiteIdLimit(SiteId,
							timezonevalue);

					Map<String, List<OpenStateTicketsCount>> result = new LinkedHashMap<String, List<OpenStateTicketsCount>>();
					List<OpenStateTicketsCount> lstTicketDetail = eventdetailService
							.EventsCountByErrorMessageBySite(SiteId, timezonevalue);
					List<OpenStateTicketsCount> lstChart2 = eventdetailService.getEventDetailBasedOnDaysBySite(SiteId,
							timezonevalue);

					Map<String, Object> series = new HashMap<String, Object>();
					Map<String, Object> sitenames = new HashMap<String, Object>();
					List<String> Categories = new ArrayList<String>();

					List<Integer> Data = new ArrayList<Integer>();

					for (Object obj : lstChart2) {

						Object[] str = (Object[]) obj;
						OpenStateTicketsCount o = new OpenStateTicketsCount();

						o.setTicketcount(str[0].toString());
						o.setSitename(str[1].toString());
						o.setSiteref(str[2].toString());
						sitenames.put(o.getSiteref(), o.getSitename());
						series.put(o.getSiteref(), o.getTicketcount());
					}

					Set<String> keys = series.keySet();
					for (String key : keys) {
						Categories.add(key);
						Data.add(Integer.valueOf((String) series.get(key)));

					}

					String finaldata = "";
					String chartdata = "";
					Integer iCnt = 0;
					if (lstTicketDetail.size() > 0) {

						String State = "";
						String Count = "";
						String Color = "";

						OpenStateTicketsCount o = new OpenStateTicketsCount();
						List<String> lstcolours = new ArrayList<String>();
						lstcolours.add("#FF0000");
						lstcolours.add("#FFA500");
						lstcolours.add("#FFFF00");
						lstcolours.add("#ADD8E6");
						lstcolours.add("#0000FF");

						for (Object obj : lstTicketDetail) {
							Object[] str = (Object[]) obj;

							State = str[0].toString();
							Count = str[1].toString();
							// Color= str[2].toString();
							o.setTicketcount(str[1].toString());
							// o.setColor(str[2].toString());

							if (iCnt == 0) {
								if (Count.equals("0")) {
									Count = "null";
								}
								// chartdata = "{name:'"+State+"',y:"+Count+",color:'"+Color+"'}";
								chartdata = "{name:'" + State + "',y:" + Count + ",color:'" + lstcolours.get(iCnt)
										+ "'}";
							} else {
								if (Count.equals("0")) {
									Count = "null";
								}
								// chartdata = chartdata +",{name:'"+State+"',y:"+Count+",color:'"+Color+"'}";
								chartdata = chartdata + ",{name:'" + State + "',y:" + Count + ",color:'"
										+ lstcolours.get(iCnt) + "'}";
							}

							iCnt = iCnt + 1;
							System.out.println(chartdata);
						}

					}

					objResponseEntity.put("eventsgriddata", lstEventDetails);
					objResponseEntity.put("kpiData", chartdata);
					objResponseEntity.put("Categories", Categories);
					objResponseEntity.put("Data", Data);
					objResponseEntity.put("sitename", site.getSiteName());

					objResponseEntity.put("Series", sitenames);
					objResponseEntity.put("siteList", getNewTicketSiteList("SingleSite", SiteId));
					objResponseEntity.put("equipmentList", getDropdownList("Equipment", id));
					objResponseEntity.put("status", true);
					objResponseEntity.put("data", "Success");
					return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : EventByErrorMessageByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This method returns events by customer.
	 * 
	 * Input Params : CustomerId,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/EventByErrorMessageByCustomer", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> EventByErrorMessageByCustomer(Integer CustomerId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			} else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				Customer customer = customerService.getCustomerById(CustomerId);
				List<EventDetail> lstEventDetails = eventdetailService.getEventDetailListByCustomerIdLimit(CustomerId,
						timezonevalue);

				Map<String, List<OpenStateTicketsCount>> result = new LinkedHashMap<String, List<OpenStateTicketsCount>>();
				List<OpenStateTicketsCount> lstTicketDetail = eventdetailService
						.EventsCountByErrorMessageByCustomerId(CustomerId, timezonevalue);
				List<OpenStateTicketsCount> lstChart2 = eventdetailService.getEventDetailBasedOnDaysByCustomerId(CustomerId,
						timezonevalue);

				Map<String, Object> series = new HashMap<String, Object>();
				Map<String, Object> sitenames = new HashMap<String, Object>();
				List<String> Categories = new ArrayList<String>();

				List<Integer> Data = new ArrayList<Integer>();

				for (Object obj : lstChart2) {

					Object[] str = (Object[]) obj;
					OpenStateTicketsCount o = new OpenStateTicketsCount();

					o.setTicketcount(str[0].toString());
					o.setSitename(str[1].toString());
					o.setSiteref(str[2].toString());
					sitenames.put(o.getSiteref(), o.getSitename());
					series.put(o.getSiteref(), o.getTicketcount());
				}

				Set<String> keys = series.keySet();
				for (String key : keys) {
					Categories.add(key);
					Data.add(Integer.valueOf((String) series.get(key)));

				}

				String finaldata = "";
				String chartdata = "";
				Integer iCnt = 0;
				if (lstTicketDetail.size() > 0) {

					String State = "";
					String Count = "";
					String Color = "";

					OpenStateTicketsCount o = new OpenStateTicketsCount();
					List<String> lstcolours = new ArrayList<String>();
					lstcolours.add("#FF0000");
					lstcolours.add("#FFA500");
					lstcolours.add("#FFFF00");
					lstcolours.add("#ADD8E6");
					lstcolours.add("#0000FF");

					for (Object obj : lstTicketDetail) {
						Object[] str = (Object[]) obj;

						State = str[0].toString();
						Count = str[1].toString();
						// Color= str[2].toString();
						o.setTicketcount(str[1].toString());
						// o.setColor(str[2].toString());

						if (iCnt == 0) {
							if (Count.equals("0")) {
								Count = "null";
							}
							// chartdata = "{name:'"+State+"',y:"+Count+",color:'"+Color+"'}";
							chartdata = "{name:'" + State + "',y:" + Count + ",color:'" + lstcolours.get(iCnt) + "'}";
						} else {
							if (Count.equals("0")) {
								Count = "null";
							}
							// chartdata = chartdata +",{name:'"+State+"',y:"+Count+",color:'"+Color+"'}";
							chartdata = chartdata + ",{name:'" + State + "',y:" + Count + ",color:'"
									+ lstcolours.get(iCnt) + "'}";
						}

						iCnt = iCnt + 1;
						System.out.println(chartdata);
					}

				}

				objResponseEntity.put("eventsgriddata", lstEventDetails);
				objResponseEntity.put("kpiData", chartdata);
				objResponseEntity.put("Categories", Categories);
				objResponseEntity.put("Data", Data);
				objResponseEntity.put("customername", customer.getCustomerName());

				objResponseEntity.put("Series", sitenames);
				objResponseEntity.put("siteList", getNewTicketSiteList("Site", id));
				objResponseEntity.put("equipmentList", getDropdownList("Equipment", id));
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : ClickEventsBySiteId
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns event based on site.
	 * 
	 * Input Params : SiteName,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/EventsBySiteId", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> ClickEventsBySiteId(String SiteName, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<EventDetail> lstEventDetails = eventdetailService.getEventDetailBasedOnSite(SiteName,
						timezonevalue);

				objResponseEntity.put("ticketgriddata", lstEventDetails);

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : ClickEventsByErrormessage
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns events by error message.
	 * 
	 * Input Params : ErrorMessage,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/ClickEventsByErrormessage", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> ClickEventsByErrormessage(String ErrorMessage, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<EventDetail> lstEventDetails = eventdetailService.ClickByErrorMessage(ErrorMessage, id,
						timezonevalue);

				objResponseEntity.put("ticketgriddata", lstEventDetails);

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : ClickEventsByErrormessageBySite
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns events by error message and site.
	 * 
	 * Input Params : ErrorMessage,SiteId,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/ClickEventsByErrormessageBySite", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> ClickEventsByErrormessageBySite(String ErrorMessage, Integer SiteId,
			HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				if (SiteId == 0) {

					List<EventDetail> lstEventDetails = eventdetailService.ClickByErrorMessage(ErrorMessage, id,
							timezonevalue);

					objResponseEntity.put("ticketgriddata", lstEventDetails);

					objResponseEntity.put("status", true);
					objResponseEntity.put("data", "Success");
					return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
				} else {
					List<EventDetail> lstEventDetails = eventdetailService.ClickByErrorMessageBySite(ErrorMessage,
							SiteId, timezonevalue);
					objResponseEntity.put("ticketgriddata", lstEventDetails);

					objResponseEntity.put("status", true);
					objResponseEntity.put("data", "Success");
					return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : ClickEventsByErrormessageByCustomerPage
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns events by error message and customer.
	 * 
	 * Input Params : ErrorMessage,CustomerId,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/ClickEventsByErrormessageByCustomerpage", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> ClickEventsByErrormessageByCustomerPage(String ErrorMessage,
			Integer CustomerId, HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				List<EventDetail> lstEventDetails = eventdetailService.ClickByErrorMessageByCustomer(ErrorMessage, id,
						timezonevalue);
				objResponseEntity.put("ticketgriddata", lstEventDetails);

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : ClickEventsByErrormessageByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns events by error message,customer and
	 * site.
	 * 
	 * Input Params : ErrorMessage,SiteId,CustoemrId,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/ClickEventsByErrormessageByCustomer", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> ClickEventsByErrormessageByCustomer(String ErrorMessage, Integer SiteId,
			Integer CustoemrId, HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				if (SiteId == 0) {
					List<EventDetail> lstEventDetails = eventdetailService.ClickByErrorMessageByCustomer(ErrorMessage,
							id, timezonevalue);
					objResponseEntity.put("ticketgriddata", lstEventDetails);

					objResponseEntity.put("status", true);
					objResponseEntity.put("data", "Success");
					return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
				} else {
					List<EventDetail> lstEventDetails = eventdetailService.ClickByErrorMessageBySite(ErrorMessage,
							SiteId, timezonevalue);
					objResponseEntity.put("ticketgriddata", lstEventDetails);

					objResponseEntity.put("status", true);
					objResponseEntity.put("data", "Success");
					return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : EventsPageLoad
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads events kpi page.
	 * 
	 * Input Params : model,session,authentication
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/eventskpi", method = RequestMethod.GET)
	public String EventsPageLoad(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());
			objAccessListBean.setMySiteFilter(false);
			objAccessListBean.setMySiteID(0);

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());

		model.addAttribute("eventdetail", new EventDetail());
		model.addAttribute("access", objAccessListBean);

		model.addAttribute("siteList", getNewTicketSiteList("Site", id));

		model.addAttribute("ticketcreation", new TicketDetail());

		return "eventskpi";
	}

	/********************************************************************************************
	 * 
	 * Function Name : SiteEventsPageLoad
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads events kpi page by site.
	 * 
	 * Input Params : siteid,model,session,authentication
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/siteeventskpi{id}", method = RequestMethod.GET)
	public String SiteEventsPageLoad(@PathVariable("id") int siteid, Model model, HttpSession session,
			Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());
			objAccessListBean.setMySiteFilter(true);
			objAccessListBean.setMySiteID(siteid);

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("eventdetail", new EventDetail());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));
		model.addAttribute("ticketcreation", new TicketDetail());

		return "eventskpi";
	}

	/********************************************************************************************
	 * 
	 * Function Name : CustomerEventsPageLoad
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads events kpi page based on customer.
	 * 
	 * Input Params : customerid,model,session,authentication
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customereventskpi{id}", method = RequestMethod.GET)
	public String CustomerEventsPageLoad(@PathVariable("id") int customerid, Model model, HttpSession session,
			Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());
			objAccessListBean.setMyCustomerFilter(true);
			objAccessListBean.setMyCustomerID(customerid);

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("ticketfilter", new TicketFilter());
		model.addAttribute("eventdetail", new EventDetail());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));
		model.addAttribute("ticketcreation", new TicketDetail());

		return "eventskpi";
	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns value for dropdown.
	 * 
	 * Input Params : DropdownName,refid
	 * 
	 * Return Value : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns value for dropdown.
	 * 
	 * Input Params : DropdownName,refid
	 * 
	 * Return Value : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		}

		if (DropdownName == "CustomerSites") {
			List<Site> ddlList = siteService.getSiteListByCustomerId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "SingleSite") {
			List<Site> ddlList = siteService.listSiteById(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "User") {
			List<User> ddlList = userService.listFieldUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		} else if (DropdownName == "Equipment") {
			// List<Equipment> ddlList = equipmentService.listEquipmentsByUserId(refid);
			List<Equipment> ddlList = equipmentService.listFilterInvertersByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getEquipmentId().toString(), ddlList.get(i).getCustomerNaming());
			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : CustomerclickAlleventsByCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns all events by customer.
	 * 
	 * Input Params : Siteid,Customerid,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/CustomerclickAlleventsByCustomer", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> CustomerclickAlleventsByCustomer(Integer Siteid, Integer Customerid,
			HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			if (authentication == null || session == null || session.getAttribute("timezonevalue") == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}
				loginUserDetails.setTimezoneoffsetmin(timezonevalue);

				if (Siteid == 0) {

					List<EventDetail> lstEventDetails = eventdetailService
							.getEventDetailListByCustomerIdLimit(Customerid, timezonevalue);

					Map<String, List<OpenStateTicketsCount>> result = new LinkedHashMap<String, List<OpenStateTicketsCount>>();
					List<OpenStateTicketsCount> lstTicketDetail = eventdetailService
							.EventsCountByErrorMessageByCustomerId(Customerid, timezonevalue);
					List<OpenStateTicketsCount> lstChart2 = eventdetailService
							.getEventDetailBasedOnDaysByCustomerId(Customerid, timezonevalue);

					Map<String, Object> series = new HashMap<String, Object>();
					Map<String, Object> sitenames = new HashMap<String, Object>();
					List<String> Categories = new ArrayList<String>();

					List<Integer> Data = new ArrayList<Integer>();

					for (Object obj : lstChart2) {

						Object[] str = (Object[]) obj;
						OpenStateTicketsCount o = new OpenStateTicketsCount();

						o.setTicketcount(str[0].toString());
						o.setSitename(str[1].toString());
						o.setSiteref(str[2].toString());
						sitenames.put(o.getSiteref(), o.getSitename());
						series.put(o.getSiteref(), o.getTicketcount());
					}

					Set<String> keys = series.keySet();
					for (String key : keys) {
						Categories.add(key);
						Data.add(Integer.valueOf((String) series.get(key)));

					}

					String finaldata = "";
					String chartdata = "";
					Integer iCnt = 0;
					if (lstTicketDetail.size() > 0) {

						String State = "";
						String Count = "";
						String Color = "";

						OpenStateTicketsCount o = new OpenStateTicketsCount();
						List<String> lstcolours = new ArrayList<String>();
						lstcolours.add("#ff3399");
						lstcolours.add("#64b5b8");
						lstcolours.add("#862d86");
						lstcolours.add("#B76262");
						lstcolours.add("#660033");

						for (Object obj : lstTicketDetail) {
							Object[] str = (Object[]) obj;

							State = str[0].toString();
							Count = str[1].toString();
							// Color= str[2].toString();
							o.setTicketcount(str[1].toString());
							// o.setColor(str[2].toString());

							if (iCnt == 0) {
								if (Count.equals("0")) {
									Count = "null";
								}
								// chartdata = "{name:'"+State+"',y:"+Count+",color:'"+Color+"'}";
								chartdata = "{name:'" + State + "',y:" + Count + ",color:'" + lstcolours.get(iCnt)
										+ "'}";
							} else {
								if (Count.equals("0")) {
									Count = "null";
								}
								// chartdata = chartdata +",{name:'"+State+"',y:"+Count+",color:'"+Color+"'}";
								chartdata = chartdata + ",{name:'" + State + "',y:" + Count + ",color:'"
										+ lstcolours.get(iCnt) + "'}";
							}

							iCnt = iCnt + 1;
							System.out.println(chartdata);
						}

					}

					objResponseEntity.put("eventsgriddata", lstEventDetails);
					objResponseEntity.put("kpiData", chartdata);
					objResponseEntity.put("Categories", Categories);
					objResponseEntity.put("Data", Data);
					objResponseEntity.put("Series", sitenames);

					objResponseEntity.put("status", true);
					objResponseEntity.put("data", "Success");
					return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

				}

				else {
					Site site = siteService.getSiteById(Siteid);
					List<EventDetail> lstEventDetails = eventdetailService.getEventDetailListBySiteIdLimit(Siteid,
							timezonevalue);

					Map<String, List<OpenStateTicketsCount>> result = new LinkedHashMap<String, List<OpenStateTicketsCount>>();
					List<OpenStateTicketsCount> lstTicketDetail = eventdetailService
							.EventsCountByErrorMessageBySite(Siteid, timezonevalue);
					List<OpenStateTicketsCount> lstChart2 = eventdetailService.getEventDetailBasedOnDaysBySite(Siteid,
							timezonevalue);

					Map<String, Object> series = new HashMap<String, Object>();
					Map<String, Object> sitenames = new HashMap<String, Object>();
					List<String> Categories = new ArrayList<String>();

					List<Integer> Data = new ArrayList<Integer>();

					for (Object obj : lstChart2) {

						Object[] str = (Object[]) obj;
						OpenStateTicketsCount o = new OpenStateTicketsCount();

						o.setTicketcount(str[0].toString());
						o.setSitename(str[1].toString());
						o.setSiteref(str[2].toString());
						sitenames.put(o.getSiteref(), o.getSitename());
						series.put(o.getSiteref(), o.getTicketcount());
					}

					Set<String> keys = series.keySet();
					for (String key : keys) {
						Categories.add(key);
						Data.add(Integer.valueOf((String) series.get(key)));

					}

					String finaldata = "";
					String chartdata = "";
					Integer iCnt = 0;
					if (lstTicketDetail.size() > 0) {

						String State = "";
						String Count = "";
						String Color = "";

						OpenStateTicketsCount o = new OpenStateTicketsCount();
						List<String> lstcolours = new ArrayList<String>();
						lstcolours.add("#ff3399");
						lstcolours.add("#64b5b8");
						lstcolours.add("#862d86");
						lstcolours.add("#B76262");
						lstcolours.add("#660033");

						for (Object obj : lstTicketDetail) {
							Object[] str = (Object[]) obj;

							State = str[0].toString();
							Count = str[1].toString();
							// Color= str[2].toString();
							o.setTicketcount(str[1].toString());
							// o.setColor(str[2].toString());

							if (iCnt == 0) {
								if (Count.equals("0")) {
									Count = "null";
								}
								// chartdata = "{name:'"+State+"',y:"+Count+",color:'"+Color+"'}";
								chartdata = "{name:'" + State + "',y:" + Count + ",color:'" + lstcolours.get(iCnt)
										+ "'}";
							} else {
								if (Count.equals("0")) {
									Count = "null";
								}
								// chartdata = chartdata +",{name:'"+State+"',y:"+Count+",color:'"+Color+"'}";
								chartdata = chartdata + ",{name:'" + State + "',y:" + Count + ",color:'"
										+ lstcolours.get(iCnt) + "'}";
							}

							iCnt = iCnt + 1;
							System.out.println(chartdata);
						}

					}

					objResponseEntity.put("eventsgriddata", lstEventDetails);
					objResponseEntity.put("kpiData", chartdata);
					objResponseEntity.put("Categories", Categories);
					objResponseEntity.put("Data", Data);
					objResponseEntity.put("sitename", site.getSiteName());

					objResponseEntity.put("Series", sitenames);

					objResponseEntity.put("status", true);
					objResponseEntity.put("data", "Success");
					return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

}
