
/******************************************************
 * 
 *    	Filename	: MDataloggerTypeController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with DataloggerType Activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.MDatalogger;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.MDataloggerTypeService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.Response;

@Controller
public class MDataloggerTypeController {
	@Autowired
	private MDataloggerTypeService dataloggerTypeService;
	@Autowired
	private UserService userService;
	@Autowired
	private ActivityService activityService;

	@Autowired
	private SiteService siteService;
	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : getAlldataloggertype
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns all dataloggertypes.
	 * 
	 * Return Value  : Response
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getAlldataloggertype", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Response getAlldataloggertype() {
		Response response = new Response();
		response.setData(dataloggerTypeService.listAll());
		response.setStatus("Success");
		return response;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getAlldatalogger
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns datalogger with condition.
	 * 
	 * Input Params  : flag
	 * 
	 * Return Value  : Response
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getAlldatalogger", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Response getAlldatalogger(@RequestParam("flag") String flag) {
		// flag=1 => get items with active -> 1; flag=0 => get all items;
		Response response = new Response();
		response.setData(dataloggerTypeService.listAllDatalogger(Integer.parseInt(flag)));
		response.setStatus("Success");
		return response;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getAlldataloggerFromStandards
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns all datalogger from standard.
	 * 
	 * Return Value  : Response
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getAlldataloggerFromStandards", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Response getAlldataloggerFromStandards() {
		// flag=1 => get items with active -> 1; flag=0 => get all items;
		Response response = new Response();
		List<MDatalogger> dataloggers = dataloggerTypeService.listAllDatalogger(1);
		List<BigInteger> obj = dataloggerTypeService.listDataloggerIdFromStandards();
		List<MDatalogger> newDataLoggers = new ArrayList<>();
		for (MDatalogger md : dataloggers) {
			if (!obj.contains(md.getDataloggerid())) {
				newDataLoggers.add(md);
			}
		}
		response.setData(newDataLoggers);

		/*
		 * response.setData(dataloggerTypeService.listAllDatalogger(Integer.parseInt(
		 * flag)));
		 */
		response.setStatus("Success");
		return response;
	}

	/********************************************************************************************
	 * 
	 * Function Name : list_datalogger_master
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads datalogger master.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/dataloggermaster", method = RequestMethod.GET)
	public String list_datalogger_master(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4) {
				objAccessListBean.setCustomerListView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		model.addAttribute("datalogger", new MDatalogger());
		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));

		return "dataloggermaster";
	}

	/********************************************************************************************
	 * 
	 * Function Name : masterSheet
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function adds or update a datalogger.
	 * 
	 * Input Params  : params,authentication
	 * 
	 * Return Value  : Response
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/recdatalogger_master", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Response masterSheet(@RequestBody Map<String, String> params, Authentication authentication)
			throws ParseException {
		Response response = new Response();
		String desc = params.get("desc");
		String remarks = params.get("remark");
		String active = params.get("active");
		String dbid = params.get("id");
		if (desc != null && !desc.isEmpty() && remarks != null && !remarks.isEmpty() && active != null
				&& !active.isEmpty()) {
			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			MDatalogger dlObject = new MDatalogger();
			dlObject.setDescription(desc);
			dlObject.setRemarks(remarks);
			dlObject.setActiveflag(Integer.parseInt(active));
			/*
			 * DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); Date
			 * date = dateFormat.parse((dateFormat.format(new Date())));
			 */
			dlObject.setCreationdate(new Date());
			dlObject.setCreatedby(id);
			if (dbid != null && !dbid.isEmpty()) {
				dlObject.setDataloggerid(new BigInteger(dbid));
			}
			dataloggerTypeService.maddDatalogger(dlObject);
			response.setStatus("Success");
			response.setData("Error");
		} else {
			response.setStatus("Failed");
			response.setData("Error");
		}

		return response;

	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

}
