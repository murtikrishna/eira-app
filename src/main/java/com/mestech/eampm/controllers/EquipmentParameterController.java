
/******************************************************
 * 
 *    	Filename	: EquipmentParameterController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals Equipment's parameter.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.model.EquipmentCategory;
import com.mestech.eampm.model.EquipmentParameter;
import com.mestech.eampm.model.EquipmentType;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.EquipmentParameterService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class EquipmentParameterController {

	@Autowired
	private EquipmentParameterService equipmentparameterservice;
	@Autowired
	private UserService userService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns values for dropdown list.
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : listUsers
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This method loads the equipment parameters page.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/equipmentparameters", method = RequestMethod.GET)
	public String listUsers(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();

		try {
			User objUser = userService.getUserById(id);
			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
			List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getUserName());

			if (objUser.getRoleID() != 4) {
				objAccessListBean.setCustomerListView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActivityID() == 1 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setDashboard("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 2 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setOverview("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 3 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setSystemMonitoring("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 4 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setVisualization("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 5 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalytics("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 6 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setPortfolioManagement("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 7 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setTicketing("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 8 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setForcasting("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 9 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setConfiguration("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 10
						&& lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalysis("visible");
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("access", objAccessListBean);

		model.addAttribute("equipmentparameter", new EquipmentParameter());
		model.addAttribute("equipmentparameterList", equipmentparameterservice.listEquipmentParameters());
		model.addAttribute("activeStatusList", getStatusDropdownList());
		model.addAttribute("ticketcreation", new TicketDetail());

		model.addAttribute("siteList", getDropdownList("Site"));
		return "equipmentparameter";
	}

	/********************************************************************************************
	 * 
	 * Function Name : adduser
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function adds or updates equipment parameter.
	 * 
	 * Input Params  : equipmentparameter
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// Same method For both Add and Update UnitOfMeasurement
	// @RequestMapping(value = "/unitofmeasurement/add", method =
	// RequestMethod.POST)
	@RequestMapping(value = "/addnewequipmentparameter", method = RequestMethod.POST)
	public String adduser(@ModelAttribute("equipmentparameter") EquipmentParameter equipmentparameter) {

		Date date = new Date();

		if (equipmentparameter.getEquipmentTypeID() == null || equipmentparameter.getEquipmentTypeID() == 0) {

			equipmentparameter.setCreationDate(date);
			equipmentparameterservice.addEquipmentParameter(equipmentparameter);
		} else {

			equipmentparameter.setLastUpdatedDate(date);
			equipmentparameterservice.updateEquipmentParameter(equipmentparameter);
		}

		return "redirect:/equipmentparameters";

	}

	/********************************************************************************************
	 * 
	 * Function Name : removeuser
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function removes equipment parameter.
	 * 
	 * Input Params  : id
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// @RequestMapping("/unitofmeasurement/remove/{id}")
	@RequestMapping("/removeequipmentparameter{id}")
	public String removeuser(@PathVariable("id") int id) {

		equipmentparameterservice.removeEquipmentParameter(id);
		return "redirect:/equipmentparameters";
	}

	/********************************************************************************************
	 * 
	 * Function Name : edituser
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This method updates an equipmenet parameter.
	 * 
	 * Input Params  : id,model
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// @RequestMapping("/unitofmeasurement/edit/{id}")
	@RequestMapping("/editequipmentparameter{id}")
	public String edituser(@PathVariable("id") int id, Model model) {
		model.addAttribute("equipmentparameter", equipmentparameterservice.getEquipmentParameterById(id));
		model.addAttribute("equipmentparameterList", equipmentparameterservice.listEquipmentParameters());
		model.addAttribute("activeStatusList", getStatusDropdownList());
		return "equipmentparameter";
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Input Params  : DropdownName
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.listSites();

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				/*
				 * if(ddlList.get(i).getCustomerReference() != null) { SiteName = SiteName +
				 * " - " + ddlList.get(i).getCustomerReference(); }
				 */

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}

		return ddlMap;
	}

}
