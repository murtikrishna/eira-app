
/******************************************************
 * 
 *    	Filename	: CustomerTypeController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Controller deals with customer map related activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.model.CustomerType;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.CustomerTypeService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class CustomerTypeController {

	@Autowired
	private CustomerTypeService customertypeService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : listCustomertypes
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads customer type page.
	 * 
	 * Input Params  : model,session
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customertypes", method = RequestMethod.GET)
	public String listCustomertypes(Model model, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());

		AccessListBean objAccessListBean = new AccessListBean();

		try {
			User objUser = userService.getUserById(id);
			// UserRole objUserRole =
			// userRoleService.getUserRoleById(objUser.getRoleID());
			List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getUserName());

			if (objUser.getRoleID() != 4) {
				objAccessListBean.setCustomerListView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActivityID() == 1 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setDashboard("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 2 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setOverview("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 3 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setSystemMonitoring("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 4 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setVisualization("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 5 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalytics("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 6 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setPortfolioManagement("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 7 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setTicketing("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 8 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setForcasting("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 9 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setConfiguration("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 10
						&& lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalysis("visible");
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("access", objAccessListBean);

		model.addAttribute("customertype", new CustomerType());
		model.addAttribute("customertypeList", customertypeService.listCustomerTypes());
		model.addAttribute("activeStatusList", getStatusDropdownList());

		return "customertype";
	}

	/********************************************************************************************
	 * 
	 * Function Name : addcustomertype
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function adds or updates a customer Type.
	 * 
	 * Input Params  : customertype,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// Same method For both Add and Update CustomerType
	// @RequestMapping(value = "/customertype/add", method = RequestMethod.POST)
	@RequestMapping(value = "/addnewcustomertype", method = RequestMethod.POST)
	public String addcustomertype(@ModelAttribute("customertype") CustomerType customertype, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		Date date = new Date();

		if (customertype.getCustomerTypeId() == null || customertype.getCustomerTypeId() == 0) {
			// new customertype, add it
			customertype.setCreationDate(date);
			customertypeService.addCustomerType(customertype);
		} else {
			// existing customertype, call update
			customertype.setLastUpdatedDate(date);
			customertypeService.updateCustomerType(customertype);
		}

		return "redirect:/customertypes";

	}

	/********************************************************************************************
	 * 
	 * Function Name : removecustomertype
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function removes a customer type.
	 * 
	 * Input Params  : id,session
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// @RequestMapping("/customertype/remove/{id}")
	@RequestMapping("/removecustomertype{id}")
	public String removecustomertype(@PathVariable("id") int id, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		customertypeService.removeCustomerType(id);
		return "redirect:/customertypes";
	}

	/********************************************************************************************
	 * 
	 * Function Name : editcustomertype
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function updates a customer type.
	 * 
	 * Input Params  : id,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// @RequestMapping("/customertype/edit/{id}")
	@RequestMapping("/editcustomertype{id}")
	public String editcustomertype(@PathVariable("id") int id, Model model, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		model.addAttribute("customertype", customertypeService.getCustomerTypeById(id));
		model.addAttribute("customertypeList", customertypeService.listCustomerTypes());
		model.addAttribute("activeStatusList", getStatusDropdownList());

		return "customertype";
	}
}
