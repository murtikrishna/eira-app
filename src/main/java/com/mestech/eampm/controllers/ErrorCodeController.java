
/******************************************************
 * 
 *    	Filename	: ErrorCodeController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals ErrorCode functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.bean.SiteBean;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.CountryRegion;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentCategory;
import com.mestech.eampm.model.EquipmentType;
import com.mestech.eampm.model.ErrorCode;
import com.mestech.eampm.model.ParameterStandard;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteType;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.EquipmentCategoryService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.EquipmentTypeService;
import com.mestech.eampm.service.ErrorCodeService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.UtilityCommon;
import com.mestech.eampm.utility.UtilityCommonDAO;

@Controller
public class ErrorCodeController {

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private EquipmentTypeService equipmenttypeService;

	@Autowired
	private EquipmentCategoryService equipmentcategoryService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private SiteService siteService;
	@Autowired
	private ErrorCodeService errorCodeService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private EquipmentTypeService equipmentTypeService;

	@Autowired
	private RoleActivityService roleActivityService;

	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();
	private UtilityCommon utilityCommon = new UtilityCommon();

	/********************************************************************************************
	 * 
	 * Function Name : ErrorcodeGridLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns all error codes.
	 * 
	 * Input Params  : session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/errorcode", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> ErrorcodeGridLoad(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				objResponseEntity.put("siteList", getDropdownList("Site"));
				objResponseEntity.put("equipmenttypeList", getDropdownList("EquipmentType"));
				objResponseEntity.put("equipmentcategory", getDropdownList("EquipmentCategory"));
				objResponseEntity.put("activeStatusList", getStatusDropdownList());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Input Params  : DropdownName
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "EquipmentType") {
			List<EquipmentType> ddlList = equipmenttypeService.listEquipmentTypes();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getEquipmentTypeId().toString(), ddlList.get(i).getEquipmentType());

			}

		} else if (DropdownName == "EquipmentCategory") {
			List<EquipmentCategory> ddlList = equipmentcategoryService.listEquipmentCategories();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getCategoryId().toString(), ddlList.get(i).getEquipmentCategory());

			}

		} else if (DropdownName == "Site") {
			List<Site> ddlList = siteService.listSites();

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getErrorCode
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns error codes.
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	public String getErrorCode() {

		ErrorCode objCust = errorCodeService.getErrorCodeByMax("Code");

		String CR = "";

		if (objCust != null) {
			CR = objCust.getCode();
		}

		if (CR == null || CR == "") {
			CR = "EI0001";
		} else {
			try {
				int NextIndex = Integer.parseInt(CR.replaceAll("EI", "")) + 1;

				if (NextIndex >= 1000) {
					CR = String.valueOf(NextIndex);
				} else if (NextIndex >= 100) {
					CR = "0" + String.valueOf(NextIndex);
				} else if (NextIndex >= 10) {
					CR = "00" + String.valueOf(NextIndex);
				}

				else {
					CR = "000" + String.valueOf(NextIndex);
				}

				CR = "EI" + CR;
			} catch (Exception e) {
				CR = "EI0001";
			}
		}

		return CR;
	}

	/********************************************************************************************
	 * 
	 * Function Name : ErrorcodePageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads the error codes page.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/errorcodes", method = RequestMethod.GET)
	public String ErrorcodePageLoad(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("errorcode", new ErrorCode());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));
		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("activeStatusList", getStatusDropdownList());

		return "errorcode";
	}

	/********************************************************************************************
	 * 
	 * Function Name : NewErrorCodeSaveorUpdate
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function adds or updates and error code.
	 * 
	 * Input Params  : lsterrorcode,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : ConstraintViolationException,Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/newerrorcode", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> NewErrorCodeSaveorUpdate(@RequestBody List<ErrorCode> lsterrorcode,
			HttpSession session, Authentication authentication) throws ServletException {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String ResponseMessage = "";
		String Dataexists1 = "";
		String Dataexists2 = "";

		try {

			if (lsterrorcode != null) {
				ErrorCode errorcode = lsterrorcode.get(0);

				if (errorcode != null) {

					Date date = new Date();
					if (errorcode.getErrorId() == null || errorcode.getErrorId() == 0) {

						List<ErrorCode> lstErrorcodes = errorCodeService.listexitsErrorCodes(0, 0);
						for (int i = 0; i < lstErrorcodes.size(); i++) {
							int equipmenttypeid = lstErrorcodes.get(i).getEquipmentTypeId();
							String Errorcode = lstErrorcodes.get(i).getErrorCode();

							if (errorcode.getEquipmentTypeId().equals(equipmenttypeid)
									&& errorcode.getErrorCode().equals(Errorcode)) {

								Dataexists1 = "Errorcode has already mapped for this Equipment type";

								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}
						errorcode.setCode(getErrorCode());
						errorcode.setCreationDate(date);
						errorcode.setCreatedBy(id);
						errorCodeService.addErrorCode(errorcode);
					} else {
						ErrorCode objErrorCode = errorCodeService.getErrorCodeById(errorcode.getErrorId());

						List<ErrorCode> lstErrorcodes = errorCodeService
								.listexitsErrorCodes(errorcode.getEquipmentTypeId(), errorcode.getErrorId());
						for (int i = 0; i < lstErrorcodes.size(); i++) {
							int equipmenttypeid = lstErrorcodes.get(i).getEquipmentTypeId();
							String Errorcode = lstErrorcodes.get(i).getErrorCode();

							if (errorcode.getEquipmentTypeId().equals(equipmenttypeid)
									&& errorcode.getErrorCode().equals(Errorcode)) {

								Dataexists1 = "Errorcode has already mapped for this Equipment type";

								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}
						objErrorCode.setEquipmentTypeId(errorcode.getEquipmentTypeId());
						objErrorCode.setEquipmentCategory(errorcode.getEquipmentCategory());
						objErrorCode.setErrorCode(errorcode.getErrorCode());
						objErrorCode.setErrorMessage(errorcode.getErrorMessage());
						objErrorCode.setApprovedBy(errorcode.getApprovedBy());

						objErrorCode.setErrorDescription(errorcode.getErrorDescription());

						objErrorCode.setMessageType(errorcode.getMessageType());
						objErrorCode.setPriority(errorcode.getPriority());
						objErrorCode.setActiveFlag(errorcode.getActiveFlag());
						objErrorCode.setLastUpdatedDate(date);
						objErrorCode.setLastUpdatedBy(id);

						errorCodeService.updateErrorCode(objErrorCode);

						objResponseEntity.put("status", true);
						objResponseEntity.put("errorcode", objErrorCode);
						objResponseEntity.put("data", "Success");
						return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

					}
					objResponseEntity.put("status", true);
					objResponseEntity.put("errorcode", errorcode);
					ResponseMessage = "Success";
				} else {
					objResponseEntity.put("status", false);
					ResponseMessage = "Invalid Request";
					objResponseEntity.put("errorcode", null);
				}

			} else {
				objResponseEntity.put("status", false);
				ResponseMessage = "Invalid Request";
				objResponseEntity.put("errorcode", null);
			}

			objResponseEntity.put("data", ResponseMessage);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (ConstraintViolationException cve) {
			objResponseEntity.put("errorcode", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", cve.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			objResponseEntity.put("errorcode", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : EditErrorCode
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function updates error code.
	 * 
	 * Input Params  : ErrorId,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/editerrorcode", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> EditErrorCode(Integer ErrorId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			ErrorCode objerrorcode = errorCodeService.getErrorCodeById(ErrorId);

			int EquipmentTypeID = objerrorcode.getEquipmentTypeId();

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			objResponseEntity.put("equipmenttype", equipmenttypeService.getEquipmentTypeById(EquipmentTypeID));

			objResponseEntity.put("errorcode", objerrorcode);
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : geterrorcodes
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns error code.
	 * 
	 * Input Params  : TypeID,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/geterrorcodes", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> geterrorcodes(Integer TypeID, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			String timezonevalue = "0";
			if (!timezonevalue.equals("") && timezonevalue != null) {
				timezonevalue = session.getAttribute("timezonevalue").toString();
			}

			List<ErrorCode> objerrorcode = errorCodeService.listErrorCodes(TypeID);
			List<EquipmentType> lstEquipmentType = equipmenttypeService.listEquipmentTypes();

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			Calendar cal = Calendar.getInstance();

			for (int i = 0; i < objerrorcode.size(); i++) {

				if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
					if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {

						if (objerrorcode.get(i).getCreationDate() != null) {

							cal.setTime(objerrorcode.get(i).getCreationDate());
							cal.add(Calendar.MINUTE, Integer.valueOf(timezonevalue));

							objerrorcode.get(i).setCreationDateText(sdf.format(cal.getTime()));

						}
						if (objerrorcode.get(i).getLastUpdatedDate() != null) {

							cal.setTime(objerrorcode.get(i).getLastUpdatedDate());
							cal.add(Calendar.MINUTE, Integer.valueOf(timezonevalue));
							objerrorcode.get(i).setLastUpdatedDateText(sdf.format(cal.getTime()));

						}
					}

					if (objerrorcode.get(i).getCreationDate() != null) {
						objerrorcode.get(i).setCreationDateText(sdf.format(objerrorcode.get(i).getCreationDate()));

					}
					if (objerrorcode.get(i).getLastUpdatedDate() != null) {
						objerrorcode.get(i)
								.setLastUpdatedDateText(sdf.format(objerrorcode.get(i).getLastUpdatedDate()));

					}

				}

				int equipmenttypeid = objerrorcode.get(i).getEquipmentTypeId();
				if (lstEquipmentType.stream().filter(p -> p.getEquipmentTypeId().equals(equipmenttypeid))
						.collect(Collectors.toList()).size() >= 1) {
					EquipmentType objEquipmentType = new EquipmentType();
					objEquipmentType = lstEquipmentType.stream()
							.filter(p -> p.getEquipmentTypeId().equals(equipmenttypeid)).collect(Collectors.toList())
							.get(0);

					objerrorcode.get(i).setEquipmentType(objEquipmentType.getEquipmentType());
				}
			}

			objResponseEntity.put("errorcode", objerrorcode);
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

}
