
/******************************************************
 * 
 *    	Filename	: EquipmentListController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Equipment related actions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.CustomerViewBean;
import com.mestech.eampm.bean.EquipmentViewBean;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.DataSource;
import com.mestech.eampm.model.DataTransaction;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.DataSourceService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteStatusService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class EquipmentListController {
	@Autowired
	private CustomerService customerService;

	@Autowired
	private DataSourceService dataSourceService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private UserService userService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : listCustomerLists
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This method loads the equipments page.
	 * 
	 * Input Params  : model,session
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/equipmentlist", method = RequestMethod.GET)
	public String listCustomerLists(Model model, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());

		AccessListBean objAccessListBean = new AccessListBean();

		List<EquipmentViewBean> lstEquipmentViewBean = new ArrayList<EquipmentViewBean>();
		List<DataSource> lstDataSourceAll = new ArrayList<DataSource>();

		List<Equipment> lstEquipment = equipmentService.listInvertersByUserId(id);
		lstDataSourceAll = dataSourceService.getDataSourceForInvertersListByUserId(id);

		for (int i = 0; i < lstEquipment.size(); i++) {
			EquipmentViewBean objEquipmentViewBean = new EquipmentViewBean();

			objEquipmentViewBean.setDivID(i);
			objEquipmentViewBean.setEquipmentCode(lstEquipment.get(i).getEquipmentCode());
			objEquipmentViewBean.setEquipmentName(lstEquipment.get(i).getCustomerNaming());
			objEquipmentViewBean.setEquipmentID(lstEquipment.get(i).getEquipmentId().toString());

			objEquipmentViewBean.setDataSearch(lstEquipment.get(i).getEquipmentCode().toLowerCase() + " "
					+ lstEquipment.get(i).getEquipmentCode().toLowerCase() + " "
					+ lstEquipment.get(i).getCustomerReference().toLowerCase());

			/*
			 * Site objSite = siteService.getSiteById(lstEquipment.get(i).getSiteID());
			 * 
			 * if(objSite!=null) { objEquipmentViewBean.setSiteName(objSite.getSiteName());
			 * Customer objCustomer =
			 * customerService.getCustomerById(objSite.getCustomerID());
			 * 
			 * if(objCustomer!=null) {
			 * objEquipmentViewBean.setCustomerName(objCustomer.getCustomerName()); }
			 * 
			 * }
			 */

			int intEquId = lstEquipment.get(i).getEquipmentId();

			List<DataSource> lstDataSource = new ArrayList<DataSource>();
			lstDataSource = lstDataSourceAll.stream().filter(p -> p.getEquipmentID() == intEquId)
					.collect(Collectors.toList());

			if (lstDataSource.size() >= 1) {
				Double dblTotalEnergy = 0.0;
				Double dblYesterdayTotalEnergy = 0.0;
				Double dblTodayEnergy = 0.0;

				if (lstDataSource.get(0).getTotalEnergy() != null) {
					dblTotalEnergy = lstDataSource.get(0).getTotalEnergy();
				}

				if (lstDataSource.get(0).getYesterdayTotalEnergy() != null) {
					dblYesterdayTotalEnergy = lstDataSource.get(0).getYesterdayTotalEnergy();
				}

				dblTodayEnergy = dblTotalEnergy - dblYesterdayTotalEnergy;
				dblTotalEnergy = dblTotalEnergy / 1000;

				objEquipmentViewBean.setTotalEnergy(String.format("%.2f", dblTotalEnergy));
				objEquipmentViewBean.setTodayEnergy(String.format("%.2f", dblTodayEnergy));// 907.2 kWh

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
				SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

				if (lstDataSource.get(0).getTimestamp() != null) {
					objEquipmentViewBean.setEnergyLastUpdate(sdf.format(lstDataSource.get(0).getTimestamp()));
				} else {
					objEquipmentViewBean.setEnergyLastUpdate("-");
				}

				String EStatus = lstDataSource.get(0).getStatus().toString();

				if (EStatus.equals("1")) {
					objEquipmentViewBean.setEquipmentStatus("Active");
				} else if (EStatus.equals("2")) {
					objEquipmentViewBean.setEquipmentStatus("Warning");
				} else if (EStatus.equals("3")) {
					objEquipmentViewBean.setEquipmentStatus("Error");
				} else {
					objEquipmentViewBean.setEquipmentStatus("Offline");
				}

			}

			lstEquipmentViewBean.add(objEquipmentViewBean);
		}

		try {
			User objUser = userService.getUserById(id);
			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
			List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getUserName());

			if (objUser.getRoleID() != 4) {
				objAccessListBean.setCustomerListView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActivityID() == 1 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setDashboard("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 2 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setOverview("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 3 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setSystemMonitoring("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 4 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setVisualization("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 5 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalytics("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 6 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setPortfolioManagement("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 7 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setTicketing("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 8 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setForcasting("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 9 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setConfiguration("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 10
						&& lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalysis("visible");
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		model.addAttribute("siteList", getDropdownList("Site", id));
		model.addAttribute("userList", getDropdownList("User", id));

		model.addAttribute("access", objAccessListBean);
		model.addAttribute("ticketcreation", new TicketDetail());

		model.addAttribute("equipmentlist", lstEquipmentViewBean);

		return "equipmentlist";
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This method returns value for dropdown list.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "User") {
			List<User> ddlList = userService.listFieldUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		}

		return ddlMap;
	}

}