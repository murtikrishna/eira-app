
/******************************************************
 * 
 *    	Filename	: LoginController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Authentication and login activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.model.User;
import com.mestech.eampm.model.UserLog;
import com.mestech.eampm.service.UserLogService;
import com.mestech.eampm.service.UserService;

@Controller
public class LoginController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserLogService userlogService;

	@Autowired
	private SessionRegistry sessionRegistry;

	/********************************************************************************************
	 * 
	 * Function Name : login
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads the login page.
	 * 
	 * Input Params : model,session,httpServletRequest
	 * 
	 * Return Value : String
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = { "/", "/login" }, method = RequestMethod.GET)
	public String login(Model model, HttpSession session, HttpServletRequest httpServletRequest) {

		model.addAttribute("login", new User());

		session.setAttribute("eampmuserid", null);

		System.out.println("eampmloginsubmit :: " + session.getAttribute("eampmloginsubmit"));

		if (session.getAttribute("eampmloginsubmit") == null) {
			session.setAttribute("eampmerrormsg", "");
		} else {
			String errMsg = (String) session.getAttribute("errMsg");// httpServletRequest.getAttribute("errorMsg")==null?"":(String)
																	// httpServletRequest.getAttribute("errorMsg");
			session.setAttribute("eampmerrormsg", errMsg);
		}

		session.setAttribute("eampmloginsubmit", null);

		return "login";
	}

	/*
	 * @RequestMapping(value = "/loginFailure", method = RequestMethod.GET) public
	 * String login1(Model model,HttpSession session) {
	 * 
	 * model.addAttribute("login", new User());
	 * 
	 * 
	 * session.setAttribute("eampmuserid", null);
	 * 
	 * if(session.getAttribute("eampmloginsubmit")==null) {
	 * session.setAttribute("eampmerrormsg", ""); }
	 * 
	 * session.setAttribute("eampmloginsubmit", null); new
	 * User().setErrorMessage("Invalid Username / Password!");
	 * session.setAttribute("eampmuserid", null);
	 * session.setAttribute("eampmerrormsg", "Invalid Username / Password!");
	 * 
	 * 
	 * return "login"; }
	 */

	/********************************************************************************************
	 * 
	 * Function Name : login1
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function used to logout's user.
	 * 
	 * Input Params : model,session,authentication
	 * 
	 * Return Value : String
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/logout1", method = RequestMethod.GET)
	public String login1(Model model, HttpSession session, Authentication authentication) {

		model.addAttribute("login", new User());
		model.addAttribute("clear_window", "clear_window");

		sessionRegistry.removeSessionInformation(session.getId());

		SecurityContextHolder.clearContext();
		if (session != null) {
			session.invalidate();
		}
		authentication = null;

		return "login";
	}

	/********************************************************************************************
	 * 
	 * Function Name : loginAuthendication
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function authenticate user.
	 * 
	 * Input Params : user,model,session
	 * 
	 * Return Value : String
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/loginauthendication", method = RequestMethod.POST)
	public String loginAuthendication(@ModelAttribute("user") User user, Model model, HttpSession session) {

		session.setAttribute("eampmloginsubmit", "1");
		String strUserName = user.getUserName();
		String strPassword = user.getPassword();
		User objUser = userService.getUserByName(strUserName);
		if (objUser.getUserName() != null && objUser.getPassword() != null) {

			if (objUser.getUserName().toUpperCase().equals(strUserName.toUpperCase())
					&& objUser.getPassword().equals(strPassword)) {
				String UserId = objUser.getUserId().toString();
				String UserCode = objUser.getUserCode();

				String UserName = objUser.getUserName();
				UserLog userlog = new UserLog();

				Date date = new Date();
				userlog.setUserName(UserName);
				userlog.setUserId(Integer.valueOf(UserId));
				userlog.setDlogDate(date);
				userlog.setActiveFlag(1);
				userlog.setSessionId(session.getId());
				userlog.setSlogDate(date);
				userlog.setCreationDate(date);
				userlogService.addUserlog(userlog);
				session.setAttribute("eampmuserid", UserId);
				session.setAttribute("eampmerrormsg", "");

				return "redirect:/dashboard";
			} else {

				user.setErrorMessage("Invalid Username / Password!");
				session.setAttribute("eampmuserid", null);
				session.setAttribute("eampmerrormsg", "Invalid Username / Password!");
				System.out.println("eampmloginsubmit ::  :: " + session.getAttribute("eampmloginsubmit"));

				return "redirect:/login";
			}
		} else {
			user.setErrorMessage("Invalid Username / Password!");
			session.setAttribute("eampmuserid", null);
			session.setAttribute("eampmerrormsg", "Invalid Username / Password!");
			System.out.println("eampmloginsubmit :: " + session.getAttribute("eampmloginsubmit"));
			return "redirect:/login";
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : accessDenied
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads access denied page.
	 * 
	 * Return Value : String
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/access-denied", method = RequestMethod.GET)
	public String accessDenied() {
		return "access_denied";
	}

}
