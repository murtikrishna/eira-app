
/******************************************************
 * 
 *    	Filename	: RoleActivityController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Role functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.config.CacheHandler;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.model.UserRole;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.Response;
import com.mestech.eampm.utility.RoleActivityResponse;
import com.mestech.eampm.utility.RoleMapRequest;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

@Controller
public class RoleActivityController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userroleService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns values for dropdown list.
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : RoleAccessPageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads role access page.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/roleaccess", method = RequestMethod.GET)
	public String RoleAccessPageLoad(Model model, HttpSession session, Authentication authentication) {

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();
		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}
			Map<String, String> roleDropDown = new HashMap<>();
			roleDropDown.put(null, "Select");
			List<UserRole> userRoles = userRoleService.listUserRoles();
			if (userRoles != null && !userRoles.isEmpty()) {
				for (UserRole userRole : userRoles) {
					if (!userRole.getRoleName().equalsIgnoreCase("FieldUser"))
						roleDropDown.put(userRole.getRoleId().toString(), userRole.getRoleName());
				}
			}
			model.addAttribute("roleDropDown", roleDropDown);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("roleactivity", new RoleActivity());
		model.addAttribute("roleactivityList", roleActivityService.listRoleActivities());
		model.addAttribute("activeStatusList", getStatusDropdownList());
		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));
		return "rolemapping";
	}

	/********************************************************************************************
	 * 
	 * Function Name : getUpdatedAccessList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns role access data.
	 * 
	 * Return Value  : List<RoleActivity> 
	 * 
	 * 
	 **********************************************************************************************/
	public List<RoleActivity> getUpdatedAccessList() {
		List<RoleActivity> ddlAccessList = roleActivityService.listRoleActivities();
		List<String> ddlAccessList1 = new ArrayList<String>();
		List<UserRole> ddlUserroleList = userroleService.listUserRoles();
		List<Activity> ddlActivityList = activityService.listActivities();
		List<UserRole> lstUserRoleList = new ArrayList<UserRole>();
		List<Activity> lstActivityList = new ArrayList<Activity>();
		String roleaceespage = "";
		String rolename = "";

		for (int i = 0; i < ddlAccessList.size(); i++) {

			Map<String, String> arr = new LinkedHashMap<String, String>();

			int roleid = ddlAccessList.get(i).getRoleID();
			int activityid = ddlAccessList.get(i).getActivityID();

			lstActivityList = ddlActivityList.stream().filter(p -> p.getActivityId() == activityid)
					.collect(Collectors.toList());
			lstUserRoleList = ddlUserroleList.stream().filter(p -> p.getRoleId() == roleid)
					.collect(Collectors.toList());

			String var1 = lstUserRoleList.get(0).getRoleName();
			String var2 = lstActivityList.get(0).getActivityName();

			ddlAccessList.get(i).setRoleName(lstUserRoleList.get(0).getRoleName());
			ddlAccessList.get(i).setAccessPages(lstActivityList.get(0).getActivityName());

			arr.put(var1, var2);

		}

		return ddlAccessList;
	}

	/********************************************************************************************
	 * 
	 * Function Name : NewRolemappingSaveorUpdate
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function updates role mapping.
	 * 
	 * Input Params  : roleMapRequest
	 * 
	 * Return Value  : Response
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/updateRoleMapping", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public @ResponseBody Response NewRolemappingSaveorUpdate(@RequestBody RoleMapRequest roleMapRequest) {
		Response response = new Response();
		try {
			if (roleMapRequest.getRoleId() != null) {
				List<RoleActivity> roleActivities = roleActivityService
						.getRoleActivityByRoleId(roleMapRequest.getRoleId());
				if (null != roleActivities && !roleActivities.isEmpty()) {
					for (RoleActivity roleActivity : roleActivities) {
						roleActivityService.deleteRoleActivity(roleActivity);
						Cache cache = CacheHandler.getCache("roleActivityCache");
						Element element = cache.get(roleActivity.getRoleID());
						if (element == null)
							continue;
						@SuppressWarnings("unchecked")
						List<RoleActivity> activities = (List<RoleActivity>) element.getObjectValue();
						if (activities != null && !activities.isEmpty()) {
							Iterator<RoleActivity> iterator = activities.iterator();
							while (iterator.hasNext()) {
								RoleActivity roleActivity2 = (RoleActivity) iterator.next();
								if (roleActivity2 != null)
									iterator.remove();
							}
						}
					}
					List<Integer> activityRequests = roleMapRequest.getActivityId();
					if (activityRequests != null && !activityRequests.isEmpty()) {
						for (Integer activityRequest : activityRequests) {
							RoleActivity activity = new RoleActivity(roleMapRequest.getRoleId(), activityRequest,
									new Integer(1), new Date());
							roleActivityService.addRoleActivity(activity);
							Cache cache = CacheHandler.getCache("roleActivityCache");
							if (cache.get(roleMapRequest.getRoleId()) != null) {
								Element element = cache.get(roleMapRequest.getRoleId());
								@SuppressWarnings("unchecked")
								List<RoleActivity> activities = (List<RoleActivity>) element.getObjectValue();
								activities.add(activity);
								cache.put(new Element(roleMapRequest.getRoleId(), activities));
							}
						}
					}
				} else {
					List<Integer> activityRequests = roleMapRequest.getActivityId();
					if (activityRequests != null && !activityRequests.isEmpty()) {
						for (Integer activityRequest : activityRequests) {
							RoleActivity activity = new RoleActivity(roleMapRequest.getRoleId(), activityRequest,
									new Integer(1), new Date());
							roleActivityService.addRoleActivity(activity);
							Cache cache = CacheHandler.getCache("roleActivityCache");
							if (cache.get(roleMapRequest.getRoleId()) == null) {
								List<RoleActivity> activities = new ArrayList<>();
								activities.add(activity);
								cache.put(new Element(roleMapRequest.getRoleId(), activities));
							} else {
								Element element = cache.get(roleMapRequest.getRoleId());
								if (element == null)
									continue;
								@SuppressWarnings("unchecked")
								List<RoleActivity> activities = (List<RoleActivity>) element.getObjectValue();
								activities.add(activity);
								cache.put(new Element(roleMapRequest.getRoleId(), activities));
							}
						}
					}
				}
				response.setStatus("Success");
				response.setData("Access granted successfully");
			} else {
				response.setStatus("failed");
				response.setData("Role must be selected before grant access..");
			}
		} catch (Exception e) {
			response.setStatus("failed");
			response.setData("Failed to grant access please try again..");
		}

		return response;
	}

	/********************************************************************************************
	 * 
	 * Function Name : RolePermissionsGridLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads the role access grid.
	 * 
	 * Return Value  : Response
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getRolePermissions", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Response RolePermissionsGridLoad() {
		Response response = new Response();
		try {
			List<RoleActivityResponse> activityResponses = new ArrayList<>();
			List<RoleActivity> roleActivities = roleActivityService.listRoleActivities();
			if (roleActivities != null && !roleActivities.isEmpty()) {
				Map<String, String> map = new HashMap<>();
				for (RoleActivity roleActivity : roleActivities) {
					UserRole userRole = userRoleService.getUserRoleById(roleActivity.getRoleID());
					Cache cache = CacheHandler.getCache("activityCache");
					Element element = cache.get(roleActivity.getActivityID());
					Activity activity = (Activity) element.getObjectValue();
					if (map.get(userRole.getRoleName()) != null) {
						String accessScreens = map.get(userRole.getRoleName());
						accessScreens = accessScreens.concat(",").concat(activity.getActivityName());
						map.put(userRole.getRoleName(), accessScreens);
					} else {
						map.put(userRole.getRoleName(), activity.getActivityName());
					}
				}
				for (Map.Entry<String, String> entry : map.entrySet()) {
					RoleActivityResponse activityResponse = new RoleActivityResponse(entry.getValue(), entry.getKey());
					activityResponses.add(activityResponse);
				}
				response.setStatus("success");
				response.setData(activityResponses);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus("failed");
			response.setData("Failed to grant access please try again..");
		}

		return response;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns data for dropdown.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

}
