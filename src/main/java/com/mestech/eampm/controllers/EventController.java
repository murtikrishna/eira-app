
/******************************************************
 * 
 *    	Filename	: EventController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Event functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.model.Event;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.EventService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class EventController {

	@Autowired
	private EventService eventservice;
	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : listEvents
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads events page
	 * 
	 * Input Params  : model,session
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/events", method = RequestMethod.GET)
	public String listEvents(Model model, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());

		AccessListBean objAccessListBean = new AccessListBean();

		try {
			User objUser = userService.getUserById(id);
			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
			List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getUserName());

			if (objUser.getRoleID() != 4) {
				objAccessListBean.setCustomerListView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActivityID() == 1 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setDashboard("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 2 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setOverview("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 3 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setSystemMonitoring("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 4 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setVisualization("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 5 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalytics("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 6 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setPortfolioManagement("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 7 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setTicketing("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 8 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setForcasting("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 9 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setConfiguration("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 10
						&& lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalysis("visible");
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("access", objAccessListBean);

		model.addAttribute("event", new Event());
		model.addAttribute("eventList", eventservice.listEvents());
		model.addAttribute("activeStatusList", getStatusDropdownList());

		return "event";
	}

	
	/********************************************************************************************
	 * 
	 * Function Name : addevent
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function adds an event.
	 * 
	 * Input Params  : event
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// Same method For both Add and Update UnitOfMeasurement
	// @RequestMapping(value = "/unitofmeasurement/add", method =
	// RequestMethod.POST)
	@RequestMapping(value = "/addnewevent", method = RequestMethod.POST)
	public String addevent(@ModelAttribute("user") Event event) {

		Date date = new Date();

		if (event.getEventId() == null || event.getEventId() == 0) {
			// new unitofmeasurement, add it
			event.setCreationDate(date);
			eventservice.addEvent(event);
		} else {
			// existing unitofmeasurement, call update
			event.setLastUpdatedDate(date);
			eventservice.updateEvent(event);
		}

		return "redirect:/events";

	}

	/********************************************************************************************
	 * 
	 * Function Name : removeevent
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function removes an event.
	 * 
	 * Input Params  : id
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// @RequestMapping("/unitofmeasurement/remove/{id}")
	@RequestMapping("/removeevent{id}")
	public String removeevent(@PathVariable("id") int id) {

		eventservice.removeEvent(id);
		return "redirect:/events";
	}

	/********************************************************************************************
	 * 
	 * Function Name : editevent
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function updates an event.
	 * 
	 * Input Params  : id,model
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// @RequestMapping("/unitofmeasurement/edit/{id}")
	@RequestMapping("/editevent{id}")
	public String editevent(@PathVariable("id") int id, Model model) {
		model.addAttribute("event", eventservice.getEventById(id));
		model.addAttribute("eventList", eventservice.listEvents());
		model.addAttribute("activeStatusList", getStatusDropdownList());
		return "event";
	}
}
