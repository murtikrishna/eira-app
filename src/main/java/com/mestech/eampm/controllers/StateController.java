
/******************************************************
 * 
 *    	Filename	: StateController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with State related Activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.CountryRegion;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.State;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.CountryRegionService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.StateService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class StateController {

	@Autowired
	private StateService stateService;

	@Autowired
	private CountryService countryService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns values for list.
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Input Params  : DropdownName
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Country") {
			List<Country> ddlList = countryService.listCountries();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getCountryId().toString(), ddlList.get(i).getCountryName());

			}

		} else if (DropdownName == "Zone") {

			ddlMap.put("North", "North");
			ddlMap.put("East", "East");
			ddlMap.put("West", "West");
			ddlMap.put("South", "South");
			ddlMap.put("Central", "Central");
			ddlMap.put("North East", "North East");
			ddlMap.put("North West", " North West");
			ddlMap.put("South East", "South East");
			ddlMap.put("South West", " South West");

		} else if (DropdownName == "StateType") {

			ddlMap.put("State", "State");
			ddlMap.put("Union Teritory", "Union Teritory");

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getUpdatedStateList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns all state list.
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	public List<State> getUpdatedStateList() {
		List<State> ddlStatesList = stateService.listStates();

		for (int i = 0; i < ddlStatesList.size(); i++) {
			String CountryName = countryService.getCountryById(ddlStatesList.get(i).getCountryID()).getCountryName();
			ddlStatesList.get(i).setCountryName(CountryName);
		}

		return ddlStatesList;
	}

	/********************************************************************************************
	 * 
	 * Function Name : listStates
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads teh states page.
	 * 
	 * Input Params  : model,session
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/states", method = RequestMethod.GET)
	public String listStates(Model model, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());

		AccessListBean objAccessListBean = new AccessListBean();

		try {
			User objUser = userService.getUserById(id);
			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
			List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getUserName());

			if (objUser.getRoleID() != 4) {
				objAccessListBean.setCustomerListView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActivityID() == 1 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setDashboard("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 2 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setOverview("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 3 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setSystemMonitoring("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 4 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setVisualization("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 5 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalytics("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 6 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setPortfolioManagement("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 7 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setTicketing("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 8 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setForcasting("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 9 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setConfiguration("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 10
						&& lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalysis("visible");
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("access", objAccessListBean);

		model.addAttribute("state", new State());

		model.addAttribute("stateList", getUpdatedStateList());
		model.addAttribute("zoneList", getDropdownList("Zone"));
		model.addAttribute("statetypeList", getDropdownList("StateType"));
		model.addAttribute("countryList", getDropdownList("Country"));
		model.addAttribute("activeStatusList", getStatusDropdownList());

		return "state";
	}

	/********************************************************************************************
	 * 
	 * Function Name : addstate
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function adds a state.
	 * 
	 * Input Params  : state,session
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// Same method For both Add and Update State
	// @RequestMapping(value = "/state/add", method = RequestMethod.POST)
	@RequestMapping(value = "/addnewstate", method = RequestMethod.POST)
	public String addstate(@ModelAttribute("state") State state, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		Date date = new Date();
		if (state.getStateId() == null || state.getStateId() == 0) {
			// new state, add it
			state.setCreationDate(date);
			stateService.addState(state);
		} else {
			// existing state, call update
			state.setLastUpdatedDate(date);
			stateService.updateState(state);
		}

		return "redirect:/states";

	}

	/********************************************************************************************
	 * 
	 * Function Name : removestate
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function removes a state.
	 * 
	 * Input Params  : id,session
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	// @RequestMapping("/state/remove/{id}")
	@RequestMapping("/removestate{id}")
	public String removestate(@PathVariable("id") int id, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		stateService.removeState(id);
		return "redirect:/states";
	}

	/********************************************************************************************
	 * 
	 * Function Name : editstate
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function updates a state.
	 * 
	 * Input Params  : id,model,session
	 * 
	 * Return Value  : String
	 *  
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	// @RequestMapping("/state/edit/{id}")
	@RequestMapping("/editstate{id}")
	public String editstate(@PathVariable("id") int id, Model model, HttpSession session) {

		if (session.getAttribute("eampmuserid") == null || session.getAttribute("eampmuserid").equals("")) {
			return "redirect:/login";
		}

		int refid = Integer.parseInt(session.getAttribute("eampmuserid").toString());
		AccessListBean objAccessListBean = new AccessListBean();

		try {
			User objUser = userService.getUserById(refid);
			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
			List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(refid);
			objAccessListBean.setUserName(objUser.getUserName());

			if (objUser.getRoleID() != 4) {
				objAccessListBean.setCustomerListView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActivityID() == 1 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setDashboard("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 2 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setOverview("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 3 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setSystemMonitoring("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 4 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setVisualization("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 5 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalytics("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 6 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setPortfolioManagement("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 7 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setTicketing("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 8 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setForcasting("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 9 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setConfiguration("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 10
						&& lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalysis("visible");
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("access", objAccessListBean);

		model.addAttribute("state", stateService.getStateById(id));

		model.addAttribute("stateList", getUpdatedStateList());

		model.addAttribute("zoneList", getDropdownList("Zone"));
		model.addAttribute("statetypeList", getDropdownList("StateType"));
		model.addAttribute("countryList", getDropdownList("Country"));
		model.addAttribute("activeStatusList", getStatusDropdownList());

		return "state";
	}
}
