
/******************************************************
 * 
 *    	Filename	: MasterUploadController.java
 *      
 *      Author		: Johnson Edwin Raj
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Bulk Upload functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentCategory;
import com.mestech.eampm.model.EquipmentType;
import com.mestech.eampm.model.MparameterIntegratedStandards;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.BulkUpload;
import com.mestech.eampm.service.EquipmentCategoryService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.EquipmentTypeService;
import com.mestech.eampm.service.ParameterStandardsService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.EquipmentRequest;
import com.mestech.eampm.utility.ErrorDetector;
import com.mestech.eampm.utility.MasterSheetType;
import com.mestech.eampm.utility.MasterUploadRequest;
import com.mestech.eampm.utility.Response;

@Controller
public class MasterUploadController {

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private EquipmentController equipmentController;

	@Autowired
	private EquipmentTypeService equipmenttypeService;

	@Autowired
	private EquipmentCategoryService equipmentCategoryService;
	@Autowired
	private ActivityService activityService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;
	@Autowired
	private BulkUpload bulkUpload;
	@Autowired
	private RoleActivityService roleActivityService;
	@Autowired
	private ParameterStandardsService ParameterStandardsService;

	/********************************************************************************************
	 * 
	 * Function Name : getAlldatalogger
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns all parameter names.
	 * 
	 * Return Value : Response
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/testingISP", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Response getAlldatalogger() {
		// flag=1 => get items with active -> 1; flag=0 => get all items;
		Response response = new Response();

		response.setData(ParameterStandardsService.getallStandardParameterNames());
		response.setStatus("Success");
		return response;
	}

	/********************************************************************************************
	 * 
	 * Function Name : listMasters
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads master upload page.
	 * 
	 * Input Params : model,session,authentication
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/masteruploads", method = RequestMethod.GET)
	public String listMasters(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);
			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));

		return "masterupload";
	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns values for dropdown.
	 * 
	 * Input Params : DropdownName,refid
	 * 
	 * Return Value : String
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : masterSheet
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function uploades bulk upload file.
	 * 
	 * Input Params : list,sheetType,authentication
	 * 
	 * Return Value : Response
	 * 
	 * 
	 **********************************************************************************************/
	/* @RequestParam(value = "secondArg") String secondArg */
	@RequestMapping(value = "/masterSheet", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Response masterSheet(@RequestBody MasterUploadRequest list,
			@RequestParam(value = "sheetType") String sheetType, Authentication authentication) {
		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		BigInteger userId = BigInteger.valueOf(loginUserDetails.getEampmuserid());
		Response response = new Response();
		if (list != null) {
			if (MasterSheetType.EQUIPMENT.getType().equalsIgnoreCase(sheetType)) {
				List<EquipmentRequest> listOfEquipments = list.getEquipment();

				List<Equipment> equipments = new LinkedList<>();

				for (EquipmentRequest equipmentRequest : listOfEquipments) {
					Equipment equipment = new Equipment();
					equipment.setActiveFlag(equipmentRequest.getActiveFlag());
					if (equipmentRequest.getCapacity() != null)
						equipment.setCapacity(new Double(equipmentRequest.getCapacity()));
					equipment.setSiteName(equipmentRequest.getSiteName());
					equipment.setCategoryName(equipmentRequest.getCategoryName());
					equipment.setEquipmentTypeName(equipmentRequest.getEquipmentTypeName());
					equipment.setPrimarySerialNumber(equipmentRequest.getPrimarySerialNumber());
					equipment.setDescription(equipmentRequest.getDescription());
					equipment.setRemarks(equipmentRequest.getRemarks());
					equipment.setCustomerReference(equipmentRequest.getCustomerReference());
					equipment.setCustomerNaming(equipmentRequest.getCustomerNaming());
					equipment.setInstallationDateInString(equipmentRequest.getInstallationDate());
					equipment.setWarrentyDateInString(equipmentRequest.getWarrantyDate());
					equipment.setEquipmentSelection(equipmentRequest.getEquipmentSelection());
					equipment.setComponents(equipmentRequest.getComponents());
					equipment.setDisconnectRating(equipmentRequest.getDisconnectRating());
					equipment.setDisconnectType(equipmentRequest.getDisconnectType());
					equipment.setCreatedBy(userId.intValue());
					equipment.setCreationDate(new Date());
					equipments.add(equipment);
				}
				List<Equipment> equips = equipmentUpload(equipments);
				if (equips == null || equips.isEmpty()) {
					response.setStatus("Success");
					response.setData(equips);
				} else {
					response.setStatus("Failed");
					response.setData(equips);
				}
				return response;
			} else if (MasterSheetType.PARAMETER.getType().equalsIgnoreCase(sheetType)) {
				List<MparameterIntegratedStandards> mparameterIntegratedStandards = list.getIntegratedStandards();
				for (MparameterIntegratedStandards integratedStandard : mparameterIntegratedStandards) {
					integratedStandard.setCreationDate(new Date());
					integratedStandard.setCreatedBy(userId);
				}
				List<MparameterIntegratedStandards> parameters = parameterUpload(mparameterIntegratedStandards);
				if (parameters == null || parameters.isEmpty()) {
					response.setStatus("Success");
					response.setData(parameters);
				} else {
					response.setStatus("Failed");
					response.setData(parameters);
				}
				return response;
			}
		} else {
			response.setStatus("Failed");
			response.setData(null);
		}
		return response;
	}

	/********************************************************************************************
	 * 
	 * Function Name : parameterUpload
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function validates and binds appropriate error to
	 * parameter.
	 * 
	 * Input Params : mparameterIntegratedStandards
	 * 
	 * Return Value : List<MparameterIntegratedStandards>
	 * 
	 * 
	 **********************************************************************************************/
	public List<MparameterIntegratedStandards> parameterUpload(
			List<MparameterIntegratedStandards> mparameterIntegratedStandards) {
		List<String> standardParmeters = ParameterStandardsService.getallStandardParameterNames();
		List<MparameterIntegratedStandards> wrongElements = new ArrayList<>();

		for (MparameterIntegratedStandards paIntegratedStandards : mparameterIntegratedStandards) {
			List<ErrorDetector> errorDetectors = new ArrayList<>();
			if (paIntegratedStandards.getStandardName() != null && !paIntegratedStandards.getStandardName().isEmpty()) {
				if (!standardParmeters.contains(paIntegratedStandards.getStandardName())) {
					ErrorDetector errorDetector = new ErrorDetector();
					errorDetector.setFieldName("standardName");
					List<String> error = new ArrayList<>();
					error.add("err-par-sta-ser");
					errorDetector.setErrors(error);
					errorDetectors.add(errorDetector);
				}
			}
			if (errorDetectors != null && !errorDetectors.isEmpty()) {
				paIntegratedStandards.setErrorDetectors(errorDetectors);
				wrongElements.add(paIntegratedStandards);
			}
		}

		if (wrongElements == null || wrongElements.isEmpty()) {
			for (MparameterIntegratedStandards integratedStandards : mparameterIntegratedStandards) {
				ParameterStandardsService.saveParameterIntegratedStandards(integratedStandards);
			}
		}
		return wrongElements;
	}

	/********************************************************************************************
	 * 
	 * Function Name : equipmentUpload
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function validates and add errors.
	 * 
	 * Input Params : equipments
	 * 
	 * Return Value : List<Equipment>
	 * 
	 * 
	 **********************************************************************************************/
	public List<Equipment> equipmentUpload(List<Equipment> equipments) {
		List<Equipment> duplicateElements = new ArrayList<>();
		List<Site> sites = siteService.listSites();
		List<String> persistedSiteNames = new ArrayList<>();
		List<String> persistedEquipmentCategory = new ArrayList<>();
		Map<String, Integer> siteMap = new TreeMap<>();
		Map<String, Integer> equipmentTypeMap = new HashMap<>();
		List<String> persistedEquipmentType = new ArrayList<>();
		if (sites != null && !sites.isEmpty()) {
			for (Site site : sites) {
				siteMap.put(site.getSiteName(), site.getSiteId());
				persistedSiteNames.add(site.getSiteName().trim().toLowerCase());
			}
		}
		List<EquipmentCategory> equipmentCategories = equipmentCategoryService.listEquipmentCategories();
		if (equipmentCategories != null && !equipmentCategories.isEmpty()) {
			for (EquipmentCategory equipmentCategory : equipmentCategories) {
				persistedEquipmentCategory.add(equipmentCategory.getEquipmentCategory().trim().toLowerCase());
			}
		}

		List<EquipmentType> equipmentTypes = equipmenttypeService.listEquipmentTypes();
		if (equipmentTypes != null && !equipmentTypes.isEmpty()) {
			for (EquipmentType equipmentType : equipmentTypes) {
				equipmentTypeMap.put(equipmentType.getEquipmentType().trim(), equipmentType.getEquipmentTypeId());
				persistedEquipmentType.add(equipmentType.getEquipmentType().trim().toLowerCase());
			}
		}
		for (Equipment equipment : equipments) {
			List<ErrorDetector> errorDetectors = new ArrayList<>();
			if (!persistedSiteNames.contains(equipment.getSiteName().trim().toLowerCase())) {
				ErrorDetector errorDetector = new ErrorDetector();
				errorDetector.setFieldName("SiteName");
				List<String> errors = new ArrayList<>();
				errors.add("err-sitenot-ser");
				errorDetector.setErrors(errors);
				errorDetectors.add(errorDetector);
			}
			if (!persistedEquipmentCategory.contains(equipment.getCategoryName().trim().toLowerCase())) {
				ErrorDetector errorDetector = new ErrorDetector();
				errorDetector.setFieldName("CategoryName");
				List<String> errors = new ArrayList<>();
				errors.add("err-catnamenot-ser");
				errorDetector.setErrors(errors);
				errorDetectors.add(errorDetector);
			}
			if (persistedEquipmentType.contains(equipment.getEquipmentTypeName().trim().toLowerCase())) {
				Integer integer = equipmentTypeMap.get(equipment.getEquipmentTypeName().trim());
				equipment.setEquipmentTypeID(integer);
			} else {
				ErrorDetector errorDetector = new ErrorDetector();
				errorDetector.setFieldName("EquipmentTypeName");
				List<String> errors = new ArrayList<>();
				errors.add("err-equtnnot-ser");
				errorDetector.setErrors(errors);
				errorDetectors.add(errorDetector);
			}
			if (errorDetectors != null && !errorDetectors.isEmpty()) {
				equipment.setErrorDetectors(errorDetectors);
				duplicateElements.add(equipment);
			}
		}
		if (duplicateElements == null || duplicateElements.isEmpty()) {
			for (Equipment equipment : equipments) {
				Integer siteId = siteMap.get(equipment.getSiteName().trim());
				if (siteId != null) {
					String equipmentCode = equipmentController.eqipmentCodeGenerator(siteId);
					equipment.setSiteID(siteId);
					equipment.setEquipmentCode(equipmentCode);
					equipmentService.addEquipment(equipment);
				}
			}
		}
		return duplicateElements;

	}

	/********************************************************************************************
	 * 
	 * Function Name : historicalUpload
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads historical upload page.
	 * 
	 * Input Params : model,session,authentication
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/historicalUpload", method = RequestMethod.GET)
	public String historicalUpload(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);
			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));

		return "historicalUpload";
	}

	/********************************************************************************************
	 * 
	 * Function Name : dataCollection
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads datacollection page.
	 * 
	 * Input Params : model,session,authentication
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/datacollection", method = RequestMethod.GET)
	public String dataCollection(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);
			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));

		return "datacollection";
	}

}