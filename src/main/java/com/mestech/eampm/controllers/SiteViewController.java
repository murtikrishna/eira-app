
/******************************************************
 * 
 *    	Filename	: SiteViewController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with viewing site related functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.AnnualYeild;
import com.mestech.eampm.bean.EquipmentListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.bean.SiteCommonBean;
import com.mestech.eampm.bean.SiteListBean;
import com.mestech.eampm.bean.SiteViewBean;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.DataSource;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.service.TicketDetailService;
import com.mestech.eampm.model.DataTransaction;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentStatus;
import com.mestech.eampm.model.EventDetail;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;
import com.mestech.eampm.model.SiteSummary;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.DataSourceService;
import com.mestech.eampm.service.DataTransactionService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.EventDetailService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteStatusService;
import com.mestech.eampm.service.SiteSummaryService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.UtilityCommonDAO;
import com.mestech.eampm.utility.Constant;
import com.mestech.eampm.utility.UtilityCommon;

@Controller
public class SiteViewController {

	private UtilityCommon utilityCommon = new UtilityCommon();
	@Autowired
	private CustomerService customerService;

	@Autowired
	private SiteStatusService siteStatusService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private DataSourceService dataSourceService;

	@Autowired
	private DataTransactionService dataTransactionService;

	@Autowired
	private Constant constant;

	@Autowired
	private SiteService siteService;

	@Autowired
	private SiteSummaryService siteSummaryService;

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private TicketDetailService ticketDetailService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	@Autowired
	private EventDetailService eventDetailService;

	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();

	/********************************************************************************************
	 * 
	 * Function Name : GetZoneTimeTextOffsetByMins
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns timezone offset as text.
	 * 
	 * Input Params : AppServerDate,TimezoneOffset,DateFormat
	 * 
	 * Return Value : String
	 * 
	 * 
	 **********************************************************************************************/
	public String GetZoneTimeTextOffsetByMins(Date AppServerDate, String TimezoneOffset, String DateFormat) {
		SimpleDateFormat sdfdate = new SimpleDateFormat(DateFormat);
		Calendar cal = new GregorianCalendar();
		cal.setTime(AppServerDate);
		Integer OffsetInMins = Integer.valueOf(TimezoneOffset);
		cal.add(Calendar.MINUTE, OffsetInMins);
		return sdfdate.format(cal.getTime());
	}

	/********************************************************************************************
	 * 
	 * Function Name : GetZoneTimeOffsetByMins
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This fuction returns timezone offset as date.
	 * 
	 * Input Params : AppServerDate,TimezoneOffset
	 * 
	 * Return Value : Date
	 * 
	 * 
	 **********************************************************************************************/
	public Date GetZoneTimeOffsetByMins(Date AppServerDate, String TimezoneOffset) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(AppServerDate);
		Integer OffsetInMins = Integer.valueOf(TimezoneOffset);
		cal.add(Calendar.MINUTE, OffsetInMins);
		return cal.getTime();
	}

	/********************************************************************************************
	 * 
	 * Function Name : listCustomerViews
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads the site view page.
	 * 
	 * Input Params : id,model,session
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/siteview{id}", method = RequestMethod.GET)
	public String listCustomerViews(@PathVariable("id") int id, Model model, HttpSession session,
			Authentication authentication) throws MalformedURLException, IOException {

		try {

			if (authentication == null) {
				return "redirect:/login";
			} else if (session == null) {
				return "redirect:/login";
			} else if (session.getAttribute("timezonevalue") == null) {
				return "redirect:/login";
			}

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int refid = loginUserDetails.getEampmuserid();

			String tile1 = "siteview_sitedetails";
			String tile2 = "siteview_totalenergy";
			String tile3 = "siteview_scheduledjobs";
			String tile4 = "siteview_energy";
			String tile5 = "siteview_events";
			String tile6 = "siteview_tickets";
			String tile7 = "siteview_performancemetrics";
			String tile8 = "siteview_sensors";
			String tile9 = "siteview_location";
			String tile10 = "siteview_equipments";
			String tile11 = "siteview_weather";
			String tile12 = "siteview_co2";

			String timezonevalue = "0";
			if (!timezonevalue.equals("") && timezonevalue != null) {
				timezonevalue = session.getAttribute("timezonevalue").toString();
			}
			loginUserDetails.setTimezoneoffsetmin(timezonevalue);

			List<TicketDetail> lstTicketDetail = ticketDetailService.getTicketDetailListBySiteId(id, timezonevalue);

			List<Equipment> lstequipment = equipmentService.listFilterInvertersBySiteId(id);
			List<Equipment> lstequipmentCount = equipmentService.listEquipmentComunicationCount(id);
			SiteViewBean objSiteViewBean = new SiteViewBean();
			Double Sum = 0.0;
			for (int i = 0; i < lstequipment.size(); i++) {
				Double capacity = lstequipment.get(i).getCapacity();

				Sum = Sum + capacity;

			}
			objSiteViewBean.setEquipmentsCapacity(String.format("%.2f", Sum) + " kW");
			objSiteViewBean.setEquipmentCount(lstequipmentCount.size());

			Site objSite = siteService.getSiteById(id);
			List<AnnualYeild> Annualyeild = siteService.getAnnualyeildbySite(id);
			List<SiteCommonBean> lstInverterDeatils = equipmentService.listInverterDetails(id);
			SiteCommonBean siteBean = new SiteCommonBean();
			for (Object object : lstInverterDeatils) {
				Object[] obj = (Object[]) object;
				siteBean.setEnergymeterCount(Integer.valueOf(obj[0].toString()));

			}

			objSiteViewBean.setEnergymeterCount(siteBean.getEnergymeterCount());

			SiteStatus objSiteStatus = siteStatusService.getSiteStatusBySiteId(id);
			try {
				InputStream is = new URL("http://api.openweathermap.org/data/2.5/weather?lat=" + objSite.getLatitude()
						+ "&lon=" + objSite.getLongitude() + "&appid=6a977327fea2cb3bf2aae26c2c0474cb").openStream();
				InputStream is1 = new URL("http://api.openweathermap.org/data/2.5/forecast?lat=" + objSite.getLatitude()
						+ "&lon=" + objSite.getLongitude() + "&appid=6a977327fea2cb3bf2aae26c2c0474cb").openStream();
				List<String> lstvalues = constant.WeatherData(is);
				String Futuretemvalue = constant.FutureWeatherData(is1, authentication);

				if (Futuretemvalue != null) {
					Double Temp = Double.valueOf(((Float.valueOf(lstvalues.get(0)))) - 273.15);
					Double FutureTemp = Double.valueOf(((Float.valueOf(Futuretemvalue))) - 273.15);
					Double Speed = Double.valueOf(((Float.valueOf(lstvalues.get(2)))) * 3.6);
					objSiteViewBean.setWeatherStatus(lstvalues.get(1));// sunny
					objSiteViewBean.setWeatherTodayTemp(String.format("%.1f", Temp) + "�C"); // 37�C
					objSiteViewBean.setWeatherYesterdayTemp(String.format("%.1f", FutureTemp) + "�C");// 37�C
					objSiteViewBean.setWindSpeed(String.format("%.1f", Speed) + " kmph");// 12 kmph

				}

			} catch (Exception e) {

			}

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

			List<DataSource> lstDataSource = dataSourceService.getDataSourceForInvertersListBySiteId(id);

			List<DataSource> lstDataSourceEm = dataSourceService.getDataSourceForEnegrymetersListBySiteId(id);

			List<DataSource> lstDataSourceScb = dataSourceService.getDataSourceForScbListBySiteId(id);

			List<DataSource> lstDataSourceTracker = dataSourceService.getDataSourceForTrackerListBySiteId(id);

			List<DataSource> lstDataSourceSesnor = dataSourceService.getDataSourceForSesnorListBySiteId(id);

			// List<DataTransaction> lstDataTransaction =
			// dataTransactionService.listDataTransactionsByFilter2(id,2);

			List<SiteSummary> lstSiteSummary = siteSummaryService.getSiteSummaryListBySiteId(id);

			List<Equipment> lstEquipmentAll = equipmentService.listInverters();
			List<Equipment> lstEquipmentEM = equipmentService.listEnergymeters();
			List<Equipment> lstEquipmentScb = equipmentService.listScbs();
			List<Equipment> lstEquipmentTracker = equipmentService.listTrackers(id);

			List<EquipmentListBean> lstEquipmentList = new ArrayList<EquipmentListBean>();
			List<EquipmentListBean> lstEquipmentList1 = new ArrayList<EquipmentListBean>();
			List<EquipmentListBean> lstEquipmentList2 = new ArrayList<EquipmentListBean>();
			List<EquipmentListBean> lstEquipmentList3 = new ArrayList<EquipmentListBean>();

			if (objSite.getEquipmentFlag() == 0) {

				lstEquipmentList = constant.ForInverter(objSiteViewBean, lstDataSource, objSite, objSiteStatus,
						timezonevalue, lstEquipmentAll, id, lstDataSourceSesnor);
			} else if (objSite.getEquipmentFlag() == 1) {

				lstEquipmentList = constant.ForInverterGrid(objSiteViewBean, lstDataSource, objSite, objSiteStatus,
						timezonevalue, lstEquipmentAll, id);

				lstEquipmentList1 = constant.ForEnergymeter(objSiteViewBean, lstDataSourceEm, objSite, objSiteStatus,
						timezonevalue, lstEquipmentEM, id, lstDataSourceSesnor);

				lstEquipmentList2 = constant.ForScbGrid(objSiteViewBean, lstDataSourceScb, objSite, objSiteStatus,
						timezonevalue, lstEquipmentScb, id);

				lstEquipmentList3 = constant.ForTrackerGrid(objSiteViewBean, lstDataSourceTracker, objSite,
						objSiteStatus, timezonevalue, lstEquipmentTracker, id);

			}

			Integer varIntTotalOpenTicket = lstTicketDetail.stream().filter(p -> p.getState().equals(1))
					.collect(Collectors.toList()).size();
			Integer varIntTotalClosedTicket = lstTicketDetail.stream().filter(p -> p.getState().equals(2))
					.collect(Collectors.toList()).size();
			Integer varIntTotalHoldTicket = lstTicketDetail.stream().filter(p -> p.getState().equals(3))
					.collect(Collectors.toList()).size();

			objSiteViewBean.setOpenTickets(varIntTotalOpenTicket.toString());// 8
			objSiteViewBean.setCompletedTickets(varIntTotalClosedTicket.toString());// 5

			List<EventDetail> lstAllEvents = eventDetailService.getEventDetailListBySiteId(id, timezonevalue);
			List<EventDetail> lstTodayEvents = eventDetailService.getTodayEventDetailListBySiteId(id, timezonevalue);

			Integer TotalEvents = lstAllEvents.size();
			Integer TodayEvents = lstTodayEvents.size();

			objSiteViewBean.setTodayEvents(TodayEvents.toString()); //
			objSiteViewBean.setTotalEvents(TotalEvents.toString()); //

			Double Annaulyeild = 0.0;
			for (Object object : Annualyeild) {
				Object[] obj = (Object[]) object;
				if (obj[0] != null) {
					Annaulyeild = utilityCommon.DoubleFromBigDecimalObject(obj[1]);
				}

			}
			if (Annaulyeild >= 1000) {
				Double dblTemp0 = Annaulyeild / 1000;
				objSiteViewBean.setAnnaulayeild(String.format("%.2f", dblTemp0) + " MWh");

			} else if (Annaulyeild >= 100000) {
				Double dblTemp0 = Annaulyeild / 1000000;
				objSiteViewBean.setAnnaulayeild(String.format("%.2f", dblTemp0) + " GWh");
			} else {
				objSiteViewBean.setAnnaulayeild(String.format("%.2f", Annaulyeild) + " kWh");

			}

			SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd");
			String ChartJsonData1 = "";
			String ChartJsonData1Parameter = "";
			String ChartJsonData1GraphValue = "";

			for (int a = 0; a < lstSiteSummary.size(); a++) {

				String varChartDate = sdfchart.format(lstSiteSummary.get(a).getTimestamp()).toString();
				long millis = lstSiteSummary.get(a).getTimestamp().getTime();
				varChartDate = String.valueOf(millis);

				Double dbTodayEnergy = lstSiteSummary.get(a).getTodayEnergy();
				String varChartValue = "";

				varChartValue = String.format("%.2f", dbTodayEnergy);

				// String varChartValue = String.format("%.2f", dbTodayEnergy * 1000 * 1000);

				if (a == 0) {
					ChartJsonData1 = "[" + varChartDate + "," + varChartValue + "]";
					// ChartJsonData1="{\"date\": \"" + varChartDate + "\",\"value1\":\"" +
					// varChartValue + "\"}";
					// ChartJsonData1="{\"date\": \"" + varChartDate + "\",\"value1\":" +
					// varChartValue + "}";
				} else {
					ChartJsonData1 = ChartJsonData1 + ",[" + varChartDate + "," + varChartValue + "]";

					// ChartJsonData1 = ChartJsonData1 + ",{\"date\": \"" + varChartDate +
					// "\",\"value1\":" + varChartValue + "}";
					// ChartJsonData1 = ChartJsonData1 + ",{\"date\": \"" + varChartDate +
					// "\",\"value1\":\"" + varChartValue + "\"}";
				}

			}

			ChartJsonData1Parameter = "{\"fromField\": \"value1\",\"toField\": \"value1\"}";
			// ChartJsonData1GraphValue = "{\"id\": \"g1\",\"valueField\":
			// \"value1\",\"comparable\": true,\"lineThickness\": 2,\"title\":\"Daywise
			// Today Energy (kWh) \",\"type\": \"smoothedLine\",\"useDataSetColors\":
			// false,\"balloonText\": \"Today
			// Energy:<b>[[value1]]</b>\",\"compareGraphBalloonText\": \"Total
			// Energy:<b>[[value1]]</b>\"}";
			ChartJsonData1GraphValue = "{\"id\": \"g1\",\"valueField\": \"value1\",\"comparable\": true,\"lineThickness\": 2,\"title\":\"Daywise Today Energy (kWh)  \",\"type\": \"column\",\"useDataSetColors\": false,\"balloonText\": \"Today Energy:<b>[[value1]]</b>\",\"compareGraphBalloonText\": \"Total Energy:<b>[[value1]]</b>\"}";

			ChartJsonData1 = "([" + ChartJsonData1 + "])";
			// ChartJsonData1 = "[" + ChartJsonData1 + "]";
			ChartJsonData1Parameter = "[" + ChartJsonData1Parameter + "]";
			ChartJsonData1GraphValue = "[" + ChartJsonData1GraphValue + "]";

			objSiteViewBean.setChartJsonData1(ChartJsonData1);
			objSiteViewBean.setChartJsonData1Parameter(ChartJsonData1Parameter);
			objSiteViewBean.setChartJsonData1GraphValue(ChartJsonData1GraphValue);

			// List<EquipmentStatus> lstEquipmentStatus =
			// equipmentStatusService.getSiteStatusListByUserId(id);

			AccessListBean objAccessListBean = new AccessListBean();
			List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
			List<String> objactivitys = new ArrayList<>();

			StringBuilder stringBuilder = new StringBuilder();

			try {
				User objUser = userService.getUserById(refid);

				lstRoleActivity = roleActivityService.listRoleActivities().stream()
						.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

				objAccessListBean.setUserID(refid);
				objAccessListBean.setUserName(objUser.getShortName());
				// MyCustomerID = objSite.getCustomerID();
				objAccessListBean.setMyCustomerID(objSite.getCustomerID());

				Customer objCustomer = customerService.getCustomerById(objSite.getCustomerID());
				if (objCustomer != null) {
					objAccessListBean.setMyCustomerName(objCustomer.getCustomerName());
				} else {
					objAccessListBean.setMyCustomerName("Customer View");
				}

				if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
					objAccessListBean.setCustomerListView("visible");
				}
				if (objUser.getRoleID() == 6) {

					objAccessListBean.setMonitoringView("visible");

					if (objSite.getSiteName().equals("Repal Renewables, Komatikuntla")) {
						objSiteViewBean.setLocationName("Demo Site");

					}
					if (objAccessListBean.getMyCustomerName().equals("VEH")) {
						objAccessListBean.setMyCustomerName("Demo Customer");

					}

					tile1 = "siteview_sitedetails";
					tile2 = "siteview_totalenergy";
					tile3 = "siteview_scheduledjobs";
					tile4 = "siteview_energy";
					tile5 = "siteview_events";
					tile6 = "siteview_tickets";
					tile7 = "siteview_performancemetrics";
					tile8 = "siteview_sensors";
					tile9 = "siteview_location";
					tile10 = "siteview_equipments";
					tile11 = "siteview_weather";
					tile12 = "siteview_co2";
				}

				for (int l = 0; l < lstRoleActivity.size(); l++) {
					if (lstRoleActivity.get(l).getActiveFlag() == 1) {
						Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
						if (null != activity) {
							if (activity.getActivityDescription() != null) {
								stringBuilder.append(activity.getActivityDescription());
								stringBuilder.append("#");
							}
						}
					}

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			objSiteViewBean.setSiteID(String.valueOf(id));

			// model.addAttribute("datatransaction",lstDataTransaction);
			model.addAttribute("siteList", getDropdownList("Site", refid));
			model.addAttribute("userList", getDropdownList("User", refid));
			model.addAttribute("classtate", "active");
			model.addAttribute("access", objAccessListBean);
			model.addAttribute("accessPages", stringBuilder.toString());
			model.addAttribute("siteview", objSiteViewBean);
			model.addAttribute("siteviewEquipmentList", lstEquipmentList);
			model.addAttribute("siteviewEquipmentList1", lstEquipmentList1);
			model.addAttribute("siteviewEquipmentList2", lstEquipmentList2);
			model.addAttribute("siteviewEquipmentList3", lstEquipmentList3);
			model.addAttribute("tile1", tile1);
			model.addAttribute("tile2", tile2);
			model.addAttribute("tile3", tile3);
			model.addAttribute("tile4", tile4);
			model.addAttribute("tile5", tile5);
			model.addAttribute("tile6", tile6);
			model.addAttribute("tile7", tile7);
			model.addAttribute("tile8", tile8);
			model.addAttribute("tile9", tile9);
			model.addAttribute("tile10", tile10);
			model.addAttribute("tile11", tile11);
			model.addAttribute("tile12", tile12);
			model.addAttribute("ticketcreation", new TicketDetail());

			return "siteview";
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/login";
			// TODO: handle exception
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns value for dropdown.
	 * 
	 * Input Params : DropdownName,refid
	 * 
	 * Return Value : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "User") {
			List<User> ddlList = userService.listFieldUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : readAll
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns string from a reader.
	 * 
	 * Input Params : rd
	 * 
	 * Return Value : String
	 * 
	 * 
	 **********************************************************************************************/
	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	/********************************************************************************************
	 * 
	 * Function Name : readJsonFromUrl
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns json from url.
	 * 
	 * Input Params : url
	 * 
	 * Return Value : JSONObject
	 * 
	 * Exceptions : IOException,JSONException
	 * 
	 * 
	 **********************************************************************************************/
	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONObject json = new JSONObject(jsonText);
			return json;
		} finally {
			is.close();
		}
	}

	/********************************************************************************************
	 * 
	 * Function Name : callfunction
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function reads url and prints weather data.
	 * 
	 * Return Value : void
	 * 
	 * 
	 **********************************************************************************************/
	public static void callfunction() throws IOException, JSONException {
		JSONObject json = readJsonFromUrl(
				"http://api.openweathermap.org/data/2.5/weather?lat=19.111976&lon=72.911129&appid=6a977327fea2cb3bf2aae26c2c0474cb");
		// System.out.println(json.toString());
		System.out.println(json.get("main"));
		System.out.println(json.get("weather"));
		System.out.println(json.get("wind"));
		String name = "main";
		String Temp = "";
		String Weather = "";
		String Windspeed = "";
		if (json.get("main").toString().contains("\"temp\":") == true) {
			Temp = json.getJSONObject("main").getString("temp");
		}

		if (json.get("wind").toString().contains("\"speed\":") == true) {
			Windspeed = json.getJSONObject("wind").getString("speed");
		}
		if (json.get("weather").toString().contains("\"main\":") == true) {
			Weather = json.getJSONArray("weather").getJSONObject(0).getString("main");
		}
		System.out.println(Temp);
		System.out.println(Weather);
		System.out.println(Windspeed);

	}

}
