
/******************************************************
 * 
 *    	Filename	: CustomerController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Controller is for Customer related functions.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.CustomerBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.CustomerType;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.State;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.CustomerTypeService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.StateService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class CustomerController {

	@Autowired
	private CustomerService customerService;
	@Autowired
	private CustomerTypeService customertypeService;
	@Autowired
	private CountryService countryService;
	@Autowired
	private StateService stateService;

	// @Autowired
	// private Constant constant;

	@Autowired
	private Environment environment;

	@Autowired
	private UserService userService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : GetCastedGraphDataBeanList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads data for customer page.
	 * 
	 * Input Params : session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customerpageload", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> CustomerPageLoad(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();
				objResponseEntity.put("customerList", getUpdatedCustomerList());
				objResponseEntity.put("customertypeList", getDropdownList("CustomerType"));
				objResponseEntity.put("primarycustomerList", getDropdownList("Customer"));
				objResponseEntity.put("customergroupList", getDropdownList("CustomerGroup"));
				objResponseEntity.put("countryList", getDropdownList("Country"));
				objResponseEntity.put("stateList", getDropdownList("State"));

				objResponseEntity.put("activeStatusList", getStatusDropdownList());
				objResponseEntity.put("siteList", getDropdownList("Site"));

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : CustomerGrid
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns value for
	 * 
	 * Input Params : session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customergrid", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> CustomerGrid(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			objResponseEntity.put("status", true);
			objResponseEntity.put("responsedata", "Success");
			objResponseEntity.put("data", getUpdatedCustomerList());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("responsedata", e.getMessage());
			objResponseEntity.put("data", null);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : NewCustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function saves customer details with image.
	 * 
	 * Input Params : jsonData,image,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : ConstraintViolationException,Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/newcustomerwithimage", method = RequestMethod.POST)

	public ResponseEntity<Map<String, Object>> NewCustomer(@RequestParam("data") String jsonData,
			@RequestParam("image") MultipartFile image, HttpSession session, Authentication authentication)
			throws IOException {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String ResponseMessage = "";
		String Dataexists1 = "";
		String Dataexists2 = "";

		try {

			System.out.println(jsonData);
			Gson googleJson = new Gson();
			Type type = new TypeToken<List<CustomerBean>>() {
			}.getType();

			List<CustomerBean> lstcustomer = googleJson.fromJson(jsonData, type);

			System.out.println(lstcustomer.size());

			System.out.println(lstcustomer.get(0).getCustomerName());

			if (lstcustomer != null) {
				CustomerBean customer = lstcustomer.get(0);

				if (customer != null) {
					System.out.println("Customer Name :: " + customer.getCustomerName());

					Date podate = new SimpleDateFormat("dd/MM/yyyy").parse(customer.getCustomerPODateText());

					Date date = new Date();

					if (customer.getCustomerId() == null) {

						customer.getCustomerName();
						List<Customer> lstcustomers = customerService.listallcustomers(0);

						for (int i = 0; i < lstcustomers.size(); i++) {
							String Customername = lstcustomers.get(i).getCustomerName();
							String Customerreference = lstcustomers.get(i).getCustomerReference();

							if (customer.getCustomerName().equals(Customername)
									&& customer.getCustomerReference().equals(Customerreference)) {

								Dataexists1 = "CustomerName and CustomerReference Number Already Exist";
								Dataexists2 = "CustomerReference Number Already Exist";
								System.out.println("Entered in this loop");
								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);
								objResponseEntity.put("Errorlog2", Dataexists2);
								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							} else if (customer.getCustomerName().equals(Customername)
									|| customer.getCustomerReference().equals(Customerreference)) {

								Dataexists1 = "CustomerName or CustomerReference Number Already Exist";
								Dataexists2 = "CustomerReference Number Already Exist";
								System.out.println("Entered in this loop");
								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);
								objResponseEntity.put("Errorlog2", Dataexists2);
								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						Customer customer1 = new Customer();

						File file = new File(
								environment.getProperty("file.filepath") + customer.getCustomerReference());
						if (!file.exists()) {
							if (file.mkdir()) {

							} else {

							}
						}

						BufferedImage image1 = ImageIO.read(image.getInputStream());
						int width = image1.getWidth();
						int height = image1.getHeight();

						float newHeight = (float) width / height;

						if (newHeight <= 1.3) {
							customer1.setImageType(1);

						} else {

							customer1.setImageType(0);
						}

						if (!image.getOriginalFilename().isEmpty()) {
							BufferedOutputStream outputStream = new BufferedOutputStream(
									new FileOutputStream(new File(file, image.getOriginalFilename())));
							outputStream.write(image.getBytes());
							outputStream.flush();
							outputStream.close();
							System.out.println("Sucess");
						}

						customer1.setCustomerName(customer.getCustomerName());
						customer1.setActiveFlag(customer.getActiveFlag());
						customer1.setCustomerCode(getCustomerCode());

						customer1.setCustomerTypeID(customer.getCustomerTypeID());
						customer1.setCustomerOrganization(customer.getCustomerOrganization());
						customer1.setCity(customer.getCity());
						customer1.setStateID(customer.getStateID());
						customer1.setCountryID(customer.getCountryID());

						customer.setPostalCode(customer.getPostalCode());

						if (customer.getContactPerson() != null) {
							customer1.setContactPerson(customer.getContactPerson());
						}
						if (customer.getCustomerDescription() != null) {
							customer1.setCustomerDescription(customer.getCustomerDescription());
						}
						if (customer.getCustomerGroup() != null) {
							customer1.setCustomerGroup(customer.getCustomerGroup());
						}
						if (!customer.getPrimaryCustomer().equals("") && customer.getPrimaryCustomer() != null) {
							customer1.setPrimaryCustomer(Integer.valueOf(customer.getPrimaryCustomer()));
						}
						if (customer.getWebsite() != null) {
							customer1.setWebsite(customer.getWebsite());
						}
						if (customer.getCustomerPONumber() != null) {
							customer1.setCustomerPONumber(customer.getCustomerPONumber());
						}
						if (customer.getPrimaryCustomer().equals("Secondary") && customer.getCustomerGroup() != null) {
							customer1.setPrimaryCustomerCode(getPrimaryCustomerCode());
						}

						customer1.setEmailID(customer.getEmailID());
						customer1.setMobile(customer.getMobile());
						customer1.setTelephone(customer.getTelephone());

						customer1.setAddress(customer.getAddress());

						customer1.setCustomerReference(customer.getCustomerReference());

						customer1.setCreationDate(date);
						customer1.setCreatedBy(id);

						String logpath = environment.getProperty("file.filepath") + customer.getCustomerReference()
								+ "/" + image.getOriginalFilename();

						customer1.setCustomerLogo(logpath);

						customer1.setImagePath(environment.getProperty("file.filename")
								+ customer.getCustomerReference() + "/" + image.getOriginalFilename());

						customerService.addCustomer(customer1);

					} else {
						Customer objcustomer = customerService
								.getCustomerById((Integer.valueOf(customer.getCustomerId())));

						List<Customer> lstcustomers = customerService
								.listallcustomers(Integer.valueOf(customer.getCustomerId()));

						for (int i = 0; i < lstcustomers.size(); i++) {
							String Customername = lstcustomers.get(i).getCustomerName();
							String Customerreference = lstcustomers.get(i).getCustomerReference();

							if (customer.getCustomerName().equals(Customername)
									&& customer.getCustomerReference().equals(Customerreference)) {

								Dataexists1 = "CustomerName and CustomerReference Number Already Exist";
								Dataexists2 = "CustomerReference Number Already Exist";
								System.out.println("Entered in this loop");
								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);
								objResponseEntity.put("Errorlog2", Dataexists2);
								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							} else if (customer.getCustomerName().equals(Customername)
									|| customer.getCustomerReference().equals(Customerreference)) {

								Dataexists1 = "CustomerName or CustomerReference Number Already Exist";
								Dataexists2 = "CustomerReference Number Already Exist";
								System.out.println("Entered in this loop");
								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);
								objResponseEntity.put("Errorlog2", Dataexists2);
								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						File file = new File(
								environment.getProperty("file.filepath") + customer.getCustomerReference());
						if (!file.exists()) {
							if (file.mkdir()) {
								System.out.println("Directory is created!");
							} else {
								System.out.println("Failed to create directory!");
							}
						}

						BufferedImage image1 = ImageIO.read(image.getInputStream());
						int width = image1.getWidth();
						int height = image1.getHeight();

						float newHeight = (float) width / height;

						System.out.println(newHeight);

						if (newHeight <= 1.3) {
							objcustomer.setImageType(1);
							System.out.println(objcustomer.getImageType());
						} else {
							System.out.println(newHeight);
							objcustomer.setImageType(0);
						}

						if (!image.getOriginalFilename().isEmpty()) {
							BufferedOutputStream outputStream = new BufferedOutputStream(
									new FileOutputStream(new File(file, image.getOriginalFilename())));
							outputStream.write(image.getBytes());
							outputStream.flush();
							outputStream.close();
							System.out.println("Sucess");
						}

						objcustomer.setCustomerTypeID(customer.getCustomerTypeID());
						objcustomer.setCustomerName((customer.getCustomerName()));
						objcustomer.setCustomerOrganization(customer.getCustomerOrganization());

						if (!customer.getPrimaryCustomer().equals("") && customer.getPrimaryCustomer() != null) {
							objcustomer.setPrimaryCustomer(Integer.valueOf(customer.getPrimaryCustomer()));
						}

						if (customer.getContactPerson() != null && !customer.getContactPerson().equals("")) {
							objcustomer.setContactPerson(customer.getContactPerson());
						}
						if (customer.getCustomerDescription() != null
								&& !customer.getCustomerDescription().equals("")) {
							objcustomer.setCustomerDescription(customer.getCustomerDescription());

						}
						if (customer.getCustomerGroup() != null && !customer.getCustomerGroup().equals("")) {
							objcustomer.setCustomerGroup(customer.getCustomerGroup());
						}

						if (customer.getWebsite() != null && !customer.getWebsite().equals("")) {
							objcustomer.setWebsite(customer.getWebsite());
						}
						if (customer.getCustomerPONumber() != null && !customer.getCustomerPONumber().equals("")) {
							objcustomer.setCustomerPONumber(customer.getCustomerPONumber());

						}

						objcustomer.setAddress(customer.getAddress());

						objcustomer.setCity(customer.getCity());

						objcustomer.setStateID(customer.getStateID());
						objcustomer.setCountryID(customer.getCountryID());
						objcustomer.setPostalCode(customer.getPostalCode());
						objcustomer.setEmailID(customer.getEmailID());
						objcustomer.setMobile(customer.getMobile());
						objcustomer.setTelephone(customer.getTelephone());

						objcustomer.setFax(customer.getFax());

						objcustomer.setCustomerPODate(podate);
						objcustomer.setCustomerReference(customer.getCustomerReference());

						objcustomer.setActiveFlag(customer.getActiveFlag());

						String logpath = environment.getProperty("file.filepath") + customer.getCustomerReference()
								+ "/" + image.getOriginalFilename();

						objcustomer.setCustomerLogo(logpath);

						objcustomer.setImagePath(environment.getProperty("file.filename")
								+ customer.getCustomerReference() + "/" + image.getOriginalFilename());

						objcustomer.setLastUpdatedDate(date);
						objcustomer.setLastUpdatedBy(id);

						customerService.updateCustomer(objcustomer);

						objResponseEntity.put("status", true);
						objResponseEntity.put("customer", objcustomer);
						objResponseEntity.put("data", "Success");
						return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

					}
					objResponseEntity.put("status", true);
					objResponseEntity.put("customer", customer);
					ResponseMessage = "Success";
				} else {
					objResponseEntity.put("status", false);
					ResponseMessage = "Invalid Request";
					objResponseEntity.put("customer", null);
				}

			} else {
				objResponseEntity.put("status", false);
				ResponseMessage = "Invalid Request";
				objResponseEntity.put("customer", null);
			}

			objResponseEntity.put("data", ResponseMessage);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (ConstraintViolationException cve) {
			objResponseEntity.put("customer", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", cve.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			objResponseEntity.put("customer", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : Customer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This method saves new customer.
	 * 
	 * Input Params : lstcustomer,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : ConstraintViolationException,Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/newcustomer", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> Customer(@RequestBody List<Customer> lstcustomer, HttpSession session,
			Authentication authentication) {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String ResponseMessage = "";
		String Dataexists1 = "";
		String Dataexists2 = "";

		try {

			if (lstcustomer != null) {
				Customer customer = lstcustomer.get(0);

				if (customer != null) {
					System.out.println("Customer Name :: " + customer.getCustomerName());

					Date podate = new SimpleDateFormat("dd/MM/yyyy").parse(customer.getCustomerPODateText());

					Date date = new Date();

					if (customer.getCustomerId() == null || customer.getCustomerId() == 0) {

						customer.getCustomerName();
						List<Customer> lstcustomers = customerService.listallcustomers(0);

						for (int i = 0; i < lstcustomers.size(); i++) {
							String Customername = lstcustomers.get(i).getCustomerName();
							String Customerreference = lstcustomers.get(i).getCustomerReference();

							if (customer.getCustomerName().equals(Customername)
									&& customer.getCustomerReference().equals(Customerreference)) {

								Dataexists1 = "CustomerName and CustomerReference Number Already Exist";
								Dataexists2 = "CustomerReference Number Already Exist";
								System.out.println("Entered in this loop");
								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);
								objResponseEntity.put("Errorlog2", Dataexists2);
								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							} else if (customer.getCustomerName().equals(Customername)
									|| customer.getCustomerReference().equals(Customerreference)) {

								Dataexists1 = "CustomerName or CustomerReference Number Already Exist";
								Dataexists2 = "CustomerReference Number Already Exist";
								System.out.println("Entered in this loop");
								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);
								objResponseEntity.put("Errorlog2", Dataexists2);
								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						customer.setCustomerCode(getCustomerCode());
						customer.setCreationDate(date);
						customer.setCustomerPODate(podate);
						customer.setActiveFlag(1);
						customer.setCreatedBy(id);
						customer.setCustomerLogo("E:/ApacheTomcat/webapps/pdf/inspire1.png");
						customer.setImagePath(environment.getProperty("file.filename") + "inspire1.png");

						if (customer.getPrimaryCustomer() != null && customer.getCustomerGroup().equals("Secondary")) {
							customer.setPrimaryCustomerCode(getPrimaryCustomerCode());
						}

						customerService.addCustomer(customer);

					} else {
						Customer objcustomer = customerService.getCustomerById((customer.getCustomerId()));

						List<Customer> lstcustomers = customerService.listallcustomers(customer.getCustomerId());

						for (int i = 0; i < lstcustomers.size(); i++) {
							String Customername = lstcustomers.get(i).getCustomerName();
							String Customerreference = lstcustomers.get(i).getCustomerReference();

							if (customer.getCustomerName().equals(Customername)
									&& customer.getCustomerReference().equals(Customerreference)) {

								Dataexists1 = "CustomerName and CustomerReference Number Already Exist";
								Dataexists2 = "CustomerReference Number Already Exist";
								System.out.println("Entered in this loop");
								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);
								objResponseEntity.put("Errorlog2", Dataexists2);
								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							} else if (customer.getCustomerName().equals(Customername)
									|| customer.getCustomerReference().equals(Customerreference)) {

								Dataexists1 = "CustomerName or CustomerReference Number Already Exist";
								Dataexists2 = "CustomerReference Number Already Exist";
								System.out.println("Entered in this loop");
								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);
								objResponseEntity.put("Errorlog2", Dataexists2);
								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						objcustomer.setCustomerTypeID(customer.getCustomerTypeID());
						objcustomer.setCustomerName((customer.getCustomerName()));
						objcustomer.setCustomerOrganization(customer.getCustomerOrganization());
						objcustomer.setCustomerDescription(customer.getCustomerDescription());

						objcustomer.setCustomerGroup(customer.getCustomerGroup());
						objcustomer.setPrimaryCustomer(customer.getPrimaryCustomer());

						objcustomer.setContactPerson(customer.getContactPerson());
						objcustomer.setAddress(customer.getAddress());

						objcustomer.setCity(customer.getCity());

						objcustomer.setStateID(customer.getStateID());
						objcustomer.setCountryID(customer.getCountryID());
						objcustomer.setPostalCode(customer.getPostalCode());
						objcustomer.setEmailID(customer.getEmailID());
						objcustomer.setMobile(customer.getMobile());
						objcustomer.setTelephone(customer.getTelephone());

						objcustomer.setFax(customer.getFax());
						objcustomer.setWebsite(customer.getWebsite());
						objcustomer.setCustomerPONumber(customer.getCustomerPONumber());
						objcustomer.setCustomerPODate(podate);
						objcustomer.setCustomerReference(customer.getCustomerReference());

						objcustomer.setActiveFlag(customer.getActiveFlag());

						if (objcustomer.getCustomerLogo() == null) {
							objcustomer.setCustomerLogo("E:/ApacheTomcat/webapps/pdf/inspire1.png");
							objcustomer.setImagePath(environment.getProperty("file.filename") + "inspire1.png");
						}

						// objcustomer.setCustomerLogo(customer.getCustomerLogo());

						objcustomer.setLastUpdatedDate(date);
						objcustomer.setLastUpdatedBy(id);

						customerService.updateCustomer(objcustomer);

						objResponseEntity.put("status", true);
						objResponseEntity.put("customer", objcustomer);
						objResponseEntity.put("data", "Success");
						return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

					}
					objResponseEntity.put("status", true);
					objResponseEntity.put("customer", customer);
					ResponseMessage = "Success";
				} else {
					objResponseEntity.put("status", false);
					ResponseMessage = "Invalid Request";
					objResponseEntity.put("customer", null);
				}

			} else {
				objResponseEntity.put("status", false);
				ResponseMessage = "Invalid Request";
				objResponseEntity.put("customer", null);
			}

			objResponseEntity.put("data", ResponseMessage);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (ConstraintViolationException cve) {
			objResponseEntity.put("customer", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", cve.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			objResponseEntity.put("customer", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns value for dropdown list.
	 * 
	 * Return Value : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns data for dropdown list.
	 * 
	 * Input Params : DropdownName,refid
	 * 
	 * Return Value : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns data for dropdown list.
	 * 
	 * Input Params : DropdownName
	 * 
	 * Return Value : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Customer") {
			List<Customer> ddlList = customerService.listCustomers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getCustomerId().toString(), ddlList.get(i).getCustomerName());
			}

		} else if (DropdownName == "PrimaryCustomer") {
			List<Customer> ddlList = customerService.listprimarycustomers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getCustomerId().toString(), ddlList.get(i).getCustomerName());
			}

		}

		else if (DropdownName == "CustomerType") {
			List<CustomerType> ddlList = customertypeService.listCustomerTypes();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getCustomerTypeId().toString(), ddlList.get(i).getCustomerType());
			}

		} else if (DropdownName == "CustomerGroup") {
			ddlMap.put("Primary", "Primary");
			ddlMap.put("Secondary", "Secondary");

		} else if (DropdownName == "Country") {
			List<Country> ddlList = countryService.listCountries();
			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getCountryId().toString(), ddlList.get(i).getCountryName());
			}
		} else if (DropdownName == "State") {
			List<State> ddlList = stateService.listStates();
			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getStateId().toString(), ddlList.get(i).getStateName());
			}
		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getUpdatedCustomerList
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function retrns list of customers.
	 * 
	 * Return Value : List<Customer>
	 * 
	 * 
	 **********************************************************************************************/
	public List<Customer> getUpdatedCustomerList() {

		// List<Customer> ddlCustomersList = customerService.listCustomers();

		List<Customer> ddlCustomersList = customerService.listConfigCustomers();

		List<Customer> ddlPrimaryCustomersList = customerService.listConfigCustomers();
		List<CustomerType> ddlCustomerTypeList = customertypeService.listCustomerTypes();
		List<Country> ddlCountryList = countryService.listCountries();
		List<State> ddlStateList = stateService.listStates();

		for (int i = 0; i < ddlCustomersList.size(); i++) {
			List<Customer> lstPrimaryCustomerList = new ArrayList<Customer>();
			List<CustomerType> lstCustomerTypeList = new ArrayList<CustomerType>();
			List<Country> lstCountryList = new ArrayList<Country>();
			List<State> lstStateList = new ArrayList<State>();

			int pcid = ddlCustomersList.get(i).getCustomerId();
			int ctype = ddlCustomersList.get(i).getCustomerTypeID();
			int countryid = ddlCustomersList.get(i).getCountryID();
			int stateid = ddlCustomersList.get(i).getStateID();
			int cid = 0;

			if (ddlCustomersList.get(i).getPrimaryCustomer() != null) {
				pcid = ddlCustomersList.get(i).getPrimaryCustomer().intValue();
			}

			lstPrimaryCustomerList = ddlPrimaryCustomersList.stream().filter(p -> p.getCustomerId() == cid)
					.collect(Collectors.toList());
			lstCustomerTypeList = ddlCustomerTypeList.stream().filter(p -> p.getCustomerTypeId() == ctype)
					.collect(Collectors.toList());
			lstCountryList = ddlCountryList.stream().filter(p -> p.getCountryId() == countryid)
					.collect(Collectors.toList());
			lstStateList = ddlStateList.stream().filter(p -> p.getStateId() == stateid).collect(Collectors.toList());

			if (lstPrimaryCustomerList.size() > 0) {
				ddlCustomersList.get(i).setPrimaryCustomerName(lstPrimaryCustomerList.get(0).getPrimaryCustomerName());
			}

			if (lstCustomerTypeList.size() > 0) {
				ddlCustomersList.get(i).setCustomerTypeName(lstCustomerTypeList.get(0).getCustomerType());
			}

			if (lstCountryList.size() > 0) {
				ddlCustomersList.get(i).setCountryName(lstCountryList.get(0).getCountryName());
			}

			if (lstStateList.size() > 0) {
				ddlCustomersList.get(i).setStateName(lstStateList.get(0).getStateName());
			}

			if (ddlCustomersList.get(i).getCustomerGroup().equalsIgnoreCase("Secondary")) {
				List<Customer> customer = customerService
						.getPrimaryCustomerById(ddlCustomersList.get(i).getPrimaryCustomer().toString());
				ddlCustomersList.get(i).setCustomerCode(
						customer.get(0).getCustomerCode() + " - " + ddlCustomersList.get(i).getPrimaryCustomerCode());

			} else {
				ddlCustomersList.get(i).setCustomerCode(ddlCustomersList.get(i).getCustomerCode());

			}

		}

		return ddlCustomersList;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getCustomerCode
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns customer code.
	 * 
	 * Return Value : String
	 * 
	 * 
	 **********************************************************************************************/
	public String getCustomerCode() {
		Customer objCust = new Customer();
		objCust = customerService.getCustomerByMax("CustomerCode");

		String CR = "";

		if (objCust != null) {
			CR = objCust.getCustomerCode();
		}

		if (CR == null || CR == "") {
			CR = "C1001";
		} else {
			try {
				int NextIndex = Integer.parseInt(CR.replaceAll("C1", "")) + 1;

				if (NextIndex >= 100) {
					CR = String.valueOf(NextIndex);
				} else if (NextIndex >= 10) {
					CR = "0" + String.valueOf(NextIndex);
				} else {
					CR = "00" + String.valueOf(NextIndex);
				}

				CR = "C1" + CR;
			} catch (Exception e) {
				CR = "C1001";
			}
		}

		return CR;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getPrimaryCustomerCode
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns primary cutomer code.
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	public String getPrimaryCustomerCode() {
		Customer objCust = new Customer();
		objCust = customerService.getPrimaryCustomerByMax("PrimaryCustomerCode");

		String CR = "";

		if (objCust != null) {
			CR = objCust.getPrimaryCustomerCode();
		}

		if (CR == null || CR == "") {
			CR = "S001";
		} else {
			try {
				int NextIndex = Integer.parseInt(CR.replaceAll("S", "")) + 1;

				if (NextIndex >= 100) {
					CR = String.valueOf(NextIndex);
				} else if (NextIndex >= 10) {
					CR = "0" + String.valueOf(NextIndex);
				} else {
					CR = "00" + String.valueOf(NextIndex);
				}

				CR = "S" + CR;
			} catch (Exception e) {
				CR = "S001";
			}
		}

		return CR;
	}

	/********************************************************************************************
	 * 
	 * Function Name : listCustomers
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function loads the customer page.
	 * 
	 * Input Params : model,session,authentication
	 * 
	 * Return Value : String
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/customers", method = RequestMethod.GET)
	public String listCustomers(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("customer", new Customer());
		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());

		model.addAttribute("siteList", getNewTicketSiteList("Site", id));
		model.addAttribute("customerList", getUpdatedCustomerList());
		model.addAttribute("customertypeList", getDropdownList("CustomerType"));
		model.addAttribute("primarycustomerList", getDropdownList("PrimaryCustomer"));
		model.addAttribute("customergroupList", getDropdownList("CustomerGroup"));
		model.addAttribute("countryList", getDropdownList("Country"));
		model.addAttribute("stateList", getDropdownList("State"));
		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("activeStatusList", getStatusDropdownList());

		return "customer";
	}

	/********************************************************************************************
	 * 
	 * Function Name : CustomerUpdation
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function updates a customer.
	 * 
	 * Input Params : CustomerId,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/editcustomer", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> CustomerUpdation(Integer CustomerId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();
			Customer objCustomer = customerService.getCustomerById(CustomerId);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			if (objCustomer.getCustomerPODate() != null) {
				objCustomer.setCustomerPODateText(sdf.format(objCustomer.getCustomerPODate()));
			}

			objResponseEntity.put("customer", objCustomer);
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getstates
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns all states.
	 * 
	 * Input Params : CountryID,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getstates", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> getstates(Integer CountryID, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();
			List<State> objState = stateService.listAllStatesByCountryID(CountryID);

			objResponseEntity.put("state", objState);
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getprimarycustomer
	 * 
	 * Author : Sarath Babu E
	 * 
	 * Time Stamp : 29-Apr-19 09:47 AM
	 * 
	 * Description : This function returns primary customer.
	 * 
	 * Input Params : Group,session,authentication
	 * 
	 * Return Value : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getprimarycustomer", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> getprimarycustomer(String Group, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();
			List<Customer> objCustomer = null;
			if (Group.equals("Secondary")) {
				objCustomer = customerService.listprimarycustomers();
			} else {

			}

			objResponseEntity.put("customerlist", objCustomer);
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

}
