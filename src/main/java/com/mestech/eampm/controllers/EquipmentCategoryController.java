
/******************************************************
 * 
 *    	Filename	: EquipmentCategoryController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Controller deals with Equipment categoty activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentCategory;
import com.mestech.eampm.model.EquipmentType;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteMap;
import com.mestech.eampm.model.SiteType;
import com.mestech.eampm.model.State;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.EquipmentCategoryService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteTypeService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.Utility;
import com.mestech.eampm.utility.UtilityCommon;
import com.mestech.eampm.utility.UtilityCommonDAO;

@Controller
public class EquipmentCategoryController {

	@Autowired
	private EquipmentCategoryService equipmentcategoryService;

	@Autowired
	private SiteTypeService sitetypeService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();
	private UtilityCommon utilityCommon = new UtilityCommon();

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentCategoryGrid
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns equipment category.
	 * 
	 * Input Params  : session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/equipmentcategory", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> EquipmentCategoryGrid(HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}

				objResponseEntity.put("equipmentcategoryList", getEquipmentCategoryList(timezonevalue));

				objResponseEntity.put("activeStatusList", getStatusDropdownList());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentType
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function create new equipment category
	 * 
	 * Input Params  : lstequipmentcategory,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions    : ConstraintViolationException,Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/newequipmentcategory", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> EquipmentType(@RequestBody List<EquipmentCategory> lstequipmentcategory,
			HttpSession session, Authentication authentication) throws ServletException {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String ResponseMessage = "";
		String Dataexists1 = "";
		String Dataexists2 = "";

		try {

			if (lstequipmentcategory != null) {
				EquipmentCategory equipmentcategory = lstequipmentcategory.get(0);

				if (equipmentcategory != null) {

					Date date = new Date();
					if (equipmentcategory.getCategoryId() == null || equipmentcategory.getCategoryId() == 0) {

						List<EquipmentCategory> lstequipmentcategories = equipmentcategoryService
								.listEquipmentCategorys(0);
						for (int i = 0; i < lstequipmentcategories.size(); i++) {
							String categoryname = lstequipmentcategories.get(i).getEquipmentCategory();

							if (equipmentcategory.getEquipmentCategory().equals(categoryname)) {

								Dataexists1 = "Category Name Already Exists";

								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						equipmentcategory.setEquipmentCategoryCode(getEquipmentCategoryCode());
						equipmentcategory.setCreationDate(date);
						equipmentcategory.setCreatedBy(id);

						equipmentcategoryService.addEquipmentCategory(equipmentcategory);
					} else {

						EquipmentCategory objEquipmentCategory = equipmentcategoryService
								.getEquipmentCategoryById(equipmentcategory.getCategoryId());

						List<EquipmentCategory> lstequipmentcategories = equipmentcategoryService
								.listEquipmentCategorys(equipmentcategory.getCategoryId());
						for (int i = 0; i < lstequipmentcategories.size(); i++) {
							String categoryname = lstequipmentcategories.get(i).getEquipmentCategory();

							if (equipmentcategory.getEquipmentCategory().equals(categoryname)) {

								Dataexists1 = "Category Name Already Exists";

								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}
						objEquipmentCategory.setEquipmentCategory(equipmentcategory.getEquipmentCategory());
						objEquipmentCategory.setCategoryGroup(equipmentcategory.getCategoryGroup());
						objEquipmentCategory.setCategoryDescription(equipmentcategory.getCategoryDescription());

						objEquipmentCategory.setActiveFlag(equipmentcategory.getActiveFlag());
						objEquipmentCategory.setRemarks(equipmentcategory.getRemarks());

						objEquipmentCategory.setApprovedBy(equipmentcategory.getApprovedBy());

						objEquipmentCategory.setLastUpdatedDate(date);
						objEquipmentCategory.setLastUpdatedBy(id);

						equipmentcategoryService.updateEquipmentCategory(objEquipmentCategory);

						objResponseEntity.put("status", true);
						objResponseEntity.put("equipmentcategory", objEquipmentCategory);
						objResponseEntity.put("data", "Success");
						return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

					}
					objResponseEntity.put("status", true);
					objResponseEntity.put("equipmentcategory", equipmentcategory);
					ResponseMessage = "Success";
				} else {
					objResponseEntity.put("status", false);
					ResponseMessage = "Invalid Request";
					objResponseEntity.put("equipmentcategory", null);
				}

			} else {
				objResponseEntity.put("status", false);
				ResponseMessage = "Invalid Request";
				objResponseEntity.put("equipmentcategory", null);
			}

			objResponseEntity.put("data", ResponseMessage);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (ConstraintViolationException cve) {
			objResponseEntity.put("equipmentcategory", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", cve.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			objResponseEntity.put("equipmentcategory", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns data for dropdown list.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns data for dropdown list.
	 * 
	 * Input Params  : DropdownName
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "SiteType") {
			List<SiteType> ddlList = sitetypeService.listSiteTypes();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteTypeId().toString(), ddlList.get(i).getSiteType());

			}

		} else if (DropdownName == "Site") {
			List<Site> ddlList = siteService.listSites();

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				/*
				 * if(ddlList.get(i).getCustomerReference() != null) { SiteName = SiteName +
				 * " - " + ddlList.get(i).getCustomerReference(); }
				 */

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		} else if (DropdownName == "CategoryGroup") {

			ddlMap.put("DL", "DL");
			ddlMap.put("PV", " PV");
			ddlMap.put("IC", "IC");
			ddlMap.put("NC", "NC");
			ddlMap.put("WS", "WS");
			ddlMap.put("INV", "INV");
			ddlMap.put("AUX", "AUX");
			ddlMap.put("CAB", "CAB");
			ddlMap.put("SCB", "SCB");
			ddlMap.put("LTP", "LTP");
			ddlMap.put("HTP", "HTP");
			ddlMap.put("TRN", "TRN");
			ddlMap.put("SYC", "SYC");
			ddlMap.put("Tracker", "Tracker");

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getUpdatedEquipmentCategoryList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns equipment category list.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : List<EquipmentCategory>
	 * 
	 * 
	 **********************************************************************************************/
	public List<EquipmentCategory> getUpdatedEquipmentCategoryList() {
		List<EquipmentCategory> ddlEquipmentCategoriesList = equipmentcategoryService.listEquipmentCategories();

		for (int i = 0; i < ddlEquipmentCategoriesList.size(); i++) {
			String SiteTypeName = sitetypeService.getSiteTypeById(ddlEquipmentCategoriesList.get(i).getSiteTypeID())
					.getSiteType();
			ddlEquipmentCategoriesList.get(i).setSiteTypeName(SiteTypeName);

		}

		return ddlEquipmentCategoriesList;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getEquipmentCategoryList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns equipment category list.
	 * 
	 * Input Params  : timezonevalue
	 * 
	 * Return Value  : List<EquipmentCategory>
	 * 
	 * 
	 **********************************************************************************************/
	public List<EquipmentCategory> getEquipmentCategoryList(String timezonevalue) {
		List<EquipmentCategory> ddlEquipmentCategoriesList = equipmentcategoryService.listEquipmentCategories();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Calendar cal = Calendar.getInstance();
		for (int i = 0; i < ddlEquipmentCategoriesList.size(); i++) {

			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {

					if (ddlEquipmentCategoriesList.get(i).getCreationDate() != null) {

						cal.setTime(ddlEquipmentCategoriesList.get(i).getCreationDate());
						cal.add(Calendar.MINUTE, Integer.valueOf(timezonevalue));

						ddlEquipmentCategoriesList.get(i).setCreationDateText(sdf.format(cal.getTime()));

					}
					if (ddlEquipmentCategoriesList.get(i).getLastUpdatedDate() != null) {

						cal.setTime(ddlEquipmentCategoriesList.get(i).getLastUpdatedDate());
						cal.add(Calendar.MINUTE, Integer.valueOf(timezonevalue));
						ddlEquipmentCategoriesList.get(i).setLastUpdatedDateText(sdf.format(cal.getTime()));

					}
				}

				if (ddlEquipmentCategoriesList.get(i).getCreationDate() != null) {
					ddlEquipmentCategoriesList.get(i)
							.setCreationDateText(sdf.format(ddlEquipmentCategoriesList.get(i).getCreationDate()));

				}
				if (ddlEquipmentCategoriesList.get(i).getLastUpdatedDate() != null) {
					ddlEquipmentCategoriesList.get(i)
							.setLastUpdatedDateText(sdf.format(ddlEquipmentCategoriesList.get(i).getLastUpdatedDate()));

				}

			}

		}

		return ddlEquipmentCategoriesList;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getEquipmentCategoryCode
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns equipment code.
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	public String getEquipmentCategoryCode() {

		EquipmentCategory objEquipmentCategory = equipmentcategoryService
				.getEquipmentCategoryByMax("EquipmentCategoryCode");

		String EQ = "";

		if (objEquipmentCategory != null) {
			EQ = objEquipmentCategory.getEquipmentCategoryCode();
		}

		if (EQ == null || EQ == "") {
			EQ = "CI0001";
		} else {
			try {
				int NextIndex = Integer.parseInt(EQ.replaceAll("CI", "")) + 1;

				if (NextIndex >= 1000) {
					EQ = String.valueOf(NextIndex);
				} else if (NextIndex >= 100) {
					EQ = "0" + String.valueOf(NextIndex);
				} else if (NextIndex >= 10) {
					EQ = "00" + String.valueOf(NextIndex);
				} else {
					EQ = "000" + String.valueOf(NextIndex);
				}

				EQ = "CI" + EQ;
			} catch (Exception e) {
				EQ = "CI0001";
			}
		}

		return EQ;
	}

	/********************************************************************************************
	 * 
	 * Function Name : listEquipmentCategories
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function return equipment categories.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/equipmentcategories", method = RequestMethod.GET)
	public String listEquipmentCategories(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			// UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("equipmentcategory", new EquipmentCategory());

		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("activeStatusList", getStatusDropdownList());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));

		return "equipmentcategory";
	}

	/********************************************************************************************
	 * 
	 * Function Name : EquipmentCategoryUpdation
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function updates equipment category.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/editequipmentcategory", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> EquipmentCategoryUpdation(Integer EQCategoryId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			objResponseEntity.put("equipmentcategory", equipmentcategoryService.getEquipmentCategoryById(EQCategoryId));
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}
}
