
/******************************************************
 * 
 *    	Filename	: UserController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with user Activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.model.UserRole;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.Constant;

@Controller
public class UserController {

	@Autowired
	private UserService userservice;
	@Autowired
	private UserService userService;

	@Autowired
	private Constant constant;

	@Autowired
	private SiteService siteService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	// private EncryptionAlgoritham objEA = new EncryptionAlgoritham();

	/********************************************************************************************
	 * 
	 * Function Name : UserGridLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns data for user page grid.
	 * 
	 * Input Params  : session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> UserGridLoad(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();
				User user = userService.getUserById(id);
				if (user != null)
					objResponseEntity.put("userList", getUpdatedUserList(user));
				objResponseEntity.put("ticketcreation", new TicketDetail());
				objResponseEntity.put("siteList", getDropdownList("Site"));
				objResponseEntity.put("userroleList", getDropdownList("Role"));

				objResponseEntity.put("activeStatusList", getStatusDropdownList());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : NewUserSaveorUpdate
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function saves or updates user.
	 * 
	 * Input Params  : lstuser,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : ConstraintViolationException,Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/newuser", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> NewUserSaveorUpdate(@RequestBody List<User> lstuser, HttpSession session,
			Authentication authentication) throws ServletException {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String ResponseMessage = "";
		String encryptpwd = "";
		String Dataexists1 = "";
		String Dataexists2 = "";
		try {

			if (lstuser != null) {
				User user = lstuser.get(0);

				if (user != null) {

					Date date = new Date();
					if (user.getUserId() == null || user.getUserId() == 0) {

						List<User> lstusers = userService.listUsers(0);

						for (int i = 0; i < lstusers.size(); i++) {
							String Username = lstusers.get(i).getUserName();
							Integer Roleid = lstusers.get(i).getRoleID();
							String Mobilenumber = lstusers.get(i).getMobileNumber();

							if (user.getUserName().equals(Username) && user.getRoleID().equals(Roleid)) {

								Dataexists1 = "UserName Already Map to this Role";

								System.out.println("Entered in this loop");
								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						String Userdetails = "Hi " + user.getUserName() + "," + System.lineSeparator()
								+ System.lineSeparator() + "Username : " + user.getUserName() + System.lineSeparator()
								+ "Password : " + user.getPassword() + System.lineSeparator() + System.lineSeparator()
								+ "Thanks & Regards," + System.lineSeparator() + " Inspire Clean Energy Pvt. Ltd."
								+ System.lineSeparator() + " Your Assets, Our Care";

						/*
						 * MailSend.sendTo(user.getEmailID(), "User Details", Userdetails, environment);
						 */
						constant.send("support@eira.io", "eira@ice4ever", user.getEmailID(), "User Details",
								Userdetails);

						user.setUserCode((getUserCode()));
						user.setCreationDate(date);
						user.setCreatedBy(id);

//						EncryptionAlgoritham objEA = new  EncryptionAlgoritham();
//						encryptpwd = objEA.MESEncryption(user.getPassword());
//
//						user.setEncryptPassword(encryptpwd);

						userService.addUser(user);
					} else {
						User objUser = userService.getUserById(user.getUserId());

						List<User> lstusers = userService.listUsers(user.getUserId());

						for (int i = 0; i < lstusers.size(); i++) {
							String Username = lstusers.get(i).getUserName();
							Integer Roleid = lstusers.get(i).getRoleID();
							String Mobilenumber = lstusers.get(i).getMobileNumber();

							if (user.getUserName().equals(Username) && user.getRoleID().equals(Roleid)) {

								Dataexists1 = "UserName Already Map to this Role";

								objResponseEntity.put("status", false);
								objResponseEntity.put("Errorlog1", Dataexists1);

								objResponseEntity.put("data", "false");
								return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

							}

						}

						objUser.setUserName(user.getUserName());
						objUser.setDesignation(user.getDesignation());
						objUser.setDepartment(user.getDepartment());
						objUser.setRoleID(user.getRoleID());

						objUser.setFirstName(user.getFirstName());
						objUser.setLastName(user.getLastName());
						objUser.setShortName(user.getShortName());

						objUser.setMobileNumber(user.getMobileNumber());
						objUser.setEmailID(user.getEmailID());

						objUser.setPassword(user.getPassword());

						String Userdetails = "Hi " + user.getUserName() + "," + System.lineSeparator()
								+ System.lineSeparator() + "Username : " + user.getUserName() + System.lineSeparator()
								+ "Password : " + user.getPassword() + System.lineSeparator() + System.lineSeparator()
								+ "Thanks & Regards," + System.lineSeparator() + " Inspire Clean Energy Pvt. Ltd."
								+ System.lineSeparator() + " Your Assets, Our Care";

						/*
						 * MailSend.sendTo(user.getEmailID(), "User Details", Userdetails, environment);
						 */
						constant.send("support@eira.io", "eira@ice4ever", user.getEmailID(), "Updated User Details",
								Userdetails);

//						encryptpwd = objEA.MESEncryption(user.getPassword());
//
//						objUser.setEncryptPassword(encryptpwd);

						objUser.setActiveFlag(user.getActiveFlag());
						objUser.setLastUpdatedDate(date);
						objUser.setLastUpdatedBy(id);

						userService.updateUser(objUser);

						objResponseEntity.put("status", true);
						objResponseEntity.put("user", objUser);
						objResponseEntity.put("data", "Success");
						return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

					}
					objResponseEntity.put("status", true);
					objResponseEntity.put("user", user);
					ResponseMessage = "Success";
				} else {
					objResponseEntity.put("status", false);
					ResponseMessage = "Invalid Request";
					objResponseEntity.put("user", null);
				}

			} else {
				objResponseEntity.put("status", false);
				ResponseMessage = "Invalid Request";
				objResponseEntity.put("user", null);
			}

			objResponseEntity.put("data", ResponseMessage);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (ConstraintViolationException cve) {
			objResponseEntity.put("user", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", cve.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			objResponseEntity.put("user", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown list.
	 * 
	 * Input Params  : DropdownName
	 * 
	 * Return Value  : Map<String, String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Role") {
			List<UserRole> ddlList = userRoleService.listUserRoles();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getRoleId().toString(), ddlList.get(i).getRoleName());
			}

		} else if (DropdownName == "Site") {
			List<Site> ddlList = siteService.listSites();

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getUpdatedUserList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns user list.
	 * 
	 * Input Params  : user
	 * 
	 * Return Value  : List<User>
	 * 
	 * 
	 **********************************************************************************************/
	public List<User> getUpdatedUserList(User user) {
		List<User> ddlUsersList = userService.listUsers();
		Map<Integer, UserRole> userRoleMap = new HashMap<>();
		List<UserRole> ddlUserRoleList = userRoleService.listUserRoles();
		if (ddlUserRoleList != null && !ddlUserRoleList.isEmpty()) {
			for (UserRole userRole : ddlUserRoleList) {
				userRoleMap.put(userRole.getRoleId(), userRole);
			}
		}
		if (ddlUsersList != null && !ddlUsersList.isEmpty()) {
			if (user.getUserName().equalsIgnoreCase("IceAdmin")) {
				Iterator<User> iterator = ddlUsersList.iterator();
				while (iterator.hasNext()) {
					User deletingObject = iterator.next();
					if (deletingObject != null && deletingObject.getUserName().equalsIgnoreCase("Superadmin")) {
						iterator.remove();
					}
				}
			}
		}
		return ddlUsersList;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getUserCode
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns user code.
	 * 
	 * Return Value  : String
	 * 
	 * 
	 **********************************************************************************************/
	public String getUserCode() {
		User objUser = new User();
		objUser = userService.getUserByMax("UserCode");

		String UR = "";

		if (objUser != null) {
			UR = objUser.getUserCode();
		}

		if (UR == null || UR == "") {
			UR = "UR00001";
		} else {
			try {
				int NextIndex = Integer.parseInt(UR.replaceAll("UR", "")) + 1;

				if (NextIndex >= 10000) {
					UR = String.valueOf(NextIndex);
				} else if (NextIndex >= 1000) {
					UR = "0" + String.valueOf(NextIndex);
				} else if (NextIndex >= 100) {
					UR = "00" + String.valueOf(NextIndex);
				} else if (NextIndex >= 10) {
					UR = "000" + String.valueOf(NextIndex);
				} else {
					UR = "0000" + String.valueOf(NextIndex);
				}

				UR = "UR" + UR;
			} catch (Exception e) {
				UR = "UR00001";
			}
			System.out.println("Handled" + UR);
		}
		System.out.println("Handled" + UR);
		return UR;
	}

	/********************************************************************************************
	 * 
	 * Function Name : UsersPageLoad
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads user page.
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String UsersPageLoad(Model model, HttpSession session, Authentication authentication)
			throws ServletException {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());

		model.addAttribute("user", new User());
		model.addAttribute("siteList", getDropdownList("Site"));
		model.addAttribute("ticketcreation", new TicketDetail());

		model.addAttribute("userroleList", getDropdownList("Role"));

		model.addAttribute("activeStatusList", getStatusDropdownList());

		return "user";
	}

	/********************************************************************************************
	 * 
	 * Function Name : UserUpdation
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function updates a user.
	 * 
	 * Input Params  : UserId,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String, Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/edituser", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> UserUpdation(Integer UserId, HttpSession session,
			Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			objResponseEntity.put("user", userService.getUserById(UserId));
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

}
