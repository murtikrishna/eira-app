
/******************************************************
 * 
 *    	Filename	: DataExportController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This Controller deals with data export activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.DataExport;
import com.mestech.eampm.bean.EquipmentListBean;
import com.mestech.eampm.bean.SiteListBean;
import com.mestech.eampm.bean.SiteViewBean;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.TicketTransaction;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.CustomerType;
import com.mestech.eampm.model.DataSource;
import com.mestech.eampm.model.DataTransaction;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentStatus;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;
import com.mestech.eampm.model.SiteSummary;
import com.mestech.eampm.model.State;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.DataSourceService;
import com.mestech.eampm.service.DataTransactionService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteStatusService;
import com.mestech.eampm.service.SiteSummaryService;
import com.mestech.eampm.service.StateService;
import com.mestech.eampm.service.TicketDetailService;
import com.mestech.eampm.service.TicketTransactionService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class DataExportController {

	@Autowired
	private CountryService countryService;
	@Autowired
	private StateService stateService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private SiteStatusService siteStatusService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private TicketDetailService ticketDetailService;

	@Autowired
	private TicketTransactionService tickettransactionService;

	@Autowired
	private DataSourceService dataSourceService;

	@Autowired
	private DataTransactionService dataTransactionService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private SiteSummaryService siteSummaryService;

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;

	/********************************************************************************************
	 * 
	 * Function Name : liStringDataExport
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads data export page.
	 * 
	 * Input Params  : id,refid,model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/dataexport{id}&ref={refid}", method = RequestMethod.GET)
	public String liStringDataExport(@PathVariable("id") int id, @PathVariable("refid") int refid, Model model,
			HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		String timezonevalue = "0";
		if (!timezonevalue.equals("") && timezonevalue != null) {
			timezonevalue = session.getAttribute("timezonevalue").toString();
		}

		DataExport objDataExport = new DataExport();

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

		List<Equipment> lstEquipment = equipmentService.listEquipmentsBySiteId(id);
		List<DataTransaction> lstDataTransaction = dataTransactionService.listDataTransactionsForDataDownload(id,
				"21-12-2017", "21-12-2017", 10000, timezonevalue);

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		List<String> objactivitys = new ArrayList<>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);
			objAccessListBean.setUserID(refid);
			objAccessListBean.setMySiteID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			Site objSite = siteService.getSiteById(id);

			if (objSite != null) {
				objAccessListBean.setMySiteName(objSite.getSiteName());
				objAccessListBean.setMyCustomerID(objSite.getCustomerID());

				Customer objCustomer = customerService.getCustomerById(objSite.getCustomerID());
				if (objCustomer != null) {
					objAccessListBean.setMyCustomerName(objCustomer.getCustomerName());
				} else {
					objAccessListBean.setMyCustomerName("Customer View");
				}

			} else {

				objAccessListBean.setMyCustomerID(7);
				objAccessListBean.setMyCustomerName("Customer View");
				objAccessListBean.setMySiteName("Site View");
			}

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID().equals(objUser.getRoleID())).collect(Collectors.toList());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("siteList", getNewTicketSiteList("Site", refid));
		model.addAttribute("site1List", getDropdownList("SingleSite", id));
		model.addAttribute("equipmentList", getDropdownList("Equipment", id));

		model.addAttribute("dataexport", objDataExport);
		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());
		model.addAttribute("datatransaction", lstDataTransaction);
		model.addAttribute("ticketcreation", new TicketDetail());
		return "dataexport";
	}

	/********************************************************************************************
	 * 
	 * Function Name : getDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns values for dropdown list.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getDropdownList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "SingleSite") {
			List<Site> ddlList = siteService.listSiteById(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
			}

		} else if (DropdownName == "Equipment") {
			List<Equipment> ddlList = equipmentService.listEquipmentsBySiteId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getEquipmentId().toString(), ddlList.get(i).getCustomerNaming());
			}

		} else if (DropdownName == "User") {
			List<User> ddlList = userService.listFieldUsers();

			for (int i = 0; i < ddlList.size(); i++) {
				ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
			}

		}

		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns values for dropdown list.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : liStringDataExportView
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function loads data .export page.
	 * 
	 * Input Params  : id,refid,model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/dataexport{id}&ref={refid}", method = RequestMethod.POST)
	public String liStringDataExportView(@PathVariable("id") int id, @PathVariable("refid") int refid, Model model,
			@ModelAttribute("dataexport") DataExport dataexport, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		} else if (session == null) {
			return "redirect:/login";
		} else if (session.getAttribute("timezonevalue") == null) {
			return "redirect:/login";
		}

		String timezonevalue = "0";
		if (!timezonevalue.equals("") && timezonevalue != null) {
			timezonevalue = session.getAttribute("timezonevalue").toString();
		}

		DataExport objDataExport = new DataExport();

		objDataExport.setSiteID(dataexport.getSiteID());
		objDataExport.setEquipmentID(dataexport.getEquipmentID());
		objDataExport.setFromDate(dataexport.getFromDate());
		objDataExport.setToDate(dataexport.getToDate());

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");

		List<Equipment> lstEquipment = equipmentService.listEquipmentsBySiteId(id);
		List<DataTransaction> lstDataTransaction = dataTransactionService.listDataTransactionsForDataDownload(
				dataexport.getSiteID(), dataexport.getFromDate(), dataexport.getToDate(), dataexport.getEquipmentID(),
				timezonevalue);

		System.out.println("Sizeeeee" + lstDataTransaction.size());

		AccessListBean objAccessListBean = new AccessListBean();

		try {
			User objUser = userService.getUserById(refid);
			List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(refid);

			objAccessListBean.setMySiteID(id);
			objAccessListBean.setUserName(objUser.getUserName());

			Site objSite = siteService.getSiteById(id);

			if (objSite != null) {
				objAccessListBean.setMySiteName(objSite.getSiteName());
				objAccessListBean.setMyCustomerID(objSite.getCustomerID());

				Customer objCustomer = customerService.getCustomerById(objSite.getCustomerID());
				if (objCustomer != null) {
					objAccessListBean.setMyCustomerName(objCustomer.getCustomerName());
				} else {
					objAccessListBean.setMyCustomerName("Customer View");
				}

			} else {

				objAccessListBean.setMyCustomerID(7);
				objAccessListBean.setMyCustomerName("Customer View");
				objAccessListBean.setMySiteName("Site View");
			}

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 5) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActivityID() == 1 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setDashboard("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 2 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setOverview("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 3 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setSystemMonitoring("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 4 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setVisualization("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 5 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalytics("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 6 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setPortfolioManagement("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 7 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setTicketing("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 8 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setForcasting("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 9 && lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setConfiguration("visible");
				} else if (lstRoleActivity.get(l).getActivityID() == 10
						&& lstRoleActivity.get(l).getActiveFlag() == 1) {
					objAccessListBean.setAnalysis("visible");
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("userList", getDropdownList("User", refid));

		model.addAttribute("siteList", getDropdownList("Site", refid));
		model.addAttribute("site1List", getDropdownList("SingleSite", id));
		model.addAttribute("equipmentList", getDropdownList("Equipment", id));

		model.addAttribute("dataexport", objDataExport);
		model.addAttribute("access", objAccessListBean);
		System.out.println(lstDataTransaction.size());
		model.addAttribute("datatransaction", lstDataTransaction);
		model.addAttribute("ticketcreation", new TicketDetail());

		return "dataexport";
	}

}
