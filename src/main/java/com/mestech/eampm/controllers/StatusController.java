
/******************************************************
 * 
 *    	Filename	: StatusController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Status related Activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentCategory;
import com.mestech.eampm.model.EquipmentType;
import com.mestech.eampm.model.MDataloggerType;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.Status;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.EquipmentTypeService;
import com.mestech.eampm.service.MDataloggerTypeService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.StatusService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.UtilityCommon;
import com.mestech.eampm.utility.UtilityCommonDAO;

@Controller
public class StatusController {

	@Autowired
	private SiteService siteService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private UserService userService;

	@Autowired
	private StatusService statusService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private EquipmentTypeService equipmentTypeService;

	@Autowired
	private MDataloggerTypeService dataloggerTypeService;

	@Autowired
	private RoleActivityService roleActivityService;

	private UtilityCommonDAO utilityCommonDAO = new UtilityCommonDAO();
	private UtilityCommon utilityCommon = new UtilityCommon();

	/********************************************************************************************
	 * 
	 * Function Name : statuspageload
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns status list.
	 * 
	 * Input Params  : session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/status", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> statuspageload(HttpSession session, Authentication authentication) {

		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {
			if (authentication == null) {
				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Session Expired");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.UNAUTHORIZED);

			}

			else {

				LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
				int id = loginUserDetails.getEampmuserid();

				String timezonevalue = "0";
				if (!timezonevalue.equals("") && timezonevalue != null) {
					timezonevalue = session.getAttribute("timezonevalue").toString();
				}

				objResponseEntity.put("statuslist", getStatusList(timezonevalue));

				objResponseEntity.put("activeStatusList", getStatusDropdownList());

				objResponseEntity.put("status", true);
				objResponseEntity.put("data", "Success");
				return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);
			}
		} catch (Exception e) {
			objResponseEntity.put("status", false);
			System.out.println(e.getMessage());
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : listStatus
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM
	 * 
	 * Description   : This fuction returns
	 * 
	 * Input Params  : model,session,authentication
	 * 
	 * Return Value  : String
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/sitestatus", method = RequestMethod.GET)
	public String listStatus(Model model, HttpSession session, Authentication authentication) {

		if (authentication == null) {
			return "redirect:/login";
		}

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		AccessListBean objAccessListBean = new AccessListBean();
		List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

		StringBuilder stringBuilder = new StringBuilder();

		try {
			User objUser = userService.getUserById(id);

			lstRoleActivity = roleActivityService.listRoleActivities().stream()
					.filter(p -> p.getRoleID() == objUser.getRoleID()).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getShortName());

			if (objUser.getRoleID() != 4 && objUser.getRoleID() != 6) {
				objAccessListBean.setCustomerListView("visible");
			}
			if (objUser.getRoleID() == 6) {
				objAccessListBean.setMonitoringView("visible");
			}

			for (int l = 0; l < lstRoleActivity.size(); l++) {
				if (lstRoleActivity.get(l).getActiveFlag() == 1) {
					Activity activity = activityService.getActivityName(lstRoleActivity.get(l).getActivityID());
					if (null != activity) {
						if (activity.getActivityDescription() != null) {
							stringBuilder.append(activity.getActivityDescription());
							stringBuilder.append("#");
						}
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		model.addAttribute("statusmaster", new Status());
		model.addAttribute("ticketcreation", new TicketDetail());
		model.addAttribute("siteList", getNewTicketSiteList("Site", id));
		model.addAttribute("classtate", "active");
		model.addAttribute("access", objAccessListBean);
		model.addAttribute("accessPages", stringBuilder.toString());

		return "status";
	}

	/********************************************************************************************
	 * 
	 * Function Name : getNewTicketSiteList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown.
	 * 
	 * Input Params  : DropdownName,refid
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getNewTicketSiteList(String DropdownName, int refid) {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();

		if (DropdownName == "Site") {
			List<Site> ddlList = siteService.getSiteListByUserId(refid);

			for (int i = 0; i < ddlList.size(); i++) {
				String SiteName = "";

				if (ddlList.get(i).getSiteName() != null) {
					SiteName = ddlList.get(i).getSiteName();
				}

				ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName);

			}

		}
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getStatusDropdownList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns value for dropdown.
	 * 
	 * Return Value  : Map<String,String>
	 * 
	 * 
	 **********************************************************************************************/
	public Map<String, String> getStatusDropdownList() {
		Map<String, String> ddlMap = new LinkedHashMap<String, String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	/********************************************************************************************
	 * 
	 * Function Name : getStatusList
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns all status lists.
	 * 
	 * Input Params  : timezonevalue
	 * 
	 * Return Value  : List<Status>
	 * 
	 * 
	 **********************************************************************************************/
	public List<Status> getStatusList(String timezonevalue) {
		List<Status> ddlStatussList = statusService.listStatuses();
		List<MDataloggerType> ddlDataloggers = dataloggerTypeService.listAll();

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Calendar cal = Calendar.getInstance();

		for (int i = 0; i < ddlStatussList.size(); i++) {
			Integer dataloggertypeid = ddlStatussList.get(i).getDataLoggerTypeId();

			if (utilityCommonDAO.DBType.equals(utilityCommonDAO.PostgreDB)) {
				if (utilityCommonDAO.TimezoneType.equals(utilityCommonDAO.UTCZone)) {

					if (ddlStatussList.get(i).getCreationDate() != null) {

						cal.setTime(ddlStatussList.get(i).getCreationDate());
						cal.add(Calendar.MINUTE, Integer.valueOf(timezonevalue));

						ddlStatussList.get(i).setCreationDateText(sdf.format(cal.getTime()));

					}
					if (ddlStatussList.get(i).getLastUpdatedDate() != null) {

						cal.setTime(ddlStatussList.get(i).getLastUpdatedDate());
						cal.add(Calendar.MINUTE, Integer.valueOf(timezonevalue));
						ddlStatussList.get(i).setLastUpdatedDateText(sdf.format(cal.getTime()));

					}
				}

				if (ddlStatussList.get(i).getCreationDate() != null) {
					ddlStatussList.get(i).setCreationDateText(sdf.format(ddlStatussList.get(i).getCreationDate()));

				}
				if (ddlStatussList.get(i).getLastUpdatedDate() != null) {
					ddlStatussList.get(i)
							.setLastUpdatedDateText(sdf.format(ddlStatussList.get(i).getLastUpdatedDate()));

				}

			}

			if (ddlStatussList.get(i).getDerivedStatus() == 0) {
				ddlStatussList.get(i).setDerivedStatusText("Offline");

			} else if (ddlStatussList.get(i).getDerivedStatus() == 1) {
				ddlStatussList.get(i).setDerivedStatusText("Active");
			} else if (ddlStatussList.get(i).getDerivedStatus() == 2) {
				ddlStatussList.get(i).setDerivedStatusText("Warning");
			} else {
				ddlStatussList.get(i).setDerivedStatusText("Down");
			}

			if (ddlDataloggers.stream().filter(p -> p.getDataloggertypeid().equals(dataloggertypeid))
					.collect(Collectors.toList()).size() >= 1) {
				System.out.println("2");
				MDataloggerType objDataloggers = new MDataloggerType();
				objDataloggers = ddlDataloggers.stream().filter(p -> p.getDataloggertypeid().equals(dataloggertypeid))
						.collect(Collectors.toList()).get(0);
				System.out.println(objDataloggers.getDescription());

				ddlStatussList.get(i).setDataLoggerTypeIdText(objDataloggers.getDescription());
			}

		}

		return ddlStatussList;
	}

	/********************************************************************************************
	 * 
	 * Function Name : StatusCode
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function adds a new status code.
	 * 
	 * Input Params  : lststatus,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions    : ConstraintViolationException,Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/newstatuscode", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> StatusCode(@RequestBody List<Status> lststatus, HttpSession session,
			Authentication authentication) throws ServletException {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String ResponseMessage = "";
		String Dataexists1 = "";
		String Dataexists2 = "";

		try {

			if (lststatus != null) {
				Status status = lststatus.get(0);

				if (status != null) {

					Date date = new Date();
					if (status.getStatusId() == null || status.getStatusId() == 0) {

						status.setCreationDate(date);
						status.setCreatedBy(id);

						statusService.addStatus(status);
					} else {
						Status objstatus = statusService.getStatusById(status.getStatusId());

						objstatus.setStatus(status.getStatus());
						objstatus.setDataLoggerTypeId(status.getDataLoggerTypeId());
						objstatus.setStatusCode(status.getStatusCode());
						objstatus.setDescription(status.getDescription());
						objstatus.setDerivedStatus(status.getDerivedStatus());

						objstatus.setActiveFlag(status.getActiveFlag());

						objstatus.setLastUpdatedDate(date);
						objstatus.setLastUpdatedBy(id);

						statusService.updateStatus(objstatus);

						objResponseEntity.put("status", true);
						objResponseEntity.put("status", objstatus);
						objResponseEntity.put("data", "Success");
						return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

					}
					objResponseEntity.put("status", true);
					objResponseEntity.put("status", status);
					ResponseMessage = "Success";
				} else {
					objResponseEntity.put("status", false);
					ResponseMessage = "Invalid Request";
					objResponseEntity.put("status", null);
				}

			} else {
				objResponseEntity.put("status", false);
				ResponseMessage = "Invalid Request";
				objResponseEntity.put("status", null);
			}

			objResponseEntity.put("data", ResponseMessage);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (ConstraintViolationException cve) {
			objResponseEntity.put("status", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", cve.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			objResponseEntity.put("status", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : StatusMaster
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function adds new status master.
	 * 
	 * Input Params  : lststatusmaster,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions    : ConstraintViolationException,Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/newstatusmaster", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> StatusMaster(@RequestBody List<MDataloggerType> lststatusmaster,
			HttpSession session, Authentication authentication) throws ServletException {
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();

		LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
		int id = loginUserDetails.getEampmuserid();

		String ResponseMessage = "";
		String Dataexists1 = "";
		String Dataexists2 = "";

		try {

			if (lststatusmaster != null) {
				MDataloggerType statusmaster = lststatusmaster.get(0);

				if (statusmaster != null) {

					Date date = new Date();
					if (statusmaster.getDataloggertypeid() == null || statusmaster.getDataloggertypeid() == 0) {

						statusmaster.setCreationdate(date);
						statusmaster.setCreatedby(id);

						dataloggerTypeService.addDatalogger(statusmaster);
					} else {
						MDataloggerType objstatusmaster = dataloggerTypeService
								.edit(statusmaster.getDataloggertypeid());

						objstatusmaster.setDescription(statusmaster.getDescription());
						objstatusmaster.setRemarks(statusmaster.getRemarks());

						objstatusmaster.setActiveflag(statusmaster.getActiveflag());

						objstatusmaster.setLastupdateddate(date);
						objstatusmaster.setLastupdatedby(id);

						dataloggerTypeService.update(objstatusmaster);

						objResponseEntity.put("status", true);
						objResponseEntity.put("objstatusmaster", objstatusmaster);
						objResponseEntity.put("data", "Success");
						return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

					}
					objResponseEntity.put("status", true);
					objResponseEntity.put("status", statusmaster);
					ResponseMessage = "Success";
				} else {
					objResponseEntity.put("status", false);
					ResponseMessage = "Invalid Request";
					objResponseEntity.put("status", null);
				}

			} else {
				objResponseEntity.put("status", false);
				ResponseMessage = "Invalid Request";
				objResponseEntity.put("status", null);
			}

			objResponseEntity.put("data", ResponseMessage);
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (ConstraintViolationException cve) {
			objResponseEntity.put("status", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", cve.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			objResponseEntity.put("status", null);
			objResponseEntity.put("status", false);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : statusUpdation
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function updates status code.
	 * 
	 * Input Params  : StatusId,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/editstatuscode", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> statusUpdation(Integer StatusId, HttpSession session,
			Authentication authentication) {
		Status objStatus = null;
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			objStatus = statusService.getStatusById(StatusId);

			objResponseEntity.put("objStatus", objStatus);
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("objStatus", objStatus);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/********************************************************************************************
	 * 
	 * Function Name : statusmasterUpdation
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function edit status master.
	 * 
	 * Input Params  : MasterId,session,authentication
	 * 
	 * Return Value  : ResponseEntity<Map<String,Object>>
	 * 
	 * Exceptions    : Exception
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/editstatusmaster", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> statusmasterUpdation(Integer MasterId, HttpSession session,
			Authentication authentication) {
		MDataloggerType objStatusmaster = null;
		Map<String, Object> objResponseEntity = new HashMap<String, Object>();
		try {

			LoginUserDetails loginUserDetails = (LoginUserDetails) authentication.getPrincipal();
			int id = loginUserDetails.getEampmuserid();

			objStatusmaster = dataloggerTypeService.edit(MasterId);

			objResponseEntity.put("objStatusmaster", objStatusmaster);
			objResponseEntity.put("status", true);
			objResponseEntity.put("data", "Success");
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.OK);

		} catch (Exception e) {
			objResponseEntity.put("status", false);
			objResponseEntity.put("objStatusmaster", objStatusmaster);
			objResponseEntity.put("data", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(objResponseEntity, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

}
