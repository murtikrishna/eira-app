
/******************************************************
 * 
 *    	Filename	: MDataloggerController.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM 
 *      
 *      Description : This controller deals with Datalogger Activities.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mestech.eampm.service.MDataloggerService;
import com.mestech.eampm.utility.Response;

@Controller
public class MDataloggerController {

	@Autowired
	private MDataloggerService mDataloggerService;

	/********************************************************************************************
	 * 
	 * Function Name : getAllDataLogger
	 * 
	 * Author        : Sarath Babu E
	 * 
	 * Time Stamp    : 29-Apr-19 09:47 AM 
	 * 
	 * Description   : This function returns all dataloggers.
	 * 
	 * Return Value  : Response
	 * 
	 * 
	 **********************************************************************************************/
	@RequestMapping(value = "/getAllDataLogger", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Response getAllDataLogger() {
		Response response = new Response();
		response.setData(mDataloggerService.listAll());
		response.setStatus("Success");
		return response;
	}

}
