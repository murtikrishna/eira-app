
/******************************************************
 * 
 *    	Filename	: StandardParameterBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Standard parameter data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

import java.util.Date;

public class StandardParameterBean {

	private Integer StandardId;

	private String StandardParameterName;

	public Integer getStandardId() {
		return StandardId;
	}

	public void setStandardId(Integer standardId) {
		StandardId = standardId;
	}

	public String getStandardParameterName() {
		return StandardParameterName;
	}

	public void setStandardParameterName(String standardParameterName) {
		StandardParameterName = standardParameterName;
	}

	public String getParameterDescription() {
		return ParameterDescription;
	}

	public void setParameterDescription(String parameterDescription) {
		ParameterDescription = parameterDescription;
	}

	public String getStandardParameterUOM() {
		return StandardParameterUOM;
	}

	public void setStandardParameterUOM(String standardParameterUOM) {
		StandardParameterUOM = standardParameterUOM;
	}

	private String ParameterDescription;

	private String StandardParameterUOM;
}
