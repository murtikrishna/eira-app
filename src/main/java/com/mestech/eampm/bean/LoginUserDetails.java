
/******************************************************
 * 
 *    	Filename	: LoginUserDetails.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Graph data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

import java.security.Principal;
import java.util.List;

public class LoginUserDetails implements Principal {

	private String userName;

	private int eampmuserid;

	private String timezoneoffsetmin;

	private String SessionID;

	private String IpAddress;

	public LoginUserDetails() {

	}

	public LoginUserDetails(String userName, int eampmuserid, String timezoneoffsetmin, String sessionID,
			String ipAddress) {
		super();
		this.userName = userName;
		this.eampmuserid = eampmuserid;
		this.timezoneoffsetmin = timezoneoffsetmin;
		SessionID = sessionID;
		IpAddress = ipAddress;
	}

	public String getSessionID() {
		return SessionID;
	}

	public void setSessionID(String sessionID) {
		SessionID = sessionID;
	}

	public String getIpAddress() {
		return IpAddress;
	}

	public void setIpAddress(String ipAddress) {
		IpAddress = ipAddress;
	}

	public String getTimezoneoffsetmin() {
		return timezoneoffsetmin;
	}

	public void setTimezoneoffsetmin(String timezoneoffsetmin) {
		this.timezoneoffsetmin = timezoneoffsetmin;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getEampmuserid() {
		return eampmuserid;
	}

	public void setEampmuserid(int eampmuserid) {
		this.eampmuserid = eampmuserid;
	}

}