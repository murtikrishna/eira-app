
/******************************************************
 * 
 *    	Filename	: AnalysisListBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Analysis data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class AnalysisListBean {

	Integer CustomerID;

	String CustomerName;

	String CustomerCode;

	String SiteIDs;

	String EquipmentIDs;

	String InverterIDs;

	String SensorIDs;

	public Integer getCustomerID() {
		return CustomerID;
	}

	public void setCustomerID(Integer customerID) {
		CustomerID = customerID;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getCustomerCode() {
		return CustomerCode;
	}

	public void setCustomerCode(String customerCode) {
		CustomerCode = customerCode;
	}

	public String getSiteIDs() {
		return SiteIDs;
	}

	public void setSiteIDs(String siteIDs) {
		SiteIDs = siteIDs;
	}

	public String getEquipmentIDs() {
		return EquipmentIDs;
	}

	public void setEquipmentIDs(String equipmentIDs) {
		EquipmentIDs = equipmentIDs;
	}

	public String getInverterIDs() {
		return InverterIDs;
	}

	public void setInverterIDs(String inverterIDs) {
		InverterIDs = inverterIDs;
	}

	public String getSensorIDs() {
		return SensorIDs;
	}

	public void setSensorIDs(String sensorIDs) {
		SensorIDs = sensorIDs;
	}

}
