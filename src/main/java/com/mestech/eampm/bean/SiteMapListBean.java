
/******************************************************
 * 
 *    	Filename	: SiteMapListBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle site map data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class SiteMapListBean {

	private String SiteID;
	private String SiteCode;
	private String SiteReference;
	private String SiteName;
	private String NetworkStatus;
	private String Address;
	private String City;
	private String State;
	private String Country;
	private String PinCode;

	public String getSiteID() {
		return SiteID;
	}

	public void setSiteID(String siteID) {
		SiteID = siteID;
	}

	public String getSiteCode() {
		return SiteCode;
	}

	public void setSiteCode(String siteCode) {
		SiteCode = siteCode;
	}

	public String getSiteReference() {
		return SiteReference;
	}

	public void setSiteReference(String siteReference) {
		SiteReference = siteReference;
	}

	public String getSiteName() {
		return SiteName;
	}

	public void setSiteName(String siteName) {
		SiteName = siteName;
	}

	public String getNetworkStatus() {
		return NetworkStatus;
	}

	public void setNetworkStatus(String networkStatus) {
		NetworkStatus = networkStatus;
	}

	public String getLat() {
		return Lat;
	}

	public void setLat(String lat) {
		Lat = lat;
	}

	public String getLng() {
		return Lng;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getPinCode() {
		return PinCode;
	}

	public void setPinCode(String pinCode) {
		PinCode = pinCode;
	}

	public void setLng(String lng) {
		Lng = lng;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	private String Lat;
	private String Lng;
	private String Type;
}
