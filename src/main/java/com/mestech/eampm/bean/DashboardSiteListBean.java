
/******************************************************
 * 
 *    	Filename	: DashboardSiteListBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Dashboard,sitelist data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class DashboardSiteListBean {

	private Integer SiteId;
	private String SiteCode;
	private String SiteReference;
	private String SiteName;
	private String Latitude;
	private String Longitude;
	private String Address;
	private String City;
	private String State;
	private String Country;
	private String PostalCode;
	private String ActiveInverterCount;
	private String InverterCount;
	private String LastChecked;
	private Double TotalEnergy;
	private Double TodayEnergy;
	private String LastDownTime;
	private String LastDataReceived;
	private String TodayHoursOn;
	private String Status;
	private Double InstallationCapacity;
	String Mobile = "";
	String Telephone = "";
	String LocationUrl = "";
	
	public DashboardSiteListBean(Integer siteId, String siteCode, String siteReference, String siteName,
			String latitude, String longitude, String address, String city, String state, String country,
			String postalCode, String activeInverterCount, String inverterCount, String lastChecked, Double totalEnergy,
			Double todayEnergy, String lastDownTime, String lastDataReceived, String todayHoursOn, String status, Double installationCapacity) {
		super();
		SiteId = siteId;
		SiteCode = siteCode;
		SiteReference = siteReference;
		SiteName = siteName;
		Latitude = latitude;
		Longitude = longitude;
		Address = address;
		City = city;
		State = state;
		Country = country;
		PostalCode = postalCode;
		ActiveInverterCount = activeInverterCount;
		InverterCount = inverterCount;
		LastChecked = lastChecked;
		TotalEnergy = totalEnergy;
		TodayEnergy = todayEnergy;
		LastDownTime = lastDownTime;
		LastDataReceived = lastDataReceived;
		TodayHoursOn = todayHoursOn;
		Status = status;
		InstallationCapacity = installationCapacity;
	}
	
	
	public DashboardSiteListBean(Integer siteId, String siteCode, String siteReference, String siteName,
			String latitude, String longitude, String address, String city, String state, String country,
			String postalCode, String activeInverterCount, String inverterCount, String lastChecked, Double totalEnergy,
			Double todayEnergy, String lastDownTime, String lastDataReceived, String todayHoursOn, String status,
			Double installationCapacity, String mobile, String telephone, String locationUrl) {
		super();
		SiteId = siteId;
		SiteCode = siteCode;
		SiteReference = siteReference;
		SiteName = siteName;
		Latitude = latitude;
		Longitude = longitude;
		Address = address;
		City = city;
		State = state;
		Country = country;
		PostalCode = postalCode;
		ActiveInverterCount = activeInverterCount;
		InverterCount = inverterCount;
		LastChecked = lastChecked;
		TotalEnergy = totalEnergy;
		TodayEnergy = todayEnergy;
		LastDownTime = lastDownTime;
		LastDataReceived = lastDataReceived;
		TodayHoursOn = todayHoursOn;
		Status = status;
		InstallationCapacity = installationCapacity;
		Mobile = mobile;
		Telephone = telephone;
		LocationUrl = locationUrl;
	}


	public Integer getSiteId() {
		return SiteId;
	}
	public void setSiteId(Integer siteId) {
		SiteId = siteId;
	}
	public String getSiteCode() {
		return SiteCode;
	}
	public void setSiteCode(String siteCode) {
		SiteCode = siteCode;
	}
	public String getSiteReference() {
		return SiteReference;
	}
	public void setSiteReference(String siteReference) {
		SiteReference = siteReference;
	}
	public String getSiteName() {
		return SiteName;
	}
	public void setSiteName(String siteName) {
		SiteName = siteName;
	}
	public String getLatitude() {
		return Latitude;
	}
	public void setLatitude(String latitude) {
		Latitude = latitude;
	}
	public String getLongitude() {
		return Longitude;
	}
	public void setLongitude(String longitude) {
		Longitude = longitude;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getPostalCode() {
		return PostalCode;
	}
	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}
	public String getActiveInverterCount() {
		return ActiveInverterCount;
	}
	public void setActiveInverterCount(String activeInverterCount) {
		ActiveInverterCount = activeInverterCount;
	}
	public String getInverterCount() {
		return InverterCount;
	}
	public void setInverterCount(String inverterCount) {
		InverterCount = inverterCount;
	}
	public String getLastChecked() {
		return LastChecked;
	}
	public void setLastChecked(String lastChecked) {
		LastChecked = lastChecked;
	}
	public Double getTotalEnergy() {
		return TotalEnergy;
	}
	public void setTotalEnergy(Double totalEnergy) {
		TotalEnergy = totalEnergy;
	}
	public Double getTodayEnergy() {
		return TodayEnergy;
	}
	public void setTodayEnergy(Double todayEnergy) {
		TodayEnergy = todayEnergy;
	}
	public String getLastDownTime() {
		return LastDownTime;
	}
	public void setLastDownTime(String lastDownTime) {
		LastDownTime = lastDownTime;
	}
	public String getLastDataReceived() {
		return LastDataReceived;
	}
	public void setLastDataReceived(String lastDataReceived) {
		LastDataReceived = lastDataReceived;
	}
	public String getTodayHoursOn() {
		return TodayHoursOn;
	}
	public void setTodayHoursOn(String todayHoursOn) {
		TodayHoursOn = todayHoursOn;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}
	
	
	public Double getInstallationCapacity() {
		return InstallationCapacity;
	}

	public void setInstallationCapacity(Double installationCapacity) {
		InstallationCapacity = installationCapacity;
	}

	
}
