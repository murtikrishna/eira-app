
/******************************************************
 * 
 *    	Filename	: GraphDataBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for Graph data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

import java.util.Date;

import javax.persistence.Column;

public class GraphDataBean {

	private Date Timestamp;

	private Double Value;

	public Date getTimestamp() {
		return Timestamp;
	}

	public void setTimestamp(Date timestamp) {
		Timestamp = timestamp;
	}

	public Double getValue() {
		return Value;
	}

	public void setValue(Double value) {
		Value = value;
	}

}
