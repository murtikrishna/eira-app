
/******************************************************
 * 
 *    	Filename	: EquipmentListBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Equipment List data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class EquipmentListBean {

	private String SiteID;
	private String EquipmentID;
	private String EquipmentCode;
	private String EquipmentReference;
	private String EquipmentName;
	private String EquipmentType;
	private String CustomerNaming;

	public String getCustomerNaming() {
		return CustomerNaming;
	}

	public void setCustomerNaming(String customerNaming) {
		CustomerNaming = customerNaming;
	}

	private String ChartJsonData1;
	private String ChartJsonData1Parameter;
	private String ChartJsonData1GraphValue;

	private String InverterEfficiency;

	public String getInverterEfficiency() {
		return InverterEfficiency;
	}

	public void setInverterEfficiency(String inverterEfficiency) {
		InverterEfficiency = inverterEfficiency;
	}

	public String getSiteID() {
		return SiteID;
	}

	public void setSiteID(String siteID) {
		SiteID = siteID;
	}

	public String getEquipmentID() {
		return EquipmentID;
	}

	public void setEquipmentID(String equipmentID) {
		EquipmentID = equipmentID;
	}

	public String getEquipmentCode() {
		return EquipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		EquipmentCode = equipmentCode;
	}

	public String getEquipmentReference() {
		return EquipmentReference;
	}

	public void setEquipmentReference(String equipmentReference) {
		EquipmentReference = equipmentReference;
	}

	public String getEquipmentName() {
		return EquipmentName;
	}

	public void setEquipmentName(String equipmentName) {
		EquipmentName = equipmentName;
	}

	public String getEquipmentType() {
		return EquipmentType;
	}

	public void setEquipmentType(String equipmentType) {
		EquipmentType = equipmentType;
	}

	public String getNetworkStatus() {
		return NetworkStatus;
	}

	public void setNetworkStatus(String networkStatus) {
		NetworkStatus = networkStatus;
	}

	public String getTodayEnergy() {
		return TodayEnergy;
	}

	public void setTodayEnergy(String todayEnergy) {
		TodayEnergy = todayEnergy;
	}

	public String getTotalEnergy() {
		return TotalEnergy;
	}

	public void setTotalEnergy(String totalEnergy) {
		TotalEnergy = totalEnergy;
	}

	public String getPerformanceRatio() {
		return PerformanceRatio;
	}

	public void setPerformanceRatio(String performanceRatio) {
		PerformanceRatio = performanceRatio;
	}

	public String getLastUpdate() {
		return LastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		LastUpdate = lastUpdate;
	}

	private String NetworkStatus;

	private String TodayEnergy;
	private String TotalEnergy;
	private String PerformanceRatio;
	private String LastUpdate;

	public String getChartJsonData1() {
		return ChartJsonData1;
	}

	public void setChartJsonData1(String chartJsonData1) {
		ChartJsonData1 = chartJsonData1;
	}

	public String getChartJsonData1Parameter() {
		return ChartJsonData1Parameter;
	}

	public void setChartJsonData1Parameter(String chartJsonData1Parameter) {
		ChartJsonData1Parameter = chartJsonData1Parameter;
	}

	public String getChartJsonData1GraphValue() {
		return ChartJsonData1GraphValue;
	}

	public void setChartJsonData1GraphValue(String chartJsonData1GraphValue) {
		ChartJsonData1GraphValue = chartJsonData1GraphValue;
	}

	private String ActivePower;
	private String ReactivePower;
	private String PowerFactor;

	public String getActivePower() {
		return ActivePower;
	}

	public void setActivePower(String activePower) {
		ActivePower = activePower;
	}

	public String getReactivePower() {
		return ReactivePower;
	}

	public void setReactivePower(String reactivePower) {
		ReactivePower = reactivePower;
	}

	public String getPowerFactor() {
		return PowerFactor;
	}

	public void setPowerFactor(String powerFactor) {
		PowerFactor = powerFactor;
	}

	private String Power;
	private String Voltage;
	private String Current;
	private String Temp;

	public String getPower() {
		return Power;
	}

	public void setPower(String power) {
		Power = power;
	}

	public String getVoltage() {
		return Voltage;
	}

	public void setVoltage(String voltage) {
		Voltage = voltage;
	}

	public String getCurrent() {
		return Current;
	}

	public void setCurrent(String current) {
		Current = current;
	}

	public String getTemp() {
		return Temp;
	}

	public void setTemp(String temp) {
		Temp = temp;
	}

	private String Sunangle;
	private String Trackerangle;
	private String Windspeed;
	private String Mode;

	public String getSunangle() {
		return Sunangle;
	}

	public void setSunangle(String sunangle) {
		Sunangle = sunangle;
	}

	public String getTrackerangle() {
		return Trackerangle;
	}

	public void setTrackerangle(String trackerangle) {
		Trackerangle = trackerangle;
	}

	public String getWindspeed() {
		return Windspeed;
	}

	public void setWindspeed(String windspeed) {
		Windspeed = windspeed;
	}

	public String getMode() {
		return Mode;
	}

	public void setMode(String mode) {
		Mode = mode;
	}
}
