
/******************************************************
 * 
 *    	Filename	: CustomerListBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Customer List data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class CustomerListBean {

	private Integer CustomerId;
	private String CustomerCode;
	private String CustomerReference;
	private String CustomerName;
	private String Address;
	private String ContactPerson;
	private String Mobile;
	private String TotalSites;

	public CustomerListBean(Integer customerId, String customerCode, String customerReference, String customerName,
			String address, String contactPerson, String mobile, String totalSites) {
		super();
		CustomerId = customerId;
		CustomerCode = customerCode;
		CustomerReference = customerReference;
		CustomerName = customerName;
		Address = address;
		ContactPerson = contactPerson;
		Mobile = mobile;
		TotalSites = totalSites;
	}

	public Integer getCustomerId() {
		return CustomerId;
	}

	public void setCustomerId(Integer customerId) {
		CustomerId = customerId;
	}

	public String getCustomerCode() {
		return CustomerCode;
	}

	public void setCustomerCode(String customerCode) {
		CustomerCode = customerCode;
	}

	public String getCustomerReference() {
		return CustomerReference;
	}

	public void setCustomerReference(String customerReference) {
		CustomerReference = customerReference;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getContactPerson() {
		return ContactPerson;
	}

	public void setContactPerson(String contactPerson) {
		ContactPerson = contactPerson;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getTotalSites() {
		return TotalSites;
	}

	public void setTotalSites(String totalSites) {
		TotalSites = totalSites;
	}

}
