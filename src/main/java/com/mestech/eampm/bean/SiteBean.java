
/******************************************************
 * 
 *    	Filename	: SiteBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Site  data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

public class SiteBean {

	private Integer SiteId;

	private Integer CustomerID;

	private String CustomerName;

	private Integer SiteTypeID;

	private String SiteTypeName;

	private String SiteCode;

	private String SiteName;

	private String SiteDescription;

	private String ContactPerson;

	private String Address;

	private String City;

	private Integer StateID;

	private String StateName;

	private Integer CountryID;

	private String CountryName;

	private String PostalCode;

	private String Longitude;

	private String Latitude;

	private String Altitude;

	private String SiteImage;

	private double InstallationCapacity;

	private String SitePONumber;

	/*
	 * @Column(name="SitePODate") private Date SitePODate;
	 * 
	 * @Column(name="SiteHandoverDate") private Date SiteHandoverDate;
	 * 
	 * 
	 * @Column(name="SiteCommisioningDate") private Date SiteCommisioningDate;
	 * 
	 * 
	 * @Column(name="InstalledOn") private Date InstalledOn;
	 */

	private String SiteOperator;

	private String SiteManafacturer;

	private String ModuleName;

	private String CommunicationType;

	private String CollectionType;

	private String FileType;

	private String CustomerReference;

	private String CustomerNaming;

	/*
	 * @Column(name="Income") private float Income;
	 */

	private Integer CurrencyID;

	private String CurrencyName;

	private String EmailID;

	private String Mobile;

	private String Telephone;

	private String Fax;

	private Integer ActiveFlag;

	private Date CreationDate;

	private Integer CreatedBy;

	private Date LastUpdatedDate;

	private Integer LastUpdatedBy;

	private Integer DataLoggerID;

	private Integer OperationMode;

	private String RemoteFtpServer;

	private String RemoteFtpUserName;

	private String RemoteFtpPassword;

	private String RemoteFtpServerPort;

	private String RemoteFtpDirectoryPath;

	private String FilePrefix;

	private String FileSuffix;

	private String FileSubstring;

	private Integer SubFolder;

	private String LocalFtpDirectory;

	private String LocalFtpDirectoryPath;

	private String LocalFtpHomeDirectory;

	private String LocalFtpUserName;

	private String LocalFtpPassword;

	private String ApiUrl;

	private String ApiKey;

	private String ServiceCode;

	private String LocalFtpDirectoryPath_Sensor;

	private String LocalFtpDirectoryPath_Alarm;

	private String LocalFtpDirectoryPath_Log;

	private String LocalFtpDirectory_Sensor;

	private String LocalFtpDirectory_Alarm;

	private String LocalFtpDirectory_Log;

	private Integer DataLoggerID_Sensor;

	private Integer DataLoggerID_Alarm;

	private Integer DataLoggerID_Log;

	private String ServiceCode_Sensor;

	private String ServiceCode_Alarm;

	private String ServiceCode_Log;

	private String Timezone;

	private String LocalFtpDirectory_Modbus;

	private String LocalFtpDirectory_Inverter;

	private String LocalFtpDirectory_Data;

	private String LocalFtpDirectoryPath_Modbus;

	private String LocalFtpDirectoryPath_Inverter;

	private String LocalFtpDirectoryPath_Data;

	private Integer DataLoggerID_Modbus;

	private Integer DataLoggerID_Inverter;

	private Integer DataLoggerID_Data;

	private String ServiceCode_Modbus;

	private String ServiceCode_Inverter;

	private String ServiceCode_Data;

}
