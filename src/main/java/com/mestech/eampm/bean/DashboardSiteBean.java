
/******************************************************
 * 
 *    	Filename	: DashboardSiteBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Dashboard,site data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class DashboardSiteBean {

	private String TotalSites;
	private String RoofTopSites;
	private String UtilitySites;
	private String OfflineStatus;
	private String ActiveStatus;
	private String WarningStatus;
	private String DownStatus;

	public DashboardSiteBean(String totalSites, String roofTopSites, String utilitySites, String offlineStatus,
			String activeStatus, String warningStatus, String downStatus) {
		super();
		TotalSites = totalSites;
		RoofTopSites = roofTopSites;
		UtilitySites = utilitySites;
		OfflineStatus = offlineStatus;
		ActiveStatus = activeStatus;
		WarningStatus = warningStatus;
		DownStatus = downStatus;
	}

	public String getTotalSites() {
		return TotalSites;
	}

	public void setTotalSites(String totalSites) {
		TotalSites = totalSites;
	}

	public String getRoofTopSites() {
		return RoofTopSites;
	}

	public void setRoofTopSites(String roofTopSites) {
		RoofTopSites = roofTopSites;
	}

	public String getUtilitySites() {
		return UtilitySites;
	}

	public void setUtilitySites(String utilitySites) {
		UtilitySites = utilitySites;
	}

	public String getOfflineStatus() {
		return OfflineStatus;
	}

	public void setOfflineStatus(String offlineStatus) {
		OfflineStatus = offlineStatus;
	}

	public String getActiveStatus() {
		return ActiveStatus;
	}

	public void setActiveStatus(String activeStatus) {
		ActiveStatus = activeStatus;
	}

	public String getWarningStatus() {
		return WarningStatus;
	}

	public void setWarningStatus(String warningStatus) {
		WarningStatus = warningStatus;
	}

	public String getDownStatus() {
		return DownStatus;
	}

	public void setDownStatus(String downStatus) {
		DownStatus = downStatus;
	}

}
