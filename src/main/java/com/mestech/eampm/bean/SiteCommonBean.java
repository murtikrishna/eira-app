/******************************************************
 * 
 *    	Filename	: SiteCommonBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Site data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class SiteCommonBean {

	private Double Equipmentscapacity;

	private int EnergymeterCount;

	public int getEnergymeterCount() {
		return EnergymeterCount;
	}

	public void setEnergymeterCount(int energymeterCount) {
		EnergymeterCount = energymeterCount;
	}

	public Double getEquipmentscapacity() {
		return Equipmentscapacity;
	}

	public void setEquipmentscapacity(Double equipmentscapacity) {
		Equipmentscapacity = equipmentscapacity;
	}

}
