
/******************************************************
 * 
 *    	Filename	: CustomerViewBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Customer View data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

import java.util.Date;

public class CustomerViewBean {

	private Integer UserID;
	private Integer DivID;
	private String UserName;
	private String DataSearch;

	public String getUserName() {
		return UserName;
	}

	public String getDataSearch() {
		return DataSearch;
	}

	public void setDataSearch(String dataSearch) {
		DataSearch = dataSearch;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public Integer getDivID() {
		return DivID;
	}

	public void setDivID(Integer divID) {
		DivID = divID;
	}

	public Integer getUserID() {
		return UserID;
	}

	public void setUserID(Integer userID) {
		UserID = userID;
	}

	private String CustomerID;
	private String CustomerName;
	private String CustomerCode;
	private String CustomerAddress;
	private String ContactPerson;
	private String TotalCapacity;
	private String Customerreference;

	public String getCustomerreference() {
		return Customerreference;
	}

	public void setCustomerreference(String customerreference) {
		Customerreference = customerreference;
	}

	public String getTotalCapacity() {
		return TotalCapacity;
	}

	public void setTotalCapacity(String totalCapacity) {
		TotalCapacity = totalCapacity;
	}

	public String getContactPerson() {
		return ContactPerson;
	}

	public void setContactPerson(String contactPerson) {
		ContactPerson = contactPerson;
	}

	private String CustomerType;
	private String CustomerOrg;
	private String CustomerEmailId;
	private String CustomerMobileNo;
	private String CustomerTelephoneNo;
	private String CustomerWebsite;

	public String getCustomerType() {
		return CustomerType;
	}

	public void setCustomerType(String customerType) {
		CustomerType = customerType;
	}

	public String getCustomerOrg() {
		return CustomerOrg;
	}

	public void setCustomerOrg(String customerOrg) {
		CustomerOrg = customerOrg;
	}

	public String getCustomerEmailId() {
		return CustomerEmailId;
	}

	public void setCustomerEmailId(String customerEmailId) {
		CustomerEmailId = customerEmailId;
	}

	public String getCustomerMobileNo() {
		return CustomerMobileNo;
	}

	public void setCustomerMobileNo(String customerMobileNo) {
		CustomerMobileNo = customerMobileNo;
	}

	public String getCustomerTelephoneNo() {
		return CustomerTelephoneNo;
	}

	public void setCustomerTelephoneNo(String customerTelephoneNo) {
		CustomerTelephoneNo = customerTelephoneNo;
	}

	public String getCustomerWebsite() {
		return CustomerWebsite;
	}

	public void setCustomerWebsite(String customerWebsite) {
		CustomerWebsite = customerWebsite;
	}

	private String RooftopCount;
	private String UtilityCount;
	private String RooftopUtilityCount;
	private String LastDownTime;

	private String TotalSiteCount;
	private String ActiveSiteCount;
	private String WarningSiteCount;
	private String DownSiteCount;
	private String OfflineSiteCount;
	private String TotalDownTime;

	private String TodayEnergy;
	private String TotalEnergy;
	private String LastUpdatedTime;
	private String EventTime;

	public String getEventTime() {
		return EventTime;
	}

	public void setEventTime(String eventTime) {
		EventTime = eventTime;
	}

	private String ProductionDate;

	private String OpenTicketCount;
	private String InprocessCount;
	private String YettoStartCount;
	private String CompletedTicketCount;
	private String TicketMessages;

	private String TodayCo2Avoided;
	private String TotalCo2Avoided;

	private String TodayEventCount;
	private String TotalEventCount;

	private String TotalProductionYield;
	private String TodayProductionYield;
	private String TotalPerformanceRatio;
	private String TodayPerformanceRatio;

	public String getMapJsonData() {
		return MapJsonData;
	}

	public void setMapJsonData(String mapJsonData) {
		MapJsonData = mapJsonData;
	}

	public String getProdJsonData() {
		return ProdJsonData;
	}

	public void setProdJsonData(String prodJsonData) {
		ProdJsonData = prodJsonData;
	}

	private String MapJsonData;
	private String ProdJsonData;

	public String getRooftopCount() {
		return RooftopCount;
	}

	public void setRooftopCount(String rooftopCount) {
		RooftopCount = rooftopCount;
	}

	public String getUtilityCount() {
		return UtilityCount;
	}

	public void setUtilityCount(String utilityCount) {
		UtilityCount = utilityCount;
	}

	public String getRooftopUtilityCount() {
		return RooftopUtilityCount;
	}

	public void setRooftopUtilityCount(String rooftopUtilityCount) {
		RooftopUtilityCount = rooftopUtilityCount;
	}

	public String getLastDownTime() {
		return LastDownTime;
	}

	public void setLastDownTime(String lastDownTime) {
		LastDownTime = lastDownTime;
	}

	public String getCustomerID() {
		return CustomerID;
	}

	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getCustomerCode() {
		return CustomerCode;
	}

	public void setCustomerCode(String customerCode) {
		CustomerCode = customerCode;
	}

	public String getCustomerAddress() {
		return CustomerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		CustomerAddress = customerAddress;
	}

	public String getTotalSiteCount() {
		return TotalSiteCount;
	}

	public void setTotalSiteCount(String totalSiteCount) {
		TotalSiteCount = totalSiteCount;
	}

	public String getActiveSiteCount() {
		return ActiveSiteCount;
	}

	public void setActiveSiteCount(String activeSiteCount) {
		ActiveSiteCount = activeSiteCount;
	}

	public String getWarningSiteCount() {
		return WarningSiteCount;
	}

	public void setWarningSiteCount(String warningSiteCount) {
		WarningSiteCount = warningSiteCount;
	}

	public String getDownSiteCount() {
		return DownSiteCount;
	}

	public void setDownSiteCount(String downSiteCount) {
		DownSiteCount = downSiteCount;
	}

	public String getOfflineSiteCount() {
		return OfflineSiteCount;
	}

	public void setOfflineSiteCount(String offlineSiteCount) {
		OfflineSiteCount = offlineSiteCount;
	}

	public String getTotalDownTime() {
		return TotalDownTime;
	}

	public void setTotalDownTime(String totalDownTime) {
		TotalDownTime = totalDownTime;
	}

	public String getTicketMessages() {
		return TicketMessages;
	}

	public void setTicketMessages(String ticketMessages) {
		TicketMessages = ticketMessages;
	}

	public String getOpenTicketCount() {
		return OpenTicketCount;
	}

	public void setOpenTicketCount(String openTicketCount) {
		OpenTicketCount = openTicketCount;
	}

	public String getInprocessCount() {
		return InprocessCount;
	}

	public void setInprocessCount(String inprocessCount) {
		InprocessCount = inprocessCount;
	}

	public String getYettoStartCount() {
		return YettoStartCount;
	}

	public void setYettoStartCount(String yettoStartCount) {
		YettoStartCount = yettoStartCount;
	}

	public String getCompletedTicketCount() {
		return CompletedTicketCount;
	}

	public void setCompletedTicketCount(String completedTicketCount) {
		CompletedTicketCount = completedTicketCount;
	}

	public String getTodayEventCount() {
		return TodayEventCount;
	}

	public void setTodayEventCount(String todayEventCount) {
		TodayEventCount = todayEventCount;
	}

	public String getTotalEventCount() {
		return TotalEventCount;
	}

	public void setTotalEventCount(String totalEventCount) {
		TotalEventCount = totalEventCount;
	}

	public String getTodayEnergy() {
		return TodayEnergy;
	}

	public void setTodayEnergy(String todayEnergy) {
		TodayEnergy = todayEnergy;
	}

	public String getTotalEnergy() {
		return TotalEnergy;
	}

	public void setTotalEnergy(String totalEnergy) {
		TotalEnergy = totalEnergy;
	}

	public String getLastUpdatedTime() {
		return LastUpdatedTime;
	}

	public void setLastUpdatedTime(String lastUpdatedTime) {
		LastUpdatedTime = lastUpdatedTime;
	}

	public String getProductionDate() {
		return ProductionDate;
	}

	public void setProductionDate(String productionDate) {
		ProductionDate = productionDate;
	}

	public String getTodayCo2Avoided() {
		return TodayCo2Avoided;
	}

	public void setTodayCo2Avoided(String todayCo2Avoided) {
		TodayCo2Avoided = todayCo2Avoided;
	}

	public String getTotalCo2Avoided() {
		return TotalCo2Avoided;
	}

	public void setTotalCo2Avoided(String totalCo2Avoided) {
		TotalCo2Avoided = totalCo2Avoided;
	}

	public String getTotalProductionYield() {
		return TotalProductionYield;
	}

	public void setTotalProductionYield(String totalProductionYield) {
		TotalProductionYield = totalProductionYield;
	}

	public String getTodayProductionYield() {
		return TodayProductionYield;
	}

	public void setTodayProductionYield(String todayProductionYield) {
		TodayProductionYield = todayProductionYield;
	}

	public String getTotalPerformanceRatio() {
		return TotalPerformanceRatio;
	}

	public void setTotalPerformanceRatio(String totalPerformanceRatio) {
		TotalPerformanceRatio = totalPerformanceRatio;
	}

	public String getTodayPerformanceRatio() {
		return TodayPerformanceRatio;
	}

	public void setTodayPerformanceRatio(String todayPerformanceRatio) {
		TodayPerformanceRatio = todayPerformanceRatio;
	}

	public String Loadfactor;

	public String getLoadfactor() {
		return Loadfactor;
	}

	public void setLoadfactor(String loadfactor) {
		Loadfactor = loadfactor;
	}
}