
/******************************************************
 * 
 *    	Filename	: TicketHistory.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Ticket history data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class TicketHistory {

	private Integer UserID;
	private Integer DivID;
	private String UserName;
	private Integer SiteID;
	private String TicketID;
	private String SerialNo;
	private String TicketDate;
	private String TicketTransaction;
	private String Remarks;

	public Integer getUserID() {
		return UserID;
	}

	public void setUserID(Integer userID) {
		UserID = userID;
	}

	public String getSerialNo() {
		return SerialNo;
	}

	public void setSerialNo(String serialNo) {
		SerialNo = serialNo;
	}

	public String getTicketDate() {
		return TicketDate;
	}

	public void setTicketDate(String ticketDate) {
		TicketDate = ticketDate;
	}

	public Integer getDivID() {
		return DivID;
	}

	public void setDivID(Integer divID) {
		DivID = divID;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public Integer getSiteID() {
		return SiteID;
	}

	public void setSiteID(Integer siteID) {
		SiteID = siteID;
	}

	public String getTicketID() {
		return TicketID;
	}

	public void setTicketID(String ticketID) {
		TicketID = ticketID;
	}

	public String getTicketTransaction() {
		return TicketTransaction;
	}

	public void setTicketTransaction(String ticketTransaction) {
		TicketTransaction = ticketTransaction;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}

}
