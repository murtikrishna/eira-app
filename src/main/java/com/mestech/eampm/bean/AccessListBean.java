
/******************************************************
 * 
 *    	Filename	: AccessListBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle access data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

 
public class AccessListBean {

	private Integer FilterFlag = 0;

	public Integer getFilterFlag() {
		return FilterFlag;
	}

	public void setFilterFlag(Integer filterFlag) {
		FilterFlag = filterFlag;
	}

	private Integer UserID;

	public Integer getMyCustomerID() {
		return MyCustomerID;
	}

	public void setMyCustomerID(Integer myCustomerID) {
		MyCustomerID = myCustomerID;
	}

	private Integer MyCustomerID;

	public String getMyCustomerName() {
		return MyCustomerName;
	}

	public void setMyCustomerName(String myCustomerName) {
		MyCustomerName = myCustomerName;
	}

	private String MyCustomerName;
	private String MySiteName;
	private Integer MySiteID;

	public boolean isMySiteFilter() {
		return MySiteFilter;
	}

	public void setMySiteFilter(boolean mySiteFilter) {
		MySiteFilter = mySiteFilter;
	}

	private boolean MySiteFilter;

	public String getMySiteName() {
		return MySiteName;
	}

	public void setMySiteName(String mySiteName) {
		MySiteName = mySiteName;
	}

	public Integer getMySiteID() {
		return MySiteID;
	}

	public void setMySiteID(Integer mySiteID) {
		MySiteID = mySiteID;
	}

	private String UserName;

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	private String Dashboard;
	private String Overview;
	private String SystemMonitoring;
	private String Visualization;
	private String Analytics;
	private String PortfolioManagement;
	private String Ticketing;
	private String Forcasting;

	public String getAnalysis() {
		return Analysis;
	}

	public void setAnalysis(String analysis) {
		Analysis = analysis;
	}

	private String CustomerListView;
	private String Analysis;

	public String getCustomerListView() {
		return CustomerListView;
	}

	public void setCustomerListView(String customerListView) {
		CustomerListView = customerListView;
	}

	public Integer getUserID() {
		return UserID;
	}

	public void setUserID(Integer userID) {
		UserID = userID;
	}

	public String getDashboard() {
		return Dashboard;
	}

	public void setDashboard(String dashboard) {
		Dashboard = dashboard;
	}

	public String getOverview() {
		return Overview;
	}

	public void setOverview(String overview) {
		Overview = overview;
	}

	public String getSystemMonitoring() {
		return SystemMonitoring;
	}

	public void setSystemMonitoring(String systemMonitoring) {
		SystemMonitoring = systemMonitoring;
	}

	public String getVisualization() {
		return Visualization;
	}

	public void setVisualization(String visualization) {
		Visualization = visualization;
	}

	public String getAnalytics() {
		return Analytics;
	}

	public void setAnalytics(String analytics) {
		Analytics = analytics;
	}

	public String getPortfolioManagement() {
		return PortfolioManagement;
	}

	public void setPortfolioManagement(String portfolioManagement) {
		PortfolioManagement = portfolioManagement;
	}

	public String getTicketing() {
		return Ticketing;
	}

	public void setTicketing(String ticketing) {
		Ticketing = ticketing;
	}

	public String getForcasting() {
		return Forcasting;
	}

	public void setForcasting(String forcasting) {
		Forcasting = forcasting;
	}

	public String getConfiguration() {
		return Configuration;
	}

	public void setConfiguration(String configuration) {
		Configuration = configuration;
	}

	private String Configuration;

	private boolean MyCustomerFilter;

	public boolean isMyCustomerFilter() {
		return MyCustomerFilter;
	}

	public void setMyCustomerFilter(boolean myCustomerFilter) {
		MyCustomerFilter = myCustomerFilter;
	}

	private Integer Ticketid;

	public Integer getTicketid() {
		return Ticketid;
	}

	public void setTicketid(Integer ticketid) {
		Ticketid = ticketid;
	}

	private String MonitoringView;

	public String getMonitoringView() {
		return MonitoringView;
	}

	public void setMonitoringView(String monitoringView) {
		MonitoringView = monitoringView;
	}
}
