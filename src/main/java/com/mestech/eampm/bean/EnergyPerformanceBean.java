

/******************************************************
 * 
 *    	Filename	: EnergyPerformanceBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Energy Perfomance data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

import java.util.Date;

public class EnergyPerformanceBean {

	 private Date Timestamp;
	 	 
	 private Double TotalEnergy;
	 
	 private Double TodayEnergy;

	public Date getTimestamp() {
		return Timestamp;
	}

	public void setTimestamp(Date timestamp) {
		Timestamp = timestamp;
	}

	public Double getTotalEnergy() {
		return TotalEnergy;
	}

	public void setTotalEnergy(Double totalEnergy) {
		TotalEnergy = totalEnergy;
	}

	public Double getTodayEnergy() {
		return TodayEnergy;
	}

	public void setTodayEnergy(Double todayEnergy) {
		TodayEnergy = todayEnergy;
	}
}
