
/******************************************************
 * 
 *    	Filename	: SiteDataloggersBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle site datalogger data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class SiteDataloggersBean {

	public Integer Dataloggerid_scb;
	public Integer Dataloggerid_sensor;
	public Integer Dataloggerid_energymeter;
	public Integer Dataloggerid_inverter;
	public Integer Dataloggerid_modbus;

	public Integer getDataloggerid_scb() {
		return Dataloggerid_scb;
	}

	public void setDataloggerid_scb(Integer dataloggerid_scb) {
		Dataloggerid_scb = dataloggerid_scb;
	}

	public Integer getDataloggerid_sensor() {
		return Dataloggerid_sensor;
	}

	public void setDataloggerid_sensor(Integer dataloggerid_sensor) {
		Dataloggerid_sensor = dataloggerid_sensor;
	}

	public Integer getDataloggerid_energymeter() {
		return Dataloggerid_energymeter;
	}

	public void setDataloggerid_energymeter(Integer dataloggerid_energymeter) {
		Dataloggerid_energymeter = dataloggerid_energymeter;
	}

	public Integer getDataloggerid_inverter() {
		return Dataloggerid_inverter;
	}

	public void setDataloggerid_inverter(Integer dataloggerid_inverter) {
		Dataloggerid_inverter = dataloggerid_inverter;
	}

	public Integer getDataloggerid_modbus() {
		return Dataloggerid_modbus;
	}

	public void setDataloggerid_modbus(Integer dataloggerid_modbus) {
		Dataloggerid_modbus = dataloggerid_modbus;
	}

	public Integer getDataloggerid_tracker() {
		return Dataloggerid_tracker;
	}

	public void setDataloggerid_tracker(Integer dataloggerid_tracker) {
		Dataloggerid_tracker = dataloggerid_tracker;
	}

	public Integer Dataloggerid_tracker;
}
