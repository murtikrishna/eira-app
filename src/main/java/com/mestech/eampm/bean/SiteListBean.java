
/******************************************************
 * 
 *    	Filename	: SiteListBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle site List data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class SiteListBean {

	private String SiteID;
	private String SiteTypeID;
	private String SiteCode;
	private String SiteReference;
	private Double InstallationCapacity;

	public Double getInstallationCapacity() {
		return InstallationCapacity;
	}

	public void setInstallationCapacity(Double installationCapacity) {
		InstallationCapacity = installationCapacity;
	}

	public String getSiteTypeID() {
		return SiteTypeID;
	}

	public void setSiteTypeID(String siteTypeID) {
		SiteTypeID = siteTypeID;
	}

	private String SiteName;
	private String NetworkStatus;

	private String TodayEnergy;

	private String PerformanceRatio;
	private String Inverters;
	private String Ems;

	public String getEms() {
		return Ems;
	}

	public void setEms(String ems) {
		Ems = ems;
	}

	private String LastUpdate;
	private String LastDownTime;

	private String TotalEnergy;

	public String getLastDownTime() {
		return LastDownTime;
	}

	public void setLastDownTime(String lastDownTime) {
		LastDownTime = lastDownTime;
	}

	public String getSiteID() {
		return SiteID;
	}

	public void setSiteID(String siteID) {
		SiteID = siteID;
	}

	public String getSiteCode() {
		return SiteCode;
	}

	public void setSiteCode(String siteCode) {
		SiteCode = siteCode;
	}

	public String getSiteReference() {
		return SiteReference;
	}

	public void setSiteReference(String siteReference) {
		SiteReference = siteReference;
	}

	public String getSiteName() {
		return SiteName;
	}

	public void setSiteName(String siteName) {
		SiteName = siteName;
	}

	public String getNetworkStatus() {
		return NetworkStatus;
	}

	public void setNetworkStatus(String networkStatus) {
		NetworkStatus = networkStatus;
	}

	public String getTodayEnergy() {
		return TodayEnergy;
	}

	public void setTodayEnergy(String todayEnergy) {
		TodayEnergy = todayEnergy;
	}

	public String getTotalEnergy() {
		return TotalEnergy;
	}

	public void setTotalEnergy(String totalEnergy) {
		TotalEnergy = totalEnergy;
	}

	public String getPerformanceRatio() {
		return PerformanceRatio;
	}

	public void setPerformanceRatio(String performanceRatio) {
		PerformanceRatio = performanceRatio;
	}

	public String getInverters() {
		return Inverters;
	}

	public void setInverters(String inverters) {
		Inverters = inverters;
	}

	public String getLastUpdate() {
		return LastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		LastUpdate = lastUpdate;
	}

	private String LoadFactor;

	public String getLoadFactor() {
		return LoadFactor;
	}

	public void setLoadFactor(String loadFactor) {
		LoadFactor = loadFactor;
	}

}
