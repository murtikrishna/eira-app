
/******************************************************
 * 
 *    	Filename	: DashboardOandMBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Dashboard,ticket data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class DashboardOandMBean {

	private String TotalTicket;
	private String OpenTicket;
	private String ClosedTicket;
	private String TotalEvent;
	private String TodayEvent;

	public DashboardOandMBean(String totalTicket, String openTicket, String closedTicket, String totalEvent,
			String todayEvent) {
		super();
		TotalTicket = totalTicket;
		OpenTicket = openTicket;
		ClosedTicket = closedTicket;
		TotalEvent = totalEvent;
		TodayEvent = todayEvent;
	}

	public String getTotalTicket() {
		return TotalTicket;
	}

	public void setTotalTicket(String totalTicket) {
		TotalTicket = totalTicket;
	}

	public String getOpenTicket() {
		return OpenTicket;
	}

	public void setOpenTicket(String openTicket) {
		OpenTicket = openTicket;
	}

	public String getClosedTicket() {
		return ClosedTicket;
	}

	public void setClosedTicket(String closedTicket) {
		ClosedTicket = closedTicket;
	}

	public String getTotalEvent() {
		return TotalEvent;
	}

	public void setTotalEvent(String totalEvent) {
		TotalEvent = totalEvent;
	}

	public String getTodayEvent() {
		return TodayEvent;
	}

	public void setTodayEvent(String todayEvent) {
		TodayEvent = todayEvent;
	}

}
