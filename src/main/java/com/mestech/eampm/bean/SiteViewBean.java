
/******************************************************
 * 
 *    	Filename	: SiteViewBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle site view data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

import java.sql.Date;

public class SiteViewBean {
	private Integer UserID;
	private Integer DivID;
	private String UserName;
	private String DataSearch;

	private String ChartJsonData1;
	private String ChartJsonData1Parameter;
	private String ChartJsonData1GraphValue;

	public String getUserName() {
		return UserName;
	}

	public String getDataSearch() {
		return DataSearch;
	}

	public void setDataSearch(String dataSearch) {
		DataSearch = dataSearch;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public Integer getDivID() {
		return DivID;
	}

	public void setDivID(Integer divID) {
		DivID = divID;
	}

	public Integer getUserID() {
		return UserID;
	}

	public void setUserID(Integer userID) {
		UserID = userID;
	}

	private int EnergymeterCount;
	private String Co;
	private String TotalCo;

	public String getTotalCo() {
		return TotalCo;
	}

	public void setTotalCo(String totalCo) {
		TotalCo = totalCo;
	}

	private String LoadFactor;

	public String getCo() {
		return Co;
	}

	public void setCo(String co) {
		Co = co;
	}

	public String getLoadFactor() {
		return LoadFactor;
	}

	public void setLoadFactor(String loadFactor) {
		LoadFactor = loadFactor;
	}

	public int getEnergymeterCount() {
		return EnergymeterCount;
	}

	public void setEnergymeterCount(int energymeterCount) {
		EnergymeterCount = energymeterCount;
	}

	private String CustomerID;
	private String SiteID;
	private Double InstallationCapacity;
	private Double InvertersCapacity;

	public Double getInvertersCapacity() {
		return InvertersCapacity;
	}

	public void setInvertersCapacity(Double invertersCapacity) {
		InvertersCapacity = invertersCapacity;
	}

	private String CommisiongDate;

	public Double getInstallationCapacity() {
		return InstallationCapacity;
	}

	public void setInstallationCapacity(Double installationCapacity) {
		InstallationCapacity = installationCapacity;
	}

	public String getCommisiongDate() {
		return CommisiongDate;
	}

	public void setCommisiongDate(String commisiongDate) {
		CommisiongDate = commisiongDate;
	}

	public String getSiteRef() {
		return SiteRef;
	}

	public void setSiteRef(String siteRef) {
		SiteRef = siteRef;
	}

	private String SiteRef;
	private String SiteName;
	private String SiteAddress;

	public String getCustomerID() {
		return CustomerID;
	}

	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}

	public String getSiteID() {
		return SiteID;
	}

	public void setSiteID(String siteID) {
		SiteID = siteID;
	}

	public String getSiteName() {
		return SiteName;
	}

	public void setSiteName(String siteName) {
		SiteName = siteName;
	}

	public String getSiteAddress() {
		return SiteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		SiteAddress = siteAddress;
	}

	public String getSiteType() {
		return SiteType;
	}

	public void setSiteType(String siteType) {
		SiteType = siteType;
	}

	public String getSiteEmailId() {
		return SiteEmailId;
	}

	public void setSiteEmailId(String siteEmailId) {
		SiteEmailId = siteEmailId;
	}

	public String getSiteMobileNo() {
		return SiteMobileNo;
	}

	public void setSiteMobileNo(String siteMobileNo) {
		SiteMobileNo = siteMobileNo;
	}

	public String getSiteTelephoneNo() {
		return SiteTelephoneNo;
	}

	public void setSiteTelephoneNo(String siteTelephoneNo) {
		SiteTelephoneNo = siteTelephoneNo;
	}

	public String getSiteWebsite() {
		return SiteWebsite;
	}

	public void setSiteWebsite(String siteWebsite) {
		SiteWebsite = siteWebsite;
	}

	private String SiteType;
	private String SiteEmailId;
	private String SiteMobileNo;
	private String SiteTelephoneNo;
	private String SiteWebsite;
	private String SiteLocation;

	public String getSiteLocation() {
		return SiteLocation;
	}

	public void setSiteLocation(String siteLocation) {
		SiteLocation = siteLocation;
	}

	private String SiteCode;
	private String Capacity;
	private String EquipmentsCapacity;

	public String getEquipmentsCapacity() {
		return EquipmentsCapacity;
	}

	public void setEquipmentsCapacity(String equipmentsCapacity) {
		EquipmentsCapacity = equipmentsCapacity;
	}

	private String CommisionningDate;
	private String SiteStatus;

	private String TotalEquipment;

	public String getTotalEquipment() {
		return TotalEquipment;
	}

	public void setTotalEquipment(String totalEquipment) {
		TotalEquipment = totalEquipment;
	}

	private String TotalEnergy;
	private String TodayEnergy;
	private String ProductionDate;
	private String EnergyLastUpdate;

	private String TotalProductionYeild;
	private String TodayProductionYeild;
	private String YesterdayProductionYeild;

	private String TotalPerformanceRatio;
	private String TodayPerformanceRatio;
	private String YesterdayPerformanceRatio;

	private String LocationUrl;
	private String LocationName;

	private String WeatherStatus;
	private String WeatherTodayTemp;
	private String WeatherYesterdayTemp;
	private String WindSpeed;

	private String Irradiation;
	private String IrradiationLastUpdate;
	private String ModuleTemp;

	private String OpenTickets;
	private String CompletedTickets;

	private String TodayScheduledCleaningJobs;
	private String TodayScheduledPMJobs;
	private String TodayScheduledJobs;
	private String TotalScheduledJobs;

	private String TodayEvents;
	private String TotalEvents;

	private String TotalInverters;
	private String ActiveInverters;
	private String ErrorInverters;

	private String ActiveEms;
	private String ErrorEms;

	private String Modules;

	private String TodayCo2Avoided;
	private String TotalCo2;

	public String getEnergyLastUpdate() {
		return EnergyLastUpdate;
	}

	public void setEnergyLastUpdate(String energyLastUpdate) {
		EnergyLastUpdate = energyLastUpdate;
	}

	public String getSiteCode() {
		return SiteCode;
	}

	public void setSiteCode(String siteCode) {
		SiteCode = siteCode;
	}

	public String getCapacity() {
		return Capacity;
	}

	public void setCapacity(String capacity) {
		Capacity = capacity;
	}

	public String getSiteStatus() {
		return SiteStatus;
	}

	public String getCommisionningDate() {
		return CommisionningDate;
	}

	public void setCommisionningDate(String commisionningDate) {
		CommisionningDate = commisionningDate;
	}

	public void setSiteStatus(String siteStatus) {
		SiteStatus = siteStatus;
	}

	public String getTotalEnergy() {
		return TotalEnergy;
	}

	public void setTotalEnergy(String totalEnergy) {
		TotalEnergy = totalEnergy;
	}

	public String getTodayEnergy() {
		return TodayEnergy;
	}

	public void setTodayEnergy(String todayEnergy) {
		TodayEnergy = todayEnergy;
	}

	public String getProductionDate() {
		return ProductionDate;
	}

	public void setProductionDate(String productionDate) {
		ProductionDate = productionDate;
	}

	public String getTotalProductionYeild() {
		return TotalProductionYeild;
	}

	public void setTotalProductionYeild(String totalProductionYeild) {
		TotalProductionYeild = totalProductionYeild;
	}

	public String getTodayProductionYeild() {
		return TodayProductionYeild;
	}

	public void setTodayProductionYeild(String todayProductionYeild) {
		TodayProductionYeild = todayProductionYeild;
	}

	public String getYesterdayProductionYeild() {
		return YesterdayProductionYeild;
	}

	public void setYesterdayProductionYeild(String yesterdayProductionYeild) {
		YesterdayProductionYeild = yesterdayProductionYeild;
	}

	public String getTotalPerformanceRatio() {
		return TotalPerformanceRatio;
	}

	public void setTotalPerformanceRatio(String totalPerformanceRatio) {
		TotalPerformanceRatio = totalPerformanceRatio;
	}

	public String getTodayPerformanceRatio() {
		return TodayPerformanceRatio;
	}

	public void setTodayPerformanceRatio(String todayPerformanceRatio) {
		TodayPerformanceRatio = todayPerformanceRatio;
	}

	public String getYesterdayPerformanceRatio() {
		return YesterdayPerformanceRatio;
	}

	public void setYesterdayPerformanceRatio(String yesterdayPerformanceRatio) {
		YesterdayPerformanceRatio = yesterdayPerformanceRatio;
	}

	public String getLocationUrl() {
		return LocationUrl;
	}

	public void setLocationUrl(String locationUrl) {
		LocationUrl = locationUrl;
	}

	public String getLocationName() {
		return LocationName;
	}

	public void setLocationName(String locationName) {
		LocationName = locationName;
	}

	public String getWeatherStatus() {
		return WeatherStatus;
	}

	public void setWeatherStatus(String weatherStatus) {
		WeatherStatus = weatherStatus;
	}

	public String getWeatherTodayTemp() {
		return WeatherTodayTemp;
	}

	public void setWeatherTodayTemp(String weatherTodayTemp) {
		WeatherTodayTemp = weatherTodayTemp;
	}

	public String getWeatherYesterdayTemp() {
		return WeatherYesterdayTemp;
	}

	public void setWeatherYesterdayTemp(String weatherYesterdayTemp) {
		WeatherYesterdayTemp = weatherYesterdayTemp;
	}

	public String getWindSpeed() {
		return WindSpeed;
	}

	public void setWindSpeed(String windSpeed) {
		WindSpeed = windSpeed;
	}

	public String getIrradiation() {
		return Irradiation;
	}

	public void setIrradiation(String irradiation) {
		Irradiation = irradiation;
	}

	public String getIrradiationLastUpdate() {
		return IrradiationLastUpdate;
	}

	public void setIrradiationLastUpdate(String irradiationLastUpdate) {
		IrradiationLastUpdate = irradiationLastUpdate;
	}

	public String getModuleTemp() {
		return ModuleTemp;
	}

	public void setModuleTemp(String moduleTemp) {
		ModuleTemp = moduleTemp;
	}

	public String getOpenTickets() {
		return OpenTickets;
	}

	public void setOpenTickets(String openTickets) {
		OpenTickets = openTickets;
	}

	public String getCompletedTickets() {
		return CompletedTickets;
	}

	public void setCompletedTickets(String completedTickets) {
		CompletedTickets = completedTickets;
	}

	public String getTodayScheduledCleaningJobs() {
		return TodayScheduledCleaningJobs;
	}

	public void setTodayScheduledCleaningJobs(String todayScheduledCleaningJobs) {
		TodayScheduledCleaningJobs = todayScheduledCleaningJobs;
	}

	public String getTodayScheduledPMJobs() {
		return TodayScheduledPMJobs;
	}

	public void setTodayScheduledPMJobs(String todayScheduledPMJobs) {
		TodayScheduledPMJobs = todayScheduledPMJobs;
	}

	public String getTodayScheduledJobs() {
		return TodayScheduledJobs;
	}

	public void setTodayScheduledJobs(String todayScheduledJobs) {
		TodayScheduledJobs = todayScheduledJobs;
	}

	public String getTotalScheduledJobs() {
		return TotalScheduledJobs;
	}

	public void setTotalScheduledJobs(String totalScheduledJobs) {
		TotalScheduledJobs = totalScheduledJobs;
	}

	public String getTodayEvents() {
		return TodayEvents;
	}

	public void setTodayEvents(String todayEvents) {
		TodayEvents = todayEvents;
	}

	public String getTotalEvents() {
		return TotalEvents;
	}

	public void setTotalEvents(String totalEvents) {
		TotalEvents = totalEvents;
	}

	public String getTotalInverters() {
		return TotalInverters;
	}

	public void setTotalInverters(String totalInverters) {
		TotalInverters = totalInverters;
	}

	public String getActiveInverters() {
		return ActiveInverters;
	}

	public void setActiveInverters(String activeInverters) {
		ActiveInverters = activeInverters;
	}

	public String getErrorInverters() {
		return ErrorInverters;
	}

	public void setErrorInverters(String errorInverters) {
		ErrorInverters = errorInverters;
	}

	public String getModules() {
		return Modules;
	}

	public void setModules(String modules) {
		Modules = modules;
	}

	public String getTodayCo2Avoided() {
		return TodayCo2Avoided;
	}

	public void setTodayCo2Avoided(String todayCo2Avoided) {
		TodayCo2Avoided = todayCo2Avoided;
	}

	public String getTotalCo2() {
		return TotalCo2;
	}

	public void setTotalCo2(String totalCo2) {
		TotalCo2 = totalCo2;
	}

	public String getChartJsonData1() {
		return ChartJsonData1;
	}

	public void setChartJsonData1(String chartJsonData1) {
		ChartJsonData1 = chartJsonData1;
	}

	public String getChartJsonData1Parameter() {
		return ChartJsonData1Parameter;
	}

	public void setChartJsonData1Parameter(String chartJsonData1Parameter) {
		ChartJsonData1Parameter = chartJsonData1Parameter;
	}

	public String getChartJsonData1GraphValue() {
		return ChartJsonData1GraphValue;
	}

	public void setChartJsonData1GraphValue(String chartJsonData1GraphValue) {
		ChartJsonData1GraphValue = chartJsonData1GraphValue;
	}

	public String getActiveEms() {
		return ActiveEms;
	}

	public void setActiveEms(String activeEms) {
		ActiveEms = activeEms;
	}

	public String getErrorEms() {
		return ErrorEms;
	}

	public void setErrorEms(String errorEms) {
		ErrorEms = errorEms;
	}

	private Integer EquipmentCount;

	public Integer getEquipmentCount() {
		return EquipmentCount;
	}

	public void setEquipmentCount(Integer equipmentCount) {
		EquipmentCount = equipmentCount;
	}

	private String Annaulayeild;

	public String getAnnaulayeild() {
		return Annaulayeild;
	}

	public void setAnnaulayeild(String annaulayeild) {
		Annaulayeild = annaulayeild;
	}

}
