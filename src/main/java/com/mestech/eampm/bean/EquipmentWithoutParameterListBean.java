
/******************************************************
 * 
 *    	Filename	: EquipmentWithoutParameterListBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Equipment without parameter data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class EquipmentWithoutParameterListBean {

	private Integer SiteID;
	private Integer EquipmentID;
	private String EquipmentCode;
	private String EquipmentName;
	private String EquipmentReference;

	public Integer getSiteID() {
		return SiteID;
	}

	public void setSiteID(Integer siteID) {
		SiteID = siteID;
	}

	public Integer getEquipmentID() {
		return EquipmentID;
	}

	public void setEquipmentID(Integer equipmentID) {
		EquipmentID = equipmentID;
	}

	public String getEquipmentCode() {
		return EquipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		EquipmentCode = equipmentCode;
	}

	public String getEquipmentName() {
		return EquipmentName;
	}

	public void setEquipmentName(String equipmentName) {
		EquipmentName = equipmentName;
	}

	public String getEquipmentReference() {
		return EquipmentReference;
	}

	public void setEquipmentReference(String equipmentReference) {
		EquipmentReference = equipmentReference;
	}

}
