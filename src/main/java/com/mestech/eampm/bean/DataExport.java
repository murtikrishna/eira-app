
/******************************************************
 * 
 *    	Filename	: DataExport.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Export data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class DataExport {

	private Integer UserID;
	private Integer DivID;
	private String UserName;
	private Integer CustomerID;
	private Integer SiteID;
	private Integer EquipmentID;
	
	private String FromDate;
	private String ToDate;
	public Integer getUserID() {
		return UserID;
	}
	public void setUserID(Integer userID) {
		UserID = userID;
	}
	public Integer getDivID() {
		return DivID;
	}
	public void setDivID(Integer divID) {
		DivID = divID;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public Integer getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(Integer customerID) {
		CustomerID = customerID;
	}
	public Integer getSiteID() {
		return SiteID;
	}
	public void setSiteID(Integer siteID) {
		SiteID = siteID;
	}
	public Integer getEquipmentID() {
		return EquipmentID;
	}
	public void setEquipmentID(Integer equipmentID) {
		EquipmentID = equipmentID;
	}
	public String getFromDate() {
		return FromDate;
	}
	public void setFromDate(String fromDate) {
		FromDate = fromDate;
	}
	public String getToDate() {
		return ToDate;
	}
	public void setToDate(String toDate) {
		ToDate = toDate;
	}
	
	
}
