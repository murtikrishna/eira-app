
/******************************************************
 * 
 *    	Filename	: EquipmentCategoryBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Equipment category data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class EquipmentCategoryBean {

	private String EquipmentCategory;

	public String getEquipmentCategory() {
		return EquipmentCategory;
	}

	public void setEquipmentCategory(String equipmentCategory) {
		EquipmentCategory = equipmentCategory;
	}

}
