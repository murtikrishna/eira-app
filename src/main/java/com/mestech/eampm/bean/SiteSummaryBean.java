
/******************************************************
 * 
 *    	Filename	: SiteSummaryBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle site summary data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

import java.util.Date;

public class SiteSummaryBean {

	private String SummaryID;

	private String SiteID;

	private Date Timestamp;

	private Double TodayEnergy;

	public String getSummaryID() {
		return SummaryID;
	}

	public void setSummaryID(String summaryID) {
		SummaryID = summaryID;
	}

	public String getSiteID() {
		return SiteID;
	}

	public void setSiteID(String siteID) {
		SiteID = siteID;
	}

	public Date getTimestamp() {
		return Timestamp;
	}

	public void setTimestamp(Date timestamp) {
		Timestamp = timestamp;
	}

	public Double getTodayEnergy() {
		return TodayEnergy;
	}

	public void setTodayEnergy(Double todayEnergy) {
		TodayEnergy = todayEnergy;
	}
}
