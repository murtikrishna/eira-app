
/******************************************************
 * 
 *    	Filename	: EquipmentViewBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Equipment view data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class EquipmentViewBean {
	private String EquipmentCode;

	private Integer UserID;
	private Integer DivID;
	private String UserName;
	private String DataSearch;

	private String CustomerID;
	private String SiteID;
	private String EquipmentID;
	private String EquipmentName;

	public String getEquipmentCode() {
		return EquipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		EquipmentCode = equipmentCode;
	}

	public Integer getUserID() {
		return UserID;
	}

	public void setUserID(Integer userID) {
		UserID = userID;
	}

	public Integer getDivID() {
		return DivID;
	}

	public void setDivID(Integer divID) {
		DivID = divID;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getDataSearch() {
		return DataSearch;
	}

	public void setDataSearch(String dataSearch) {
		DataSearch = dataSearch;
	}

	public String getCustomerID() {
		return CustomerID;
	}

	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}

	public String getSiteID() {
		return SiteID;
	}

	public void setSiteID(String siteID) {
		SiteID = siteID;
	}

	public String getEquipmentID() {
		return EquipmentID;
	}

	public void setEquipmentID(String equipmentID) {
		EquipmentID = equipmentID;
	}

	public String getEquipmentName() {
		return EquipmentName;
	}

	public void setEquipmentName(String equipmentName) {
		EquipmentName = equipmentName;
	}

	public String getEquipmentType() {
		return EquipmentType;
	}

	public void setEquipmentType(String equipmentType) {
		EquipmentType = equipmentType;
	}

	public String getSiteName() {
		return SiteName;
	}

	public void setSiteName(String siteName) {
		SiteName = siteName;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getTotalEnergy() {
		return TotalEnergy;
	}

	public void setTotalEnergy(String totalEnergy) {
		TotalEnergy = totalEnergy;
	}

	public String getTodayEnergy() {
		return TodayEnergy;
	}

	public void setTodayEnergy(String todayEnergy) {
		TodayEnergy = todayEnergy;
	}

	public String getEquipmentStatus() {
		return EquipmentStatus;
	}

	public void setEquipmentStatus(String equipmentStatus) {
		EquipmentStatus = equipmentStatus;
	}

	public String getEnergyLastUpdate() {
		return EnergyLastUpdate;
	}

	public void setEnergyLastUpdate(String energyLastUpdate) {
		EnergyLastUpdate = energyLastUpdate;
	}

	private String EquipmentType;

	private String SiteName;
	private String CustomerName;

	private String TotalEnergy;
	private String TodayEnergy;
	private String EquipmentStatus;
	private String EnergyLastUpdate;

}
