
/******************************************************
 * 
 *    	Filename	: TicketFilter.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Ticket filter data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class TicketFilter {

	private Integer UserID;
	private Integer DivID;
	private String UserName;
	private Integer SiteID;
	private String TicketMode;
	private String TicketCategory;
	private String TicketType;
	private String State;

	private String Priority;
	private String Severity;

	private Integer Equipmentname;

	public Integer getEquipmentname() {
		return Equipmentname;
	}

	public void setEquipmentname(Integer equipmentname) {
		Equipmentname = equipmentname;
	}

	private String FromDate;

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	private String ToDate;

	public Integer getUserID() {
		return UserID;
	}

	public void setUserID(Integer userID) {
		UserID = userID;
	}

	public Integer getDivID() {
		return DivID;
	}

	public void setDivID(Integer divID) {
		DivID = divID;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getTicketMode() {
		return TicketMode;
	}

	public void setTicketMode(String ticketMode) {
		TicketMode = ticketMode;
	}

	public String getTicketCategory() {
		return TicketCategory;
	}

	public void setTicketCategory(String ticketCategory) {
		TicketCategory = ticketCategory;
	}

	public String getTicketType() {
		return TicketType;
	}

	public void setTicketType(String ticketType) {
		TicketType = ticketType;
	}

	public String getPriority() {
		return Priority;
	}

	public void setPriority(String priority) {
		Priority = priority;
	}

	public String getSeverity() {
		return Severity;
	}

	public void setSeverity(String severity) {
		Severity = severity;
	}

	public Integer getSiteID() {
		return SiteID;
	}

	public void setSiteID(Integer siteID) {
		SiteID = siteID;
	}

	public String getFromDate() {
		return FromDate;
	}

	public void setFromDate(String fromDate) {
		FromDate = fromDate;
	}

	public String getToDate() {
		return ToDate;
	}

	public void setToDate(String toDate) {
		ToDate = toDate;
	}

}
