
/******************************************************
 * 
 *    	Filename	: OpenStateTicketsCount.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Open state tickets data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class OpenStateTicketsCount {

	private String ticketstatus;

	private String Days;

	private String ticketcount;

	private String color;

	private String sitename;

	private String siteref;

	public String getSitename() {
		return sitename;
	}

	public void setSitename(String sitename) {
		this.sitename = sitename;
	}

	public String getSiteref() {
		return siteref;
	}

	public void setSiteref(String siteref) {
		this.siteref = siteref;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getTicketstatus() {
		return ticketstatus;
	}

	public void setTicketstatus(String ticketstatus) {
		this.ticketstatus = ticketstatus;
	}

	public String getDays() {
		return Days;
	}

	public void setDays(String days) {
		Days = days;
	}

	public String getTicketcount() {
		return ticketcount;
	}

	public void setTicketcount(String ticketcount) {
		this.ticketcount = ticketcount;
	}

}
