
/******************************************************
 * 
 *    	Filename	: EquipmentWithParameterListBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Equipment with parameter data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class EquipmentWithParameterListBean {

	private Integer SiteID;
	private Integer EquipmentID;
	private String EquipmentCategory;
	private String CustomerNaming;

	private Integer StandardId;
	private String StandardParameterName;
	private String ParameterDescription;
	private String StandardParameterUOM;

	public Integer getSiteID() {
		return SiteID;
	}

	public void setSiteID(Integer siteID) {
		SiteID = siteID;
	}

	public Integer getEquipmentID() {
		return EquipmentID;
	}

	public void setEquipmentID(Integer equipmentID) {
		EquipmentID = equipmentID;
	}

	public String getEquipmentCategory() {
		return EquipmentCategory;
	}

	public void setEquipmentCategory(String equipmentCategory) {
		EquipmentCategory = equipmentCategory;
	}

	public String getCustomerNaming() {
		return CustomerNaming;
	}

	public void setCustomerNaming(String customerNaming) {
		CustomerNaming = customerNaming;
	}

	public Integer getStandardId() {
		return StandardId;
	}

	public void setStandardId(Integer standardId) {
		StandardId = standardId;
	}

	public String getStandardParameterName() {
		return StandardParameterName;
	}

	public void setStandardParameterName(String standardParameterName) {
		StandardParameterName = standardParameterName;
	}

	public String getParameterDescription() {
		return ParameterDescription;
	}

	public void setParameterDescription(String parameterDescription) {
		ParameterDescription = parameterDescription;
	}

	public String getStandardParameterUOM() {
		return StandardParameterUOM;
	}

	public void setStandardParameterUOM(String standardParameterUOM) {
		StandardParameterUOM = standardParameterUOM;
	}

}
