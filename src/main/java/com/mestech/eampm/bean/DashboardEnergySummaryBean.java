
/******************************************************
 * 
 *    	Filename	: DashboardEnergySummaryBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Dashboard energy view data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class DashboardEnergySummaryBean {

	private String ProductionOn;
	private String LastChecked;
	private String LastDataReceived;
	private String LastDownTime;
	private Double TotalEnergy;
	private Double TodayEnergy;
	private Double TodayHoursOn;
	private Double Co2;
	private Double TotalCo2;

	public Double getTotalCo2() {
		return TotalCo2;
	}

	public void setTotalCo2(Double totalCo2) {
		TotalCo2 = totalCo2;
	}

	public Double getCo2() {
		return Co2;
	}

	public void setCo2(Double co2) {
		Co2 = co2;
	}

	public DashboardEnergySummaryBean(String productionOn, String lastChecked, String lastDataReceived,
			String lastDownTime, Double totalEnergy, Double todayEnergy, Double todayHoursOn, Double co2,
			Double totalCo2) {
		super();
		ProductionOn = productionOn;
		LastChecked = lastChecked;
		LastDataReceived = lastDataReceived;
		LastDownTime = lastDownTime;
		TotalEnergy = totalEnergy;
		TodayEnergy = todayEnergy;
		TodayHoursOn = todayHoursOn;
		Co2 = co2;
		TotalCo2 = totalCo2;
	}

	public String getProductionOn() {
		return ProductionOn;
	}

	public void setProductionOn(String productionOn) {
		ProductionOn = productionOn;
	}

	public String getLastChecked() {
		return LastChecked;
	}

	public void setLastChecked(String lastChecked) {
		LastChecked = lastChecked;
	}

	public String getLastDataReceived() {
		return LastDataReceived;
	}

	public void setLastDataReceived(String lastDataReceived) {
		LastDataReceived = lastDataReceived;
	}

	public String getLastDownTime() {
		return LastDownTime;
	}

	public void setLastDownTime(String lastDownTime) {
		LastDownTime = lastDownTime;
	}

	public Double getTotalEnergy() {
		return TotalEnergy;
	}

	public void setTotalEnergy(Double totalEnergy) {
		TotalEnergy = totalEnergy;
	}

	public Double getTodayEnergy() {
		return TodayEnergy;
	}

	public void setTodayEnergy(Double todayEnergy) {
		TodayEnergy = todayEnergy;
	}

	public Double getTodayHoursOn() {
		return TodayHoursOn;
	}

	public void setTodayHoursOn(Double todayHoursOn) {
		TodayHoursOn = todayHoursOn;
	}

}
