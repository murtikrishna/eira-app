
/******************************************************
 * 
 *    	Filename	: AnnualYeild.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle Annual yield data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

public class AnnualYeild {

	private Integer SiteID;
	private Double annualyeildvalue;

	public Double getAnnualyeildvalue() {
		return annualyeildvalue;
	}

	public void setAnnualyeildvalue(Double annualyeildvalue) {
		this.annualyeildvalue = annualyeildvalue;
	}

	public Integer getSiteID() {
		return SiteID;
	}

	public void setSiteID(Integer siteID) {
		SiteID = siteID;
	}

}