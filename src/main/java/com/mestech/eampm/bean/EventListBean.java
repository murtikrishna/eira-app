
/******************************************************
 * 
 *    	Filename	: EventListBean.java
 *      
 *      Author		: Sarath Babu E
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle event list data.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.bean;

import java.util.Date;

import javax.persistence.Column;

public class EventListBean {

	private Integer TransactionId;

	private String EventID;

	private String EventCode;

	private String ErrorID;

	private String ErrorCode;

	private String EquipmentID;

	private String EquipmentName;

	private String SiteID;

	private String SiteName;

	private String Severity;

	private String Priority;

	private String Remarks;

	private Date EventTimestamp;

	private Date ClosedTimeStamp;

	private String EventOccurrence;

	private String EventStatus;

	private String ActiveFlag;

	private Date CreationDate;

	private Date LastUpdatedDate;

	private Integer CreatedBy;

	private Integer LastUpdatedBy;

}
