
/******************************************************
 * 
 *    	Filename	: AppInitializer.java
 *      
 *      Author		: Johnson Edwinraj
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to initialize application.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		// TODO Auto-generated method stub
		return new Class[] { EiraApplication.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String[] getServletMappings() {
		// TODO Auto-generated method stub
		return new String[] { "/" };
	}

	// @Override
	// protected Filter[] getServletFilters() {
	// // DelegatingFilterProxy delegateFilterProxy = new DelegatingFilterProxy();
	// // delegateFilterProxy.setTargetBeanName("requestHandlerFilter");
	// return new Filter[] { new RequestHandler() };
	// }

}
