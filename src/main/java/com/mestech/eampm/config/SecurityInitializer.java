

/******************************************************
 * 
 *    	Filename	: SecurityInitializer.java
 *      
 *      Author		: Johnson Edwinraj
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is provides security features to application.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
