/// *****************************************************************
//
// Author :Johnson EdwinRaj S
//
// Copyright 2019.
//
// All rights reserved.
// ******************************************************************/
//
// package com.mestech.eampm.config;
//
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.beans.factory.annotation.Qualifier;
// import org.springframework.security.authentication.AuthenticationProvider;
// import
/// org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
// import org.springframework.security.core.Authentication;
// import org.springframework.security.core.AuthenticationException;
// import org.springframework.security.core.context.SecurityContextHolder;
// import org.springframework.security.core.userdetails.UserDetails;
// import org.springframework.security.core.userdetails.UserDetailsService;
// import
/// org.springframework.security.web.authentication.WebAuthenticationDetails;
// import org.springframework.stereotype.Component;
//
// import com.mestech.eampm.bean.LoginUserDetails;
//
// @Component("authenticateDecider")
// public class AuthenticateDecider implements AuthenticationProvider {
// @Autowired
// @Qualifier("userService")
// private UserDetailsService userDetailsService;
//
// @Override
// public Authentication authenticate(Authentication authentication) throws
/// AuthenticationException {
// WebAuthenticationDetails authenticationDetails = (WebAuthenticationDetails)
/// authentication.getDetails();
// if (authenticationDetails.getSessionId() != null &&
/// !authenticationDetails.getSessionId().equals("")) {
// UserDetails userDetails =
/// userDetailsService.loadUserByUsername(authentication.getName() + "#"
// + authentication.getCredentials().toString() + "#" +
/// authenticationDetails.getSessionId());
// if (userDetails != null) {
// LoginUserDetails loginUserDetails = new
/// LoginUserDetails(authentication.getName(),
// Integer.valueOf(userDetails.getUsername()), null,
/// authenticationDetails.getSessionId(),
// authenticationDetails.getRemoteAddress());
// SecurityContextHolder.getContext().setAuthentication(new
/// UsernamePasswordAuthenticationToken(
// loginUserDetails, userDetails.getPassword(), userDetails.getAuthorities()));
// return new UsernamePasswordAuthenticationToken(loginUserDetails,
/// userDetails.getPassword(),
// userDetails.getAuthorities());
// }
// }
// return null;
//
// }
//
// @Override
// public boolean supports(Class<?> authentication) {
// return authentication.equals(UsernamePasswordAuthenticationToken.class);
// }
// }
