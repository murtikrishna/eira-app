
/******************************************************
 * 
 *    	Filename	: CacheHandler.java
 *      
 *      Author		: Johnson Edwinraj
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to handle cache.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.config;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

public class CacheHandler {

	private CacheHandler() {

	}

	public static Cache getCache(String name) {
		CacheManager cacheManager = CacheManager.getInstance();
		return cacheManager.getCache(name);
	}
}
