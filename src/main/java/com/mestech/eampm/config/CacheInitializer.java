
/******************************************************
 * 
 *    	Filename	: CacheInitializer.java
 *      
 *      Author		: Johnson Edwinraj
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to initialize cache.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.config;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.ProtectedUrl;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.ProtectedPathService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.UserService;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

public class CacheInitializer {

	@Autowired
	private RoleActivityService roleActivityService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private UserService userService;
	@Autowired
	private ProtectedPathService protectedPathService;

	@SuppressWarnings("unchecked")
	@PostConstruct
	public void roleActivityInitializer() {
		Cache cache = CacheHandler.getCache("roleActivityCache");
		List<RoleActivity> roleActivities = roleActivityService.listRoleActivities();
		if (null != roleActivities && !roleActivities.isEmpty()) {
			for (RoleActivity roleActivity : roleActivities) {
				List<RoleActivity> activities = new ArrayList<>();
				if (cache.get(roleActivity.getRoleID()) != null) {
					Element element = cache.get(roleActivity.getRoleID());
					activities = (List<RoleActivity>) element.getObjectValue();
					activities.add(roleActivity);
					cache.put(new Element(roleActivity.getRoleID(), activities));
				} else {
					activities.add(roleActivity);
					cache.put(new Element(roleActivity.getRoleID(), activities));
				}

			}
		}
	}

	@PostConstruct
	public void activityInitializer() {
		Cache cache = CacheHandler.getCache("activityCache");
		List<Activity> activities = activityService.listActivities();
		if (null != activities && !activities.isEmpty()) {
			for (Activity activity : activities) {
				cache.put(new Element(activity.getActivityId(), activity));
			}
		}

	}

	@PostConstruct
	public void protectedPathInitializer() {
		Cache cache = CacheHandler.getCache("protectedActivity");
		List<Activity> activities = activityService.listActivities();
		if (null != activities && !activities.isEmpty()) {
			List<String> paths = new ArrayList<>();
			for (Activity activity : activities) {
				if (activity.getActivityUrl() != null)
					paths.add(activity.getActivityUrl());
			}
			cache.put(new Element("protectedPath", paths));
		}
	}

	@PostConstruct
	public void userInitializer() {
		Cache cache = CacheHandler.getCache("userCache");
		List<User> users = userService.listUsers();
		if (null != users && !users.isEmpty()) {
			for (User user : users) {
				cache.put(new Element(user.getUserId(), user));
			}
		}
	}

	@PostConstruct
	public void innerUrlProtector() {
		Cache cache = CacheHandler.getCache("innerUrl");
		List<ProtectedUrl> protectedUrls = protectedPathService.getAll();
		if (protectedUrls != null && !protectedUrls.isEmpty()) {
			List<String> paths = new ArrayList<>();
			for (ProtectedUrl protectedUrl : protectedUrls) {
				if (protectedUrl.getProtectedUrl() != null)
					paths.add(protectedUrl.getProtectedUrl());
				if (cache.get(protectedUrl.getActivityId()) == null) {
					List<String> urls = new ArrayList<>();
					if (protectedUrl.getProtectedUrl() != null) {
						urls.add(protectedUrl.getProtectedUrl());
						cache.put(new Element(protectedUrl.getActivityId(), urls));
					}
				} else {
					Element element = cache.get(protectedUrl.getActivityId());
					@SuppressWarnings("unchecked")
					List<String> urls = (List<String>) element.getObjectValue();
					if (protectedUrl.getProtectedUrl() != null)
						urls.add(protectedUrl.getProtectedUrl());
					cache.put(new Element(protectedUrl.getActivityId(), urls));
				}
			}
			cache.put(new Element("innerPath", paths));
		}
	}

}
