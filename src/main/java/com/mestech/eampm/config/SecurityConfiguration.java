
/******************************************************
 * 
 *    	Filename	: SecurityConfiguration.java
 *      
 *      Author		: Johnson Edwinraj
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used for secuiry.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	@Autowired
	@Qualifier("authenticationFailure")
	private AuthenticationFailureHandler authenticationFailureHandler;

	@Bean
	public RequestHandler requestHandler() {
		return new RequestHandler();
	}

	@Autowired
	public void globalUserDetails(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(requestHandler());
	}

	@Bean
	public BCryptPasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.addFilterBefore(requestHandler(), UsernamePasswordAuthenticationFilter.class);
		http.authorizeRequests().antMatchers("/resources/**", "/access-denied").permitAll();
		http.formLogin().loginPage("/login").loginProcessingUrl("/j_spring_security_check")
				.usernameParameter("userName").passwordParameter("password").defaultSuccessUrl("/dashboard")
				.failureUrl("/login").failureHandler(authenticationFailureHandler).and().exceptionHandling().and()
				.logout().logoutUrl("/logout").deleteCookies("JSESSIONID").permitAll();
		http.csrf().disable();
		http.sessionManagement().maximumSessions(1).and().sessionFixation().migrateSession();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**");
	}
}
