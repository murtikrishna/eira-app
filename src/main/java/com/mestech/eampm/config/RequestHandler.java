
/******************************************************
 * 
 *    	Filename	: RequestHandler.java
 *      
 *      Author		: Johnson Edwinraj
 *      
 *      Time Stamp	: 29-Apr-19 09:47 AM  
 *      
 *      Description : This class is used to filter request.
 *      
 *      
 *******************************************************/
package com.mestech.eampm.config;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.mestech.eampm.bean.LoginUserDetails;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.User;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

@Component("requestHandler")
public class RequestHandler extends OncePerRequestFilter implements AuthenticationProvider {
	@Autowired
	@Qualifier("userService")
	private UserDetailsService userDetailsService;
	// @Autowired
	// private RoleActivityService roleActivityService;
	// @Autowired
	// private ActivityService activityService;
	// @Autowired
	// private UserService userService;
	@Autowired
	private AccessDenied accessDenied;

	@SuppressWarnings("unchecked")
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		try {
			Cache activityCache = CacheHandler.getCache("protectedActivity");
			Element protectedelement = activityCache.get("protectedPath");
			List<String> protectedPath = (List<String>) protectedelement.getObjectValue();
			Cache innerCache = CacheHandler.getCache("innerUrl");
			Element innerPath = innerCache.get("innerPath");
			List<String> innerPaths = null;
			if (innerPath != null) {
				innerPaths = (List<String>) innerPath.getObjectValue();
			}
			if ((protectedPath != null && !protectedPath.isEmpty() && protectedPath.contains(request.getServletPath()))
					|| (innerPaths != null && !innerPaths.isEmpty()
							&& innerPaths.contains(request.getServletPath().replaceAll("[0-9]", "")))) {
				boolean isValid = false;
				if (SecurityContextHolder.getContext().getAuthentication() != null
						&& !"/access-denied".equals(request.getServletPath())) {
					LoginUserDetails loginUserDetails = (LoginUserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal();
					if (null != loginUserDetails) {
						Cache userCache = CacheHandler.getCache("userCache");
						Element element = userCache.get(loginUserDetails.getEampmuserid());
						User user = null;
						if (element != null)
							user = (User) element.getObjectValue();
						if (null != user) {
							if (null != user.getRoleID()) {
								Cache roleActivityCache = CacheHandler.getCache("roleActivityCache");
								Element roleActiveElement = roleActivityCache.get(user.getRoleID());
								List<RoleActivity> roleActivities = null;
								if (roleActiveElement != null)
									roleActivities = (List<RoleActivity>) roleActiveElement.getObjectValue();
								if (roleActivities != null && !roleActivities.isEmpty()) {
									for (RoleActivity roleActivity : roleActivities) {
										if (roleActivity.getActivityID() != null) {
											Cache activityByIdCache = CacheHandler.getCache("activityCache");
											Element activityElement = activityByIdCache
													.get(roleActivity.getActivityID());
											if (activityElement == null)
												continue;
											Activity activity = (Activity) activityElement.getObjectValue();
											if (activity != null && activity.getActivityUrl() != null) {
												if (request.getServletPath().equals(activity.getActivityUrl())) {
													isValid = true;
													break;
												} else {
													Cache cache = CacheHandler.getCache("innerUrl");
													Element element2 = cache.get(activity.getActivityId());
													if (element2 == null)
														continue;
													List<String> urls = (List<String>) element2.getObjectValue();
													if (urls != null && !urls.isEmpty() && urls.contains(
															request.getServletPath().replaceAll("[0-9]", ""))) {
														isValid = true;
														break;
													}
												}
											}
										}
									}
								}

							}
						}
					}
				} else if (SecurityContextHolder.getContext().getAuthentication() == null
						&& request.getAttribute("LoginPage") == null) {
					request.setAttribute("LoginPage", "Loaded");
					response.sendRedirect("./");
					filterChain.doFilter(request, response);
				} else if ("/access-denied".equals(request.getServletPath())) {
					request.setAttribute("AccessDenied", "Loaded");
					filterChain.doFilter(request, response);
				}
				if (isValid) {
					filterChain.doFilter(request, response);
				} else if (request.getAttribute("LoginPage") == null) {
					accessDenied.handle(request, response, null);
				}
			} else {
				filterChain.doFilter(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		WebAuthenticationDetails authenticationDetails = (WebAuthenticationDetails) authentication.getDetails();
		if (authenticationDetails.getSessionId() != null && !authenticationDetails.getSessionId().equals("")) {
			UserDetails userDetails = userDetailsService.loadUserByUsername(
					authentication.getName() + "johnEdwin333" + authentication.getCredentials().toString()
							+ "johnEdwin333" + authenticationDetails.getSessionId());
			if (userDetails != null) {
				LoginUserDetails loginUserDetails = new LoginUserDetails(authentication.getName(),
						Integer.valueOf(userDetails.getUsername()), null, authenticationDetails.getSessionId(),
						authenticationDetails.getRemoteAddress());
				SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
						loginUserDetails, userDetails.getPassword(), userDetails.getAuthorities()));
				return new UsernamePasswordAuthenticationToken(loginUserDetails, userDetails.getPassword(),
						userDetails.getAuthorities());
			}
		}
		return null;

	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
